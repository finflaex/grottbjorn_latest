﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DESDB;
using Finflaex.fxMVC.Authorization;
using Finflaex.fxMVC.WebSocket;
using Finflaex.HtmlDom;
using Finflaex.MsSQL.Connection;
using Finflaex.Reflection;
using modLkClient.Models;

#endregion

namespace modLkClient
{
    [Export(typeof (IController))]
    public class modLkClient : WSController
    {
        [Roles("client")]
        public async Task<ActionResult> Index(string id)
        {
            var result = new List<string>();

            // start threads

            var get_list_ds = GetMainListDs(id);
            var get_list_to = GetMainListTo(id);
            var get_list_instrument = GetMainListInstrument(id);
            var get_list_docs = GetMainListFinishDateDocuments(id);

            // get ds

            var data_ds = await get_list_ds;

            var title = "Состояние портфеля на "
                .Append((data_ds.FirstOrDefault()?.CurrentDate ?? DateTime.Now).ToString("dd.MM.yyyy"));

            fxcHtmlTag
                .H4(title)
                .WrapDiv.AddClass("page-header")
                .WrapDiv.AddClass("clearfix")
                .ToString()
                .Action(result.Add);

            var content = fxcHtmlTag
                .Div().AddClass("row");

            var column = fxcHtmlTag.Div().Class("col-xs-12", "col-sm-12", "col-md-6", "col-lg-6");

            if (data_ds.Any())
            {
                var sum_ds = data_ds.Sum(v => v.Result);

                data_ds
                    .GroupBy(v => v.CurrencyName)
                    .Select(item => new ModelListDs
                    {
                        CurrencyName = item.Key,
                        Rate = item.Sum(v => v.Rate)/item.Count(),
                        Balance = item.Sum(v => v.Balance),
                        Block = item.Sum(v => v.Block),
                        Dolg = item.Sum(v => v.Dolg),
                        Result = item.Sum(v => v.Result)
                    })
                    .ToArray()
                    .Action(item =>
                    {
                        GenerateInfoMainListDS_CurrencyPlace("В разрезе валют", item, sum_ds)
                            .Action(v => column.AddContent(v));
                    });

                data_ds
                    .GroupBy(v => v.MarketPlace)
                    .Select(v => GenerateInfoMainListDS_MarketPlaces(v.Key, v.ToArray(), sum_ds))
                    .ForEach(v => column.AddContent(v));
            }

            if (column.ContentList.Any())
            {
                content.Content(column);
            }

            var data_instr = await get_list_instrument;

            var column2 = fxcHtmlTag.Div()
                .Class("col-xs-12", "col-sm-12", "col-md-6", "col-lg-6");

            if (data_instr.Any())
            {
                GenerateInfoMainListInstrument("Инструменты", data_instr)
                    .Action(v => column2.AddContent(v));
            }

            var data_docs = (await get_list_docs)
                .OrderBy(v=>v.EndDate)
                .ToArray();

            if (data_docs.Any())
            {
                GenerateInfoMainListDocuments("Документы", data_docs)
                    .Action(v => column2.AddContent(v));
            }

            if (column2.ContentList.Any())
            {
                content.Content(column2);
            }

            result.Add(content);

            var data_to = await get_list_to;

            if (data_to.Any())
            {
                GenerateInfoMainListTO("Требования и обязательства", data_to)
                    .Action(v => result.Add(v));
            }


            //var data_instrument = await GetMainListInstrument();
            //var data_documents = await GetMainListFinishDateDocuments();
            //var info_instrument = GenerateInfoMainListInstrument("Инструменты", data_instrument);
            //var info_document = GenerateInfoMainListDocuments("Документы", data_documents);

            //result.Add(new fxcHtmlTag(fxeHtmlTag.div)
            //        .AddClass("row")
            //        .AddContent(info_instrument, info_document)
            //        .ToString());

            //var data_to = await GetMainListTo();

            //GenerateInfoMainListTO("Требования и обязательства", data_to)
            //    .IfNotNull(item => result.Add(new fxcHtmlTag(fxeHtmlTag.div)
            //        .AddClass("row")
            //        .AddContent(item)
            //        .ToString()));

            return this
                .GetResource("index")
                .ToString(Encoding.UTF8)
                .Replace(new Dictionary<string, string>
                {
                    {"<!--table-->", result.Join(string.Empty)}
                })
                .SelectSingle(Content);
        }

        private fxcHtmlTag GenerateInfoMainListDocuments(
            string key,
            ModelListFinishDocument[] item)
        {
            var caption = fxcHtmlTag.H3(key)
                .Style(new CSS
                {
                    Float = fxeFloat.left
                })
                .WrapCaption;

            var market_head = fxcHtmlTag.TR()
                .AddContent(fxcHtmlTag.TH("Наименование документа"))
                .AddContent(fxcHtmlTag.TH("Срок окончания")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .WrapTHead;

            var market_body = new fxcHtmlTag(fxeHtmlTag.tbody);

            item
                .Select(record =>
                {
                    var line = fxcHtmlTag.TR()
                        .AddContent(fxcHtmlTag.TD(record.DocumentName)
                            .Style(new CSS
                            {
                                TextAlign = fxeHorizontalAlign.left
                            }))
                        .AddContent(fxcHtmlTag.TD(record.EndDate.ToString("dd.MM.yyyy"))
                            .Style(new CSS
                            {
                                TextAlign = fxeHorizontalAlign.right
                            }));
                    if (record.EndDate < DateTime.Now)
                    {
                        line.Class("danger");
                    }
                    else if (record.EndDate < DateTime.Now.AddMonths(1))
                    {
                        line.Class("warning");
                    }

                    return line;
                })
                .ToArray()
                .Action(market_body.AddContent);

            return fxcHtmlTag
                .Table()
                .Content(caption)
                .Content(market_head)
                .Content(market_body)
                .Class("table", "table-condensed", "table-responsive")
                .WrapDiv
                .Class("panel-body")
                .WrapDiv
                .Class("panel", "panel-default")

                //.WrapDiv
                //.Class("col-xs-12", "col-sm-12", "col-md-6", "col-lg-6")
                ;
        }

        private fxcHtmlTag GenerateInfoMainListInstrument(
            string key,
            ModelListInstrument[] item)
        {
            var caption = fxcHtmlTag.H3(key)
                .Style(new CSS
                {
                    Float = fxeFloat.left
                })
                .WrapCaption;

            var market_head = fxcHtmlTag.TR()
                .AddContent(fxcHtmlTag.TH("Инструмент"))
                .AddContent(fxcHtmlTag.TH("Количество")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Курс")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .WrapTHead;

            var market_body = new fxcHtmlTag(fxeHtmlTag.tbody);

            item
                .Select(line => fxcHtmlTag.TR()
                    .AddContent(fxcHtmlTag.TD(line.InstrumentName)
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.left
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Count.ToString())
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Rate.CompareExt(1) ? string.Empty : line.Rate.ToString("N"))
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                )
                .ToArray()
                .Action(market_body.AddContent);

            return fxcHtmlTag
                .Table()
                .Content(caption)
                .Content(market_head)
                .Content(market_body)
                .Class("table", "table-condensed", "table-responsive")
                .WrapDiv
                .Class("panel-body")
                .WrapDiv
                .Class("panel", "panel-default")

                //.WrapDiv
                //.Class("col-xs-12", "col-sm-12", "col-md-6", "col-lg-6")
                ;
        }

        private static fxcHtmlTag GenerateInfoMainListTO(
            string key,
            ModelListTO[] item)
        {
            var caption = fxcHtmlTag.H3(key)
                .Style(new CSS
                {
                    Float = fxeFloat.left
                })
                .WrapCaption;

            var market_head = fxcHtmlTag.TR()
                .AddContent(fxcHtmlTag.TH("Дата исполнения"))
                .AddContent(fxcHtmlTag.TH("Валюта"))
                .AddContent(fxcHtmlTag.TH("Инструмент"))
                .AddContent(fxcHtmlTag.TH("Требование")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Обязательство")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Итого")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .WrapTHead;

            var market_body = new fxcHtmlTag(fxeHtmlTag.tbody);

            item
                .Select(line => fxcHtmlTag.TR()
                    .Content(fxcHtmlTag.TD(line.SettleDate.ToString("dd.MM.yyyy")))
                    .Content(fxcHtmlTag.TD(line.CurrencyName ?? string.Empty))
                    .Content(fxcHtmlTag.TD(line.InstrumentName ?? string.Empty))
                    .Content(fxcHtmlTag.TD(line.NeedOut.CompareExt(0) ? string.Empty : line.NeedOut.ToString("N"))
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .Content(fxcHtmlTag.TD(line.NeedIn.CompareExt(0) ? string.Empty : line.NeedIn.ToString("N"))
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .Content(fxcHtmlTag.TD(line.Result.ToString("N"))
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                )
                .ToArray()
                .Action(market_body.AddContent);

            var result = fxcHtmlTag
                .Table()
                .Content(caption)
                .Content(market_head)
                .Content(market_body)
                .Class("table", "table-condensed", "table-responsive")
                .WrapDiv
                .Class("panel-body")
                .WrapDiv
                .Class("panel", "panel-default")
                .WrapDiv
                .Class("col-lg-8");

            var temp = fxcHtmlTag.Div()
                .Class("col-lg-2", "hidden-xs", "hidden-sm", "hidden-md");

            return fxcHtmlTag.Div()
                .Content(temp)
                .Content(result)
                .Content(temp)
                .Class("row");
        }

        private static fxcHtmlTag GenerateInfoMainListDS_MarketPlaces(
            string key,
            ModelListDs[] item,
            double sum)
        {
            var item_sum = item.Sum(v => v.Result);
            var proc = item.Sum(v => v.Result)/(sum/100);

            var summ_title = fxcHtmlTag.H3(item_sum.ToString("N"))
                .Style(new CSS
                {
                    Float = fxeFloat.right,
                    MarginLeft = "30px"
                });

            var market_sum = new fxcHtmlTag(fxeHtmlTag.div)
            {
                ["data-dimension"] = "100",
                ["data-text"] = proc.ToString("N") + "%",
                ["data-width"] = "12",
                ["data-percent"] = $"{Math.Round(proc)}",
                ["data-type"] = "half"
            }
                .Class("progress-circular-blue");

            var caption = fxcHtmlTag.H3(key)
                .Style(new CSS
                {
                    Float = fxeFloat.left
                })
                .WrapCaption
                .Content(market_sum);

            var market_head = fxcHtmlTag.TR()
                .AddContent(fxcHtmlTag.TH("Валюта"))
                .AddContent(fxcHtmlTag.TH("Остаток")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Блокировано")
                    .Class("hidden-xs", "visible-sm", "hidden-md", "visible-lg")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Долг перед брокером")
                    .Class("hidden-xs", "visible-sm", "hidden-md", "visible-lg")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Курс")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Остаток по курсу")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .WrapTHead;

            var market_body = fxcHtmlTag.THead();

            item
                .Select(line => fxcHtmlTag.TR()
                    .AddContent(fxcHtmlTag.TD(line.CurrencyName)
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.left
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Balance.ToString("N"))
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Block.CompareExt(0) ? string.Empty : line.Block.ToString("N"))
                        .Class("hidden-xs", "visible-sm", "hidden-md", "visible-lg")
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Dolg.CompareExt(0) ? string.Empty : line.Dolg.ToString("N"))
                        .Class("hidden-xs", "visible-sm", "hidden-md", "visible-lg")
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Rate.CompareExt(1) ? string.Empty : line.Rate.ToString("N"))
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Result.ToString("N"))
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                )
                .ToArray()
                .Action(market_body.AddContent);

            return fxcHtmlTag
                .Table()
                .Content(caption)
                .Content(market_head)
                .Content(market_body)
                .Class("table", "table-condensed", "table-responsive")
                .WrapDiv
                .Content(summ_title)
                .Class("panel-body")
                .WrapDiv
                .Class("panel", "panel-default")

                //.WrapDiv
                //.Class("col-xs-12", "col-sm-12", "col-md-6", "col-lg-6")
                ;
        }

        private static fxcHtmlTag GenerateInfoMainListDS_CurrencyPlace(
            string key,
            ModelListDs[] item,
            double sum)
        {
            var summ_title = fxcHtmlTag.H3(sum.ToString("N"))
                .Style(new CSS
                {
                    Float = fxeFloat.right,
                    MarginLeft = "30px"
                });

            var caption = fxcHtmlTag.H3(key)
                .Style(new CSS
                {
                    Float = fxeFloat.left
                })
                .WrapCaption;

            var market_head = fxcHtmlTag.TR()
                .AddContent(fxcHtmlTag.TH("Валюта"))
                .AddContent(fxcHtmlTag.TH("Остаток")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Блокировано")
                    .Class("hidden-xs", "visible-sm", "hidden-md", "visible-lg")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Долг перед брокером")
                    .Class("hidden-xs", "visible-sm", "hidden-md", "visible-lg")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Курс")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .AddContent(fxcHtmlTag.TH("Остаток по курсу")
                    .Style(new CSS
                    {
                        TextAlign = fxeHorizontalAlign.right
                    }))
                .WrapTHead;

            var market_body = fxcHtmlTag.THead();

            item
                .Select(line => fxcHtmlTag.TR()
                    .AddContent(fxcHtmlTag.TD(line.CurrencyName)
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.left
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Balance.ToString("N"))
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Block.CompareExt(0) ? string.Empty : line.Block.ToString("N"))
                        .Class("hidden-xs", "visible-sm", "hidden-md", "visible-lg")
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Dolg.CompareExt(0) ? string.Empty : line.Dolg.ToString("N"))
                        .Class("hidden-xs", "visible-sm", "hidden-md", "visible-lg")
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Rate.CompareExt(1) ? string.Empty : line.Rate.ToString("N"))
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        }))
                    .AddContent(fxcHtmlTag.TD(line.Result.ToString("N"))
                        .Style(new CSS
                        {
                            TextAlign = fxeHorizontalAlign.right
                        })))
                .ToArray()
                .Action(market_body.AddContent);

            return fxcHtmlTag
                .Table()
                .Content(caption)
                .Content(market_head)
                .Content(market_body)
                .Class("table", "table-condensed", "table-responsive")
                .WrapDiv
                .Content(summ_title)
                .Class("panel-body")
                .WrapDiv
                .Class("panel", "panel-default")
                .Style(new CSS())

                //.WrapDiv
                //.Class("col-xs-12", "col-sm-12", "col-md-6", "col-lg-6")
                ;
        }

        #region DB

        public Task<ModelListDs[]> GetMainListDs(string id = null)
        {
            return Task.Run(() =>
            {
                var sql = this
                    .GetResource("SelectMainListDS")
                    .ToString(Encoding.UTF8)
                    .Replace(new Dictionary<string, string>
                    {
                        {"\\-\\-\\'\\[ClientCode\\]\\'", $"'{id ?? User.Login}'"}
                    });

                return fxcConnection
                    .Connect(GDB.ClientCS, connect => connect
                        .ExecuteReader(reader => reader
                            .ReadValues()
                            .Select(v => new ModelListDs(v))
                            .ToArray(), sql));
            });
        }

        public Task<ModelListTO[]> GetMainListTo(string id = null)
        {
            return Task.Run(() =>
            {
                var sql = this
                    .GetResource("SelectMainListTO")
                    .ToString(Encoding.UTF8)
                    .Replace(new Dictionary<string, string>
                    {
                        {"\\-\\-\\'\\[ClientCode\\]\\'", $"'{id ?? User.Login}'"}
                    });

                return fxcConnection
                    .Connect(GDB.ClientCS, connect => connect
                        .ExecuteReader(reader => reader
                            .ReadValues()
                            .Select(v => new ModelListTO(v))
                            .ToArray(), sql));
            });
        }

        public Task<ModelListInstrument[]> GetMainListInstrument(string id = null)
        {
            return Task.Run(() =>
            {
                var sql = this
                    .GetResource("SelectMainListInstrument")
                    .ToString(Encoding.UTF8)
                    .Replace(new Dictionary<string, string>
                    {
                        {"\\-\\-\\'\\[ClientCode\\]\\'", $"'{id ?? User.Login}'"}
                    });

                return fxcConnection
                    .Connect(GDB.ClientCS, connect => connect
                        .ExecuteReader(reader => reader
                            .ReadValues()
                            .Select(v => new ModelListInstrument(v))
                            .ToArray(), sql));
            });
        }

        public Task<ModelListFinishDocument[]> GetMainListFinishDateDocuments(string id = null)
        {
            return Task.Run(() =>
            {
                var sql = this
                    .GetResource("SelectMainListFinishDateDocuments")
                    .ToString(Encoding.UTF8)
                    .Replace(new Dictionary<string, string>
                    {
                        {"\\-\\-\\'\\[ClientCode\\]\\'", $"'{id ?? User.Login}'"}
                    });

                return fxcConnection
                    .Connect(GDB.ClientCS, connect => connect
                        .ExecuteReader(reader => reader
                            .ReadValues()
                            .Select(v => new ModelListFinishDocument(v))
                            .ToArray(), sql));
            });
        }

        #endregion
    }
}