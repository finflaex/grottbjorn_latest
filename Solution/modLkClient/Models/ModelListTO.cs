﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Razor.Parser.SyntaxTree;
using Finflaex.Reflection;

namespace modLkClient.Models
{
    public class ModelListTO
    {
        public ModelListTO(object[] data)
        {
            // ReSharper disable once PossibleInvalidOperationException
            SettleDate = data[2].AsType<string>().ToDateTime("dd.MM.yyyy").Value;
            CurrencyName = data[3].AsType<string>();
            InstrumentName = data[4].AsType<string>();
            NeedOut = data[5].AsType<string>().ToDouble();
            NeedIn = data[6].AsType<string>().ToDouble();
            Result = data[7].AsType<string>().ToDouble();
        }

        public DateTime SettleDate { get; set; }
        public string CurrencyName { get; set; }
        public string InstrumentName { get; set; }
        public double NeedOut { get; set; }
        public double NeedIn { get; set; }
        public double Result { get; set; }
    }
}
