﻿#region

using Finflaex.Reflection;

#endregion

namespace modLkClient.Models
{
    public class ModelListInstrument
    {
        public ModelListInstrument(object[] data)
        {
            InstrumentName = data[2].AsType<string>();
            Count = data[6].AsType<string>().ToInt();
            Rate = data[7].AsType<string>().ToDouble();
        }

        public string InstrumentName { get; set; }
        public int Count { get; set; }
        public double Rate { get; set; }
    }
}