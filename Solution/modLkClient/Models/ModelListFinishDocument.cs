﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Reflection;

namespace modLkClient.Models
{
    public class ModelListFinishDocument
    {
        public ModelListFinishDocument(object[] data)
        {
            DocumentName = data[1].AsType<string>();
            EndDate = Convert.ToDateTime(data[2]);
        }

        public string DocumentName { get; set; }
        public DateTime EndDate { get; set; }
    }
}
