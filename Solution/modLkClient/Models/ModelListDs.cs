﻿#region

using System;
using Finflaex.Reflection;

#endregion

namespace modLkClient.Models
{
    public class ModelListDs
    {
        public ModelListDs()
        {
            
        }
        public ModelListDs(object[] data)
        {
            // ReSharper disable once PossibleInvalidOperationException
            CurrentDate = data[1].AsType<string>().ToDateTime("dd.MM.yyyy").Value;
            CurrencyName = data[2].AsType<string>();
            Balance = data[7].AsType<string>().ToDouble();
            Block = data[8].AsType<string>().ToDouble();
            Dolg = data[9].AsType<string>().ToDouble();
            Rate = data[10].AsType<string>().ToDouble();
            Result = data[11].AsType<string>().ToDouble();
            MarketPlace = data[12].AsType<string>();
        }

        public DateTime CurrentDate { get; set; }
        public double Balance { get; set; }
        public double Block { get; set; }
        public string CurrencyName { get; set; }
        public double Dolg { get; set; }
        public string MarketPlace { get; set; }
        public double Rate { get; set; }
        public double Result { get; set; }
    }
}