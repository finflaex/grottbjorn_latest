﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.fxMVC.Authorization;
using Finflaex.Plugin;
using Finflaex.Reflection;
using Finflaex.WS;

namespace modLkClient
{
    [Export(typeof (fxiMvcModul))]
    public class cfgLkClient : fxaMvcModul
    {
        [Roles("client")]
        public override IEnumerable<fxiModulMenuItem> Menu
            => new List<fxiModulMenuItem>()
            {
                new fxaModulMenuItem()
                {
                    Caption = "Портфель",
                    Icon = "fa fa-briefcase",
                    Action = "WsLib.request("
                             + fxsJsonResponse
                                 .LoadHtmlPost(
                                     $"{nameof(modLkClient)}/Index"
                                     , "#page")
                                 .ToJson()
                             + ")"
                },
                new fxaModulMenuItem()
                {
                    Caption = "Поручения",
                    Icon = "fa fa-edit",
                },
                new fxaModulMenuItem()
                {
                    Caption = "Экспорт",
                    Icon = "fa fa-file-code-o",
                },
                new fxaModulMenuItem()
                {
                    Caption = "Торги",
                    Icon = "fa fa-line-chart",
                },
                new fxaModulMenuItem()
                {
                    Caption = "Отчеты",
                    Icon = "fa fa-paste",
                },
                new fxaModulMenuItem()
                {
                    Caption = "Депонентам",
                    Icon = "fa fa-users",
                },
                new fxaModulMenuItem()
                {
                    Caption = "Софт",
                    Icon = "fa fa-save",
                },
            };
    }
}
