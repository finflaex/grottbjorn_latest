﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Self;
using Finflaex.Service;

namespace wcfSertificate
{
    internal class WcfCertificate : fxcService<WcfCertificate>
    {
        private CWcfCertificate wcf;

        public WcfCertificate()
        {

        }

        private static void Main(string[] args)
        {
            if (args.Any() && args.Contains("install"))
            {
                Install(fxsMain.ExecutablePath);
            }
            else if (args.Any() && args.Contains("uninstall"))
            {
                Uninstall();
            }
            else
            {
                if (!IsInstall())Install();
                new WcfCertificate().Run();
            }
        }

        protected override void OnStart(string[] args)
        {
            wcf = new CWcfCertificate().DoWork(() => true);
        }

        protected override void OnStop()
        {
            wcf.DoWork(() => false);
        }

        protected override void OnPause()
        {
            wcf.DoWork(() => false);
        }

        protected override void OnContinue()
        {
            wcf.DoWork(() => true);
        }
    }
}
