﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Certificate;
using Finflaex.Reflection;
using Finflaex.WCF;

namespace wcfSertificate
{
    public class CWcfCertificate 
        : fxcWcfService<CWcfCertificate, IWcfSertificate>
        , IWcfSertificate
    {
        public CWcfCertificate() 
            : base("certificate")
        {
        }

        public X509Certificate2 GetBySerial(string serial)
            => fxsCertificate.FindBySerial(serial);

        public X509Certificate2[] GetCertificates() => fxsCertificate.List;

        public byte[] Sign(string serial, byte[] data)
        {
            var cert = fxsCertificate.FindBySerial(serial);
            var algoritm = new Oid("2.16.840.1.101.3.4.2.1", "sha256");

            return data.CertSign(cert, algoritm);
        }
    }
}
