﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace wcfSertificate
{
    [ServiceContract]
    public interface IWcfSertificate
    {
        [OperationContract]
        X509Certificate2[] GetCertificates();

        [OperationContract]
        X509Certificate2 GetBySerial(string serial);

        [OperationContract]
        byte[] Sign(string serial, byte[] data);
    }
}
