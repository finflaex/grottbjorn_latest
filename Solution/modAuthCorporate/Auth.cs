﻿#region

using System.ComponentModel.Composition;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DESDB;
using Finflaex.fxMVC.Authorization;
using Finflaex.fxMVC.WebSocket;
using Finflaex.Reflection;
using Finflaex.WS;

#endregion

namespace modAuthCorporate
{
    [Export(typeof (IController))]
    public class Auth : WSController
    {
        [Roles(true, false)]
        public ActionResult FormLogin()
        {
            return this
                .GetResource("index")
                .ToString(Encoding.UTF8)
                .ToHtmlMin()
                .SelectSingle(Content);
        }

        [Roles(true)]
        public ActionResult ActionLogin(string email, string password, bool? remember = false)
        {
            //var email = Data.GetString("l");
            //var password = Data.GetString("p");
            //var persistent = Data.Get<bool>("b");
            var auth = GDB.CreateAuthentificate(email, password, Request.UserHostAddress, Request.UserHostName,
                User.Guid);

            HttpContext.SetUser(auth, remember ?? false);

            if (auth.Login == null)
            {
                return RedirectToAction("FormLogin");
            }
            return Redirect("/");
        }

        [Roles]
        public ActionResult ActionLogout()
        {
            var auth = HttpContext.SetUser();
            return Redirect("/");
        }
    }
}