﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using Finflaex.Plugin;
using Finflaex.Reflection;

#endregion

namespace mvcWebix
{
    [Export(typeof (fxiMvcPlugin))]
    public class mvcWebix : fxaMvcPlugin
    {
        public override string Name => "Webix";

        public override string HeadScript
            => this.GetResource(nameof(HeadScript) + ".js").ToString(Encoding.UTF8)
               + ";" +
               this.GetResource(nameof(HeadScript) + "_skin.js").ToString(Encoding.UTF8)
               + ";" +
               this.GetResource(nameof(HeadScript) + "_ru.js").ToString(Encoding.UTF8);

        public override string HeadStyle
            => this.GetResource(nameof(HeadStyle)).ToString(Encoding.UTF8);

        public override string Template
            => this.GetResource(nameof(Template)).ToString(Encoding.UTF8);

        public override string GetContent(params object[] param)
        {
            var data = param.FirstOrDefault() ?? new object();
            return Template.Replace(new Dictionary<string, string>
            {
                {"(\\{\\/\\*data\\*\\/\\})", data.ToJson()}
            });
        }
    }
}