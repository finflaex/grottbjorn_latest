﻿#region

using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using Finflaex.Plugin;
using Finflaex.Reflection;

#endregion

namespace mvcFileUpload
{
    [Export(typeof (fxiMvcPlugin))]
    public class mvcFileUpload : fxaMvcPlugin
    {
        public override string HeadStyle
            => this.GetResource(nameof(HeadStyle)).ToString(Encoding.UTF8);

        public override string Template
            => this.GetResource("Template").ToString(Encoding.UTF8);

        public override string Name => "FileUpload";

        public override string HeadScript
            => this
                .GetResources(nameof(HeadScript))
                .Select(v => v.ToString(Encoding.UTF8))
                .Join(";");

        public override string AfterContentScript
            => this.GetResource(nameof(AfterContentScript)).ToString(Encoding.UTF8);

        public string ID => "id=[\",'](.+)[\",']".ToRegex().Match(Body).Groups[1].Value;

        public override string GetContent(params object[] param)
        {
            var model = param.FirstOrDefault();

            var after = AfterContentScript.Replace(new Dictionary<string, string>()
            {
                {"({ID})", ID},
                {"(\\/\\*model\\*\\/)", model?.ToJson() ?? string.Empty},
            });

            return new StringBuilder()
                .AppendLine(Body)
                .AppendLine(after.ScriptTagWrap())
                .ToString();
        }
    }
}