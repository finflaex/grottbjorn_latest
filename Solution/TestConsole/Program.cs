﻿#region

using System;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using Finflaex.Certificate;
using Finflaex.Lan;
using Finflaex.Reflection;
using TestConsole.SBC;

#endregion

namespace TestConsole
{
    internal class Program
    {
        [ServiceContract]
        public interface IHelloWorldService
        {
            [OperationContract]
            string SayHello(string name);
        }

        public class HelloWorldService : IHelloWorldService
        {
            public string SayHello(string name)
            {
                return string.Format("Hello, {0}", name);
            }
        }

        private static void Main(string[] args)
        {
            var client = new WcfSertificateClient();

            var sign = "test string"
                .ToBytes(Encoding.UTF8)
                .SelectSingle(data => client.Sign("4e4c5e8d000000008551", data))
                .UrlTokenEncode();

            Console.WriteLine(sign);

            var result = sign
                .UrlTokenDecode()
                .CertUnSign()
                .ToString(Encoding.UTF8);

            Console.WriteLine(result);

            Console.ReadLine();

            client.Close();
        }
    }
}