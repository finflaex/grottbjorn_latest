namespace DESDB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class restart05092016 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tDescriptions",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Value = c.String(),
                        sysActDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysStartDT = c.DateTime(nullable: false),
                        sysStatus = c.Int(nullable: false),
                        UserChange_sysGuid = c.Guid(nullable: false),
                        sysNext_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.tUsers", t => t.UserChange_sysGuid)
                .ForeignKey("dbo.tDescriptions", t => t.sysNext_sysGuid)
                .Index(t => t.UserChange_sysGuid)
                .Index(t => t.sysNext_sysGuid);
            
            CreateTable(
                "dbo.tDirectories",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Name = c.String(),
                        sysActDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysStartDT = c.DateTime(nullable: false),
                        sysStatus = c.Int(nullable: false),
                        Description_sysGuid = c.Guid(),
                        UserChange_sysGuid = c.Guid(nullable: false),
                        Json_sysGuid = c.Guid(),
                        Root_sysGuid = c.Guid(),
                        sysNext_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.tDescriptions", t => t.Description_sysGuid, cascadeDelete: true)
                .ForeignKey("dbo.tUsers", t => t.UserChange_sysGuid)
                .ForeignKey("dbo.tJsonDatas", t => t.Json_sysGuid, cascadeDelete: true)
                .ForeignKey("dbo.tDirectories", t => t.Root_sysGuid)
                .ForeignKey("dbo.tDirectories", t => t.sysNext_sysGuid)
                .Index(t => t.Description_sysGuid)
                .Index(t => t.UserChange_sysGuid)
                .Index(t => t.Json_sysGuid)
                .Index(t => t.Root_sysGuid)
                .Index(t => t.sysNext_sysGuid);
            
            CreateTable(
                "dbo.tFiles",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Name = c.String(),
                        Ext = c.String(),
                        Size = c.Long(nullable: false),
                        Content = c.Binary(),
                        Directory = c.String(),
                        sysActDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysStartDT = c.DateTime(nullable: false),
                        sysStatus = c.Int(nullable: false),
                        Description_sysGuid = c.Guid(),
                        UserChange_sysGuid = c.Guid(nullable: false),
                        tUser_sysGuid = c.Guid(),
                        Json_sysGuid = c.Guid(),
                        sysNext_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.tDescriptions", t => t.Description_sysGuid, cascadeDelete: true)
                .ForeignKey("dbo.tUsers", t => t.UserChange_sysGuid)
                .ForeignKey("dbo.tUsers", t => t.tUser_sysGuid)
                .ForeignKey("dbo.tJsonDatas", t => t.Json_sysGuid, cascadeDelete: true)
                .ForeignKey("dbo.tFiles", t => t.sysNext_sysGuid)
                .Index(t => t.Description_sysGuid)
                .Index(t => t.UserChange_sysGuid)
                .Index(t => t.tUser_sysGuid)
                .Index(t => t.Json_sysGuid)
                .Index(t => t.sysNext_sysGuid);
            
            CreateTable(
                "dbo.tJsonDatas",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Value = c.String(),
                        sysActDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysStartDT = c.DateTime(nullable: false),
                        sysStatus = c.Int(nullable: false),
                        sysNext_sysGuid = c.Guid(),
                        UserChange_sysGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.tJsonDatas", t => t.sysNext_sysGuid)
                .ForeignKey("dbo.tUsers", t => t.UserChange_sysGuid)
                .Index(t => t.sysNext_sysGuid)
                .Index(t => t.UserChange_sysGuid);
            
            CreateTable(
                "dbo.tUsers",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Password = c.String(),
                        Login = c.String(),
                        sysActDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysStartDT = c.DateTime(nullable: false),
                        sysStatus = c.Int(nullable: false),
                        UserChage_sysGuid = c.Guid(nullable: false),
                        sysNext_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.tUsers", t => t.UserChage_sysGuid)
                .ForeignKey("dbo.tUsers", t => t.sysNext_sysGuid)
                .Index(t => t.UserChage_sysGuid)
                .Index(t => t.sysNext_sysGuid);
            
            CreateTable(
                "dbo.tRoles",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.sysGuid);
            
            CreateTable(
                "dbo.tSessions",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        DateEnd = c.DateTime(),
                        DateStart = c.DateTime(),
                        Value = c.Guid(nullable: false),
                        HostAddress = c.String(),
                        HostName = c.String(),
                        User_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.tUsers", t => t.User_sysGuid, cascadeDelete: true)
                .Index(t => t.User_sysGuid);
            
            CreateTable(
                "dbo.tUsertRoles",
                c => new
                    {
                        tUser_sysGuid = c.Guid(nullable: false),
                        tRole_sysGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.tUser_sysGuid, t.tRole_sysGuid })
                .ForeignKey("dbo.tUsers", t => t.tUser_sysGuid)
                .ForeignKey("dbo.tRoles", t => t.tRole_sysGuid)
                .Index(t => t.tUser_sysGuid)
                .Index(t => t.tRole_sysGuid);
            
            CreateTable(
                "dbo.tDirectorytFiles",
                c => new
                    {
                        tDirectory_sysGuid = c.Guid(nullable: false),
                        tFile_sysGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.tDirectory_sysGuid, t.tFile_sysGuid })
                .ForeignKey("dbo.tDirectories", t => t.tDirectory_sysGuid)
                .ForeignKey("dbo.tFiles", t => t.tFile_sysGuid)
                .Index(t => t.tDirectory_sysGuid)
                .Index(t => t.tFile_sysGuid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tDescriptions", "sysNext_sysGuid", "dbo.tDescriptions");
            DropForeignKey("dbo.tDirectories", "sysNext_sysGuid", "dbo.tDirectories");
            DropForeignKey("dbo.tDirectories", "Root_sysGuid", "dbo.tDirectories");
            DropForeignKey("dbo.tDirectories", "Json_sysGuid", "dbo.tJsonDatas");
            DropForeignKey("dbo.tDirectorytFiles", "tFile_sysGuid", "dbo.tFiles");
            DropForeignKey("dbo.tDirectorytFiles", "tDirectory_sysGuid", "dbo.tDirectories");
            DropForeignKey("dbo.tFiles", "sysNext_sysGuid", "dbo.tFiles");
            DropForeignKey("dbo.tFiles", "Json_sysGuid", "dbo.tJsonDatas");
            DropForeignKey("dbo.tUsers", "sysNext_sysGuid", "dbo.tUsers");
            DropForeignKey("dbo.tSessions", "User_sysGuid", "dbo.tUsers");
            DropForeignKey("dbo.tUsertRoles", "tRole_sysGuid", "dbo.tRoles");
            DropForeignKey("dbo.tUsertRoles", "tUser_sysGuid", "dbo.tUsers");
            DropForeignKey("dbo.tFiles", "tUser_sysGuid", "dbo.tUsers");
            DropForeignKey("dbo.tUsers", "UserChage_sysGuid", "dbo.tUsers");
            DropForeignKey("dbo.tFiles", "UserChange_sysGuid", "dbo.tUsers");
            DropForeignKey("dbo.tDirectories", "UserChange_sysGuid", "dbo.tUsers");
            DropForeignKey("dbo.tJsonDatas", "UserChange_sysGuid", "dbo.tUsers");
            DropForeignKey("dbo.tDescriptions", "UserChange_sysGuid", "dbo.tUsers");
            DropForeignKey("dbo.tJsonDatas", "sysNext_sysGuid", "dbo.tJsonDatas");
            DropForeignKey("dbo.tFiles", "Description_sysGuid", "dbo.tDescriptions");
            DropForeignKey("dbo.tDirectories", "Description_sysGuid", "dbo.tDescriptions");
            DropIndex("dbo.tDirectorytFiles", new[] { "tFile_sysGuid" });
            DropIndex("dbo.tDirectorytFiles", new[] { "tDirectory_sysGuid" });
            DropIndex("dbo.tUsertRoles", new[] { "tRole_sysGuid" });
            DropIndex("dbo.tUsertRoles", new[] { "tUser_sysGuid" });
            DropIndex("dbo.tSessions", new[] { "User_sysGuid" });
            DropIndex("dbo.tUsers", new[] { "sysNext_sysGuid" });
            DropIndex("dbo.tUsers", new[] { "UserChage_sysGuid" });
            DropIndex("dbo.tJsonDatas", new[] { "UserChange_sysGuid" });
            DropIndex("dbo.tJsonDatas", new[] { "sysNext_sysGuid" });
            DropIndex("dbo.tFiles", new[] { "sysNext_sysGuid" });
            DropIndex("dbo.tFiles", new[] { "Json_sysGuid" });
            DropIndex("dbo.tFiles", new[] { "tUser_sysGuid" });
            DropIndex("dbo.tFiles", new[] { "UserChange_sysGuid" });
            DropIndex("dbo.tFiles", new[] { "Description_sysGuid" });
            DropIndex("dbo.tDirectories", new[] { "sysNext_sysGuid" });
            DropIndex("dbo.tDirectories", new[] { "Root_sysGuid" });
            DropIndex("dbo.tDirectories", new[] { "Json_sysGuid" });
            DropIndex("dbo.tDirectories", new[] { "UserChange_sysGuid" });
            DropIndex("dbo.tDirectories", new[] { "Description_sysGuid" });
            DropIndex("dbo.tDescriptions", new[] { "sysNext_sysGuid" });
            DropIndex("dbo.tDescriptions", new[] { "UserChange_sysGuid" });
            DropTable("dbo.tDirectorytFiles");
            DropTable("dbo.tUsertRoles");
            DropTable("dbo.tSessions");
            DropTable("dbo.tRoles");
            DropTable("dbo.tUsers");
            DropTable("dbo.tJsonDatas");
            DropTable("dbo.tFiles");
            DropTable("dbo.tDirectories");
            DropTable("dbo.tDescriptions");
        }
    }
}
