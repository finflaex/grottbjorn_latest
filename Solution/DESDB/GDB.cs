﻿#region

using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DESDB.Base;
using DESDB.Tables;
using Finflaex.fxMVC.Authorization;
using Finflaex.Log;
using Finflaex.MsSQL.Connection;
using Finflaex.Reflection;

#endregion

namespace DESDB
{
    public class GDB
    {
#if DEBUG
        public static string ClientCS
        //=> "Data Source=mars;Initial Catalog=WebClient;Integrated Security=False;User ID=sa;Password=411;";
        => "Data Source=10.0.0.120; Initial Catalog=WebClient;Integrated Security=False;User ID=WebMars;Password=webmars111;";
#else
        public static string ClientCS
            //=> "Data Source=mars;Initial Catalog=WebClient;Integrated Security=False;User ID=sa;Password=411;";
        => "Data Source=10.0.0.120; Initial Catalog=WebClient;Integrated Security=False;User ID=WebMars;Password=webmars111;";
#endif

        public static void Initialize()
        {
            using (var db = new FWPdb())
            {
                try
                {
                    db.Initialize();
                }
                catch
                {
                }
            }
        }


        /// <summary>
        ///     Проверяет актуальность сессии по ее идентификатору
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public static bool SessionValid(Guid id)
        {
            using (var db = new FWPdb())
            {
                return db.Sessions
                    .FirstOrDefault(v => v.Value == id)?
                    .User != null;
            }
        }


        public class MyOrg
        {
            public static long OGRN => 1026604938370;
            public static long INN => 6660040152;
        }

        #region SYSTEM

        public static R TransactRet<R>(Func<FWPdb, R> act)
        {
            return Ret(db =>
            {
                using (var transact = db.Database.BeginTransaction())
                {
                    var result = default(R);
                    try
                    {
                        result = act.Invoke(db);
                        transact.Commit();
                    }
                    catch (Exception error)
                    {
                        fxsLog.Write(fxeLog.Error, error, typeof (GDB));
                        transact.Rollback();
                    }
                    return result;
                }
            });
        }

        public static void Transact(Action<FWPdb> act)
        {
            Act(db =>
            {
                using (var transact = db.Database.BeginTransaction())
                {
                    try
                    {
                        act?.Invoke(db);
                        transact.Commit();
                    }
                    catch (Exception error)
                    {
                        fxsLog.Write(fxeLog.Error, error, typeof (GDB));
                        transact.Rollback();
                    }
                }
            });
        }

        public static void Act(Action<FWPdb> act)
        {
            using (var db = new FWPdb())
            {
                act?.Invoke(db);
            }
        }

        public static R Ret<R>(Func<FWPdb, R> act)
        {
            using (var db = new FWPdb())
            {
                return act.Invoke(db);
            }
        }

        public static Task<R> RetAsync<R>(Func<FWPdb, R> act)
        {
            return Task.Run(() => Ret(act));
        }

        #endregion

        public static Authentificate CreateAuthentificate(
            string login,
            string password,
            string address,
            string name,
            Guid session_guid)
        {
            var client = fxcConnection.Connect(ClientCS,
                connection => connection
                    .ExecuteReader(
                        reader => reader.ReadValues(),
                        fxr.GetResource<GDB>("SelectClient.sql").ToString(Encoding.UTF8)
                    )
                    .Where(v =>
                    {
                        var str = v[1].AsType<string>();
                        if (str == null) return false;
                        return str.Equals(login, StringComparison.InvariantCultureIgnoreCase);
                    })
                    .Select(v => v[0].AsType<int>())
                    .FirstOrDefault());

            if (client != 0)
            {
                return new Authentificate
                {
                    Guid = Guid.NewGuid(),
                    Roles = new[] {"client"},
                    Login = login
                };
            }

            return Ret(db =>
            {
                var session = db.Sessions
                    .Find(session_guid)
                              ?? db.Sessions.Add(new tSession
                              {
                                  HostAddress = address,
                                  HostName = name,
                                  Value = session_guid
                              });
                var user = db.Users
                    .WhereLogin(login, password)
                    .FirstOrDefault();

                if (session != null && user != null)
                {
                    user.Sessions.Add(session);
                    db.SaveChanges();
                }

                var roles = user?
                    .Roles
                    .Select(v => v.Value)
                    .ToArray() ?? new string[0];

                return new Authentificate
                {
                    Guid = user?.sysGuid ?? Guid.NewGuid(),
                    Login = user?.Login,
                    Roles = roles
                };
            });
        }

        public static tUser LoadUser(Guid arg)
            => Ret(db => db.Users.Find(arg));
    }
}