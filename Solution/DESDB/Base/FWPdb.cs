﻿#region

using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using DESDB.Tables;
using Finflaex.MsSQL.Bases;
using Finflaex.Reflection;

#endregion

namespace DESDB.Base
{
    public class FWPdb :
        //fxaDB<FWPdb>
        DbContext
    {
        public FWPdb()
//#if DEBUG
            //: this("Data Source=mars;Initial Catalog=DeveloperWebDB;Integrated Security=False;User ID=sa;Password=411;")
//#else
            //: this("Data Source=mars;Initial Catalog=CorporateWebDB;Integrated Security=False;User ID=sa;Password=411;")
            :this("Data Source=10.0.0.120; Initial Catalog=CorporateWebDB;Integrated Security=False;User ID=WebMars;Password=webmars111;")
//#endif
        {
        }

        public FWPdb(string connectionString)
            : base(connectionString)
        {
        }

        public DbSet<tUser> Users { get; set; }
        public DbSet<tRole> Roles { get; set; }
        public DbSet<tSession> Sessions { get; set; }
        public DbSet<tFile> Files { get; set; }
        public DbSet<tDescription> Descriptions { get; set; }
        public DbSet<tJsonData> Jsons { get; set; }
        public DbSet<tDirectory> Directories { get; set; }

        public void Initialize()
        {
            var role_architector = Roles
                .FirstOrDefault(v=>v.Value == "architector")
                .IfNull(r =>
                {
                    var result = Roles.Add(new tRole {Value = "architector"});
                    SaveChanges();
                    return result;
                });

            var role_developer = Roles
                .FirstOrDefault(v => v.Value == "developer")
                .IfNull(r =>
                {
                    var result = Roles.Add(new tRole {Value = "developer"});
                    SaveChanges();
                    return result;
                });

            var role_sdl = Roles
                .FirstOrDefault(v => v.Value == "sdl")
                .IfNull(r =>
                {
                    var result = Roles.Add(new tRole {Value = "sdl"});
                    SaveChanges();
                    return result;
                });

            var system = Users
                .Find(Guid.Empty)
                .IfNull(u =>
                {
                    var sys = new tUser
                    {
                        sysGuid = Guid.Empty,
                        Login = "system",
                        Password = Guid.NewGuid().ToString("N")
                    };
                    sys.UserChage = sys;
                    Users.Add(sys);
                    SaveChanges();
                    return sys;
                });

            Users
                .WhereLogin("architector")
                .FirstOrDefault()
                .IfNull(u =>
                {
                    var user = new tUser
                    {
                        Login = "architector",
                        Password = "carmageddon",
                        UserChage = system
                    };
                    user.Roles.Add(role_architector);
                    user.Roles.Add(role_developer);
                    Users.Add(user);
                    SaveChanges();
                });

            Users
                .WhereLogin("developer")
                .FirstOrDefault()
                .IfNull(u =>
                {
                    var user = new tUser
                    {
                        Login = "developer",
                        Password = "superuser",
                        UserChage = system
                    };
                    user.Roles.Add(role_developer);
                    Users.Add(user);
                    SaveChanges();
                });

            Users
                .WhereLogin("test_sdl")
                .FirstOrDefault()
                .IfNull(u =>
                {
                    var user = new tUser
                    {
                        Login = "test_sdl",
                        Password = "password",
                        UserChage = system
                    };
                    user.Roles.Add(role_sdl);
                    Users.Add(user);
                    SaveChanges();
                });
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

#region USERS

            modelBuilder.Entity<tUser>().HasMany(v => v.Roles).WithMany(v => v.Users);
            modelBuilder.Entity<tUser>().HasMany(v => v.Sessions).WithOptional(v => v.User).WillCascadeOnDelete(true);

#endregion

#region FILE_CONFIG

            modelBuilder
                .Entity<tDirectory>()
                .HasOptional(v => v.Root)
                .WithMany(v => v.Directories)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<tDirectory>()
                .HasOptional(v => v.Description)
                .WithOptionalDependent(v => v.Directory)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<tDirectory>()
                .HasOptional(v => v.Json)
                .WithOptionalDependent(v => v.Directory)
                .WillCascadeOnDelete(true);

            modelBuilder
                .Entity<tDirectory>()
                .HasMany(v => v.Files)
                .WithMany(v => v.Directories);
            modelBuilder.Entity<tFile>()
                .HasOptional(v => v.Description)
                .WithOptionalDependent(v => v.File)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<tFile>()
                .HasOptional(v => v.Json)
                .WithOptionalDependent(v => v.File)
                .WillCascadeOnDelete(true);

#endregion

#region CHANGE

            modelBuilder.Entity<tUser>().HasOptional(v => v.sysNext).WithOptionalDependent(v => v.sysPrev);
            modelBuilder.Entity<tDescription>().HasOptional(v => v.sysNext).WithOptionalDependent(v => v.sysPrev);
            modelBuilder.Entity<tJsonData>().HasOptional(v => v.sysNext).WithOptionalDependent(v => v.sysPrev);
            modelBuilder.Entity<tFile>().HasOptional(v => v.sysNext).WithOptionalDependent(v => v.sysPrev);
            modelBuilder.Entity<tDirectory>().HasOptional(v => v.sysNext).WithOptionalDependent(v => v.sysPrev);

#endregion

#region USER_CHANGE

            modelBuilder.Entity<tUser>()
                .HasMany(v => v.EditUsers)
                .WithRequired(v => v.UserChage)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<tUser>()
                .HasMany(v => v.EdiDescriptions)
                .WithRequired(v => v.UserChange)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<tUser>()
                .HasMany(v => v.EditDatas)
                .WithRequired(v => v.UserChange)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<tUser>()
                .HasMany(v => v.EditFiles)
                .WithRequired(v => v.UserChange)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<tUser>()
                .HasMany(v=> v.EditDirectories)
                .WithRequired(v => v.UserChange)
                .WillCascadeOnDelete(false);

#endregion
        }
    }
}