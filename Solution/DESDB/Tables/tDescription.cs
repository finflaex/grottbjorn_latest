﻿#region

using System;
using Finflaex.MsSQL.Tables;

#endregion

namespace DESDB.Tables
{
    public class tDescription : fxaDescription<tDescription>
    {
        #region SHARP

        public tDescription()
        {
        }

        #endregion

        #region LINKS

        public virtual tUser UserChange { get; set; }
        public virtual tDirectory Directory { get; set; }
        public virtual tFile File { get; set; }

        #endregion
    }
}