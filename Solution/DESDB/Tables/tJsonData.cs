﻿#region

using System;
using Finflaex.MsSQL.Tables;
using Finflaex.Reflection;

#endregion

namespace DESDB.Tables
{
    public class tJsonData : fxaJsonData<tJsonData>
    {
        #region SHARP

        public tJsonData()
        {
            
        }

        public tJsonData(tUser user, object data = null)
            : this()
        {
            UserChange = user;
            Value = data?.ToJson();
        }

        #endregion

        #region LINKS

        public virtual tDirectory Directory { get; set; }
        public virtual tFile File { get; set; }
        public virtual tUser UserChange { get; set; }

        #endregion
    }
}