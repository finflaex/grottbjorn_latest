﻿#region

using System;
using System.Linq;
using Finflaex.MsSQL.Tables;

#endregion

namespace DESDB.Tables
{
    public class tSession : fxaAutoGuid<tSession>
    {
        #region SHARP

        public tSession()
        {
            DateStart = DateTime.Now;
        }
        #endregion

        #region LINK

        public virtual tUser User { get; set; }

        #endregion

        #region DATA

        public DateTime? DateEnd { get; set; }
        public DateTime? DateStart { get; set; }
        public Guid Value { get; set; }
        public string HostAddress { get; set; }
        public string HostName { get; set; }

        #endregion
    }

    public static class rSession
    {
        public static tSession FindValue(
            this IQueryable<tSession> @this,
            Guid guid)
        {
            return @this.FirstOrDefault(v => v.Value.Equals(guid));
        }
    }
}