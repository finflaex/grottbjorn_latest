﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Finflaex.MsSQL.Tables;
using Finflaex.Reflection;

#endregion

namespace DESDB.Tables
{
    public class tUser : fxaAutoGuid<tUser>, fxiTableChange<tUser>
    {
        #region SHARP

        public tUser()
        {
            sysStartDT = DateTime.Now;
            sysEndDT = DateTime.MaxValue;
            sysActDT = DateTime.Now;
            sysStatus = fxeSysStatus.actual;

            Roles = new HashSet<tRole>();
            Sessions = new HashSet<tSession>();
            Files = new HashSet<tFile>();

            Login = string.Empty;
            Password = string.Empty;

            EdiDescriptions = new HashSet<tDescription>();
            EditUsers = new HashSet<tUser>();
            EditDatas = new HashSet<tJsonData>();
            EditFiles = new HashSet<tFile>();
            EditDirectories = new HashSet<tDirectory>();
        }

        #endregion

        #region DATAHIDE

        [Column("Password")]
        public string ___Password { get; set; }

        #endregion

        #region DATA

        [Change]
        public string Login { get; set; }

        [Change]
        [NotMapped]
        public string Password
        {
            get { return ___Password.Decrypt(); }
            set { ___Password = value.Encrypt(); }
        }

        #endregion

        #region LINKS

        public virtual ICollection<tRole> Roles { get; set; }
        public virtual ICollection<tSession> Sessions { get; set; }
        public virtual ICollection<tFile> Files { get; set; }
        [Change]
        public virtual tUser UserChage { get; set; }


        public virtual ICollection<tDescription> EdiDescriptions { get; set; }
        public virtual ICollection<tUser> EditUsers { get; set; }
        public virtual ICollection<tJsonData> EditDatas { get; set; }
        public virtual ICollection<tFile> EditFiles { get; set; }
        public virtual ICollection<tDirectory> EditDirectories { get; set; }

        #endregion

        #region SYSTEM

        public DateTime sysActDT { get; set; }

        public DateTime sysEndDT { get; set; }

        public virtual tUser sysNext { get; set; }

        public virtual tUser sysPrev { get; set; }

        public DateTime sysStartDT { get; set; }

        public fxeSysStatus sysStatus { get; set; }

        #endregion
    }

    public static class fxrUser
    {
        public static IQueryable<tUser> WhereLogin(this IQueryable<tUser> @this, string login)
        {
            return @this.Where(v => v.Login == login && v.sysStatus == fxeSysStatus.actual);
        }

        public static IQueryable<tUser> WhereLogin(this IQueryable<tUser> @this, string login, string password)
        {
            var pass = password.Encrypt();
            return @this.Where(v => v.Login == login && v.___Password == pass && v.sysStatus == fxeSysStatus.actual);
        }
    }
}