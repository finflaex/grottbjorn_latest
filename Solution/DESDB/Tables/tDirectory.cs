﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using Finflaex.MsSQL.Tables;
using Finflaex.Reflection;

#endregion

namespace DESDB.Tables
{
    public class tDirectory : fxaAutoGuid<tDirectory>,
        fxiTableChange<tDirectory>
    {
        #region SHARP

        public tDirectory()
        {
            sysStartDT = DateTime.Now;
            sysEndDT = DateTime.MaxValue;
            sysActDT = DateTime.Now;
            sysStatus = fxeSysStatus.actual;

            Directories = new HashSet<tDirectory>();
            Files = new HashSet<tFile>();
        }

        #endregion

        #region  FIELDS

        public string Name { get; set; }

        #endregion

        #region LINKS

        public virtual ICollection<tDirectory> Directories { get; set; }
        public virtual ICollection<tFile> Files { get; set; }
        public virtual tDirectory Root { get; set; }
        public virtual tDescription Description { get; set; }
        public virtual tJsonData Json { get; set; }

        #region SYSTEM

        public DateTime sysActDT { get; set; }
        public DateTime sysEndDT { get; set; }
        public DateTime sysStartDT { get; set; }
        public fxeSysStatus sysStatus { get; set; }
        public virtual tDirectory sysPrev { get; set; }
        public virtual tDirectory sysNext { get; set; }
        public virtual tUser UserChange { get; set; }

        #endregion

        #endregion
    }

    public static class rDirectory
    {
        public static tDirectory FindDirectory(
            this DbSet<tDirectory> query,
            string path)
        {
            var dir = path.GetDirectoryName();
            var paths = Directory.EnumerateDirectories(path).ToArray();
            if (!paths.Any()) return null;

            tDirectory directory;
            var index = 0;
            do
            {
                directory = query
                    .FirstOrDefault(v => v.Name.Equals(paths[index], StringComparison.CurrentCultureIgnoreCase));
                index++;
            } while (directory != null && paths.Length > index);
            return directory;
        }

        public static tDirectory FindRoot(
            this IQueryable<tDirectory> @this,
            string name)
        {
            return @this
                .FirstOrDefault(v
                    => v.sysStatus == fxeSysStatus.actual
                       && v.Root == null
                       && v.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
        }

        public static tDirectory FindName(
            this IEnumerable<tDirectory> @this,
            string name)
        {
            return @this
                .FirstOrDefault(v
                    => v.sysStatus == fxeSysStatus.actual
                       && v.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
        }
    }
}