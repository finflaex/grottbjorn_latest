﻿#region

using System.Collections.Generic;
using System.Linq;
using Finflaex.MsSQL.Tables;

#endregion

namespace DESDB.Tables
{
    public class tRole : fxaAutoGuid<tRole>
    {
        #region DATA

        public string Value { get; set; }

        #endregion

        #region LINKS

        public virtual ICollection<tUser> Users { get; set; }

        #endregion

        #region SHARP

        public tRole()
        {
            Users = new HashSet<tUser>();
        }

        public static implicit operator string(tRole value) => value.ToString();
        public override string ToString() => Value;

        #endregion
    }

    public static class rRole
    {
        //public static IQueryable<tRole> FindValues(
        //    this IQueryable<tRole> @this,
        //    params string[] roles)
        //{
        //    return @this
        //        .Where(v => roles.Contains(v.Value));
        //}

        //public static tRole FindValue(
        //    this IQueryable<tRole> @this,
        //    string role)
        //{
        //    return @this
        //        .FirstOrDefault(v => role.Equals(v.Value));
        //}
    }
}