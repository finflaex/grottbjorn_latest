﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Finflaex.MsSQL.Tables;

#endregion

namespace DESDB.Tables
{
    public class tFile : fxaAutoGuid<tFile>, fxiTableChange<tFile>
    {
        #region SHARP

        public tFile()
        {
            sysStartDT = DateTime.Now;
            sysEndDT = DateTime.MaxValue;
            sysActDT = DateTime.Now;
            sysStatus = fxeSysStatus.actual;

            Directories = new HashSet<tDirectory>();
        }

        #endregion

        #region DATA

        [Change]
        public string Name { get; set; }

        [Change]
        public string Ext { get; set; }

        [Change]
        public long Size { get; set; }

        [Change]
        public byte[] Content { get; set; }

        [Change]
        public string Directory { get; set; }

        #endregion

        #region SYSTEM

        public DateTime sysActDT { get; set; }
        public DateTime sysEndDT { get; set; }
        public DateTime sysStartDT { get; set; }
        public fxeSysStatus sysStatus { get; set; }
        public virtual tFile sysPrev { get; set; }
        public virtual tFile sysNext { get; set; }

        #endregion

        #region LINKS

        public virtual tDescription Description { get; set; }
        public virtual tJsonData Json { get; set; }

        [Change]
        public virtual tUser UserChange { get; set; }

        public virtual ICollection<tDirectory> Directories { get; set; }

        #endregion
    }

    public static class rFile
    {
        public static IQueryable<tFile> Actual(
            this IQueryable<tFile> @this)
        {
            return @this.Where(v => v.sysStatus == fxeSysStatus.actual);
        }

        public static IQueryable<tFile> WhereDirectory_actual(
            this IQueryable<tFile> @this,
            params string[] dirs)
        {
            return @this
                .Actual()
                .Where(v => dirs.Contains(v.Directory));
        }
    }
}