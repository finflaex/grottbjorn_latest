﻿#region

using System.ComponentModel.Composition;
using System.Threading.Tasks;
using System.Web.Mvc;
using Finflaex.fxMVC.WebSocket;

#endregion

namespace modAdminRole
{
    [Export(typeof (IController))]
    public class modAdminRole : WSController
    {
        public async Task<ActionResult> Index()
        {
            return await DoAsync(ws => { return new EmptyResult(); });
        }
    }
}