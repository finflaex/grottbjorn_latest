﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.fxMVC.Authorization;
using Finflaex.Plugin;
using Finflaex.Reflection;
using Finflaex.WS;

namespace modAdminRole
{
    [Export(typeof(fxiMvcModul))]
    public class cfgAdminRole : fxaMvcModul
    {
        #region INFO

        public override string Name => "AdminRole";

        public override string Version => "start";

        public override int Prioritet => 0;

        public override DateTime? DataCreate => new DateTime(2016, 08, 16);

        #endregion

        [Roles("admin")]
        public override IEnumerable<fxiModulMenuItem> Menu
            => new List<fxiModulMenuItem>()
            {
                new fxaModulMenuItem()
                {
                    Caption = "Админка",
                    Icon = "fa fa-sliders",
                    Submenu = new []
                    {
                        new fxaModulMenuItem()
                        {
                            Caption = "Роли",
                            Action = "WsLib.request("
                                     + fxsJsonResponse
                                         .LoadHtmlPost($"{nameof(modAdminRole)}/Index", "#page")
                                         .ToJson()
                                     + ")",
                        }
                    }
                }
            };
    }
}
