﻿#region

using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Web.Mvc;
using Finflaex.fxMVC.Authorization;
using Finflaex.MEF;
using Finflaex.Plugin;

#endregion

namespace FinflaexWEB.Pages.System.Config
{
    public class ConfigController : Controller
    {
        [Roles("admin")]
        public ActionResult ReloadMef()
        {
            this
                .LoadMef("this", Assembly.GetExecutingAssembly())
                .LoadMef("plugins", "~/plugins")
                .LoadMef("modules", "~/modules")
                .LoadMef("App_Data", "~/App_Data");
            return new EmptyResult();
        }
    }

    [Export(typeof(fxiMvcModul))]
    public class modCOnfig : fxaMvcModul
    {
        [Roles("admin")]
        public override IEnumerable<fxiModulMenuItem> Menu
            => new List<fxiModulMenuItem>()
            {
                new fxaModulMenuItem()
                {
                    Caption = "Админка",
                    Submenu = new []
                    {
                        new fxaModulMenuItem()
                        {
                            Caption = "Перегрузить плагины",
                            Action = "$.post('/config/reloadmef')",
                        }
                    }
                }
            };
    }
}