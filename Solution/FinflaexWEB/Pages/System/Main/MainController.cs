﻿#region

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Finflaex.fxMVC.Authorization;
using Finflaex.fxMVC.WebSocket;
using Finflaex.HtmlDom;
using Finflaex.MVC;
using Finflaex.Plugin;
using Finflaex.Reflection;
using Finflaex.WS;
using Microsoft.Ajax.Utilities;

#endregion

namespace FinflaexWEB.Pages.System.Main
{
    public class MainController : WSController
    {
        [Roles(true)]
        public ActionResult Index()
        {
            ViewBag.Title = "GrottBjorn";

            ViewBag.WsScript = this
                .GetResource("ws_start")
                .ToString(Encoding.UTF8)
                .ToJsMin();

            var auth = HttpContext.GetUser();
            if (auth.Login == null)
            {
                auth = HttpContext.SetUser();
            }
            Response.Cookies["session"].Value = auth.Guid.ToString();

            return View();
        }

        #region TOP_NAVIGATION_BAR

        [Roles(true)]
        public ActionResult TopNavigationBar() => View();

        [Roles(true)]
        public ActionResult TopNavigationBar_Left_All() => View();

        [Roles]
        public ActionResult TopNavigationBar_Left_Reg()
        {
            var model = new TopNavigationBar_Left_Reg_Model()
            {
            };
            return View(model);
        }

        [Roles(true, false)]
        public ActionResult TopNavigationBar_Left_UnReg() => View();

        [Roles(true)]
        public ActionResult TopNavigationBar_Right_All() => View();

        [Roles]
        public ActionResult TopNavigationBar_Right_Reg() => View();

        [Roles(true, false)]
        public ActionResult TopNavigationBar_Right_UnReg() => View();

        #endregion END TOP_NAVIGATION_BAR

        #region LEFT_PANEL

        [Roles(true)]
        public ActionResult LeftPanel_All()
        {
            var model = new LeftPanel_All_Model()
            {
                
            };
            return View(model);
        }

        [Roles]
        public ActionResult LeftPanel_Reg()
        {
            return View(new LeftPanel_Reg_Model()
            {
                Login = HttpContext.GetUser().Login,
                Menu = Moduls.Where(modul =>
                {
                    var props = modul.GetAttributesOfProperty("Menu");
                    var atrs = props.OfType<RolesAttribute>().ToArray();

                    return !atrs.Any() || atrs.Where(User.Verificate).Any();
                })
                    .SelectMany(modul => modul.Menu)
                    .GroupBy(v => v.Caption)
                    .Select(v => new fxaModulMenuItem()
                    {
                        Submenu = v.Where(s => s.Submenu != null).SelectMany(s => s.Submenu).ToArray(),
                        Caption = v.Key,
                        Icon = v.Select(s => s.Icon).WhereNotNull().Join(" "),
                        Action = v.Select(s => s.Action?.ToString()).WhereNotNull().Join(";")
                    })
                    .Cast<fxiModulMenuItem>()
                    .ToArray(),
            });
        }

        [Roles(true, false)]
        public ActionResult LeftPanel_UnReg()
        {
            var model = new LeftPanel_UnReg_Model()
            {
                
            };
            return View(model);
        }

        #endregion END LEFT_PANEL

        #region CONTENT

        [Roles]
        public ActionResult Content_Reg()
        {
            return View();
        }

        #endregion END CONTENT

        #region RIGHT_PANEL

        [Roles]
        public ActionResult RightPanel_Reg() => View(new object[1]);

        [Roles(true, false)]
        public ActionResult RightPanel_UnReg() => View(new object[1]);

        #endregion END RIGHT_PANEL

        #region DOM

        public ActionResult Head()
        {
            return Plugins
                .SelectMany(v => new[]
                {
                    v.HeadStyle.ToCssMin().StyleTagWrap(),
                    v.HeadScript.ToJsMin().ScriptTagWrap(),
                })
                .Join(string.Empty)
                .SelectSingle(Content);
        }

        public ActionResult HeadBody()
        {
            return Plugins
                .SelectMany(v => new[]
                {
                    v.BodyStyle.ToCssMin().StyleTagWrap()
                })
                .Join(string.Empty)
                .SelectSingle(Content);
        }

        public ActionResult FootBody()
        {
            return Plugins
                .SelectMany(v => new[]
                {
                    v.BodyScript.ToJsMin().ScriptTagWrap(),
                })
                .Join(string.Empty)
                .SelectSingle(Content);
        }

        #endregion
    }
}