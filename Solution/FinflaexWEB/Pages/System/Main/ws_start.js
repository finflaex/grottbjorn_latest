﻿var ws = $.connection("/ws");

WsLib.send = function (data) {
    if (data) {
        if (typeof data === "string" || data instanceof String)
            ws.send(data);
        else
            ws.send(JSON.stringify(data));
    }
};
ws.received(function (data) {
    WsLib.request(data);
});
ws.start().done(function () {
    WsLib.send({ c: "start", guid: lib.cookie_get("session") });
});

WsLib.response = function (v) {
    var result = {};
    if (v.c) {
        switch (v.c) {
            default:
                break;
        }
        if (v.b) {
            result.c = "back";
            result.b = v.b;
            return result;
        }
    }
    return null;
};