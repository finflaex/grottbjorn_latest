﻿using Finflaex.Plugin;

namespace FinflaexWEB.Pages.System.Main
{
    public class LeftPanel_Reg_Model
    {
        public LeftPanel_Reg_Model()
        {
        }

        public string Login { get; set; }

        public fxiModulMenuItem[] Menu { get; set; }
    }
}