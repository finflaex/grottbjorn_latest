﻿#region

using System.Web.Mvc;
using Finflaex.fxMVC.Authorization;

#endregion

namespace FinflaexWEB
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Clear();
            filters.Add(new RolesAttribute());
        }
    }
}