﻿using System.Web.Optimization;

namespace FinflaexWEB
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/start_script_header")
                .Include("~/Content/start/script/jquery-{version}.js")
                .Include("~/Content/start/script/jquery-ui.js")
                .Include("~/Content/start/script/bootstrap.js")
                .Include("~/Scripts/less-{version}.js")
                .Include("~/Scripts/jquery.signalR-{version}.js")
                .Include("~/Scripts/fxLib.js")

                .Include("~/Content/start/script/progressbar.js")
                .Include("~/Content/start/script/jquery.sparkline.js")
                .Include("~/Content/start/script/jquery.circliful.js")
                );
            bundles.Add(new ScriptBundle("~/start_script_footer")
                .Include("~/Content/start/script/jRespond.js")
                .Include("~/Content/start/script/modernizr.custom.js")
                .Include("~/Content/start/script/jquery.velocity.js")
                .Include("~/Content/start/script/jquery.slimscroll.js")
                .Include("~/Content/start/script/jquery.slimscroll.horizontal.js")
                .Include("~/Content/start/script/fastclick.js")
                .Include("~/Content/start/script/jquery.quicksearch.js")
                .Include("~/Content/start/script/bootbox.js")
                .Include("~/Content/start/script/jquery.flot.custom.js")
                .Include("~/Content/start/script/jquery.flot.resize.js")
                .Include("~/Content/start/script/jquery.flot.pie.js")
                .Include("~/Content/start/script/jquery.flot.time.js")
                .Include("~/Content/start/script/jquery.flot.growraf.js")
                .Include("~/Content/start/script/jquery.flot.categories.js")
                .Include("~/Content/start/script/jquery.flot.stack.js")
                .Include("~/Content/start/script/jquery.flot.orderBars.js")
                .Include("~/Content/start/script/jquery.flot.tooltip.js")
                .Include("~/Content/start/script/jquery.flot.curvedLines.js")
                .Include("~/Content/start/script/waypoints.js")
                .Include("~/Content/start/script/skyicons.js")
                .Include("~/Content/start/script/jquery.gritter.js")
                .Include("~/Content/start/script/jquery.countTo.js")
                .Include("~/Content/start/script/jquery.dynamic.js")
                .Include("~/Content/start/script/main.js")
                .Include("~/Content/start/script/dashboard.js")
                );

            bundles.Add(new StyleBundle("~/start_style")
                    .Include("~/Content/start/style/bootstrap.css")
                    .Include("~/Content/start/style/custom.css")
                    .Include("~/Content/start/style/icons.css")
                    .Include("~/Content/start/style/main.css")
                    .Include("~/Content/start/style/plugins.css")
                );

            bundles.Add(
                new ScriptBundle("~/scripts")
                    .Include("~/Scripts/jquery-{version}.js")
                    .Include("~/Scripts/jquery.signalR-{version}.js")
                    .Include("~/Scripts/bootstrap.js")
                    .Include("~/Scripts/less-{version}.js")
                    .Include("~/Scripts/pnotify.custom.js")
                    .Include("~/Scripts/fileinput.js")
                    .Include("~/Scripts/fileinput.ru.js")
                    .Include("~/Scripts/fxLib.js")
                );
            bundles.Add(
                new StyleBundle("~/styles")
                    .Include("~/Content/bootstrap.css")
                    .Include("~/Content/bootstrap-theme.css")
                    .Include("~/Content/pnotify.custom.css")
                    .Include("~/Content/bootstrap-fileinput/css/fileinput.css")
                );

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*",
            //            "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            //bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
            //            "~/Content/themes/base/jquery.ui.core.css",
            //            "~/Content/themes/base/jquery.ui.resizable.css",
            //            "~/Content/themes/base/jquery.ui.selectable.css",
            //            "~/Content/themes/base/jquery.ui.accordion.css",
            //            "~/Content/themes/base/jquery.ui.autocomplete.css",
            //            "~/Content/themes/base/jquery.ui.button.css",
            //            "~/Content/themes/base/jquery.ui.dialog.css",
            //            "~/Content/themes/base/jquery.ui.slider.css",
            //            "~/Content/themes/base/jquery.ui.tabs.css",
            //            "~/Content/themes/base/jquery.ui.datepicker.css",
            //            "~/Content/themes/base/jquery.ui.progressbar.css",
            //            "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}