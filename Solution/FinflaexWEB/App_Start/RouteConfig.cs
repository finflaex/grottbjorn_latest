﻿#region

using System.Web.Mvc;
using System.Web.Routing;

#endregion

namespace FinflaexWEB
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Result", "{controller}/{action}.{result}/{id}",
            new { controller = "Home", action = "Index", id = "", result = "view" });

            routes.MapRoute("Default", "{controller}/{action}/{id}", new
            {
                controller = "Main",
                action = "Index",
                id = UrlParameter.Optional
            });

            //RouteTable.Routes.MapRoute("Plugin", "ext/{action}/{*param}", new
            //{
            //    controller = "fxcDefaultController",
            //    action = "Index"
            //});
        }
    }
}