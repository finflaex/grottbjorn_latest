﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finflaex.fxMVC.Authorization;

namespace FinflaexWEB.Areas.Test.Controllers
{
    public class TestController : Controller
    {        
        [Roles(true)]
        public ActionResult Index()
        {
            var modules = HttpContext.ApplicationInstance.Modules;
            string[] modArray = modules.AllKeys;
            return View(modArray);
        }
    }
}