﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CsQuery;
using Finflaex.fxMVC.Authorization;
using Finflaex.Html;
using Finflaex.HtmlDom;
using Finflaex.Reflection;

namespace FinflaexWEB.Areas.News.Controllers
{
    public class NewsController : Controller
    {
        //// GET: News/News
        //public ActionResult Index()
        //{
        //    return View();
        //}

            [Roles(true)]
        //[HttpPost]
        public ActionResult GetNews()
            {
                return fxsHtml
                    .Get("http://grottbjorn.com/news/novosti/")
                    .SelectSingle(CQ.Create)[".content .news"]
                    .Select(item => new
                    {
                        date = item.Cq().Find(".date").Text(),
                        caption = item.Cq().Find(".name").Text(),
                        text = item.Cq().Find("p").Text(),
                        link = item.Cq().Find(".name").Attr<string>("href")
                    })
                    .Select(item =>
                    {
                        var result = new StringBuilder();
                        result.Append("<li><p>");
                        result.Append($"<a href='{item.link}'>{item.caption}</a>");
                        result.Append(item.text);
                        result.Append(
                            $"<span class='timeline-icon'><i class='fa fa-newspaper-o color-gray-light'></i></span>");
                        result.Append($"<span class='timeline-date'>{item.date}</span>");
                        result.Append("</p></li>");
                        return result.ToString();
                    })
                    .Join(string.Empty)
                    .Action(Content)
                    .AsType<ActionResult>();
            }
    }
}