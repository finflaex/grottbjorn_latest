﻿using System.Web.Mvc;

namespace FinflaexWEB.Areas.News
{
    public class NewsAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "News";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "News_default",
                "News/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}