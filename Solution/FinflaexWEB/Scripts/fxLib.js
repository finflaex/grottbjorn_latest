///**
// * Created by finflaex on 04.08.2015.
// */

var lib = {};

lib.guid = (function() {
    // Private array of chars to use
    var CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split("");

    return function (len, radix) {
        var chars = CHARS, uuid = [], rnd = Math.random;
        radix = radix || chars.length;

        if (len) {
            // Compact form
            for (var i = 0; i < len; i++) uuid[i] = chars[0 | rnd()*radix];
        } else {
            // rfc4122, version 4 form
            var r;

            // rfc4122 requires these characters
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = "-";
            uuid[14] = "4";

            // Fill in random data.  At i==19 set the high bits of clock sequence as
            // per rfc4122, sec. 4.1.5
            for (var i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    r = 0 | rnd()*16;
                    uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
                }
            }
        }

        return uuid.join("");
    };
})();

lib.cookie_get = function(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};
lib.cookie_set = function(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        if (options.hasOwnProperty(propName)) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }
    }

    document.cookie = updatedCookie;
};
lib.cookie_del = function(name) {
// ReSharper disable once PossiblyUnassignedProperty
    window.setCookie(name, "", {
        expires: -1
    });
}

lib.function = function (script) { return eval("(function(){" + script + "})"); }

lib.tagReplace = function(target, value)
{
    while (true) {
        var t = document.getElementsByTagName(target)[0];
        if (t === undefined) break;
        var n = document.createElement(value);
        n.innerHTML = t.innerHTML;
        t.parentNode.appendChild(n);
        t.parentNode.removeChild(t);
    }
}

lib.eval = function(code) {
    window.execScript ? execScript(code) : window.eval(code);
}

Base64 = {

    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",


    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },


    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}


    $(function(){
        $.fn.screenCenter = function() {
            this.css("position", "absolute");
            this.css("top", (this.parent().innerHeight() - this.outerHeight()) / 2 + this.parent().scrollTop() + "px");
            this.css("left", (this.parent().innerWidth() - this.outerWidth()) / 2 + this.parent().scrollLeft() + "px");
            return this;
        };
        $.fn.caret = function (begin, end) {
            // ���� ������ ����� - ������� �� �������
            if (this.length == 0) return;
            if (typeof begin == "number")
            {
                end = (typeof end == "number") ? end : begin;
                return this.each(function ()
                {
                    if (this.setSelectionRange)
                    {
                        this.setSelectionRange(begin, end);
                    } else if (this.createTextRange)
                    {
                        var range = this.createTextRange();
                        range.collapse(true);
                        range.moveEnd("character", end);
                        range.moveStart("character", begin);
                        try { range.select(); } catch (ex) { }
                    }
                });
            } else {
                if (this[0].setSelectionRange) {
                    begin = this[0].selectionStart;
                    end = this[0].selectionEnd;
                } else if (document.selection && document.selection.createRange) {
                    var range = document.selection.createRange();
                    begin = 0 - range.duplicate().moveStart("character", -100000);
                    end = begin + range.text.length;
                }
                //if (begin == end)
                return begin;
                //else
                //return {begin: begin, end: end};
            }
        };
        $.fn.Value = function(val) {
            var result;
            if(this.is("input")) {
                switch (this.attr("type")) {
                    case "time":
                    case "date":
                    case "datetime-local":
                    case "password":
                    case "text":
                    case "hidden":
                    case "number":
                        if (val != undefined)this.val(val);
                        result = this.val();
                        break;
                    case "checkbox":
                        if (val != undefined)this.prop("checked", val);
                        result = this.prop("checked");
                        break;
                }
            }
            else if (this.attr("contentEditable") == "true"){
                if (val != undefined)this.text(val);
                result = this.text();
            }
            else if (this.is("select")){
                if (val != undefined)this.val(val);
                result = this.val();
            }
            return result;
        };
        $.fn.innerWidth = function (val) {
            var o = this;
            if (val != undefined) {
                var value = parseInt(val);
                //value += parseInt(o.css("padding-left"));
                //value += parseInt(o.css("padding-right"));
                value += parseInt(o.css("margin-left"));
                value += parseInt(o.css("margin-right"));
                value += parseInt(o.css("border-left-width"));
                value += parseInt(o.css("border-right-width"));
                o.css("width", value.toString());
            }
            var result = parseInt(o.css("width"));
            //result -= parseInt(o.css("padding-left"));
            //result -= parseInt(o.css("padding-right"));
            result -= parseInt(o.css("margin-left"));
            result -= parseInt(o.css("margin-right"));
            result -= parseInt(o.css("border-left-width"));
            result -= parseInt(o.css("border-right-width"));
            return result;
        };
        $.fn.innerHeight = function (val) {
            var o = this;
            if (val != undefined) {
                var value = parseInt(val);
                //value += parseInt(o.css("padding-top"));
                //value += parseInt(o.css("padding-bottom"));
                value += parseInt(o.css("margin-top"));
                value += parseInt(o.css("margin-bottom"));
                value += parseInt(o.css("border-top-width"));
                value += parseInt(o.css("border-bottom-width"));
                o.css("height", value.toString());
            }
            var result = parseInt(o.css("height"));
            //result -= parseInt(o.css("padding-top"));
            //result -= parseInt(o.css("padding-bottom"));
            result -= parseInt(o.css("margin-top"));
            result -= parseInt(o.css("margin-bottom"));
            result -= parseInt(o.css("border-top-width"));
            result -= parseInt(o.css("border-bottom-width"));
            return result;
        };
        $.fn.outerWidth = function (val) {
            var o = this;
            if (val != undefined) {
                var value = parseInt(val);
                value -= parseInt(o.css("padding-left"));
                value -= parseInt(o.css("padding-right"));
                value -= parseInt(o.css("margin-left"));
                value -= parseInt(o.css("margin-right"));
                value -= parseInt(o.css("border-left-width"));
                value -= parseInt(o.css("border-right-width"));
                o.css("width", value.toString());
            }
            var result = parseInt(o.css("width"));
            result += parseInt(o.css("padding-left"));
            result += parseInt(o.css("padding-right"));
            result += parseInt(o.css("margin-left"));
            result += parseInt(o.css("margin-right"));
            result += parseInt(o.css("border-left-width"));
            result += parseInt(o.css("border-right-width"));
            return result;
        };
        $.fn.outerHeight = function (val) {
            var o = this;
            if (val != undefined) {
                var value = parseInt(val);
                value -= parseInt(o.css("padding-top"));
                value -= parseInt(o.css("padding-bottom"));
                value -= parseInt(o.css("margin-top"));
                value -= parseInt(o.css("margin-bottom"));
                value -= parseInt(o.css("border-top-width"));
                value -= parseInt(o.css("border-bottom-width"));
                o.css("height", value.toString());
            }
            var result = parseInt(o.css("height"));
            result += parseInt(o.css("padding-top"));
            result += parseInt(o.css("padding-bottom"));
            result += parseInt(o.css("margin-top"));
            result += parseInt(o.css("margin-bottom"));
            result += parseInt(o.css("border-top-width"));
            result += parseInt(o.css("border-bottom-width"));
            return result;
        };
        $.fn.slideRightShow= function() {
            return this.each(function() {
                jQuery(this).animate({width: "show"});
            });
        };
        $.fn.slideLeftHide= function() {
            return this.each(function() {
                jQuery(this).animate({width: "hide"});
            });
        };
    });

    (function ($) {
        /// JSON plugin
        "use strict";

        var escape = /["\\\x00-\x1f\x7f-\x9f]/g,
            meta = {
                '\b': "\\b",
                '\t': "\\t",
                '\n': "\\n",
                '\f': "\\f",
                '\r': "\\r",
                '"': '\\"',
                '\\': "\\\\"
            },
            hasOwn = Object.prototype.hasOwnProperty;

        /**
         *
         *
         */
        $.toJSON = typeof JSON === "object" && JSON.stringify ? JSON.stringify : function (o) {
            if (o === null) {
                return "null";
            }

            var pairs, k, name, val,
                type = $.type(o);

            if (type === "undefined") {
                return undefined;
            }

            // Also covers instantiated Number and Boolean objects,
            // which are typeof 'object' but thanks to $.type, we
            // catch them here. I don't know whether it is right
            // or wrong that instantiated primitives are not
            // exported to JSON as an {"object":..}.
            // We choose this path because that's what the browsers did.
            if (type === "number" || type === "boolean") {
                return String(o);
            }
            if (type === "string") {
                return $.quoteString(o);
            }
            if (typeof o.toJSON === "function") {
                return $.toJSON(o.toJSON());
            }
            if (type === "date") {
                var month = o.getUTCMonth() + 1,
                    day = o.getUTCDate(),
                    year = o.getUTCFullYear(),
                    hours = o.getUTCHours(),
                    minutes = o.getUTCMinutes(),
                    seconds = o.getUTCSeconds(),
                    milli = o.getUTCMilliseconds();

                if (month < 10) {
                    month = "0" + month;
                }
                if (day < 10) {
                    day = "0" + day;
                }
                if (hours < 10) {
                    hours = "0" + hours;
                }
                if (minutes < 10) {
                    minutes = "0" + minutes;
                }
                if (seconds < 10) {
                    seconds = "0" + seconds;
                }
                if (milli < 100) {
                    milli = "0" + milli;
                }
                if (milli < 10) {
                    milli = "0" + milli;
                }
                return '"' + year + "-" + month + "-" + day + "T" +
                    hours + ":" + minutes + ":" + seconds +
                    "." + milli + 'Z"';
            }

            pairs = [];

            if ($.isArray(o)) {
                for (k = 0; k < o.length; k++) {
                    pairs.push($.toJSON(o[k]) || "null");
                }
                return "[" + pairs.join(",") + "]";
            }

            // Any other object (plain object, RegExp, ..)
            // Need to do typeof instead of $.type, because we also
            // want to catch non-plain objects.
            if (typeof o === "object") {
                for (k in o) {
                    // Only include own properties,
                    // Filter out inherited prototypes
                    if (hasOwn.call(o, k)) {
                        // Keys must be numerical or string. Skip others
                        type = typeof k;
                        if (type === "number") {
                            name = '"' + k + '"';
                        } else if (type === "string") {
                            name = $.quoteString(k);
                        } else {
                            continue;
                        }
                        type = typeof o[k];

                        // Invalid values like these return undefined
                        // from toJSON, however those object members
                        // shouldn't be included in the JSON string at all.
                        if (type !== "function" && type !== "undefined") {
                            val = $.toJSON(o[k]);
                            pairs.push(name + ":" + val);
                        }
                    }
                }
                return "{" + pairs.join(",") + "}";
            }
        };

        /**
         * jQuery.evalJSON
         * Evaluates a given json string.
         *
         * @param str {String}
         */
        $.evalJSON = typeof JSON === "object" && JSON.parse ? JSON.parse : function (str) {
            /*jshint evil: true */
            return eval("(" + str + ")");
        };

        /**
         * jQuery.secureEvalJSON
         * Evals JSON in a way that is *more* secure.
         *
         * @param str {String}
         */
        $.secureEvalJSON = typeof JSON === "object" && JSON.parse ? JSON.parse : function (str) {
            var filtered =
                str
                    .replace(/\\["\\\/bfnrtu]/g, "@")
                    .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]")
                    .replace(/(?:^|:|,)(?:\s*\[)+/g, "");

            if (/^[\],:{}\s]*$/.test(filtered)) {
                /*jshint evil: true */
                return eval("(" + str + ")");
            }
            throw new SyntaxError("Error parsing JSON, source is not valid.");
        };

        /**
         * jQuery.quoteString
         * Returns a string-repr of a string, escaping quotes intelligently.
         * Mostly a support function for toJSON.
         * Examples:
         * >>> jQuery.quoteString('apple')
         * "apple"
         *
         * >>> jQuery.quoteString('"Where are we going?", she asked.')
         * "\"Where are we going?\", she asked."
         */
        $.quoteString = function (str) {
            if (str.match(escape)) {
                return '"' + str.replace(escape, function (a) {
                    var c = meta[a];
                    if (typeof c === "string") {
                        return c;
                    }
                    c = a.charCodeAt();
                    return "\\u00" + Math.floor(c / 16).toString(16) + (c % 16).toString(16);
                }) + '"';
            }
            return '"' + str + '"';
        };

    }(jQuery));

    Math.uuid = (function() {
        // Private array of chars to use
        var CHARS = "0123456789abcdefghijklmnopqrstuvwxyz".split("");

        return function (len, radix) {
            var chars = CHARS, uuid = [];
            radix = radix || chars.length;

            if (len) {
                // Compact form
                for (var i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
            } else {
                // rfc4122, version 4 form
                var r;

                // rfc4122 requires these characters
                uuid[8] = uuid[13] = uuid[18] = uuid[23] = "-";
                uuid[14] = "4";

                // Fill in random data.  At i==19 set the high bits of clock sequence as
                // per rfc4122, sec. 4.1.5
                for (var i = 0; i < 36; i++) {
                    if (!uuid[i]) {
                        r = 0 | Math.random()*16;
                        uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
                    }
                }
            }

            return uuid.join("");
        };
    })();

    // A more compact, but less performant, RFC4122v4 compliant solution:
    Math.uuid2 = function() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == "x" ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    };

    function JSONConverter(val) {
        if (val === null) {
            return null;
        } else if (typeof val === "object") {
            if (val.Base64) {
                var read = Base64.decode(val.data);
                switch (val.Base64) {
                    case "string":
                        return read;
                    case "script":
                        return eval(read);
                    case "function":
                        return lib.function(read);
                }
            } else {
                for (var prop in val)
                    if (val.hasOwnProperty(prop))
                        val[prop] = JSONConverter(val[prop]);
            }
        }
        else if (Array.isArray(val))
            // ReSharper disable once QualifiedExpressionMaybeNull
            for (var i = 0; i < val.length; i++)
                val[i] = JSONConverter(val[i]);
        return val;
    }


    $.preloadImages = function () {
        if (typeof arguments[arguments.length - 1] == "function") {
            var callback = arguments[arguments.length - 1];
        } else {
            var callback = false;
        }
 
        if (typeof arguments[0] == "object") {
            var images = arguments[0];
            var n = images.length;
        } else {
            var images = arguments;
            var n = images.length - 1;
        }
        var not_loaded = n;
        for (var i = 0; i < n; i++) {
            jQuery(new Image()).attr("src", images[i]).load(function() {
                if (--not_loaded < 1 && typeof callback == "function") {
                    callback();
                }
            });
        }
    }



var WsLib = {
    
}
WsLib.request = function (data) {
    if (typeof data === "string" || data instanceof String) {
        data = JSON.parse(data);
    }
    var v = JSONConverter(data);
    if (Array.isArray(v)) {
        v.forEach(function (item, i, arr) {
            var read = WsLib.parse(item);
            if (this.send) this.send(read);
        });
    } else {
        var read = this.parse(v);
        if (this.send) this.send(read);
    }
}
WsLib.response = null;
WsLib.parse = function(v) {
    var result = {}
    if (v.c) {
        switch (v.c) {
        case "eval":
            lib.eval(Base64.decode(v.d));
            break;
        case "send":
            return v.d;
        case "ping":
            return { c: "pong" };
        case "lhtml":
            $.ajax({
                url: v.u,
                dataType: "html",
                beforeSend: function() {
                    $(v.t).html('<div class="loader center" style="width:30px;height:30px"></div>');
                },
                success: function(data) {
                    $(v.t).html(data);
                },
                error: function() {
                    $(v.t).html("error");
                }
            });
            break;
        case "html":
            $(v.t).html(Base64.decode(v.d));
            break;
        case "append":
            $(v.t).append(Base64.decode(v.d));
            break;
        case "prepend":
            $(v.t).prepend(Base64.decode(v.d));
            break;
        case "scook":
            lib.cookie_set(v.n, v.v);
            break;
        case "gcook":
            result.r = lib.cookie_get(v.n);
            break;
        case "jquery":
            var target = $(v.t);
            target[v.m].apply(target, v.p);
            break;
        case "notify":
            //new PNotify({ text: v.m, type: v.t });
            $.gritter.add({
                title: "Warrning !!!",
                text: v.m,
                time: "",
                close_icon: "l-arrows-remove s16",
                icon: "glyphicon glyphicon-user",
                class_name: "error-notice"
            });
            break;
        case "redirect":
            window.location.replace(v.u);
            break;
        default:
            if (this.response)
                return this.response(v);
            break;
        }
        if (v.b) {
            result.c = "back";
            result.b = v.b;
            return result;
        }
    }
    if (this.response)
        return this.response(v);
    else
        return null;
}
WsLib.send = null;

/// onDomChange(function(){ 
///     alert("The Times They Are a-Changin'");
/// });
(function (window) {
    var last = +new Date();
    var delay = 100; // default delay

    // Manage event queue
    var stack = [];

    function callback() {
        var now = +new Date();
        if (now - last > delay) {
            for (var i = 0; i < stack.length; i++) {
                stack[i]();
            }
            last = now;
        }
    }

    // Public interface
    var onDomChange = function (fn, newdelay) {
        if (newdelay) delay = newdelay;
        stack.push(fn);
    };

    // Naive approach for compatibility
    function naive() {

        var last = document.getElementsByTagName("*");
        var lastlen = last.length;
        var timer = setTimeout(function check() {

            // get current state of the document
            var current = document.getElementsByTagName("*");
            var len = current.length;

            // if the length is different
            // it's fairly obvious
            if (len != lastlen) {
                // just make sure the loop finishes early
                last = [];
            }

            // go check every element in order
            for (var i = 0; i < len; i++) {
                if (current[i] !== last[i]) {
                    callback();
                    last = current;
                    lastlen = len;
                    break;
                }
            }

            // over, and over, and over again
            setTimeout(check, delay);

        }, delay);
    }

    //
    //  Check for mutation events support
    //

    var support = {};

    var el = document.documentElement;
    var remain = 3;

    // callback for the tests
    function decide() {
        if (support.DOMNodeInserted) {
            window.addEventListener("DOMContentLoaded", function () {
                if (support.DOMSubtreeModified) { // for FF 3+, Chrome
                    el.addEventListener("DOMSubtreeModified", callback, false);
                } else { // for FF 2, Safari, Opera 9.6+
                    el.addEventListener("DOMNodeInserted", callback, false);
                    el.addEventListener("DOMNodeRemoved", callback, false);
                }
            }, false);
        } else if (document.onpropertychange) { // for IE 5.5+
            document.onpropertychange = callback;
        } else { // fallback
            naive();
        }
    }

    // checks a particular event
    function test(event) {
        el.addEventListener(event, function fn() {
            support[event] = true;
            el.removeEventListener(event, fn, false);
            if (--remain === 0) decide();
        }, false);
    }

    // attach test events
    if (window.addEventListener) {
        test("DOMSubtreeModified");
        test("DOMNodeInserted");
        test("DOMNodeRemoved");
    } else {
        decide();
    }

    // do the dummy test
    var dummy = document.createElement("div");
    el.appendChild(dummy);
    el.removeChild(dummy);

    // expose
    window.onDomChange = onDomChange;
})(window);

//onDomChange(function () {
//    var marq = document.getElementsByTagName('file')[0];
//    var div = document.createElement('div');
//    div.innerHTML = marq.innerHTML;
//    marq.parentNode.appendChild(div);
//    marq.parentNode.removeChild(marq);
//});

(function ($, window) {
    //<ul id="contextMenu" class="dropdown-menu" role="menu" style="display:none">
    //    <li><a tabindex="-1" href="#">Action</a></li>
    //    <li><a tabindex="-1" href="#">Another action</a></li>
    //    <li><a tabindex="-1" href="#">Something else here</a></li>
    //    <li class="divider"></li>
    //    <li><a tabindex="-1" href="#">Separated link</a></li>
    //</ul>
    //<script>
    //    $("#cik_cb_table").contextMenu({
    //        menuSelector: "#contextMenu",
    //        menuSelected: function(invokedOn, selectedMenu) {
    //            var msg = "You selected the menu item '" + selectedMenu.text() +
    //                "' on the value '" + invokedOn.text() + "'";
    //            alert(msg);
    //        }
    //    });
    //</script>


    $.fn.contextMenu = function (settings) {

        return this.each(function () {

            // Open context menu
            $(this).on("contextmenu", function (e) {
                // return native menu if pressing control
                if (e.ctrlKey) return;

                //open menu
                var $menu = $(settings.menuSelector)
                    .data("invokedOn", $(e.target))
                    .show()
                    .css({
                        position: "fixed",
                        left: getMenuPosition(e.clientX, "width", "scrollLeft"),
                        top: getMenuPosition(e.clientY, "height", "scrollTop")
                    })
                    .off("click")
                    .on("click", "a", function (e) {
                        $menu.hide();

                        var $invokedOn = $menu.data("invokedOn");
                        var $selectedMenu = $(e.target);

                        settings.menuSelected.call(this, $invokedOn, $selectedMenu);
                    });

                return false;
            });

            //make sure menu closes on any click
            $("body").click(function () {
                $(settings.menuSelector).hide();
            });
        });

        function getMenuPosition(mouse, direction, scrollDir) {
            var win = $(window)[direction](),
                scroll = $(window)[scrollDir](),
                menu = $(settings.menuSelector)[direction](),
                position = mouse + scroll;

            // opening menu would pass the side of the page
            if (mouse + menu > win && menu < mouse)
                position -= menu;

            return position;
        }

    };
})(jQuery, window);