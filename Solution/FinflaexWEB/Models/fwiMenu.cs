﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Finflaex.HtmlDom;
using Finflaex.Reflection;

#endregion

namespace FinflaexWEB.Models
{
    public interface fwiMenu
    {
        string MefMenu();
    }

    public static class fwrMenu
    {
        public static fwcMenuGroup CreateMenu(
            this fwiMenu @this, 
            string caption)
        {
            return new fwcMenuGroup(caption);
        }
    }

    public class fwcMenuGroup
    {
        private readonly string caption;
        private readonly string id;
        private readonly List<object> Items = new List<object>();
        public fwcMenuGroup(string caption)
        {
            this.caption = caption;
            this.id = Guid.NewGuid().ToString("N");
        }

        public static implicit operator string(fwcMenuGroup group) => group.ToString();

        public fwcMenuGroup AddItem(string caption, string script)
        {
            caption
                .ToHtmlTag(fxeHtmlTag.a)
                .OnClick(script)
                .IfNotNull(Items.Add);
            return this;
        }

        public override string ToString()
        {
            var div = new fxcHtmlTag(fxeHtmlTag.div)
                .AddContent(Items.ToArray())
                .AddClass("collapse");

            var a = caption
                .ToHtmlTag(fxeHtmlTag.a)
                .AddAttribute("href", $"#{div.Id}")
                .AddAttribute("data-toggle", "collapse");

            return $"{a}{div}";
        }
    }
}