﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Finflaex.MEF;
using Finflaex.MVC;
using Finflaex.Reflection;
using Finflaex.Schedule;
using Finflaex.Self;
using Quartz;
using Quartz.Impl;

//using DESDB;

#endregion

namespace FinflaexWEB
{
    public class Global : HttpApplication
    {
        private void Application_Start(object sender, EventArgs e)
        {
            // Код, выполняемый при запуске приложения
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);

            // Bundle
#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // factory
            //fxrHttpContext.CreateEmptyAuthentificate += GDB.CreateAuthentificate;

#region MEF

            this
                .LoadMef("this", Assembly.GetExecutingAssembly())
                .LoadMef("plugins", "~/plugins")
                .LoadMef("modules", "~/modules")
                .LoadMef("App_Data", "~/App_Data")
                .UpdateMef();

            fxcControllerFactory.Create += factory => factory.UpdateMef();

            new fxaSchedule().ScheduleStart();

#endregion

            GlobalFilters.Filters.Clear();
            fxsMain.MvcAsaxInitialize();

            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}