﻿#region

//using DESDB;
using DESDB;
using Finflaex.Log;
using FinflaexWEB;
using FinflaexWEB.Pages.System.WebSocket;
using Microsoft.Owin;
using Owin;

#endregion

[assembly: OwinStartup(typeof (StartupOWIN))]

namespace FinflaexWEB
{
    public class StartupOWIN
    {
        public void Configuration(IAppBuilder app)
        {
            GDB.Initialize();

            app.MapSignalR();
            app.MapSignalR<SignalR>("/ws");
            fxsLog.Write(fxeLog.Debug, "Сервер запущен");
        }
    }
}