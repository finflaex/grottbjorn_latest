﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Finflaex.Certificate;
using Finflaex.fxMVC.Authorization;
using Finflaex.fxMVC.WebSocket;
using Finflaex.Reflection;
using Finflaex.Types;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

#endregion

namespace modESIA
{
    [Export(typeof (IController))]
    public class modESIA : WSController
    {
        private static readonly string client_id = "GROTTBJORN"; //Мнемоника системы, его можно узнать у ЕСИА
        private static readonly string state = Guid.NewGuid().ToString("D"); //гуид для всяких проверок
        private static readonly string server_url = "https://esia-portal1.test.gosuslugi.ru/aas/oauth2/ac";
        private static readonly string server_url_2 = "https://esia-portal1.test.gosuslugi.ru/aas/oauth2/te";
        private static readonly string server_url_prns = "https://esia-portal1.test.gosuslugi.ru/rs/prns/";

        //private static readonly string server_url = "https://esia.gosuslugi.ru/aas/oauth2/ac";
        //    //адресс по которому есиа вернет авторизационный код

        //private static readonly string server_url_2 = "https://esia.gosuslugi.ru/aas/oauth2/te";
        //    //адресс по которому получим маркер

        //private static readonly string server_url_prns = "https://esia.gosuslugi.ru/rs/prns/";
            //тут мы узнаем ФИО пользователя который к нам хочет залогини

        [Roles(true, false)]
        public ActionResult ESIA()
        {
            //string scope = "openid";
            var scope = "fullname id_doc";
            //string scope = "usr_inf";
            var timestamp = DateTime.UtcNow.ToString("yyyy.MM.dd HH:mm:ss +0000");
            var access_type = "online";
            var response_type = "code";

            var redirect_uri = Request.Url.Scheme
                .Append("://")
                .Append(Request.Url.Host)
                .AppendWhere(Request.Url.Port == 80, Request.Url.Port)
                .Append("/modEsia/ESIA_OK");

            var cert = fxsCertificate.FindBySerial("62 13 b1 a2 dc 3f 39 a9 4c 7e 0b 79 c7 f5 4e e3");
            var algoritm = new Oid("2.16.840.1.101.3.4.2.1", "sha256");
            var client_secret = (scope + timestamp + client_id + state)
                .ToBytes(Encoding.UTF8)
                .CertSign(cert, algoritm)
                .UrlTokenEncode();

            return new Dictionary<string, string>
            {
                ["client_id"] = client_id,
                ["client_secret"] = client_secret,
                ["redirect_uri"] = redirect_uri,
                ["scope"] = scope,
                ["response_type"] = response_type,
                ["state"] = state,
                ["timestamp"] = timestamp,
                ["access_type"] = access_type
            }
                .Select(v => $"{v.Key}={v.Value}")
                .Join("&")
                .Replace("+", "%2b")
                .SelectSingle(param => Redirect($"{server_url}?{param}"));
        }

        [Roles(true, false)]
        public ActionResult ESIA_OK()
        {
            var state_r = Request.QueryString["state"];
            var code = Request.QueryString["code"];
            var error_description = Request.QueryString["error_description"];

            if (state == state_r)
            {
                if (error_description != null)
                {
                    return Content(error_description.UrlDecode());
                }

                var scope = "fullname id_doc";
                var timestamp = DateTime.UtcNow.ToString("yyyy.MM.dd HH:mm:ss +0000");
                var redirect_uri = Request.Url.Scheme
                    .Append("://")
                    .Append(Request.Url.Host)
                    .AppendWhere(Request.Url.Port == 80, ":" + Request.Url.Port)
                    .Append("/modESIA/ESIA_OK");
                var msg = scope + timestamp + client_id + state;
                var msgBytes = Encoding.UTF8.GetBytes(msg);
                var encodedSignature = Cert.Sign(msgBytes);
                var client_secret = HttpServerUtility.UrlTokenEncode(encodedSignature);

                //генерим post запрос для получения маркера
                {
                    var marker = server_url_2
                        .SelectSingle(WebRequest.Create).AsType<HttpWebRequest>()
                        .Action(request =>
                        {
                            request.ContentType = "application/x-www-form-urlencoded";
                            request.Method = "POST";
                            request.Timeout = int.MaxValue;

                            request
                                .SelectInstance<StreamWriter>()
                                .Action(writer =>
                                {
                                    var param = new Dictionary<string, string>
                                    {
                                        ["client_id"] = client_id,
                                        ["code"] = code,
                                        ["grant_type"] = "authorization_code",
                                        ["client_secret"] = client_secret,
                                        ["state"] = state,
                                        ["redirect_uri"] = redirect_uri,
                                        ["scope"] = scope,
                                        ["timestamp"] = timestamp,
                                        ["token_type"] = "Bearer"
                                    }
                                        .Select(v => $"{v.Key}={v.Value}")
                                        .Join("&");
                                    writer.Write(param.Replace("+", "%2b"));
                                    writer.Flush();
                                    writer.Close();
                                });
                        })
                        .GetResponse().AsType<HttpWebResponse>()
                        .SelectInstance<StreamReader>()
                        .ReadToEnd()
                        .FromJson<ESIA_Marker_Answer>();


                    var marker_parts = marker.access_token.Split('.');

                    var header = Cert.Base64UrlDecode(marker_parts[0]).ToString(Encoding.UTF8);
                    var payload = Cert.Base64UrlDecode(marker_parts[1]).ToString(Encoding.UTF8);
                    string oid = JsonConvert.DeserializeObject<dynamic>(payload)["urn:esia:sbj_id"];

                    //генерим запрос для получения иформации о пользователе
                    var content = new StringBuilder();

                    var fullname = REST(marker, server_url_prns + oid);
                    content.Append($"<p>Фамилия: <b>{fullname["lastName"]}</b></p>");
                    content.Append($"<p>Имя: <b>{fullname["firstName"]}</b></p>");
                    content.Append($"<p>Отчество: <b>{fullname["middleName"]}</b></p>");

                    REST(marker, server_url_prns + oid + "/docs")["elements"]
                        .AsType<JArray>()
                        .ToObject<string[]>()
                        .ForEach((index, url) =>
                        {
                            var doc = REST(marker, url);
                            content.Append($"<h3>Документ №{index}</h3>");
                            content.Append($"<p>Серия: <b>{doc["series"]}</b></p>");
                            content.Append($"<p>Номер: <b>{doc["number"]}</b></p>");
                            content.Append($"<p>Выдан: <b>{doc["issuedBy"]}</b></p>");
                            content.Append($"<p>Код подразделения: <b>{doc["issueId"]}</b></p>");
                            content.Append($"<p>Дата выдачи: <b>{doc["issueDate"]}</b></p>");
                            content.Append($"<p>Статус: <b>{doc["vrfStu"]}</b></p>");
                        });

                    return Content(content.ToString());
                }
            }
            return Content("error esia");
        }

        public fxcDictSO REST(
            ESIA_Marker_Answer marker,
            string url)
            => WebRequest.Create(url).AsType<HttpWebRequest>()
                .Action(request =>
                {
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.Method = "GET";
                    request.Headers["Authorization"] = "Bearer " + marker.access_token;
                    request.Timeout = int.MaxValue;
                })
                .GetResponse().AsType<HttpWebResponse>()
                .SelectSingle(v => v.GetResponseStream())
                .SelectInstance<StreamReader>()
                .ReadToEnd()
                .FromJson<fxcDictSO>();
    }

    public class ESIA_Marker_Answer
    {
        public string state { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public string id_token { get; set; }
        public string access_token { get; set; }
    }
}