﻿#region

using System;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using Finflaex.Certificate;

#endregion

namespace modESIA
{
    public class Cert
    {
        public static X509Certificate2 Get => fxsCertificate
            .FindBySerial("62 13 b1 a2 dc 3f 39 a9 4c 7e 0b 79 c7 f5 4e e3")
            ;

        public static byte[] Sign(byte[] data, X509Certificate2 certificate = null)
        {
            if (certificate == null)
            {
                certificate = Get;
            }

            if (data == null)
                throw new ArgumentNullException(nameof(data));

            // setup the data to sign
            var content = new ContentInfo(data);
            var signedCms = new SignedCms(content, true);
            var signer = new CmsSigner(certificate);
            var sha256 = new Oid("2.16.840.1.101.3.4.2.1", "sha256");
            signer.DigestAlgorithm = sha256;

            // create the signature
            signedCms.ComputeSignature(signer);
            var signature = signedCms.Encode();

            return signature;
        }

        public static bool ValidateCmsSignature(byte[] data, byte[] signature, X509Certificate2 certificate = null)
        {
            if (certificate == null)
            {
                certificate = Get;
            }

            var result = false;

            if (data == null)
                throw new ArgumentNullException(nameof(data));
            if (signature == null)
                throw new ArgumentNullException(nameof(signature));
            if (certificate == null)
                throw new ArgumentNullException(nameof(certificate));

            // setup the data to sign
            var content = new ContentInfo(data);
            var signedCms = new SignedCms(content, true);

            try
            {
                signedCms.Decode(signature);
                signedCms.CheckSignature(new X509Certificate2Collection(certificate), true);
                result = true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }

            return result;
        }

        public static string Base64UrlEncode(byte[] arg)
        {
            var s = Convert.ToBase64String(arg); // Regular base64 encoder
            s = s.Split('=')[0]; // Remove any trailing '='s
            s = s.Replace('+', '-'); // 62nd char of encoding
            s = s.Replace('/', '_'); // 63rd char of encoding
            return s;
        }

        public static byte[] Base64UrlDecode(string arg)
        {
            var s = UrlDecode(arg);
            return Convert.FromBase64String(s); // Standard base64 decoder
        }

        public static string UrlDecode(string arg)
        {
            var s = arg;
            s = s.Replace('-', '+'); // 62nd char of encoding
            s = s.Replace('_', '/'); // 63rd char of encoding
            switch (s.Length%4) // Pad with trailing '='s
            {
                case 0:
                    break; // No pad chars in this case
                case 2:
                    s += "==";
                    break; // Two pad chars
                case 3:
                    s += "=";
                    break; // One pad char
                default:
                    throw new Exception(
                        "Illegal base64url string!");
            }

            return s;
        }

        public static bool ValidateSignature(byte[] data, byte[] signature, X509Certificate2 certificate = null)
        {
            if (certificate == null)
            {
                certificate = Get;
            }

            var result = false;
            using (var csp = (RSACryptoServiceProvider) certificate.PublicKey.Key)
            {
                using (var hasher = new SHA256Managed())
                {
                    var hash = hasher.ComputeHash(data);
                    var id = CryptoConfig.MapNameToOID("SHA256");
                    var isDataok = csp.VerifyData(data, id, signature);
                    var isHashOk = csp.VerifyHash(hash, id, signature);

                    //  можно ещё так
                    //RSAPKCS1SignatureDeformatter rsaDeformatter = new RSAPKCS1SignatureDeformatter(csp);
                    //rsaDeformatter.SetHashAlgorithm("SHA256");
                    //bool isHashOk2 = rsaDeformatter.VerifySignature(hash, signature);

                    result = isDataok && isHashOk;
                }
            }
            return result;
        }
    }
}