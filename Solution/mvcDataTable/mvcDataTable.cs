﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsQuery;
using Finflaex.Plugin;
using Finflaex.Reflection;

namespace mvcDataTable
{
    [Export(typeof(fxiMvcPlugin))]
    public class mvcDataTable : fxaMvcPlugin
    {
        public override string Name => "DataTable";

        public override string Version => "start";

        public override DateTime? DataCreate => new DateTime(2016, 08, 15);

        public override int Prioritet => 0;

        public override string HeadScript
            => this.GetResource("jquery.dataTables").ToString(Encoding.UTF8)
               + this.GetResource("dataTables.tableTools").ToString(Encoding.UTF8)
               + this.GetResource("dataTables.bootstrap").ToString(Encoding.UTF8)
               + this.GetResource("dataTables.responsive").ToString(Encoding.UTF8);

        public override string AfterContentScript
            => this.GetResource(nameof(AfterContentScript)).ToString(Encoding.UTF8);

        public override string GetContent(params object[] param)
        {
            var config = param.FirstOrDefault();

            var doc = CQ.CreateDocument(Body);
            var table = doc["table"];
            var id = table.Attr("id") ?? Guid.NewGuid().ToString("N");
            table.Attr("id", id);
            var body = doc.ToString();

            return Body + AfterContentScript.Replace(new Dictionary<string, string>()
            {
                {"\\/\\*id\\*\\/", id},
                {"\\/\\*config\\*\\/", config?.ToJson() ?? string.Empty}
            }).ScriptTagWrap();
        }
    }
}
