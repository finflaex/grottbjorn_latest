﻿#region

using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using Finflaex.Reflection;

#endregion

namespace Finflaex.MEF
{
    public static class fxsMef
    {
        private static readonly List<MefRecord> Containers = new List<MefRecord>();

        public static T LoadMef<T>(
            this T @this,
            string name,
            params object[] param)
        {
            @this.RemoveMef(name);
            Load(param)
                .IfNotNull(cat =>
                {
                    Containers.Add(new MefRecord
                    {
                        Catalog = cat,
                        Parametrs = param,
                        Name = name
                    });
                });
            return @this;
        }

        public static T UpdateMef<T>(
            this T @this
            , params string[] name
            )
        {
            Containers
                .Where(v => name.Length == 0 || name.Contains(v.Name))
                .SelectMany(mef => mef.Catalog)
                .SelectSingle(cat => new CompositionContainer(new AggregateCatalog(cat)))
                .ComposeParts(@this);
            return @this;
        }

        public static T RemoveMef<T>(
            this T @this,
            string name)
        {
            Containers.Where(v => v.Name == name).ForEach(v => Containers.Remove(v));
            return @this;
        }

        public static AssemblyCatalog[] Load(params object[] param)
        {
            var result = new List<AssemblyCatalog>();

            param
                .OfType<Assembly>()
                .Select(v => new AssemblyCatalog(v))
                .ForEach(result.Add);

            param
                .OfType<byte[]>()
                .Select(Assembly.Load)
                .Select(v => new AssemblyCatalog(v))
                .ForEach(result.Add);

            param
                .OfType<string>()
                .SelectMany(path =>
                {
                    if (File.Exists(path) && path.GetFileExtension() == "dll")
                    {
                        return File
                            .ReadAllBytes(path)
                            .SelectSingle(Assembly.Load)
                            .SelectSingle(v => new AssemblyCatalog(v))
                            .SingleToArray();
                    }
                    if (Directory.Exists(path))
                    {
                        return Directory
                            .GetFiles(path, "*.dll", SearchOption.AllDirectories)
                            .Select(File.ReadAllBytes)
                            .Select(Assembly.Load)
                            .Select(v => new AssemblyCatalog(v))
                            .ToArray();
                    }
                    if (Directory.Exists(path.GetServerDirectoryName()))
                    {
                        return path
                            .GetServerDirectoryName()
                            .SelectSingle(v => Directory.GetFiles(v, "*.dll", SearchOption.AllDirectories))
                            .Select(File.ReadAllBytes)
                            .Select(Assembly.Load)
                            .Select(v => new AssemblyCatalog(v))
                            .ToArray();
                    }
                    return new AssemblyCatalog[0];
                })
                .ForEach(result.Add);

            return result.ToArray();
        }

        private class MefRecord
        {
            public string Name;
            public object[] Parametrs;
            public AssemblyCatalog[] Catalog;
        }
    }
}