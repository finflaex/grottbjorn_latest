﻿#region

using System.ComponentModel;
using System.Linq;
using System.ServiceProcess;
using Finflaex.Reflection;
using Finflaex.Self;

#endregion

namespace Finflaex.Service
{
    public class fxcService<TThis> : ServiceBase
        where TThis : fxcService<TThis>
    {
        private readonly IContainer components;

        protected fxcService()
        {
            components = new Container();
            ServiceName = typeof (TThis).Name;
        }

        public static string Name
            => typeof (TThis).Name;

        public static string Path
            => fxsMain.ExecutablePath;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                components?.Dispose();
            }
            base.Dispose(disposing);
        }

        public void Run()
        {
            Run(this);
        }

        public static void Install(string fullpath = null, string name = null)
            => fxsService
                .InstallService(fullpath ?? Path, name ?? Name, name ?? Name);

        public static void Uninstall(string name = null)
            => fxsService
                .UnInstallService(name ?? Name);

        public static void Start(string name = null)
            => fxsService
                .Start(name ?? Name);

        public static void Stop(string name = null)
            => fxsService
                .Stop(name ?? Name);

        public static bool IsInstall(string name = null)
            => fxsService
                .List()
                .Select(v => v.ServiceName)
                .Contains(name ?? Name);

        public static bool Initialize(string path = null, string name = null)
            => IsInstall(name ?? Name)
                .IfTrue(() => Run(typeof (TThis).Create().AsType<TThis>()));
    }
}