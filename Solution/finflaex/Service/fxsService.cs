﻿#region

using System;
using System.ServiceProcess;
using Finflaex.WinApi;

#endregion

namespace Finflaex.Service
{
    public static class fxsService
    {
        public const int SC_MANAGER_CREATE_SERVICE = 0x0002;
        public const int SERVICE_WIN32_OWN_PROCESS = 0x00000010;
        //public const int SERVICE_DEMAND_START = 0x00000003;
        public const int SERVICE_ERROR_NORMAL = 0x00000001;
        public const int STANDARD_RIGHTS_REQUIRED = 0xF0000;
        public const int SERVICE_QUERY_CONFIG = 0x0001;
        public const int SERVICE_CHANGE_CONFIG = 0x0002;
        public const int SERVICE_QUERY_STATUS = 0x0004;
        public const int SERVICE_ENUMERATE_DEPENDENTS = 0x0008;
        public const int SERVICE_START = 0x0010;
        public const int SERVICE_STOP = 0x0020;
        public const int SERVICE_PAUSE_CONTINUE = 0x0040;
        public const int SERVICE_INTERROGATE = 0x0080;
        public const int SERVICE_USER_DEFINED_CONTROL = 0x0100;

        public const int SERVICE_ALL_ACCESS = STANDARD_RIGHTS_REQUIRED |
                                              SERVICE_QUERY_CONFIG |
                                              SERVICE_CHANGE_CONFIG |
                                              SERVICE_QUERY_STATUS |
                                              SERVICE_ENUMERATE_DEPENDENTS |
                                              SERVICE_START |
                                              SERVICE_STOP |
                                              SERVICE_PAUSE_CONTINUE |
                                              SERVICE_INTERROGATE |
                                              SERVICE_USER_DEFINED_CONTROL;

        public const int SERVICE_AUTO_START = 0x00000002;

        /// <summary>
        ///     This method installs and runs the service in the service control manager.
        /// </summary>
        /// <param name="svcPath">The complete path of the service.</param>
        /// <param name="svcName">Name of the service.</param>
        /// <param name="svcDispName">Display name of the service.</param>
        /// <returns>True if the process went thro successfully. False if there was any error.</returns>
        public static bool InstallService(string svcPath, string svcName, string svcDispName)
        {
            try
            {
                var sc_handle = advapi32.OpenSCManager(null, null, SC_MANAGER_CREATE_SERVICE);
                if (sc_handle != 0)
                {
                    var sv_handle = advapi32.CreateService(sc_handle, svcName, svcDispName, SERVICE_ALL_ACCESS,
                        SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, SERVICE_ERROR_NORMAL, svcPath, null, 0, null,
                        null, null);
                    if (sv_handle == 0)
                    {
                        advapi32.CloseServiceHandle(sc_handle);
                        return false;
                    }
                    //now trying to start the service
                    var i = advapi32.StartService(sv_handle, 0, null);
                    // If the value i is zero, then there was an error starting the service.
                    // note: error may arise if the service is already running or some other problem.
                    if (i == 0)
                    {
                        //Console.WriteLine("Couldnt start service");
                        return false;
                    }
                    //Console.WriteLine("Success");
                    advapi32.CloseServiceHandle(sc_handle);
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///     This method uninstalls the service from the service conrol manager.
        /// </summary>
        /// <param name="svcName">Name of the service to uninstall.</param>
        public static bool UnInstallService(string svcName)
        {
            var GENERIC_WRITE = 0x40000000;
            var sc_hndl = advapi32.OpenSCManager(null, null, GENERIC_WRITE);
            if (sc_hndl != 0)
            {
                var DELETE = 0x10000;
                var svc_hndl = advapi32.OpenService(sc_hndl, svcName, DELETE);
                //Console.WriteLine(svc_hndl.ToInt32());
                if (svc_hndl == 0) return false;
                var i = advapi32.DeleteService(svc_hndl);
                if (i != 0)
                {
                    advapi32.CloseServiceHandle(sc_hndl);
                    return true;
                }
                advapi32.CloseServiceHandle(sc_hndl);
                return false;
            }
            return false;
        }

        public static void Start(string name)
        {
            var sc = new ServiceController(name);
            sc.Refresh();
            if (sc.Status == ServiceControllerStatus.Running) return;
            if (sc.Status == ServiceControllerStatus.PausePending)
            {
                sc.WaitForStatus(ServiceControllerStatus.Paused);
                sc.Continue();
            }
            if (sc.Status == ServiceControllerStatus.StopPending)
            {
                sc.WaitForStatus(ServiceControllerStatus.Stopped);
            }
            if (sc.Status != ServiceControllerStatus.StartPending)
            {
                sc.Start();
            }
            sc.WaitForStatus(ServiceControllerStatus.Running);
        }

        public static void Stop(string name)
        {
            var sc = new ServiceController(name);
            sc.Refresh();
            if (sc.Status == ServiceControllerStatus.Stopped) return;
            if (sc.Status == ServiceControllerStatus.PausePending)
            {
                sc.WaitForStatus(ServiceControllerStatus.Paused);
                sc.Continue();
            }
            if (sc.Status == ServiceControllerStatus.ContinuePending
                || sc.Status == ServiceControllerStatus.StartPending)
            {
                sc.WaitForStatus(ServiceControllerStatus.Running);
            }
            if (sc.Status != ServiceControllerStatus.StopPending)
            {
                sc.Stop();
            }
            sc.WaitForStatus(ServiceControllerStatus.Stopped);
        }

        public static ServiceController[] List()
        {
            return ServiceController.GetServices();
        }
    }
}