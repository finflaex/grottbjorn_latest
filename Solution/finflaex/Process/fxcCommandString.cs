﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security;
using System.Text;
using Finflaex.Reflection;
using Finflaex.Self;
using NUnit.Framework;

#endregion

namespace Finflaex.Process
{
    [TestFixture]
    public class fxcCommandString : IDisposable
    {
        private readonly System.Diagnostics.Process _proc;
        private readonly string path;

        public fxcCommandString() : this(null, null)
        {
        }

        public fxcCommandString(string login = null, string password = null, string path = null)
        {
            this.path = path ?? fxsMain.ExecutableDirectory;
            _proc = new System.Diagnostics.Process
            {
                StartInfo = new ProcessStartInfo(@"cmd.exe")
                {
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    WorkingDirectory = path,
                    LoadUserProfile = true
                }
            };

            _proc.StartInfo.UserName = login
                .IsNullOrEmpty()
                ? null
                : login;

            _proc.StartInfo.Password = password
                .IsNullOrEmpty()
                ? null
                : new SecureString()
                    .IfNotNull(str => { password.ToCharArray().ForEach(str.AppendChar); });

            _proc.Start();

            Wait();
        }

        private string Wait()
        {
            var list = new List<string>();
            var read = string.Empty;

            while (true)
            {
                var ch = StreamReader.Read();
                if (ch == 10) continue;
                if (ch == 13)
                {
                    read = Encoding
                        .GetEncoding(866)
                        .GetString(Encoding.GetEncoding(1251).GetBytes(read));
                    if (!read.IsNullOrEmpty())
                    {
                        list.Add(read);
                    }
                    read = string.Empty;
                    continue;
                }
                read += (char) ch;
                if (read.Length == path.Length + 1
                    && read.Compare($"{path}>"))
                {
                    return list.Join("\r\n");
                }
            }
        }


        private StreamReader StreamReader => _proc.StandardOutput;

        private StreamWriter StreamWriter => _proc.StandardInput;

        public string Execute(string cmd)
        {
            StreamWriter.WriteLine(cmd);
            return Wait();
        }

        public void Dispose()
        {
            _proc?.Dispose();
        }

        [Test]
        public void Test()
        {
            var cmd = new fxcCommandString();
            cmd.Execute("ipconfig");
        }
    }
}