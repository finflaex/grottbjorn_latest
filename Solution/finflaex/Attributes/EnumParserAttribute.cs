﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Attributes
{
    public class EnumParserAttribute : Attribute
    {
        public string Name { get; set; }
    }
}
