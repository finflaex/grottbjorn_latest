﻿#region

using System.Net;
using System.Net.Mail;

#endregion

namespace Finflaex.Mail
{
    public class fxcMail
    {
        private readonly SmtpClient smtp;

        public fxcMail(string host, int port, string login, string pass, bool ssl = false)
        {
            smtp = new SmtpClient
            {
                Host = host,
                Port = port,
                EnableSsl = ssl,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(login, pass)
            };
        }

        public void Send(string from, string to, string title, string body)
        {
            using (var message = new MailMessage(from, to)
            {
                Subject = title,
                Body = body
            })
            {
                smtp.Send(message);
            }
        }

        public void Send(MailAddress from, MailAddress to, string title, string body)
        {
            using (var message = new MailMessage(from, to)
            {
                Subject = title,
                Body = body
            })
            {
                smtp.Send(message);
            }
        }
    }
}