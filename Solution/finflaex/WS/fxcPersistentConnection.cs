﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Finflaex.Attributes;
using Finflaex.fxMVC.Authorization;
using Finflaex.Log;
using Finflaex.MVC;
using Finflaex.Plugin;
using Finflaex.Reflection;
using Finflaex.Self;
using Finflaex.Types;
using Microsoft.AspNet.SignalR;

#endregion

namespace Finflaex.WS
{
    public abstract class fxcPersistentConnection : PersistentConnection, fxiStackEventClass
    {
        private static readonly fxcStack<fxcStackEventMethod> stack = new fxcStack<fxcStackEventMethod>();
        public static readonly Dictionary<Guid, fxcPersistentConnection> Connections = new Dictionary<Guid, fxcPersistentConnection>();

        protected fxcPersistentConnection()
        {
            try
            {
                fxsMain.MEF?.ComposeParts(this);
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    GetType(),
                    MethodBase.GetCurrentMethod(),
                    error);
            }
        }

        //[ImportMany(typeof (fxiJsonPlugin))]
        //public IEnumerable<fxiJsonPlugin> Plugins { get; set; }

        public Guid ConnectionID { get; private set; }
        public Guid ConnectionGuid { get; private set; }

        public fxcStack<fxcStackEventMethod> _stack => stack;

        protected override Task OnConnected(IRequest request, string connectionId)
        {
            ConnectionID = Guid.Parse(connectionId);
            var value = connected();
            return value == null ? Task.CompletedTask : Connection.Send(connectionId, value);
        }

        protected override Task OnDisconnected(IRequest request, string connectionId, bool stopCalled)
        {
            if (Connections.ContainsKey(ConnectionGuid))
            {
                Connections.Remove(ConnectionGuid);
            }
            ConnectionID = Guid.Parse(connectionId);
            var value = disconected();
            return value == null ? Task.CompletedTask : Connection.Send(connectionId, value);
        }

        protected virtual object connected()
        {
            return null;
        }

        protected virtual object disconected()
        {
            return null;
        }

        protected override Task OnReconnected(IRequest request, string connectionId)
        {
            ConnectionID = Guid.Parse(connectionId);
            var value = connected();
            return value == null ? Task.CompletedTask : Connection.Send(connectionId, value);
        }

        protected override Task OnReceived(IRequest request, string connectionId, string data)
        {
            ConnectionID = Guid.Parse(connectionId);
            string send;

            var read = data.FromJson<fxcDictSO>();
            read["ConnectionID"] = ConnectionID;

            if (read.GetString("c")?.Equals("start") ?? false)
            {
                ConnectionGuid = read.GetString("guid").ToGuid();
                Connections[ConnectionGuid] = this;
                return Task.CompletedTask;
            }

            if (read.GetString("c")?.Equals("close") ?? false)
            {
                return OnDisconnected(request, connectionId, false);
            }

            try
            {
                var back = read.Get<string>("b");
                if (back != null)
                {
                    object value;
                    if (this.RunStackMethod(back, read, out value))
                    {
                        send = GetSend(value);
                        if (send != null)
                        {
                            return Connection.Send(connectionId, send);
                        }
                    }
                }

                var result = this.RunMethod(read.Get<string>("c"), ConnectionID, read);
                send = GetSend(result);
                if (send != null)
                {
                    return Connection.Send(connectionId, send);
                }
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    typeof (fxcPersistentConnection),
                    MethodBase.GetCurrentMethod(),
                    error,
                    "запуск метода");
            }

            //try
            //{
            //    send = Plugins
            //        .OrderBy(v => v.Prioritet)
            //        .Where(v =>
            //        {
            //            var roles = v
            //                .GetType()
            //                .GetAttributesOfMethod<RolesAttribute, qwe>(() => v.Run)
            //                .FirstOrDefault()?
            //                .Roles;

            //            return Access(ConnectionID, roles);
            //        })
            //        .WhereNotNull()
            //        .AsParallel()
            //        .AsOrdered()
            //        .Select(v =>
            //        {
            //            return v.Run(read);
            //        })
            //        .WhereNotNull()
            //        .FirstOrDefault()?
            //        .SelectSingle(GetSend);
            //    if (send != null)
            //    {
            //        return Connection.Send(connectionId, send);
            //    }
            //}
            //catch (Exception error)
            //{
            //    fxsLog.Write(
            //        fxeLog.FinflaexError,
            //        typeof (fxcPersistentConnection),
            //        MethodBase.GetCurrentMethod(),
            //        error,
            //        "запуск метода плагина");
            //}

            return Task.CompletedTask;
        }

        private delegate Func<object,object> qwe();

        private static string GetSend(object value)
        {
            if (value == null)
            {
                return null;
            }
            if (value is string)
            {
                return value.AsType<string>();
            }
            if (value is fxcDictSO)
            {
                return value.AsType<fxcDictSO>().ToJson();
            }
            if (value is fxcDictSO[])
            {
                return value.AsType<fxcDictSO[]>().ToJson();
            }
            if (value is object[])
            {
                var arr = value.AsType<object[]>();
                var dct = arr.OfType<fxcDictSO>().FirstOrDefault()?.ToJson();
                var lst = arr.OfType<fxcDictSO[]>().FirstOrDefault()?.ToJson();
                var str = arr.OfType<string>().FirstOrDefault();
                return dct ?? str ?? lst;
            }
            return null;
        }

        public Task Send(object data) => Connection.Send(ConnectionID.ToString(), GetSend(data));
    }
}