﻿#region

using Finflaex.Reflection;
using Finflaex.Types;

#endregion

namespace Finflaex.WS
{
    public static class fxsJsonResponse
    {
        public static fxcDictSO Send(
            fxcDictSO data) => new fxcDictSO
            {
                ["c"] = "send",
                ["d"] = data
            };

        public static fxcDictSO SetCookie(
            string name,
            string value) => new fxcDictSO
            {
                ["c"] = "scook",
                ["n"] = name,
                ["v"] = value
            };

        public static fxcDictSO LoadHtmlPost(
            string url,
            string target
            ) => new fxcDictSO
            {
                ["c"] = "lhtml",
                ["u"] = url,
                ["t"] = target
            };

        public static fxcDictSO Html(
            string target,
            string data
            ) => new fxcDictSO
            {
                ["c"] = "html",
                ["t"] = target,
                ["d"] = data.ToHtmlMin().ToBase64()
            };

        public static fxcDictSO Append(
            string target,
            string data
            ) => new fxcDictSO
            {
                ["c"] = "append",
                ["t"] = target,
                ["d"] = data.ToHtmlMin().ToBase64()
            };

        public static fxcDictSO Prepend(
            string target,
            string data
            ) => new fxcDictSO
            {
                ["c"] = "prepend",
                ["t"] = target,
                ["d"] = data.ToHtmlMin().ToBase64()
            };

        public static fxcDictSO Eval(
            string code)
            => new fxcDictSO()
            {
                ["c"] = "eval",
                ["d"] = code.ToJsMin().ToBase64(),
            };

        public static fxcDictSO Notify(
            string message,
            fxeTypeNotify? type = null)
            => new fxcDictSO()
            {
                ["c"] = "notify",
                ["m"] = message,
                ["t"] = (type ?? fxeTypeNotify.info).ToString()
            };

        public static fxcDictSO Redirect(
            string url)
            =>new fxcDictSO()
            {
                ["c"] = "redirect",
                ["u"] = url,
            };
    }
}