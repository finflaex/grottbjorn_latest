﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.WS
{
    public enum fxeTypeNotify
    {
        info,
        success,
        danger,
        error,
    }
}
