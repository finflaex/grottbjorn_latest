﻿#region

using System;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

#endregion

namespace Finflaex.WS
{
    public class fxcWssServer
    {
        private readonly Socket socket;

        public fxcWssServer(string certificate_path, int port, Action<SslStream> action = null)
        {
            CreateWebSocketServer(port, certificate_path, out socket, action);
        }

        private static IPEndPoint CreateWebSocketServer(int port, string path, out Socket soc, Action<SslStream> action)
        {
            soc = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            var endpoint = new IPEndPoint(IPAddress.Loopback, port);
            soc.Bind(endpoint);
            soc.Listen(int.MaxValue);
            ListenForClients(soc, path, action);
            return endpoint;
        }

        private static void ListenForClients(Socket soc, string path, Action<SslStream> action = null)
        {
            soc.BeginAccept(result =>
            {
                new Thread(() => ListenForClients(soc, path)).Start();
                var clientSocket = soc.EndAccept(result);
                using (
                    var stream = new SslStream(
                        new NetworkStream(clientSocket),
                        false,
                        (sender, certificate, chain, sslPolicyErrors) => sslPolicyErrors == SslPolicyErrors.None,
                        (sender, targetHost, localCertificates, remoteCertificate, acceptableIssuers)
                            => new X509Certificate2(path), EncryptionPolicy.RequireEncryption)
                    )
                {
                    stream.AuthenticateAsServer(new X509Certificate2(path), true, SslProtocols.Tls12, true);
                    action?.Invoke(stream);
                    stream.Close();
                    stream.Dispose();
                }
            }, null);
        }
    }
}