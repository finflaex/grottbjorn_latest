﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Finflaex.fxMVC.Authorization;
using Finflaex.MEF;
using Finflaex.Plugin;
using Finflaex.Reflection;
using Finflaex.Types;
using Finflaex.WS;

#endregion

namespace Finflaex.fxMVC.WebSocket
{
    public class WSController : Controller
    {
        private fxcDictSO _data;

        public WSController()
        {
            this.UpdateMef();
        }

        [ImportMany(typeof (fxiMvcPlugin))]
        public IEnumerable<fxiMvcPlugin> Plugins { get; set; }

        [ImportMany(typeof(fxiMvcModul))]
        public IEnumerable<fxiMvcModul> Moduls { get; set; }

        private Guid ConnectionGuid => HttpContext.GetUser().Guid;

        public new Authentificate User => HttpContext.GetUser();

        protected fxcDictSO Data
            => _data
               ?? (_data = this.ReadPost().FromJson<fxcDictSO>());

        private fxcPersistentConnection Connection
            => fxcPersistentConnection.Connections.ContainsKey(ConnectionGuid)
                ? fxcPersistentConnection.Connections[ConnectionGuid]
                : null;

        public void WsSend(object data) => Connection?.Send(data);

        public EmptyResult Do(
            Action<fxcPersistentConnection> action)
        {
            action?.Invoke(Connection);
            return new EmptyResult();
        }

        public ActionResult Do(
            Func<fxcPersistentConnection, ActionResult> action)
            => action?.Invoke(Connection)
               ?? new EmptyResult();

        public Task<ActionResult> DoAsync(
            Func<fxcPersistentConnection, ActionResult> func)
            => Task.Run(()
                => func?.Invoke(Connection)
                   ?? new EmptyResult());

        public Task<ActionResult> DoAsync(
            Action<fxcPersistentConnection> func)
            => Task.Run(() =>
            {
                func?.Invoke(Connection);
                return new EmptyResult().AsType<ActionResult>();
            });

        public ActionResult Plugin(string name, Func<fxiMvcPlugin, object> action = null, Func<object> none = null)
        {
            var plugin = Plugins?.FirstOrDefault(v => v.Name == name);
            object result = plugin == null ? none?.Invoke() : action?.Invoke(plugin);

            return result is string
                ? result.AsType<string>().SelectSingle(Content)
                : result is ActionResult
                    ? result.AsType<ActionResult>()
                    : new EmptyResult();
        }

        public string Plugin(
            string name,
            Func<fxiMvcPlugin, string> action = null,
            Func<string> none = null)
        {
            var plugin = Plugins?.FirstOrDefault(v => v.Name == name);
            return plugin == null ? none?.Invoke() : action?.Invoke(plugin);
        }
    }
}