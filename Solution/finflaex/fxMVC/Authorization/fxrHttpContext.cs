﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Finflaex.Reflection;

#endregion

namespace Finflaex.fxMVC.Authorization
{
    public static class fxrHttpContext
    {
        public static event Func<Guid, Authentificate> CreateEmptyAuthentificate;

        public static Authentificate SetUser(
            this HttpContextBase @this,
            Authentificate data = null,
            bool persistent = false)
        {
            var result = data ?? new Authentificate
            {
                Guid = Guid.NewGuid(),
                Roles = new string[0]
            };

            var ticket = new FormsAuthenticationTicket(
                1, 
                FormsAuthentication.FormsCookieName, 
                DateTime.Now,
                persistent ? DateTime.MaxValue : DateTime.Now.Add(FormsAuthentication.Timeout), 
                persistent,
                result.ToJson(), 
                FormsAuthentication.FormsCookiePath);

            @this.Response.Cookies.Set(new HttpCookie(FormsAuthentication.FormsCookieName)
            {
                Value = FormsAuthentication.Encrypt(ticket),
                Expires = ticket.Expiration
            });

            return result;
        }

        public static Authentificate GetUser(
            this HttpContextBase @this)
        {
            Authentificate result = null;
            FormsAuthenticationTicket ticket = null;
            var cook = @this.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
            if (cook?.Value.IsNotNullOrEmpty() ?? true)
            {
                result = CreateEmptyAuthentificate?
                    .Invoke(Guid.Empty);
            }
            else
            {
                ticket = FormsAuthentication.Decrypt(cook.Value);
                if (ticket == null)
                {
                    result = CreateEmptyAuthentificate?
                        .Invoke(Guid.Empty);
                }
            }

            return result
                   ?? ticket?.UserData.FromJson<Authentificate>()
                   ?? CreateEmptyAuthentificate?
                       .Invoke(ticket?.Name.ToGuid() ?? Guid.Empty);
        }
    }

    public class Authentificate
    {
        public Guid Guid { get; set; }
        public string Login { get; set; }
        public string[] Roles { get; set; }

        public bool InRoles(IEnumerable<string> arg)
            => Roles.Intersect(arg).Any();

        public bool InRole(string arg)
            => Roles.Contains(arg);

        public bool Verificate(RolesAttribute role)
        {
            if (Login == "architector")
            {
                return true;
            }
            if (!role.Anonymous && Roles.Contains("developer"))
            {
                return true;
            }
            if (role.Anonymous && Login == null)
            {
                return true;
            }
            if (role.Registration && Login != null && role.Roles.Length == 0)
            {
                return true;
            }
            return role.Registration && Login != null && InRoles(role.Roles);
        }
    }
}