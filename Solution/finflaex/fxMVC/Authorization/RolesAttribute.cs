﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Finflaex.Reflection;

namespace Finflaex.fxMVC.Authorization
{
    [AttributeUsage(AttributeTargets.All)]
    public class RolesAttribute : AuthorizeAttribute
    {
        public RolesAttribute(
            bool anonymous = false, 
            bool registr = true, 
            params string[] roles)
        {
            Anonymous = anonymous;
            Registration = registr;
            Roles = roles;
        }

        public RolesAttribute(
            params string[] roles)
            : this(false, true, roles)
        {
        }

        public bool Anonymous { get; private set; }

        private AuthorizationContext _currentContext;
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            _currentContext = filterContext;

            string controllerName = filterContext.RouteData.Values["controller"].AsType<string>();
            string actionName = filterContext.RouteData.Values["action"].AsType<string>();

            if (!AuthorizeCore(filterContext.HttpContext))
                filterContext.Result = new EmptyResult();
        }

        public new string[] Roles { get; }
        public bool Registration { get; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var auth = httpContext.GetUser() ?? httpContext.SetUser();
            return auth.Verificate(this);
        }
    }
}
