﻿using System;
using System.Web;
using System.Web.Mvc;
using Finflaex.Reflection;

namespace Finflaex.fxMVC.Authorization
{
    public class OnlyAnonymousAttribute: AuthorizeAttribute
    {
        private AuthorizationContext _currentContext;
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            _currentContext = filterContext;
            //if (!AuthorizeCore(filterContext.HttpContext))
                base.OnAuthorization(filterContext);
        }


        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.Request.IsAuthenticated && httpContext.GetUser().Login == null;
        }
    }
}
