﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Finflaex.Reflection;

namespace Finflaex.fxMVC.File
{
    public class MvcFile
    {
        public byte[] Data { get; internal set; }
        public string Name { get; internal set; }
        public int Size { get; internal set; }
        public string Type { get; internal set; }
    }

    public static class fxrFile
    {
        public static MvcFile[] LoadFiles(
            this IController @this)
        {
            var result = new List<MvcFile>();
            var files = HttpContext
                .Current
                .Request
                .Files;
            files
                .Cast<string>()
                .ForEach(name =>
                {
                    var file = files[name];
                    new MvcFile()
                    {
                        Name = file.FileName,
                        Type = file.ContentType,
                        Size = file.ContentLength,
                        Data = file.InputStream.ToBytes()
                    }.IfNotNull(result.Add);
                });

            return result.ToArray();
        }
    }
}
