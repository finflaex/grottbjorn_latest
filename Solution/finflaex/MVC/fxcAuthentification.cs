﻿#region

using System;
using System.Reflection;
using System.Web;
using System.Web.Security;
using Finflaex.Log;
using Finflaex.MsSQL.Tables;
using Finflaex.Reflection;

#endregion

namespace Finflaex.MVC
{
    public class fxcAuthentification<TUser>
        where TUser : class, fxiTableKeyGuid
    {
        private const string cookieName = "__AUTH_COOKIE";

        public static Func<string, TUser> GetUser;


        public fxcAuthentification(HttpContextBase context)
        {
            this.context = context;
            try
            {
                var authCookie = context.Request.Cookies.Get(cookieName);
                if (!(authCookie?.Value.IsNullOrEmpty() ?? true))
                {
                    var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    user = GetUser.Invoke(ticket.Name);
                }
                else
                {
                    user = typeof (TUser).Create().AsType<TUser>();
                }
            }
            catch (Exception error)
            {
                user = typeof (TUser).Create().AsType<TUser>();
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    typeof (fxcAuthentification<TUser>),
                    MethodBase.GetCurrentMethod(),
                    error);
            }

            context.Session?.Add("fxAuth", this);
        }

        public TUser user { get; set; }

        public HttpContextBase context { get; }

        public void CreateCookie(string userName, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(
                1,
                userName,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                isPersistent,
                string.Empty,
                FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket.
            var encTicket = FormsAuthentication.Encrypt(ticket);

            // Create the cookie.
            var AuthCookie = new HttpCookie(cookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
            context.Response.Cookies.Set(AuthCookie);
        }

        public void ClearCookie()
        {
            var httpCookie = context.Response.Cookies[cookieName];
            if (httpCookie == null) return;
            httpCookie.Value = string.Empty;
            user = null;
        }
    }
}