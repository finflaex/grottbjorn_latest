﻿#region

using Microsoft.Web.WebSockets;

#endregion

namespace Finflaex.MVC
{
    public class fxaWsHandler : WebSocketHandler
    {
        public static readonly WebSocketCollection Clients = new WebSocketCollection();

        public override void OnOpen()
        {
            base.OnOpen();
            Clients.Add(this);
        }
    }
}