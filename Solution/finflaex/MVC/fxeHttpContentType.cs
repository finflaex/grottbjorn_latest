﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Finflaex.MVC
{
    public enum fxeHttpContentType : long
    {
        [Description("text/plain; charset=utf-8")]
        plain = 0,

        [Description("text/HTML")]
        HTML = 1,

        [Description("text/js; charset=utf-8")]
        js = 4,

        [Description("text/css; charset=utf-8")]
        css = 5,

        [Description("text/css; charset=utf-8")]
        less = 6,


        [Description("image/GIF")]
        Gif = 2,

        [Description("image/JPEG")]
        JPEG = 3,

        [Description("image/x-icon")]
        icon = 7,

        [Description("image/png")]
        png = 8,


        [Description("application/x-font-woff")]
        woff = 9,

        [Description("application/x-font-ttf")]
        ttf = 10,
    }
}
