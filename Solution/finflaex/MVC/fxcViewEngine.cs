﻿#region

using System.Linq;
using System.Web.Mvc;

#endregion

namespace Finflaex.MVC
{
    public class fxcViewEngine : RazorViewEngine
    {
        public fxcViewEngine(params object[] param)
        {
            var qwe = param
                .OfType<string>()
                .SelectMany(v => new[]
                {
                    "/Views/Shared/{0}.cshtml",
                    v + "/{1}/Views/{0}.cshtml"
                    //v + "/Views/Shared/{0}.cshtml",
                })
                .ToArray();

            ViewLocationFormats = ViewLocationFormats
                .Concat(qwe)
                .ToArray();
        }
    }
}