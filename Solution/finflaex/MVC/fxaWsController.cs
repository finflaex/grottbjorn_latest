﻿#region

using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Web.WebSockets;

#endregion

namespace Finflaex.MVC
{
    public abstract class fxaWsController<T> : ApiController
        where T : fxaWsHandler
    {
        public HttpResponseMessage Get()
        {
            HttpContext.Current.AcceptWebSocketRequest((T) Activator.CreateInstance(typeof (T)));
            return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
        }
    }
}