#region

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Finflaex.Log;
using Finflaex.Plugin;
using Finflaex.Reflection;
using Finflaex.Self;
using Finflaex.Types;

#endregion

namespace Finflaex.MVC
{
    public class fxcActionInvoker : IActionInvoker
    {
        public fxcActionInvoker()
        {
            fxsMain.MEF?.ComposeParts(this);
        }

        //[ImportMany(typeof (fxiClassPlugin))]
        //// ReSharper disable once MemberCanBePrivate.Global
        //// ReSharper disable once UnusedAutoPropertyAccessor.Global
        //public IEnumerable<fxiClassPlugin> Plugins { get; set; }

        public bool InvokeAction(ControllerContext controllerContext,
            string actionName)
        {
            var values = controllerContext.RouteData.Values;

            var controller = values.ContainsKey("controller")
                ? values["controller"].AsType<string>()
                : "unknown";

            var method = values.ContainsKey("action")
                ? values["action"].AsType<string>()
                : "unknown";

            var param = new fxcDictSO();
            foreach (var val in values) param[val.Key] = val.Value;

            var cookie = GetCookie(controllerContext);

            var result = GetResult(controller, method, param, controllerContext, cookie)
                         ?? GetResult("unknown", "unknown", param, controllerContext, cookie);

            if (result == null) return false;
            var new_cookie = result
                .OfType<fxcCookieData>()
                .FirstOrDefault() ?? cookie;
            // ReSharper disable once PossibleNullReferenceException
            controllerContext
                .HttpContext
                .Response
                .Cookies["finflaex"]
                .Value = new_cookie
                    .ToJson()
                    .Encrypt()
                    .ToBase64();
            controllerContext.HttpContext.Response.Write(result.OfType<string>().FirstOrDefault() ?? "��������� �����");
            return true;
        }

        private static fxcCookieData GetCookie(ControllerContext request)
        {
            var cookie = request
                .HttpContext
                .Request
                .Cookies["finflaex"]?
                .Value
                .FromBase64()
                .Decrypt()
                .FromJson<fxcCookieData>();
            return cookie ?? new fxcCookieData();
        }

        private object[] GetResult(string controller, string method, params object[] values)
        {
            values = values.AddRangeRet(new[]
            {
                controller,
                method
            })
                .ToArray();

            //try
            //{
            //    return Plugins
            //        .Where(v => v.Type.Compare(controller) && v.Methods.Contains(method))
            //        .OrderByDescending(v=>v.Prioritet)
            //        .AsParallel()
            //        .AsOrdered()
            //        .Select(v => v.Run(values))
            //        .WhereNotNull()
            //        .FirstOrDefault();
            //}
            //catch (Exception error)
            //{
            //    fxsLog.Write(
            //        fxeLog.FinflaexError,
            //        GetType(),
            //        MethodBase.GetCurrentMethod(),
            //        error);
            //    return null;
            //}

            return null;
        }
    }
}