﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using Finflaex.Log;
using Finflaex.Reflection;
using Finflaex.Self;
using Finflaex.Types;

#endregion

namespace Finflaex.MVC
{
    /// <summary>
    ///     ControllerBuilder.Current.SetControllerFactory(new ControllerFactory()); in global.asax
    /// </summary>
    public sealed class fxcControllerFactory : IControllerFactory
    {
        [ImportMany(typeof (IController))]
        public IEnumerable<IController> Controllers { get; set; }

        public static event Action<fxcControllerFactory> Create;

        public IController CreateController(RequestContext requestContext,
            string controllerName)
        {
            //if (controllerName.ToLower() == "resetmef")
            //{
            //    fxsMain.LoadMef();
            //    fxsMain.MEF.ComposeParts(this);
            //    return null;
            //}

            OnCreate(this);

            var type = BuildManager
                .GetReferencedAssemblies()
                .Cast<Assembly>()
                .SelectMany(a =>
                {
                    try
                    {
                        return a.GetTypes();
                    }
                    catch (Exception error)
                    {
                        fxsLog.Write(
                            fxeLog.FinflaexError,
                            fxeLog.AdminError,
                            typeof(fxcControllerFactory),
                            MethodBase.GetCurrentMethod(),
                            error,
                            "Ресурс не загружен",
                            new fxcDictSO()
                            {
                               ["resurce"] = a.FullName,
                            });
                    }
                    return new Type[0];
                })
                .Where(v => v.IsSubclassOf(typeof (Controller)))
                .AddRangeRet(Controllers?.Select(v => v.GetType()) ?? new List<Type>())
                .FirstOrDefault(v
                    => v.Name.ToLower() == controllerName.ToLower() ||
                       (v.Name.Length > 10 &&
                        v.Name.Remove(v.Name.Length - 10).ToLower() == controllerName.ToLower()));

            return (IController) DependencyResolver
                .Current
                .GetService(type ?? typeof (fxcDefaultController));
        }

        public SessionStateBehavior GetControllerSessionBehavior(RequestContext
            requestContext, string controllerName)
        {
            return SessionStateBehavior.Default;
        }

        public void ReleaseController(IController controller)
        {
            var disposable = controller as IDisposable;
            disposable?.Dispose();
        }

        private void OnCreate(fxcControllerFactory obj)
        {
            Create?.Invoke(obj);
        }
    }
}