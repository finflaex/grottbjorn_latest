﻿using System;

namespace Finflaex.MVC
{
    public class fxcCookieData
    {
        public fxcCookieData()
        {
            SessionID = Guid.Empty;
            OpenDate = DateTime.Now;
        }

        public Guid SessionID { get; set; }
        public DateTime OpenDate { get; set; }
    }
}