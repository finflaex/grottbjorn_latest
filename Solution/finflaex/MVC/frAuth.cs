using System;
using System.Web;
using Finflaex.MsSQL.Tables;

namespace Finflaex.MVC
{
    public static class frAuth
    {
        public static T GetValue<T>(
            this HttpSessionStateBase session, 
            string key) 
            => session.GetValue<T>(key, default(T));

        public static T GetValue<T>(
            this HttpSessionStateBase session, 
            string key, 
            T defaultValue) 
            => session[key] != null 
                ? (T) Convert.ChangeType(session[key], typeof (T)) 
                : defaultValue;
    }
}