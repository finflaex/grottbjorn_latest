#region

using System.Web.Mvc;

#endregion

namespace Finflaex.MVC
{
    public class fxcDefaultController : Controller
    {
        public fxcDefaultController()
        {
            ActionInvoker = new fxcActionInvoker();
        }
    }
}