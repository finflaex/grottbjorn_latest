﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Finflaex.Reflection;

namespace Finflaex.MVC
{
    public class fxcBinaryResult : ActionResult
    {
        public fxcBinaryResult(
            Stream stream,
            fxeHttpContentType contentType)
        {
            ContentType = contentType;
            this.stream = stream;
        }

        public fxcBinaryResult(
            byte[] bytes,
            fxeHttpContentType contentType)
            : this(new MemoryStream(bytes), contentType)
        {
        }

        public Stream stream { get; }
        public fxeHttpContentType ContentType { get; }

        public override void ExecuteResult(
            ControllerContext context)
        {
            context
                .IfNull(read =>
                {
                    throw new ArgumentNullException("context = null");
                })
                .HttpContext
                .Response
                .IfNotNull(response =>
                {
                    response.ContentType = ContentType.Desc();

                    var buffer = new byte[4096];
                    while (true)
                    {
                        var read = stream.Read(buffer, 0, buffer.Length);
                        if (read == 0) break;
                        response.OutputStream.Write(buffer, 0, read);
                    }

                    response.End();
                });
        }
    }
}
