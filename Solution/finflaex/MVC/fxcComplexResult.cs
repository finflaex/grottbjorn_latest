﻿#region

using System;
using System.Web.Mvc;

#endregion

namespace Finflaex.MVC
{
    /*
        Наш route должен стоять перед default
        routes.MapRoute("Result", "{controller}/{action}.{result}/{id}",
        new { controller = "Home", action = "Index", id = "", result = "view" });
    */

    /// <summary>
    ///     https://habrahabr.ru/post/66355/
    /// </summary>
    public class fxcComplexResult : ActionResult
    {
        public Func<JsonResult> Json { get; set; }

        public Func<ViewResult> View { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            object value;
            if (context.RouteData.Values.TryGetValue("result", out value))
            {
                var result = value.ToString();
                if (result.Equals("json", StringComparison.InvariantCultureIgnoreCase))
                {
                    Json().ExecuteResult(context);
                    return;
                }
            }

            View().ExecuteResult(context);
        }
    }
}