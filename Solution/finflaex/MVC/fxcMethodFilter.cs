﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using Finflaex.Reflection;

namespace Finflaex.MVC
{
    public class fxcMethodFilter : ActionFilterAttribute
    {
        private readonly Func<string[], bool> _role_intersec;

        public fxcMethodFilter(Func<string[], bool> role_intersec)
        {
            _role_intersec = role_intersec;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext
                .ActionDescriptor
                .ControllerDescriptor
                .ControllerType
                .GetMethods()
                .Select(v => new
                {
                    method = v,
                    attr = (atrMethodAttribute)GetCustomAttribute(v, typeof(atrMethodAttribute))
                })
                .Where(v => v.attr != null && v.attr.Name == filterContext.ActionDescriptor.ActionName)
                .OrderBy(v => v.attr.Order)
                .FirstOrDefault(v => _role_intersec(v.attr.Role.Split(',')))
                .IfNotNull(v =>
                {
                    var controller = filterContext
                        .ActionDescriptor
                        .ControllerDescriptor;
                    var method = controller
                        .FindAction(filterContext.Controller.ControllerContext, v.method.Name);
                    try
                    {
                        if (filterContext.HttpContext.Request.HttpMethod.ToLower() == "get")
                        {
                            var data = filterContext.HttpContext.Request.QueryString.ToDictionary<string, object>();
                            filterContext.Result = method
                                .Execute(filterContext.Controller.ControllerContext, data)
                                as ActionResult;
                        }
                        else
                        {
                            filterContext.Result = method
                                .Execute(filterContext.Controller.ControllerContext, filterContext.ActionParameters)
                                as ActionResult;
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary()
                        {
                            {"controller", controller.ControllerName},
                            {"action", method.ActionName}
                        });
                    }
                });
        }
    }
}
