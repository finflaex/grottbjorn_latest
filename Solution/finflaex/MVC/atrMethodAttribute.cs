using System;
using System.Web.Mvc;

namespace Finflaex.MVC
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class atrMethodAttribute : FilterAttribute
    {
        public atrMethodAttribute(string name, string role, int order = 0)
        {
            Name = name;
            Role = role;
            Order = order;
        }

        public string Name { get; }
        public string Role { get; }
    }
}