﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Reflection;
using ServiceStack.Auth;

namespace Finflaex.Certificate
{
    public static class fxsCertificate
    {
        public static X509Certificate2[] List
        {
            get
            {
                var storeNames = Enum
                    .GetValues(typeof (StoreName))
                    .Cast<StoreName>();

                return Enum
                    .GetValues(typeof (StoreLocation))
                    .Cast<StoreLocation>()
                    .SelectMany(location => storeNames.Select(name => new X509Store(name, location)))
                    .SelectMany(store =>
                    {
                        store.Open(OpenFlags.MaxAllowed);
                        return store.Certificates.Cast<X509Certificate2>();
                    })
                    .ToArray();
            }
        }

        public static X509Certificate2 FindBySerial(string value)
        {
            value = value.ToUpper().Replace(" ", string.Empty);
            return Find(X509FindType.FindBySerialNumber, value);
        }

        public static X509Certificate2 Find(X509FindType type, object value)
        {
            var storeNames = Enum
                .GetValues(typeof (StoreName))
                .Cast<StoreName>();

            return Enum
                .GetValues(typeof (StoreLocation))
                .Cast<StoreLocation>()
                .SelectMany(location => storeNames.Select(name => new X509Store(name, location)))
                .SelectMany(store =>
                {
                    store.Open(OpenFlags.MaxAllowed);
                    return store.Certificates.Cast<X509Certificate2>();
                })
                .FirstOrDefault(cert =>
                {
                    switch (type)
                    {
                        //case X509FindType.FindByThumbprint:
                        //    break;
                        //case X509FindType.FindBySubjectName:
                        //    break;
                        //case X509FindType.FindBySubjectDistinguishedName:
                        //    break;
                        //case X509FindType.FindByIssuerName:
                        //    break;
                        //case X509FindType.FindByIssuerDistinguishedName:
                        //    break;
                        case X509FindType.FindBySerialNumber:
                            return cert.SerialNumber == value.AsType<string>();
                        //case X509FindType.FindByTimeValid:
                        //    break;
                        //case X509FindType.FindByTimeNotYetValid:
                        //    break;
                        //case X509FindType.FindByTimeExpired:
                        //    break;
                        //case X509FindType.FindByTemplateName:
                        //    break;
                        //case X509FindType.FindByApplicationPolicy:
                        //    break;
                        //case X509FindType.FindByCertificatePolicy:
                        //    break;
                        //case X509FindType.FindByExtension:
                        //    break;
                        //case X509FindType.FindByKeyUsage:
                        //    break;
                        //case X509FindType.FindBySubjectKeyIdentifier:
                        //    break;
                        default:
                            throw new ArgumentException("unknownt X509FindType");
                    }
                });
        }

        public static X509Certificate2 Load(string path)
            => Load(File.ReadAllBytes(path));

        public static X509Certificate2 Load(byte[] raw)
            => new X509Certificate2(raw);

        public static byte[] Sign(X509Certificate2 cert, byte[] document, Oid algoritm = null)
        {
            ContentInfo contentInfo = new ContentInfo(document);
            SignedCms signedCms = new SignedCms(contentInfo);
            CmsSigner cmsSigner = new CmsSigner(cert);
            if (algoritm != null)
            {
                cmsSigner.DigestAlgorithm = algoritm;
            }

            signedCms.ComputeSignature(cmsSigner);
            return signedCms.Encode();
        }

        public static byte[] CertSign(this byte[] @this,
            X509Certificate2 certificate,
            Oid algoritm = null)
            => Sign(certificate, @this, algoritm);

        public static bool Verify(
            X509Certificate2 cert, 
            byte[] document)
        {
            ContentInfo contentInfo = new ContentInfo(document);
            SignedCms signedCms = new SignedCms(contentInfo, true);
            try
            {
                signedCms.Decode(document);
                signedCms.CheckSignature(true);
            }
            catch (CryptographicException e)
            {
                return false;
            }
            return true;
        }

        public static byte[] UnSign(
            byte[] document)
        {
            SignedCms signedCms = new SignedCms();
            try
            {
                signedCms.Decode(document);
                return signedCms.ContentInfo.Content;
            }
            catch (CryptographicException e)
            {
                return null;
            }
        }

        public static byte[] CertUnSign(this byte[] @this) 
            => UnSign(@this);
    }
}
