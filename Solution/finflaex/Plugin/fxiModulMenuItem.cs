﻿#region

using System.Collections.Generic;

#endregion

namespace Finflaex.Plugin
{
    public interface fxiModulMenuItem
    {
        fxiModulMenuItem[] Submenu { get; }
        string Caption { get; }
        object Action { get; }
        string Icon { get; }
    }
}