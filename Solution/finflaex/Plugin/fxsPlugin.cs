﻿#region

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Finflaex.Log;
using Finflaex.Reflection;
using Finflaex.Self;
using Microsoft.CSharp;

#endregion

namespace Finflaex.Plugin
{
    public static class fxsPlugin
    {
        public static T[] LoadPlugins<T>(params string[] catalog)
        {
            try
            {
                return catalog
                    .AddRet(Directory.GetCurrentDirectory())
                    .SelectMany(directory =>
                    {
                        if (!Directory.Exists(directory))
                        {
                            fxsLog.Write(fxeLog.UserInfo, $"неверный каталог для загрузки плагинов: {directory}");
                            return new T[0];
                        }
                        return directory
                            .SelectSingle(Directory.GetFiles)
                            .AsParallel()
                            .SelectMany(path =>
                            {
                                var ext = Path.GetExtension(path)?.Remove(0, 1);
                                switch (ext)
                                {
                                    case "cs":
                                        return new[] {IsMacros(path)};
                                    case "dll":
                                        return IsPlugin(path);
                                    default:
                                        return new object[0];
                                }
                            })
                            .OfType<T>()
                            .ToArray();
                    })
                    .ToArray();
            }
            catch (Exception error)
            {
                fxsLog.Write(fxeLog.Error, MethodBase.GetCurrentMethod(), error);
            }
            return new T[0];
        }

        public static List<object> LoadMacros(string mask = "*.*", params string[] catalog)
        {
            var result = new List<object>();
            try
            {
                catalog
                    .ToList()
                    .AddRet(Directory.GetCurrentDirectory())
                    .ForEach(directory =>
                    {
                        if (!Directory.Exists(directory))
                        {
                            fxsLog.Write(fxeLog.UserInfo, $"неверный каталог для загрузки плагинов: {directory}");
                            return;
                        }
                        directory
                            .SelectSingle(dir => Directory.GetFiles(dir, mask))
                            .AsParallel()
                            .Select(IsMacros)
                            .WhereNotNull()
                            .ForEach(result.Add);
                    });
            }
            catch (Exception error)
            {
                fxsLog.Write(fxeLog.Error, MethodBase.GetCurrentMethod(), error);
            }
            return result;
        }

        private static fxiPlugin[] IsPlugin(byte[] file)
        {
            try
            {
                var pin = typeof (fxiPlugin).FullName;
                return Assembly
                    .Load(file)
                    .GetTypes()
                    .AsParallel()
                    .Where(v => v.GetInterface(pin) != null)
                    .Select(Activator.CreateInstance)
                    .Cast<fxiPlugin>()
                    .WhereNotNull()
                    .ToArray();
            }
            catch (Exception error)
            {
                fxsLog.Write(fxeLog.Error, MethodBase.GetCurrentMethod(), error);
                return null;
            }
        }

        private static fxiPlugin[] IsPlugin(string file_url)
        {
            try
            {
                return IsPlugin(File.ReadAllBytes(file_url));
            }
            catch (Exception error)
            {
                fxsLog.Write(fxeLog.Error, MethodBase.GetCurrentMethod(), error);
                return null;
            }
        }

        private static object IsMacros(string file_url)
        {
            try
            {
                var read = File.ReadAllLines(file_url);
                if (read[0].StartsWith("//"))
                {
                    var typename = read[0].Remove(0, 2);
                    var code = new StringBuilder();
                    read.Skip(1).ForEach(line => code.AppendLine(line));
                    return readMacros(typename, code.ToString());
                }
                else
                {
                    var typename = Path.GetFileNameWithoutExtension(file_url);
                    var code = new StringBuilder();
                    read.ForEach(line => code.AppendLine(line));
                    return readMacros(typename, code.ToString());
                }
            }
            catch (Exception error)
            {
                fxsLog.Write(fxeLog.Error, MethodBase.GetCurrentMethod(), error);
                return null;
            }
        }

        public static object readMacros(string typename, string code, string[] assemblies = null)
        {
            if (code == null) throw new ArgumentNullException();
            var codeCompiler = new CSharpCodeProvider(new Dictionary<string, string> {{"CompilerVersion", "v4.0"}});
            var parameters = new CompilerParameters(assemblies ?? new string[0]);
            var path = Assembly.GetExecutingAssembly().Location;
            parameters.ReferencedAssemblies.Add(path);
            var results = codeCompiler.CompileAssemblyFromSource(parameters, code);
            if (!results.Errors.HasErrors)
            {
                return results.CompiledAssembly.CreateInstance(typename);
            }
            var errlist = results.Errors.Cast<object>().Select(v => v.ToString()).ToArray();
            throw new ArgumentException("Ошибки при компиляции => " + string.Join(";", errlist));
        }
    }
}