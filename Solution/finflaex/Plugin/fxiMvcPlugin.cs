﻿namespace Finflaex.Plugin
{
    public interface fxiMvcPlugin : fxiPlugin
    {
        string HeadStyle { get; }
        string BodyStyle { get; }
        string BodyScript { get; }
        string Template { get; }
        string HeadScript { get; }
        string AfterContentScript { get; }
        string BeforeContentScript { get; }
        string Body { get; set; }

        string GetContent(params object[] model);
    }
}