﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace Finflaex.Plugin
{
    public class fxaMvcModul : fxiMvcModul
    {
        public virtual DateTime? DataCreate => null;

        public virtual string Name => null;

        public virtual string Version => null;

        public virtual IEnumerable<fxiModulMenuItem> Menu => null;

        public virtual int Prioritet => 0;

        public virtual object Execute(object data) => null;
    }
}