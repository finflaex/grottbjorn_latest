﻿#region

using System;

#endregion

namespace Finflaex.Plugin
{
    public interface fxiPlugin
    {
        string Name { get; }
        string Version { get; }
        DateTime? DataCreate { get; }
        int Prioritet { get; }
    }
}