﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Plugin
{
    public class fxaModulMenuItem : fxiModulMenuItem
    {
        public object Action { get; set; }

        public string Caption { get; set; }

        public string Icon { get; set; }

        public fxiModulMenuItem[] Submenu { get; set; }
    }
}
