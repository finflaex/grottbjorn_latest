﻿#region

using System.Collections.Generic;

#endregion

namespace Finflaex.Plugin
{
    public interface fxiMvcModul : fxiPlugin
    {
        IEnumerable<fxiModulMenuItem> Menu { get; }
        object Execute(object data);
    }
}