﻿#region

using System;
using System.Linq;
using Finflaex.Reflection;

#endregion

namespace Finflaex.Plugin
{
    public class fxaMvcPlugin : fxiMvcPlugin
    {
        public virtual string Name => null;

        public virtual string Version => null;

        public virtual DateTime? DataCreate => null;

        public virtual string AfterContentScript => null;

        public virtual string BeforeContentScript => null;

        public virtual string BodyStyle => null;

        public virtual string HeadScript => null;

        public virtual string HeadStyle => null;

        public virtual string Template => null;

        public virtual string Body { get; set; }

        public virtual string BodyScript => null;

        public virtual int Prioritet => 0;

        public virtual string GetContent(params object[] param)
        {
            return GetType()
                .GetProperties()
                .Where(v => v.PropertyType == typeof (string))
                .SelectMany(v => new[]
                {
                    new
                    {
                        key = $"(<!--{v.Name}-->|/\\*{v.Name}\\*/)",
                        value = v.GetValue(this).AsType<string>()
                    }
                })
                .Where(v => v.value != null)
                .ToDictionary(v => v.key, v => v.value)
                .SelectSingle(Template.Replace);
        }
    }
}