﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Html
{
    public static class fxsHtml
    {
        public static string Get(string url)
        {
            try
            {
                HttpWebRequest myRequest = (HttpWebRequest) WebRequest.Create(url);
                myRequest.Method = "GET";
                WebResponse myResponse = myRequest.GetResponse();
                StreamReader sr = new StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.UTF8);
                string result = sr.ReadToEnd();
                sr.Close();
                myResponse.Close();

                return result;
            }
            catch (Exception err)
            {
                return null;
            }
        }
    }
}
