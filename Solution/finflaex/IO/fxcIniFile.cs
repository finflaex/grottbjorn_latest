﻿using System.Text;
using Finflaex.WinApi;

namespace Finflaex.IO
{
    public class fxcIniFile
    {
        private readonly string _path;

        public fxcIniFile(string path)
        {
            _path = path;
        }

        public void Write(
            string section,
            string key,
            string value)
        {
            kernel32.WritePrivateProfileString(section, key, value, _path);
        }

        public string Read(
            string section,
            string key)
        {
            var temp = new StringBuilder(255);
            kernel32.GetPrivateProfileString(section, key, string.Empty, temp, 255, _path);
            return temp.ToString();
        }
    }
}