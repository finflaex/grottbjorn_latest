﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Finflaex.Log;
using Finflaex.Reflection;
using Finflaex.Self;
using Finflaex.Types;
using Timer = System.Timers.Timer;

#endregion

namespace Finflaex.IO
{
    public class fxcFileAsync
    {
        private readonly Queue<string> stack = new Queue<string>();

        private readonly Timer timer = new Timer
        {
            AutoReset = true,
            Enabled = true,
            Interval = 10
        };

        private readonly bool append;
        private readonly int buffer_size;
        private bool flag;

        public fxcFileAsync()
        {
            timer.Elapsed += (sender, args) =>
            {
                if (File == null || flag || !(flag = stack.Count > 0)) return;
                stack.Count.For(index => File.Write(stack.Dequeue()));
                flag = false;
            };
        }

        ~fxcFileAsync()
        {
            while (flag) Thread.Sleep(0);
            if (File == null) return;
            try
            {
                stack.Count.For(index => File.Write(stack.Dequeue()));
            }
            catch { }
        }

        /// <summary>
        /// </summary>
        /// <param name="path">
        ///     между строковыми элементами вставляется обратный слеш,
        ///     кроме последнего элемента с точкой в начале.
        ///     принимает Encoding || utf8, true - добавление, false - перезапись, int -размер буфера
        /// </param>
        public fxcFileAsync(params object[] param)
            : this()
        {
            var path = param.OfType<string>().ToArray();
            Encoding = param.OfType<Encoding>().FirstOrDefault() ?? Encoding.UTF8;
            append = param.OfType<bool>().FirstOrDefault();
            buffer_size = path.OfType<int>().FirstOrDefault().IsNoll(() => 4096);

            Path = path.Length > 1
                ? (
                    path[path.Length - 1].StartsWith(".")
                        ? path.Take(path.Length - 2).Join("\\")
                        : path.Join("\\")
                ) 
                : path.Length == 1
                ? path[0]
                : string.Empty;

            var dir = Path
                .GetDirectoryNameEx()
                .IsNullOrWhiteSpace(() => fxsMain.ExecutablePath.GetDirectoryNameEx());
            var name = Path
                .GetFileNameWithoutExtension()
                .IsNullOrWhiteSpace(()
                    => DateTime.Now.ToString("yyyy_MM_dd")
                       + (append ? string.Empty : "_" + DateTime.Now.ToString("HH_mm_ss")));
            var ext = Path
                .GetFileExtension()
                .IsNullOrWhiteSpace(() => "txt");

            Directory
                .CreateDirectory(dir);

            Path = $@"{dir}\{name}.{ext}";

            File = new StreamWriter(Path, append, Encoding, buffer_size)
            {
                AutoFlush = true,
            };
        }

        public Encoding Encoding { get; }

        public StreamWriter File { get; }

        public string Path { get; }

        public void Write(string value)
        {
            stack.Enqueue(value);
        }

        public void WriteLine(string value)
        {
            stack.Enqueue($"{value}\r\n");
        }
    }
}