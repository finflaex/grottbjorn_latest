﻿#region

using System.Collections.Generic;
using System.IO;
using System.Media;

#endregion

namespace Finflaex.IO
{
    public class fxcFileAudioPlayer
    {
        private readonly Dictionary<string, byte[]> _streams = new Dictionary<string, byte[]>();
        private readonly SoundPlayer player = new SoundPlayer();
        private readonly object sync = new object();

        private string _nowPlaying;

        public void Play(string file)
        {
            byte[] bytes;
            if (!_streams.TryGetValue(file, out bytes))
            {
                bytes = File.ReadAllBytes(file);
                _streams.Add(file, bytes);
            }

            lock (sync)
            {
                _nowPlaying = file;
                player.Stream = new MemoryStream(bytes);
                player.Play();
            }
        }

        public void Stop(string file)
        {
            lock (sync)
            {
                if (_nowPlaying == file)
                {
                    player.Stop();
                }
            }
        }
    }
}