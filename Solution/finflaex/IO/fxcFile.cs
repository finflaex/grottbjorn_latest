﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Finflaex.Log;
using Finflaex.Reflection;
using Finflaex.Self;

#endregion

namespace Finflaex.IO
{
    public class fxcFile<TThis>
        where TThis : fxcFile<TThis>
    {
        private readonly string fullPath;

        /// <summary>
        /// </summary>
        /// <param name="root"></param>
        /// <param name="param">принимает строки как части пути в указанной последовательности и FileMode</param>
        public fxcFile(string root = null, params object[] param)
        {
            fullPath = new LinkedList<string>()
                .AddRet(root ?? fxsMain.ExecutablePath)
                .AddRangeRet(param.OfType<string>())
                .Join('\\');
            Mode = param
                .OfType<FileMode>()
                .AddRet(FileMode.Create)
                .First();
            Directory
                .CreateDirectory(Catalog);
        }

        public string Catalog => fullPath.GetDirectoryNameEx();
        public string Name => fullPath.GetFileNameWithoutExtension();
        public string Ext => fullPath.GetFileExtension();
        public FileMode Mode { get; }

        public TThis Create(bool replace = false)
        {
            try
            {
                var exists = File.Exists(fullPath);
                if (exists && replace)
                {
                    File.Delete(fullPath);
                    File.Create(fullPath);
                }
                else if (!exists)
                {
                    File.Create(fullPath);
                }
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.Error,
                    typeof (TThis),
                    MethodBase.GetCurrentMethod(),
                    error,
                    new Dictionary<string, object>
                    {
                        ["path"] = fullPath
                    });
            }
            return this.AsType<TThis>();
        }

        public TThis Write(string data)
        {
            Task.Run(() =>
            {
                start:
                try
                {
                    using (var writer = new StreamWriter(File.Open(fullPath, Mode)))
                    {
                        writer.BaseStream.Seek(0, SeekOrigin.End);
                        writer.Write(data);
                    }
                }
                catch (IOException)
                {
                    Thread.Sleep(10);
                    goto start;
                }
                catch (Exception error)
                {
                    fxsLog.Write(
                        fxeLog.Error,
                        typeof (TThis),
                        MethodBase.GetCurrentMethod(),
                        error,
                        "Запись в файл не произведена",
                        new Dictionary<string, object>
                        {
                            ["data"] = data
                        });
                }
            });
            return this.AsType<TThis>();
        }

        public string ReadAllText()
        {
            try
            {
                return File.ReadAllText(fullPath);
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.Error,
                    typeof (TThis),
                    MethodBase.GetCurrentMethod(),
                    error,
                    "Файл не прочитан");
                return null;
            }
        }
    }
}