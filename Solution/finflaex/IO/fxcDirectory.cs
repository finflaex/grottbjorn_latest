﻿#region

using System;
using System.IO;
using System.Reflection;
using Finflaex.Log;
using Finflaex.Reflection;

#endregion

namespace Finflaex.IO
{
    public class fxcDirectory
    {
        private readonly string directory;

        public fxcDirectory(string path)
        {
            directory = path;
        }

        public bool Copy(
            string destination) 
            => Copy(directory, destination);

        public static bool Copy(
            string source, 
            string destination)
        {
            try
            {
                var files = ListFiles(source);
                foreach (var old_path in files)
                {
                    var new_path = old_path.Replace(source, destination);
                    Directory.CreateDirectory(new_path.GetDirectoryNameEx());
                    File.Copy(old_path, new_path);
                }
                return true;
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.Error,
                    typeof (fxcDirectory),
                    MethodBase.GetCurrentMethod(),
                    error);
                return false;
            }
        }

        public string[] ListFiles(
            SearchOption option = SearchOption.AllDirectories,
            string mask = "*.*")
            => ListFiles(directory, option, mask);

        public static string[] ListFiles(
            string path,
            SearchOption option = SearchOption.AllDirectories,
            string mask = "*.*")
        {
            return Directory.GetFiles(path, mask, option);
        }
    }
}