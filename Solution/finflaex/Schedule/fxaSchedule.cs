﻿#region

using System.Collections.Generic;
using System.ComponentModel.Composition;
using Finflaex.MEF;
using Finflaex.Reflection;
using Quartz;
using Quartz.Impl;

#endregion

namespace Finflaex.Schedule
{
    public class fxaSchedule
    {
        public fxaSchedule()
        {
            this.UpdateMef();
        }

        [ImportMany(typeof (IScheduleJob))]
        public IEnumerable<IScheduleJob> Schedules { get; set; }

        public void ScheduleStart()
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            Schedules.ForEach(v =>
            {
                var job = JobBuilder
                    .Create(v.GetType())
                    .WithIdentity(v.Key)
                    .Build();
                var trigger = TriggerBuilder
                    .Create()
                    .WithIdentity(v.Key);
                if (v.Starter == null)
                {
                    trigger.StartNow();
                }
                else
                {
                    trigger.StartAt(v.Starter.Value);
                }

                trigger.WithSimpleSchedule(t =>
                {
                    t.WithInterval(v.Interval);
                    if (v.Repeat == null)
                    {
                        t.WithRepeatCount(v.Repeat ?? 0);
                    }
                    else
                    {
                        t.RepeatForever();
                    }
                });
                var trigger_build = trigger.Build();
                scheduler.ScheduleJob(job, trigger_build);
            });
        }
    }
}