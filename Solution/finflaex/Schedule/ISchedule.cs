﻿using System;
using Quartz;

namespace Finflaex.Schedule
{
    public interface IScheduleJob : IJob
    {
        string Key { get; set; }

        /// <summary>
        /// nul = now
        /// </summary>
        DateTimeOffset? Starter { get; set; }

        TimeSpan Interval { get; set; }

        /// <summary>
        /// null = forever
        /// </summary>
        int? Repeat { get; set; }
    }
}
