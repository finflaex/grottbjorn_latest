﻿using System;
using System.Reflection;
using System.Web.Mvc;
using Finflaex.Log;
using Finflaex.Reflection;
using Microsoft.Win32;
using NUnit.Framework;

namespace Finflaex.Reestr
{
    [TestFixture]
    public static class fxsRegistry
    {
        public static string Folder => "finflaex";

        public static RegistryKey folder => Registry
            .LocalMachine?
            .OpenSubKey("SOFTWARE", true)?
            .CreateSubKey(Folder, true);

        public static void WriteConfig<TVal>(string key, TVal value)
        {
            try
            {
                if (folder == null)
                {
                    throw new Exception($"проблема открыть реестр по пути 'HKEY_CURRENT_USER\\{Folder}'");
                }
                if (typeof (TVal) == typeof (string))
                {
                    folder.SetValue(key, value);
                }
                else
                {
                    folder.SetValue(key, value.ToJson());
                }
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    typeof (fxsRegistry),
                    MethodBase.GetCurrentMethod(),
                    error);
            }
        }

        [Test]
        public static void Test_Registry()
        {
            var qwe = "test";
            try
            {
                fxsRegistry.WriteConfig("test", qwe);
            }
            catch (Exception error)
            {
                Assert.Fail($"записал -> {error.Message}");
            }

            var read1 = fxsRegistry.ReadConfig<string>("test");
            fxsRegistry.RemoveConfig("test");
            var read2 = fxsRegistry.ReadConfig<string>("test");

            Assert.AreEqual(read1?.GetType(), qwe.GetType(), "прочитал - типы не совпадают");
            Assert.AreEqual(read1, "test", "прочитал - значения не совпадают");
            Assert.AreNotEqual(read2?.GetType(), qwe.GetType(), "удалил - типы совпадают");
            Assert.AreNotEqual(read2, "test", "удалил - значения совпадают");
        }

        public static TVal ReadConfig<TVal>(string key)
        {
            try
            {
                if (folder == null)
                {
                    throw new Exception($"проблема открыть реестр по пути 'HKEY_CURRENT_USER\\{Folder}'");
                }
                return 
                    typeof (TVal) == typeof (string) 
                    ? folder.GetValue(key).AsType<TVal>() 
                    : folder.GetValue(key).AsType<string>().FromJson<TVal>();
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    typeof(fxsRegistry),
                    MethodBase.GetCurrentMethod(),
                    error);
                return default(TVal);
            }
        }

        public static void RemoveConfig(string key)
        {
            try
            {
                if (folder == null)
                {
                    throw new Exception($"проблема открыть реестр по пути 'HKEY_CURRENT_USER\\{Folder}'");
                }
                folder.DeleteValue(key);
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    typeof(fxsRegistry),
                    MethodBase.GetCurrentMethod(),
                    error);
            }
        }
    }
}
