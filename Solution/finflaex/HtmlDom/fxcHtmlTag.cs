﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finflaex.Reflection;
using Finflaex.Types;

#endregion

namespace Finflaex.HtmlDom
{
    public class fxcHtmlTag
    {
        public fxcDictSO Attributes = new fxcDictSO();
        public List<object> ContentList;
        public StringBuilder Scripts = new StringBuilder();

        private readonly fxeHtmlTag tag;

        /// <summary>
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="param">string or fxcHtmltag</param>
        public fxcHtmlTag(fxeHtmlTag tag, params object[] param)
        {
            this.tag = tag;
            ContentList = param.ToList();
            Id = Guid.NewGuid().ToString("N");
        }

        public string Id { get; }

        #region WRAP

        public fxcHtmlTag WrapDiv
            => Div().AddContent(this);

        public fxcHtmlTag WrapTHead
            => THead().AddContent(this);

        public fxcHtmlTag WrapCaption
            => Caption().AddContent(this);

        #endregion

        #region ATTRIBUTE

        public fxcHtmlTag Style(string value) 
            => this.AddAttribute("style", value);

        public fxcHtmlTag Style(CSS values) => values
            .Select(v => $"{v.Key}:{v.Value.ToString()}")
            .Join(";")
            .Action(Style);

        public fxcHtmlTag Class(params string[] values)
            => this.AddClass(values);

        public fxcHtmlTag Content(params object[] data)
            => this.AddContent(data);

        #endregion

        public string this[string key]
        {
            get { return Attributes.FirstOrDefault(v => v.Key == key).ToString(); }
            set { Attributes[key] = value; }
        }

        public static implicit operator string(fxcHtmlTag tag) => tag.ToString();

        public override string ToString()
        {
            Attributes["id"] = Id;

            var result = new StringBuilder();
            result.Append($"<{tag}");

            Attributes
                .Select(v =>
                {
                    if (v.Value == null)
                        return $"{v.Key}";
                    if (v.Value is bool)
                        return $"{v.Key}={v.Value}";
                    return $"{v.Key}=\"{v.Value}\"";
                })
                .Join(" ")
                .IsNotNullOrWhiteSpace(v =>
                {
                    var item = v.Prepend(" ");
                    result.Append(item);
                });

            result.Append($">");

            //content
            //    .IsNotNullOrEmpty(v =>
            //    {
            //        result.Append(v);
            //    });

            var inner = ContentList
                .Select(v => v.ToString())
                .Join("")
                .IsNotNullOrWhiteSpace(v => { result.Append(v); })
                ;

            result.Append($"</{tag}>");

            Scripts
                .ToString()
                .IsNotNullOrWhiteSpace(script =>
                {
                    script
                        .ToHtmlTag(fxeHtmlTag.script)
                        .IfNotNull(v => { result.Append(v); });
                });

            return result.ToString();
        }

        #region CREATE

        public static fxcHtmlTag H3(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.h3, data);

        public static fxcHtmlTag H2(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.h2, data);

        public static fxcHtmlTag H4(params object[] data)
    => new fxcHtmlTag(fxeHtmlTag.h4, data);

        public static fxcHtmlTag Div(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.div, data);

        public static fxcHtmlTag Table(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.table, data);

        public static fxcHtmlTag Caption(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.caption, data);

        public static fxcHtmlTag THead(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.thead, data);

        public static fxcHtmlTag TBody(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.tbody, data);

        public static fxcHtmlTag TH(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.th, data);

        public static fxcHtmlTag TR(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.tr, data);

        public static fxcHtmlTag TD(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.td, data);

        public static fxcHtmlTag LI(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.li, data);

        public static fxcHtmlTag P(params object[] data)
            => new fxcHtmlTag(fxeHtmlTag.p, data);

        #endregion
    }

    public static class fxrHtmlTag
    {
        public static fxcHtmlTag ToHtmlTag(
            this string @this,
            fxeHtmlTag tag)
        {
            return new fxcHtmlTag(tag, @this);
        }

        public static T AddAttribute<T>(
            this T @this,
            string key,
            object value)
            where T : fxcHtmlTag
        {
            @this.Attributes.Add(key, value);
            return @this;
        }

        public static T AddClass<T>(
            this T @this,
            params string[] classes)
            where T : fxcHtmlTag
        {
            var read = @this
                .Attributes["class"]?
                .AsType<string>() ?? string.Empty;

            @this.Attributes["class"] = read
                .Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                .AddRangeRet(classes)
                .Join(" ");

            return @this;
        }

        public static T AddContent<T>(
            this T @this,
            params object[] content)
            where T : fxcHtmlTag
        {
            @this.ContentList.AddRange(content);
            return @this;
        }

        public static T OnClick<T>(
            this T @this,
            string script)
            where T : fxcHtmlTag
        {
            @this
                .Scripts
                .Append($"$(\"#{@this.Id}\").click(function(e){{{script}}})");
            return @this;
        }
    }

    public class CSS : fxcDictSO
    {
        public fxeHorizontalAlign TextAlign
        {
            get { return this["text-align"].AsType<string>().ToEnum<fxeHorizontalAlign>(); }
            set { this["text-align"] = value.ToString(); }
        }

        public string MinWidth
        {
            get { return this["min-width"].AsType<string>(); }
            set { this["min-width"] = value; }
        }

        public string MarginLeft
        {
            get { return this["margin-left"].AsType<string>(); }
            set { this["margin-left"] = value; }
        }

        public fxeFloat Float
        {
            get { return this["float"].AsType<string>().ToEnum<fxeFloat>(); }
            set { this["float"] = value.ToString(); }
        }
    }

    public enum fxeHorizontalAlign
    {
        right,
        left,
    }

    public enum fxeFloat
    {
        left,
        right,
        none
    }
}