﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.HtmlDom
{
    public enum fxeHtmlTag
    {
        a,
        div,
        script,
        li,
        i,
        span,
        input,
        table,
        thead,
        tbody,
        tr,
        th,
        td,
        h3,
        caption,
        h2,
        h4,
        p
    }
}
