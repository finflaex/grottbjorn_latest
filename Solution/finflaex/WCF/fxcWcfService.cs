﻿#region

using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using Finflaex.Lan;
using Finflaex.Reflection;

#endregion

namespace Finflaex.WCF
{
    public interface IWcfService
    {
    }

    public abstract class fxcWcfService<TType, TInterface>
        where TType : class
    {
        private readonly ServiceHost host;
        private readonly Uri uri_base;
        private readonly Uri uri_concret;
        private bool work;

        public fxcWcfService(string name, int port = 56789)
        {
            uri_base = new Uri($"http://{fxsIPv4.IPv4}:{port}");
            uri_concret = new Uri($"http://{fxsIPv4.IPv4}:{port}/{name}");

            host = new ServiceHost(typeof(TType), uri_concret);

            // почему то не работает
            //host.AddServiceEndpoint(typeof (TInterface), new BasicHttpBinding
            //{
            //    MaxReceivedMessageSize = int.MaxValue
            //}, uri_concret);

            ServiceMetadataBehavior smb = new ServiceMetadataBehavior
            {
                HttpGetEnabled = true,
                MetadataExporter = {PolicyVersion = PolicyVersion.Policy15},
            };
            host.Description.Behaviors.Add(smb);

            work = false;
        }

        public bool Work
        {
            get { return work; }
            set
            {
                if (work == value) return;
                if (work = value) host.Open();
                else host.Close();
            }
        }

        public TType DoWork(Action<bool> act)
        {
            act.Invoke(Work);
            return this.AsType<TType>();
        }

        public TType DoWork(Func<bool> act)
        {
            Work = act.Invoke();
            return this.AsType<TType>();
        }

        public TType DoWork(Func<bool, bool> act)
        {
            Work = act.Invoke(Work);
            return this.AsType<TType>();
        }

        ~fxcWcfService()
        {
            Work = false;
        }
    }
}