﻿#region

using System;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace Finflaex.WinApi
{
    public static class user32
    {
        public const int SW_HIDE = 0;
        public const int SW_SHOW = 5;

        public const string VistaStartMenuCaption = "Start";
        public static uint vistaStartMenuWnd = 0;

        [DllImport("user32.dll")]
        public static extern int GetWindowText(
            uint hWnd, 
            StringBuilder text, 
            int count);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool EnumThreadWindows(
            int threadId, 
            Func<uint, uint, bool> pfnEnum, 
            uint lParam);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint FindWindow(
            string lpClassName, 
            string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint FindWindowEx(
            uint parentHandle, 
            uint childAfter, 
            string className,
            string windowTitle);

        [DllImport("user32.dll")]
        public static extern uint FindWindowEx(
            uint parentHwnd, 
            uint childAfterHwnd, 
            uint className,
            string windowText);

        [DllImport("user32.dll")]
        public static extern int ShowWindow(
            uint hwnd, 
            int nCmdShow);

        [DllImport("user32.dll")]
        public static extern uint GetWindowThreadProcessId(
            uint hwnd, 
            out int lpdwProcessId);
    }
}