﻿#region

using System;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace Finflaex.WinApi
{
    public static class kernel32
    {
        #region get executing path

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int GetModuleFileName(
            uint hModule, 
            StringBuilder buffer, 
            int length);

        #endregion

        #region ini file

        [DllImport("kernel32")]
        public static extern long WritePrivateProfileString(
            string section,
            string key,
            string val,
            string filePath);

        [DllImport("kernel32")]
        public static extern int GetPrivateProfileString(
            string section,
            string key, 
            string def, 
            StringBuilder retVal,
            int size, 
            string filePath);

        #endregion
    }
}