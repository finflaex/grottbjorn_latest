﻿#region

using System;
using System.Runtime.InteropServices;

#endregion

namespace Finflaex.WinApi
{
    public static class advapi32
    {
        [DllImport("advapi32.dll")]
        public static extern uint OpenSCManager(string lpMachineName, string lpSCDB, int scParameter);

        [DllImport("Advapi32.dll")]
        public static extern uint CreateService(uint SC_HANDLE, string lpSvcName, string lpDisplayName,
            int dwDesiredAccess, int dwServiceType, int dwStartType, int dwErrorControl, string lpPathName,
            string lpLoadOrderGroup, int lpdwTagId, string lpDependencies, string lpServiceStartName, string lpPassword);

        [DllImport("advapi32.dll")]
        public static extern void CloseServiceHandle(uint SCHANDLE);

        [DllImport("advapi32.dll")]
        public static extern int StartService(uint SVHANDLE, int dwNumServiceArgs, string lpServiceArgVectors);

        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern uint OpenService(uint SCHANDLE, string lpSvcName, int dwNumServiceArgs);

        [DllImport("advapi32.dll")]
        public static extern int DeleteService(uint SVHANDLE);

        [DllImport("kernel32.dll")]
        public static extern int GetLastError();
    }
}