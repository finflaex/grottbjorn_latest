﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.MsSQL.Bases
{
    public class fxcMigration<T>
        : MigrateDatabaseToLatestVersion<T, fxcMigrationConfig<T>>
        where T : fxaDB<T>
    {
    }
}
