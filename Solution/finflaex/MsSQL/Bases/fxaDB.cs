﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Finflaex.Reflection;
using Finflaex.Self;

namespace Finflaex.MsSQL.Bases
{
    public abstract class fxaDB<T> : DbContext
        where T : fxaDB<T>
    {
        public static string CreateLocalConnectionString(string dbname, string directory = null)
        {
            var path = Path.Combine(directory ?? fxsMain.ExecutableDirectory, $"{dbname}.mdf");
            return $@"Data Source=(LocalDB)\MSSQLLocalDB;" +
                   $@"AttachDbFilename={path};" +
                   $@"Integrated Security=True;" +
                   $@"Database={dbname};";
        }

        public static string CreateServerLocalConnectionString(string dbname, string directory = null)
        {
            var dir = directory == null ? null : HostingEnvironment.MapPath(directory);
            return CreateLocalConnectionString(dbname, dir);
        }

        public static T db { get; set; }

        #region MIGRATION

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        #endregion

        #region SYSTEM

        public fxaDB(
            string connectionString, 
            IDatabaseInitializer<T> initializer = null)
            : base(connectionString)
        {
            try
            {
                var path = Path.Combine(fxsMain.ExecutableDirectory, "basa");
                Directory.CreateDirectory(path);
                AppDomain.CurrentDomain.SetData("DataDirectory", path);
            }
            finally
            {
                Database.SetInitializer(initializer ?? new fxcMigration<T>());
            }

            Configuration.LazyLoadingEnabled = true;

            db = this.AsType<T>();
        }

        #endregion
    }
}
