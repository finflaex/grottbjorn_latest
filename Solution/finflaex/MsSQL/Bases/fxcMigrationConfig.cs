﻿using System.Data.Entity.Migrations;

namespace Finflaex.MsSQL.Bases
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class fxcMigrationConfig<T>
        : DbMigrationsConfiguration<T>
        where T : fxaDB<T>
    {
        public fxcMigrationConfig()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(T context)
        {
            if (!context.Database.Exists())
            {
                context.Database.Create();
            }
            base.Seed(context);
        }
    }
}