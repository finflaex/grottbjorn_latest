﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Finflaex.Log;
using Finflaex.Reflection;

#endregion

namespace Finflaex.MsSQL.Tables
{
    public interface fxiTableHistory
    {
        Guid Guid { get; set; }
        DateTime sysStartDT { get; set; }
        DateTime sysEndDT { get; set; }
        DateTime sysActDT { get; set; }
        fxeSysStatus sysStatus { get; set; }
        byte[] sysModification { get; set; }
    }

    public static class fxrTableHistory
    {
        public static IQueryable<T> WhereStatus<T>(this IQueryable<T> @this, fxeSysStatus status)
            where T : fxiTableHistory
        {
            return @this.Where(v => v.sysStatus == status);
        }

        public static IQueryable<T> WhereGuid_actual<T>(this IQueryable<T> @this,
            Guid guid)
            where T : fxiTableHistory
        {
            return @this
                .Where(v
                    => v.sysStatus == fxeSysStatus.actual
                       && v.Guid == guid);
        }

        public static int GetHistoryCount(this fxiTableHistory self)
        {
            return self
                .GetType()
                .GetProperties()
                .Select(v => v
                    .GetAttributesOfProperty()
                    .OfType<atrHistory>()
                    .FirstOrDefault())
                .Where(v => v != null)
                .Max(v => v.Index)
                .Increment();
        }

        /// <summary>
        ///     сравнивает две сущности на предмет изменений
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="value"></param>
        /// <returns>true если есть изменения</returns>
        public static bool CompareHistory<T>(this T self, T value)
            where T : fxiTableHistory
        {
            var temp = new BitArray(self.GetHistoryCount());

            self
                .GetType()
                .GetProperties()
                .Select(prop => new
                {
                    prop,
                    attr = prop.GetAttributesOfProperty().OfType<atrHistory>().FirstOrDefault()
                })
                .Where(read => read.attr != null)
                .ForEach(read =>
                {
                    var old_value = read.prop.GetValue(value);
                    var new_value = read.prop.GetValue(self);
                    var index = read.attr.Index;
                    temp.Set(index, !old_value.CompareExt(new_value));
                });

            self.sysModification = temp.ToByteArray();
            return temp.Cast<bool>().Any(bit => bit);
        }

        /// <summary>
        ///     Сохраняет историю без db.SaveChange()
        /// </summary>
        /// <typeparam name="TVal"></typeparam>
        /// <param name="self"></param>
        /// <param name="value"></param>
        /// <param name="action">Здесь обрабатываем коллекции и возвращаем Guid</param>
        /// <returns></returns>
        public static TVal History<TVal>(this DbSet<TVal> self, TVal value, Func<TVal, Guid?> action)
            where TVal : class, fxiTableHistory
        {
            try
            {
                var oldGuid = action?.Invoke(value);
                var old = oldGuid == null
                    ? null
                    : self.FirstOrDefault(v => v.Guid == oldGuid && v.sysStatus == fxeSysStatus.actual);
                if (old == null)
                {
                    value.CompareHistory(typeof (TVal).Create().AsType<TVal>());
                    value.sysStatus = fxeSysStatus.actual;
                    value.sysStartDT = value.sysActDT = DateTime.Now;
                    return self.Add(value);
                }
                if (value.CompareHistory(old))
                {
                    old.sysEndDT = old.sysActDT = value.sysStartDT = value.sysActDT = DateTime.Now;
                    old.sysStatus = fxeSysStatus.history;
                    value.sysStatus = fxeSysStatus.actual;
                    value.Guid = oldGuid.AsType<Guid>();
                    return self.Add(value);
                }
                old.sysActDT = DateTime.Now;
                return old;
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.Error,
                    typeof (DbSet<TVal>),
                    MethodBase.GetCurrentMethod(),
                    error,
                    new Dictionary<string, object>
                    {
                        ["value"] = value
                    });
                return null;
            }
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class atrHistory : Attribute
    {
        public atrHistory(int index, string description = null)
        {
            Index = index;
            Description = description;
        }

        public int Index { get; }
        public string Description { get; }
    }
}