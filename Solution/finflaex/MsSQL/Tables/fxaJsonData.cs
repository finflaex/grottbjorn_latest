﻿#region

using System;
using Finflaex.Reflection;

#endregion

namespace Finflaex.MsSQL.Tables
{
    public class fxaJsonData<TThis> 
        : fxaAutoGuid<TThis>, 
        fxiTableChange<TThis>
        where TThis:fxaJsonData<TThis>
    {
        #region SHARP

        public fxaJsonData()
        {
            sysStartDT = DateTime.Now;
            sysActDT = DateTime.Now;
            sysEndDT = DateTime.MaxValue;
            sysStatus = fxeSysStatus.actual;
        }

        #endregion

        #region DATA

        [Change]
        public virtual string Value { get; set; }

        #endregion

        #region SYSTEM

        public DateTime sysActDT { get; set; }

        public DateTime sysEndDT { get; set; }


        public DateTime sysStartDT { get; set; }

        public fxeSysStatus sysStatus { get; set; }
        public virtual TThis sysNext { get; set; }

        public virtual TThis sysPrev { get; set; }

        #endregion
    }
}