﻿#region

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace Finflaex.MsSQL.Tables
{
    public interface fxiTableKeyGuid
    {
        [Key]
        Guid sysGuid { get; set; }
    }
}