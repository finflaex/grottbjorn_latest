﻿#region

using System.ComponentModel.DataAnnotations;

#endregion

namespace Finflaex.MsSQL.Tables
{
    public interface fxiTableConcurent
    {
        [Timestamp]
        byte[] sysVersion { get; set; }
    }
}