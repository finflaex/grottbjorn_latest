﻿#region

using System;

#endregion

namespace Finflaex.MsSQL.Tables
{
    public class fxaDescription<TThis> 
        : fxaAutoGuid<TThis>
        , fxiTableChange<TThis>
        where TThis : fxaDescription<TThis>
    {
        #region SHARP

        public fxaDescription()
        {
            sysStartDT = DateTime.Now;
            sysActDT = DateTime.Now;
            sysEndDT = DateTime.MaxValue;
            sysStatus = fxeSysStatus.actual;
        }

        #endregion

        #region DATA

        [Change]
        public string Value { get; set; }

        #endregion

        #region SYSTEM

        public DateTime sysActDT { get; set; }

        public DateTime sysEndDT { get; set; }

        public DateTime sysStartDT { get; set; }

        public fxeSysStatus sysStatus { get; set; }
        public virtual TThis sysPrev { get; set; }

        public virtual TThis sysNext { get; set; }

        #endregion
    }
}