﻿namespace Finflaex.MsSQL.Tables
{
    public enum fxeSysStatus
    {
        remove = -10,
        block = -1,
        none = 0,
        history = 1,
        actual = 10
    }
}