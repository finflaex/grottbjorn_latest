﻿#region

using System;
using System.ComponentModel.DataAnnotations;
using Finflaex.Reflection;

#endregion

namespace Finflaex.MsSQL.Tables
{
    public abstract class fxaAutoGuid<TThis>
        : fxiTableKeyGuid
        where TThis : fxaAutoGuid<TThis>
    {
        protected fxaAutoGuid()
        {
            sysGuid = Guid.NewGuid();
        }

        [Key]
        public Guid sysGuid { get; set; }

        public TThis Clone()
        {
            return this.MemberwiseClone().AsType<TThis>();
        }
    }
}