﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Reflection;

namespace Finflaex.MsSQL.Tables
{
    public interface fxiTableChange<T>
        where T : fxiTableChange<T>
    {
        T sysPrev { get; set; }
        T sysNext { get; set; }

        DateTime sysStartDT { get; set; }
        DateTime sysActDT { get; set; }
        DateTime sysEndDT { get; set; }
        fxeSysStatus sysStatus { get; set; }

        T Clone();
    }

    public static class fxrTableChange
    {
        public static TVal Change<TVal>(this DbSet<TVal> @this, Guid guid)
            where TVal : class, fxiTableChange<TVal>, fxiTableKeyGuid
        {
            var record = @this.FirstOrDefault(v=>v.sysGuid == guid && v.sysStatus == fxeSysStatus.actual);
            if (record == null) return null;

            var shot = typeof (TVal).Create().AsType<TVal>();

            record.sysActDT = shot.sysStartDT;
            shot.sysStatus = fxeSysStatus.history;

            record
                .GetPropertiInfos()
                .Where(property => property
                    .GetAttributesOfProperty()
                    .OfType<ChangeAttribute>()
                    .Any())
                .ForEach(property =>
                {
                    try
                    {
                        var value = property.GetValue(record);
                        property.SetValue(shot, value);
                    }
                    catch { }
                });

            var prev = record.sysPrev;

            if (prev == null)
            {
                record.sysPrev = shot;
            }
            else
            {
                prev.sysEndDT = shot.sysStartDT;
                record.sysPrev = shot;
                shot.sysPrev = prev;
            }

            return record;
        }

        public static TVal Change<TVal>(this DbSet<TVal> @this, TVal value)
            where TVal : class, fxiTableChange<TVal>, fxiTableKeyGuid
            => @this.Change(value.sysGuid);

        public static void RemoveChange<TVal>(this DbSet<TVal> @this, Guid guid)
            where TVal : class, fxiTableChange<TVal>, fxiTableKeyGuid
        {
            var record = @this.RemoveChangeLog(guid);
            @this.Remove(record);
        }

        public static TVal RemoveChangeLog<TVal>(this DbSet<TVal> @this, Guid guid)
            where TVal : class, fxiTableChange<TVal>, fxiTableKeyGuid
        {
            var record = @this.Find(guid);
            if (record == null) return null;
            while (record.sysPrev != null) record = record.sysPrev;
            while (record.sysNext != null)
            {
                record = record.sysNext;
                @this.Remove(record.sysPrev);
            };
            return record;
        }
    }
}
