﻿#region

using System;
using System.Linq;
using System.Management;
using System.Net;
using System.Text;
using Finflaex.Reflection;
using NUnit.Framework;

#endregion

namespace Finflaex.Lan
{
    [TestFixture]
    public static class fxsIPv4
    {
        public static string[] IPList => Dns
            .GetHostName()
            .SelectSingle(Dns.GetHostAddresses)
            .Select(v => v.ToString())
            .ToArray();

        public static string IPv4
        {
            get
            {
                var reg = @"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})".ToRegex();
                return IPList.FirstOrDefault(v => reg.Match(v).Success);
            }
        }

        public static string ToIPv4String(this uint @this)
        {
            return @this == 0
                ? string.Empty
                : new StringBuilder()
                    .Append((@this >> 24) & 255)
                    .Append(".")
                    .Append((@this >> 16) & 255)
                    .Append(".")
                    .Append((@this >> 8) & 255)
                    .Append(".")
                    .Append(@this & 255)
                    .ToString();
        }

        public static uint ToIPv4UInt(this string @this)
        {
            try
            {
                uint intValue = 0;
                foreach (var stringPart in @this.Split('.', ','))
                {
                    intValue <<= 8;
                    intValue |= uint.Parse(stringPart);
                }
                return intValue;
            }
            catch
            {
                return 0;
            }
        }

        public static uint IPv4_Count_toMask(this int @this)
        {
            return uint.MaxValue << (32 - @this);
        }

        public static int IPv4_Mask_toCount(this uint @this)
        {
            var result = 0;
            @this = ~@this;
            while (@this != 0)
            {
                result++;
                @this = @this >> 1;
            }
            return 32 - result;
        }

        /// <summary>
        ///     Set's a new IP Address and it's Submask of the local machine
        /// </summary>
        /// <param name="ip_address">The IP Address</param>
        /// <param name="subnet_mask">The Submask IP Address</param>
        /// <remarks>Requires a reference to the System.Management namespace</remarks>
        public static void SetIP(string ip_address, string subnet_mask)
        {
            new ManagementClass("Win32_NetworkAdapterConfiguration")
                .GetInstances()
                .Cast<ManagementObject>()
                .Where(objMO => (bool) objMO["IPEnabled"])
                .ForEach(objMO =>
                {
                    var newIP = objMO.GetMethodParameters("EnableStatic");
                    newIP["IPAddress"] = new[] {ip_address};
                    newIP["SubnetMask"] = new[] {subnet_mask};
                    objMO.InvokeMethod("EnableStatic", newIP, null);
                });
        }

        /// <summary>
        ///     Set's a new Gateway address of the local machine
        /// </summary>
        /// <param name="gateway">The Gateway IP Address</param>
        /// <remarks>Requires a reference to the System.Management namespace</remarks>
        public static void SetGateway(string gateway)
        {
            new ManagementClass("Win32_NetworkAdapterConfiguration")
                .GetInstances()
                .Cast<ManagementObject>()
                .Where(objMO => (bool) objMO["IPEnabled"])
                .ForEach(objMO =>
                {
                    var newGateway = objMO.GetMethodParameters("SetGateways");
                    newGateway["DefaultIPGateway"] = new[] {gateway};
                    newGateway["GatewayCostMetric"] = new[] {1};
                    objMO.InvokeMethod("SetGateways", newGateway, null);
                });
        }

        /// <summary>
        ///     Set's the DNS Server of the local machine
        /// </summary>
        /// <param name="NIC">NIC address</param>
        /// <param name="DNS">DNS server address</param>
        /// <remarks>Requires a reference to the System.Management namespace</remarks>
        public static void SetDNS(string NIC, string DNS)
        {
            new ManagementClass("Win32_NetworkAdapterConfiguration")
                .GetInstances()
                .Cast<ManagementObject>()
                .ForEach(objMO =>
                {
                    if (!(bool) objMO["IPEnabled"]) return;
                    if (!objMO["Caption"].Equals(NIC)) return;
                    var newDNS = objMO.GetMethodParameters("SetDNSServerSearchOrder");
                    newDNS["DNSServerSearchOrder"] = DNS.Split(',');
                    objMO.InvokeMethod("SetDNSServerSearchOrder", newDNS, null);
                });
        }

        /// <summary>
        ///     Set's WINS of the local machine
        /// </summary>
        /// <param name="NIC">NIC Address</param>
        /// <param name="priWINS">Primary WINS server address</param>
        /// <param name="secWINS">Secondary WINS server address</param>
        /// <remarks>Requires a reference to the System.Management namespace</remarks>
        public static void SetWINS(string NIC, string priWINS, string secWINS)
        {
            new ManagementClass("Win32_NetworkAdapterConfiguration")
                .GetInstances()
                .Cast<ManagementObject>()
                .ForEach(objMO =>
                {
                    if (!(bool) objMO["IPEnabled"]) return;
                    if (!objMO["Caption"].Equals(NIC)) return;
                    var wins = objMO.GetMethodParameters("SetWINSServer");
                    wins.SetPropertyValue("WINSPrimaryServer", priWINS);
                    wins.SetPropertyValue("WINSSecondaryServer", secWINS);
                    objMO.InvokeMethod("SetWINSServer", wins, null);
                });
        }

        [Test]
        public static void Test()
        {
            var test = "127.0.0.1";
            var result = test.ToIPv4UInt().ToIPv4String();
            Assert.AreEqual(test, result);

            var test2 = 10;
            var result2 = test2.IPv4_Count_toMask().IPv4_Mask_toCount();
            Assert.AreEqual(test2, result2);

            var v1 = test.ToIPv4UInt().ToIPv4String();
            var v2 = test2.IPv4_Count_toMask().ToIPv4String();
        }
    }
}