﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Reflection;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Finflaex.Types
{
    public class fxcDictSO : Dictionary<string, object>
    {
        public SqlParameter[] AsSqlParameters
            => this
                .Select(item => new SqlParameter(item.Key, item.Value))
                .ToArray();

        public new object this[string data]
        {
            get { return ContainsKey(data) ? base[data] : null; }
            set { base[data] = value; }
        }

        public R Get<R>(string s) => this[s].AsType<R>();
        public string GetString(string s) => this[s].AsType<string>();
        public int GetInt(string s) => this[s].AsType<int>();

        public T GetJObjectToObject<T>(string name)
        {
            return this[name].AsType<JObject>().ToObject<T>();
        }
    }
}
