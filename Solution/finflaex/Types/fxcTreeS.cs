﻿#region

using System.Linq;
using Finflaex.Reflection;

#endregion

namespace Finflaex.Types
{
    public class fxcTreeS : fxcTree<string, fxcTreeS>
    {
        public string Compile()
        {
            return Content
                .RetSingle(c =>
                {
                    return c += Childs
                        .Select(v => v.Compile())
                        .Join(string.Empty);
                })
                .Prepend(Before ?? string.Empty)
                .Append(After ?? string.Empty);
        }
    }
}