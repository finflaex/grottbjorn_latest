﻿#region

using System;

#endregion

namespace Finflaex.Types
{
    /// <summary>
    ///     private static <fxcStackEventMethod> stack = new fxcStack();
    ///     public fxcStack<fxcStackEventMethod> _stack => _stack;
    /// </summary>
    public interface fxiStackEventClass
    {
        fxcStack<fxcStackEventMethod> _stack { get; }
    }

    public class fxcStackEventMethod
    {
        public fxcStackEventMethod(Func<fxcDictSO, object> func)
        {
            guid = Guid.NewGuid().ToString();
            this.func = func;
        }

        public string guid { get; }

        public Func<fxcDictSO, object> func { get; }
    }

    public static class fxrStackEventClass
    {
        public static string AddStackMethod(this fxiStackEventClass self,
            Func<fxcDictSO, object> func)
        {
            var method = new fxcStackEventMethod(func);
            self._stack.Add(method);
            return method.guid;
        }

        public static bool RunStackMethod(this fxiStackEventClass self,
            string name,
            fxcDictSO data,
            out object result)
        {
            if (self._stack.Get(v => v.guid, name) != null)
            {
                result = self._stack.Rem(v => v.guid, name).func.Invoke(data);
                return true;
            }
            result = null;
            return false;
        }
    }
}