﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Reflection;

namespace Finflaex.Types
{
    public class fxcTree<T,TThis>
        where TThis : fxcTree<T,TThis>
    {
        public string Name { get; set; }
        public string Target { get; set; }

        public List<TThis> Childs { get; set; }

        public T Before { get; set; }
        public T After { get; set; }
        public T Content { get; set; }

        public fxcTree()
        {
            Name = null;
            Target = null;

            Childs = new List<TThis>();

            Before = default(T);
            After = default(T);
            Content = default(T);
        }

        public TThis Add(TThis value)
        {
            Childs.Add(value);
            return this.AsType<TThis>();
        }
    }
}
