﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

#endregion

namespace Finflaex.Types
{
    public class fxcStack<V> : LinkedList<V>
    {
        public void Add(V value)
        {
            AddLast(value);
        }

        public V Get<R>(Expression<Func<V, R>> action, R value)
        {
            var act = action.Compile();
            return this.FirstOrDefault(v => act(v).Equals(value));
        }

        public V Rem<R>(Expression<Func<V, R>> action, R value)
            where R : class
        {
            var result = Get(action, value);
            Remove(result);
            return result;
        }
    }
}