﻿#region

using System;
using System.Collections.Generic;
using Finflaex.DBS.Items;

#endregion

namespace Finflaex.DBS.Bases
{
    public static class Documents
    {
        public static readonly IEnumerable<itemDocumentType> DocumetTypes;

        static Documents()
        {
            DocumetTypes = new List<itemDocumentType>
            {
                new itemDocumentType
                {
                    Code = 1,
                    Description = "Паспорт гражданина СССР",
                    Note =
                        "Паспорт, удостоверяющий личность гражданина СССР и действующий на территории РФ до 31 декабря 2005 года",
                    EndActual = new DateTime(2005, 12, 31),
                    ShortName = "ПАСПОРТ",
                    Shablon = "R-ББ 999999"
                },
                new itemDocumentType
                {
                    Code = 2,
                    Description = "Загранпаспорт гражда нина СССР",
                    Note = "Паспорт, удостоверяющий личность гражданина РФ за пределами РФ, образца до 1997 года",
                    EndActual = new DateTime(1997, 1, 1),
                    ShortName = "ЗГПАСПОРТ",
                    Shablon = "99 0999999"
                },
                new itemDocumentType
                {
                    Code = 3,
                    Description = "Свидетельство о рождении",
                    Note = "Для лиц, не достигших 16-летнего возраста",
                    EndActual = new DateTime(1997, 10, 01),
                    ShortName = "СВИД О РОЖД",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 3,
                    Description = "Свидетельство о рождении",
                    Note = "Для лиц, не достигших 14-летнего возраста",
                    StartActual = new DateTime(1997, 10, 01),
                    ShortName = "СВИД О РОЖД",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 4,
                    Description = "Удостоверение личности офицера",
                    Note = "Для военнослужащих (офицеров,прапорщиков, мичманов)",
                    ShortName = "УДОСТ ОФИЦЕРА",
                    Shablon = "ББ 0999999"
                },
                new itemDocumentType
                {
                    Code = 5,
                    Description = "Справка об освобождении из места лишения свободы",
                    Note = "Для лиц, освободившихся из мест лишения cвободы",
                    ShortName = "СПРАВКА ОБ ОСВ",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 6,
                    Description = "Паспорт Минморфлота",
                    Note = "Паспорт моряка Минморфлота СССР (РФ), выданный до 1997 г.",
                    EndActual = new DateTime(1997, 1, 1),
                    ShortName = "ПАСПОРТ МОРФЛТ",
                    Shablon = "ББ 999999"
                },
                new itemDocumentType
                {
                    Code = 7,
                    Description = "Военный билет солдата (матроса, сержанта, старшины)",
                    Note =
                        "Военный билет для солдат, матросов, сержантов и старшин, проходящих военную службу по призыву или контракту",
                    ShortName = "ВОЕННЫЙ БИЛЕТ",
                    Shablon = "ББ 0999999"
                },
                new itemDocumentType
                {
                    Code = 8,
                    Description = "Временное удостоверение, выданное взамен военного билета",
                    Note = "Временное удостоверение, выданное взамен военного билета",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 9,
                    Description = "Дипломатический паспорт гражданина РФ",
                    Note = "Дипломатический паспорт для граждан РФ",
                    ShortName = "ДИППАСПОРТ РФ",
                    Shablon = "99 9999999"
                },
                new itemDocumentType
                {
                    Code = 10,
                    Description = "Паспорт иностранного гражданина",
                    Note =
                        "Заграничный паспорт для постоянно проживающих за границей физических лиц, которые временно находятся на территории Российской Федерации",
                    ShortName = "ИНПАСПОРТ",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 11,
                    Description = "Свидетельство о регистрации ходатайства иммигранта о признании его беженцем",
                    Note = "Для беженцев, не имеющих статуса беженца",
                    ShortName = "СВИД БЕЖЕНЦА",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 12,
                    Description = "Вид на жительство ",
                    Note = "Вид на жительство в Российской Федерации для лиц без гражданства ",
                    ShortName = "ВИД НА ЖИТЕЛЬ",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 13,
                    Description = "Удостоверение беженца в РФ",
                    Note = "Для беженцев",
                    ShortName = "УДОСТ БЕЖЕНЦА",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 14,
                    Description = "Временное удостоверение личности гражданина Российской Федерации",
                    Note = "Временное удостоверение личности гражданина Российской Федерации по форме 2П",
                    ShortName = "ВРЕМ УДОСТ",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 15,
                    Description = "Разрешение на временное проживание в Российской Федерации",
                    Note = "Разрешение на временное проживание в Российской Федерации",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 18,
                    Description = "Свидетельство о предоставлении временного убежища на территории Российской Федерации",
                    Note = "Свидетельство о предоставлении временного убежища на территории Российской Федерации",
                    Shablon = "ББ-999  9999999"
                },
                new itemDocumentType
                {
                    Code = 19,
                    Description = "Свидетельство о предоставлении временного убежища на территории Российской Федерации",
                    Note = "Свидетельство о предоставлении временного убежища на территории Российской Федерации"
                },
                new itemDocumentType
                {
                    Code = 21,
                    Description = "Паспорт гражданина Российской Федерации",
                    Note =
                        "Паспорт гражданина Российской Федерации, действующий на территории Российской Федерации с 1 октября 1997 года",
                    StartActual = new DateTime(1997, 10, 1),
                    ShortName = "ПАСПОРТ РОССИИ",
                    Shablon = "99 99 9999990"
                },
                new itemDocumentType
                {
                    Code = 22,
                    Description = "Загранпаспорт гражданина РФ",
                    Note = "Паспорт, удостоверяющий личность гражданина РФ за пределами РФ,образца 1997 года",
                    StartActual = new DateTime(1997, 1, 1),
                    ShortName = "ЗГПАСПОРТ РФ",
                    Shablon = "99 9999999"
                },
                new itemDocumentType
                {
                    Code = 23,
                    Description = "Свидетельство о рождении, выданное уполномоченным органом иностранного государства",
                    Note = "Для иностранных граждан, не достигших 16-летнего возраста",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 24,
                    Description = "Удостоверение личности военнослужащего РФ",
                    Note = "Удостоверение личности военнослужащего Российской Федерации",
                    Shablon = "ББ 9999999"
                },
                new itemDocumentType
                {
                    Code = 26,
                    Description = "Паспорт моряка",
                    Note =
                        "Паспорт моряка (удостоверение личности гражданина, работающего на судах заграничного плавания или на иностранных судах) образца 1997 г.",
                    StartActual = new DateTime(1997, 1, 1),
                    ShortName = "ПАСПОРТ МОРЯКА",
                    Shablon = "ББ 0999999"
                },
                new itemDocumentType
                {
                    Code = 27,
                    Description = "Военный билет офицера запаса",
                    Note = "Военный билет офицера запаса",
                    ShortName = "ВОЕН БИЛЕТ ОЗ",
                    Shablon = "ББ 0999999"
                },
                new itemDocumentType
                {
                    Code = 28,
                    Description = "Временное удостоверение, выданное взамен военного билета",
                    Note = "Временное удостоверение, выданное взамен военного билета",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 29,
                    Description = "Временное удостоверение личности гражданина Российской Федерации",
                    Note = "Временное удостоверение личности гражданина Российской Федерации",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 60,
                    Description = "Документы, подтверждающие факт регистрации по месту жительства",
                    Note = "Документы, подтверждающие факт регистрации по месту жительства"
                },
                new itemDocumentType
                {
                    Code = 61,
                    Description = "Свидетельство о регистрации по месту жительства",
                    Note = "Свидетельство о регистрации по месту жительства",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 62,
                    Description = "Вид на жительство иностранного гражданина",
                    Note = "Вид на жительство иностранного гражданина"
                },
                new itemDocumentType
                {
                    Code = 81,
                    Description = "Свидетельство о смерти",
                    Note = "Свидетельство о смерти",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 91,
                    Description = "Иные документы, выдаваемые органами МВД",
                    Note = "Иные выдаваемые органами внутренних дел РФ документы, удостоверяющие личность гражданина",
                    ShortName = "ПРОЧЕЕ",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 93,
                    Description = "Дипломатический паспорт",
                    Note = "Дипломатический паспорт",
                    Shablon = "99 9999999"
                },
                new itemDocumentType
                {
                    Code = 94,
                    Description =
                        "Справка для участия в выборах или в референдуме, выдаваемая гражданам РФ, находящимся в местах содержания под стражей подозреваемых и обвиняемых, не имеющим паспорт",
                    Note =
                        "Справка для участия в выборах или в референдуме, выдаваемая гражданам РФ, находящимся в местах содержания под стражей подозреваемых и обвиняемых, не имеющим паспорт",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                },
                new itemDocumentType
                {
                    Code = 95,
                    Description = "Служебный паспорт",
                    Note = "Служебный паспорт",
                    Shablon = "99 9999999"
                },
                new itemDocumentType
                {
                    Code = 96,
                    Description = "Свидетельство на въезд (возвращение) в РФ",
                    Note = "Свидетельство на въезд (возвращение) в РФ",
                    Shablon = "SSSSSSSSSSSSSSSSSSSSSSSSS"
                }
            };
        }
    }
}