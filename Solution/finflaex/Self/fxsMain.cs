﻿#region

using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Finflaex.Attributes;
using Finflaex.fxMVC.Authorization;
using Finflaex.Log;
using Finflaex.MVC;
using Finflaex.Plugin;
using Finflaex.Reflection;
using Finflaex.Types;
using Finflaex.WinApi;

#endregion

namespace Finflaex.Self
{
    public static class fxsMain
    {
        public static fxcDictSO Values = new fxcDictSO();

        public static CompositionContainer MEF
        {
            get { return Values.Get<CompositionContainer>("mef"); }
            set { Values["mef"] = value; }
        }

        public static string[] PluginDirs
        {
            get { return Values.Get<string[]>("mef_dir"); }
            set { Values["mef_dir"] = value; }
        }

        public static string ExecutablePath
        {
            get
            {
                var executablePath = "";
                var assembly1 = Assembly.GetEntryAssembly();
                if (assembly1 == null)
                {
                    var builder1 = new StringBuilder(260);
                    kernel32.GetModuleFileName(0, builder1, builder1.Capacity);
                    executablePath = Path.GetFullPath(builder1.ToString());
                }
                else
                {
                    var text1 = assembly1.EscapedCodeBase;
                    var uri1 = new Uri(text1);
                    executablePath = uri1.Scheme == "file" ? GetLocalPath(text1) : uri1.ToString();
                }

                var uri2 = new Uri(executablePath);
                if (uri2.Scheme == "file")
                {
                    new FileIOPermission(FileIOPermissionAccess.PathDiscovery, executablePath).Demand();
                }
                return executablePath;
            }
        }

        public static string ExecutableDirectory => ExecutablePath.GetDirectoryName();

        public static string ServerPath(string path)
        {
            path = path.Replace("\\", "/");
            return HttpContext.Current.Server.MapPath(path);
        }

        public static string GetLocalPath(string fileName)
        {
            var uri1 = new Uri(fileName);
            return uri1.LocalPath + uri1.Fragment;
        }

        public static void MvcAsaxInitialize(params object[] param)
        {
            //RouteTable.Routes.MapRoute(
            //    name: "Plugin",
            //    url: "ext/{action}/{*param}",
            //    defaults: new { controller = "fxcDefaultController", action = "Index"});

            //RouteTable.Routes.MapRoute(
            //    name: "Result",
            //    url: "{controller}/{action}.{result}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = "", result = "view" });

            var ControllerFactory = new fxcControllerFactory();
            ControllerBuilder.Current.SetControllerFactory(ControllerFactory);

            //ViewEngines.Engines.Clear();
            //ViewEngines.Engines.Add(new fxcViewEngine("~/plugins"));

            GlobalFilters.Filters.Clear();
            GlobalFilters.Filters.Add(new RolesAttribute());
        }

        public static CompositionContainer LoadMef(params object[] param)
        {
            var catalog = new AggregateCatalog();
            var files = param
                .OfType<string>()
                .AddRangeRet(PluginDirs ?? new string[0])
                .AddRet(ExecutableDirectory)
                .SelectMany(p => Directory.GetFiles(p, "*.dll"))
                .ToArray();
            var assemblies = param
                .OfType<Assembly>()
                .ToArray();
            files
                .Select(p =>
                {
                    try
                    {
                        return new AssemblyCatalog(Assembly.Load(File.ReadAllBytes(p)));
                    }
                    catch (Exception error)
                    {
                        fxsLog.Write(
                            fxeLog.FinflaexError,
                            fxeLog.AdminError,
                            typeof (fxsMain),
                            MethodBase.GetCurrentMethod(),
                            error);
                        return null;
                    }
                })
                .AddRet(new AssemblyCatalog(Assembly.GetExecutingAssembly()))
                .AddRangeRet(assemblies.Select(a => new AssemblyCatalog(a)))
                .WhereNotNull()
                .ForEach(p =>
                {
                    catalog.Catalogs.Add(p);
                });
            //catalog.Catalogs.Add(new DirectoryCatalog(ExecutableDirectory));
            return MEF = new CompositionContainer(catalog);
        }
    }
}