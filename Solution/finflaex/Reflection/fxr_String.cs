﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using Finflaex.Attributes;
using Finflaex.Self;
using NUnit.Framework;
using RazorEngine;
using ServiceStack.Html;
using Encoding = System.Text.Encoding;

#endregion

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static byte[] ToBytes_BlockCopy(this string value)
        {
            var bytes = new byte[value.Length*sizeof (char)];
            Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static byte[] ToBytes(this string self, Encoding encoding)
        {
            return encoding.GetBytes(self);
        }

        public static string ToString_BlockCopy(this byte[] bytes)
        {
            var characters = new char[bytes.Length/sizeof (char)];
            Buffer.BlockCopy(bytes, 0, characters, 0, bytes.Length);
            return new string(characters).Trim((char) 0);
        }

        public static string ToString(this byte[] @this,
            Encoding encoding)
        {
            if (encoding.Equals(Encoding.UTF8) 
                && @this.Length > 2
                && @this[0] == 0xEF 
                && @this[1] == 0xBB 
                && @this[2] == 0xBF)
            {
                @this = @this.Skip(3).ToArray();
            }
            var result = encoding.GetString(@this);
            return result;
        }

        public static string UrlDecode(this string self)
            => HttpUtility
                .UrlDecode(self);

        public static string UrlEncode(this string self)
            => HttpUtility
                .UrlEncode(self);

        public static string UrlTokenEncode(this byte[] @this)
            => HttpServerUtility.UrlTokenEncode(@this);

        public static byte[] UrlTokenDecode(this string @this)
            => HttpServerUtility.UrlTokenDecode(@this);

        public static bool IsNullOrEmpty(this string self)
            => string
                .IsNullOrEmpty(self);

        public static bool IsNullOrWhiteSpace(this string self)
            => string
                .IsNullOrWhiteSpace(self);

        public static string IsNullOrEmpty(this string self,
            Func<string> func)
            => self
                .IsNullOrEmpty()
                ? func?.Invoke()
                : self;

        public static string IsNotNullOrWhiteSpace(this string self,
            Func<string> func)
            => string
                .IsNullOrWhiteSpace(self)
                ? self
                : func?.Invoke();

        public static string IsNotNullOrEmpty(this string self,
            Func<string> func)
            => self
                .IsNullOrEmpty()
                ? self
                : func?.Invoke();

        public static bool IsNotNullOrWhiteSpace(
            this string @this,
            Action<string> func = null)
        {
            var result = string.IsNullOrWhiteSpace(@this);
            if (!result) func?.Invoke(@this);
            return result;
        }

        public static bool IsNotNullOrEmpty(
            this string @this,
            Action<string> func = null)
        {
            var result = string.IsNullOrEmpty(@this);
            if (!result) func?.Invoke(@this);
            return result;
        }

        public static string IsNullOrWhiteSpace(this string self,
            Func<string> func)
            => string
                .IsNullOrWhiteSpace(self)
                ? func?.Invoke()
                : self;

        /// <summary>
        ///     извлекает полный путь к каталогу из переданного пути
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string GetDirectoryName(this string self)
        {
            return self
                .IsNullOrEmpty()
                ? string.Empty
                : Path.GetDirectoryName(self);
        }

        public static string GetServerDirectoryName(this string self)
        {
            var path = self.Replace("\\", "/");
            path = HttpContext.Current.Server.MapPath(path);
            return path.GetDirectoryNameEx();
        }

        /// <summary>
        ///     Расширенная версия добавляет слеш в конец при проверке
        /// </summary>
        /// <param name="this"></param>
        /// <returns>Результат без слеша в конце</returns>
        public static string GetDirectoryNameEx(this string @this)
        {
            var path = @this
                .GetFileExtension()
                .IsNullOrEmpty()
                ? (@this.EndsWith("\\") ? @this : @this.Append("\\"))
                : @this;

            var result = path.GetDirectoryName();

            return result ?? (
                path.IsNullOrEmpty()
                    ? string.Empty
                    : path.Length <= 1
                        ? string.Empty
                        : (path.EndsWith("\\")
                            ? path.Remove(path.Length - 1)
                            : path)
                );
        }

        public static string GetFileNameWithoutExtension(this string self)
            => self.IsNullOrEmpty() ? string.Empty : Path.GetFileNameWithoutExtension(self);

        public static string GetFileExtension(this string self)
            => self.IsNullOrEmpty() ? string.Empty : Path.GetExtension(self)?.TrimStart('.');

        public static string Encrypt(this string value, string password = "finflaex")
        {
            try
            {
                byte[] Results;
                var UTF8 = new UTF8Encoding();
                var HashProvider = new MD5CryptoServiceProvider();
                var TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(password));
                var TDESAlgorithm = new TripleDESCryptoServiceProvider
                {
                    Key = TDESKey,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };
                var DataToEncrypt = UTF8.GetBytes(value);
                try
                {
                    var Encryptor = TDESAlgorithm.CreateEncryptor();
                    Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
                }
                finally
                {
                    TDESAlgorithm.Clear();
                    HashProvider.Clear();
                }
                return Convert.ToBase64String(Results);
            }
            catch
            {
                return "";
            }
        }

        public static string Decrypt(this string value, string password = "finflaex")
        {
            try
            {
                byte[] Results;
                var UTF8 = new UTF8Encoding();
                var HashProvider = new MD5CryptoServiceProvider();
                var TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(password));
                var TDESAlgorithm = new TripleDESCryptoServiceProvider();
                TDESAlgorithm.Key = TDESKey;
                TDESAlgorithm.Mode = CipherMode.ECB;
                TDESAlgorithm.Padding = PaddingMode.PKCS7;
                var DataToDecrypt = Convert.FromBase64String(value);
                try
                {
                    var Decryptor = TDESAlgorithm.CreateDecryptor();
                    Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
                }
                finally
                {
                    TDESAlgorithm.Clear();
                    HashProvider.Clear();
                }
                return UTF8.GetString(Results);
            }
            catch
            {
                return "";
            }
        }

        public static string ToBase64(this string self)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(self));
        }

        public static string FromBase64(this string self)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(self));
        }

        public static Guid ToGuid(this string self)
        {
            return Guid.Parse(self);
        }

        public static DateTime? ToDateTime(this string self, params string[] mask)
        {
            return mask.Length == 0
                ? DateTime.Parse(self)
                : DateTime.ParseExact(self, mask, CultureInfo.InvariantCulture, DateTimeStyles.None);
        }

        public static int ToInt(this string self)
        {
            return int.Parse(self);
        }

        public static long ToLong(this string self)
        {
            return self.IsNullOrEmpty() ? 0 : long.Parse(self);
        }

        public static string TrimEnd(this string self, string shablon)
        {
            var start = self.Length - shablon.Length;
            var inner = self.Substring(start);
            return inner == shablon
                ? self.Remove(start)
                : self;
        }

        public static T ToEnum<T>(this string self)
            where T : struct
        {
            var test = typeof (T).Create();
            if (test is Enum)
            {
                try
                {
                    return Enum.Parse(typeof (T), self).AsType<T>();
                }
                catch (ArgumentException)
                {
                    var list = EnumToList<T>();
                    foreach (var item in list)
                    {
                        var flag = typeof (T)
                            .GetField(item.ToString())
                            .GetCustomAttributes(typeof (EnumParserAttribute), false)
                            .OfType<EnumParserAttribute>()
                            .Select(v => v.Name)
                            .Contains(self);

                        if (flag) return item;
                    }
                    return 0.AsType<T>();
                }
            }
            throw new Exception("Указанный тип не является перечислением");
        }

        public static Stream ToStream(this string self)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(self);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static Regex ToRegex(this string self)
        {
            return new Regex(self);
        }

        public static string ToXml(this object self, params object[] param)
        {
            var nameSpace = param.OfType<XmlSerializerNamespaces>().FirstOrDefault();
            var ident = param.OfType<bool>().FirstOrDefault();
            var encoding = param.OfType<Encoding>().FirstOrDefault() ?? Encoding.UTF8;
            var act = param.OfType<Func<XmlWriter, XmlWriter>>().FirstOrDefault();

            var xmlSerializer = new XmlSerializer(self.GetType());

            var config = new XmlWriterSettings
            {
                Encoding = encoding,
                Indent = ident
            };

            var result = new StringBuilder();

            using (var writer = XmlWriter.Create(result, config))
            {
                var my = act?.Invoke(writer);
                if (nameSpace == null)
                {
                    xmlSerializer.Serialize(my ?? writer, self);
                }
                else
                {
                    xmlSerializer.Serialize(my ?? writer, self, nameSpace);
                }
            }

            return result.ToString().Replace("utf-16", "utf-8");

            //using (StringWriter textWriter = new fxcUtf8StringWriter())
            //{
            //    if (nameSpace != null)
            //    {
            //        xmlSerializer.Serialize(textWriter, self, nameSpace);
            //    }
            //    else
            //    {
            //        xmlSerializer.Serialize(textWriter, self);
            //    }
            //    return textWriter.ToString();
            //}
        }

        public static T FromXml<T>(this string self)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                return serializer.Deserialize(self.ToStream()).AsType<T>();
            }
            catch
            {
                return default(T);
            }
        }

        public static string WriteInFile(this string self, string path)
        {
            using (var file = new StreamWriter(path))
            {
                var buf = self.ToCharArray();
                file.Write(buf, 0, buf.Length);
            }
            return self;
        }

        public static string ReadFromFile(this string self)
        {
            using (var file = new StreamReader(self))
            {
                return file.ReadToEnd();
            }
        }

        public static int LevenshteinDistance(this string self, string value)
        {
            if (self == null || value == null) return 0;
            var m = new int[self.Length + 1, value.Length + 1];

            for (var i = 0; i <= self.Length; i++)
            {
                m[i, 0] = i;
            }
            for (var j = 0; j <= value.Length; j++)
            {
                m[0, j] = j;
            }

            for (var i = 1; i <= self.Length; i++)
            {
                for (var j = 1; j <= value.Length; j++)
                {
                    var diff = self[i - 1] == value[j - 1] ? 0 : 1;

                    m[i, j] = Math.Min(Math.Min(m[i - 1, j] + 1,
                        m[i, j - 1] + 1),
                        m[i - 1, j - 1] + diff);
                }
            }
            return m[self.Length, value.Length];
        }

        public static double CompareExt(this string str1, string str2)
        {
            var z = str1.Length - str2.Length;
            if (z < 0)
            {
                for (var i = z; i < 0; i++)
                    str1 += " ";
            }
            if (z > 0)
            {
                for (var i = z; i > 0; i--)
                    str2 += " ";
            }
            var len = str1.Length;
            if (len != str2.Length)
                throw new NotSupportedException("Этот метод пока не поддерживает сравнение разных по длине строк.");
            if (len == 0)
                throw new ArgumentException("Строка не должна быть пустой.");
            var equalChars = 0;
            for (var i = 0; i < len; i++)
                if (str1[i] == str2[i])
                    equalChars++;
            return (double) equalChars/len;
        }

        public static string Append(this string self, object value)
            => self + value.ToString().IsNullOrEmpty(() => string.Empty);

        public static string AppendWhere(this string self, bool flag, object value)
            => flag
                ? self + value.ToString().IsNullOrEmpty(() => string.Empty)
                : self;

        public static string Prepend(this string self, string value)
            => value.IsNullOrEmpty(() => string.Empty) + self;

        public static string Repeat(this string self, int count)
        {
            var result = new StringBuilder();
            count.For(index => result.Append(self));
            return result.ToString();
        }

        public static string CompleteAfter(this string self, string value, int count)
        {
            var cr = (count - (double) self.Length)/value.Length;
            return self + value.Repeat(cr.ToInt());
        }

        public static string CompleteBefore(this string self, string value, int count)
        {
            var cr = (count - (double) self.Length)/value.Length;
            return value.Repeat(cr.ToInt()) + self;
        }

        public static string Complete(this string self, string before, string after, int count)
        {
            var cr = (count - self.Length - before.Length - after.Length)/2;
            return before + " ".Repeat(cr) + self + " ".Repeat(cr) + after;
        }

        public static string CompleteBoth(this string self, string value, int count)
        {
            var cr = (count - self.Length)/value.Length/2;
            return value.Repeat(cr) + self + value.Repeat(cr);
        }

        public static string Both(this string self, string value)
        {
            return value + self + value;
        }

        public static string Align(this string self, int count, string before = null)
        {
            var result = new StringBuilder();
            while (self.Length > count)
            {
                result.AppendLine((before ?? string.Empty) + self.Substring(0, count));
                self = self.Substring(count);
            }
            result.Append((before ?? string.Empty) + self);
            return result.ToString();
        }

        public static string[] ToLines(this string self, StringSplitOptions options = StringSplitOptions.None)
        {
            return self.Split(new[] {"\r\n"}, options);
        }

        public static ActionResult ToActionResult(this string self)
        {
            return new ContentResult
            {
                Content = self
            };
        }

        public static bool Compare(this string @this, string value, bool IgnoreCase = true)
        {
            return
                IgnoreCase
                    ? string.Equals(@this, value, StringComparison.CurrentCultureIgnoreCase)
                    : @this == value;
        }

        public static string RazorParseText(this string template, object model = null)
        {
            return model == null ? Razor.Parse(template) : Razor.Parse(template, model);
        }

        public static string RazorParseFile(this string path, object model = null)
        {
            var template =
                fxsMain.ServerPath(path).ReadFromFile();
            return template.RazorParseText(model);
        }

        public static string ToJsMin(this string @this)
        {
            try
            {
                return Minifiers.JavaScript.Compress(@this);
            }
            catch
            {
                return @this;
            }
        }

        public static string ToCssMin(this string @this)
        {
            try
            {
                return Minifiers.Css.Compress(@this);
            }
            catch
            {
                return @this;
            }
        }

        public static string ToHtmlMin(this string @this)
        {
            try
            {
                var min = Minifiers.HtmlAdvanced.AsType<HtmlCompressor>();
                //min.RemoveComments = true;
                return min.Compress(@this);
            }
            catch
            {
                return @this;
            }
        }

        [Test]
        public static void Test_CompareExt()
        {
            var result3 = "мама".CompareExt("папа");
            var result4 = "василь".CompareExt("василий");
            var result5 = "hello world".CompareExt("hello world");
            var result6 = "hello".CompareExt("world");
        }

        /// <summary>
        ///     Тестируем расширенную версию извлечения полного адреса каталого из пути
        /// </summary>
        [Test]
        public static void Test_GetDirectoryNameEx()
        {
            string path, result;

            /// должно вернуть, как приняло
            path = "c:\\test";
            result = path.GetDirectoryNameEx();
            Assert.AreEqual(result, "c:\\test");

            /// должно вернуть без слеша
            path = "c:\\test\\";
            result = path.GetDirectoryNameEx();
            Assert.AreEqual(result, "c:\\test");

            /// должно вернуть только каталог
            path = "c:\\test\\test.txt";
            result = path.GetDirectoryNameEx();
            Assert.AreEqual(result, "c:\\test");

            /// должно вернуть имя диска с двоеточием
            path = "c:";
            result = path.GetDirectoryNameEx();
            Assert.AreEqual(result, "c:");

            /// должно вернуть имя диска с двоеточием
            /// без слеша
            path = "c:\\";
            result = path.GetDirectoryNameEx();
            Assert.AreEqual(result, "c:");
        }

        [Test]
        public static void Test_StringToEnum()
        {
            var test = SeekOrigin.Begin.ToString();
            var test2 = "qwe";
            Assert.AreEqual(test.ToEnum<SeekOrigin>(), SeekOrigin.Begin);
            Assert.AreEqual(test2.ToEnum<SeekOrigin>(), 0.AsType<SeekOrigin>());
        }

        [Test]
        public static void Test_ToDateTime()
        {
            var test = "300616";
            var compare = new DateTime(2016, 06, 30);
            Assert.AreEqual(compare, test.ToDateTime("ddMMyy"));
        }

        public static string TagWrap(this string @this, string tagName)
        {
            return $"<{tagName}>{@this}</{tagName}>";
        }

        public static string StyleTagWrap(this string @this)
        {
            return @this.TagWrap("style");
        }

        public static string ScriptTagWrap(this string @this)
        {
            return @this.TagWrap("script");
        }

        public static string Replace(
            this string @this,
            Dictionary<string, string> data)
        {
            return @this == null ? null : data
                .Aggregate(@this, (current, pair)
                    => Regex.Replace(current, pair.Key, pair.Value));
        }

        public static double ToDouble(this string @this)
        {
            try
            {
                return Double.Parse(@this);
            }
            catch
            {
                return 0;
            }
        }
    }
}