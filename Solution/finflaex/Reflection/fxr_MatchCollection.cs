﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static string[] ToArray(this MatchCollection self)
        {
            var result  = self
                .OfType<Match>()
                .SelectMany(v => v.Groups.Cast<Group>().Select(g => g.Value))
                .ToArray();
            return result.Any() 
                ? result.Skip(1).ToArray() 
                : new string[0];
        }
    }
}