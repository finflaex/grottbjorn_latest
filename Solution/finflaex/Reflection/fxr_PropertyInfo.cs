﻿#region

using System;
using System.Reflection;

#endregion

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static Attribute[] GetAttributesOfProperty(this PropertyInfo self)
        {
            return Attribute.GetCustomAttributes(self);
        }
    }
}