﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using DescriptionAttribute = System.ComponentModel.DescriptionAttribute;

namespace Finflaex.Reflection
{
    partial class fxr
    {
        /// <summary>
        /// Сравнивает числовые значения
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool Is<T>(this Enum type, T value)
        {
            try
            {
                return (long)(object)type == (long)(object)value;
            }
            catch
            {
                return false;
            }
        }

        public static List<T> EnumToList<T>()
            where T : struct
        {
            object qwe = typeof(T).Create();
            if (qwe is Enum)
            {
                return Enum.GetValues(typeof (T)).Cast<T>().ToList();
            }
            throw new Exception("Указанный тип не является перечеслением");
        }

        [Test]
        public static void Test_EnumToList()
        {
            
            var test = new List<SeekOrigin>()
            {
                SeekOrigin.Begin,
                SeekOrigin.Current,
                SeekOrigin.End
            };
            var read1 = fxr.EnumToList<SeekOrigin>();
            Assert.AreEqual(test, read1);
        }

        private static string GetCustomDescription(object objEnum)
        {
            var fi = objEnum.GetType().GetField(objEnum.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : objEnum.ToString();
        }

        public static string Desc(this Enum value)
        {
            return GetCustomDescription(value);
        }
    }
}