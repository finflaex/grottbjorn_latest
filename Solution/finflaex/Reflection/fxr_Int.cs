﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Finflaex.Reflection
{
    [TestFixture]
    partial class fxr
    {
        public static int Increment(this int self, int? value = null) => self + (value ?? 1);
        public static int Decrement(this int self, int? value = null) => self - (value ?? 1);

        [Test]
        public static void Test_Increment()
        {
            var result = 1.Increment();
            Assert.AreEqual(2, result);
        }

        public static int For(this int self, Action<int> action = null)
        {
            if (action != null)
            {
                for (int i = 0; i < self; i++)
                {
                    action?.Invoke(i);
                }
            }
            return self;
        }

        public static R[] Select<R>(this int @this, Func<int, R> action = null)
        {
            var result = new List<R>();
            for (var i = 0; i < @this; i++)
            {
                var read = action == null ? default(R) : action.Invoke(i);
                result.Add(read);
            }
            return result.ToArray();
        }

        public static bool IsNoll(this int self)
            => self == 0;

        public static int IsNoll(this int self, Func<int> func) 
            => self == 0 ? func?.Invoke() ?? 0 : self;
    }
}