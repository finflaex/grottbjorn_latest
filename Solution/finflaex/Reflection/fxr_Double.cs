﻿using System;

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static double Truncate(this double self)
        {
            return Math.Truncate(self);
        }

        public static int ToInt(this double self)
        {
            return (int) Math.Truncate(self);
        }
    }
}