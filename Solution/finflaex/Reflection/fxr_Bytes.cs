﻿#region

using System.Drawing;
using System.IO;

#endregion

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static byte[] IconToBytes(Icon icon)
        {
            using (var ms = new MemoryStream())
            {
                icon.Save(ms);
                return ms.ToArray();
            }
        }

        public static Icon BytesToIcon(byte[] bytes)
        {
            using (var ms = new MemoryStream(bytes))
            {
                return new Icon(ms);
            }
        }
    }
}