﻿#region

using System.Net.Security;

#endregion

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static int BufferSize = 0x400;

        public static string ReadMessage(this SslStream stream)
        {
            var buffer = new byte[BufferSize];
            stream.Read(buffer, 0, BufferSize);
            return ToString_BlockCopy(buffer);
        }
    }
}