﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Finflaex.Log;

#endregion

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T> self)
        {
            return self.Where(v => v != null);
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> self, Action<T> act = null)
        {
            try
            {
                var forEach = self?.ToArray() ?? new T[0];
                if (self != null && act != null)
                {
                    for (int i = 0; i < forEach.Length; i++)
                    {
                        act(forEach[i]);
                    }
                }
                return forEach;
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    typeof(fxr),
                    MethodBase.GetCurrentMethod(),
                    error,
                    "Ошибка перебора цикла");
            }
            return self;
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> self, Action<int,T> act = null)
        {
            var forEach = self as T[] ?? self.ToArray();
            if (self != null && act != null)
            {
                for (int i = 0; i < self?.Count(); i++)
                {
                    act(i, forEach[i]);
                }
            }
            return forEach;
        }

        public static IEnumerable<T> AddRet<T>(this IEnumerable<T> self, T value)
        {
            var result = self as List<T> ?? self?.ToList() ?? new List<T>();
            result.Add(value);
            return result;
        }

        public static IEnumerable<T> AddRangeRet<T>(this IEnumerable<T> self, IEnumerable<T> value)
        {
            var result = self as List<T> ?? self?.ToList() ?? new List<T>();
            result.AddRange(value);
            return result;
        }

        public static string Join(this IEnumerable<string> self, string sep)
        {
            return string.Join(sep, self);
        }

        public static string Join(this IEnumerable<string> self, char sep)
        {
            return string.Join(new string(new[] {sep}), self);
        }

        /// <summary>
        ///     функция генерирующая новый список если список равен NULL или пуст
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static IEnumerable<T> IfEmpty<T>(this IEnumerable<T> self,
            Func<IEnumerable<T>> action = null)
        {
            if (self == null || !(self as T[] ?? self.ToArray()).Any())
                return action?.Invoke() ?? self;
            return self;
        }
    }
}