﻿#region

using System;

#endregion

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static bool IfTrue(this bool self, Action act = null)
        {
            if (self)
            {
                act?.Invoke();
            }
            return self;
        }
    }
}