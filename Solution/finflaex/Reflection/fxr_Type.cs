﻿#region

using System;
using System.Linq;
using System.Reflection;

#endregion

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static object Create(this Type self, params object[] param)
            => param.Length == 0 ? Activator.CreateInstance(self) : Activator.CreateInstance(self, param);

        public static object RunMethod(this Type self, string name, params object[] param)
        {
            return self.InvokeMember(
                name,
                BindingFlags.DeclaredOnly |
                BindingFlags.Public |
                BindingFlags.NonPublic |
                BindingFlags.Instance |
                BindingFlags.CreateInstance,
                null,
                null,
                param);
        }

        public static object GetPropertyValue(this Type self, string name)
        {
            return self
                .GetProperties()
                .FirstOrDefault(v => v.Name == name)?
                .GetMethod?
                .Invoke(null, new object[0]);
        }

        public static Type GetPropertyValue(this Type self, string name, out object value)
        {
            value = self.GetPropertyValue(name);
            return self;
        }

        public static Type GetPropertyValue(this Type self, string name, Action<object> action)
        {
            action?.Invoke(self.GetPropertyValue(name));
            return self;
        }

        public static Type SetPropertyValue(this Type self, string name, object value)
        {
            self
                .GetProperties()
                .FirstOrDefault(v => v.Name == name)?
                .SetMethod?
                .Invoke(null, new[] {value});
            return self;
        }
    }
}