﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.ReflectionModel;
using System.Linq;

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static IEnumerable<Type> GeTypes(this CompositionContainer self)
        {
            return self
                .Catalog
                .Parts
                .Where(part => part.ExportDefinitions.Any(e => e.Metadata.ContainsKey("ExportTypeIdentity")))
                .Select(part => ReflectionModelServices.GetPartType(part).Value);
        }
    }
}