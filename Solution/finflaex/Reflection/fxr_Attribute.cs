﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static Attribute[] GetAttributesOfProperty<T, P>(this T self, Expression<Func<T, P>> action)
        {
            return Attribute.GetCustomAttributes(((MemberExpression)action.Body).Member);
        }

        public static Attribute[] GetAttributesOfProperty<T>(this T self, string name)
        {
            var prop = self.GetPropertiInfos().FirstOrDefault(v => v.Name == name);
            return self.GetAttributesOfProperty(prop);
        }

        public static Attribute[] GetAttributesOfProperty<T>(this T self, PropertyInfo propinfo)
        {
            return propinfo.GetCustomAttributes(true).OfType<Attribute>().ToArray();
            //return Attribute.GetCustomAttributes(propinfo, typeof(T));
        }

        public static Tt[] GetAttributesOfProperty<Tt,To>(this Type @this, 
            Expression<Func<To, object>> action)
            where Tt : Attribute
        {
            var propInfo = action
                .Body
                .AsType<MemberExpression>()
                .Member
                .AsType<PropertyInfo>();

            var firstOrDefault = @this
                .GetProperties()
                .FirstOrDefault(v => v.Name == propInfo?.Name);

            if (firstOrDefault == null) return new Tt[0];
            return firstOrDefault.GetAttributesOfProperty().OfType<Tt>().ToArray();
        }

        public static Tt[] GetAttributesOfMethod<Tt, D>(this Type @this,
            Expression<D> action)
            where Tt : Attribute
        {
            var methInfo = action
                    .Body.AsType<UnaryExpression>()
                    .Operand.AsType<MethodCallExpression>()
                    .Object.AsType<ConstantExpression>()
                    .Value.AsType<MethodInfo>();

            methInfo = @this
                .GetMethods()
                .FirstOrDefault(v => v.Name == methInfo.Name);

            if (methInfo == null) return new Tt[0];

            return methInfo.GetCustomAttributes(typeof(Tt)).OfType<Tt>().ToArray();
        }
    }
}