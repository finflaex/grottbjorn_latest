﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Finflaex.Reflection
{
    public class fxcExceptionTrace
    {
        public StackFrame frame;

        public fxcExceptionTrace(StackFrame frame)
        {
            this.frame = frame;
        }

        public int Column { get; internal set; }
        public string File { get; internal set; }
        public int Line { get; internal set; }
        public string Method { get; internal set; }
    }

    partial class fxr
    {
        public static fxcExceptionTrace[] ToTraces(this Exception self)
        {
            return new StackTrace(self)
                .GetFrames()?
                .Select(frame => new fxcExceptionTrace(frame)
                {
                    File = frame.GetFileName(),
                    Column = frame.GetFileColumnNumber(),
                    Line = frame.GetFileLineNumber(),
                    Method = frame.GetMethod().ToString(),
                })
                .ToArray();
        }
    }
}