﻿using System;

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static bool Has<T>(this Enum type, T value)
        {
            try
            {
                return (((long)(object)type & (long)(object)value) == (long)(object)value);
            }
            catch
            {
                return false;
            }
        }

        public static T Add<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)(((long)(object)type | (long)(object)value));
            }
            catch (Exception ex)
            {
                throw new ArgumentException($"Could not append value from enumerated type '{typeof(T).Name}'.", ex);
            }
        }


        public static T Remove<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)(((long)(object)type & ~(long)(object)value));
            }
            catch (Exception ex)
            {
                throw new ArgumentException($"Could not remove value from enumerated type '{typeof(T).Name}'.", ex);
            }
        }
    }
}