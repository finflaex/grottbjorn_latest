﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Reflection;
using Newtonsoft.Json;
using NUnit.Framework;

#endregion

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static R SelectSingle<T, R>(this T self, Func<T, R> act = null)
            where R : class
            => act?.Invoke(self);

        public static R SelectInstance<R>(this object @this)
            => Activator.CreateInstance(typeof (R), @this).AsType<R>();

        public static T RetSingle<T>(this T self, Func<T, T> act = null)
            where T : class
        {
            return act?.Invoke(self);
        }

        public static R AsType<R>(this object self)
        {
            if (self is DBNull)
                return default(R);
            return (R) self;
        }

        public static object ChangeType(this object self, Type type)
            => Convert.ChangeType(self, type);

        public static T ChangeType<T>(this object self)
            => (T) Convert.ChangeType(self, typeof (T));

        public static object RunMethod(this object self, string name, params object[] param)
        {
            var methods = self
                .GetType()
                .GetMethods(
                    BindingFlags.DeclaredOnly |
                    BindingFlags.Public |
                    BindingFlags.NonPublic |
                    BindingFlags.Instance |
                    BindingFlags.CreateInstance |
                    BindingFlags.Static
                );

            return (
                methods
                    .Where(method =>
                    {
                        return method.Name.Compare(name);
                    })
                    .Select(method =>
                    {
                        return new {method, parametrs = method.GetParameters()};
                    })
                    .Where(@t =>
                    {
                        return @t.parametrs.Length == param.Length;
                    })
                    .Select(@t =>
                    {
                        return @t.method.Invoke(self, param.Length == 0 ? null : param);
                    })
                ).FirstOrDefault();

            //var type = self.GetType();
            //var method = type.GetMethod(name, BindingFlags.DeclaredOnly |
            //BindingFlags.Public | BindingFlags.NonPublic |
            //BindingFlags.Instance | BindingFlags.CreateInstance | BindingFlags.Static);
            //if (method == null || method.ContainsGenericParameters) return null;
            //return method.Invoke(self, method.GetParameters().Any() ? param : new object[0]);

            //return method != null && !method.ContainsGenericParameters ? method.Invoke(self, param) : null;
        }

        public static object GetPropertyValue(this object self, string name)
            => self.GetType().GetRuntimeProperties().FirstOrDefault(v => v.Name == name)?.GetValue(self);

        public static object GetFieldValue(this object self, string name)
            => self.GetType().GetRuntimeFields().FirstOrDefault(v => v.Name == name)?.GetValue(self);

        public static T Action<T>(this T self, Action<T> act = null)
            where T : class 
        {
            act?.Invoke(self);
            return self;
        }

        public static R Action<T,R>(this T self, Func<T,R> act = null)
            where T : class
            where R : class 
        {
            return act?.Invoke(self);
        }

        public static string ToJson(this object self, bool format = false)
            => self == null
                ? string.Empty
                : JsonConvert.SerializeObject(self, format ? Formatting.Indented : Formatting.None);

        [Test]
        public static void Test_Object_ToJson()
        {
            object test = null;
            var read = test.ToJson();
            Assert.AreEqual(read, string.Empty, "если null вернуть пустую строку");
        }

        public static T FromJson<T>(this string self) 
            => self == null 
            ? default(T) 
            : JsonConvert.DeserializeObject<T>(self);

        [Test]
        public static void Test_Object_FromJson()
        {
            string test = null;
            try
            {
                var read = test.FromJson<object>();
            }
            catch (Exception)
            {
                Assert.Fail("если NULL вернуть default(T)");
            }

        }

        public static bool CompareExt<T>(this T self, T value)
        {
            return typeof (T) == typeof (double)
                ? Math.Abs(self.AsType<double>() - value.AsType<double>()) < 0.0000000001
                : typeof (T) == typeof (decimal)
                    ? Math.Abs(self.AsType<decimal>() - value.AsType<decimal>()) < 0.0000001M
                    : typeof (T) == typeof (float)
                        ? Math.Abs(self.AsType<float>() - value.AsType<float>()) < 0.0000001
                        : typeof (T) == typeof (string)
                            ? self.AsType<string>() == value.AsType<string>()
                            : self.Equals(value);
        }

        public static T IfNotNull<T>(this T self, Action<T> action = null)
            where T : class 
        {
            if (self != null)
            {
                action?.Invoke(self);
            }
            return self;
        }

        public static R IfNotNull<T,R>(this T self, Func<T,R> action = null)
            where T : class
            where R : class 
        {
            if (self != null)
            {
               return action?.Invoke(self);
            }
            return null;
        }

        public static T IfNull<T>(this T self, Action<T> action = null)
            where T : class
        {
            if (self == null)
            {
                action?.Invoke(self);
            }
            return self;
        }

        public static T IfNull<T>(this T self, Func<T, T> action = null)
            where T : class
        {
            if (self == null)
            {
                return action?.Invoke(self);
            }
            return self;
        }

        public static IEnumerable<PropertyInfo> GetPropertiInfos<T>(this T self, params string[] names)
        {
            var type = self.GetType();
            var result = type.GetProperties();
            return names.Length > 0
                ? result.Where(v => names.Contains(v.Name))
                : result;
        }

        public static void Act<T>(this T self, Action<T> action)
        {
            action?.Invoke(self);
        }

        public static R Act<T,R>(this T self, Func<T,R> action)
            where R : class 
        {
            return action?.Invoke(self);
        }

        public static T[] SingleToArray<T>(this T @this)
        {
            return new T[] {@this};
        }

        /// <summary>
        /// Возвращает ресурс который лежит в том же namespace по имени
        /// </summary>
        /// <param name="this"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static byte[] GetResource(
            this object @this,
            string name)
        {
            var asm = @this.GetType().Assembly;
            var obj_name_space = @this.GetType().Namespace ?? "none";
            var res_name = asm
                .GetManifestResourceNames()
                .Where(v => v.StartsWith(obj_name_space))
                .FirstOrDefault(v => v.ToLower().Contains(name.ToLower()));
            if (res_name == null)
            {
                return new byte[0];
            }
            return asm
                .GetManifestResourceStream(res_name)?
                .ToBytes()
                .ToArray() ?? new byte[0];
        }

        public static byte[] GetResource<T>(string name)
        {
            var asm = typeof(T).Assembly;
            var res_name = asm
                .GetManifestResourceNames()
                .FirstOrDefault(v => v.ToLower().Contains(name.ToLower()));
            if (res_name == null)
            {
                return new byte[0];
            }
            return asm
                .GetManifestResourceStream(res_name)?
                .ToBytes()
                .ToArray() ?? new byte[0];
        }

        /// <summary>
        /// Возвращает ресурс который лежит в том же namespace по имени
        /// </summary>
        /// <param name="this"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static byte[][] GetResources(
            this object @this,
            string name)
        {
            var asm = @this.GetType().Assembly;
            var obj_name_space = @this.GetType().Namespace ?? "none";
            return asm
                .GetManifestResourceNames()
                .Where(v => v.StartsWith(obj_name_space))
                .Where(v => v.ToLower().Contains(name.ToLower()))
                .Select(v=>asm.GetManifestResourceStream(v).ToBytes())
                .ToArray();
        }
    }
}