﻿#region

using System.Threading.Tasks;

#endregion

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static T WaitResult<T>(this Task<T> self)
        {
            self.Wait();
            return self.Result;
        }
    }
}