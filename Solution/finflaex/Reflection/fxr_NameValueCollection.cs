﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this NameValueCollection col)
        {
            var dict = new Dictionary<TKey, TValue>();
            var keyConverter = TypeDescriptor.GetConverter(typeof (TKey));
            var valueConverter = TypeDescriptor.GetConverter(typeof (TValue));

            foreach (string name in col)
            {
                var key = (TKey) keyConverter.ConvertFromString(name);
                TValue value;
                try
                {
                    value = (TValue) valueConverter.ConvertFromString(col[name]);
                }
                catch
                {
                    value = col[name].AsType<TValue>();
                }
                dict.Add(key, value);
            }

            return dict;
        }
    }
}