﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static IEnumerable<T> ToIEnumerable<T>(this ICollection<T> self) 
            => self.Cast<T>();
    }
}