﻿#region

using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

#endregion

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static Task<string> ReadPostAsync<T>(this T self)
            where T : Controller
            => new StreamReader(HttpContext.Current.Request.InputStream)
                .ReadLineAsync();

        public static string ReadPost<T>(this T self)
            where T : Controller
            => new StreamReader(HttpContext.Current.Request.InputStream)
                .ReadLine();

        public static string Render<T>(this T self, string viewName = null, object model = null)
            where T : Controller
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = self
                    .ControllerContext
                    .RouteData
                    .GetRequiredString("action");

            self.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(self.ControllerContext, viewName);
                var viewContext = new ViewContext(self.ControllerContext, viewResult.View, self.ViewData, self.TempData,
                    sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public static string GetCookie<T>(this T @this, string key)
            where T : Controller
        {
            return @this.HttpContext.Request.Cookies[key]?.Value;
        }
    }
}