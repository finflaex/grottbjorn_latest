﻿using System;
using System.Reflection;

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static bool IsParams(this ParameterInfo self)
        {
            return Attribute.IsDefined(self, typeof(ParamArrayAttribute));
        }
    }
}