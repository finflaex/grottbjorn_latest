﻿using System;
using System.IO;

namespace Finflaex.Reflection
{
    partial class fxr
    {
        public static byte[] ToBytes<T>(this T @this)
            where T : Stream
        {
            var result = new byte[@this.Length];
            using (var reader = new BinaryReader(@this))
            {
                reader.Read(result, 0, Convert.ToInt32(@this.Length));
            }
            return result;
        }
    }
}