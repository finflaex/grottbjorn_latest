﻿#region

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Finflaex.Reflection;

#endregion

namespace Finflaex.Forms
{
    public class fxcFormTray : Form
    {
        private IContainer components;
        public ContextMenuStrip TrayContextMenu;
        public NotifyIcon TrayIcon;

        public fxcFormTray()
        {
            AutoScaleMode = AutoScaleMode.Font;
            InitializeComponent();
            //TrayIcon.Icon = Properties.Resources.network_connections.AsType<Icon>();
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fxcFormTray));
            this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.TrayContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SuspendLayout();
            // 
            // TrayIcon
            // 
            this.TrayIcon.ContextMenuStrip = this.TrayContextMenu;
            this.TrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayIcon.Icon")));
            this.TrayIcon.Text = "notifyIcon1";
            this.TrayIcon.Visible = true;
            this.TrayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TrayIcon_MouseDoubleClick);
            // 
            // TrayContextMenu
            // 
            this.TrayContextMenu.Name = "TrayContextMenu";
            this.TrayContextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // fxcFormTray
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Name = "fxcFormTray";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.fxcFormTray_FormClosed);
            this.ResumeLayout(false);

        }

        private void TrayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Show();
                WindowState = FormWindowState.Normal;
            }
            else
            {
                Hide();
                WindowState = FormWindowState.Minimized;
            }
        }

        private void fxcFormTray_Resize(object sender, EventArgs e)
        {
            ShowInTaskbar = WindowState != FormWindowState.Minimized;
        }

        private void fxcFormTray_FormClosed(object sender, FormClosedEventArgs e)
        {
            TrayIcon.Visible = false;
            TrayIcon.Dispose();
        }
    }
}