﻿#region

using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using Finflaex.Reflection;

#endregion

namespace Finflaex.XML
{
    public class fxcXmlValidator
    {
        public readonly List<XmlSchemaException> Errors = new List<XmlSchemaException>();

        public fxcXmlValidator(string xsd, string xml)
        {
            var xmlXsd = XmlReader.Create(xsd.ToStream());

            var sc = new XmlSchemaSet();
            sc.Add(null, xmlXsd);

            var settings = new XmlReaderSettings
            {
                ValidationType = ValidationType.Schema,
                Schemas = sc
            };

            settings.ValidationEventHandler += (sender, error) => { Errors.Add(error.Exception); };
            var reader = XmlReader.Create(xml.ToStream(), settings);
            while (reader.Read()) ;
        }
    }
}