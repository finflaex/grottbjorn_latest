/****** Скрипт для команды SelectTopNRows из среды SSMS  ******/
SELECT 
	C.ClientID,
	C.LastName,
	C.FirstName,
	C.MiddleName,
	D.SeriesNumber + ' ' + D.DocumentNumber as doc,
	C.DateOfBirth
  FROM [Clients].[dbo].[Clients] C
  left join [Clients].[dbo].[Documents] D on D.ClientID = C.ClientID
  where
  PersonTypeID = 1
  and ClientTypeID = 2
  and D.DocumentTypeID = 1
  and C.Status = 1