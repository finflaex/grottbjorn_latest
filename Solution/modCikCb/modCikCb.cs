﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DESDB;
using DESDB.Tables;
using Finflaex.fxMVC.Authorization;
using Finflaex.fxMVC.File;
using Finflaex.fxMVC.WebSocket;
using Finflaex.HtmlDom;
using Finflaex.Log;
using Finflaex.MsSQL.Connection;
using Finflaex.MsSQL.Tables;
using Finflaex.Reflection;
using Finflaex.Types;
using Finflaex.WS;
using Finflaex.XML;
using modCikCb.Models;
using System.Diagnostics;

#endregion

namespace modCikCb
{
    [Export(typeof (IController))]
    public class modCikCb : WSController
    {
        public const string RootCatalog = "CikCb";

        [Roles("sdl")]
        public async Task<ActionResult> Form() => await DoAsync(ws =>
        {
            var upload = Plugin("FileUpload", plugin =>
            {
                plugin.Body = new fxcHtmlTag(fxeHtmlTag.input)
                    .AddAttribute("type", "file")
                    .AddAttribute("multiple", true)
                    .AddClass("file");
                return plugin.GetContent(new
                {
                    uploadUrl = $"{GetType().Name}/FileUpload",
                    allowedFileExtensions = new[] {"xml", "enc", "sgn"},
                    maxFilesNum = 1,
                    //autoReplace = true,
                    //uploadAsync = true,
                    showPreview = false,
                    //showBrowse = false,
                    showCancel = false,
                    browseOnZoneClick = true,
                    showUploadedThumbs = false,
                    removeFromPreviewOnError = true,
                    fileActionSettings = new
                    {
                        showZoom = false
                    },
                });
            });

            var table = Plugin("DataTable", plugin =>
            {
                plugin.Body = new fxcHtmlTag(fxeHtmlTag.table)
                    .AddClass(
                        "table",
                        "table-bordered",
                        "table-striped",
                        "table-hover",
                        "dt-responsive",
                        "non-responsive",
                        "dataTable",
                        "no-footer",
                        "collapsed",
                        "dtr-inline")
                    .AddContent(
                        new fxcHtmlTag(fxeHtmlTag.thead,
                            new fxcHtmlTag(fxeHtmlTag.tr,
                                new fxcHtmlTag(fxeHtmlTag.th, "Имя"),
                                new fxcHtmlTag(fxeHtmlTag.th, "Дата запроса"),
                                new fxcHtmlTag(fxeHtmlTag.th, "Дата обработки"),
                                new fxcHtmlTag(fxeHtmlTag.th, "Запрос"),
                                new fxcHtmlTag(fxeHtmlTag.th, "Результат")
                                )));
                return plugin.GetContent(new
                {
                    ajax = new
                    {
                        url = $"{GetType().Name}/DataTable",
                        type = "POST"
                    },
                    autoWidth = false,
                    columns = new[]
                    {
                        new {width = 100, orderable = true },
                        new {width = 100, orderable = true },
                        new {width = 150, orderable = true },
                        new {width = 250, orderable = true },
                        new {width = 400, orderable = true },
                    }
                });
            });

            return this
                .GetResource("index")
                .ToString(Encoding.UTF8)
                .Replace(new Dictionary<string, string>()
                {
                    {"<!--upload-->", upload},
                    {"<!--table-->", table}
                })
                .SelectSingle(Content);
        });

        [Roles("sdl")]
        public async Task<ActionResult> FileUpload()
        {
            // грузим файлы
            var file = this.LoadFiles().FirstOrDefault();
            
            return await DoAsync(ws =>
            {
                try
                {
                    // проверяем файл
                    var file_request = CreateFileRequest(file);
                    tFile file_response;
                    // получаем данные
                    var xml = file.Data.ToString(Encoding.UTF8);
                    // проверяем данные
                    var quitance = CreateQuitance(file, xml);

                    RequestFile response = null;
                    if (quitance == null)
                    {
                        // если жалоб нет, то сравниваем данные
                        response = CreateResponse(xml);

                        // если совпадений нет
                        if (!response.Persons.Any())
                        {
                            quitance = EmptyFormat(response.FileName);
                            file_response = CreateFileResponse(response, quitance);
                        }
                        else
                        {
                            file_response = CreateFileConfirmResponse(response);
                        }
                    }
                    else
                    {
                        // пишем результаты проверки
                        response = new RequestFile
                        {
                            FileName = new RequestFileName(file_request.Name),
                        };
                        file_response = CreateFileResponse(response, quitance);
                    }


                    // пишем к себе в базу отчет о проделанной работе
                    GDB.Transact(db =>
                    {
                        //var name = $"{response.FileName.Date.ToString("yyyy")}_{response.FileName.Number}";
                        var user = db.Users.FirstOrDefault(v => v.sysGuid == User.Guid);

                        var directory_root = db
                            .Directories
                            .FirstOrDefault(v
                                => v.Name == RootCatalog
                                   && v.sysStatus == fxeSysStatus.actual)
                                             ?? db.Directories.Add(new tDirectory()
                                             {
                                                 Name = "CikCb",
                                                 UserChange = user,
                                             });

                        var name_year = response.FileName.Date.ToString("yyyy");

                        var directory_year = db
                            .Directories
                            .FirstOrDefault(v
                                => v.Name == name_year
                                   && v.sysStatus == fxeSysStatus.actual)
                                             ?? db
                                                 .Directories
                                                 .Add(new tDirectory()
                                                 {
                                                     Name = name_year,
                                                     UserChange = user,
                                                     Root = directory_root,
                                                 });

                        var name_order = response.FileName.Number.ToString("0000");

                        var directory_order = db
                            .Directories
                            .FirstOrDefault(v
                                => v.Name == name_order
                                   && v.sysStatus == fxeSysStatus.actual)
                                              ?? db
                                                  .Directories
                                                  .Add(new tDirectory()
                                                  {
                                                      Name = name_order,
                                                      UserChange = user,
                                                      Root = directory_year
                                                  });

                        file_request.sysStartDT = response.FileName.Date.Date;

                        var old_request = directory_order
                            .Files
                            .FirstOrDefault(v
                                => v.sysStatus == fxeSysStatus.actual
                                   && v.Name == file_request.Name
                            );

                        if (old_request == null)
                        {
                            file_request.UserChange = user;
                            directory_order.Files.Add(file_request);
                        }
                        else
                        {
                            old_request = db.Files.Change(old_request.sysGuid);
                            old_request.sysPrev.UserChange
                                = old_request.UserChange
                                    = user;
                            old_request.Ext = file_request.Ext;
                            old_request.sysStartDT = file_request.sysStartDT;
                            old_request.Content = file_request.Content;
                            old_request.Size = file_request.Size;
                        }

                        //var old_response = directory_order
                        //    .Files
                        //    .FirstOrDefault(v
                        //        => v.sysStatus == fxeSysStatus.actual
                        //           && v.Name == file_response.Name);

                        //if (old_response == null)
                        //{
                            directory_order.Files.Add(file_response);
                            
                            file_response.UserChange = user;
                        //}
                        //else
                        //{
                        //    old_response = db.Files.Change(old_response.sysGuid);
                        //    old_response.sysPrev.UserChange
                        //        = old_response.UserChange
                        //            = user;
                        //    old_response.Ext = file_response.Ext;
                        //    old_response.sysStartDT = file_response.sysStartDT;
                        //    old_response.Content = file_response.Content;
                        //    old_response.Size = file_response.Size;
                        //}

                        db.SaveChanges();
                    });

                    // выводим информацию пользователю
                    ws.Send(fxsJsonResponse.Eval("table.ajax.reload()"));
                }
                catch (Exception error)
                {
                    // выводим сообщение о непредвиденной ошибке
                    ws.Send(fxsJsonResponse.Notify("ошибка"));
                    ws.Send(error.ToJson());
                    return Json(error);
                }

                return Json(new object());
            });
        }

        /// <summary>
        /// Оформляем ответ
        /// </summary>
        /// <param name="response"></param>
        /// <param name="quitance"></param>
        /// <returns></returns>
        private tFile CreateFileResponse(RequestFile response, Quitance quitance)
        {
            var data = quitance == null
                ? response.ToString().ToBytes(Encoding.UTF8)
                : quitance.ToString().ToBytes(Encoding.UTF8);

            return new tFile()
            {
                Content = data,
                UserChange = User.Guid.SelectSingle(GDB.LoadUser),
                Ext = "xml",
                Name = quitance == null
                    ? response.FileName.ToString()
                    : quitance.MessageID,
                Size = data.Length,
            };
        }

        private tFile CreateFileConfirmResponse(RequestFile response)
        {
            var num = RequestFileName.request.ToRegex().Matches(response.FileName).ToArray()[4];
            var name = $"D{GDB.MyOrg.OGRN.ToString("0000000000000")}_{DateTime.Now:ddMMyy_HHmmss}_Z_{num}";

            var data = response.ToXml().ToBytes(Encoding.UTF8);

            return new tFile()
            {
                Content = data,
                UserChange = User.Guid.SelectSingle(GDB.LoadUser),
                Ext = "xml",
                Name = name,
                Size = data.Length,
            };
        }

        /// <summary>
        /// Сравнение присланных данных с нашими
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private RequestFile CreateResponse(string xml)
        {
            // парсим файл
            var request = xml.FromXml<RequestFile>();
            // грузим запрос списка клиентов
            var CikCbQuery_ListClients = this.GetResource("CikCbQuery_ListClients").ToString(Encoding.UTF8);
            // грузим запрос данных по клиенту
            var CikCbQuery_DataOfClient = this.GetResource("CikCbQuery_DataOfClient").ToString(Encoding.UTF8);
            // строка подключения для выполнения запросов
            var connection_string = "Data Source=10.0.0.120;Integrated Security=False;User ID=WebMars;Password=webmars111;";

            fxcConnection.Connect(connection_string, connect =>
            {
                request.Persons = connect
                    .ExecuteReader(CikCbQuery_ListClients)
                    .AsParallel()
                    .Where(read => !read.OfType<DBNull>().Any())
                    .Select(read => new
                    {
                        id = read[0].ToString(),
                        surname = read[1].ToString(),
                        name = read[2].ToString(),
                        patronymic = read[3].ToString(),
                        passport = read[4].ToString().Replace(" ", string.Empty),
                        birthday = read[5].ToString().Substring(0, 10)
                    })
                    .SelectMany(read =>
                        from person in request.Persons
                        where person.PersonInfo.Document.Type.Code == 21
                        select new
                        {
                            read,
                            person,
                            compare = new
                            {
                                name = person.PersonInfo.FIOD.Name,
                                patronymic = person.PersonInfo.FIOD.Patronymic,
                                surname = person.PersonInfo.FIOD.Surname,
                                birthday = person.PersonInfo.FIOD.DateBirthday,
                                passport = $"{person.PersonInfo.Document.Serial}{person.PersonInfo.Document.Number}".Replace(" ", string.Empty)
                            }
                        })
                    .Where(data =>
                    {
                        var cen = data.compare.name.Compare(data.read.name);
                        var cep = data.compare.patronymic.Compare(data.read.patronymic);
                        var ces = data.compare.surname.Compare(data.read.surname);
                        var ceb = data.compare.birthday.Compare(data.read.birthday);
                        var ced = data.compare.passport.Compare(data.read.passport);
                        return ced == 1 && ceb == 1 && cen > 0.7 && cep > 0.7 && ces > 0.7;
                    })
                    .ToArray()
                    .Action(v=>
                    {
                        connect.Close();
                    })
                    .Select(read =>
                    {
                        connect.Open();

                        var secs = connect
                                    .ExecuteReader(CikCbQuery_DataOfClient, new fxcDictSO
                                    {
                                        ["@ClientID"] = read.read.id,
                                        ["@Date"] = request.Query.Date.AddDays(1),
                                    })
                                    .Select(sec =>
                                    {
                                        decimal price = 0;
                                        decimal.TryParse(sec[7].ToString().Replace('.',','), out price);
                                        var result = new Securities
                                        {
                                            Organization = new Organization
                                            {
                                                Name = sec[5].ToString(),
                                                INN = sec[6].ToString()
                                            },
                                            Paper = new Paper
                                            {
                                                Price = price,
                                                Count = sec[3].ToString(),
                                                Type = "91"
                                            },
                                            PaperID = ""
                                        };
                                        return result;
                                    })
                                    .ToArray();

                        connect.Close();

                        return new Person
                        {
                            PersonInfo = read.person.PersonInfo,
                            Guid = read.person.Guid,
                            ServiceInfo = read.person.ServiceInfo,
                            Account = read.person.Account,
                            Securities = secs
                        };
                    })
                    .Where(read => read.Securities.Any())
                    .ToArray();
            });

            request.OraganizationInfo = new OraganizationInfo
            {
                Address = "620062, г. Екатеринбург, пр. Ленина, 101/2",
                Email = "gb@grottbjorn.com",
                FIO = "Лебедев Виктор Юрьевич",
                INN = 6660040152,
                OGRN = GDB.MyOrg.OGRN,
                Name = "Grottbjorn",
                Phone = "+7(343)365-44-11",
            };

            return request;
        }

        // проверка xml на xsd
        private Quitance CreateQuitance(MvcFile file, string xml)
        {

            var name = file.Name.GetFileNameWithoutExtension();

            var name_validate = new RequestFileName(name);
            if (name_validate.Complited &&
                (name_validate.Type == TypeRequest.Packet || name_validate.Type == TypeRequest.Personal))
            {
                // если файл зашифрован, то материмся на пьяного шифровщика
                if (name.EndsWith("enc")) return ErrorEnc(name_validate);

                // если файл с подписью, то материмся на сертификат
                if (name.EndsWith("sig")) return ErrorSig(name_validate);

                // грузима правила
                var VO_CIK_CB_7 = this.GetResource("VO_CIK_CB_7").ToString(Encoding.UTF8);

                try
                {
                    var body_validate = new fxcXmlValidator(VO_CIK_CB_7, xml);

                    // проверяем соответствует ли файл всем правилам
                    if (body_validate.Errors.Any())
                    {
                        // если не соответствует то тукаем носом в места несоответствия
                        return ErrorFormat(name_validate,
                            body_validate.Errors
                                .Select(error => $"[{error.LineNumber},{error.LinePosition},{error.Message}]")
                                .Join(";"));
                    }
                }
                catch (Exception err)
                {
                    // если вообще чтото левое, то закатываем глаза и падаем в обморок
                    return ErrorFormat(name_validate, err.Message);
                }
            }
            return null;
        }

        // проверка файла на файловость
        private tFile CreateFileRequest(MvcFile file)
        {
            if (file == null) throw new Exception("файл не загружен");
            var name = new RequestFileName(file.Name);

            return new tFile()
            {
                Content = file.Data,
                UserChange = User.Guid.SelectSingle(GDB.LoadUser),
                Ext = file.Name.GetFileExtension(),
                //Name = file.Name.GetFileNameWithoutExtension(),
                Name = name.ToString(),
                Size = file.Size,
            };
        }

        [Roles("sdl")]
        public async Task<FileResult> FileDownload(string id)
        {
            return await Task.Run(() =>
            {
                return GDB.Ret(db =>
                {
                    var file = db.Files.Find(id.ToGuid());

                    return File(
                        file?.Content ?? "файл не найден".ToBytes(Encoding.UTF8),
                        MediaTypeNames.Application.Octet,
                        $"{file?.Name ?? "unknown"}.{file?.Ext ?? "txt"}");
                });
            });
        }

        /// <summary>
        /// Таблица предыдущих действий
        /// </summary>
        /// <returns></returns>
        public JsonResult DataTable()
        {
            return GDB.Ret(db =>
            {
                var query =
                    from root in db.Directories
                    where root.Name == RootCatalog
                          && root.sysStatus == fxeSysStatus.actual

                    from year in root.Directories
                    where year.sysStatus == fxeSysStatus.actual
                    orderby year.Name descending

                    from order in year.Directories
                    where order.sysStatus == fxeSysStatus.actual
                    orderby order.Name descending

                    //from file in order.Files
                    //where file.sysStatus == fxeSysStatus.actual

                    select new
                    {
                        Name = year.Name + "_" + order.Name,
                        Query = order.Files.Where(file
                            => file.sysStatus == fxeSysStatus.actual
                               && file.Name.StartsWith("F"))
                               .Select(f=>new
                               {
                                   start = f.sysStartDT,
                                   name = f.Name,
                                   ext = f.Ext,
                                   guid = f.sysGuid
                               })
                               .FirstOrDefault(),
                        Quitance = order.Files.Where(file
                            => file.sysStatus == fxeSysStatus.actual
                               && file.Name.StartsWith("D"))
                               .OrderByDescending(v=>v.sysStartDT)
                               .Select(f => new
                               {
                                   start = f.sysStartDT,
                                   name = f.Name,
                                   ext = f.Ext,
                                   guid = f.sysGuid
                               })
                               .FirstOrDefault(),
                    };

                var list = query
                    .OrderByDescending(v=>v.Name)
                    .AsEnumerable()
                    .Select(v => new[]
                    {
                        v.Name,

                        v.Query.start.ToString("yyyy/MM/dd"),

                        v.Quitance.start.ToString("yyyy/MM/dd HH:mm"),

                        "<a href='" + GetType().Name + "/FileDownload/" + v.Query.guid.ToString("N") +
                        "' download class='btn btn-link'>" + v.Query.name + "." + v.Query.ext + "</a>",

                        "<a href='" + GetType().Name + "/FileDownload/" + v.Quitance.guid.ToString("N") +
                        "' download class='btn btn-link'>" + v.Quitance.name + "." + v.Quitance.ext + "</a>"
                    })
                    .ToArray();

                return Json(new { data = list });
            });
        }

        #region WROKER

        private static Quitance ErrorFormat(
            RequestFileName ID,
            string message = "default",
            int code = 1003)
        {
            return new Quitance
            {
                AcknowledgementType = FileAcknowledgementType.Item8,
                ResultCode = code.ToString("0000"),
                ResultText = message,
                To = ID.SourceStringFullID,
                From = $"D{GDB.MyOrg.OGRN.ToString("0000000000000")}",
                CorrelationMessageID = ID
            };
        }

        private static Quitance ErrorEnc(
            RequestFileName ID,
            string message = "Невозможно расшифровать файл обмена",
            int code = 1002)
        {
            return new Quitance
            {
                AcknowledgementType = FileAcknowledgementType.Item8,
                ResultCode = code.ToString("0000"),
                ResultText = message,
                To = ID.SourceStringFullID,
                From = $"D{GDB.MyOrg.OGRN.ToString("0000000000000")}",
                CorrelationMessageID = ID
            };
        }

        private static Quitance ErrorSig(
            RequestFileName ID,
            string message = "Некорректная электронная подпись",
            int code = 1001)
        {
            return new Quitance
            {
                AcknowledgementType = FileAcknowledgementType.Item8,
                ResultCode = code.ToString("0000"),
                ResultText = message,
                To = ID.SourceStringFullID,
                From = $"D{GDB.MyOrg.OGRN.ToString("0000000000000")}",
                CorrelationMessageID = ID
            };
        }

        private static Quitance EmptyFormat(
            RequestFileName ID,
            string message = "Совпадения отсутствуют")
        {
            return new Quitance
            {
                AcknowledgementType = FileAcknowledgementType.Item10,
                ResultCode = 1000.ToString("0000"),
                ResultText = message,
                To = ID.SourceStringFullID,
                From = $"D{GDB.MyOrg.OGRN.ToString("0000000000000")}",
                CorrelationMessageID = ID
            };
        }

        #endregion
    }

    public static class stringExt
    {
        public static string RetIfNull(
            this string @this, string value)
            => string.IsNullOrWhiteSpace(@this)
            ? value : @this;

        public static double Compare(
            this string @this, 
            string value)
        {
            var val1 = @this
                .ToLower()
                .Replace(new Dictionary<string, string>
                {
                    ["ё"] = "e",
                    ["  "] = " "
                });

            var val2 = value
                .ToLower()
                .Replace(new Dictionary<string, string>
                {
                    ["ё"] = "e",
                    ["  "] = " "
                });

            return val1 == null || val2 == null
                ? 1
                : val1.CompareExt(val2);
        }
    }
}