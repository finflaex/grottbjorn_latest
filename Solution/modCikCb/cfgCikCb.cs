using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Finflaex.fxMVC.Authorization;
using Finflaex.Plugin;
using Finflaex.Reflection;
using Finflaex.WS;

namespace modCikCb
{
    [Export(typeof (fxiMvcModul))]
    public class cfgCikCb : fxiMvcModul
    {
        public string Name => "CikCb";

        [Roles("sdl")]
        public IEnumerable<fxiModulMenuItem> Menu
            => new List<fxiModulMenuItem>
            {
                new fxaModulMenuItem
                {
                    Caption = "��������",
                    Icon = "fa fa-cloud-upload",
                    Submenu = new []
                    {
                        new fxaModulMenuItem
                        {
                            Caption = "���",
                            Action = "WsLib.request("
                                     + fxsJsonResponse
                                         .LoadHtmlPost($"{nameof(modCikCb)}/Form", "#page")
                                         .ToJson()
                                     + ")"
                        }
                    }
                }
            };

        public string Version => "start";

        public DateTime? DataCreate => new DateTime(12, 08, 2016);

        public int Prioritet => 0;

        public object Execute(object data) => null;
    }
}