﻿#region

using System;
using System.Linq;
using System.Reflection;
using System.Text;
using DESDB;
using Finflaex.Log;
using Finflaex.MsSQL.Connection;
using Finflaex.Reflection;
using Finflaex.Types;
using Finflaex.XML;
using modCikCb.Models;

#endregion

namespace modCikCb
{
    public class CikCbWorker
    {
        public static string VO_CIK_CB_7 => new CikCbWorker().GetResource("VO_CIK_CB_7").ToString(Encoding.UTF8);
        public static string VO_CIK_CB_K_6 => new CikCbWorker().GetResource("VO_CIK_CB_K_6").ToString(Encoding.UTF8);

        public static object Query(
            string connection_string,
            string sql_client,
            string sql_securities,
            string file_name,
            string xml)
        {
            var request_validate = new RequestFileName(file_name);

            // если имя файла соответствует шаблону
            // и тип запроса пакетный или персональный
            if (request_validate.Complited
                && (request_validate.Type == TypeRequest.Packet || request_validate.Type == TypeRequest.Personal))
            {
                if (file_name.GetFileExtension().EndsWith("enc"))
                    return ErrorEnc(request_validate);

                if (file_name.GetFileExtension().EndsWith("sig"))
                    return ErrorSig(request_validate);

                if (xml.Equals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"))
                    return ErrorFormat(request_validate, "Отсутствует корневой элемент");

                file_name = file_name.GetFileNameWithoutExtension();

                // переходим к проверке структуры
                var validate = new fxcXmlValidator(VO_CIK_CB_7, xml);

                // проверка на наличие ошибок
                if (validate.Errors.Any())
                {
                    // возвращаем квитанцию об ошибке в структуре
                    return ErrorFormat(request_validate,
                        validate
                            .Errors
                            .Select(error => $"[{error.LineNumber},{error.LinePosition},{error.Message}]")
                            .Join(";"));
                }

                // получаем данные запроса в привычном виде
                var request = xml.FromXml<RequestFile>();

                // соединяемся с базой данных и делаем обработку
                fxcConnection.Connect(connection_string, connect =>
                {
                    request.Persons = connect
                        .ExecuteReader(sql_client, new fxcDictSO
                        {
                            ["@data"] = request.Query.Date.AddDays(1)
                        })
                        .AsParallel()
                        .Select(read =>
                        {
                            if (read.OfType<DBNull>().Any())
                            {
                                fxsLog.Write(
                                    fxeLog.Error,
                                    fxeLog.UserInfo,
                                    typeof (CikCbWorker),
                                    MethodBase.GetCurrentMethod(),
                                    $"Клиент с ИД = {read[0]} не участвует в проверке потому, что содержит нулевые поля",
                                    new fxcDictSO
                                    {
                                        ["sql"] =
                                            sql_client.Replace("\r", string.Empty)
                                                .Replace("\n", string.Empty)
                                                .Replace("\t", string.Empty),
                                        ["record"] = read.ToJson()
                                    });
                                return null;
                            }
                            return new
                            {
                                id = read[0].AsType<int>(),
                                patronymic = read[1].ToString().ToLower().Replace("ё", "е"),
                                name = read[2].ToString().ToLower().Replace("ё", "е"),
                                surname = read[3].ToString().ToLower().Replace("ё", "е"),
                                passport = read[4].ToString().Replace(" ", ""),
                                birthday = read[5].ChangeType<DateTime>().Date
                            };
                        })
                        .WhereNotNull()
                        .Select(read =>
                        {
                            return request
                                .Persons
                                .Select(person =>
                                {
                                    var ven = person.PersonInfo.FIOD.Name.ToLower().Replace("ё", "е");
                                    var vep = person.PersonInfo.FIOD.Surname.ToLower().Replace("ё", "е");
                                    var ves = person.PersonInfo.FIOD.Patronymic.ToLower().Replace("ё", "е");
                                    var ved =
                                        (person.PersonInfo.Document.Serial + person.PersonInfo.Document.Number).Replace(
                                            " ", "");
                                    var veb =
                                        person.PersonInfo.FIOD.DateBirthday.ToDateTime("dd.MM.yyyy")
                                            .AsType<DateTime>()
                                            .Date;

                                    var cen = ven.CompareExt(read.name);
                                    var cep = vep.CompareExt(read.surname);
                                    var ces = ves.CompareExt(read.patronymic);
                                    var ced = ved.CompareExt(read.passport);

                                    if (person.PersonInfo.Document.Type?.Code != 21
                                        || ced < 1
                                        || cen < 0.7 || cep < 0.7 || ces < 0.7
                                        || read.birthday != veb)
                                        return null;

                                    person.Securities = connect
                                        .ExecuteReader(sql_securities, new fxcDictSO
                                        {
                                            ["@clientid"] = read.id
                                        })
                                        .Select(sec => new Securities
                                        {
                                            Organization = new Organization
                                            {
                                                Name = sec[0].AsType<string>(),
                                                INN = sec[0].AsType<string>()
                                            },
                                            Paper = new Paper
                                            {
                                                Price = sec[0].AsType<decimal>()
                                            },
                                            PaperID = ""
                                        })
                                        .ToArray();

                                    return person.Securities.Length > 0
                                        ? person
                                        : null;
                                })
                                .FirstOrDefault(v => v != null);
                        })
                        .WhereNotNull()
                        .ToArray();
                });

                if (request.Persons.Any())return request;

                return EmptyFormat(request_validate);
            }

            var quitance_validate = new QuitanceFileName(file_name);
            if (quitance_validate.Complited)
            {
                var validate = new fxcXmlValidator(VO_CIK_CB_K_6, xml);
                if (validate.Errors.Any())
                {
                    return ErrorFormat(request_validate,
                        validate
                            .Errors
                            .Select(error => $"[{error.LineNumber},{error.LinePosition},{error.Message}]")
                            .Join(";"));
                }
                return EmptyFormat(request_validate);
            }

            return ErrorFormat(request_validate, "Неверный идентификационный номер");
        }

        private static Quitance ErrorFormat(
            RequestFileName ID,
            string message = "default",
            int code = 1003)
        {
            return new Quitance
            {
                AcknowledgementType = FileAcknowledgementType.Item8,
                ResultCode = code.ToString("0000"),
                ResultText = message,
                To = ID.SourceStringFullID,
                From = $"D{GDB.MyOrg.OGRN.ToString("0000000000000")}",
                CorrelationMessageID = ID
            };
        }

        private static Quitance ErrorEnc(
            RequestFileName ID,
            string message = "Невозможно расшифровать файл обмена",
            int code = 1002)
        {
            return new Quitance
            {
                AcknowledgementType = FileAcknowledgementType.Item8,
                ResultCode = code.ToString("0000"),
                ResultText = message,
                To = ID.SourceStringFullID,
                From = $"D{GDB.MyOrg.OGRN.ToString("0000000000000")}",
                CorrelationMessageID = ID
            };
        }

        private static Quitance ErrorSig(
            RequestFileName ID,
            string message = "Некорректная электронная подпись",
            int code = 1001)
        {
            return new Quitance
            {
                AcknowledgementType = FileAcknowledgementType.Item8,
                ResultCode = code.ToString("0000"),
                ResultText = message,
                To = ID.SourceStringFullID,
                From = $"D{GDB.MyOrg.OGRN.ToString("0000000000000")}",
                CorrelationMessageID = ID
            };
        }

        private static Quitance EmptyFormat(
            RequestFileName ID,
            string message = "Совпадения отсутствуют")
        {
            return new Quitance
            {
                AcknowledgementType = FileAcknowledgementType.Item10,
                ResultCode = 1000.ToString("0000"),
                ResultText = message,
                To = ID.SourceStringFullID,
                From = $"D{GDB.MyOrg.OGRN.ToString("0000000000000")}",
                CorrelationMessageID = ID
            };
        }
    }
}