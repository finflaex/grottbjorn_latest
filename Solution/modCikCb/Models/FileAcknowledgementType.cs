#region

using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

#endregion

namespace modCikCb.Models
{
    /// <remarks />
    [GeneratedCode("xsd", "4.6.1055.0")]
    [Serializable]
    [XmlType(AnonymousType = true)]
    public enum FileAcknowledgementType
    {
        /// <remarks />
        [XmlEnum("8")] Item8,

        /// <remarks />
        [XmlEnum("9")] Item9,

        /// <remarks />
        [XmlEnum("10")] Item10
    }
}