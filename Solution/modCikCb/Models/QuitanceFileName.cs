﻿#region

using System.Linq;
using System.Text;
using Finflaex.Attributes;
using Finflaex.Reflection;

#endregion

namespace modCikCb.Models
{
    public class QuitanceFileName : ResponseFileName
    {
        public QuitanceFileName(string value)
            : base(value)
        {
            regex = quitance.ToRegex();
        }

        public static string quitance => @"(["
                                         + fxr.EnumToList<TypeMember>().Skip(1).Select(v =>
                                             typeof (TypeMember)
                                                 .GetField(v.ToString())
                                                 .GetCustomAttributes(typeof (EnumParserAttribute), false)
                                                 .OfType<EnumParserAttribute>()
                                                 .FirstOrDefault()?
                                                 .Name ?? "?"
                                             ).Join(",") +
                                         @"]{1})(\d{13})_(\d{6}_\d{6})_["
                                         + fxr.EnumToList<TypeRequest>().Skip(1).Select(v =>
                                             typeof (TypeRequest)
                                                 .GetField(v.ToString())
                                                 .GetCustomAttributes(typeof (EnumParserAttribute), false)
                                                 .OfType<EnumParserAttribute>()
                                                 .FirstOrDefault()?
                                                 .Name ?? "?"
                                             ).Join(",") +
                                         @"]{1}_(\d{4})_(\d{4})_([{"
                                         + fxr.EnumToList<TypeMember>().Skip(1).Select(v =>
                                             typeof (TypeMember)
                                                 .GetField(v.ToString())
                                                 .GetCustomAttributes(typeof (EnumParserAttribute), false)
                                                 .OfType<EnumParserAttribute>()
                                                 .FirstOrDefault()?
                                                 .Name ?? "?"
                                             ).Join(",") +
                                         @"]{1})(\d{13})";

        public int Code { get; set; }

        public long TargetOGRN { get; set; }

        public string StringTargetTypeMember { get; set; }

        public TypeRequest TargetTypeMember
        {
            get { return StringTargetTypeMember?.ToEnum<TypeRequest>() ?? TypeRequest.Unknown; }
            set
            {
                StringTargetTypeMember = typeof (TypeRequest)
                    .GetField(value.ToString())
                    .GetAttributesOfProperty(nameof(EnumParserAttribute))
                    .OfType<EnumParserAttribute>()
                    .FirstOrDefault()?.Name ?? "?";
            }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.Append(StringSourceTypeMember);
            result.Append(SourceOGRN.ToString("0000000000000"));
            result.Append("_");
            result.Append(Date.ToString("ddMMgg_hhMMss"));
            result.Append("_");
            result.Append(StringTypeRequest);
            result.Append("_");
            result.Append(Number.ToString("0000"));
            result.Append("_");
            result.Append(Code.ToString("0000"));
            result.Append("_");
            result.Append(StringTargetTypeMember);
            result.Append(TargetOGRN.ToString("0000000000000"));
            return result.ToString();
        }
    }
}