﻿namespace modCikCb.Models
{
    public class mvcResponseFile
    {
        public string Message { get; internal set; }
        public dbQuery Query { get; internal set; }
        public dbResponse Response { get; internal set; }
    }
}