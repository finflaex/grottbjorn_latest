﻿#region

using System;
using System.Linq;
using System.Text;
using Finflaex.Attributes;
using Finflaex.Reflection;

#endregion

namespace modCikCb.Models
{
    public class ResponseFileName : RequestFileName
    {
        private DateTime? _date;

        public ResponseFileName(string value)
            : base(value)
        {
            regex = response.ToRegex();
            _date = DateTime.UtcNow.AddHours(3);
        }

        public static string response => @"(["
                                         + fxr.EnumToList<TypeMember>().Skip(1).Select(v =>
                                             typeof (TypeMember)
                                                 .GetField(v.ToString())
                                                 .GetCustomAttributes(typeof (EnumParserAttribute), false)
                                                 .OfType<EnumParserAttribute>()
                                                 .FirstOrDefault()?
                                                 .Name ?? "?"
                                             ).Join(",") +
                                         @"]{1})(\d{13})_(\d{6}_\d{6})_["
                                         + fxr.EnumToList<TypeRequest>().Skip(1).Select(v =>
                                             typeof (TypeRequest)
                                                 .GetField(v.ToString())
                                                 .GetCustomAttributes(typeof (EnumParserAttribute), false)
                                                 .OfType<EnumParserAttribute>()
                                                 .FirstOrDefault()?
                                                 .Name ?? "?"
                                             ).Join(",") +
                                         @"]{1}_(\d{4})";

        public new DateTime Date
        {
            get { return _date ?? matches[2].ToDateTime("ddMMgg_hhMMss").AsType<DateTime>(); }
            set { _date = value; }
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.Append(StringSourceTypeMember);
            result.Append(SourceOGRN.ToString("0000000000000"));
            result.Append("_");
            result.Append(Date.ToString("ddMMgg_hhMMss"));
            result.Append("_");
            result.Append(StringTypeRequest);
            result.Append("_");
            result.Append(Number.ToString("0000"));
            return result.ToString();
        }
    }
}