﻿#region

using Finflaex.Attributes;

#endregion

namespace modCikCb.Models
{
    public enum TypeMember
    {
        Unknown = 0,

        [EnumParser(Name = "K")] CreditOrganization = 1,

        [EnumParser(Name = "D")] Depositary = 2,

        [EnumParser(Name = "R")] Registrator = 3,

        [EnumParser(Name = "S")] SpecDepositary = 4,

        [EnumParser(Name = "B")] BankRF = 5,

        [EnumParser(Name = "F")] FCI = 6
    }
}