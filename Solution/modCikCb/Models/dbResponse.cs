﻿using System;

namespace modCikCb.Models
{
    public class dbResponse
    {
        public int Code { get; set; }
        public DateTime Date { get; set; }
        public string FileName { get; set; }
        public Guid Guid { get; set; }
        public string Message { get; set; }
        public long Size { get; set; }
    }
}
