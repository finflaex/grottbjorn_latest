﻿#region

using System;

#endregion

namespace modCikCb.Models
{
    public class dbQuery
    {
        public DateTime Date { get; set; }
        public string FileName { get; set; }
        public Guid Guid { get; set; }
        public int Number { get; set; }
        public long Size { get; set; }
    }
}