#region

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;
using Finflaex.Reflection;
using Finflaex.XML;

#endregion

namespace modCikCb.Models
{
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "File", Namespace = "", IsNullable = false)]
    public class Quitance
    {
        private string _message_id;

        public Quitance()
        {
            MessageType = FileMessageType.Item2;
            Priority = FilePriority.Item5;
            CreateTime = DateTime.UtcNow.AddHours(3).ToString("dd.MM.yyyy");
        }

        public FileAcknowledgementType AcknowledgementType { get; set; }

        public string ResultCode { get; set; }

        public string ResultText { get; set; }

        public string To { get; set; }

        public string From { get; set; }

        public string MessageID
        {
            get
            {
                if (_message_id == null)
                {
                    var reg = RequestFileName.request.ToRegex().Matches(CorrelationMessageID).ToArray();
                    var number = reg.Length > 0 ? reg[4] : 0.ToString("0000");
                    var date = DateTime.UtcNow.AddHours(3).ToString("ddMMyy_HHmmss");
                    _message_id = $"{From}_{date}_K_{number}_{ResultCode}_{To}";
                }
                return _message_id;
            }
            set { _message_id = value; }
        }

        /// <remarks />
        public string CorrelationMessageID { get; set; }

        /// <remarks />
        public FileMessageType MessageType { get; set; }

        /// <remarks />
        public FilePriority Priority { get; set; }

        /// <remarks />
        public string CreateTime { get; set; }

        public override string ToString()
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);
            //ns.Add("noNamespaceSchemaLocation", "VO_CIK_CB_K_6.xsd");
            //ns.Add("xsi", "VO_CIK_CB_K_6.xsd");
            Func<XmlWriter, XmlWriter> act = writer => new fxcCustomXmlWriter(writer)
            {
                NoNamespaceSchemaLocation = "VO_CIK_CB_K_6.xsd"
            };
            //return this.ToXml(act);
            return this.ToXml(ns, act, true);
        }
    }
}