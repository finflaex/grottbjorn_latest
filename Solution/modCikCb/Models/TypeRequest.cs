﻿#region

using Finflaex.Attributes;

#endregion

namespace modCikCb.Models
{
    public enum TypeRequest
    {
        Unknown = 0,

        [EnumParser(Name = "Z")] Packet = 1,

        [EnumParser(Name = "P")] Personal = 2,

        [EnumParser(Name = "K")] Quitantion = 3
    }
}