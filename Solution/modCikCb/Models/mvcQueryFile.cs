﻿using System;

namespace modCikCb.Models
{
    public class mvcQueryFile
    {
        public string number { get; set; }
        public string dateQuery { get; set; }
        public string name { get; set; }
        public string dateWork { get; set; }
        public Guid guidQuery { get; set; }
        public Guid guidResponse { get; set; }
    }
}