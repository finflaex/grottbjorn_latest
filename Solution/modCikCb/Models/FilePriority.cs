﻿#region

using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

#endregion

namespace modCikCb.Models
{
    /// <remarks />
    [GeneratedCode("xsd", "4.6.1055.0")]
    [Serializable]
    [XmlType(AnonymousType = true)]
    public enum FilePriority
    {
        /// <remarks />
        [XmlEnum("5")] Item5
    }
}