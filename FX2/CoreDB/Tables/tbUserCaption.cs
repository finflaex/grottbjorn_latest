﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CoreDB.Base;
using CoreDB.Repository;

namespace CoreDB.Tables
{
    public class tbeUserRole : tbLink
    { }

    public class tbRole : tbValue
    {
        public static async Task Initialize(DataDb db)
        {
            await DataRepository.Create<RoleRepository>(db).CreateRole(Guid.Empty, "system", "Система");
        }
    }

    public class tbUserCaption : tbDynValue
    {
    }

    public class tbUserLogin : tbDynValue
    {
    }

    public class tbUserPassword : tbDynValue
    {
    }

    public class tbUserTimeZone : tbDynValue
    {
    }

    public class tbUserConfig : tbDynValue
    {
    }
}
