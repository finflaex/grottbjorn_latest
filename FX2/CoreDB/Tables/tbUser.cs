﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CoreDB.Base;
using CoreDB.Repository;
using CoreSupport.Reflection;

namespace CoreDB.Tables
{
    public class tbUser : tbKey
    {
        public static async Task Initialize(DataDb db)
        {
            using (var userRep = new UserRepository(db))
            {
                var user = await userRep.GetUserByLogin("architector");

                if (user.IsNullOrEmpty())
                {
                    user = await userRep.CreateUser(null);
                    await userRep.SetLogin(null, user.Value, "architector");
                }

                await userRep.SetCaption(null, user.Value, "Система");
                await userRep.SetPassword(null, user.Value, "password");
                await userRep.SetRoles(Guid.Empty, user.Value, "system");
            }
        }
    }
}
