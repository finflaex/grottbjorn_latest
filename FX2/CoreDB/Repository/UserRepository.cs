﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreDB.Base;
using CoreDB.Tables;
using CoreSupport.MVC.Authorization;
using Microsoft.EntityFrameworkCore;

namespace CoreDB.Repository
{
    public class UserRepository : DataRepository
    {
        #region System

        public UserRepository(DataDb db) : base(db)
        {
        }

        #endregion

        public async Task<Guid> CreateUser(
            Guid? author)
        {
            return await CreateKey<tbUser>(author);
        }

        public async Task<tbUserCaption> SetCaption(
            Guid? author,
            Guid keyID,
            string value)
        {
            return await KeySetDynValue<tbUser, tbUserCaption>(author, keyID, null, value);
        }

        public async Task<string> GetCaption(
            Guid? keyID)
        {
            var read = await KeyGetDynValue<tbUser, tbUserCaption>(keyID, null);
            return read?.StringValue;
        }

        public async Task<tbUserLogin> SetLogin(
            Guid? author,
            Guid keyID,
            string value)
        {
            return await KeySetDynValue<tbUser, tbUserLogin>(author, keyID, null, value);
        }

        public async Task<string> GetLogin(
            Guid? keyID)
        {
            var read = await KeyGetDynValue<tbUser, tbUserLogin>(keyID, null);
            return read?.StringValue;
        }

        public async Task<tbUserPassword> SetPassword(
            Guid? author,
            Guid keyID,
            string value)
        {
            return await KeySetDynValue<tbUser, tbUserPassword>(author, keyID, null, value);
        }

        public async Task<string> GetPassword(
            Guid? keyID)
        {
            var read = await KeyGetDynValue<tbUser, tbUserPassword>(keyID, null);
            return read?.StringValue;
        }

        public async Task<tbUserPassword> SetTimeZone(
            Guid? author,
            Guid keyID,
            int value)
        {
            return await KeySetDynValue<tbUser, tbUserPassword>(author, keyID, null, value);
        }

        public async Task<int> GetTimeZone(
            Guid? keyID)
        {
            var read = await KeyGetDynValue<tbUser, tbUserPassword>(keyID, null);
            return read?.IntValue ?? 0;
        }

        public async Task SetConfig(Guid? author, Guid KeyID, string key, object value)
        {
            await KeySetDynValue<tbUser, tbUserConfig>(author, KeyID, key, value);
        }

        public async Task<string> GetConfigString(Guid author, string key)
        {
            var result = await KeyGetDynValue<tbUser, tbUserConfig>(author, key);
            return result?.StringValue;
        }

        public async Task<bool> GetConfigBool(Guid author, string key)
        {
            var result = await KeyGetDynValue<tbUser, tbUserConfig>(author, key);
            return result?.BoolValue ?? false;
        }

        public async Task SetRoles(Guid author, Guid keyID, params string[] param)
        {
            await KeySetValues<tbUser, tbeUserRole, tbRole>(author, keyID, param);
        }

        public async Task<AuthRecord> GetAuthor(string login, string password)
        {
            var query =
                from user in db.Keys.OfType<tbUser>()
                let _values = (
                    from value in user.Values
                    where value.Start < db.Now
                    where value.End > db.Now
                    select value
                )
                let _links = (
                    from link in user.Links
                    where link.Start < db.Now
                    where link.End > db.Now
                    select link
                )
                let _login = _values
                    .Where(value => value is tbUserLogin)
                    .Select(value => value.StringValue)
                    .FirstOrDefault()
                let _password = _values
                    .Where(value => value is tbUserPassword)
                    .Select(v => v.StringValue)
                    .FirstOrDefault()
                where _login == login
                where _password == password
                select new AuthRecord
                {
                    Guid = user.ID,
                    Caption = _values
                        .Where(value => value is tbUserCaption)
                        .Select(v => v.StringValue)
                        .FirstOrDefault(),
                    Roles = _links
                        .Where(link => link is tbeUserRole)
                        .Select(v=>v.Value.StringValue)
                        .ToArray(),
                    TimeZone = _values
                        .Where(value => value is tbUserTimeZone)
                        .Select(v => v.IntValue)
                        .FirstOrDefault(),
                };

            var read = await query.ToArrayAsync();

            return read.FirstOrDefault() ?? new AuthRecord
            {
                Guid = Guid.Empty,
                Caption = "Гость",
                Roles = new[] {"guest"}
            };
        }

        public async Task<Guid?> GetUserByLogin(string login)
        {
            var query =
                from value in db.DynValues.OfType<tbUserLogin>()
                where value.StringValue == login
                select value.KeyID;

            return await query.FirstOrDefaultAsync();
        }

        public async Task<tbUserConfig[]> GetConfigs(Guid? keyID)
        {
            var query =
                from value in db.DynValues.OfType<tbUserConfig>()
                where value.KeyID == keyID
                where value.Start < db.Now
                where value.End > db.Now
                select value;

            return await query.ToArrayAsync();
        }
    }
}
