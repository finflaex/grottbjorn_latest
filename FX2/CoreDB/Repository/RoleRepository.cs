﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreDB.Base;
using CoreDB.Tables;
using CoreSupport.Type;
using Microsoft.EntityFrameworkCore;

namespace CoreDB.Repository
{
    public class RoleRepository : DataRepository
    {
        public RoleRepository(DataDb db) : base(db)
        {
        }

        public async Task<KeyValue[]> GetRoles()
        {
            var query =
                from role in db.Values.OfType<tbRole>()
                where role.StringValue != "system"
                select new KeyValue
                {
                    Key = role.StringValue,
                    Value = role.Caption
                };

            return await query.ToArrayAsync();
        }

        public async Task CreateRole(Guid author, string key, string caption = null)
        {
            var query_get =
                from role in db.Values.OfType<tbRole>()
                where role.StringValue == key
                select role;

            var read = await query_get.FirstOrDefaultAsync();

            if (read == null)
            {
                read = new tbRole
                {
                    Caption = caption ?? key,
                    StringValue = key
                };

                db.Values.Add(read);
            }
            else
            {
                read.Caption = caption;
            }

            await db.CommitAsync(author);
        }
    }
}
