﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CoreDB.Base;
using CoreSupport.Reflection;
using Microsoft.EntityFrameworkCore;

namespace CoreDB.Repository
{
    public class DataRepository : IDisposable
    {
        protected DataDb db;

        public DataRepository(DataDb db)
        {
            this.db = db;
        }

        public void Dispose()
        {
        }

        public static T Create<T>(DataDb db)
            where T : DataRepository
        {
            return fxr.Create<T>(db);
        }

        protected async Task<Guid> CreateKey<TK>(
            Guid? author = null,
            string key = null)
            where TK : tbKey
        {
            var result = typeof(TK).Create().AsType<TK>();

            using (var t = await db.Database.BeginTransactionAsync())
            {
                try
                {
                    result = db.Keys.Add(result).Entity.AsType<TK>();
                    await db.CommitAsync(author);
                    t.Commit();
                }
                catch (Exception err)
                {
                    t.Rollback();
                    throw err;
                }
            }

            return result.ID;
        }

        protected async Task<TV> KeySetDynValue<TK, TV>(
            Guid? author,
            Guid keyID,
            string caption,
            object value)
            where TK : tbKey
            where TV : tbDynValue
        {
            try
            {
                var result = await KeyGetDynValue<TK, TV>(keyID, caption);

                switch (value)
                {
                    case null:
                        if (result != null)
                            if (
                                result.BoolValue == false
                                && result.IntValue == 0
                                && result.StringValue == null
                                && result.RawValue == null
                            )
                                return result;
                            else
                                result.End = db.Now;
                        result = typeof(TV).Create().AsType<TV>();
                        result.KeyID = keyID;
                        result.Start = DateTimeOffset.Now;
                        result.BoolValue = false;
                        result.IntValue = 0;
                        result.StringValue = null;
                        result.RawValue = null;
                        result.Caption = caption;
                        db.DynValues.Add(result);
                        break;

                    case string stringValue:
                        if (result != null)
                            if (result.StringValue == stringValue)
                                return result;
                            else
                                result.End = db.Now;
                        result = typeof(TV).Create().AsType<TV>();
                        result.KeyID = keyID;
                        result.Start = DateTimeOffset.Now;
                        result.StringValue = stringValue;
                        result.Caption = caption;
                        db.DynValues.Add(result);
                        break;

                    case int intValue:
                        if (result != null)
                            if (result.IntValue == intValue)
                                return result;
                            else
                                result.End = db.Now;
                        result = typeof(TV).Create().AsType<TV>();
                        result.KeyID = keyID;
                        result.Start = DateTimeOffset.Now;
                        result.IntValue = intValue;
                        result.Caption = caption;
                        db.DynValues.Add(result);
                        break;

                    case bool boolValue:
                        if (result != null)
                            if (result.BoolValue == boolValue)
                                return result;
                            else
                                result.End = db.Now;
                        result = typeof(TV).Create().AsType<TV>();
                        result.KeyID = keyID;
                        result.Start = DateTimeOffset.Now;
                        result.BoolValue = boolValue;
                        result.Caption = caption;
                        db.DynValues.Add(result);
                        break;

                    case byte[] rawValue:
                        if (result != null)
                            if (result.RawValue == rawValue)
                                return result;
                            else
                                result.End = db.Now;
                        result = typeof(TV).Create().AsType<TV>();
                        result.KeyID = keyID;
                        result.Start = DateTimeOffset.Now;
                        result.RawValue = rawValue;
                        result.Caption = caption;
                        db.DynValues.Add(result);
                        break;
                }

                await db.CommitAsync(author);

                return result;
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        protected async Task<TV> KeyGetDynValue<TK, TV>(
            Guid? keyID,
            string caption)
            where TK : tbKey
            where TV : tbDynValue
        {
            var query =
                from read_val in db.DynValues.OfType<TV>()
                where read_val.KeyID == keyID
                where read_val.Caption == caption
                select read_val;

            var result = await query.FirstOrDefaultAsync();

            return result;
        }

        protected async Task<TV[]> GetValues<TV>(
            params string[] param)
            where TV : tbValue
        {
            var flag = !param.Any();

            var query =
                from value in db.Values.OfType<TV>()
                where flag || param.Contains(value.StringValue)
                select value;

            return await query.ToArrayAsync();
        }

        protected async Task<TV[]> KeyGetValues<TK, TL, TV>(
            Guid? keyID)
            where TK : tbKey
            where TL : tbLink
            where TV : tbValue
        {
            var query =
                from key in db.Keys
                where key is TK
                where key.ID == keyID
                from link in key.Links
                where link is TL
                where link.Start < db.Now
                where link.End > db.Now
                select link.Value;

            return await query.Cast<TV>().ToArrayAsync();
        }

        public async Task KeySetValues<TK, TL, TV>(
            Guid author,
            Guid keyID,
            params string[] param)
            where TK : tbKey
            where TL : tbLink
            where TV : tbValue
        {
            var nid = await GetValues<TV>(param);
            var has = await KeyGetValues<TK, TL, TV>(keyID);

            var delete = has.Except(nid).Select(v => v.ID).ToArray();

            var query_delete =
                from e in db.Links.OfType<TL>()
                where e.KeyID == keyID
                where e.Start < db.Now
                where e.End > db.Now
                where delete.Contains(e.ValueID)
                select e;

            foreach (var record in query_delete)
                record.End = db.Now;

            var added = nid.Except(has)
                .Select(v =>
                {
                    var link = typeof(TL).Create().AsType<TL>();
                    link.KeyID = keyID;
                    link.Start = db.Now;
                    link.ValueID = v.ID;
                    return link;
                });

            await db.Links.AddRangeAsync(added);
            await db.CommitAsync(author);
        }
    }
}