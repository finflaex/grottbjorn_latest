﻿#region

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoreDB.Tables;
using CoreSupport.DataBase.Base;
using CoreSupport.DataBase.Table;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.DependencyInjection;

#endregion

namespace CoreDB.Base
{
    public class DataDb : FxDbContext<DataDb>
    {
        private static TimeSpan? _now;

        public DateTimeOffset Now
        {
            get
            {
                if (_now == null)
                    return DateTimeOffset.Now;
                return DateTimeOffset.Now - _now.Value;
            }
            set { _now = DateTimeOffset.Now - value; }
        }

        public static async Task Initialize(IServiceProvider appApplicationServices)
        {
            var db = appApplicationServices.GetService<DataDb>();

            await tbRole.Initialize(db);
            await tbUser.Initialize(db);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                //"Data Source=mars;Integrated Security=False;User ID=sa;Password=411;Database=CoreDB;"
                $@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=CoreDB;Integrated Security=True;"
            );
        }

        public DbSet<tbKey> Keys { get; set; }
        public DbSet<tbValue> Values { get; set; }
        public DbSet<tbDynValue> DynValues { get; set; }
        public DbSet<tbLink> Links { get; set; }

        protected override void OnModelCreating(ModelBuilder db)
        {
            base.OnModelCreating(db);

            db.Entity<tbUser>();

            db.Entity<tbUserCaption>();
            db.Entity<tbUserLogin>();
            db.Entity<tbUserPassword>();
            db.Entity<tbUserLogin>();
            db.Entity<tbUserConfig>();

            db.Entity<tbRole>();
            db.Entity<tbeUserRole>();

            db.Entity<tbKey>()
                .HasMany(v => v.Links)
                .WithOne(v => v.Key)
                .HasForeignKey(v => v.KeyID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            db.Entity<tbKey>()
                .HasMany(v=>v.Values)
                .WithOne(v=>v.Key)
                .HasForeignKey(v=>v.KeyID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            db.Entity<tbValue>()
                .HasMany(v => v.Links)
                .WithOne(v => v.Value)
                .HasForeignKey(v => v.ValueID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }

    public class tbKey
        : fxaGuidTable
            , fxiKeyTable
            , fxiOrderTable
            , fxiStartEndTable
            , fxiCreateAuthorIdTable
            , fxiDateCreateTable
    {
        public string Key { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public Guid? CreateAuthorID { get; set; }
        public DateTimeOffset DateCreate { get; set; }

        public tbKey()
        {
            Links = new HashSet<tbLink>();
            Values = new HashSet<tbDynValue>();
        }

        public virtual ICollection<tbLink> Links { get; set; }
        public virtual ICollection<tbDynValue> Values { get; set; }
        public int Order { get; set; }
    }

    public class tbValue
        : fxaGuidTable
            , fxiMultiValueTable
            , fxiOrderTable
            , fxiCaptionTable
            , fxiCreateAuthorIdTable
            , fxiDateCreateTable
    {
        public bool BoolValue { get; set; }
        public int IntValue { get; set; }
        public byte[] RawValue { get; set; }
        public string StringValue { get; set; }
        public Guid? CreateAuthorID { get; set; }
        public DateTimeOffset DateCreate { get; set; }

        public tbValue()
        {
            Links = new HashSet<tbLink>();
        }

        public virtual ICollection<tbLink> Links { get; set; }
        public int Order { get; set; }
        public string Caption { get; set; }
    }

    public class tbDynValue
        : fxaGuidTable
            , fxiOrderTable
            , fxiCaptionTable
            , fxiMultiValueTable
            , fxiStartEndTable
            , fxiCreateAuthorIdTable
            , fxiDateCreateTable
            , fxiUpdateAuthorIdTable
            , fxiDateModifyTable
    {
        public bool BoolValue { get; set; }
        public int IntValue { get; set; }
        public byte[] RawValue { get; set; }
        public string StringValue { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public Guid? CreateAuthorID { get; set; }
        public DateTimeOffset DateCreate { get; set; }
        public Guid? UpdateAuthorID { get; set; }
        public DateTimeOffset DateModify { get; set; }

        public Guid KeyID { get; set; }
        public virtual tbKey Key { get; set; }

        public int Order { get; set; }
        public string Caption { get; set; }
    }

    public class tbLink
        : fxaGuidTable
            , fxiStartEndTable
            , fxiCreateAuthorIdTable
            , fxiDateCreateTable
            , fxiUpdateAuthorIdTable
            , fxiDateModifyTable
    {
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public Guid? CreateAuthorID { get; set; }
        public DateTimeOffset DateCreate { get; set; }
        public Guid? UpdateAuthorID { get; set; }
        public DateTimeOffset DateModify { get; set; }


        public Guid KeyID { get; set; }
        public tbKey Key { get; set; }

        public Guid ValueID { get; set; }
        public tbValue Value { get; set; }
    }
}