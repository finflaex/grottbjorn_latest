﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreDB.Migrations
{
    public partial class @base : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Keys",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreateAuthorID = table.Column<Guid>(nullable: true),
                    DateCreate = table.Column<DateTimeOffset>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Start = table.Column<DateTimeOffset>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Keys", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Values",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    BoolValue = table.Column<bool>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    CreateAuthorID = table.Column<Guid>(nullable: true),
                    DateCreate = table.Column<DateTimeOffset>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    IntValue = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    RawValue = table.Column<byte[]>(nullable: true),
                    StringValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Values", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "DynValues",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    BoolValue = table.Column<bool>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    CreateAuthorID = table.Column<Guid>(nullable: true),
                    DateCreate = table.Column<DateTimeOffset>(nullable: false),
                    DateModify = table.Column<DateTimeOffset>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    IntValue = table.Column<int>(nullable: false),
                    KeyID = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    RawValue = table.Column<byte[]>(nullable: true),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    StringValue = table.Column<string>(nullable: true),
                    UpdateAuthorID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DynValues", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DynValues_Keys_KeyID",
                        column: x => x.KeyID,
                        principalTable: "Keys",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Links",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CreateAuthorID = table.Column<Guid>(nullable: true),
                    DateCreate = table.Column<DateTimeOffset>(nullable: false),
                    DateModify = table.Column<DateTimeOffset>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    End = table.Column<DateTimeOffset>(nullable: true),
                    KeyID = table.Column<Guid>(nullable: false),
                    Start = table.Column<DateTimeOffset>(nullable: true),
                    UpdateAuthorID = table.Column<Guid>(nullable: true),
                    ValueID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Links", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Links_Keys_KeyID",
                        column: x => x.KeyID,
                        principalTable: "Keys",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Links_Values_ValueID",
                        column: x => x.ValueID,
                        principalTable: "Values",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DynValues_KeyID",
                table: "DynValues",
                column: "KeyID");

            migrationBuilder.CreateIndex(
                name: "IX_Links_KeyID",
                table: "Links",
                column: "KeyID");

            migrationBuilder.CreateIndex(
                name: "IX_Links_ValueID",
                table: "Links",
                column: "ValueID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DynValues");

            migrationBuilder.DropTable(
                name: "Links");

            migrationBuilder.DropTable(
                name: "Keys");

            migrationBuilder.DropTable(
                name: "Values");
        }
    }
}
