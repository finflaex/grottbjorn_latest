﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CoreDB.Base;

namespace CoreDB.Migrations
{
    [DbContext(typeof(DataDb))]
    [Migration("20170722112016_base")]
    partial class @base
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CoreDB.Base.tbDynValue", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("BoolValue");

                    b.Property<string>("Caption");

                    b.Property<Guid?>("CreateAuthorID");

                    b.Property<DateTimeOffset>("DateCreate");

                    b.Property<DateTimeOffset>("DateModify");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<DateTimeOffset?>("End");

                    b.Property<int>("IntValue");

                    b.Property<Guid>("KeyID");

                    b.Property<int>("Order");

                    b.Property<byte[]>("RawValue");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<string>("StringValue");

                    b.Property<Guid?>("UpdateAuthorID");

                    b.HasKey("ID");

                    b.HasIndex("KeyID");

                    b.ToTable("DynValues");

                    b.HasDiscriminator<string>("Discriminator").HasValue("tbDynValue");
                });

            modelBuilder.Entity("CoreDB.Base.tbKey", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("CreateAuthorID");

                    b.Property<DateTimeOffset>("DateCreate");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<DateTimeOffset?>("End");

                    b.Property<string>("Key");

                    b.Property<int>("Order");

                    b.Property<DateTimeOffset?>("Start");

                    b.HasKey("ID");

                    b.ToTable("Keys");

                    b.HasDiscriminator<string>("Discriminator").HasValue("tbKey");
                });

            modelBuilder.Entity("CoreDB.Base.tbLink", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("CreateAuthorID");

                    b.Property<DateTimeOffset>("DateCreate");

                    b.Property<DateTimeOffset>("DateModify");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<DateTimeOffset?>("End");

                    b.Property<Guid>("KeyID");

                    b.Property<DateTimeOffset?>("Start");

                    b.Property<Guid?>("UpdateAuthorID");

                    b.Property<Guid>("ValueID");

                    b.HasKey("ID");

                    b.HasIndex("KeyID");

                    b.HasIndex("ValueID");

                    b.ToTable("Links");

                    b.HasDiscriminator<string>("Discriminator").HasValue("tbLink");
                });

            modelBuilder.Entity("CoreDB.Base.tbValue", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("BoolValue");

                    b.Property<string>("Caption");

                    b.Property<Guid?>("CreateAuthorID");

                    b.Property<DateTimeOffset>("DateCreate");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<int>("IntValue");

                    b.Property<int>("Order");

                    b.Property<byte[]>("RawValue");

                    b.Property<string>("StringValue");

                    b.HasKey("ID");

                    b.ToTable("Values");

                    b.HasDiscriminator<string>("Discriminator").HasValue("tbValue");
                });

            modelBuilder.Entity("CoreDB.Tables.tbUserConfig", b =>
                {
                    b.HasBaseType("CoreDB.Base.tbDynValue");


                    b.ToTable("tbUserConfig");

                    b.HasDiscriminator().HasValue("tbUserConfig");
                });

            modelBuilder.Entity("CoreDB.Tables.tbUserLogin", b =>
                {
                    b.HasBaseType("CoreDB.Base.tbDynValue");


                    b.ToTable("tbUserLogin");

                    b.HasDiscriminator().HasValue("tbUserLogin");
                });

            modelBuilder.Entity("CoreDB.Tables.tbUserPassword", b =>
                {
                    b.HasBaseType("CoreDB.Base.tbDynValue");


                    b.ToTable("tbUserPassword");

                    b.HasDiscriminator().HasValue("tbUserPassword");
                });

            modelBuilder.Entity("CoreDB.Tables.tbUser", b =>
                {
                    b.HasBaseType("CoreDB.Base.tbKey");


                    b.ToTable("tbUser");

                    b.HasDiscriminator().HasValue("tbUser");
                });

            modelBuilder.Entity("CoreDB.Tables.tbeUserRole", b =>
                {
                    b.HasBaseType("CoreDB.Base.tbLink");


                    b.ToTable("tbeUserRole");

                    b.HasDiscriminator().HasValue("tbeUserRole");
                });

            modelBuilder.Entity("CoreDB.Tables.tbRole", b =>
                {
                    b.HasBaseType("CoreDB.Base.tbValue");


                    b.ToTable("tbRole");

                    b.HasDiscriminator().HasValue("tbRole");
                });

            modelBuilder.Entity("CoreDB.Base.tbDynValue", b =>
                {
                    b.HasOne("CoreDB.Base.tbKey", "Key")
                        .WithMany("Values")
                        .HasForeignKey("KeyID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CoreDB.Base.tbLink", b =>
                {
                    b.HasOne("CoreDB.Base.tbKey", "Key")
                        .WithMany("Links")
                        .HasForeignKey("KeyID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CoreDB.Base.tbValue", "Value")
                        .WithMany("Links")
                        .HasForeignKey("ValueID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
