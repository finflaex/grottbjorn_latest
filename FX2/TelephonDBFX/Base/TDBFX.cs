﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.Reflection;
using Npgsql;
using TelephonDBFX.Tables;

namespace TelephonDBFX.Base
{
    //[DbConfigurationType(typeof(ConfigTDBFX))]
    public class TDBFX : DbContext
    {
        public static string Connect
        {
            get
            {
                var databaseName = "asterisk";
                var userName = "asterisk_read";
                var password = "asterisk_read";
                var host = "10.0.0.10";
                //var port = 5432;
                return $"Server={host}; " + 
                    $"User Id={userName};" + 
                    $"Password={password};" + 
                    $"Database={databaseName};";
            }
        }

        public static DbConnection DbConnect
        {
            get
            {
                var factory = DbProviderFactories.GetFactory("Npgsql");
                var connect = factory.CreateConnection();
                connect.ConnectionString = Connect;
                return connect;
            }
        }

        public TDBFX()
            : base(DbConnect,true)
        {

        }

        public DbSet<TELEFON> TELEFON { get; set; }

        public static void action(Action<TDBFX> act)
        {
            if (act != null)
            {
                using (var db = new TDBFX())
                {
                    act.Invoke(db);
                }
            }
        }
    }
}
