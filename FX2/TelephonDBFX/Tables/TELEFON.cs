﻿#region

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace TelephonDBFX.Tables
{
    [Table("cdr", Schema = "public")]
    public class TELEFON
    {
        [Key]
        public long acctid { get; set; }

        public DateTime calldate { get; set; }

        public string clid { get; set; }

        public string src { get; set; }

        public string dst { get; set; }

        public string dcontext { get; set; }

        public string channel { get; set; }

        public string dstchannel { get; set; }

        public string lastapp { get; set; }

        public string lastdata { get; set; }

        public long duration { get; set; }

        public long billsec { get; set; }

        public string disposition { get; set; }

        public long amaflags { get; set; }

        public string accountcode { get; set; }

        public string uniqueid { get; set; }

        public string userfield { get; set; }
    }
}