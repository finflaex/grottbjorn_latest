﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DBFXFias
{
    public class ADDROBJ
    {
        public double ACTSTATUS { get; set; }
        public string AOGUID { get; set; }

        [Key]
        public string AOID { get; set; }
        public double AOLEVEL { get; set; }
        public string AREACODE { get; set; }
        public string AUTOCODE { get; set; }
        public double CENTSTATUS { get; set; }
        public string CITYCODE { get; set; }
        public string CODE { get; set; }
        public double CURRSTATUS { get; set; }
        public DateTime ENDDATE { get; set; }
        public string FORMALNAME { get; set; }
        public string IFNSFL { get; set; }
        public string IFNSUL { get; set; }
        public string NEXTID { get; set; }
        public string OFFNAME { get; set; }
        public string OKATO { get; set; }
        public string OKTMO { get; set; }
        public double OPERSTATUS { get; set; }
        public string PARENTGUID { get; set; }
        public string PLACECODE { get; set; }
        public string PLAINCODE { get; set; }
        public string POSTALCODE { get; set; }
        public string PREVID { get; set; }
        public string REGIONCODE { get; set; }
        public string SHORTNAME { get; set; }
        public DateTime STARTDATE { get; set; }
        public string STREETCODE { get; set; }
        public string TERRIFNSFL { get; set; }
        public string TERRIFNSUL { get; set; }
        public DateTime UPDATEDATE { get; set; }
        public string CTARCODE { get; set; }
        public string EXTRCODE { get; set; }
        public string SEXTCODE { get; set; }
        public double LIVESTATUS { get; set; }
        public string NORMDOC { get; set; }
    }
}
