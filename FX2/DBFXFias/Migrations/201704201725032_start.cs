namespace DBFXFias.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ADDROBJs",
                c => new
                    {
                        AOID = c.String(nullable: false, maxLength: 128),
                        ACTSTATUS = c.Double(nullable: false),
                        AOGUID = c.String(),
                        AOLEVEL = c.Double(nullable: false),
                        AREACODE = c.String(),
                        AUTOCODE = c.String(),
                        CENTSTATUS = c.Double(nullable: false),
                        CITYCODE = c.String(),
                        CODE = c.String(),
                        CURRSTATUS = c.Double(nullable: false),
                        ENDDATE = c.DateTime(nullable: false),
                        FORMALNAME = c.String(),
                        IFNSFL = c.String(),
                        IFNSUL = c.String(),
                        NEXTID = c.String(),
                        OFFNAME = c.String(),
                        OKATO = c.String(),
                        OKTMO = c.String(),
                        OPERSTATUS = c.Double(nullable: false),
                        PARENTGUID = c.String(),
                        PLACECODE = c.String(),
                        PLAINCODE = c.String(),
                        POSTALCODE = c.String(),
                        PREVID = c.String(),
                        REGIONCODE = c.String(),
                        SHORTNAME = c.String(),
                        STARTDATE = c.DateTime(nullable: false),
                        STREETCODE = c.String(),
                        TERRIFNSFL = c.String(),
                        TERRIFNSUL = c.String(),
                        UPDATEDATE = c.DateTime(nullable: false),
                        CTARCODE = c.String(),
                        EXTRCODE = c.String(),
                        SEXTCODE = c.String(),
                        LIVESTATUS = c.Double(nullable: false),
                        NORMDOC = c.String(),
                    })
                .PrimaryKey(t => t.AOID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ADDROBJs");
        }
    }
}
