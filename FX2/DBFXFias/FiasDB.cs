﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Finflaex.Support.Reflection;

namespace DBFXFias
{
    public class FiasDB : DbContext
    {
        public FiasDB()
            : base($@"Data Source=mars;Integrated Security=False;User ID=sa;Password=411;Database={nameof(FiasDB)};")
        {
            
        }

        public DbSet<ADDROBJ> AddrObj { get; set; }

        public IQueryable<ADDROBJ> ListByKladr(IEnumerable<string> kladrs)
        {
            return from adr in AddrObj
                where kladrs.Contains(adr.CODE)
                orderby adr.AOLEVEL
                select adr;
        }
    }

    public static class helpFiasDB
    {
        public static string ToFormatString(
            this IEnumerable<ADDROBJ> list)
            => list
                .Select(v => $"{v.SHORTNAME} {v.FORMALNAME}")
                .Select(v => @"(\s*\(.*\)\s*)".ToRegex().Replace(v, ""))
                .Join(", ");

        public static IEnumerable<Guid> ToGuidsList(
            this IEnumerable<ADDROBJ> list)
            => list.Select(v => v.AOGUID.ToGuid());
    }
}
