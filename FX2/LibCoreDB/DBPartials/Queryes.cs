﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSupport.MVC.Authorization;
using CoreSupport.Reflection;
using LibCoreDB.Tables;
using Microsoft.EntityFrameworkCore;

namespace LibCoreDB
{
    partial class DataDb
    {
        public IQueryable<E> User_queryEvents<E>(Guid? user_id)
            where E : dbEvent
            => (
                    from e in Events
                    let now = Now
                    where e is E
                    where e.UserGuid == user_id
                    where e.Start < now
                    where e.End > now
                    select e
                )
                .OfType<E>();

        public IQueryable<EventValue<E, V>> User_queryFullValues<E, V>(Guid? user_id)
            where E : dbEvent
            where V : dbValue
            => (
                    from e in Events
                    where e.UserGuid == user_id
                    where e is E
                    where e.Start < Now
                    where e.End > Now
                    from v in Values
                    where v.ID == e.ValueGuid
                    select new EventValue<E, V>
                    {
                        Value = (V)v,
                        Event = (E)e
                    }
                )
                .Where(v => v.Event != null)
                .Where(v => v.Value != null);
    }
}
