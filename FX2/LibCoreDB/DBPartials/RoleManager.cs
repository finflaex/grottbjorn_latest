﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSupport.Reflection;
using LibCoreDB.Tables;
using Microsoft.EntityFrameworkCore;

namespace LibCoreDB
{
    partial class DataDb
    {
        /// <summary>
        /// Получить гуид роли по ключу
        /// </summary>
        /// <param name="roleKey"></param>
        /// <returns></returns>
        public async Task<Guid?> GetRole_byKey(
            string roleKey)
            => await getGuidValue_byKey<dbRole>(roleKey);

        /// <summary>
        /// Создет роль и возвращает ее гуид, 
        /// если роль с таким ключом есть то просто возвращает гуид этой роли
        /// </summary>
        /// <param name="AuthorID"></param>
        /// <param name="roleKey"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public async Task<Guid?> CreateRole(
            Guid? AuthorID,
            string roleKey,
            string roleName = null)
        {
            if (roleKey.isEmpty()) return null;

            var result = await GetRole_byKey(roleKey);

            if (!result.IsNullOrEmpty()) return result;

            var record = new dbRole
            {
                Key = roleKey,
                StringValue = roleName ?? roleKey
            };

            result = record.ID;

            await Values.AddAsync(record);
            await CommitAsync(AuthorID);

            return result;
        }

        /// <summary>
        /// Возвращает список имеющихся ролей
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<dbRole>> ListRoles()
            => await queryValues<dbRole>()
                .Where(v => v.Key != "system")
                .ToArrayAsync();
    }
}
