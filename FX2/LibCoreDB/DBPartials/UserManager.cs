﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreSupport.MVC.Authorization;
using CoreSupport.Reflection;
using LibCoreDB.Tables;
using Microsoft.EntityFrameworkCore;

namespace LibCoreDB
{
    partial class DataDb
    {
        public async Task<string> User_getStringValue<TE>(
            Guid? userId, string key = null)
            where TE : dbEvent
        {
            return await User_queryEvents<TE>(userId)
                .Where(v => v.Key == key)
                .Select(v => v.StringValue)
                .SingleOrDefaultAsync();
        }

        public async Task<string> User_getStringValue<TE, TV>(
            Guid? userId, string key = null)
            where TE : dbEvent
            where TV : dbValue
        {
            return await User_queryFullValues<TE, TV>(userId)
                .Where(v => v.Value.Key == key)
                .Select(v => v.Value.StringValue)
                .SingleOrDefaultAsync();
        }

        public async Task<int> User_getIntValue<TE>(
            Guid? userId, string key = null)
            where TE : dbEvent
        {
            return await User_queryEvents<TE>(userId)
                .Where(v => v.Key == key)
                .Select(v => v.IntValue)
                .SingleOrDefaultAsync();
        }

        public async Task<int> User_getIntValue<TE, TV>(
            Guid? userId, string key = null)
            where TE : dbEvent
            where TV : dbValue
        {
            return await User_queryFullValues<TE, TV>(userId)
                .Where(v => v.Value.Key == key)
                .Select(v => v.Value.IntValue)
                .SingleOrDefaultAsync();
        }

        public async Task<bool> User_getBoolValue<TE>(
            Guid? userId, string key = null)
            where TE : dbEvent
        {
            return await User_queryEvents<TE>(userId)
                .Where(v => v.Key == key)
                .Select(v => v.BoolValue)
                .SingleOrDefaultAsync();
        }

        public async Task<bool> User_getBoolValue<TE, TV>(
            Guid? userId, string key = null)
            where TE : dbEvent
            where TV : dbValue
        {
            return await User_queryFullValues<TE, TV>(userId)
                .Where(v => v.Value.Key == key)
                .Select(v => v.Value.BoolValue)
                .SingleOrDefaultAsync();
        }

        public async Task<byte[]> User_getRawValue<TE>(
            Guid? userId, string key = null)
            where TE : dbEvent
        {
            return await User_queryEvents<TE>(userId)
                .Where(v => v.Key == key)
                .Select(v => v.RawValue)
                .SingleOrDefaultAsync();
        }

        public async Task<byte[]> User_getRawValue<TE, TV>(
            Guid? userId, string key = null)
            where TE : dbEvent
            where TV : dbValue
        {
            return await User_queryFullValues<TE, TV>(userId)
                .Where(v => v.Value.Key == key)
                .Select(v => v.Value.RawValue)
                .SingleOrDefaultAsync();
        }

        public async Task User_setStringValue<TE>(
            Guid? userId,
            string value)
            where TE : dbEvent
        {
            await User_setStringValue<TE>(userId, null, value);
        }

        public async Task User_setStringValue<TE>(
            Guid? userId,
            string key,
            string value)
            where TE : dbEvent
        {
            var read = await User_queryEvents<TE>(userId)
                .Where(v => v.Key == key)
                .SingleOrDefaultAsync();

            if (read != null)
            {
                if (read.StringValue == value)
                    return;
                read.End = Now;
            }

            var _event = Activator.CreateInstance<TE>();
            _event.Start = Now;
            _event.UserGuid = userId;
            _event.Key = key;
            _event.StringValue = value;
            await Events.AddAsync(_event);
            await CommitAsync(userId);
        }

        public async Task User_setStringValue<TE, TV>(
            Guid? userId,
            string value)
            where TE : dbEvent
            where TV : dbValue
        {
            await User_setStringValue<TE, TV>(userId, null, value);
        }

        public async Task User_setStringValue<TE, TV>(
            Guid? userId,
            string key,
            string value)
            where TE : dbEvent
            where TV : dbValue
        {
            var read = await User_queryFullValues<TE, TV>(userId)
                .Where(v => v.Value.Key == key)
                .SingleOrDefaultAsync();

            if (read != null)
            {
                if (read.Value.StringValue == value)
                    return;
                read.Event.End = Now;
            }

            var _value = Activator.CreateInstance<TV>();
            _value.Key = key;
            _value.StringValue = value;
            await Values.AddAsync(_value);
            await CommitAsync(userId);

            var _event = Activator.CreateInstance<TE>();
            _event.Start = Now;
            _event.UserGuid = userId;
            _event.ValueGuid = _value.ID;
            await Events.AddAsync(_event);
            await CommitAsync(userId);

            await CommitAsync(userId);
        }

        public async Task User_setIntValue<TE>(
            Guid? userId,
            int value)
            where TE : dbEvent
        {
            await User_setIntValue<TE>(userId, null, value);
        }

        public async Task User_setIntValue<TE>(
            Guid? userId,
            string key,
            int value)
            where TE : dbEvent
        {
            var read = await User_queryEvents<TE>(userId)
                .Where(v => v.Key == key)
                .SingleOrDefaultAsync();

            if (read != null)
            {
                if (read.IntValue == value)
                    return;
                read.End = Now;
            }

            var _event = Activator.CreateInstance<TE>();
            _event.Start = Now;
            _event.UserGuid = userId;
            _event.Key = key;
            _event.IntValue = value;
            await Events.AddAsync(_event);
            await CommitAsync(userId);
        }

        public async Task User_setIntValue<TE, TV>(
            Guid? userId,
            int value)
            where TE : dbEvent
            where TV : dbValue
        {
            await User_setIntValue<TE, TV>(userId, null, value);
        }

        public async Task User_setIntValue<TE, TV>(
            Guid? userId,
            string key,
            int value)
            where TE : dbEvent
            where TV : dbValue
        {
            var read = await User_queryFullValues<TE, TV>(userId)
                .Where(v => v.Value.Key == key)
                .SingleOrDefaultAsync();

            if (read != null)
            {
                if (read.Value.IntValue == value)
                    return;
                read.Event.End = Now;
            }

            var _value = Activator.CreateInstance<TV>();
            _value.Key = key;
            _value.IntValue = value;
            await Values.AddAsync(_value);
            await CommitAsync(userId);

            var _event = Activator.CreateInstance<TE>();
            _event.Start = Now;
            _event.UserGuid = userId;
            _event.ValueGuid = _value.ID;
            await Events.AddAsync(_event);
            await CommitAsync(userId);
        }

        public async Task User_setBoolValue<TE>(
            Guid? userId,
            bool value)
            where TE : dbEvent
        {
            await User_setBoolValue<TE>(userId, null, value);
        }

        public async Task User_setBoolValue<TE>(
            Guid? userId,
            string key,
            bool value)
            where TE : dbEvent
        {
            var read = await User_queryEvents<TE>(userId)
                .Where(v => v.Key == key)
                .SingleOrDefaultAsync();

            if (read != null)
            {
                if (read.BoolValue == value)
                    return;
                read.End = Now;
            }

            var _event = Activator.CreateInstance<TE>();
            _event.Start = Now;
            _event.UserGuid = userId;
            _event.Key = key;
            _event.BoolValue = value;
            await Events.AddAsync(_event);
            await CommitAsync(userId);
        }

        public async Task User_setBoolValue<TE, TV>(
            Guid? userId,
            bool value)
            where TE : dbEvent
            where TV : dbValue
        {
            await User_setBoolValue<TE, TV>(userId, null, value);
        }

        public async Task User_setBoolValue<TE, TV>(
            Guid? userId,
            string key,
            bool value)
            where TE : dbEvent
            where TV : dbValue
        {
            var read = await User_queryFullValues<TE, TV>(userId)
                .Where(v => v.Value.Key == key)
                .SingleOrDefaultAsync();

            if (read != null)
            {
                if (read.Value.BoolValue == value)
                    return;
                read.Event.End = Now;
            }

            var _value = Activator.CreateInstance<TV>();
            _value.Key = key;
            _value.BoolValue = value;
            await Values.AddAsync(_value);
            await CommitAsync(userId);

            var _event = Activator.CreateInstance<TE>();
            _event.Start = Now;
            _event.UserGuid = userId;
            _event.ValueGuid = _value.ID;
            await Events.AddAsync(_event);
            await CommitAsync(userId);
        }

        public async Task<AuthRecord> GetAuthor(
            string login,
            string password)
        {
            AuthRecord result = null;

            if (login.notEmpty() && password.notEmpty())
            {
                var query =
                    from user in Users
                    let now = Now
                    where user.Events
                              .Where(v => v is dbeUserLogin)
                              .Where(v => v.Start < now && v.End > now)
                              .Select(v => v.StringValue)
                              .SingleOrDefault() == login
                    where user.Events
                              .Where(v => v is dbeUserPassword)
                              .Where(v => v.Start < now && v.End > now)
                              .Select(v => v.StringValue)
                              .SingleOrDefault() == password
                    select new AuthRecord
                    {
                        Guid = user.ID,
                        Caption = user.Events
                            .Where(v => v is dbeUserCaption)
                            .Where(v => v.Start < now && v.End > now)
                            .Select(v => v.StringValue)
                            .SingleOrDefault(),
                        Roles = user.Events
                            .Where(v => v is dbeUserRole)
                            .Where(v => v.Start < now && v.End > now)
                            .Select(v => v.Value.Key)
                            .ToArray(),
                        TimeZone = user.Events
                            .Where(v => v is dbeUserTimeZone)
                            .Where(v => v.Start < now && v.End > now)
                            .Select(v => v.IntValue)
                            .SingleOrDefault()
                    };

                result = await query.SingleOrDefaultAsync();
            }

            if (result == null)
                result = new AuthRecord();

            return result;
        }

        public async Task<Guid?> AddUser(
            string userLogin)
        {
            if (userLogin.isEmpty()) return null;

            var result = await GetUser_byLogin(userLogin);

            if (!result.IsNullOrEmpty()) return result;

            var user = new dbUser();
            result = user.ID;
            user.Events.Add(new dbeUserLogin(userLogin));

            await Users.AddAsync(user);
            await CommitAsync();

            return result;
        }

        public async Task<Guid?> GetUser_byLogin(
            string login)
        {
            var query =
                from e in Events.OfType<dbeUserLogin>()
                where e.Start < Now && e.End > Now
                where e.StringValue == login
                select e.UserGuid;

            return await query.SingleOrDefaultAsync();
        }

        public async Task<dbeUserRole> User_addRole(
            string userLogin,
            string roleKey)
        {
            if (userLogin.isEmpty() || roleKey.isEmpty()) return null;
            var userId = await GetUser_byLogin(userLogin);
            var roleId = await GetRole_byKey(roleKey);
            return await User_addRole(userId, roleId);
        }

        public async Task<dbeUserRole> User_addRole(
            Guid? userId,
            string roleKey)
        {
            if (userId.IsNullOrEmpty() || roleKey.isEmpty()) return null;
            var roleId = await GetRole_byKey(roleKey);
            return await User_addRole(userId, roleId);
        }

        public async Task<dbeUserRole> User_addRole(
            string userLogin,
            Guid? roleId)
        {
            if (userLogin.isEmpty() || roleId.IsNullOrEmpty()) return null;
            var userId = await GetUser_byLogin(userLogin);
            return await User_addRole(userId, roleId);
        }

        public async Task<dbeUserRole> User_addRole(
            Guid? userId,
            Guid? roleId)
        {
            if (userId.IsNullOrEmpty() || roleId.IsNullOrEmpty()) return null;

            var query =
                from e in Events.OfType<dbeUserRole>()
                where e.UserGuid == userId
                where e.ValueGuid == roleId
                where e.Start < Now && e.End > Now
                select e;

            var result = await query.SingleOrDefaultAsync();

            if (result != null) return result;

            result = new dbeUserRole
            {
                UserGuid = userId,
                ValueGuid = roleId
            };

            await Events.AddAsync(result);
            await CommitAsync();

            return result;
        }

        public async Task User_setRoles(
            Guid? userId,
            IEnumerable<string> roles)
        {
            if (userId == null) return;

            if (roles == null)
            {
                roles = new string[0];
            }

            var readRemoved = await (
                    from e in Events.OfType<dbeUserRole>()
                    where e.UserGuid == userId
                    where e.Start < Now && e.End > Now
                    select e)
                    .Include(v=>v.Value)
                .ToArrayAsync();

            var readAdded = await (
                    from e in Values.OfType<dbRole>()
                    where roles.Contains(e.Key)
                    select e)
                .ToArrayAsync();

            readRemoved
                .Where(e => !readAdded.Select(v => v.Key).Contains(e.Value.Key))
                .ForEach(e => { e.End = Now; });

            readAdded
                .Where(v => !readRemoved.Select(e => e.Value.Key).Contains(v.Key))
                .ForEach(v =>
                {
                    Events.AddRange(new dbeUserRole
                    {
                        UserGuid = userId,
                        Value = v,
                        Start = Now
                    });
                });

            await CommitAsync();
        }

        public async Task User_delRole(
            Guid? userId,
            Guid? roleId)
        {
            var query =
                from e in Events
                where e is dbeUserRole
                where e.UserGuid == userId
                where e.ValueGuid == roleId
                select e;

            var read = await query.ToArrayAsync();

            foreach (var value in read)
                value.End = Now;

            await CommitAsync();
        }

        public async Task<string> User_getPassword(
            Guid? userId)
        {
            return await User_getStringValue<dbeUserPassword>(userId);
        }

        public async Task User_setPassword(
            Guid? userId,
            string password)
        {
            await User_setStringValue<dbeUserPassword>(userId, password);
        }

        public async Task<string> User_getLogin(
            Guid? userId)
        {
            return await User_getStringValue<dbeUserLogin>(userId);
        }

        public async Task User_setLogin(
            Guid? userId,
            string login)
        {
            await User_setStringValue<dbeUserLogin>(userId, login);
        }

        public async Task<int> User_getTimeZone(
            Guid? userId)
        {
            return await User_getIntValue<dbeUserTimeZone>(userId);
        }

        public async Task User_setTimeZone(
            Guid? userId,
            int timezone)
        {
            await User_setIntValue<dbeUserTimeZone>(userId, timezone);
        }

        public async Task<string> User_getCaption(
            Guid? userId)
        {
            return await User_getStringValue<dbeUserCaption>(userId);
        }

        public async Task User_setCaption(
            Guid? userId,
            string caption)
        {
            await User_setStringValue<dbeUserCaption>(userId, caption);
        }


        public async Task<IEnumerable<dbeUserConfig>> User_getConfigs(
            Guid? userId)
        {
            return await User_queryEvents<dbeUserConfig>(userId)
                .ToArrayAsync();
        }

        public async Task User_setConfig(
            Guid? userId,
            string key,
            bool value)
        {
            await User_setBoolValue<dbeUserConfig>(userId, key, value);
        }

        public async Task User_setConfig(
            Guid? userId,
            string key,
            string value)
        {
            await User_setStringValue<dbeUserConfig>(userId, key, value);
        }
    }

    public class EventValue<TE, TV>
    {
        public TE Event { get; set; }
        public TV Value { get; set; }
    }
}