﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreSupport.Reflection;
using LibCoreDB.Tables;
using Microsoft.EntityFrameworkCore;

namespace LibCoreDB
{
    partial class DataDb
    {
        /// <summary>
        /// Простой запрос
        /// </summary>
        /// <returns></returns>
        public IQueryable<dbValue> queryValues()
            => from value in Values
                select value;

        /// <summary>
        /// Простой запрос с указанием типа
        /// </summary>
        /// <typeparam name="TV"></typeparam>
        /// <returns></returns>
        public IQueryable<TV> queryValues<TV>()
            where TV : dbValue
            => from value in Values.OfType<TV>()
               select value;

        /// <summary>
        /// Простой запрос по ключу
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<Guid?> getGuidValue_byKey(
            string key)
        {
            if (key.isEmpty()) return null;

            var read = await queryValues()
                .Where(v => v.Key == key)
                .FirstOrDefaultAsync();

            return read?.ID;
        }

        /// <summary>
        /// Простой запрос по ключу и типу
        /// </summary>
        /// <typeparam name="TV"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<Guid?> getGuidValue_byKey<TV>(
            string key)
            where TV: dbValue
        {
            if (key.isEmpty()) return null;

            var read = await queryValues<TV>()
                .Where(v => v.Key == key)
                .FirstOrDefaultAsync();

            return read?.ID;
        }
    }
}
