using CoreSupport.Reflection;
using CoreSupport.Type;

namespace LibCoreDB
{
    public class ModelDB
    {
        public ModelDB(
            DataDb db,
            DictSO value)
        {
            this.db = db;
            this.value = value ?? new DictSO();
        }

        public ModelDB(
            DataDb db,
            object value = null)
            : this(db, value?.ToDictSO())
        {
        }

        public DictSO value { get; set; }

        public DataDb db { get; set; }
    }
}