using System;
using System.Collections.Generic;
using CoreSupport.DataBase.Table;

namespace LibCoreDB.Tables
{
    public class dbClient
        : fxaGuidTable
            , fxiCaptionTable
            , fxiDescriptionTable
            , fxiStartEndTable
            , fxiCreateAuthorIdTable
            , fxiUpdateAuthorIdTable
    {
        public string Caption { get; set; }
        public string Description { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public Guid? CreateAuthorID { get; set; }
        public Guid? UpdateAuthorID { get; set; }

        public dbClient()
        {
            Events = new HashSet<dbEvent>();
        }

        public virtual ICollection<dbEvent> Events { get; set; }
    }
}