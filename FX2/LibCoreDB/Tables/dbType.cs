using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CoreSupport.DataBase.Table;

namespace LibCoreDB.Tables
{
    public class dbType
        : fxaGuidTable
            , fxiCaptionTable
            , fxiDescriptionTable
            , fxiOrderTable
    {
        public string Key { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }

        public dbType()
        {
            Events = new HashSet<dbEvent>();
        }

        public virtual ICollection<dbEvent> Events { get; set; }
    }

    public class dbClientType : dbType
    {
    }
}