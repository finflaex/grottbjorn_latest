﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreSupport.DataBase.Table;
using CoreSupport.Reflection;

#endregion

namespace LibCoreDB.Tables
{
    public class dbUser : fxaGuidTable
    {
        public dbUser()
        {
            Events = new HashSet<dbEvent>();
        }

        public virtual ICollection<dbEvent> Events { get; set; }

        public static async Task Initialize(DataDb db)
        {
            var user = await db.AddUser("architector");
            var roleSystem = await db.CreateRole(user, "system");
            await db.User_addRole(user, roleSystem);
            await db.User_setPassword(user, "password");
            await db.User_setTimeZone(user, 5);
            await db.User_setCaption(user, "Система");
        }
    }
}