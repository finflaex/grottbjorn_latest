#region

using System;
using System.Linq;
using CoreSupport.DataBase.Table;

#endregion

namespace LibCoreDB.Tables
{
    public class dbEvent
        : fxaGuidTable, 
        fxiDateCreateTable, 
        fxiCreateAuthorIdTable, 
        fxiOrderTable, 
        fxiStartEndTable, 
        fxiKeyTable,
        fxiMultiValueTable
    {
        public dbEvent()
        {
        }

        protected dbEvent(dbValue value)
            :this()
        {
            Value = value;
            ValueGuid = value.ID;
        }

        public dbEvent(byte[] value)
            :this()
        {
            RawValue = value;
        }

        public dbEvent(string value)
            : this()
        {
            StringValue = value;
        }

        public dbEvent(bool value)
            : this()
        {
            BoolValue = value;
        }

        public dbEvent(int value)
            : this()
        {
            IntValue = value;
        }

        public dbEvent(string key, byte[] value)
            : this(value)
        {
            Key = key;
        }

        public dbEvent(string key, string value)
            : this(value)
        {
            Key = key;
        }

        public dbEvent(string key, bool value)
            : this(value)
        {
            Key = key;
        }

        public dbEvent(string key, int value)
            : this(value)
        {
            Key = key;
        }

        public Guid? ValueGuid { get; set; }
        public virtual dbValue Value { get; set; }

        public Guid? TypeGuid { get; set; }
        public virtual dbType Type { get; set; }

        public Guid? ClientGuid { get; set; }
        public virtual dbClient Client { get; set; }

        public Guid? UserGuid { get; set; }
        public virtual dbUser User { get; set; }

        public string StringValue { get; set; }
        public byte[] RawValue { get; set; }
        public int IntValue { get; set; }
        public bool BoolValue { get; set; }

        public Guid? CreateAuthorID { get; set; }
        public DateTimeOffset DateCreate { get; set; }
        public int Order { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public string Key { get; set; }
    }

    public class dbeUserRole : dbEvent
    {
    }

    public class dbeUserLogin : dbEvent
    {
        public dbeUserLogin()
        {
        }
        public dbeUserLogin(string value) : base(value)
        {
            Start = DateTimeOffset.Now;
        }
    }

    public class dbeUserPassword : dbEvent
    {
        public dbeUserPassword()
        {
        }
        public dbeUserPassword(string value) : base(value)
        {
            Start = DateTimeOffset.Now;
        }
    }

    public class dbeUserCaption : dbEvent
    {
        public dbeUserCaption()
        {
        }
        public dbeUserCaption(string value) : base(value)
        {
            Start = DateTimeOffset.Now;
        }
    }

    public class dbeUserTimeZone : dbEvent
    {
        public dbeUserTimeZone()
        {
        }
        public dbeUserTimeZone(int value) : base(value)
        {
            Start = DateTimeOffset.Now;
        }
    }

    public class dbeUserConfig : dbEvent
    {
    }

    public class dbeClientLabel : dbEvent
    {
        public dbeClientLabel()
        {
        }
        public dbeClientLabel(dbLabel value) : base(value)
        {
            Start = DateTimeOffset.Now;
        }
    }

    public class dbeClientCaption : dbEvent
    {
        public dbeClientCaption()
        {
        }
        public dbeClientCaption(string value) : base(value)
        {
            Start = DateTimeOffset.Now;
        }
    }

    public class dbeClientInn : dbEvent
    {
        public dbeClientInn()
        {
        }
        public dbeClientInn(string value) : base(value)
        {
            Start = DateTimeOffset.Now;
        }
    }

    public class dbeClientFullName : dbEvent
    {
        public dbeClientFullName()
        {
        }
        public dbeClientFullName(string value) : base(value)
        {
            Start = DateTimeOffset.Now;
        }
    }

    public class dbeClientShortName : dbEvent
    {
        public dbeClientShortName()
        {
        }
        public dbeClientShortName(string value) : base(value)
        {
            Start = DateTimeOffset.Now;
        }
    }
}