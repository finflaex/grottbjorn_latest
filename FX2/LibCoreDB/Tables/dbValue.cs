#region

using System;
using System.Collections.Generic;
using System.Text;
using CoreSupport.DataBase.Table;
using CoreSupport.Reflection;

#endregion

namespace LibCoreDB.Tables
{
    public class dbValue
        : fxaGuidTable, fxiKeyTable, fxiOrderTable, fxiCreateAuthorIdTable
    {
        public dbValue()
            :base()
        {
            
        }

        public dbValue(int value)
            : this()
        {
            IntValue = value;
        }

        public dbValue(string key, bool value)
            : this()
        {
            Key = key;
            BoolValue = value;
        }

        public dbValue(string key, string value)
            : this()
        {
            Key = key;
            StringValue = value;
        }

        public dbValue(string key, int value)
            : this(value)
        {
            Key = key;
        }

        public string StringValue { get; set; }
        public byte[] RawValue { get; set; }
        public int IntValue { get; set; }
        public bool BoolValue { get; set; }

        public virtual ICollection<dbEvent> Events { get; set; }
        public Guid? CreateAuthorID { get; set; }

        public string Key { get; set; }

        public int Order { get; set; }
    }

    public class dbRole : dbValue
    {
        public const string KeySystem = "system";
    }

    public class dbLabel : dbValue
    {
    }
}