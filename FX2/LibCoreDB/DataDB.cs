﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreSupport.DataBase.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.DependencyInjection;
using LibCoreDB.Tables;

#endregion

namespace LibCoreDB
{
    public partial class DataDb : FxDbContext<DataDb>
    {
        private static TimeSpan? _now;
        public DateTimeOffset Now
        {
            get
            {
                if (_now == null)
                    return DateTimeOffset.Now;
                else
                    return DateTimeOffset.Now - _now.Value;
            }
            set { _now = DateTimeOffset.Now - value; }
        }

        public static async Task Initialize(IServiceProvider appApplicationServices)
        {
            var db = appApplicationServices.GetService<DataDb>();

            await dbUser.Initialize(db);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "Data Source=mars;Integrated Security=False;User ID=sa;Password=411;Database=CoreDB2;"
                //$@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=CoreDB2;Integrated Security=True;"
            );
        }

        public DbSet<dbType> Types { get; set; }
        public DbSet<dbValue> Values { get; set; }
        public DbSet<dbEvent> Events { get; set; }
        public DbSet<dbUser> Users { get; set; }
        public DbSet<dbClient> Clients { get; set; }


        protected override void OnModelCreating(ModelBuilder db)
        {
            base.OnModelCreating(db);

            db.Entity<dbRole>().ToTable("Values");
            db.Entity<dbeUserRole>().ToTable("Events");

            db.Entity<dbeUserLogin>().ToTable("Events");
            db.Entity<dbeUserPassword>().ToTable("Events");
            db.Entity<dbeUserCaption>().ToTable("Events");
            db.Entity<dbeUserTimeZone>().ToTable("Events");
            db.Entity<dbeUserConfig>().ToTable("Events");

            db.Entity<dbeClientCaption>().ToTable("Events");
            db.Entity<dbeClientInn>().ToTable("Events");

            #region events

            db.Entity<dbUser>()
                .HasMany(v => v.Events)
                .WithOne(v => v.User)
                .HasForeignKey(v => v.UserGuid)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            db.Entity<dbValue>()
                .HasMany(v => v.Events)
                .WithOne(v => v.Value)
                .HasForeignKey(v => v.ValueGuid)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            db.Entity<dbClient>()
                .HasMany(v => v.Events)
                .WithOne(v => v.Client)
                .HasForeignKey(v => v.ValueGuid)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            db.Entity<dbType>()
                .HasMany(v => v.Events)
                .WithOne(v => v.Type)
                .HasForeignKey(v => v.ValueGuid)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            #endregion
        }
    }
}
