//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClassLibrary1
{
    using System;
    using System.Collections.Generic;
    
    public partial class ForeignRegistrations
    {
        public System.Guid ID { get; set; }
        public Nullable<long> CountryID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Address { get; set; }
        public string Number { get; set; }
    
        public virtual Oksms Oksms { get; set; }
        public virtual Organizations Organizations { get; set; }
    }
}
