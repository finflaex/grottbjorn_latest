//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClassLibrary1
{
    using System;
    using System.Collections.Generic;
    
    public partial class PathFiles
    {
        public System.Guid ID { get; set; }
        public int PathOldID { get; set; }
        public string Path { get; set; }
        public int Type { get; set; }
        public System.Guid EventID { get; set; }
        public int Size { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
    
        public virtual Events Events { get; set; }
    }
}
