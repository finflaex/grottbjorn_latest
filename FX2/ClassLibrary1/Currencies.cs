//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClassLibrary1
{
    using System;
    using System.Collections.Generic;
    
    public partial class Currencies
    {
        public System.Guid ID { get; set; }
        public string CurrencyKey { get; set; }
        public System.Guid OrganizationID { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public decimal Year { get; set; }
        public decimal Single { get; set; }
    
        public virtual HelpCurrencies HelpCurrencies { get; set; }
        public virtual Organizations Organizations { get; set; }
    }
}
