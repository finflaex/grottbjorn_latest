﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Support.Attributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class fatValue : Attribute
    {
        public fatValue(string value)
        {
            Key = string.Empty;
            Value = value;
        }

        public fatValue(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public string Value { get; set; }
        public string Key { get; set; }
    }

    public static class fxhCaptionAttribute
    {
        public static string AtValue(this Enum value, string key = null)
        {
            return value
                       .GetType()
                       .GetField(value.ToString())
                       .GetCustomAttributes(typeof(fatValue))
                       .OfType<fatValue>()
                       .Where(v => key == null || v.Key == key)
                       .Select(v => v.Value)
                       .FirstOrDefault() ?? string.Empty;
        }
    }
}
