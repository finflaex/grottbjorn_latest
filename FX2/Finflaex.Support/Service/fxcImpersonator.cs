﻿#region

using System;
using System.ComponentModel;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.InteropServices;
using System.Security.Principal;
using Finflaex.Support.WinApi;

#endregion

namespace Finflaex.Support.Service
{
    public class fxcImpersonator : IDisposable
    {
        private WindowsImpersonationContext _wic;

        /// <summary>
        ///     Begins impersonation with the given credentials, Logon type and Logon provider.
        /// </summary>
        /// <param name="userName"> Name of the user.</param>
        /// <param name="domainName"> Name of the domain.</param>
        /// <param name="password"> The password. <see cref="System.String" /></param>
        /// < param name="logonType">Type of the logon.</param>
        /// <param name="logonProvider">
        ///     The logon provider.
        ///     <see cref="Mit.Sharepoint.WebParts.EventLogQuery.Network.LogonProvider" />
        /// </param>
        public fxcImpersonator(string userName, string domainName, string password, advapi32.LogonType logonType,
            advapi32.LogonProvider logonProvider)
        {
            Impersonate(userName, domainName, password, logonType, logonProvider);
        }

        /// <summary>
        ///     Begins impersonation with the given credentials.
        /// </summary>
        /// <param name="userName"> Name of the user.</param>
        /// <param name="domainName"> Name of the domain.</param>
        /// <param name="password"> The password. <see cref="System.String" /></param>
        public fxcImpersonator(string userName, string domainName, string password)
        {
            Impersonate(userName, domainName, password, advapi32.LogonType.LOGON32_LOGON_INTERACTIVE,
                advapi32.LogonProvider.LOGON32_PROVIDER_DEFAULT);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Impersonator" /> class.
        /// </summary>
        public fxcImpersonator()
        {
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            UndoImpersonation();
        }

        /// <summary>
        ///     Impersonates the specified user account.
        /// </summary>
        /// <param name="userName"> Name of the user.</param>
        /// <param name="domainName"> Name of the domain.</param>
        /// <param name="password"> The password. <see cref="System.String" /></param>
        /// < param name="logonType">Type of the logon.</param>
        /// <param name="logonProvider"> The logon provider. <see cref="EventLogQuery.Network.LogonProvider" /></param>
        public void Impersonate(
            string userName,
            string domainName,
            string password,
            advapi32.LogonType logonType = advapi32.LogonType.LOGON32_LOGON_INTERACTIVE,
            advapi32.LogonProvider logonProvider = advapi32.LogonProvider.LOGON32_PROVIDER_DEFAULT)
        {
            UndoImpersonation();

            var logonToken = IntPtr.Zero;
            var logonTokenDuplicate = IntPtr.Zero;
            try
            {
                // revert to the application pool identity, saving the identity of the current requestor
                _wic = WindowsIdentity.Impersonate(IntPtr.Zero);

                // do logon & impersonate
                if (advapi32.LogonUser(userName,
                        domainName,
                        password,
                        (int) logonType,
                        (int) logonProvider,
                        ref logonToken) != 0)
                    if (
                        advapi32.DuplicateToken(logonToken, (int) advapi32.ImpersonationLevel.SecurityImpersonation,
                            ref logonTokenDuplicate) != 0)
                    {
                        var wi = new WindowsIdentity(logonTokenDuplicate);
                        wi.Impersonate();
                        // discard the returned identity context (which is the context of the application pool)
                    }
                    else
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                else
                    throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            finally
            {
                if (logonToken != IntPtr.Zero)
                    advapi32.CloseHandle(logonToken);

                if (logonTokenDuplicate != IntPtr.Zero)
                    advapi32.CloseHandle(logonTokenDuplicate);
            }
        }

        /// <summary>
        ///     Stops impersonation.
        /// </summary>
        private void UndoImpersonation()
        {
            // restore saved requestor identity
            _wic?.Undo();
            _wic = null;
        }
    }
}