﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlTypes;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB2.Tables.Interfaces;
using Finflaex.Support.Reflection;

namespace Finflaex.Support.DB2.Bases
{
    public class fxaDbContext<TThis>
        : DbContext
        where TThis : fxaDbContext<TThis>
    {
        public fxaDbContext(string connString)
            : base(connString)
        {
            AuthorID = Guid.Empty;
        }

        public static DateTimeOffset? CurentDate = null;

        public DateTimeOffset Now => CurentDate ?? DateTimeOffset.Now;

        public Guid AuthorID { get; set; }

        public TRec Add<TRec>(TRec record) where TRec : class
        {
            if (record == null) return null;
            Entry(record).State = EntityState.Added;
            return Set<TRec>().Add(record);
        }

        public TRec Attach<TRec>(TRec record) where TRec : class
        {
            if (record == null) return null;
            return Set<TRec>().Attach(record);
        }

        public TRec Detach<TRec>(TRec record) where TRec : class
        {
            if (record == null) return null;

            var entry = Entry(record);

            entry.State = EntityState.Detached;

            return entry.Entity;
        }

        public TThis BeginTracking()
        {
            Configuration.AutoDetectChangesEnabled = false;
            return this.AsType<TThis>();
        }

        public TThis Commit(bool worked = true)
        {
            if (worked) Loging();
            SaveChanges();
            return this.AsType<TThis>();
        }

        public TThis Delete<TRec>(TRec record, bool real = false)
            where TRec : class
        {
            if (record != null)
            {
                record = Attach(record);
                Entry(record).State = EntityState.Deleted;
            }

            return this.AsType<TThis>();
        }

        public TThis EndTracking()
        {
            ChangeTracker.DetectChanges();
            Configuration.AutoDetectChangesEnabled = true;
            return this.AsType<TThis>();
        }

        public TThis Rollback()
        {
            ChangeTracker.Entries().ForEach(v => v.Reload());
            return this.AsType<TThis>();
        }

        public TRec Update<TRec>(TRec record) where TRec : class
        {
            record = Attach(record);
            Entry(record).State = EntityState.Modified;
            return record;
        }

        public TEntity AddOrUpdate<TEntity>(TEntity entity)
            where TEntity : class, fxiGuidTable
        {
            var tracked = Set<TEntity>().Find(entity.ID);
            if (tracked != null)
            {
                Entry(tracked).CurrentValues.SetValues(entity);
                return tracked;
            }

            Set<TEntity>().Add(entity);
            return entity;
        }

        public TEntity AddIfNotExists<TEntity>(TEntity entity)
            where TEntity : class, fxiGuidTable
        {
            var tracked = Set<TEntity>().Find(entity.ID);
            if (tracked != null) return tracked;

            return Set<TEntity>().Add(entity);
        }

        protected virtual void Loging()
        {
            ChangeTracker
                .Entries()
                .ForEach(entry =>
                {
                    if (entry.State == EntityState.Added || entry.State == EntityState.Modified)
                    {
                        entry.CurrentValues.PropertyNames.ForEach(name =>
                        {
                            entry.Property(name).CurrentValue.ActionIfNotNull(property =>
                            {
                                property.GetType().Action(type =>
                                {
                                    if (type == typeof(DateTime))
                                    {
                                        if (property.AsType<DateTime>() < SqlDateTime.MinValue.Value)
                                        {
                                            if (type.IsGenericType &&
                                                type.GetGenericTypeDefinition() == typeof(Nullable<>))
                                            {
                                                entry.Property(name).CurrentValue = null;
                                            }
                                            else
                                            {
                                                entry.Property(name).CurrentValue = SqlDateTime.MinValue.Value;
                                            }
                                        }
                                    }
                                });
                            });
                        });
                    }


                    if (entry.Entity is fxiCreateAuthorIdTable && entry.State == EntityState.Added)
                    {
                        entry.Entity.AsType<fxiCreateAuthorIdTable>().CreateAuthorID = AuthorID;
                    }
                    if (entry.Entity is fxiUpdateAuthorIdTable && (entry.State == EntityState.Added || entry.State == EntityState.Modified))
                    {
                        entry.Entity.AsType<fxiUpdateAuthorIdTable>().UpdateAuthorID = AuthorID;
                    }
                    if (entry.Entity is fxiDateCreateTable && entry.State == EntityState.Added)
                    {
                        entry.Entity.AsType<fxiDateCreateTable>().DateCreate = Now;
                    }
                    if (entry.Entity is fxiDateModifyTable && (entry.State == EntityState.Added || entry.State == EntityState.Modified))
                    {
                        entry.Entity.AsType<fxiDateModifyTable>().DateModify = Now;
                    }
                });
        }
    }
}
