﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB2.Tables.Interfaces;

namespace Finflaex.Support.DB2.Tables.Abstracts
{
    public abstract class fxaHierarchyGuidTable<T>
        : fxiHierarchyTable<T> , fxiGuidTable
        where T : fxaHierarchyGuidTable<T>
    {
        public fxaHierarchyGuidTable()
        {
            ID = Guid.NewGuid();
            Childs = new HashSet<T>();
        }

        [Key, Column(Order = 0)]
        public Guid ID { get; set; }
        public Guid? ParentID { get; set; }
        [ForeignKey(nameof(ParentID))]
        public T Parent { get; set; }
        public ICollection<T> Childs { get; set; }
    }
}
