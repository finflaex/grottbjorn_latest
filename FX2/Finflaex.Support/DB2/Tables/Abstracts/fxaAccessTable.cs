﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Finflaex.Support.DB2.Tables.Interfaces;

namespace Finflaex.Support.DB2.Tables.Abstracts
{
    [Table("AccessList")]
    public abstract class fxaAccessTable<TUser, TTarget, TType>
        : fxiAccessTable<TUser, TTarget, TType>
        where TUser : fxiGuidTable
        where TTarget : fxiGuidTable
    {
        public fxaAccessTable()
        {
            
        }

        public fxaAccessTable(params object[] param)
            :this()
        {
            User = param.OfType<TUser>().FirstOrDefault();
            Target = param.OfType<TTarget>().FirstOrDefault();
            Type = param.OfType<TType>().FirstOrDefault();
        }

        [Key, Column(Order = 0), ForeignKey(nameof(User))]
        public Guid UserID { get; set; }

        [Key, Column(Order = 1), ForeignKey(nameof(Target))]
        public Guid TargetID { get; set; }

        [Key, Column(Order = 3)]
        public TType Type { get; set; }

        public virtual TUser User { get; set; }
        public virtual TTarget Target { get; set; }
    }
}