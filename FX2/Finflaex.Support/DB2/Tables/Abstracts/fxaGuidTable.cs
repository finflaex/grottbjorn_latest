﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB2.Tables.Interfaces;

namespace Finflaex.Support.DB2.Tables.Abstracts
{
    public class fxaGuidTable : fxiGuidTable
    {
        public fxaGuidTable()
        {
            ID = Guid.NewGuid();
        }
        public Guid ID { get; set; }
    }
}
