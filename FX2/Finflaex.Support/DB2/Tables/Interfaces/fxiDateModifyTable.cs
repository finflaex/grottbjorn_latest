﻿using System;

namespace Finflaex.Support.DB2.Tables.Interfaces
{
    public interface fxiDateModifyTable
    {
        DateTimeOffset DateModify { get; set; }
    }
}