﻿using System;

namespace Finflaex.Support.DB2.Tables.Interfaces
{
    public interface fxiUpdateAuthorIdTable
    {
        Guid? UpdateAuthorID { get; set; }
    }
}