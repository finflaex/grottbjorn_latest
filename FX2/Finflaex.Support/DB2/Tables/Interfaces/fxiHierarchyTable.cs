﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

#endregion

namespace Finflaex.Support.DB2.Tables.Interfaces
{
    public interface fxiHierarchyTable<T> : fxiIdTable<Guid>
        where T : fxiHierarchyTable<T>
    {
        Guid? ParentID { get; set; }
        T Parent { get; set; }
        ICollection<T> Childs { get; set; }
    }

    public static class fxrHierarchyTable
    {
        public static List<T> ToParents<T>(
            this IQueryable<T> @this,
            Guid id)
            where T : class, fxiHierarchyTable<T>
        {
            var list = new List<T>();
            var record = @this.FirstOrDefault(v => v.ID == id);
            while (record != null)
            {
                list.Add(record);
                record = record.Parent;
            }
            return list;
        }

        public static IQueryable<T> WithParents<T>(
            this DbContext @this,
            Guid id)
            where T : class, fxiHierarchyTable<T>
        {
            return
                @this.Database.SqlQuery<T>(
                    $@"WITH RECURSIVE child_to_parents AS (
                  SELECT * FROM {typeof (
                        T).Name}
                      WHERE [ID] = @id
                  UNION ALL
                  SELECT * FROM {typeof
                            (T).Name}, child_to_parents
                      WHERE [ID] = [child_to_parents].[ParentID]
                )
                SELECT * FROM child_to_parents",
                    new SqlParameter("id", id))
                    .AsQueryable();
        }
    }
}