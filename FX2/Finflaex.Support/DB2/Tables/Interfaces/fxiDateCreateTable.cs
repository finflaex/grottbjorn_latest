﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Support.DB2.Tables.Interfaces
{
    public interface fxiDateCreateTable
    {
        [Display(Name = "Дата создания")]
        DateTimeOffset DateCreate { get; set; }
    }
}
