﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Support.DB2.Tables.Interfaces
{
    public interface fxiCreateAuthorIdTable
    {
        Guid? CreateAuthorID { get; set; }
    }
}
