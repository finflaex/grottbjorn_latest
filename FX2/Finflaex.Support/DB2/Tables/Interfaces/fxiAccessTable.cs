﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Support.DB2.Tables.Interfaces
{
    public interface fxiAccessTable<TUser, TTarget, TType>
        where TUser : fxiGuidTable
        where TTarget : fxiGuidTable
    {
        Guid UserID { get; set; }
        Guid TargetID { get; set; }
        TType Type { get; set; }
        TUser User { get; set; }
        TTarget Target { get; set; }
    }
}
