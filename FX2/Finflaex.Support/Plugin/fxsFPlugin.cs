﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Finflaex.Support.IO;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.Plugin
{
    public static class fxsFPlugin
    {
        private static readonly Dictionary<string, fxcFPluginFile> basa = new Dictionary<string, fxcFPluginFile>();
        public static event Action<fxcFPluginFile> onAdd;
        public static event Action<fxcFPluginFile> onDelete;
        public static event Action<fxcFPluginFile> onChange;

        public static void Load(params string[] paths)
        {
            paths.ForEach(path => { path.ToDirectory().OnWatcher += OnOnWatcher; });

            paths
                .SelectMany(path => path.EnumerableFiles("*.dll", false))
                .ForEach(path =>
                {
                    var plugin = path.Instance<fxcFPluginFile>();
                    onAdd?.Invoke(plugin);
                    basa.Add(path, plugin);
                });
        }

        private static void OnOnWatcher(FileSystemEventArgs args)
        {
            switch (args.ChangeType)
            {
                case WatcherChangeTypes.Created:
                    if ((args.FullPath.ToPath().Ext == "dll") && File.Exists(args.FullPath))
                    {
                        var plugin = args.FullPath.Instance<fxcFPluginFile>();
                        onAdd?.Invoke(plugin);
                        basa.Add(args.FullPath, plugin);
                    }
                    break;
                case WatcherChangeTypes.Deleted:
                    var del_plugin = basa[args.FullPath];
                    onDelete?.Invoke(del_plugin);
                    basa.Remove(args.FullPath);
                    break;
                case WatcherChangeTypes.Changed:
                    if ((args.FullPath.ToPath().Ext == "dll") && File.Exists(args.FullPath))
                    {
                        basa.Remove(args.FullPath);
                        var change_plugin = args.FullPath.Instance<fxcFPluginFile>();
                        onChange?.Invoke(change_plugin);
                        basa.Add(args.FullPath, change_plugin);
                    }
                    break;
            }
        }

        /// <summary>
        /// </summary>
        /// <typeparam name="T">Interface</typeparam>
        /// <returns></returns>
        public static IEnumerable<Type> List<T>()
            where T : class
        {
            return basa.SelectMany(v => v.Value.GetExtension<T>());
        }

        public static void LoadServer(params string[] paths)
        {
            paths
                .Select(v => v
                    .ToPath()
                    .ToServerPath())
                .ToArray()
                .Action(Load);
        }
    }

    public class fxcFPluginFile
    {
        private readonly Assembly assembly;

        public fxcFPluginFile(string file)
        {
            assembly = file.FromFile().ToAssembly();
        }

        public IEnumerable<Type> GetExtension<T>()
            where T : class
        {
            return assembly
                .GetTypes()
                .Where(t => !t.IsInterface && !t.IsAbstract)
                .Where(t => t.GetInterface(typeof(T).Name) != null);
        }
    }
}