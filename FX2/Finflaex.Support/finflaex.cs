﻿#region

using System;
using System.IO;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using Finflaex.Support.IO;
using Finflaex.Support.WinApi;

#endregion

namespace Finflaex.Support
{
    public static class finflaex
    {
        public static string ExecutablePath
        {
            get
            {
                var executablePath = "";
                var assembly1 = Assembly.GetEntryAssembly();
                if (assembly1 == null)
                {
                    var builder1 = new StringBuilder(260);
                    kernel32.GetModuleFileName(IntPtr.Zero, builder1, builder1.Capacity);
                    executablePath = Path.GetFullPath(builder1.ToString());
                }
                else
                {
                    var text1 = assembly1.EscapedCodeBase;
                    var uri1 = new Uri(text1);
                    executablePath = uri1.Scheme == "file" ? text1.ToPath().ToLocalPath() : uri1.ToString();
                }

                var uri2 = new Uri(executablePath);
                if (uri2.Scheme == "file")
                    new FileIOPermission(FileIOPermissionAccess.PathDiscovery, executablePath).Demand();
                return executablePath;
            }
        }

        public static string ExecutableDirectory
            => Path.GetDirectoryName(ExecutablePath);
    }
}