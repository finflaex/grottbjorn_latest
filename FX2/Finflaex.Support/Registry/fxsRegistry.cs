﻿#region

using System;
using System.Reflection;
using Finflaex.Support.Log;
using Finflaex.Support.Reflection;
using Microsoft.Win32;

#endregion

namespace Finflaex.Support.Registry
{
    public static class fxsRegistry
    {
        public static string Folder => "finflaex";

        public static RegistryKey folder => Microsoft.Win32.Registry
            .LocalMachine?
            .OpenSubKey("SOFTWARE", true)?
            .CreateSubKey(Folder, true);

        public static void WriteConfig<TVal>(string key, TVal value)
        {
            try
            {
                if (folder == null)
                    throw new Exception($@"проблема открыть реестр по пути 'HKEY_CURRENT_USER\{Folder}'");
                if (typeof(TVal) == typeof(string))
                    folder.SetValue(key, value);
                else
                    folder.SetValue(key, value.ToJson());
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    typeof(fxsRegistry),
                    MethodBase.GetCurrentMethod(),
                    error);
            }
        }

        public static TVal ReadConfig<TVal>(string key)
        {
            try
            {
                if (folder == null)
                    throw new Exception($@"проблема открыть реестр по пути 'HKEY_CURRENT_USER\{Folder}'");
                return
                    typeof(TVal) == typeof(string)
                        ? folder.GetValue(key).AsType<TVal>()
                        : folder.GetValue(key).AsType<string>().FromJson<TVal>();
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    typeof(fxsRegistry),
                    MethodBase.GetCurrentMethod(),
                    error);
                return default(TVal);
            }
        }

        public static void RemoveConfig(string key)
        {
            try
            {
                if (folder == null)
                    throw new Exception($@"проблема открыть реестр по пути 'HKEY_CURRENT_USER\{Folder}'");
                folder.DeleteValue(key);
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    typeof(fxsRegistry),
                    MethodBase.GetCurrentMethod(),
                    error);
            }
        }
    }
}