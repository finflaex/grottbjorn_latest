﻿#region

using System;
using System.IO;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.Console
{
    public static class fxsConsole
    {
        public static TextWriter OriginalWriter { get; set; }

        public static void RedirectToFile(string path)
        {
            StreamWriter writer;
            OriginalWriter = System.Console.Out;
            try
            {
                var ostrm = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
                writer.AutoFlush = true;
            }
            catch (Exception e)
            {
                System.Console.WriteLine("Cannot open Redirect.txt for writing");
                System.Console.WriteLine(e.Message);
                return;
            }
            System.Console.SetOut(writer);
        }

        public static void Reset()
        {
            OriginalWriter.ActionIfNotNull(() => { System.Console.SetOut(OriginalWriter); });
        }
    }
}