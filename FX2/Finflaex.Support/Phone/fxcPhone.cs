namespace Finflaex.Support.Phone
{
    public class fxcPhone
    {
        public static implicit operator string(fxcPhone value) => value.ToString();

        public bool IsRus => RusPhoneNumber != null;
        public bool Parse { get; internal set; }
        public string CountryCode { get; internal set; }
        public string RusPhoneNumber { get; internal set; }
        public string RusRegionCode { get; internal set; }
        public string ZagranPhoneNumber { get; internal set; }
        public string RawPhone { get; internal set; }

        public override string ToString()
            => Parse
                ? IsRus
                    ? $"{RusRegionCode}{RusPhoneNumber}"
                    : $"{CountryCode}{ZagranPhoneNumber}"
                : RawPhone;
    }
}