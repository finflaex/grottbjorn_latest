﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Finflaex.Support.Reflection;

namespace Finflaex.Support.Phone
{
    public static class fxsPhone
    {
        public static IEnumerable<string> RusCodeOut = new[]
        {
            "(?:(?!7[67])7)",
            "810",
            "8",
            "998",
            "98",
            "9",
        };

        public static IEnumerable<string> ZagranCodeOut = new[]
        {
            "99810",
            "9810",
            "98",
            "810",
            "9",
            "09",
            "10",
            "19",
            "29"
        };

        private static IEnumerable<string> RusRegionCode => new[]
        {
            @"[3456789]\d{2}"
        };

        private static IEnumerable<string> RusPhone => new[]
        {
            @"\d{7}"
        };

        private static IEnumerable<string> CountryCode => new[]
        {
            "1",
            "7",
            "2[017]",
            "3[0123469]",
            "4[013456789]",
            "5[12345678]",
            "6[0123456",
            "8[1246]",
            "9[0123458]",
            "212",
            "2[23456][0123456789]",
            "284",
            "29[1789]",
            "34[05]",
            "35[0123456789]",
            "37[012345678]",
            "38[015679]",
            "42[01]",
            "441",
            "473",
            "5[09][0123456789]",
            "649",
            "664",
            "6[78][0123456789]",
            "69[012]",
            "758",
            "767",
            "78[47]",
            "809",
            "85[02356]",
            "86[89]",
            "876",
            "88[06]",
            "96[0123456789]",
            "97[1234567]",
            "99[234569]",
            "99[34568]"
        };

        public static IEnumerable<string> ZagranPhoneNumber => new[]
        {
            @"\d{6,11}"
        };

        public static string RusRegexPattern => $@"^(?:{RusCodeOut.Join("|")})?({RusRegionCode.Join("|")})?({RusPhone.Join("|")})$";

        public static string ZagranRegexPattern => $@"^(?:{ZagranCodeOut.Join("|")})?({CountryCode.Join("|")})({ZagranPhoneNumber.Join("|")})$";

        public static string AllRegexPattern => $@"(?:{RusRegexPattern}$)|(?:{ZagranRegexPattern})";

        public static fxcPhone PhoneParse(this string @this)
            => @this
                .ClearExcept("0123456789")
                .DoRegexMatches(AllRegexPattern)
                .FuncSelect(matches =>
                {
                    var result = new fxcPhone
                    {
                        RawPhone = @this
                    };
                    if (result.Parse = matches.Length == 1)
                    {
                        var groups = matches[0]
                            .Groups.Cast<Group>()
                            .Select(v => v.Value)
                            .ToArray();

                        result.RusRegionCode = groups.Skip(1).FirstOrDefault().retIfNullOrWhiteSpace(null);
                        result.RusPhoneNumber = groups.Skip(2).FirstOrDefault().retIfNullOrWhiteSpace(null);
                        result.CountryCode = groups.Skip(3).FirstOrDefault().retIfNullOrWhiteSpace(null);
                        result.ZagranPhoneNumber = groups.Skip(4).FirstOrDefault().retIfNullOrWhiteSpace(null);

                        if (result.IsRus && result.RusRegionCode == null)
                        {
                            result.RusRegionCode = "343";
                        }
                    }
                    return result;
                });

        public static bool IsPhoneNumber(this string @this)
        {
            return @this
                .ClearExcept("0123456789")
                .DoRegexIsMatch(AllRegexPattern);
        }
    }
}
