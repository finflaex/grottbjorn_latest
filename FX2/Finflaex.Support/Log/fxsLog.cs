﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

#endregion

namespace Finflaex.Support.Log
{
    public static class fxsLog
    {
        private static readonly List<fxiLog> _logers;

        static fxsLog()
        {
            _logers = new List<fxiLog>();
            AddEvents(EventLoger);
        }

        private static event Action<fxcLog> _event;

        /// <summary>
        /// </summary>
        /// <param name="param">
        ///     MethodBase, Exception, string message,fxeLog[] types,Type, fxcDictSO data, DateTime, int code, bool propbros
        /// </param>
        public static void Write(params object[] param)
        {
            var data = new fxcLog(param);
            try
            {
                _event?.Invoke(data);
            }
            catch (Exception err)
            {
                ;
            }
            if (data.Types.Contains(fxeLog.Error) && (data.Exception != null) && data.Probros)
                throw data.Exception;
        }

        public static void SetEvents(params Action<fxcLog>[] param)
        {
            _event = null;
            AddEvents(EventLoger);
            AddEvents(param);
        }

        private static void EventLoger(fxcLog log)
            => _logers.ForEach(loger => loger.OnMessage(log));

        public static void AddEvents(params Action<fxcLog>[] param)
        {
            param.ForEach(action => _event += action);
        }

        public static void AddLoggers(params fxiLog[] param)
        {
            _logers.AddRange(param);
        }

        public static string qwe(int value = 0)
        {
            try
            {
                var qwe = 0;
                return (value/qwe).ToString();
            }
            catch (Exception error)
            {
                Write(
                    fxeLog.Error,
                    "+".Repeat(200),
                    MethodBase.GetCurrentMethod(),
                    typeof(fxsLog),
                    200,
                    error,
                    new fxcDictSO
                    {
                        ["qwe"] = "asd"
                    },
                    false
                );
                return null;
            }
        }
    }
}