﻿#region

using System;
using System.Linq;
using System.Reflection;
using System.Text;
using Finflaex.Support.DB.Connections;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

#endregion

namespace Finflaex.Support.Log
{
    public class fxcLog
    {
        private readonly object[] param;

        public fxcLog(object[] param)
        {
            this.param = param;
            Date = DateTime.Now;
        }

        public MethodBase Method => param.OfType<MethodBase>().FirstOrDefault();
        public Exception Exception => param.OfType<Exception>().FirstOrDefault();
        public string Message => param.OfType<string>().FirstOrDefault();
        public fxeLog[] Types => param.OfType<fxeLog>().ToArray();
        public Type Class => param.OfType<Type>().FirstOrDefault();
        public fxcDictSO Data => param.OfType<fxcDictSO>().FirstOrDefault();
        public DateTime Date { get; }
        public int Code => param.OfType<int>().FirstOrDefault();
        public bool Probros => param.OfType<bool>().FirstOrDefault();
        public bool SqlWork => (Class != typeof(fxcConnection)) && (Code != -123456789);

        public override string ToString()
        {
            var result = new StringBuilder();

            Types
                .Select(v => v.ToString())
                .Join(", ")
                .Append(" [ ")
                .Append(Code.ToString("0000"))
                .Append(" ][ ")
                .Append(Date.ToString("HH:mm:ss.ffff"))
                .Append(" ] ")
                .CompleteAfter("=", 80)
                .Action(result.AppendLine);
            if (Message != null)
            {
                "\tMESSAGE:"
                    .Action(result.AppendLine);
                Message?.Align(64, "\t\t").Action(result.AppendLine);
            }
            if ((Class != null) || (Method != null))
                "\tTARGET:"
                    .Action(result.AppendLine);
            if (Class != null)
                "\t\tclass:\t\t"
                    .Append(Class?.FullName ?? "unknown")
                    .Action(result.AppendLine);
            if (Method != null)
            {
                "\t\tmethod:\t\t"
                    .Append(Method?.Name ?? "unknown")
                    .Action(result.AppendLine);
                "\t\treturn:\t\t"
                    .Append(Method.AsType<MethodInfo>()?.ReturnType.FullName ?? "unknown")
                    .Action(result.AppendLine);
                Method?
                    .GetParameters()
                    .ForEach((i, v) =>
                    {
                        $"\t\tparam[{i.ToString("00")}]:\t"
                            .Append($"{v.Name} as {v.ParameterType.FullName}")
                            .Action(result.AppendLine);
                    });
            }
            var err = Exception;
            while (err != null)
            {
                $"\tEXCEPTION [ next > {Probros.ToString().ToLower()} ]:"
                    .Action(result.AppendLine);
                "\t\ttype:\t\t"
                    .Append(Exception.GetType().FullName)
                    .Action(result.AppendLine);
                "\t\tmessage:\t"
                    .Append(Exception.Message)
                    .Action(result.AppendLine);
                Exception?
                    .ToTraces()
                    .ForEach((index, trace) =>
                    {
                        $"\t\ttrace[{index:00}]:\t"
                            .Append($"[{trace.File}],[{trace.Line},{trace.Column}] {trace.Method}")
                            .Action(result.AppendLine);
                    });
                err = err.InnerException;
            }
            if (Data != null)
                $"\tDATA:\r\n"
                    .Append(
                        Data
                            .ToJson(true)
                            .ToLines()
                            .Select(v => "\t" + v)
                            .Join("\r\n")
                    )
                    .Action(result.AppendLine);

            return result.ToString();
        }
    }
}