﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Timers;
using Finflaex.Support.Mail;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.Log
{
    public class fxcMailLog : fxcMail, fxiLog, IDisposable
    {
        public readonly List<MailAddress> AdressBook = new List<MailAddress>();
        private readonly MailAddress from;
        private readonly List<string> Messages = new List<string>();

        public fxcMailLog(string host, int port, string login, string pass, bool ssl = false)
            : base(host, login, pass, port, ssl)
        {
            from = new MailAddress(login, From);
            var timer = new Timer
            {
                AutoReset = true,
                Interval = 60000
            };
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        public string Title { get; set; }
        public string From { get; set; }

        public void OnMessage(fxcLog log)
        {
            lock (Messages)
            {
                Messages.Add(log.ToString());
            }
        }

        public void Dispose()
        {
            Timer_Elapsed(null, null);
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Messages.Any().IfTrue(() =>
            {
                lock (Messages)
                {
                    var body = Messages.Join("\r\n");
                    AdressBook.ForEach(contact => Send(from, contact, Title, body));
                    Messages.Clear();
                }
            });
        }
    }
}