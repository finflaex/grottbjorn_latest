﻿namespace Finflaex.Support.Log
{
    public enum fxeLog
    {
        Debug,
        UserInfo,
        Error,
        FinflaexError,
        AdminError
    }
}