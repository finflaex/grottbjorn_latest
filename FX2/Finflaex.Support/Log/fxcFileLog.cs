﻿#region

using Finflaex.Support.IO;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.Log
{
    public class fxcFileLog : fxcFileAsync, fxiLog
    {
        public fxcFileLog(params object[] param)
            : base(param)
        {
        }

        public void OnMessage(fxcLog log)
        {
            log.ToString().Action(Write);
        }
    }
}