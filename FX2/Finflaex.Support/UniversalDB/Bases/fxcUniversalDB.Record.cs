﻿#region

using System;
using System.Linq;
using System.Linq.Expressions;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.UniversalDB.Bases
{
    partial class fxcUniversalDB<TThis, TType, TRecord, TLink, TValue>
    {
        private TRecord _record_second;
        private TRecord _record_select;
        private TRecord _record_target;

        public TRecord RecordSelect => _record_select;
        public TRecord RecordTarget => _record_target;
        public TRecord RecordSecond => _record_second;

        public TThis RecordForTarget => _record_select
            .ActionOut(out _record_target)
            .FuncSelect(this.AsType<TThis>)
            .TypeForTarget;

        public TThis RecordFromTarget => _record_target
            .ActionOut(out _record_select)
            .FuncSelect(this.AsType<TThis>)
            .TypeFromTarget;

        public TThis RecordFromSecond => _record_second
            .ActionOut(out _record_select)
            .FuncSelect(this.AsType<TThis>)
            .TypeFromSecond;

        public TThis ClearRecordSelect => _record_select
            .FuncSelect(null)
            .ActionOut(out _record_select)
            .FuncSelect(this.AsType<TThis>)
            .ClearTypeSelect;

        public TThis ClearRecordTarget => _record_target
            .FuncSelect(null)
            .ActionOut(out _record_target)
            .FuncSelect(this.AsType<TThis>)
            .ClearTypeTarget;

        public TThis ClearRecordSecond => _record_second
            .FuncSelect(null)
            .ActionOut(out _record_second)
            .FuncSelect(this.AsType<TThis>)
            .ClearTypeSecond;

        public TThis RecordForSecond(object category_link = null, object type_link = null) => _record_select
            .ActionOut(out _record_second)
            .ActionWhere(second => second.NotNull() && RecordTarget.NotNull(), second =>
            {
                category_link
                    .NotNull()
                    .And(type_link.NotNull)
                    .IfTrue(() => type_link
                        .AsType<int>()
                        .ToGuid(category_link.AsType<int>())
                        .FuncSelect(guid => Types.Find(guid))
                        .ActionOut(out _type_link));

                var guid_type_link = TypeLink?.ID;

                Links
                    .FirstOrDefault(v
                        => (v.Target == RecordTarget.ID)
                           && (v.Second == second.ID)
                           && (v.Type == guid_type_link))
                    .FuncIfNull(() => typeof(TLink)
                        .CreateInstance<TLink>()
                        .Action(link =>
                        {
                            link.Target = RecordTarget.ID;
                            link.Second = RecordSecond.ID;
                            link.Type = guid_type_link;
                        })
                        .FuncSelect<TLink, TLink>(Add)
                        .Action(true, Commit));
            })
            .FuncSelect(this.AsType<TThis>)
            .TypeForSecond;

        public TThis FindRecord(
                Expression<Func<TRecord, bool>> expression)
            => TypeSelect.ActionIfNotNull(() =>
                {
                    Records
                        .Where(v => v.Type == TypeSelect.ID)
                        .Where(expression)
                        .FirstOrDefault()
                        .ActionOut(out _record_select);
                })
                .FuncSelect(this.AsType<TThis>);

        public TThis CreateRecord(
                Action<TRecord> action = null)
            => TypeSelect
                .ActionIfNotNull(type =>
                {
                    typeof(TRecord)
                        .CreateInstance<TRecord>()
                        .ActionIfNotNull(record => { record.Type = type.ID; })
                        .ActionIfNotNull(action)
                        .FuncIfNotNull<TRecord, TRecord>(Add)
                        .ActionOut(out _record_select)
                        .ActionIfNotNull(true, Commit);
                })
                .FuncSelect(this.AsType<TThis>);

        public TThis EditRecord(
                Action<TRecord> action)
            => RecordSelect
                .ActionIfNotNull(action)
                .FuncIfNotNull<TRecord, TRecord>(Update)
                .ActionOut(out _record_select)
                .FuncSelect(true, Commit);

        public TThis DeleteRecord() => RecordSelect
            .FuncSelect(false, Delete)
            .FuncSelect<TRecord>(null)
            .ActionOut(out _record_select)
            .FuncSelect(true, Commit);
    }
}