﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Finflaex.Support.Reflection;
using Finflaex.Support.UniversalDB.Tables;

#endregion

namespace Finflaex.Support.UniversalDB.Bases
{
    public partial class fxcUniversalDB<TThis, TType, TRecord, TLink, TValue>
        : DbContext
        where TThis : fxcUniversalDB<TThis, TType, TRecord, TLink, TValue>
        where TLink : fxcUDB_LinkTable
        where TValue : fxcUDB_ValueTable
        where TRecord : fxcUDB_RecordTable
        where TType : fxcUDB_TypeTable
    {
        public fxcUniversalDB(string connection_string = null)
            : base(connection_string)
        {
        }

        public Guid Author { get; set; }

        public DbSet<TLink> Links { get; set; }
        public DbSet<TValue> Values { get; set; }
        public DbSet<TRecord> Records { get; set; }
        public DbSet<TType> Types { get; set; }

        public TRec Add<TRec>(TRec record) where TRec : class
        {
            if (record == null) return null;
            Entry(record).State = EntityState.Added;
            return Set<TRec>().Add(record);
        }

        public TRec Attach<TRec>(TRec record) where TRec : class
        {
            return Set<TRec>().Attach(record);
        }

        public TThis BeginTracking()
        {
            Configuration.AutoDetectChangesEnabled = false;
            return this.AsType<TThis>();
        }

        public TThis Commit(bool worked = true)
        {
            if (worked) Loging();
            SaveChanges();
            return this.AsType<TThis>();
        }

        public TThis Delete<TRec>(TRec record, bool real = false)
            where TRec : class
        {
            fxcUDB_RecordTable version;

            record
                .AsType<fxcUDB_RecordTable>().ActionOut(out version)
                .NotNull().Or(real)
                .IfTrue(() =>
                {
                    Entry(record).State = EntityState.Deleted;
                    Set<TRec>().Remove(record);
                })
                .IfFalse(() =>
                {
                    version.EndDate = DateTimeOffset.Now;
                    Entry(record).State = EntityState.Modified;
                });

            return this.AsType<TThis>();
        }

        public TThis EndTracking()
        {
            ChangeTracker.DetectChanges();
            Configuration.AutoDetectChangesEnabled = true;
            return this.AsType<TThis>();
        }

        public virtual void InitializeCreate()
        {
        }

        public TThis Rollback()
        {
            ChangeTracker.Entries().ForEach(v => v.Reload());
            return this.AsType<TThis>();
        }

        public TRec Update<TRec>(TRec record) where TRec : class
        {
            record = Attach(record);
            Entry(record).State = EntityState.Modified;
            return record;
        }

        private void Loging()
        {
            ChangeTracker
                .Entries()
                .Where(v =>
                    (v.State == EntityState.Added)
                    || (v.State == EntityState.Modified)
                    || (v.State == EntityState.Deleted))
                .ForEach(entry =>
                {
                    var type = entry.Entity.GetType();

                    if (!(entry.Entity is fxcUDB_RecordTable)) return;

                    var now = DateTimeOffset.Now;
                    var max = DateTimeOffset.MaxValue;

                    var record = entry.Entity;
                    var clone = entry.State
                        .Equals(EntityState.Modified)
                        .SelectSingleIf(
                            () => Activator.CreateInstance(type),
                            () => null);

                    var original = record?.AsType<fxcUDB_RecordTable>();
                    var history = clone?.AsType<fxcUDB_RecordTable>();

                    var state = entry.State;

                    var IsModified = state.Equals(EntityState.Modified);

                    if (original.IsNull()) return;

                    (entry.State == EntityState.Deleted
                            ? entry.OriginalValues.PropertyNames
                            : entry.CurrentValues.PropertyNames)
                        .FuncSelect(read =>
                        {
                            var list = read.ToList();
                            var item = list.FirstOrDefault(nameof(fxcUDB_RecordTable.EndDate).Equals);

                            return item
                                .NotNull()
                                .SelectSingleIf(
                                    () => new List<string>().AddRet(item).AddRangeRet(list),
                                    () => read);
                        })
                        .ForEach(prop_name =>
                        {
                            var prop = entry.Property(prop_name);

                            switch (prop_name)
                            {
                                case nameof(fxcUDB_RecordTable.Original):
                                    IsModified.And(history.NotNull).IfTrue(() => { history.Original = original.ID; });
                                    return;
                                case nameof(fxcUDB_RecordTable.Author):
                                    IsModified.IfTrue(() => { original.Author = Author; });
                                    return;
                                case nameof(fxaUDB_KeyTable.ID):
                                    return;
                                case nameof(fxcUDB_RecordTable.StartDate):
                                    IsModified.And(history.NotNull)
                                        .IfTrue(() => { history.StartDate = original.StartDate; });
                                    prop.IsModified.IfTrue(() => { original.StartDate = now; });
                                    return;
                                case nameof(fxcUDB_RecordTable.EndDate):
                                    IsModified
                                        .IfTrue(() =>
                                        {
                                            prop.IsModified
                                                .And(original.EndDate <= now)
                                                .IfTrue(() => { clone = null; })
                                                .IfFalse(() => { history.EndDate = now; });
                                        })
                                        .IfFalse(() => { original.EndDate = max; });
                                    return;
                                default:
                                    IsModified.And(clone.NotNull)
                                        .IfTrue(
                                            () => { type.GetProperty(prop_name).SetValue(clone, prop.OriginalValue); });
                                    return;
                            }
                        });

                    if (clone != null) Set(type).Add(clone);
                });
        }
    }
}