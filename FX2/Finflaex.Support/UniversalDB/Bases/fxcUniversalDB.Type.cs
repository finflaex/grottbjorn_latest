﻿#region

using System;
using System.Linq;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.UniversalDB.Bases
{
    partial class fxcUniversalDB<TThis, TType, TRecord, TLink, TValue>
    {
        private TType _type_link;
        private TType _type_second;
        private TType _type_select;
        private TType _type_target;

        public TType TypeSelect => _type_select;
        public TType TypeTarget => _type_target;
        public TType TypeSecond => _type_second;
        public TType TypeLink => _type_link;

        public TThis TypeForTarget
            => _type_select
                .ActionOut(out _type_target)
                .ActionWhere(type => type.NotNull() && RecordTarget.NotNull(), type => { RecordTarget.Type = type.ID; })
                .FuncSelect(true, Commit);

        public TThis TypeForSecond
            => _type_select
                .ActionOut(out _type_select)
                .ActionWhere(type => type.NotNull() && RecordSecond.NotNull(), type => { RecordSecond.Type = type.ID; })
                .FuncSelect(true, Commit);

        public TThis TypeForLink
            => _type_select
                .ActionOut(out _type_select)
                .FuncSelect(this.AsType<TThis>);

        public TThis TypeFromTarget
            => _type_target
                .ActionOut(out _type_select)
                .ActionWhere(type => type.NotNull() && RecordSelect.NotNull(), type => { RecordSelect.Type = type.ID; })
                .FuncSelect(true, Commit);

        public TThis TypeFromSecond
            => _type_second
                .ActionOut(out _type_select)
                .ActionWhere(type => type.NotNull() && RecordSelect.NotNull(), type => { RecordSelect.Type = type.ID; })
                .FuncSelect(true, Commit);

        public TThis TypeFromLink
            => _type_link
                .ActionOut(out _type_select)
                .FuncSelect(this.AsType<TThis>);

        public TThis ClearTypeSelect
            => _type_select
                .FuncSelect(null)
                .ActionOut(out _type_select)
                .FuncSelect(this.AsType<TThis>);

        public TThis ClearTypeTarget
            => _type_target
                .FuncSelect(null)
                .ActionOut(out _type_target)
                .FuncSelect(this.AsType<TThis>);

        public TThis ClearTypeSecond
            => _type_second
                .FuncSelect(null)
                .ActionOut(out _type_second)
                .FuncSelect(this.AsType<TThis>);

        public TThis ClearTypeLink
            => _type_link
                .FuncSelect(null)
                .ActionOut(out _type_link)
                .FuncSelect(this.AsType<TThis>);

        public TThis FindType(
                Guid id)
            => Types
                .Find(id)
                .ActionOut(out _type_select)
                .FuncSelect(this.AsType<TThis>);

        public TThis FindType(
                object category,
                object id)
            => id
                .FuncSelect(read => read.AsType<int>().ToGuid(category.AsType<int>()))
                .FuncSelect(FindType);

        public TThis FindType(string key)
            => Types
                .FirstOrDefault(v => v.Key == key)
                .ActionOut(out _type_select)
                .FuncSelect(this.AsType<TThis>);

        public TThis CreateType(
                object category,
                object id,
                Action<TType> action = null)
            => id
                .AsType<int>()
                .ToGuid(category.AsType<int>())
                .FuncSelect(guid => Types.Find(guid))
                .FuncIfNull(() => typeof(TType)
                    .CreateInstance<TType>(id, category)
                    .ActionIfNotNull(type => { type.Parent = TypeSelect?.ID; })
                    .ActionIfNotNull(action)
                    .FuncIfNotNull<TType, TType>(Add)
                    .ActionIfNotNull(true, Commit))
                .ActionOut(out _type_select)
                .FuncSelect(this.AsType<TThis>);

        public TThis EditType(
                Action<TType> action)
            => TypeSelect
                .ActionIfNotNull(action)
                .FuncIfNotNull<TType, TType>(Update)
                .ActionOut(out _type_select)
                .FuncSelect(true, Commit);

        public TThis DeleteType()
            => TypeSelect
                .FuncSelect(false, Delete)
                .FuncSelect<TType>(null)
                .ActionOut(out _type_select)
                .FuncSelect(true, Commit);
    }
}