﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.UniversalDB.Bases
{
    partial class fxcUniversalDB<TThis, TType, TRecord, TLink, TValue>
    {
        public enum StackEnum
        {
        }

        public static LinkedList<StackItem> Stack = new LinkedList<StackItem>();

        public TThis SetItem<T>(StackEnum key, T value)
        {
            var has = Stack
                .FirstOrDefault(v => (v.Author == Author) && (v.Key == key))
                .ActionIfNotNull(v => v.Value = value)
                .ActionIfNull(() =>
                {
                    Stack.AddLast(new StackItem
                    {
                        Key = key,
                        Author = Author,
                        Value = value
                    });
                });
            return this.AsType<TThis>();
        }

        public TThis EditItem(StackEnum key, Action<StackItem> action)
        {
            Stack
                .FirstOrDefault(v => (v.Author == Author) && (v.Key == key))
                .ActionIfNotNull(action);
            return this.AsType<TThis>();
        }

        public TThis DeleteItem(StackEnum key)
        {
            Stack
                .FirstOrDefault(v => (v.Author == Author) && (v.Key == key))
                .ActionIfNotNull(v => Stack.Remove(v));
            return this.AsType<TThis>();
        }

        public class StackItem
        {
            public Guid Author { get; set; }
            public StackEnum Key { get; set; }
            public object Value { get; set; }
        }
    }
}