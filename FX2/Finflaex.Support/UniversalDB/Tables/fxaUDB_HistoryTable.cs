﻿#region

using System;

#endregion

namespace Finflaex.Support.UniversalDB.Tables
{
    public class fxaUDB_HistoryTable
        : fxaUDB_KeyTable
    {
        protected fxaUDB_HistoryTable()
        {
            StartDate = DateTimeOffset.Now;
            EndDate = DateTimeOffset.MaxValue;
            Original = null;
        }

        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public Guid? Original { get; set; }
    }
}