﻿#region

using System;
using System.ComponentModel.DataAnnotations.Schema;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.UniversalDB.Tables
{
    [Table("Types")]
    [System.Data.Linq.Mapping.Table]
    public class fxcUDB_TypeTable
        : fxaUDB_KeyTable
    {
        public fxcUDB_TypeTable()
        {
        }

        public fxcUDB_TypeTable(int big_id, int sub_id = 0)
            : base(big_id.ToGuid(sub_id))
        {
        }

        public Guid? Parent { get; set; }
        public string Key { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
    }
}