﻿#region

using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace Finflaex.Support.UniversalDB.Tables
{
    [Table("Values")]
    [System.Data.Linq.Mapping.Table]
    public class fxcUDB_ValueTable
        : fxaUDB_KeyTable
    {
        public byte[] RawValue { get; set; }
    }
}