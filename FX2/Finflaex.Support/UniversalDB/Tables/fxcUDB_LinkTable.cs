﻿#region

using System;
using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace Finflaex.Support.UniversalDB.Tables
{
    [Table("Links")]
    [System.Data.Linq.Mapping.Table]
    public class fxcUDB_LinkTable
        : fxaUDB_KeyTable
    {
        public Guid Target { get; set; }
        public Guid? Type { get; set; }
        public Guid Second { get; set; }
    }
}