﻿#region

using System;
using System.ComponentModel.DataAnnotations;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.UniversalDB.Tables
{
    public abstract class fxaUDB_KeyTable
    {
        public fxaUDB_KeyTable(Guid new_id)
        {
            ID = new_id;
        }

        public fxaUDB_KeyTable()
            : this(Guid.NewGuid())
        {
        }

        public fxaUDB_KeyTable(int big_id, int sub_id = 0, int level1 = 0, int level2 = 0, int level3 = 0)
            : this(big_id.ToGuid(sub_id, level1, level2, level3))
        {
        }

        [Key]
        public Guid ID { get; protected set; }

        public Guid Author { get; set; }
    }
}