﻿#region

using System;
using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace Finflaex.Support.UniversalDB.Tables
{
    [Table("Records")]
    [System.Data.Linq.Mapping.Table]
    public class fxcUDB_RecordTable
        : fxaUDB_HistoryTable
    {
        public Guid Type { get; set; }
        public string Key { get; set; }
        public Guid? Value { get; set; }
        public string StringValue { get; set; }
        public double NumValue { get; set; }
    }
}