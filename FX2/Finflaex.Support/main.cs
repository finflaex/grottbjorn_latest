﻿#region

using System;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Web.Hosting;
using System.Web.Mvc;
using Finflaex.Support.IO;
using Finflaex.Support.Log;
using Finflaex.Support.MVC;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Finflaex.Support.Service;
using Finflaex.Support.Types;
using Finflaex.Support.WinApi;

#endregion

namespace Finflaex.Support
{
    public class main
    {
        public static fxcDictSO Values = new fxcDictSO();

        public static string CurrentDirectory
        {
            get
            {
                var executablePath = "";
                var assembly1 = Assembly.GetEntryAssembly();
                if (assembly1 == null)
                {
                    var builder1 = new StringBuilder(260);
                    kernel32.GetModuleFileName(IntPtr.Zero, builder1, builder1.Capacity);
                    executablePath = Path.GetFullPath(builder1.ToString());
                }
                else
                {
                    var text1 = assembly1.EscapedCodeBase;
                    var uri1 = new Uri(text1);
                    executablePath = uri1.Scheme == "file" ? text1.ToPath().ToLocalPath() : uri1.ToString();
                }

                var uri2 = new Uri(executablePath);
                if (uri2.Scheme == "file")
                    new FileIOPermission(FileIOPermissionAccess.PathDiscovery, executablePath).Demand();
                return executablePath.ToPath().ToLocalPath().GetDirectory();
            }
        }

        public static CompositionContainer MEF
        {
            get { return Values.Get<CompositionContainer>("mef"); }
            set { Values["mef"] = value; }
        }

        public static string[] PluginDirs
        {
            get { return Values.Get<string[]>("mef_dir"); }
            set { Values["mef_dir"] = value; }
        }

        public static string ExecutablePath
        {
            get
            {
                var executablePath = "";
                var assembly1 = Assembly.GetEntryAssembly();
                if (assembly1 == null)
                {
                    var builder1 = new StringBuilder(260);
                    kernel32.GetModuleFileName(IntPtr.Zero, builder1, builder1.Capacity);
                    executablePath = Path.GetFullPath(builder1.ToString());
                }
                else
                {
                    var text1 = assembly1.EscapedCodeBase;
                    var uri1 = new Uri(text1);
                    executablePath = uri1.Scheme == "file" ? GetLocalPath(text1) : uri1.ToString();
                }

                var uri2 = new Uri(executablePath);
                if (uri2.Scheme == "file")
                    new FileIOPermission(FileIOPermissionAccess.PathDiscovery, executablePath).Demand();
                return executablePath;
            }
        }

        public static string ExecutableDirectory => ExecutablePath.GetDirectoryName();

        public static void DllMain()
        {
            Initialize();
        }

        public static void Initialize()
        {
            // Если сборка не загружена пошарим в ресурсах
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                var dll_name = args.Name.Split(',').First();

                return AppDomain.CurrentDomain.GetAssemblies()
                    .Select(a => a.GetResource(dll_name).ToAssembly())
                    .FirstOrDefault(v => v != null);
            };
        }

        public static string ServerPath(string path)
        {
            path = path.Replace("\\", "/");

            //return HttpContext.Current.Server.MapPath(path);
            return HostingEnvironment.MapPath(path);
        }

        public static string GetLocalPath(string fileName)
        {
            var uri1 = new Uri(fileName);
            return uri1.LocalPath + uri1.Fragment;
        }

        public static void MvcAsaxInitialize(params object[] param)
        {
            //RouteTable.Routes.MapRoute(
            //    name: "Plugin",
            //    url: "ext/{action}/{*param}",
            //    defaults: new { controller = "fxcDefaultController", action = "Index"});

            //RouteTable.Routes.MapRoute(
            //    name: "Result",
            //    url: "{controller}/{action}.{result}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = "", result = "view" });

            var ControllerFactory = new fxcControllerFactory();
            ControllerBuilder.Current.SetControllerFactory(ControllerFactory);

            //ViewEngines.Engines.Clear();
            //ViewEngines.Engines.Add(new fxcViewEngine("~/plugins"));

            GlobalFilters.Filters.Clear();
            GlobalFilters.Filters.Add(new RolesAttribute());
        }

        public static CompositionContainer LoadMef(params object[] param)
        {
            var catalog = new AggregateCatalog();
            var files = param
                .OfType<string>()
                .AddRange(PluginDirs ?? new string[0])
                .Add(ExecutableDirectory)
                .SelectMany(p => Directory.GetFiles(p, "*.dll"))
                .ToArray();
            var assemblies = param
                .OfType<Assembly>()
                .Add(Assembly.GetExecutingAssembly())
                .Add(Assembly.GetEntryAssembly())
                .ToArray();
            files
                .Select(p =>
                {
                    try
                    {
                        return Assembly.Load(File.ReadAllBytes(p));
                    }
                    catch (Exception error)
                    {
                        fxsLog.Write(
                            fxeLog.FinflaexError,
                            fxeLog.AdminError,
                            typeof(main),
                            MethodBase.GetCurrentMethod(),
                            error);
                        return null;
                    }
                })
                .AddRange(assemblies)
                .WhereNotNull()
                .Select(v => v.Instance<AssemblyCatalog>())
                .Action(catalog.Catalogs.AddRange);

            return MEF = catalog.Instance<CompositionContainer>();
        }

        public static void AsUser(string login, string domen, string password, Action action)
        {
            using (new fxcImpersonator(login, domen, password))
            {
                action?.Invoke();
            }
        }
    }
}