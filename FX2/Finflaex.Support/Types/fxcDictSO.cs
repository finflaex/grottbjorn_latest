﻿#region

using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Finflaex.Support.Reflection;
using Newtonsoft.Json.Linq;

#endregion

namespace Finflaex.Support.Types
{
    public class fxcDictSO : Dictionary<string, object>
    {
        public SqlParameter[] AsSqlParameters
            => this
                .Select(item => new SqlParameter(item.Key, item.Value))
                .ToArray();

        public new object this[string data]
        {
            get { return ContainsKey(data) ? base[data] : null; }
            set { base[data] = value; }
        }

        public R Get<R>(string s) => this[s].AsType<R>();
        public string GetString(string s) => this[s].AsType<string>();
        public int GetInt(string s) => this[s].AsType<int>();

        public bool GetBool(string s) => this[s].AsType<bool>();

        public T GetJObjectToObject<T>(string name)
        {
            if (ContainsKey(name))
            {
                return this[name].AsType<JObject>().ToObject<T>();
            }
            return default(T);
        }

        public fxcDictSO Set(string key, object value)
        {
            this[key] = value;
            return this;
        }
    }

    public class fxcDictSS : Dictionary<string, string>
    {
        public new string this[string data]
        {
            get { return ContainsKey(data) ? base[data] : null; }
            set { base[data] = value; }
        }
    }
}