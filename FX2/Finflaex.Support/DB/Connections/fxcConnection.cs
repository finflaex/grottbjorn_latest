﻿#region

using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Finflaex.Support.Log;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

#endregion

namespace Finflaex.Support.DB.Connections
{
    public class fxcConnection
    {
        private readonly SqlConnection _connection;

        public fxcConnection(string connection_string)
        {
            _connection = new SqlConnection(connection_string);
        }

        public bool Connected => _connection.State == ConnectionState.Open;

        public object ExecuteScalar(string sql, fxcDictSO fxcDictSo = null)
        {
            try
            {
                var cmd = new SqlCommand(sql, _connection);
                if ((fxcDictSo != null) && (fxcDictSo.Count > 0))
                    cmd.Parameters.AddRange(fxcDictSo.AsSqlParameters);
                return cmd.ExecuteScalar();
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    GetType(),
                    MethodBase.GetCurrentMethod(),
                    error);
                return null;
            }
        }

        public fxcConnection Open()
        {
            if ((_connection != null) && (_connection.State != ConnectionState.Open))
                try
                {
                    _connection.Open();
                }
                catch (Exception error)
                {
                    fxsLog.Write(
                        fxeLog.FinflaexError,
                        GetType(),
                        MethodBase.GetCurrentMethod(),
                        error,
                        new fxcDictSO
                        {
                            ["ConnectionString"] = _connection.ConnectionString,
                            ["CurrentDirrectory"] = main.CurrentDirectory
                        },
                        -123456789);
                }
            return this.AsType<fxcConnection>();
        }

        public fxcConnection Close()
        {
            if ((_connection != null) && (_connection.State != ConnectionState.Closed))
                try
                {
                    _connection.Close();
                }
                catch (Exception error)
                {
                    fxsLog.Write(
                        fxeLog.FinflaexError,
                        GetType(),
                        MethodBase.GetCurrentMethod(),
                        error);
                }
            return this.AsType<fxcConnection>();
        }

        public void ExecuteNonQuery(string sql, fxcDictSO param = null)
        {
            try
            {
                var cmd = new SqlCommand(sql, _connection);
                if ((param != null) && (param.Count > 0))
                    cmd.Parameters.AddRange(param.AsSqlParameters);
                cmd.ExecuteNonQuery();
            }
            catch (Exception error)
            {
                fxsLog.Write(
                    fxeLog.FinflaexError,
                    GetType(),
                    MethodBase.GetCurrentMethod(),
                    error,
                    param
                );
            }
        }

        public static void Connect(string connString, Action<fxcConnection> action = null)
        {
            var connect = new fxcConnection(connString);
            connect.Open();
            if (connect.Connected)
                action?.Invoke(connect);
            connect.Close();
        }

        public static R Connect<R>(string connString, Func<fxcConnection, R> action = null)
        {
            var result = default(R);
            var connect = new fxcConnection(connString);
            connect.Open();
            if (connect.Connected && (action != null))
                result = action.Invoke(connect);
            connect.Close();
            return result;
        }

        public void ExecuteReader(Action<SqlDataReader> action, params object[] param)
        {
            var sql = param.OfType<string>().Join(" ");
            var vals = param.OfType<fxcDictSO>().FirstOrDefault();
            var cmd = new SqlCommand(sql, _connection);
            if ((vals != null) && (vals.Count > 0))
                cmd.Parameters.AddRange(vals.AsSqlParameters);
            using (var reader = cmd.ExecuteReader())
            {
                action?.Invoke(reader);
            }
        }

        public R ExecuteReader<R>(Func<SqlDataReader, R> action, params object[] param)
            where R : class
        {
            var sql = param.OfType<string>().Join(" ");
            var vals = param.OfType<fxcDictSO>().FirstOrDefault();
            var cmd = new SqlCommand(sql, _connection);
            if ((vals != null) && (vals.Count > 0))
                cmd.Parameters.AddRange(vals.AsSqlParameters);
            using (var reader = cmd.ExecuteReader())
            {
                return action?.Invoke(reader);
            }
        }

        public object[][] ExecuteReader(params object[] param)
        {
            var sql = param.OfType<string>().Join(" ");
            var vals = param.OfType<fxcDictSO>().FirstOrDefault();
            var cmd = new SqlCommand(sql, _connection);
            if ((vals != null) && (vals.Count > 0))
                cmd.Parameters.AddRange(vals.AsSqlParameters);
            return cmd.ExecuteReader().ReadValues();
        }
    }
}