﻿#region

using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Finflaex.Support.DB.Bases.Interfaces;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.DB.Repositories
{
    public abstract class fxaSingle_Repository<TThis, TDB, TRec>
        where TThis : fxaSingle_Repository<TThis, TDB, TRec>
        where TDB : DbContext, fxiBaseDbContext<TDB>
        where TRec : fxaKeyTable
    {
        protected TRec select;

        public fxaSingle_Repository(TDB db)
        {
            DB = db;
        }

        public TDB DB { get; }
        public TRec Select => select;
        public DbSet<TRec> Set => DB?.Set<TRec>();
        public IQueryable<TRec> Query => Set?.AsType<IQueryable<TRec>>();

        public virtual TThis Create(Action<TRec> action)
        {
            Activator
                .CreateInstance<TRec>()
                .Action(action)
                .FuncSelect(DB.Add)
                .ActionOut(out select);
            DB.Commit();
            return this.AsType<TThis>();
        }

        public virtual TThis CreateIfNull(Action<TRec> action)
        {
            return select.IsNull()
                ? Create(action)
                : this.AsType<TThis>();
        }

        public virtual TThis Delete()
        {
            Select.ActionIfNotNull(record =>
            {
                DB.Delete(record).Commit();
                select = null;
            });
            return this.AsType<TThis>();
        }

        public virtual TThis Edit(Action<TRec> action)
        {
            Select
                .ActionIfNotNull(action)
                .ActionIfNotNull(record =>
                {
                    DB.Update(record);
                    DB.Commit();
                });
            return this.AsType<TThis>();
        }

        public virtual TThis Find(Expression<Func<TRec, bool>> expression)
        {
            Query
                .FirstOrDefault(expression)
                .ActionOut(out select);
            return this.AsType<TThis>();
        }

        public virtual TThis Clear()
        {
            select = null;
            return this.AsType<TThis>();
        }
    }
}