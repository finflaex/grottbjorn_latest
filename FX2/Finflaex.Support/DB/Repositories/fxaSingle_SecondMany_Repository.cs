﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Finflaex.Support.DB.Bases.Interfaces;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.DB.Repositories
{
    public abstract class fxaSingle_SecondMany_Repository<TThis, TRoot, TDB, TRec, TSecond, TLog, TLink>
        : fxaSingle_Repository<TThis, TDB, TSecond>
        where TThis : fxaSingle_SecondMany_Repository<TThis, TRoot, TDB, TRec, TSecond, TLog, TLink>
        where TRoot : fxaSingle_Repository<TRoot, TDB, TRec>
        where TDB : DbContext, fxiDbContext<TDB, TLog, TLink>
        where TRec : fxaKeyTable
        where TSecond : fxaKeyTable, fxiSecondTable
        where TLog : fxcLog
        where TLink : fxcLink
    {
        protected List<TSecond> select_list;

        public fxaSingle_SecondMany_Repository(TRoot root_repo)
            : base(root_repo?.DB)
        {
            RootRepository = root_repo;
        }

        public TRoot RootRepository { get; }
        public List<TSecond> SelectList => select_list ?? new List<TSecond>();
        public DbSet<TRec> RootSet => RootRepository?.Set;
        public IQueryable<TRec> RootQuery => RootRepository?.Query;
        public TRec RootSelect => RootRepository?.Select;
        public Guid RootID => RootSelect?.ID ?? Guid.Empty;

        public int Count => Set.Count(v => v.Root == RootID);
        public int CountSelect => SelectList.Count;

        public override TThis Create(Action<TSecond> action)
        {
            RootSelect.ActionIfNotNull(root =>
            {
                base.Create(second =>
                {
                    second.Root = RootSelect.ID;
                    action(second);
                });
            });
            return this.AsType<TThis>();
        }

        public override TThis Find(Expression<Func<TSecond, bool>> expression = null)
        {
            Query
                .Where(v => v.Root == RootID)
                .Where(expression)
                .ToList()
                .ActionOut(out select_list)
                .FirstOrDefault()
                .ActionOut(out select);
            return this.AsType<TThis>();
        }

        public virtual TThis ClearList()
        {
            SelectList.Clear();
            return this.AsType<TThis>();
        }

        public virtual TThis DeleteList()
        {
            Set.RemoveRange(SelectList);
            DB.Commit();
            return this.AsType<TThis>();
        }
    }
}