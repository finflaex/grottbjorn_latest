﻿#region

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.DB.Tables.Federals
{
    public class OKOPF
    {
        [Key]
        public int ID { get; set; }

        public string Code { get; set; }
        public string Caption { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }

        [NotMapped]
        public static OKOPF[] List => Assembly
            .GetExecutingAssembly()
            .GetResource($"{nameof(OKOPF)}.csv")
            .Instance<MemoryStream>()
            .ReadLines(Encoding.GetEncoding(1251))
            .Select(line => line.Split('>'))
            .Select(arr => new OKOPF
            {
                ID = arr[0].ToInt(),
                Code = arr[1],
                Caption = arr[2].Trim('"').FirstUp(),
                StartDate = arr[4].ToDateTime("dd.MM.yyyy", DateTime.Now),
                EndDate = arr[5].ToDateTime("dd.MM.yyyy", DateTime.MaxValue),
                Description = arr[10].Trim('"').FirstUp()
            })
            .ToArray();
    }
}