﻿#region

using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.DB.Tables.Federals
{
    public class CityPhoneCode
    {
        public string City { get; set; }
        public string PhoneCode { get; set; }

        [NotMapped]
        public static CityPhoneCode[] List => "http://phone.rin.ru/html/Russia.html"
            .GetFormRequest(Encoding.GetEncoding(1251))
            .DoRegexMatches(@"<tr><td>(.*)<\/td><td>&nbsp;(.*)\s<br><\/td><\/tr>", RegexOptions.IgnoreCase)
            .Select(match => new CityPhoneCode
            {
                PhoneCode = match.Groups[2].Value,
                City = match.Groups[1].Value
            })
            .ToArray();
    }
}