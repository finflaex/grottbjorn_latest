﻿#region

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.DB.Tables.Federals
{
    public class CountryPhoneCode
    {
        [Key]
        public int ID { get; set; }

        public string TitleRU { get; set; }
        public string TitleEN { get; set; }
        public string PhoneCode { get; set; }

        [NotMapped]
        public static string SQL => Assembly
            .GetExecutingAssembly()
            .GetResource($"{nameof(CountryPhoneCode)}.sql")
            .ToString(Encoding.GetEncoding(1251));

        [NotMapped]
        public static CountryPhoneCode[] List => SQL
            .DoRegexMatches(@"(\d*), '(.*)', '(.*)', '(.*)'")
            .Select(match => new CountryPhoneCode
            {
                ID = match.Groups[1].Value.ToInt(),
                TitleRU = match.Groups[2].Value,
                TitleEN = match.Groups[3].Value,
                PhoneCode = match.Groups[4].Value
            })
            .ToArray();
    }
}