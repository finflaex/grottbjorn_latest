﻿#region

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.DB.Tables.Federals
{
    public class OKVED2
    {
        [Key]
        public int ID { get; set; }

        public string Section { get; set; }
        public string Position { get; set; }
        public string Code { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public DateTime StarDate { get; set; }
        public DateTime EndDate { get; set; }


        [NotMapped]
        public static OKVED2[] List => Assembly
            .GetExecutingAssembly()
            .GetResource($"{nameof(OKVED2)}.csv")
            .Instance<MemoryStream>()
            .ReadLines(Encoding.GetEncoding(1251))
            .Select(line => line.Split('>'))
            .Where(arr => arr[3].Length > 5)
            .Select(arr => new OKVED2
            {
                ID = arr[0].ToInt(),
                Section = arr[1],
                Position = arr[2],
                Code = arr[3],
                Caption = arr[4].Trim('"').FirstUp(),
                Description = $"{arr[5]}{arr[6]}".Trim('"').FirstUp(),
                StarDate = arr[7].ToDateTime("dd.MM.yyyy"),
                EndDate = arr[8].ToDateTime("dd.MM.yyyy", DateTime.MaxValue)
            })
            .ToArray();
    }
}