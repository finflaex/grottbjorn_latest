﻿#region

using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.DB.Tables.Federals
{
    public class BuhStringCode
    {
        public string Caption { get; set; }
        public string StringCode { get; set; }

        [NotMapped]
        public static BuhStringCode[] List => Assembly
            .GetExecutingAssembly()
            .GetResource($"{nameof(BuhStringCode)}.csv")
            .Instance<MemoryStream>()
            .ReadLines(Encoding.GetEncoding(1251))
            .Select(line => line.Split(';'))
            .Select(arr => new BuhStringCode
            {
                Caption = arr[0],
                StringCode = arr[1]
            })
            .ToArray();
    }
}