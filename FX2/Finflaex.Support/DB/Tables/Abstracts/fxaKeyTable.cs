﻿#region

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Finflaex.Support.DB.Tables.Interfaces;

#endregion

namespace Finflaex.Support.DB.Tables.Abstracts
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public abstract class fxaKeyTable : fxiKeyTable
    {
        protected fxaKeyTable(Guid new_id)
        {
            ID = new_id;
        }

        protected fxaKeyTable()
            : this(Guid.NewGuid())
        {
        }

        [Key]
        [Column(Order = 0)]
        public Guid ID { get; set; }

        public void ReWriteID(Guid new_id) => ID = new_id;
    }
}