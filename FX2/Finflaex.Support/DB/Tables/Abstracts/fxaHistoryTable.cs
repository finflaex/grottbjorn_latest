﻿#region

using System;
using Finflaex.Support.DB.Tables.Interfaces;

#endregion

namespace Finflaex.Support.DB.Tables.Abstracts
{
    public class fxaHistoryTable : fxaKeyTable, fxiHistoryTable
    {
        public fxaHistoryTable(
            Guid? target,
            DateTimeOffset? start_date = null,
            DateTimeOffset? end_date = null)
        {
            verTarget = target;
            verStartDate = start_date ?? DateTimeOffset.Now;
            verEndDate = end_date ?? DateTimeOffset.MaxValue;
        }

        public fxaHistoryTable()
            : this(null)
        {
            verStartDate = DateTimeOffset.Now;
        }

        public DateTimeOffset verStartDate { get; set; }
        public DateTimeOffset verEndDate { get; set; }
        public Guid? verTarget { get; set; }
    }
}