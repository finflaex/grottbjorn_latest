﻿#region

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

#endregion

namespace Finflaex.Support.DB.Tables
{
    [Table("SysLog")]
    public class fxcLog : fxaKeyTable, fxiAuthorTable
    {
        public DateTimeOffset When { get; set; }
        public string Type { get; set; }
        public Guid Target { get; set; }
        public string Property { get; set; }
        public byte[] OriginalValue { get; set; }
        public byte[] ModifyValue { get; set; }
        public EntityState State { get; set; }
        public Guid Author { get; set; }
    }
}