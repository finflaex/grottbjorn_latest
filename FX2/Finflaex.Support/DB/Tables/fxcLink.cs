﻿#region

using System;
using System.ComponentModel.DataAnnotations.Schema;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

#endregion

namespace Finflaex.Support.DB.Tables
{
    [Table("SysLink")]
    public class fxcLink : fxaHistoryTable, fxiCommentTable
    {
        public Guid Root { get; set; }
        public Guid Second { get; set; }
        public Guid Type { get; set; }
        public string TypeRoot { get; set; }
        public string TypeSecond { get; set; }
        public string Comment { get; set; }
    }
}