﻿#region

using System;

#endregion

namespace Finflaex.Support.DB.Tables.Interfaces
{
    public interface fxiTypedTable : fxiKeyTable
    {
        Guid Typed { get; set; }
    }
}