﻿namespace Finflaex.Support.DB.Tables.Interfaces
{
    public interface fxiDescriptionTable : fxiKeyTable
    {
        string Description { get; set; }
    }
}