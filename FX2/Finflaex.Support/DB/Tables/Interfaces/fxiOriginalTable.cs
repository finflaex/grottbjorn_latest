﻿#region

using System;

#endregion

namespace Finflaex.Support.DB.Tables.Interfaces
{
    public interface fxiOriginalTable
    {
        Guid? Original { get; set; }
    }
}