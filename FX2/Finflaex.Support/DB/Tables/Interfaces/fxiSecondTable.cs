﻿#region

using System;

#endregion

namespace Finflaex.Support.DB.Tables.Interfaces
{
    public interface fxiSecondTable : fxiKeyTable
    {
        Guid Root { get; set; }
    }
}