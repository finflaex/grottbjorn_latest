﻿#region

using System;

#endregion

namespace Finflaex.Support.DB.Tables.Interfaces
{
    public interface fxiAuthorTable
        : fxiKeyTable
    {
        Guid Author { get; set; }
    }
}