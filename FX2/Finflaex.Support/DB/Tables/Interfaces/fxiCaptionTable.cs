﻿namespace Finflaex.Support.DB.Tables.Interfaces
{
    public interface fxiCaptionTable
        : fxiKeyTable
    {
        string Caption { get; set; }
    }
}