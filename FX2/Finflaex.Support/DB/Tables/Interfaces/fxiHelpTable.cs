﻿namespace Finflaex.Support.DB.Tables.Interfaces
{
    public interface fxiHelpTable<TKey, TValue>
    {
        TKey Key { get; set; }
        TValue Value { get; set; }
    }
}