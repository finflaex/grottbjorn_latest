﻿#region

using System;

#endregion

namespace Finflaex.Support.DB.Tables.Interfaces
{
    public interface fxiHistoryTable
        : fxiKeyTable
    {
        DateTimeOffset verStartDate { get; set; }
        DateTimeOffset verEndDate { get; set; }
        Guid? verTarget { get; set; }
    }
}