﻿#region

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace Finflaex.Support.DB.Tables.Interfaces
{
    public interface fxiKeyTable
    {
        [Key]
        Guid ID { get; }
    }
}