﻿namespace Finflaex.Support.DB.Tables.Interfaces
{
    public interface fxiCommentTable : fxiKeyTable
    {
        string Comment { get; set; }
    }
}