﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using Finflaex.Support.DB.Attributes;
using Finflaex.Support.DB.Bases.Interfaces;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Atributes;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.DB.Bases.Abstracts
{
    public abstract class fxaDbContext<TThis, TLog, TLink>
        : DbContext
            , fxiDbContext<TThis, TLog, TLink>
        where TThis : fxaDbContext<TThis, TLog, TLink>
        where TLog : fxcLog
        where TLink : fxcLink
    {
        public fxaDbContext(string connectionString)
            : base(connectionString)
        {
            Author = Guid.Empty;
            FlagWriteLog = false;
        }

        public bool FlagWriteLog { get; set; }

        public Guid Author { get; set; }

        public DbSet<TLink> SysLinks { get; set; }

        public DbSet<TLog> SysLogs { get; set; }

        public TRec Add<TRec>(TRec record) where TRec : class
        {
            if (record == null) return null;
            Entry(record).State = EntityState.Added;
            return Set<TRec>().Add(record);
        }

        public TRec Attach<TRec>(TRec record) where TRec : class
        {
            if (record == null) return null;
            return Set<TRec>().Attach(record);
        }

        public TThis BeginTracking()
        {
            Configuration.AutoDetectChangesEnabled = false;
            return this.AsType<TThis>();
        }

        public TThis Commit(bool worked = true)
        {
            if (worked) Loging();
            SaveChanges();
            return this.AsType<TThis>();
        }

        public TThis Delete<TRec>(TRec record, bool real = false)
            where TRec : class
        {
            if (record != null)
            {
                fxiHistoryTable version;

                record
                    .AsType<fxiHistoryTable>().ActionOut(out version)
                    .IsNull().Or(real)
                    .IfTrue(() =>
                    {
                        Entry(record).State = EntityState.Deleted;
                        Set<TRec>().Remove(record);
                    })
                    .IfFalse(() =>
                    {
                        version.verEndDate = DateTimeOffset.Now;
                        Entry(record).State = EntityState.Modified;
                    });
            }

            return this.AsType<TThis>();
        }

        public TThis EndTracking()
        {
            ChangeTracker.DetectChanges();
            Configuration.AutoDetectChangesEnabled = true;
            return this.AsType<TThis>();
        }

        public virtual void InitializeCreate()
        {
        }

        public TThis Rollback()
        {
            ChangeTracker.Entries().ForEach(v => v.Reload());
            return this.AsType<TThis>();
        }

        public TRec Update<TRec>(TRec record) where TRec : class
        {
            record = Attach(record);
            Entry(record).State = EntityState.Modified;
            return record;
        }

        public TThis WriteLink(fxiKeyTable root, fxiKeyTable second)
        {
            var link = Activator.CreateInstance<TLink>();
            link.Root = root.ID;
            link.Second = second.ID;
            link.TypeRoot = root.GetType().Name;
            link.TypeSecond = second.GetType().Name;
            Set<TLink>().Add(link);
            return this.AsType<TThis>();
        }

        public TThis WriteLog(DbEntityEntry target, DbPropertyEntry property, bool delete = false)
        {
            var hasLog = target.Entity
                .GetType()
                .GetCustomAttributes()
                .Select(v => v.AsType<NotDbLogAttribute>())
                .All(v => v == null);

            //.GetCustomAttribute(typeof (NotDbLogAttribute), true)
            //.AsType<NotDbLogAttribute>() == null;

            var isModify = new[] {EntityState.Added, EntityState.Deleted}
                               .Contains(target.State)
                           || (target.State.Equals(EntityState.Modified)
                               && (property.OriginalValue == property.CurrentValue));

            if (FlagWriteLog && hasLog && isModify)
            {
                var log = Activator.CreateInstance<TLog>();
                log.Author = Author;
                log.When = DateTimeOffset.Now;
                log.Type = target.Entity.GetType().Name;
                log.Target = target.Entity.AsType<fxiKeyTable>().ID;
                log.Property = property.Name;
                log.ModifyValue = (target.State != EntityState.Deleted) && !delete
                    ? property.CurrentValue?.AsBytes()
                    : null;
                log.OriginalValue = target.State != EntityState.Added
                    ? property.OriginalValue?.AsBytes()
                    : null;
                log.State = delete
                    ? EntityState.Deleted
                    : target.State;
                Set<TLog>().Add(log);
            }
            return this.AsType<TThis>();
        }

        private void Loging()
        {
            ChangeTracker
                .Entries()
                .Where(v =>
                    (v.State == EntityState.Added)
                    || (v.State == EntityState.Modified)
                    || (v.State == EntityState.Deleted))
                .ForEach(entry =>
                {
                    var type = entry.Entity.GetType();
                    var now = DateTimeOffset.Now;
                    var max = DateTimeOffset.MaxValue;

                    //var currentGuid = entry.Entity.AsType<fxiKeyTable>();
                    var currentHistory = entry.Entity.AsType<fxiHistoryTable>();
                    var currentAuthor = entry.Entity.AsType<fxiAuthorTable>();

                    var remove = false;

                    var clone
                        = (entry.State == EntityState.Modified)
                          && (currentHistory != null)
                            ? Activator.CreateInstance(type)
                            : null;

                    //var cloneGuid = clone.AsType<fxiKeyTable>();
                    var cloneHistory = clone.AsType<fxiHistoryTable>();
                    //var cloneAuthor = clone.AsType<fxiAuthorTable>();

                    var modified = false;

                    (entry.State == EntityState.Deleted
                            ? entry.OriginalValues.PropertyNames
                            : entry.CurrentValues.PropertyNames)
                        .FuncSelect(read =>
                        {
                            var list = read.ToList();
                            var item = list.FirstOrDefault(nameof(fxiHistoryTable.verEndDate).Equals);

                            return item
                                .NotNull()
                                .SelectSingleIf(
                                    () => new List<string>()
                                        .AddRet(item)
                                        .AddRangeRet(list),
                                    () => read);
                        })
                        .ForEach(prop_name =>
                        {
                            var prop = entry.Property(prop_name);

                            nameof(fxiHistoryTable.verTarget)
                                .Equals(prop_name)
                                .And(entry.State.Equals(EntityState.Modified))
                                .IfTrue(() => { cloneHistory.verTarget = currentHistory.ID; });

                            switch (prop_name)
                            {
                                case nameof(fxiHistoryTable.verTarget):
                                    entry.State
                                        .Equals(EntityState.Modified)
                                        .And(currentHistory.NotNull)
                                        .IfTrue(() => { cloneHistory.verTarget = currentHistory.ID; });
                                    return;
                                case nameof(fxiAuthorTable.Author):
                                    entry.State
                                        .Equals(EntityState.Modified)
                                        .And(currentAuthor.NotNull)
                                        .IfTrue(() => { currentAuthor.Author = Author; });
                                    return;
                                case nameof(fxiKeyTable.ID):
                                    return;
                                case nameof(fxiHistoryTable.verStartDate):
                                    cloneHistory
                                        .NotNull()
                                        .IfTrue(() =>
                                        {
                                            if (currentHistory.verStartDate > now)
                                            {
                                                clone = null;
                                            }
                                            else
                                            {
                                                cloneHistory.verStartDate = currentHistory.verStartDate;
                                                currentHistory.verStartDate = now;
                                            }
                                        });

                                    //entry.State
                                    //    .Equals(EntityState.Modified)
                                    //    .And(currentHistory.NotNull)
                                    //    .IfTrue(() => { cloneHistory.verStartDate = currentHistory.verStartDate; });
                                    //prop.IsModified
                                    //    .And(currentHistory.NotNull)
                                    //    .IfFalse(() => { currentHistory.verStartDate = now; });

                                    return;
                                case nameof(fxiHistoryTable.verEndDate):
                                    cloneHistory
                                        .NotNull()
                                        .IfTrue(() =>
                                        {
                                            if (currentHistory.verEndDate <= now)
                                            {
                                                clone = null;
                                                remove = true;
                                            }
                                            else
                                                cloneHistory.verEndDate = now;
                                        });

                                    //currentHistory.NotNull().IfTrue(() =>
                                    //{
                                    //    entry.State.Equals(EntityState.Modified).IfTrue(() =>
                                    //    {
                                    //        prop
                                    //            .IsModified
                                    //            .And(currentHistory.verEndDate <= now)
                                    //            .IfTrue(() => { clone = null; })
                                    //            .IfFalse(() => { cloneHistory.verEndDate = now; });
                                    //    });
                                    //    prop.IsModified.IfFalse(() => { currentHistory.verEndDate = max; });
                                    //});

                                    return;
                                default:
                                    Attribute
                                        .IsDefined(type.GetProperty(prop_name), typeof(DoNotHistory), true)
                                        .IfFalse(() =>
                                        {
                                            modified |= (entry.State != EntityState.Added)
                                                        && (entry.State != EntityState.Deleted)
                                                        &&
                                                        (prop.CurrentValue?.ToString() != prop.OriginalValue?.ToString());
                                        });

                                    entry.State
                                        .Equals(EntityState.Modified)
                                        .And(clone.NotNull)
                                        .IfTrue(() =>
                                        {
                                            type
                                                .GetProperty(prop_name)
                                                .SetValue(clone, prop.OriginalValue);
                                        });

                                    entry.State.Equals(EntityState.Added).IfTrue(() =>
                                    {
                                        WriteLog(prop, prop_name, entry, type, EntityState.Added);
                                    });

                                    entry.State.Equals(EntityState.Modified).IfTrue(() =>
                                    {
                                        WriteLog(prop, prop_name, entry, type, remove ? EntityState.Deleted : EntityState.Modified);
                                    });

                                    entry.State.Equals(EntityState.Deleted).IfTrue(() =>
                                    {
                                        WriteLog(prop, prop_name, entry, type, EntityState.Deleted);
                                    });


                                    var delete = entry.State
                                        .Equals(EntityState.Modified)
                                        .And(clone.IsNull);

                                    new[] {EntityState.Added, EntityState.Deleted}
                                        .Contains(entry.State)
                                        .Or(delete)
                                        .OrNot(entry.State.Equals(EntityState.Modified) &&
                                               (prop.CurrentValue == prop.OriginalValue))
                                        .IfTrue(() => { WriteLog(entry, prop, delete); });
                                    return;
                            }
                        });

                    if (modified && (clone != null)) Set(type).Add(clone);
                });
        }

        private void WriteLog(DbPropertyEntry prop, string prop_name, DbEntityEntry entry, Type type, EntityState state)
        {
            if (entry.Entity.AsType<fxiKeyTable>().IsNull())
            {
                return;
            }

            if (state == EntityState.Added
                && prop.CurrentValue == null)
            {
                return;
            }

            var log = typeof(TLog).CreateInstance<fxcLog>();

            log.Author = Author;
            log.ModifyValue = prop.CurrentValue.AsBytes();
            log.OriginalValue = state == EntityState.Modified ? prop.OriginalValue.AsBytes() : new byte[0];
            log.Property = prop_name;
            log.State = state;
            log.Target = entry.Entity.AsType<fxiKeyTable>().ID;
            log.Type = type.Name.Split('_')[0];
            log.When = DateTimeOffset.Now;

            Set(typeof(TLog)).Add(log);
        }
    }
}