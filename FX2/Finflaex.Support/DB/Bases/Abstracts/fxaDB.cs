﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Bases.Interfaces;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.Reflection;

namespace Finflaex.Support.DB.Bases.Abstracts
{
    public abstract class fxaDB<TThis>
        : DbContext, fxiDbContext<TThis> 
        where TThis : fxaDB<TThis>
    {
        public virtual Guid AuthorGuid { get; set; }

        public DateTimeOffset Now { get; set; }

        public fxaDB(string connectionString)
        {
            AuthorGuid = Guid.Empty;
            Now = DateTimeOffset.Now;
        }

        public virtual TRec Add<TRec>(TRec record) where TRec : class
        {
            if (record == null) return null;
            Entry(record).State = EntityState.Added;
            return Set<TRec>().Add(record);
        }

        public virtual TRec Attach<TRec>(TRec record) where TRec : class
        {
            if (record == null) return null;
            return Set<TRec>().Attach(record);
        }

        public virtual TThis BeginTracking()
        {
            Configuration.AutoDetectChangesEnabled = false;
            return this.AsType<TThis>();
        }

        public virtual TThis Commit()
        {
            SaveChanges();
            return this.AsType<TThis>();
        }

        public virtual TThis Delete<TRec>(TRec record, bool real = false)
            where TRec : class
        {
            if (record != null)
            {
                fxiHistoryTable version;

                record
                    .AsType<fxiHistoryTable>().ActionOut(out version)
                    .IsNull().Or(real)
                    .IfTrue(() =>
                    {
                        Entry(record).State = EntityState.Deleted;
                        Set<TRec>().Remove(record);
                    })
                    .IfFalse(() =>
                    {
                        version.verEndDate = DateTimeOffset.Now;
                        Entry(record).State = EntityState.Modified;
                    });
            }

            return this.AsType<TThis>();
        }

        public virtual TThis EndTracking()
        {
            ChangeTracker.DetectChanges();
            Configuration.AutoDetectChangesEnabled = true;
            return this.AsType<TThis>();
        }

        public virtual TThis Rollback()
        {
            ChangeTracker.Entries().ForEach(v => v.Reload());
            return this.AsType<TThis>();
        }

        public virtual TRec Update<TRec>(TRec record) where TRec : class
        {
            record = Attach(record);
            Entry(record).State = EntityState.Modified;
            return record;
        }
    }
}
