﻿using System;
using System.Data.Entity;

namespace Finflaex.Support.DB.Bases.Abstracts
{
    public interface fxiDbContext<TThis> where TThis : DbContext, fxiDbContext<TThis>
    {
        Guid AuthorGuid { get; set; }
        DateTimeOffset Now { get; set; }

        TRec Add<TRec>(TRec record) where TRec : class;
        TRec Attach<TRec>(TRec record) where TRec : class;
        TThis BeginTracking();
        TThis Commit();
        TThis Delete<TRec>(TRec record, bool real = false) where TRec : class;
        TThis EndTracking();
        TThis Rollback();
        TRec Update<TRec>(TRec record) where TRec : class;
    }
}