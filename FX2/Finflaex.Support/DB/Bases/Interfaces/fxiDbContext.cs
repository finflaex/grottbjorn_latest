﻿#region

using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Interfaces;

#endregion

namespace Finflaex.Support.DB.Bases.Interfaces
{
    public interface fxiDbContext<TThis, TLog, TLink>
        : fxiBaseDbContext<TThis>
        where TThis : DbContext, fxiDbContext<TThis, TLog, TLink>
        where TLog : fxcLog
        where TLink : fxcLink
    {
        DbSet<TLink> SysLinks { get; set; }
        DbSet<TLog> SysLogs { get; set; }

        TThis WriteLink(fxiKeyTable root, fxiKeyTable second);
        TThis WriteLog(DbEntityEntry target, DbPropertyEntry property, bool delete = false);
    }
}