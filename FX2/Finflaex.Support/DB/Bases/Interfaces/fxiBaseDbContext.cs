﻿#region

using System;
using System.Data.Entity;

#endregion

namespace Finflaex.Support.DB.Bases.Interfaces
{
    public interface fxiBaseDbContext<TThis> : IDisposable
        where TThis : DbContext, fxiBaseDbContext<TThis>
    {
        Guid Author { get; set; }

        TThis Commit(bool worked = true);
        TThis Rollback();


        TRec Attach<TRec>(TRec record) where TRec : class;
        TRec Add<TRec>(TRec record) where TRec : class;
        TRec Update<TRec>(TRec record) where TRec : class;
        TThis Delete<TRec>(TRec record, bool real = false) where TRec : class;

        TThis BeginTracking();
        TThis EndTracking();

        void InitializeCreate();
    }
}