﻿#region

using System;
using System.IO;
using System.Web;

#endregion

namespace Finflaex.Support.IO
{
    public class fcPath
    {
        private string path;

        public fcPath(string path)
        {
            this.path = path;
        }

        public string Ext => Path.GetExtension(path)?.Substring(1);

        public static implicit operator string(fcPath value) => value.ToString();
        public static implicit operator fcPath(string value) => new fcPath(value);

        public override string ToString() => path;

        public string ToLocalPath()
        {
            var uri1 = new Uri(path);
            return uri1.LocalPath + uri1.Fragment;
        }

        public string ToServerPath()
        {
            path = path.Replace("\\", "/");
            return HttpContext.Current.Server.MapPath(path);
        }
    }

    public static class frPath
    {
        public static fcPath ToPath(this string @this)
            => new fcPath(@this);
    }
}