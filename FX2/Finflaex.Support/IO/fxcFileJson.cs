﻿#region

using System.IO;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.IO
{
    public class fxcFileJson : fxcFile<fxcFileJson>
    {
        public fxcFileJson(string path, string file)
            : base(path, file, FileMode.Create)
        {
        }

        public T Load<T>() => ReadAllText().FromJson<T>();
        public void Save<T>(T value) => Write(value.ToJson());
    }
}