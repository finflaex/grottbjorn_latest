﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.IO
{
    public class fcDirectory
    {
        private string _mask;

        private string _path;

        public fcDirectory(params object[] param)
        {
            _path = param.OfType<string>().FirstOrDefault() ?? finflaex.ExecutableDirectory;
            _mask = param.OfType<string>().Skip(1).FirstOrDefault() ?? "*.*";
            var filters = param.OfType<NotifyFilters>();

            Watcher = new FileSystemWatcher
            {
                Path = _path,
                Filter = _mask,
                IncludeSubdirectories = true,
                //EnableRaisingEvents = true,
            };

            if (filters.Any())
                Watcher.NotifyFilter = filters.Aggregate((s, v) => s |= v);

            Watcher.Changed += WatcherOnChanged;
            Watcher.Created += WatcherOnChanged;
            Watcher.Deleted += WatcherOnChanged;

            RunWatcher = true;
        }

        public string Path
        {
            get { return _path; }
            set
            {
                Watcher.Path = _path = value;
                Create();
            }
        }

        public string Mask
        {
            get { return _mask; }
            set { Watcher.Filter = _mask = value; }
        }

        public bool Exists => Directory.Exists(Path);
        public FileSystemWatcher Watcher { get; set; }

        public bool RunWatcher
        {
            get { return Watcher.EnableRaisingEvents; }
            set { Watcher.EnableRaisingEvents = value; }
        }

        public static implicit operator string(fcDirectory value) => value.ToString();
        public static implicit operator fcDirectory(string value) => new fcDirectory(value);

        private void WatcherOnChanged(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            sender.AsType<FileSystemWatcher>().EnableRaisingEvents = false;
            OnWatcher?.Invoke(fileSystemEventArgs);
            OnFullWatcher?.Invoke(this, fileSystemEventArgs);
            sender.AsType<FileSystemWatcher>().EnableRaisingEvents = true;
        }

        public event Action<FileSystemEventArgs> OnWatcher;
        public event Action<fcDirectory, FileSystemEventArgs> OnFullWatcher;

        public fcDirectory Create()
        {
            if (!Exists) Directory.CreateDirectory(Path);
            return this;
        }

        public override string ToString() => _path;
    }

    public static class frDirectory
    {
        public static fcDirectory ToDirectory(this string @this, params object[] param)
        {
            var data = new List<object> {@this};
            data.AddRange(param);
            return new fcDirectory(data.ToArray());
        }
    }
}