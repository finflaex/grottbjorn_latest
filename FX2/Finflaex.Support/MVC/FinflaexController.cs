﻿#region

using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.MVC
{
    public class FinflaexController<TThis> : Controller
        where TThis : FinflaexController<TThis>, IController
    {
        public fxaAuth Author => HttpContext.GetFinflaexAuth<fxaAuth>() ?? new fxaAuth();

        public virtual void InitializeDB()
        {
            ;
        }

        public string ActionPath(string nameAction) => typeof(TThis)
                                                           .FullName
                                                           .Split('.')
                                                           .SkipWhile(v => v != "Areas")
                                                           .Skip(1)
                                                           .FirstOrDefault()
                                                           .FuncSelect(area
                                                               => typeof(TThis)
                                                                   .Name
                                                                   .RemoveEnd("Controller")
                                                                   .FuncSelect(controller => area != null
                                                                       ? $"/{area}/{controller}/"
                                                                       : $"/{controller}/")) + nameAction;

        public string JsonObject(object value)
        {
            var shablon = "(\"/\\*<finflaex>)(.*?)(</finflaex>\\*/\")";
            var result = value.ToJson();
            Regex.Replace(result, shablon, "$2").ActionOut(out result);
            Regex.Replace(result, "(\\\\r\\\\n|\\\\r|\\\\n)", string.Empty).ActionOut(out result);
            Regex.Replace(result, "(constructor)", "$init").ActionOut(out result);
            return result;
        }

        protected static string function(string script)
            => $"/*<finflaex>{script}</finflaex>*/";
    }
}