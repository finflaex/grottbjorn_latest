﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.JSON;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;
using Microsoft.AspNet.SignalR;

#endregion

namespace Finflaex.Support.MVC
{
    public abstract class fxaWebSocket<T> : PersistentConnection
        where T : fxaWebSocket<T>
    {
        public static readonly Dictionary<string, T> stack
            = new Dictionary<string, T>();

        public string СonnectionId { get; set; }

        protected override Task OnConnected(
            IRequest request,
            string connectionId)
        {
            СonnectionId = connectionId;
            stack.Add(connectionId, this.AsType<T>());
            return Task.CompletedTask;
        }


        protected override Task OnDisconnected(IRequest request, string connectionId, bool stopCalled)
        {
            stack.Remove(connectionId);
            return Task.CompletedTask;
        }

        public void Send(object data)
        {
            Connection.Send(СonnectionId, data.ToJsonObject());
        }

        //public void Load(object id, string url)
        //    => Send(new fxaWsJsonObject
        //    {
        //        command = "webix",
        //        data = url,
        //        method = "Load",
        //        target = id.ToString()
        //    });

        //public void Eval(object data)
        //    => Send(new fxaWsJsonObject
        //    {
        //        command = "eval",
        //        data = data
        //    });

        //public void SendAction(string path, object data = null)
        //{
        //    if (data == null)
        //    {
        //        Eval($"ws.Send({{uri:'{path}'}});");
        //    }
        //    else
        //    {
        //        data = data.ToJson().Replace('\"', '\'');
        //        Eval($"ws.Send({{uri:'{path}', data:{data}}});");
        //    }
        //}

        //public void Message(string message)
        //    => Send(new fxaWsJsonObject
        //    {
        //        command = "webix",
        //        data = message,
        //        method = "Message"
        //    });

        //public void SetValues(object id, object data)
        //    => Send(new fxaWsJsonObject
        //    {
        //        command = "webix",
        //        target = id.ToString(),
        //        data = data,
        //        method = "SetValues"
        //    });

        //public void Error(string message)
        //    => Send(new fxaWsJsonObject
        //    {
        //        command = "webix",
        //        data = message,
        //        method = "Error"
        //    });

        //public void WSReplace(object id, object new_obj)
        //    => Send(new fxaWsJsonObject
        //    {
        //        command = "webix",
        //        method = "Replace",
        //        target = id.ToString(),
        //        data = new_obj
        //    });

        //public void Hide(object id)
        //    => Send(new fxaWsJsonObject
        //    {
        //        command = "webix",
        //        method = "Hide",
        //        target = id.ToString()
        //    });

        //public void Show(object id)
        //    => Send(new fxaWsJsonObject
        //    {
        //        command = "webix",
        //        method = "Show",
        //        target = id.ToString()
        //    });

        //public void WSAppend(object id, object new_obj)
        //    => Send(new fxaWsJsonObject
        //    {
        //        command = "webix",
        //        method = "Append",
        //        target = id.ToString(),
        //        data = new_obj
        //    });

        //public fxcDictSO text(
        //        string name,
        //        string label = null)
        //    => new fxcDictSO()
        //        .Set("view", "text")
        //        .Set("name", name)
        //        .Set("label", label);

        //public fxcDictSO password(
        //        string name,
        //        string label = null)
        //    => text(name, label)
        //        .Set("type", "password");

        //public void SetCookie(
        //    string key,
        //    string value,
        //    DateTime? experiaes = null)
        //{
        //    var result = new fxaWsJsonObject
        //    {
        //        command = "setCookie",
        //        name = key,
        //        data = value
        //    };

        //    if (experiaes != null)
        //        result
        //            .target = experiaes
        //            .AsType<DateTime>()
        //            .ToJSMilliseconds()
        //            .ToString();

        //    Send(result);
        //}

        //public void Clear(object id)
        //    => Send(new fxaWsJsonObject
        //    {
        //        command = "webix",
        //        method = "ClearData",
        //        target = id
        //    });

        //public void Parse(object id, object data)
        //    => Send(new fxaWsJsonObject
        //    {
        //        target = id,
        //        command = "webix",
        //        method = "ParseData",
        //        data = data
        //    });

        //public void AddItem(object id, object data)
        //    => Send(new fxaWsJsonObject
        //    {
        //        target = id.ToString(),
        //        command = "webix",
        //        method = "AddItem",
        //        data = data
        //    });

        //public void UpdateItem(object id, object data)
        //    => Send(new fxaWsJsonObject
        //    {
        //        target = id.ToString(),
        //        command = "webix",
        //        method = "UpdateData",
        //        data = new[]
        //        {
        //            data
        //        }
        //    });

        //public void Log(string message)
        //{
        //    AddItem("log", new {message});
        //}

        //public void Log(ref Guid? id, string message)
        //{
        //    if (id == null)
        //        id = Guid.NewGuid();
        //    UpdateItem("log",
        //        new
        //        {
        //            id = id.Value.ToString("N"),
        //            message
        //        });
        //}

        //public void DennyAccess()
        //{
        //    Error("Доступ запрещен");
        //}

        //public void Context(object target, string callback, params object[] data)
        //{
        //    Send(new fxaWsJsonObject
        //    {
        //        data = data,
        //        command = "webix",
        //        method = "Context",
        //        target = target.ToString(),
        //        callback = (@"function(id){
        //            var context = this.getContext();
        //            var view = context.obj;
        //            var item = view.getItem(context.id);
        //            " + callback + @"
        //            }")
        //            .AsJsonFunction()
        //    });
        //}

        //public bool Access(params string[] roles)
        //    => Author
        //        .Roles
        //        .Intersect(roles)
        //        .Any()
        //        .Or(Author.is_god)
        //        .IfFalse(DennyAccess);
    }

    public static class fxrIRequest
    {
        public static T GetCookie<T>(
                this IRequest @this,
                string name)
            where T : class
            => @this
                .Cookies[name]
                .Value?
                .Replace(' ', '+')
                .FromBase64()
                .Decrypt()
                .ToString(Encoding.UTF8)
                .FromJson<T>();

    }
}