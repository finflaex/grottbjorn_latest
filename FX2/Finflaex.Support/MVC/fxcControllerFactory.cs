﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using Finflaex.Support.Plugin;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.MVC
{
    public class fxcControllerFactory : IControllerFactory
    {
        public IController CreateController(RequestContext requestContext, string controllerName)
        {
            var type = BuildManager
                .GetReferencedAssemblies()
                .Cast<Assembly>()
                .SelectMany(a =>
                {
                    try
                    {
                        return a.GetTypes();
                    }
                    // ReSharper disable once UnusedVariable
                    catch
                    {
                        // ignored
                    }
                    return new Type[0];
                })
                .Where(v => v.IsSubclassOf(typeof(Controller)))
                .AddRange(OtherType?.Invoke() ?? new List<Type>())
                .AddRange(fxsFPlugin.List<IController>())
                .FirstOrDefault(v =>
                {
                    return v.Name.EqualICIC(controllerName)
                           || ((v.Name.Length > 10)
                               && v.Name.RemoveEnd("Controller").EqualICIC(controllerName));
                });

            return (IController) DependencyResolver
                .Current
                .GetService(type ?? typeof(DefaultController));
        }

        public SessionStateBehavior GetControllerSessionBehavior(RequestContext requestContext, string controllerName)
        {
            return SessionStateBehavior.Default;
        }

        public void ReleaseController(IController controller)
        {
            var disposable = controller as IDisposable;
            disposable?.Dispose();
        }

        public static void Initialize()
        {
            ControllerBuilder.Current.SetControllerFactory(new fxcControllerFactory());
        }

        public static event Func<IEnumerable<Type>> OtherType;
        public static event Func<IActionInvoker> DefaultType;

        protected class DefaultController : Controller
        {
            public DefaultController()
            {
                ActionInvoker = DefaultType?.Invoke();
            }
        }
    }
}