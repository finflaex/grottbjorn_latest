﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Security;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

#endregion

namespace Finflaex.Support.MVC
{
    public abstract class fxaSignalRHub<T> : Hub
        where T : fxaSignalRHub<T>
    {
        private class HubMethodData
        {
            public Guid ID { get; set; }
            public string Key { get; set; }
            public Action<T, object> Method { get; set; }
        }

        public Guid? ID
        {
            get
            {
                Cookie cookie;
                if (Context.Request.Cookies.TryGetValue(FormsAuthentication.FormsCookieName, out cookie))
                {
                    return cookie
                        .Value
                        .Replace(' ','+')
                        .FromBase64()
                        .Decrypt()
                        .ToString(Encoding.UTF8)
                        .FromJson<fxaAuth>()?.Author;
                }
                return null;
            }
        }

        private readonly static fxcSignalRMap<Guid> _connections = new fxcSignalRMap<Guid>();

        private static Dictionary<Guid?, Action<object>> Functions = new Dictionary<Guid?, Action<object>>();

        protected static readonly Dictionary<Guid, Action<T>> ConnectList = new Dictionary<Guid, Action<T>>();

        private static readonly List<HubMethodData> Methods = new List<HubMethodData>();

        public static string Script => typeof(T)
            .Assembly
            .ListResources()
            .Where(v => v.name.Contains(typeof(T).Namespace))
            .Where(v => v.name.EndsWith(".js"))
            .FuncSelect(list =>
            {
                var result = new StringBuilder()
                    .AppendLine($"var {typeof(T).Name} = $.connection.{typeof(T).Name.FirstOnlyDown()};");
                list.ForEach(script =>
                {
                    var name = script.name.RemoveEnd(".js").Split('.').Last();
                    var text = script.value.ToString(Encoding.UTF8);
                    result.AppendLine($"{typeof(T).Name}.client.{name} = {text}");
                });
                return result.ToString();
            });

        public override Task OnConnected()
        {
            if (ID != null)
            {
                _connections.Add(ID.Value, Context.ConnectionId);
                if (ConnectList.ContainsKey(ID.Value))
                {
                    ConnectList[ID.Value](this.AsType<T>());
                }
            }
            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            if (ID != null)
            {
                if (!_connections.GetConnections(ID.Value).Contains(Context.ConnectionId))
                {
                    _connections.Add(ID.Value, Context.ConnectionId);
                }
                _connections.Add(ID.Value, Context.ConnectionId);
            }
            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            if (ID != null)
            {
                _connections.Remove(ID.Value, Context.ConnectionId);
            }
            return base.OnDisconnected(stopCalled);
        }

        public static void Connect(Guid? id, Action<T> act)
        {
            if (id == null) return;
            if (ConnectList.ContainsKey(id.Value)) ConnectList.Remove(id.Value);
            ConnectList.Add(id.Value, act);
        }

        public static void run(Guid? id, Action<dynamic> act)
        {
            if (id != null)
            {
                foreach (var connection in _connections.GetConnections(id.Value))
                {
                    var context = GlobalHost
                        .ConnectionManager
                        .GetHubContext<T>();
                    context
                        .Clients
                        .User(connection)
                        .Action(act);
                }
            }
        }


        public T AddMethod(string key, Action<T, object> act)
        {
            if (ID != null)
            {
                var method = Methods.FirstOrDefault(v => v.ID == ID.Value && v.Key == key);
                if (method != null) Methods.Remove(method);
                method = new HubMethodData
                {
                    ID = ID.Value,
                    Key = key,
                    Method = act
                };
                Methods.Add(method);
            }
            return this.AsType<T>();
        }

        public class ActionRead
        {
            public Guid? id { get; set; }
            public object data { get; set; }
        }

        public void Request(ActionRead read)
        {
            if (read?.id == null) return;
            var guid = read.id.Value;
            if (Functions.ContainsKey(guid))
            {
                Functions[guid](read.data);
            }
        }

        public class MethodRead
        {
            public string key { get; set; }
            public object data { get; set; }
        }

        public void Method(MethodRead read)
        {
            var method = Methods.FirstOrDefault(v => v.ID == ID && v.Key == read.key);
            method?.Method?.Invoke(this.AsType<T>(), read.data);
        }

        public static void AddAction(Guid id, Action<object> act)
        {
            if (!Functions.ContainsKey(id))
            {
                Functions.Add(id, act);
            }
        }
    }
}