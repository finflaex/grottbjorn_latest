﻿namespace Finflaex.Support.MVC
{
    public class fxaWsJsonObject
    {
        public string area;
        public string callback;
        public string command;
        public object data;
        public string method;
        public string name;
        public string session;
        public object target;
        public string uri;
        public string user;
    }
}