﻿#region

using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Finflaex.Support.MVC.Interfaces;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.MVC.Atributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class RolesAttribute : AuthorizeAttribute, fxiRoles
    {
        public static string UnauthRedirect = "Home/Unreg";
        public static string LoginRedirect = "Home/Login";
        public static bool UnauthError = false;

        private AuthorizationContext _currentContext;

        public RolesAttribute(
            bool anonymous = false,
            bool registr = true,
            params string[] roles)
        {
            Anonymous = anonymous;
            Registration = registr;
            Roles = roles;
        }

        public RolesAttribute(
            params string[] roles)
            : this(false, true, roles)
        {
        }

        public bool Anonymous { get; }

        public new string[] Roles { get; }
        public bool Registration { get; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            _currentContext = filterContext;

            var areaName = filterContext.RouteData.Values["area"].AsType<string>();
            var controllerName = filterContext.RouteData.Values["controller"].AsType<string>();
            var actionName = filterContext.RouteData.Values["action"].AsType<string>();

            

            if (UnauthError)
            {
                if (!AuthorizeCore(filterContext.HttpContext))
                    filterContext.Result = new HttpUnauthorizedResult();
            }
            else if (UnauthRedirect != null)
            {
                var paths = UnauthRedirect.Split('/');

                var finflaex = filterContext.HttpContext.VerifyFinflaexAuth(this);

                finflaex.IfFalse(() =>
                {
                    if (paths.Length == 2)
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                        {
                            {"area", null},
                            {"controller", paths[0]},
                            {"action", paths[1]},
                            {"path", filterContext.HttpContext.Request.RawUrl}
                        });
                    }
                    else if(paths.Length == 3)
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                        {
                            {"area", paths[0]},
                            {"controller", paths[1]},
                            {"action", paths[2]},
                            {"path", filterContext.HttpContext.Request.RawUrl}
                        });
                    }
                });
            }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.VerifyFinflaexAuth(this);
        }
    }
}