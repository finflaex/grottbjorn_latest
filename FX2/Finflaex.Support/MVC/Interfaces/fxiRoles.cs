﻿namespace Finflaex.Support.MVC.Interfaces
{
    public interface fxiRoles
    {
        bool Anonymous { get; }
        bool Registration { get; }
        string[] Roles { get; }
    }
}