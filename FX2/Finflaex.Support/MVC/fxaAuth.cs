﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Finflaex.Support.MVC.Interfaces;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.MVC
{
    [Serializable]
    public class fxaAuth
    {
        public bool is_god = false;

        public fxaAuth()
        {
            Roles = new List<string>();
            Author = Guid.Empty;
            Session = Guid.Empty;
            Time = DateTime.Now;
        }

        public Guid Author { get; set; }
        public Guid Session { get; set; }
        public DateTime Time { get; set; }

        public string Login { get; set; }
        public string Caption { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public bool Registered => Author != Guid.Empty;
        public bool IsGod => is_god || Author == 1.ToGuid() || (Roles?.Contains("admin") ?? false);

        public bool Verificate(params string[] roles)
        {
            return IsGod || (Registered && Roles.Intersect(roles).Any());
        }

        public bool Verificate(fxiRoles roles)
        {
            if (IsGod) return true;

            if (Registered)
            {
                return roles.Roles.Any() 
                    ? roles.Roles.Intersect(Roles).Any() 
                    : roles.Registration;
            }
            return roles.Anonymous;
        }
    }
}