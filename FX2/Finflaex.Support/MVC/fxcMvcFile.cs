﻿using System.IO;
using Finflaex.Support.Reflection;

namespace Finflaex.Support.MVC
{
    public class fxcMvcFile
    {
        public int Size { get; set; }
        public string FullName { get; set; }
        public string Name => FullName.GetFileNameWithoutExtension();
        public string Ext => FullName.GetFileExtension();

        private Stream _stream = null;

        public Stream Stream
        {
            get { return _stream; }
            set
            {
                _stream = value;
                _raw = null;
            }
        }

        private byte[] _raw = null;
        public byte[] Raw => _raw ?? (_raw = Stream?.ReadBytes());
    }
}
