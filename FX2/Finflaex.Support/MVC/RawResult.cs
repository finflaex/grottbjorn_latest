﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Finflaex.Support.Reflection;

namespace Finflaex.Support.MVC
{
    public class RawResult: ActionResult
    {
        public RawResult(Stream imageStream, string contentType)
        {
            if (imageStream == null)
                throw new ArgumentNullException(nameof(imageStream));
            if (contentType == null)
                throw new ArgumentNullException(nameof(contentType));

            ImageStream = imageStream;
            ContentType = contentType;
        }

        public Stream ImageStream { get; }
        public string ContentType { get; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            var response = context.HttpContext.Response;

            response.ContentType = ContentType;

            var buffer = new byte[4096];
            while (true)
            {
                int read = ImageStream.Read(buffer, offset: 0, count: buffer.Length);
                if (read == 0)
                    break;

                response.OutputStream.Write(buffer, offset: 0, count: read);
            }
            response.End();
        }
    }

    public static class RawHelper
    {
        public static RawResult ToRawResult(this Stream @this, string contentType)
            => @this.Instance<RawResult>(contentType);

        public static RawResult ToRawResult(this byte[] @this, string contentType)
            => @this.Instance<MemoryStream>().ToRawResult(contentType);

        public static RawResult ToRawResult(this string @this, string contentType, Encoding encode = null)
            => @this.ToBytes(encode ?? Encoding.UTF8).ToRawResult(contentType);

        public static RawResult ToTextResult(this string @this, Encoding encode = null)
            => @this.ToBytes(encode ?? Encoding.UTF8).ToRawResult("text/html");
    }
}
