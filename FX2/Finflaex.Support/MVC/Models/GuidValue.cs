﻿using System;

namespace Finflaex.Support.MVC.Models
{
    public class GuidValue
    {
        public Guid Guid { get; set; }
        public string Value { get; set; }
    }
}