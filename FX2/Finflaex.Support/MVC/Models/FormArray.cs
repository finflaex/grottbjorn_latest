﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Support.MVC.Models
{
    public class FormArray
    {
        public string name { get; set; }
        public object value { get; set; }
    }

    public class FormArrayResult : Collection<FormArray>
    {
        public object this[string key]
        {
            get { return this.Items.Where(v => v.name == key).Select(v => v.value).FirstOrDefault(); }
        }
    }
}
