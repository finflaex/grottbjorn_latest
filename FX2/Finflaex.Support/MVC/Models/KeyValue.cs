﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Support.MVC.Models
{
    public class KeyValue
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
