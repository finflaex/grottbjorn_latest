﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Support.MVC.Models
{
    public class MainValue
    {
        public MainValue()
        {
            id = Guid.Empty;
            target = Guid.Empty;
        }
        public Guid id { get; set; }
        public Guid target { get; set; }
        public string value { get; set; }
        public int index { get; set; }
        public IEnumerable<Guid> guids { get; set; }
    }
}
