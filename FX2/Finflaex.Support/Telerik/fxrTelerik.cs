﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Support.Telerik
{
    public static class fxrTelerik
    {
        public static string jsKendoTemplate(this object id, string nameOfValue = "data") => $@"kendo.template($('\\#{id}').html())({nameOfValue})";
    }
}
