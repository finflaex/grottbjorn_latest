﻿using Finflaex.Support.ApiOnline.Yandex.Models;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

namespace Finflaex.Support.ApiOnline.Yandex
{
    public class fxsYandex
    {
        private static string key = "178f83b2-172e-4a6f-a85e-1091ccc91496";

        private static fxcDictSO UrlList = new fxcDictSO()
        {
            ["yandex"] = "https://search-maps.yandex.ru/v1/"
        };

        public static fxcYandex Query(
            string value,
            string language = "ru")
            => value == null
                ? null
                : UrlList
                    .GetString("yandex")
                    .Append($"?lang={language}")
                    .Append($"&text={value}")
                    .Append($"&apikey={key}")
                    .GetFormRequest()
                    .FromJson<fxcYandex>();
    }
}
