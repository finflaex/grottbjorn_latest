namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// CompanyMetaData.Features
    /// </summary>
    public class Feature1
    {
        /// <summary>
        /// ������������� ������, ������� ����� ������������� �����������.
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// ��� �������� ���� value (values). ��������� ��������:
        /// "bool" � ������ ��������;
        /// "enum" � ������;
        /// "text" � ������������ �����.
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// �������� ������.
        /// </summary>
        public string name { get; set; }
        public Value[] values { get; set; }
        public object value { get; set; }
    }
}