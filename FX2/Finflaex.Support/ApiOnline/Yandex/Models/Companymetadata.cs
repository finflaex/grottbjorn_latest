namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// CompanyMetaData
    /// </summary>
    public class Companymetadata
    {
        /// <summary>
        /// ������������� �����������.
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// �������� �����������.
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// ��������� ���� � �������� �����������. �������������� �� �����, ������� ������������� �������.
        /// ������ ���� ������������ �� ���� ����� ��������, ��������� �� ���� �����.������ ����� � ��� ������� ������� ������� ��������������� �����, � ������ � ������� ���������� �������.
        /// </summary>
        public int[][] nameHighlight { get; set; }
        /// <summary>
        /// ����� �����������.
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// ��������� ���� � ������ �����������. �������������� ����� ��� ����� ����, ������� ��������� � ������� �������.
        /// ������ ���� ������������ �� ���� ����� ��������, ��������� �� ���� �����.������ ����� � ��� ������� ������� ������� ��������������� �����, � ������ � ������� ���������� �������.
        /// </summary>
        public int[][] addressHighlight { get; set; }
        /// <summary>
        /// ���� �����������.
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// �������� ������.
        /// </summary>
        public string postalCode { get; set; }
        /// <summary>
        /// ������ ���������, � ������� ������ ����������� (��������, ����� �������, ����� ��� �������).
        /// </summary>
        public Category[] Categories { get; set; }
        /// <summary>
        /// ������ ���������� ������� ����������� � ������ ���������� ����������.
        /// </summary>
        public Phone[] Phones { get; set; }
        /// <summary>
        /// ����� ������ �����������.
        /// </summary>
        public Hours Hours { get; set; }
        /// <summary>
        /// �������� �������������� ������ �����������.
        /// </summary>
        public Geo Geo { get; set; }
        /// <summary>
        /// ��������� ����, ������� ���������� �������� ��� ����������� �����������.
        /// </summary>
        public string Advert { get; set; }
        /// <summary>
        /// ������, ������� ������������� ����������� (��������, ������ �����������).
        /// </summary>
        public Feature1[] Features { get; set; }
        /// <summary>
        /// ������ �����������, ����������� �������.
        /// </summary>
        public Snippet Snippet { get; set; }
        /// <summary>
        /// ������ �� ��������-�������, ���������� �����-���� ���������� �� ����������� (��������, ������ �� ������ � ���������� �����).
        /// </summary>
        public Link[] Links { get; set; }
        /// <summary>
        /// ���������� �� �������(��������, ����� ��� ����), ������������ �������� ������������ ���������� ������.������ ���� ������������, ����� ������������ ���������� �� ����������.
        /// </summary>
        public string Distance { get; set; }
    }
}