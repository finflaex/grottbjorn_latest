namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    public class Geometry
    {
        public string type { get; set; }
        public float[] coordinates { get; set; }
    }
}