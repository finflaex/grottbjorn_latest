namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// properties
    /// </summary>
    public class Properties
    {
        /// <summary>
        /// ���������� � �����������, ��������������� �������� �� ������������.
        /// </summary>
        public Attribution Attribution { get; set; }
        /// <summary>
        /// ����������, ����������� ������ � �����.
        /// </summary>
        public Responsemetadata ResponseMetaData { get; set; }
    }
}