namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// features
    /// </summary>
    public class Feature
    {
        /// <summary>
        /// ������ ����� �������� Feature.
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// ���������� � ��������� �������.
        /// </summary>
        public Properties1 properties { get; set; }
        /// <summary>
        /// �������� ��������� ���������� �������.
        /// </summary>
        public Geometry geometry { get; set; }
    }
}