namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// CompanyMetaData.Hours
    /// </summary>
    public class Hours
    {
        /// <summary>
        /// �������� ������.
        /// </summary>
        public Availability[] Availabilities { get; set; }
        /// <summary>
        /// �������� ������ ������ � ���� ������������� ������.
        /// </summary>
        public string text { get; set; }
        /// <summary>
        /// ������� ����.
        /// </summary>
        public int tzOffset { get; set; }
    }
}