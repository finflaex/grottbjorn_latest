namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// CompanyMetaData.Links
    /// </summary>
    public class Link
    {
        /// <summary>
        /// ��� ��������-�������, ����������� �����-���� ���������� �� �����������. ��������� ��������:
        /// "social" � ���������� ����;
        /// "attribution" � ���� ���������� �������� �� �����������;
        /// "showtimes" � �����.
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// �������� ��������-�������.
        /// </summary>
        public string aref { get; set; }
        /// <summary>
        /// ������ �� ��������-������.
        /// </summary>
        public string href { get; set; }
    }
}