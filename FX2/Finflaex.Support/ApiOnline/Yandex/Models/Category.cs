namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// CompanyMetaData.Categories
    /// </summary>
    public class Category
    {
        /// <summary>
        /// ����� ���������.
        /// </summary>
        public string _class { get; set; }
        /// <summary>
        /// �������� ���������.
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// ��������� ���� � �������� ���������. �������������� �� �����, ������� ������������� �������.
        /// ������ ���� ������������ �� ���� ����� ��������, ��������� �� ���� �����.������ ����� � ��� ������� ������� ������� ��������������� �����, � ������ � ������� ���������� �������.
        /// </summary>
        public int[][] nameHighlight { get; set; }
    }
}