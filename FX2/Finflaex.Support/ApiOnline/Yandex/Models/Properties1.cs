namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// features.properties
    /// </summary>
    public class Properties1
    {
        /// <summary>
        /// �������� �������� �� ��������� �����������: �����, ���������� ����������, ����� ������, ��� ������������ � ��.
        /// </summary>
        public Companymetadata CompanyMetaData { get; set; }
        /// <summary>
        /// �����, ������� ������������� ��������� � �������� ������������ ��� ����������� ��������� �����������.
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// �����, ������� ������������� ��������� � �������� ��������� ��� ����������� ��������� �����������.
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// ������� �������, � ������� ������ �����������. �������� ���������� ������ ������� � ������� �������� ����� �������. ���������� ������� � ������������������ ��������, ������.
        /// </summary>
        public float[][] boundedBy { get; set; }
        /// <summary>
        /// �������� ����������, ���������������� �������� � ������ �����������.
        /// </summary>
        public string[] attributions { get; set; }
    }
}