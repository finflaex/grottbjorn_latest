namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// ResponseMetaData.SearchRequest
    /// </summary>
    public class Searchrequest
    {
        /// <summary>
        /// ������ �������.
        /// </summary>
        public string request { get; set; }
        /// <summary>
        /// ������������ ���������� ������������ �����������.
        /// </summary>
        public int results { get; set; }
        /// <summary>
        /// ���������� ������������ �����������.
        /// </summary>
        public int skip { get; set; }
        /// <summary>
        /// ������� �������, � ������� ���������������� ��������� ������� �������.
        /// ������� �������� � ���� ���������(� ������������������ �������, �������) ������ �������� � ������� ������� ����� �������.
        /// ������� ������� ������������ �������� �������������.
        /// </summary>
        public float[][] boundedBy { get; set; }
    }
}