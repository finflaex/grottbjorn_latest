namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// SearchResponse.Sort
    /// </summary>
    public class Sort
    {
        /// <summary>
        /// ��� ���������� ��������� �����������. ��������� ��������:
        /// "distance" � �� ���������� �� ��������������� �������.����� ���������� ������������, ���� � ������� ��� ������ ����������� ������ �����.���� �������� �� ���� ����� ������ �� ���������� ������, �� ���������� ������, �������� ������� � ����.
        /// "rank" � �� ������������� ������.
        /// </summary>
        public string type { get; set; }
    }
}