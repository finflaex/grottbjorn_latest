﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// Верхний уровень
    /// </summary>
    public class fxcYandex
    {
        /// <summary>
        /// Всегда имеет значение FeatureCollection.
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// Контейнер метаданных, описывающих запрос и ответ.
        /// </summary>
        public Properties properties { get; set; }
        /// <summary>
        /// Контейнер результатов поиска.
        /// </summary>
        public Feature[] features { get; set; }
    }
}