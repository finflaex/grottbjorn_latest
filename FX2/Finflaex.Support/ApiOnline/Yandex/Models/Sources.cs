namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    public class Sources
    {
        public Source twitter { get; set; }
        public Source vkontakte { get; set; }
        public Source yandex { get; set; }
        public Source facebook { get; set; }
    }
}