namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// CompanyMetaData.Phones
    /// </summary>
    public class Phone
    {
        /// <summary>
        /// ��� ���������� ���������� (��������, ������� ��� ����).
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// ������ ����� �������� (��� �����) � ����� ������ � ����� ������.
        /// </summary>
        public string formatted { get; set; }
        /// <summary>
        /// ��� ������.
        /// </summary>
        public string country { get; set; }
        /// <summary>
        /// ��� ������.
        /// </summary>
        public string prefix { get; set; }
        /// <summary>
        /// ����� �������� ��� �����.
        /// </summary>
        public string number { get; set; }
        /// <summary>
        /// ��������.
        /// </summary>
        public string info { get; set; }
    }
}