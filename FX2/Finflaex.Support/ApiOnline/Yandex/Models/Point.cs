namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// SearchResponse.Point
    /// </summary>
    public class Point
    {
        /// <summary>
        /// ��� ���������.
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// ���������� �����������.
        /// </summary>
        public float[] coordinates { get; set; }
    }
}