namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    public class Interval
    {
        public string from { get; set; }
        public string to { get; set; }
    }
}