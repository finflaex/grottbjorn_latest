namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// CompanyMetaData.Geo
    /// </summary>
    public class Geo
    {
        /// <summary>
        /// Точность геокодирования адреса организации (см. подробнее).
        /// https://tech.yandex.ru/maps/doc/geocoder/desc/reference/precision-docpage/
        /// </summary>
        public string precision { get; set; }
    }
}