namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    public class Availability
    {
        public bool Everyday { get; set; }
        public bool TwentyFourHours { get; set; }
        public Interval[] Intervals { get; set; }
    }
}