namespace Finflaex.Support.ApiOnline.Yandex.Models
{
    /// <summary>
    /// ResponseMetaData
    /// </summary>
    public class Responsemetadata
    {
        /// <summary>
        /// ����������, ����������� ������.
        /// </summary>
        public Searchrequest SearchRequest { get; set; }
        /// <summary>
        /// ����������, ����������� �����.
        /// </summary>
        public Searchresponse SearchResponse { get; set; }
    }
}