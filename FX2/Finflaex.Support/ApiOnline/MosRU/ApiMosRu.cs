﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

namespace Finflaex.Support.ApiOnline.MosRU
{
    public static class ApiMosRu
    {
        private const string key = "53fb894deb4c111a90aa2142c4c96368";
        private const string url = "https://apidata.mos.ru/";

        private static fxcDictSS Keys =>
            new fxcDictSS
            {
                ["okopf"] = "2283",
                ["okved2"] = "2745",
                ["currency"] = "2293",
                ["country"] = "2724",
            };

        private static string Constructor(string id)
        {
            return url
                .Append("v1/")
                .Append("datasets/")
                .Append($"{Keys[id]}/")
                .Append("rows/");
        }

        private static string Constructor(int id)
        {
            return url
                .Append("v1/")
                .Append("datasets/")
                .Append($"{id}/")
                .Append("rows/");
        }

        public static Okopf[] Okopfs
            => Constructor("okopf")
                .GetWebRequest(act =>
                {

                })
                .ToString(Encoding.UTF8)
                .FromJson<Okopf[]>();

        public static Okopf Okopf(object code)
            => Constructor("okopf")
                .Append($"?q={code}")
                .GetWebRequest(act =>
                {

                })
                .ToString(Encoding.UTF8)
                .FromJson<Okopf[]>()
                .FirstOrDefault();

        public static Okved2[] Okved2s
            => Constructor("okved2")
                .GetWebRequest(act =>
                {

                })
                .ToString(Encoding.UTF8)
                .FromJson<Okved2[]>();

        public static Currency[] Currency
            => Constructor("currency")
                .GetWebRequest(act =>
                {

                })
                .ToString(Encoding.UTF8)
                .FromJson<Currency[]>();

        public static Country[] Countries
            => Constructor("country")
                .GetWebRequest(act =>
                {

                })
                .ToString(Encoding.UTF8)
                .FromJson<Country[]>();
    }

    public class Okopf
    {
        public int global_id { get; set; }
        public int Number { get; set; }
        public OkopfData Cells { get; set; }
    }

    public class OkopfData
    {
        public int global_id { get; set; }
        public string Kod { get; set; }
        public string Name { get; set; }
        public string NOMDESCR { get; set; }
    }


    public class Okved2
    {
        public int global_id { get; set; }
        public int Number { get; set; }
        public Okved2Data Cells { get; set; }
    }

    public class Okved2Data
    {
        public int global_id { get; set; }
        public string Nomdescr { get; set; }
        public string Razdel { get; set; }
        public string Kod { get; set; }
        public string Name { get; set; }
        public string Idx { get; set; }
    }


    public class Currency
    {
        public int global_id { get; set; }
        public int Number { get; set; }
        public CurrencyData Cells { get; set; }
    }

    public class CurrencyData
    {
        public int global_id { get; set; }
        public string CODE { get; set; }
        public string STRCODE { get; set; }
        public string NAME { get; set; }
        public string COUNTRY { get; set; }
    }

    public class Country
    {
        public int global_id { get; set; }
        public int Number { get; set; }
        public CountryData Cells { get; set; }
    }

    public class CountryData
    {
        public int global_id { get; set; }
        public string ALFA3 { get; set; }
        public string SHORTNAME { get; set; }
        public string FULLNAME { get; set; }
        public string ALFA2 { get; set; }
        public string CODE { get; set; }
    }

}
