﻿using System;

namespace Finflaex.Support.ApiOnline.CBR
{
    public class CbrNewsDocument
    {
        public CbrNewsDocument()
        {
        }

        public DateTime Date { get; set; }
        public string ID { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
    }
}