﻿using System;
using System.ComponentModel;

namespace Finflaex.Support.ApiOnline.CBR
{
    public class CbrValuteCurs
    {
        public CbrValuteCurs()
        {
        }

        [Description("Название валюты")]
        public string Name { get; set; }
        [Description("Номинал")]
        public int Volume { get; set; }
        [Description("Курс")]
        public decimal Curs { get; set; }
        [Description("Числ.код")]
        public string CodeNum { get; set; }
        [Description("Стр.код")]
        public string CodeChar { get; set; }
        [Description("Дата")]
        public DateTime Date { get; set; }
    }
}