﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.WebPages;
using System.Windows.Media.Animation;
using Finflaex.Support.ApiOnline.CBR.Models;
using Finflaex.Support.ApiOnline.PostApi.Models;
using Finflaex.Support.CbrOnline;
using Finflaex.Support.CbrSecurityMarket;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

namespace Finflaex.Support.ApiOnline.CBR
{
    public static class fxsCBR
    {
        private static fxcDictSO UrlList = new fxcDictSO()
        {
            ["cbr-xml-daily"] = "www.cbr-xml-daily.ru/archive/",
            ["cbr"] = "www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx"
        };

        public static Valute CursCurrencyDaily()
            => "http://www.cbr-xml-daily.ru/daily_json.js"
                .GetFormRequest()
                .FromJson<QuotesDay>()
                .Valute;

        public static Valute CursCurrencyOnDate(
                DateTime data)
            => UrlList
                    .GetString("cbr-xml-daily")
                    .Prepend("http://")
                    .Append(data.ToString("yyyy-MM-dd"))
                    .Append(".js")
                    .GetFormRequest()
                    .FromJson<QuotesDay>()
                    .Valute;

        public static string SendCBR(string data, string target)
            => UrlList
                .GetString("cbr")
                .Prepend("http://")
                .GetWebRequest(request =>
                    {
                        request.ContentType = "text/xml; charset=utf-8";
                        request.Headers.Add("SOAPAction", "http://web.cbr.ru/" + target);
                    },
                    data
                        .Prepend(
                            @"<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/""><soap:Body>")
                        .Append("</soap:Body></soap:Envelope>")
                        .ToBytes(Encoding.UTF8)
                )
                .ToString(Encoding.UTF8)
                .RemoveStart(@"<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Body>")
                .RemoveEnd(@"</soap:Body></soap:Envelope>");

        public static string SendCBR12(string data, string target)
            => UrlList
                .GetString("cbr")
                .Prepend("http://")
                .GetWebRequest(request =>
                    {
                        request.ContentType = "application/soap+xml; charset=utf-8;";
                        request.Headers.Add("SOAPAction", "http://web.cbr.ru/" + target);
                    },
                    data
                        .Prepend(@"<?xml version=""1.0"" encoding=""utf-8""?><soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope""><soap12:Body>")
                        .Append("</soap12:Body></soap12:Envelope>")
                        .ToBytes(Encoding.UTF8)
                )
                .ToString(Encoding.UTF8)
                .RemoveStart(
                    @"<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Body>")
                .RemoveEnd(@"</soap:Body></soap:Envelope>");

        /// <summary>
        /// http://www.cbr.ru/scripts/Root.asp?Prtid=DWS
        /// </summary>
        public static CbrOnline.DailyInfo mdi = new DailyInfo()
        {
            AllowAutoRedirect = true,
        };

        public static CbrSecurityMarket.SecInfo sdi = new SecInfo()
        {
            AllowAutoRedirect = true,
        };

        public static CbrValuteCurs[] GetCursOnDate(DateTime date) => mdi
            .GetCursOnDate(date)
            .Copy()
            .Tables[0]
            .Rows
            .Cast<DataRow>()
            .Select(item => new CbrValuteCurs()
            {
                Name = item[0].ToString().Trim('\r', '\n', ' '),
                Volume = item[1].ToString().AsInt(),
                Curs = item[2].ToString().AsDecimal(),
                CodeNum = item[3].ToString().Trim('\r', '\n', ' '),
                CodeChar = item[4].ToString().Trim('\r', '\n', ' '),
                Date = date
            })
            .ToArray();

        public static CbrNewsDocument[] GetNews(
                DateTime from,
                DateTime to)
            => mdi
                .NewsInfo(from, to)
                .Copy()
                .Tables[0]
                .Rows
                .Cast<DataRow>()
                .Select(item => new CbrNewsDocument()
                {
                    ID = item[0].ToString().Trim('\r', '\n', ' '),
                    Date = item[1].ToString().ToDateTime(),
                    Title = item[2].ToString().Trim('\r', '\n', ' '),
                    Url = item[3].ToString().Trim('\r', '\n', ' '),
                })
                .ToArray();
    }
}
