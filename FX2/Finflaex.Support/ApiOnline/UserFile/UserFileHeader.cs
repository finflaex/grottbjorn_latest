﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Finflaex.Support.Reflection;
using Microsoft.VisualBasic.FileIO;

#endregion

namespace Finflaex.Support.ApiOnline.UserFile
{
    public class UserFileHeader
    {
        private int call_comment = -1;
        private int call_date = -1;
        private int contact_fio = -1;
        private int contact_fio_comment = -1;
        private int contact_mail = -1;
        private int contact_mail_comment = -1;
        private int contact_phone = -1;
        private int contact_phone_comment = -1;
        private int contact_post = -1;
        private int contact_post_comment = -1;
        private int contact_url = -1;
        private int contact_url_comment = -1;
        private int id = -1;
        private int meeting_acc = -1;
        private int meeting_comment = -1;
        private int meeting_date = -1;
        private int org_comment = -1;
        private int org_inn = -1;
        private int org_name = -1;
        private int org_name_short = -1;
        private int org_address = -1;
        private int client_code = -1;

        public UserFileHeader(string header)
        {
            header
                .Split('|')
                .Select(v => v.ToLower())
                .Select(v=>v.Trim())
                .ForEach((index, value) =>
                {
                    value.Equal("id").IfTrue(() => id = index);
                    value.Equal("org_inn").IfTrue(() => org_inn = index);
                    value.Equal("org_name").IfTrue(() => org_name = index);
                    value.Equal("org_name_short").IfTrue(() => org_name_short = index);
                    value.Equal("org_comment").IfTrue(() => org_comment = index);
                    value.Equal("contact_mail").IfTrue(() => contact_mail = index);
                    value.Equal("contact_phone").IfTrue(() => contact_phone = index);
                    value.Equal("contact_url").IfTrue(() => contact_url = index);
                    value.Equal("contact_phone_comment").IfTrue(() => contact_phone_comment = index);
                    value.Equal("contact_mail_comment").IfTrue(() => contact_mail_comment = index);
                    value.Equal("contact_url_comment").IfTrue(() => contact_url_comment = index);
                    value.Equal("contact_post").IfTrue(() => contact_post = index);
                    value.Equal("contact_post_comment").IfTrue(() => contact_post_comment = index);
                    value.Equal("contact_fio").IfTrue(() => contact_fio = index);
                    value.Equal("contact_fio_comment").IfTrue(() => contact_fio_comment = index);
                    value.Equal("meeting_date").IfTrue(() => meeting_date = index);
                    value.Equal("meeting_comment").IfTrue(() => meeting_comment = index);
                    value.Equal("meeting_acc").IfTrue(() => meeting_acc = index);
                    value.Equal("call_date").IfTrue(() => call_date = index);
                    value.Equal("call_comment").IfTrue(() => call_comment = index);
                    value.Equal("org_address").IfTrue(() => org_address = index);
                    value.Equal("client_code").IfTrue(() => client_code = index);
                });
        }

        public UserFileOrgModel[] Parse(string[] lines) 
        {
            var result = new List<UserFileOrgModel>();

            UserFileOrgModel value = null;

            lines
                //.Select(v => v.Split(splits))
                .Select(v =>
                {
                    v = v.Clear("\r\n\t");
                    var par = new TextFieldParser(v.Replace('"','\'').ToStream());
                    par.SetDelimiters("|");
                    return par.ReadFields().Select(f => f.IsNotNullOrWhiteSpace() ? f.Replace('|', ';') : string.Empty).ToArray();
                })
                .ForEach(line =>
                {
                    var flag = line.Verify(id);

                    value.NotNull().AndNot(flag).IfTrue(() =>
                    {
                        result.Add(value);
                        value = null;
                    });

                    value.IsNull().AndNot(flag).IfTrue(() =>
                    {
                        value = new UserFileOrgModel
                        {
                            comment = line.Value(org_comment),
                            inn = line.Value(org_inn),
                            name = line.Value(org_name),
                            short_name = line.Value(org_name_short),
                            address = line.Value(org_address),
                            client_code = line.Value(client_code)
                        };
                    });

                    value.ActionIfNotNull(() =>
                    {
                        ParseContact(line, value);
                    });
                });

            value.ActionIfNotNull(()=>result.Add(value));

            return result.ToArray();
        }

        private void ParseContact(string[] line, UserFileOrgModel value)
        {
            if (line.Verify(contact_fio) || line.Verify(contact_post) || line.Verify(contact_phone) || line.Verify(contact_mail) || line.Verify(contact_url))
            {
                var person = new UserFilePersonModel
                    {
                        fio = line.Value(contact_fio),
                        post = line.Value(contact_post),
                        fio_comment = line.Value(contact_fio_comment),
                        post_comment = line.Value(contact_post_comment)
                    }
                    .Action(value.person.Add);

                if (line.Verify(contact_mail))
                    person.contact.Add(new UserFileContactModel
                    {
                        value = line.Value(contact_mail),
                        type = "email",
                        comment = line.Value(contact_mail_comment)
                    });

                if (line.Verify(contact_phone))
                    person.contact.Add(new UserFileContactModel
                    {
                        value = line.Value(contact_phone),
                        type = "phone",
                        comment = line.Value(contact_phone_comment)
                    });

                if (line.Verify(contact_url))
                    person.contact.Add(new UserFileContactModel
                    {
                        value = line.Value(contact_url),
                        type = "web",
                        comment = line.Value(contact_url_comment)
                    });

                if (line.Verify(call_date) && person.contact.Any())
                {
                    var date = line.Value(call_date).ToDateTime(
                        "dd.MM.yy",
                        "dd.MM.yy H:mm",
                        "dd.MM.yy H:mm:ss",
                        "dd.MM.yy HH:mm",
                        "dd.MM.yy HH:mm:ss",
                        "dd.MM.yyyy",
                        "dd.MM.yyyy H:mm",
                        "dd.MM.yyyy H:mm:ss",
                        "dd.MM.yyyy HH:mm",
                        "dd.MM.yyyy HH:mm:ss");

                    var contact = person.contact.LastOrDefault();

                    if ((date != null) && (contact != null))
                        contact.call = new UserFileCallModel
                        {
                            date = date.Value,
                            comment = line.Value(call_comment)
                        };
                }

                if (line.Verify(meeting_date) && person.contact.Any())
                {
                    var date = line.Value(meeting_date)
                        .ToDateTime(
                        "dd.MM.yy",
                        "dd.MM.yy H:mm",
                        "dd.MM.yy H:mm:ss",
                        "dd.MM.yy HH:mm",
                        "dd.MM.yy HH:mm:ss",
                        "dd.MM.yyyy",
                        "dd.MM.yyyy H:mm",
                        "dd.MM.yyyy H:mm:ss",
                        "dd.MM.yyyy HH:mm",
                        "dd.MM.yyyy HH:mm:ss");

                    var contact = person.contact.LastOrDefault();

                    if ((date != null) && (contact != null))
                        contact.meeting = new UserFileMeetingModel
                        {
                            date = date.Value,
                            comment = line.Value(meeting_comment),
                            acc = line.Value(meeting_acc)
                        };
                }
            }
        }
    }

    internal static class helpLine
    {
        public static string Value(this string[] @this, int index)
        {
            if (index == -1) return null;
            var list = new List<string>(@this);
            while (list.Count < index + 1)
            {
                list.Add(string.Empty);
            }
            @this = list.ToArray();
            return @this.Verify(index) ? @this[index].Clear("\r\n\t") : null;
        }

        public static bool Verify(this string[] @this, int index)
        {
            var list = new List<string>(@this);
            while (list.Count < index + 1)
            {
                list.Add(string.Empty);
            }
            @this = list.ToArray();
            return (index > -1) && @this[index].NotNullOrWhiteSpace();
        }
    }

    public class UserFileOrgModel
    {
        public UserFileOrgModel()
        {
            person = new List<UserFilePersonModel>();
        }

        public string inn { get; set; }
        public string name { get; set; }
        public string short_name { get; set; }
        public string comment { get; set; }
        public IEnumerable<UserFilePersonModel> person { get; set; }
        public string address { get; set; }
        public string client_code { get; set; }
    }

    public class UserFileMeetingModel
    {
        public DateTime date { get; set; }
        public string comment { get; set; }
        public string acc { get; set; }
    }

    public class UserFileCallModel
    {
        public DateTime date { get; set; }
        public string comment { get; set; }
    }

    public class UserFilePersonModel
    {
        public UserFilePersonModel()
        {
            contact = new List<UserFileContactModel>();
        }

        public string post { get; set; }
        public string fio { get; set; }
        public string post_comment { get; set; }
        public string fio_comment { get; set; }
        public IEnumerable<UserFileContactModel> contact { get; set; }
    }

    public class UserFileContactModel
    {
        public string type { get; set; }
        public string value { get; set; }
        public string comment { get; set; }
        public UserFileMeetingModel meeting { get; set; }
        public UserFileCallModel call { get; set; }
    }
}