﻿#region

using System;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.ApiOnline.Moex
{
    public static class fxsMoex
    {
        private const string url = "http://iss.moex.com/iss/";

        /// <summary>
        ///     Получить сводные обороты по рынкам.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static documentDataRow[] turnovers(object code)
            => url
                .Append($"turnovers")
                .Append($".xml")
                .GetWebRequest()
                .ToString(Encoding.UTF8)
                .FromXml<document>()
                .data
                .rows;


        /// <remarks />
        [Serializable]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        [XmlRoot(Namespace = "", IsNullable = false)]
        public class document
        {
            private documentData dataField;

            /// <remarks />
            public documentData data
            {
                get { return dataField; }
                set { dataField = value; }
            }
        }

        /// <remarks />
        [Serializable]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class documentData
        {
            private string idField;

            private documentDataMetadata metadataField;

            private documentDataRow[] rowsField;

            /// <remarks />
            public documentDataMetadata metadata
            {
                get { return metadataField; }
                set { metadataField = value; }
            }

            /// <remarks />
            [XmlArrayItem("row", IsNullable = false)]
            public documentDataRow[] rows
            {
                get { return rowsField; }
                set { rowsField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string id
            {
                get { return idField; }
                set { idField = value; }
            }
        }

        /// <remarks />
        [Serializable]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class documentDataMetadata
        {
            private documentDataMetadataColumn[] columnsField;

            /// <remarks />
            [XmlArrayItem("column", IsNullable = false)]
            public documentDataMetadataColumn[] columns
            {
                get { return columnsField; }
                set { columnsField = value; }
            }
        }

        /// <remarks />
        [Serializable]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class documentDataMetadataColumn
        {
            private ushort bytesField;

            private bool bytesFieldSpecified;

            private byte max_sizeField;

            private bool max_sizeFieldSpecified;

            private string nameField;

            private string typeField;

            /// <remarks />
            [XmlAttribute]
            public string name
            {
                get { return nameField; }
                set { nameField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string type
            {
                get { return typeField; }
                set { typeField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public ushort bytes
            {
                get { return bytesField; }
                set { bytesField = value; }
            }

            /// <remarks />
            [XmlIgnore]
            public bool bytesSpecified
            {
                get { return bytesFieldSpecified; }
                set { bytesFieldSpecified = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public byte max_size
            {
                get { return max_sizeField; }
                set { max_sizeField = value; }
            }

            /// <remarks />
            [XmlIgnore]
            public bool max_sizeSpecified
            {
                get { return max_sizeFieldSpecified; }
                set { max_sizeFieldSpecified = value; }
            }
        }

        /// <remarks />
        [Serializable]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class documentDataRow
        {
            private string emitent_idField;

            private string emitent_innField;

            private string emitent_okpoField;

            private string emitent_titleField;

            private string gosregField;

            private string groupField;

            private uint idField;

            private byte is_tradedField;

            private string isinField;

            private string marketprice_boardidField;

            private string nameField;

            private string primary_boardidField;

            private string regnumberField;

            private string secidField;

            private string shortnameField;

            private string typeField;

            /// <remarks />
            [XmlAttribute]
            public uint id
            {
                get { return idField; }
                set { idField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string secid
            {
                get { return secidField; }
                set { secidField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string shortname
            {
                get { return shortnameField; }
                set { shortnameField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string regnumber
            {
                get { return regnumberField; }
                set { regnumberField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string name
            {
                get { return nameField; }
                set { nameField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string isin
            {
                get { return isinField; }
                set { isinField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public byte is_traded
            {
                get { return is_tradedField; }
                set { is_tradedField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string emitent_id
            {
                get { return emitent_idField; }
                set { emitent_idField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string emitent_title
            {
                get { return emitent_titleField; }
                set { emitent_titleField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string emitent_inn
            {
                get { return emitent_innField; }
                set { emitent_innField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string emitent_okpo
            {
                get { return emitent_okpoField; }
                set { emitent_okpoField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string gosreg
            {
                get { return gosregField; }
                set { gosregField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string type
            {
                get { return typeField; }
                set { typeField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string group
            {
                get { return groupField; }
                set { groupField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string primary_boardid
            {
                get { return primary_boardidField; }
                set { primary_boardidField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string marketprice_boardid
            {
                get { return marketprice_boardidField; }
                set { marketprice_boardidField = value; }
            }
        }


        /// <remarks />
        [Serializable]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        [XmlRoot(Namespace = "", IsNullable = false)]
        public class row
        {
            private byte is_hiddenField;

            private string nameField;

            private string precisionField;

            private byte sort_orderField;

            private string titleField;

            private string typeField;

            private string valueField;

            /// <remarks />
            [XmlAttribute]
            public string name
            {
                get { return nameField; }
                set { nameField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string title
            {
                get { return titleField; }
                set { titleField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string value
            {
                get { return valueField; }
                set { valueField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string type
            {
                get { return typeField; }
                set { typeField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public byte sort_order
            {
                get { return sort_orderField; }
                set { sort_orderField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public byte is_hidden
            {
                get { return is_hiddenField; }
                set { is_hiddenField = value; }
            }

            /// <remarks />
            [XmlAttribute]
            public string precision
            {
                get { return precisionField; }
                set { precisionField = value; }
            }
        }
    }
}