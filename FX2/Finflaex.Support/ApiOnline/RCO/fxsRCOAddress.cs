﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.MVC.Enum;
using Finflaex.Support.Reflection;

namespace Finflaex.Support.ApiOnline.RCO
{
    public static class fxsRCOAddress
    {
        private static string url = "http://addr.rco.ru/default.aspx";

        public static IEnumerable<AddressInfo> RCOAddress(this string @this)
        {
            var request = WebRequest.Create(url).AsType<HttpWebRequest>();


            var response = request.GetResponse();

            var read1 = response.GetResponseStream().ReadBytes().ToString(Encoding.GetEncoding(1251));
            var regex = read1.DoRegexMatches("<input.*name=\"(\\S+)\".*value=\"(\\S+)\".*\\/>");
            var data = new Dictionary<string, string>
            {
                ["__VIEWSTATE"] = regex[0].Groups[2].Value.UrlEncode(),
                ["__VIEWSTATEENCRYPTED"] = string.Empty,
                ["__EVENTVALIDATION"] = regex[1].Groups[2].Value.UrlEncode(),
                ["tbAddress"] = @this.UrlEncode(Encoding.GetEncoding(1251)),
                ["btnAnalyze"] = "Разобрать".UrlEncode(Encoding.GetEncoding(1251)),
                ["cbUseFuzzy"] = "on"
            };


            request = WebRequest.Create(url).AsType<HttpWebRequest>();
            request.CookieContainer = new CookieContainer();
            request.CookieContainer.Add(response.AsType<HttpWebResponse>().Cookies);
            request.Method = "POST";
            var form = data.Select(v => $"{v.Key}={v.Value}").Join("&").ToBytes(Encoding.UTF8);
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            request.Headers.Add("Accept-Encoding", "deflate");
            request.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            request.Headers.Add("Cache-Control", "no-cache");
            request.Headers.Add("Origin", "addr.rco.ru");
            request.Referer = url;
            request.Headers.Add("Upgrade-Insecure-Requests", "1");
            request.UserAgent =
                @"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36";
            request.ContentType = ContentTypes.ApplicationXWWWFormUrlencoded;
            request.ContentLength = form.Length;
            request.Headers.Add("X-Compress", null);
            using (var stream = request.GetRequestStream())
            {
                stream.Write(form, 0, form.Length);
            }

            response = request.GetResponse();
            var read2 = response.GetResponseStream().ReadBytes().ToString(Encoding.GetEncoding(1251));

            return read2
                .DoRegexMatches(@"<tr>\s*<td>(\d+)<\/td><td>\s*(.*)\s*<\/td><td>(\d+|&nbsp;)\s*<\/td><td>\s*(\d*)\s*<\/td>\s*<\/tr>")
                .Select(v => new AddressInfo
                {
                    //code = v.Groups[1].Value.Trim(),
                    Value = v.Groups[2].Value.Trim().RemoveStart("индекс "),
                    KLADR = v.Groups[3].Value.Trim().RemoveStart("&nbsp;"),
                    Level = v.Groups[4].Value.Trim().ToInt()
                })
                .ToArray();
        }


        public class AddressInfo
        {
            public string KLADR { get; set; }
            public string Value { get; set; }
            public int Level { get; set; }
        }

        public static IEnumerable<string> GetKladrParents(this AddressInfo @this)
        {
            if (@this == null) return null;
            var read = (@this?.KLADR).retIfNullOrWhiteSpace("0".Repeat(13));
            read += "0".Repeat(13 - read.Length);
            return new[]
                {
                    read.Take(2).InString().Append("0".Repeat(11)),
                    read.Take(5).InString().Append("0".Repeat(8)),
                    read.Take(8).InString().Append("0".Repeat(5)),
                    read.Take(11).InString().Append("0".Repeat(2)),
                    read.Take(13).InString()
                }
                .Distinct();
        }

        public static string GetOtherAddress(
            this AddressInfo @this,
            string city)
            => @this == null ? null : @"(\d{6},\s)"
                .ToRegex()
                .Replace(@this.Value, "")
                .RemoveStart(city)
                .RemoveStart(", ");
    }
}
