﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class Fssregistration
    {
        public string registrationDate { get; set; }
        public string number { get; set; }
        public Pfr fss { get; set; }
    }
}