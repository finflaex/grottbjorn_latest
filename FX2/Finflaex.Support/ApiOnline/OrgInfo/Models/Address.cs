﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class Address
    {
        public AddressItem region { get; set; }
        public AddressItem auto { get; set; }
        public AddressItem area { get; set; }
        public AddressItem city { get; set; }
        public AddressItem cityArea { get; set; }
        public AddressItem place { get; set; }
        public AddressItem street { get; set; }
        public AddressItem ext { get; set; }
        public AddressItem sext { get; set; }
        public string house { get; set; }
        public string building { get; set; }
        public string flat { get; set; }
        public string postalIndex { get; set; }
        public string fullHouseAddress { get; set; }
    }
}