﻿#region

using System;

#endregion

namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class Closeinfo
    {
        public DateTime date { get; set; }
        public Type closeReason { get; set; }
    }
}