﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class Person
    {
        public string id { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string surName { get; set; }
        public string inn { get; set; }
        public string url { get; set; }
        public string fullName { get; set; }
        public string fullNameWithInn { get; set; }
    }
}