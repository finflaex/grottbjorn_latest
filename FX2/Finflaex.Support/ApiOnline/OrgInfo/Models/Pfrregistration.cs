﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class Pfrregistration
    {
        public string registrationDate { get; set; }
        public string number { get; set; }
        public Pfr pfr { get; set; }
    }
}