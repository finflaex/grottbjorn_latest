﻿#region

using System.Collections.Generic;

#endregion

namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class SpyzerModel
    {
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Dictionary<int, float> Profit { get; set; }
    }
}