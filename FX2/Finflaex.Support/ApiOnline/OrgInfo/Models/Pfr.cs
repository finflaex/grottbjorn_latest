﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class Pfr
    {
        public string id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string fullName { get; set; }
    }
}