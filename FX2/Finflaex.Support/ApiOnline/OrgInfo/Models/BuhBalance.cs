﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class BuhBalance
    {
        public int Year { get; set; }
        public string Code { get; set; }
        public string Caption { get; set; }
        public float Value { get; set; }
        public string Inn { get; set; }
    }
}