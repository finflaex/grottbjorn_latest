﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class Type
    {
        public int id { get; set; }
        public int level { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public int code { get; set; }
    }
}