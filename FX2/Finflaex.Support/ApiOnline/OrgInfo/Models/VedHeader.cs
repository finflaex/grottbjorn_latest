﻿#region

using System;
using System.Linq;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class VedHeader
    {
        public int adr_export;
        public int adr_import;
        public int contact_name;
        public int contact_phone;
        public int contact_post;
        public int country_dst;
        public int country_src;
        public int currency;
        public int curs;

        public int name_export;
        public int name_import;
        public int number;
        public int org_inn;

        public int stat;
        public int summ;
        public int ved_sum;

        public VedHeader(string header)
        {
            header
                .Split(';')
                .Select(v => v.ToLower())
                .ForEach((index, value) =>
                {
                    value.Contains("g011").IfTrue(() => stat = index);
                    new[] {"kod", "nd", "num"}.Any(value.Contains).IfTrue(() => number = index);
                    value.Contains("g091").IfTrue(() => org_inn = index);
                    value.Contains("g221").IfTrue(() => currency = index);
                    value.Contains("g222").IfTrue(() => summ = index);
                    value.Contains("g23").And(!value.Contains("g230")).IfTrue(() => curs = index);
                    value.Contains("g5441").IfTrue(() => contact_name = index);
                    value.Contains("g5442").IfTrue(() => contact_phone = index);
                    value.Contains("g5447").IfTrue(() => contact_post = index);
                    value.Contains("g15").And(!value.Contains("g15a")).IfTrue(() => country_src = index);
                    value.Contains("g17").And(!value.Contains("g17a")).IfTrue(() => country_dst = index);
                    value.Contains("g12").And(!value.Contains("g121")).IfTrue(() => ved_sum = index);
                    value.Contains("g023").And(!value.Contains("g0231")).IfTrue(() => adr_export = index);
                    value.Contains("g083").And(!value.Contains("g0831")).IfTrue(() => adr_import = index);
                    value.Contains("g022").IfTrue(() => name_export = index);
                    value.Contains("g082").IfTrue(() => name_import = index);
                });
        }

        public VedParserModel ParseLine(string line)
        {
            return line
                .Split(';')
                .FuncSelect(value =>
                {
                    var curs_value = value[curs].ToFloat();
                    var summ_value = value[summ].ToFloat();
                    var summ_ved = value[ved_sum].ToFloat();
                    var summ_result = curs_value*summ_value;
                    var flag = value[stat].ToLower().Equals("им");

                    return new VedParserModel
                    {
                        Inn = value[org_inn],
                        Summ = summ_result == 0 ? summ_ved : summ_result,
                        Number = value[number],
                        Date = value[number].DoRegexMatches(@"(\d*)")[2].Groups[1].Value.ToDateTime("ddMMyy"),
                        Import = flag,
                        Currency = value[currency],
                        Curs = curs_value,
                        Country = flag ? value[country_src] : value[country_dst],
                        FIO = value[contact_name],
                        Post = value[contact_post],
                        Phone = value[contact_phone],
                        Address = value[flag ? adr_import : adr_export],
                        Name = value[flag ? name_import : name_export]
                    };
                });
        }
    }

    public class VedParserModel
    {
        public string Inn { get; internal set; }
        public float Summ { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public bool Import { get; set; }
        public string Currency { get; set; }
        public float Curs { get; set; }
        public string Country { get; set; }
        public string FIO { get; set; }
        public string Post { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
    }
}