﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class Okved2
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public Type parent { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public string fullName { get; set; }
    }
}