﻿#region

using System;

#endregion

namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class IpData
    {
        public int id { get; set; }
        public Person person { get; set; }
        public DateTime lastUpdateDate { get; set; }
        public Okved1 mainOkved1 { get; set; }
        public Okved1[] okved1 { get; set; }
        public object[] okved2 { get; set; }
        public Closeinfo closeInfo { get; set; }
        public string ogrn { get; set; }
        public DateTime ogrnDate { get; set; }
    }
}