﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class AddressItem
    {
        public string id { get; set; }
        public string name { get; set; }
        public string typeName { get; set; }
        public string typeShortName { get; set; }
        public string fullName { get; set; }
        public string aoid { get; set; }
        public string guid { get; set; }
        public string regionCode { get; set; }
        public string autoCode { get; set; }
        public string areaCode { get; set; }
        public string cityCode { get; set; }
        public string ctarCode { get; set; }
        public string placeCode { get; set; }
        public string streetCode { get; set; }
        public string extrCode { get; set; }
        public string sextCode { get; set; }
        public string kladrCode { get; set; }
        public string postalCode { get; set; }
        public string level { get; set; }
        public string okato { get; set; }
        public string live { get; set; }
        public string companyCount { get; set; }
        public string oktmo { get; set; }
        public Type type { get; set; }
        public string url { get; set; }
    }
}