﻿#region

using System;

#endregion

namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class VedDeclarationModel
    {
        public bool Import { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public string Inn { get; set; }
        public string Currency { get; set; }
        public float Curs { get; set; }
        public float Value { get; set; }
        public string Country { get; set; }
    }
}