﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class Root<T>
    {
        public T[] data { get; set; }
    }
}