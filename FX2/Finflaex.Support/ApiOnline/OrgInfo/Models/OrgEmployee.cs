﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class OrgEmployee
    {
        public int id { get; set; }
        public OrgData company { get; set; }
        public Person person { get; set; }
        public Post post { get; set; }
        public string postName { get; set; }
    }
}