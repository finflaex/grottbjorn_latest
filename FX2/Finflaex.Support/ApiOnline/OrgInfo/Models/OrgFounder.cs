﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class OrgFounder
    {
        public int id { get; set; }
        public OrgData company { get; set; }
        public Person personOwner { get; set; }
        public float price { get; set; }
        public bool ownerRussia { get; set; }
        public OrgData companyOwner { get; set; }
    }
}