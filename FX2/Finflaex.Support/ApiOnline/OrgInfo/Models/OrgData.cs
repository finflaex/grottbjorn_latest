﻿#region

using System;

#endregion

namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class OrgData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public string ogrn { get; set; }
        public DateTime ogrnDate { get; set; }
        public string inn { get; set; }
        public string kpp { get; set; }
        public Closeinfo closeInfo { get; set; }
        public Okopf okopf { get; set; }
        public DateTime lastUpdateDate { get; set; }
        public Authorizedcapital authorizedCapital { get; set; }
        public Address address { get; set; }
        public Okved1 mainOkved1 { get; set; }
        public Okved1[] okved1 { get; set; }
        public Okved2 mainOkved2 { get; set; }
        public Okved2[] okved2 { get; set; }
        public Pfrregistration pfrRegistration { get; set; }
        public Fssregistration fssRegistration { get; set; }
        public string url { get; set; }

        public ForeignRegistration foreignRegistration { get; set; }
    }


    public class ForeignRegistration
    {
        public Oksm oksm { get; set; }
        public DateTime date { get; set; }
        public string address { get; set; }
        public string number { get; set; }
    }

    public class Oksm
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string fullName { get; set; }
    }

}