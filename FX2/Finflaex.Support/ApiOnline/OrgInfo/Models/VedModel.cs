﻿#region

using System.Collections.Generic;

#endregion

namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class VedModel
    {
        public VedModel()
        {
            Contacts = new List<VedContactModel>();
            Declarations = new List<VedDeclarationModel>();
        }

        public List<VedContactModel> Contacts { get; set; }
        public List<VedDeclarationModel> Declarations { get; set; }
    }
}