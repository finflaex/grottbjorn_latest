﻿namespace Finflaex.Support.ApiOnline.OrgInfo.Models
{
    public class VedContactModel
    {
        public string FIO { get; set; }
        public string Phone { get; set; }
        public string Post { get; set; }
        public string OrigPhone { get; set; }
        public string Inn { get; set; }
        public string Mail { get; set; }
        public string WWW { get; set; }
        public string Fax { get; set; }
        public string OrigFax { get; set; }
    }
}