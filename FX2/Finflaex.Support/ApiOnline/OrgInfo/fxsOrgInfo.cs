﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Finflaex.Support.ApiOnline.OrgInfo.Models;
using Finflaex.Support.Ocr;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.ApiOnline.OrgInfo
{
    public static class fxsOrgInfo
    {
        public static Dictionary<string, string> urls => new Dictionary<string, string>
        {
            {"spyzer", "https://spyzer.ru"},
            {"ogrn_online", "https://ru.rus.company/интеграция"},
            {"infobroker", "http://infobroker.ru"},
            {"list_org", "http://www.list-org.com"},
            {"buhbalance", "http://www.znaytovar.ru"},
            {"contragents_balans", "https://contragents.ru"},
            {"ved", "https://ved-gtd.com"},
            {"service_online", "https://service-online.su"}
        };

        public static VedContactModel[] OrgContacts(string inn)
        {
            return new /*Task<VedContactModel[]>*/[]
                {
                    Task.Run(() => QueryInfobroker(inn)),
                    Task.Run(() => QueryListOrg(inn)),
                    Task.Run(() => QueryCustomsVed(inn).Contacts.ToArray()),
                    //Task.Run(() => QuerySpyzer(inn)),
                    Task.Run(() => QueryServiceOnline(inn))
                }
                .Action(Task.WaitAll)
                .SelectMany(v => v.Result)
                .ToArray();

            //.FuncSelect(list => {
            //    var result = new List<VedContactModel>();

            //    list.ForEach(contact =>
            //    {
            //        contact.OrigPhone.ActionIfNotNull(read =>
            //        {


            //            //return read
            //            //    .Split(new[] {',', '/'})
            //            //    .Select(item =>
            //            //    {
            //            //        return item
            //            //            .DoRegex(@"(?:\+?)(?:[7,8]?)\D*(\d?)\D*(\d?)\D*(\d?)\D*(\d?)\D*(\d?)\D*(\d?)\D*(\d?)\D*(\d?)\D*(\d?)\D*(\d?)")
            //            //            .
            //            //    })
            //            //    .Action(result.AddRange);
            //        });
            //    });

            //    return result;
            //})
            //.ToArray();
        }

        public static VedContactModel[] QueryServiceOnline(string inn)
        {
            return $"{urls["service_online"]}/catalog/katalog-firm.php?inn={inn}"
                .GetFormRequest()
                .FuncSelect(content =>
                {
                    var contacts = new List<VedContactModel>();

                    content
                        .DoRegexMatches("<a href=\"tel:\\s?(.*?)\">")
                        .Select(match => match.Groups[1].Value)
                        .Select(phone => new VedContactModel
                        {
                            Inn = inn,
                            OrigPhone = phone,
                            Phone = phone
                                .DoRegexMatches(@"(\d)")
                                .Select(v => v.Groups[1].Value[0])
                                .ToArray()
                                .Instance<string>()
                        })
                        .Action(contacts.AddRange);

                    content
                        .DoRegexMatches("<a href=\"mailto:\\s?(\\w*@\\w*\\.\\w*)\">")
                        .Select(match => match.Groups[1].Value.ToLower())
                        .Where(mail => !mail.Contains("infobroker"))
                        .Select(mail => new VedContactModel
                        {
                            Inn = inn,
                            Mail = mail
                        })
                        .Action(contacts.AddRange);

                    content
                        .DoRegexMatches("<td>Факс<\\/td>\\W*<td>(.*)<\\/td>")
                        .Select(match => match.Groups[1].Value.ToLower())
                        .Where(fax => !fax.IsNullOrWhiteSpace())
                        .Select(fax => new VedContactModel
                        {
                            Inn = inn,
                            Fax = fax
                        })
                        .Action(contacts.AddRange);

                    return contacts.ToArray();
                });
        }

        public static Task<BuhBalance[]> QueryContragentsBalanceAsync(string inn, int year = 0)
            => Task.Run(() => QueryContragentsBalance(inn, year));

        public static BuhBalance[] QueryContragentsBalance(string inn, int year = 0)
        {
            year = year == 0 ? DateTime.Now.Year - 1 : year;

            return $"{urls["contragents_balans"]}/balansy/{inn}/{year}"
                .GetFormRequest()
                .DoRegexMatches(@"\[(\d*)\] => (\d*)")
                .Select(match => new
                {
                    index = match.Groups[1].Value[4],
                    value = new BuhBalance
                    {
                        Year = year,
                        Code = match.Groups[1].Value.Take(4).ToArray().Instance<string>(),
                        Value = match.Groups[2].Value.ToFloat()
                    }
                })
                .Where(v => v.index == '3')
                .Select(v => v.value)
                .Where(v => !v.Value.Equals(0))
                .ToArray();
        }

        public static BuhBalance[] QueryBuhBalance(string inn)
        {
            return $"{urls["buhbalance"]}/finance/firm{inn}.html"
                .GetFormRequest()
                .FuncSelect(content =>
                {
                    if (content.IsNullOrEmpty())
                        return new BuhBalance[0];

                    var years = content
                        .DoRegexMatches(
                            @"<tr>\s*<th>.*?<\/th><th>(.*?)<\/th><th>(.*?)<\/th><th>(.*?)<\/th><th>(.*?)<\/th><\/tr>")
                        .FirstOrDefault()?
                        .Groups
                        .Cast<Group>()
                        .Select(group => group.Value.ToInt())
                        .Skip(1)
                        .ToArray();

                    return content
                        .DoRegexMatches(
                            @"<tr><td>(\d*)\s(.*?)<\/td><td>(.*?)<\/td><td>(.*?)<\/td><td>(.*?)<\/td><td>(.*?)<\/tr>")
                        .SelectMany(match => Enumerable
                            .Range(0, 4)
                            .Select(index => new
                            {
                                index = match.Groups[1].Value[4],
                                value = new BuhBalance
                                {
                                    Inn = inn,
                                    Year = years[index],
                                    Caption = match.Groups[2].Value,
                                    Code = match.Groups[1].Value.Take(4).ToArray().Instance<string>(),
                                    Value = match.Groups[index + 3].Value.ToFloat()
                                }
                            }))
                        .Where(v => v.index == '3')
                        .Select(v => v.value)
                        .Where(v => !v.Value.Equals(0))
                        .ToArray();
                });
        }

        /// <summary>
        ///     Телефон, факс и выручка за последние 5 лет по ИНН или ОГРН
        /// </summary>
        /// <param name="inn"></param>
        /// <returns></returns>
        public static Task<VedContactModel[]> QuerySpyzerAsync(string inn)
            => Task.Run(() => QuerySpyzer(inn));

        /// <summary>
        ///     Телефон, факс и выручка за последние 5 лет по ИНН или ОГРН
        /// </summary>
        /// <param name="inn"></param>
        /// <returns></returns>
        public static VedContactModel[] QuerySpyzer(string inn)
        {
            return $"{urls["spyzer"]}/?search={inn}"
                .GetFormRequest()
                .DoRegexMatches("<a href=\"(\\/company\\/.*)\"\" class=\".*company")
                .FirstOrDefault()?.Groups[1].Value
                .FuncSelect(read => $"{urls["spyzer"]}{read}".GetFormRequest())
                .FuncSelect(content =>
                {
                    var recognize = new fxcRecognize(
                        "spyzer",
                        b => b.ToLowerMeasurement(item => item[0], 0),
                        v => v > 180);

                    var result = new List<VedContactModel>();

                    content
                        .DoRegexMatches(@"<tr><td.*>Телефон<\/th\><td><img.*src='data:image\/png;base64,(.*)'><\/td><\/tr>")
                        .Select(match => match.Groups[1].Value.FromBase64().FuncSelect(recognize.Recognize))
                        .Join(" ")
                        .FuncWhere(read => read.IsNullOrWhiteSpace(), read => null)?
                        .Split(new[] {',', '/'}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(read => new VedContactModel
                        {
                            Inn = inn,
                            OrigPhone = read,
                            Phone = read
                                .DoRegexMatches(@"(\d)")
                                .Select(v => v.Groups[1].Value[0])
                                .ToArray()
                                .Instance<string>()
                        })
                        .Action(result.AddRange);

                    content
                        .DoRegexMatches(@"<tr><td.*>Факс<\/th\><td><img.*src='data:image\/png;base64,(.*)'><\/td><\/tr>")
                        .Select(match => match.Groups[1].Value.FromBase64().FuncSelect(recognize.Recognize))
                        .Join(" ")
                        .FuncWhere(read => read.IsNullOrWhiteSpace(), read => null)?
                        .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(read => new VedContactModel
                        {
                            Inn = inn,
                            OrigFax = read,
                            Fax = read
                                .DoRegexMatches(@"(\d)")
                                .Select(v => v.Groups[1].Value[0])
                                .ToArray()
                                .Instance<string>()
                        })
                        .Action(result.AddRange);

                    return result.ToArray();
                });
        }

        public static VedContactModel[] QueryInfobroker(string inn)
        {
            return $"{urls["infobroker"]}/search?query={inn}"
                .GetFormRequest()
                .FuncSelect(content =>
                {
                    var contacts = new List<VedContactModel>();

                    content
                        .DoRegexMatches("<a href=\"tel:\\s?(.*?)\">")
                        .Select(match => match.Groups[1].Value)
                        .Select(phone => new VedContactModel
                        {
                            Inn = inn,
                            OrigPhone = phone,
                            Phone = phone
                                .DoRegexMatches(@"(\d)")
                                .Select(v => v.Groups[1].Value[0])
                                .ToArray()
                                .Instance<string>()
                        })
                        .Action(contacts.AddRange);

                    content
                        .DoRegexMatches("<a href=\"mailto:\\s?(\\w*@\\w*\\.\\w*)\">")
                        .Select(match => match.Groups[1].Value.ToLower())
                        .Where(mail => !mail.Contains("infobroker"))
                        .Select(mail => new VedContactModel
                        {
                            Inn = inn,
                            Mail = mail
                        })
                        .Action(contacts.AddRange);

                    content
                        .DoRegexMatches("<a href=\"http\\w?:\\/\\/WWW\\.\\w*\\.\\w{2,3}\">(WWW\\.\\w*\\.\\w{2,3})<\\/a>")
                        .Select(match => match.Groups[1].Value.ToLower())
                        .Select(www => new VedContactModel
                        {
                            Inn = inn,
                            WWW = www
                        })
                        .Action(contacts.AddRange);

                    return contacts.ToArray();
                });
        }

        public static Task<VedContactModel[]> QueryListOrgAsync(string inn)
            => Task.Run(() => QueryListOrg(inn));

        public static VedContactModel[] QueryListOrg(string inn)
        {
            return $"{urls["list_org"]}/search.php?type=inn&val={inn}"
                       .GetFormRequest()
                       .DoRegexMatches(@"<a href='(\/company\/\d*)'>().*<\/a>")
                       .Select(match => $"{urls["list_org"]}{match.Groups[1].Value}")
                       .FirstOrDefault()?
                       .GetFormRequest()
                       .FuncSelect(content =>
                       {
                           var result = new List<VedContactModel>();

                           content
                               .DoRegexMatches(@"<p><i>Телефон:<\/i>(.*)<\/p>")
                               .Select(match => match.Groups[1].Value)
                               .Select(read => new VedContactModel
                               {
                                   Inn = inn,
                                   Phone = read
                                       .DoRegexMatches(@"(\d)")
                                       .Select(v => v.Groups[1].Value[0])
                                       .ToArray()
                                       .Instance<string>(),
                                   OrigPhone = read
                               })
                               .Action(result.AddRange);

                           content
                               .DoRegexMatches(@"<p><i>Эл.почта \(e-mail\):<\/i>(.*)<\/p>")
                               .Select(match => match.Groups[1].Value)
                               .Select(read => new VedContactModel
                               {
                                   Inn = inn,
                                   Mail = read
                               })
                               .Action(result.AddRange);

                           content
                               .DoRegexMatches(@"<p><i>Сайт \(www\):<\/i>(.*)<\/p>")
                               .Select(match => match.Groups[1].Value)
                               .Select(read => new VedContactModel
                               {
                                   Inn = inn,
                                   WWW = read
                               })
                               .Action(result.AddRange);

                           return result.ToArray();
                       }) ?? new VedContactModel[0];
        }

        /// <summary>
        ///     Поиск компании по ИНН, ОГРН или по названию. С помощью этой команды можно получить идентификатор нужной компании и
        ///     далее с помощью следующих команд получить детальную информацию.
        /// </summary>
        /// <param name="inn"></param>
        /// <param name="ogrn"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static OrgData[] SearchOrg(
            string inn = null,
            string ogrn = null,
            string name = null)
        {
            return $"{urls["ogrn_online"]}/компании/?наименование={name}&инн={inn}&огрн={ogrn}"
                       .GetFormRequest()
                       .FromJson<OrgData[]>() ?? new OrgData[0];
        }

        /// <summary>
        ///     Поиск компании по ИНН, ОГРН или по названию. С помощью этой команды можно получить идентификатор нужной компании и
        ///     далее с помощью следующих команд получить детальную информацию.
        /// </summary>
        /// <param name="inn"></param>
        /// <param name="ogrn"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Task<OrgData[]> SearchOrgAsync(
                string inn = null,
                string ogrn = null,
                string name = null)
            => Task.Run(() => SearchOrg(inn, ogrn, name));

        /// <summary>
        ///     Возвращает данные из ЕГРЮЛ о компании с идентификатором {id}
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static OrgData LoadOrg(OrgData data)
        {
            return $"{urls["ogrn_online"]}/компании/{data.id}/"
                .GetFormRequest()
                .FromJson<OrgData>();
        }

        /// <summary>
        ///     Возвращает данные из ЕГРЮЛ о компании с идентификатором {id}
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static OrgData LoadOrg(string id)
        {
            if (id.IsNullOrWhiteSpace()) return null;
            return $"{urls["ogrn_online"]}/компании/{id}/"
                .GetFormRequest()
                .FromJson<OrgData>();
        }

        /// <summary>
        ///     Возвращает данные из ЕГРЮЛ о компании с идентификатором {id}
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<OrgData> LoadOrgAsync(OrgData data)
            => Task.Run(() => LoadOrg(data));

        /// <summary>
        ///     Возвращает данные об учредителях компании с идентификатором {id}
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static OrgFounder[] LoadOrgFounders(OrgData data)
        {
            return $"{urls["ogrn_online"]}/компании/{data.id}/учредители/"
                .GetFormRequest()
                .FromJson<OrgFounder[]>();
        }

        /// <summary>
        ///     Возвращает данные об учредителях компании с идентификатором {id}
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<OrgFounder[]> LoadOrgFoundersAsync(OrgData data)
            => Task.Run(() => LoadOrgFounders(data));

        /// <summary>
        ///     Возвращает данные о сотрудниках, умеющих право подписи в компании с идентификатором {id}
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static OrgEmployee[] LoadOrgEmployees(OrgData data)
        {
            return $"{urls["ogrn_online"]}/компании/{data.id}/сотрудники/"
                .GetFormRequest()
                .FromJson<OrgEmployee[]>();
        }

        /// <summary>
        ///     Возвращает данные о сотрудниках, умеющих право подписи в компании с идентификатором {id}
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<OrgEmployee[]> LoadOrgEmployeesAsync(OrgData data)
            => Task.Run(() => LoadOrgEmployees(data));

        /// <summary>
        ///     Возвращает другие компании, в которых данная компания является соучредителем
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static OrgData[] LoadOrgChilds(OrgData data)
        {
            return $"{urls["ogrn_online"]}/компании/{data.id}/зависимые/"
                .GetFormRequest()
                .FromJson<OrgData[]>();
        }

        /// <summary>
        ///     Возвращает другие компании, в которых данная компания является соучредителем
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<OrgData[]> LoadOrgChildsAsync(OrgData data)
            => Task.Run(() => LoadOrgChilds(data));

        /// <summary>
        ///     Возвращает информацию о человеке с указанным идентификатором
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Person LoadPerson(Person data)
        {
            return $"{urls["ogrn_online"]}/люди/{data.id}/"
                .GetFormRequest()
                .FromJson<Person>();
        }

        /// <summary>
        ///     Возвращает информацию о человеке с указанным идентификатором
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<Person> LoadPersonAsync(Person data)
            => Task.Run(() => LoadPerson(data));

        /// <summary>
        ///     Возвращает информацию должностях, которые занимает указанный человек
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static OrgEmployee[] LoadEmployees(Person data)
        {
            return $"{urls["ogrn_online"]}/люди/{data.id}/должности/"
                .GetFormRequest()
                .FromJson<OrgEmployee[]>();
        }

        /// <summary>
        ///     Возвращает информацию должностях, которые занимает указанный человек
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<OrgEmployee[]> LoadEmployeesAsync(Person data)
            => Task.Run(() => LoadEmployees(data));

        /// <summary>
        ///     Возвращает информацию о компаниях, в которых данный человек является учредителем
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static OrgData[] LoadOrgData(Person data)
        {
            return $"{urls["ogrn_online"]}/люди/{data.id}/компании/"
                .GetFormRequest()
                .FromJson<OrgData[]>();
        }

        /// <summary>
        ///     Возвращает информацию о компаниях, в которых данный человек является учредителем
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<OrgData[]> LoadOrgDataAsync(Person data)
            => Task.Run(() => LoadOrgData(data));

        /// <summary>
        ///     Поиск индивидуальных предпринимателей
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IpData[] LoadIpData(Person data)
        {
            return $"{urls["ogrn_online"]}/ип/?человек={data.id}&инн={data.inn}"
                .GetFormRequest()
                .FromJson<IpData[]>();
        }

        /// <summary>
        ///     Поиск индивидуальных предпринимателей
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<IpData[]> LoadIpDataAsync(Person data)
            => Task.Run(() => LoadIpData(data));

        /// <summary>
        ///     Поиск индивидуальных предпринимателей
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IpData[] LoadIpData(string ogrn)
        {
            return $"{urls["ogrn_online"]}/ип/?огрнип={ogrn}"
                .GetFormRequest()
                .FromJson<IpData[]>();
        }

        /// <summary>
        ///     Поиск индивидуальных предпринимателей
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<IpData[]> LoadIpDataAsync(string ogrn)
            => Task.Run(() => LoadIpData(ogrn));

        public static VedModel QueryCustomsVed(string inn, int? year = null)
        {
            year = year ?? DateTime.Now.Year.Dec();
            var lines = $"{urls["ved"]}/full/report_aj/?inn={inn}&base=import&do=zip&year={year}"
                .GetCsvWebRequest().ToList();
            $"{urls["ved"]}/full/report_aj/?inn={inn}&base=export&do=zip&year={year}"
                .GetCsvWebRequest().ToList().Skip(1).Action(lines.AddRange);

            return ParseCustomsVed(lines);
        }

        public static VedModel ParseCustomsVed(IEnumerable<string> lines)
        {
            var header = lines
                .First()
                .Instance<VedHeader>();

            var declarations = lines
                .Skip(1)
                .AsParallel()
                .Select(line =>
                {
                    try
                    {
                        return header.ParseLine(line);
                    }
                    catch
                    {
                        return null;
                    }
                })
                .WhereNotNull()
                .Where(v => (v.Inn.Length == 10) || (v.Inn.Length == 12));

            return new VedModel
            {
                Declarations = declarations
                    .AsParallel()
                    .GroupBy(v => v.Number)
                    .Select(v => v.First())
                    .Select(declaration => new VedDeclarationModel
                    {
                        Inn = declaration.Inn,
                        Value = declaration.Summ,
                        Number = declaration.Number,
                        Date = declaration.Date,
                        Country = declaration.Country,
                        Currency = declaration.Currency,
                        Curs = declaration.Curs,
                        Import = declaration.Import
                    })
                    .ToList(),
                Contacts = declarations
                    .AsParallel()
                    .GroupBy(declaration => declaration.Phone.DoRegexMatches(@"(\d)")
                        .Select(v => v.Groups[1].Value[0])
                        .ToArray()
                        .Instance<string>())
                    .Select(v => v.First())
                    .Select(contact => new VedContactModel
                    {
                        Inn = contact.Inn,
                        FIO = contact.FIO,
                        Post = contact.Post,
                        OrigPhone = contact.Phone,
                        Phone = contact.Phone
                    })
                    .ToList()
            };
        }
    }
}