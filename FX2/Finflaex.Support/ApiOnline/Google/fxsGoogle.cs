﻿using System.Collections.Generic;
using Finflaex.Support.ApiOnline.Google.Models;
using Finflaex.Support.ApiOnline.PostApi.Models;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

namespace Finflaex.Support.ApiOnline.PostApi
{
    public static class fxsGoogle
    {
        private static string ApiKey = "AIzaSyAHBWPV_QVZpX3-fnTdizZn_IzuGQ74pXg";

        private static fxcDictSO UrlList = new fxcDictSO()
        {
            ["google"] = "https://maps.googleapis.com/maps/api/"
        };

        public static IEnumerable<fxcGoogle> GoogleAddress(
                string address,
                string language = "ru")
            => address.IsNullOrWhiteSpace()
                ? new fxcGoogle[0]
                : UrlList
                    .GetString("google")
                    .Append("geocode/json")
                    .Append($"?language={language}")
                    .Append($"&address={address.UrlEncode()}")
                    .GetFormRequest()
                    .FromJson<GoogleClass>()
                    .results;

        private class GoogleClass
        {
            public IEnumerable<fxcGoogle> results { get; set; }
            public string status { get; set; }
        }

        //distancematrix/json?
        //origins=Vancouver+BC|Seattle&
        //destinations=San+Francisco|Victoria+BC&mode=bicycling&
        //language=fr-FR
        //&key=AIzaSyAHBWPV_QVZpX3-fnTdizZn_IzuGQ74pXg
        public static DistanceMatrix DistanceAddress(
                string adr1,
                string adr2,
                string language = "ru")
            => adr1.IsNullOrWhiteSpace()
                .Or(adr2.IsNullOrWhiteSpace)
                ? null
                : UrlList
                    .GetString("google")
                    .Append("distancematrix/json")
                    .Append($"?language={language}")
                    .Append($"&origins={adr1.UrlEncode()}")
                    .Append($"&destinations={adr2.UrlEncode()}")
                    .Append($"&key={ApiKey}")
                    .GetFormRequest()
                    .FromJson<DistanceMatrix>();
    }
}
