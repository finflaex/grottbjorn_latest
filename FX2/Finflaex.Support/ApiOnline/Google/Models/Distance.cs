﻿namespace Finflaex.Support.ApiOnline.Google.Models
{
    public class Distance
    {
        public string text { get; set; }
        public int value { get; set; }
    }
}