﻿namespace Finflaex.Support.ApiOnline.Google.Models
{
    public class fxcGoogle
    {
        public Address_Components[] address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public string[] types { get; set; }
    }

    public class Geometry
    {
        public Bounds bounds { get; set; }
        public Bound location { get; set; }
        public string location_type { get; set; }
        public Bounds viewport { get; set; }
    }

    public class Bounds
    {
        public Bound northeast { get; set; }
        public Bound southwest { get; set; }
    }

    public class Bound
    {
        public float lat { get; set; }
        public float lng { get; set; }
    }

    public class Address_Components
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }

}
