﻿namespace Finflaex.Support.ApiOnline.Google.Models
{
    public class Duration
    {
        public string text { get; set; }
        public int value { get; set; }
    }
}