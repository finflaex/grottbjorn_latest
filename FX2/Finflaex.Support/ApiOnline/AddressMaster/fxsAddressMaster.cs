﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.ApiOnline.PostApi;
using Finflaex.Support.ApiOnline.PostApi.Models;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

namespace Finflaex.Support.ApiOnline.AddressMaster
{
    public static class fxsAddressMaster
    {
        private static fxcDictSO UrlList = new fxcDictSO()
        {
            ["addressmaster"] = "http://addressmaster.ru/AutoCompleteServlet?term="
        };

        public static IEnumerable<Addresslist> ParseAddress(
                string address)
            => address.IsNullOrWhiteSpace()
                ? new Addresslist[0]
                : UrlList
                    .GetString("addressmaster")
                    .Append(address)
                    .GetFormRequest()
                    .FromJson<Rootobject>()
                    .addressList;
    }


    public class Rootobject
    {
        public Addresslist[] addressList { get; set; }
    }

    public class Addresslist
    {
        public string ZIPCODE { get; set; }
        public string ADDRESS_ID { get; set; }
        public HOUSE_INFO HOUSE_INFO { get; set; }
        public string MATCH_STATUS { get; set; }
        public ADDRESS_INFO[] ADDRESS_INFO { get; set; }
        public string ADDRESS_GUID { get; set; }

        public string FormatAddress
        {
            get
            {
                return ADDRESS_INFO
                    .OrderBy(v => v.level)
                    .Select(v => $"{v.elementTypeName}. {v.formalName}")
                    .Join(", ")
                    .Prepend(HOUSE_INFO.IsNull() ? "" : $"{HOUSE_INFO.ZIPCODE}, ")
                    .Append(HOUSE_INFO.IsNull() ? "" : $" дом. {HOUSE_INFO.HOUSE_NUMBER}");
            }
        }
    }

    public class HOUSE_INFO
    {
        public string ZIPCODE { get; set; }
        public string HOUSE_GUID { get; set; }
        public string HOUSE_NUMBER { get; set; }
        public string HOUSE_ID { get; set; }
    }

    public class ADDRESS_INFO
    {
        public Guid aoId { get; set; }
        public int level { get; set; }
        public string elementTypeName { get; set; }
        public Guid aoGuid { get; set; }
        public string formalName { get; set; }
        public string allPostalCodes { get; set; }
        public string zipCode { get; set; }
    }

}
