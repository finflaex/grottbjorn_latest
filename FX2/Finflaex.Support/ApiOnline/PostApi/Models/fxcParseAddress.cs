﻿namespace Finflaex.Support.ApiOnline.PostApi.Models
{
    public class fxcParseAddress
    {
        public string ZIPCODE { get; set; }
        public string ADDRESS_ID { get; set; }
        public fxcParseHouse HOUSE_INFO { get; set; }
        public string MATCH_STATUS { get; set; }
        public ADDRESS_INFO[] ADDRESS_INFO { get; set; }
        public string ADDRESS_GUID { get; set; }
    }

    public class fxcParseHouse
    {
        public string ZIPCODE { get; set; }
        public string HOUSE_GUID { get; set; }
        public string HOUSE_NUMBER { get; set; }
        public string HOUSE_ID { get; set; }
        public string BUILDING_NUMBER { get; set; }
    }

    public class ADDRESS_INFO
    {
        public string aoId { get; set; }
        public int level { get; set; }
        public string elementTypeName { get; set; }
        public string aoGuid { get; set; }
        public string formalName { get; set; }
        public string allPostalCodes { get; set; }
    }


    public class fxcParseAddressDetails
    {
        public string OKTMO { get; set; }
        public string HOUSE_NUMBER { get; set; }
        public object IFNSUL { get; set; }
        public object FIAS_ID { get; set; }
        public string BUILDING_NUMBER { get; set; }
        public object OKATO { get; set; }
        public string FIAS_GUID { get; set; }
        public string KLADR { get; set; }
        public object IFNSFL { get; set; }
        public string ZIPCODE { get; set; }
        public object STRUCTURE_STATUS_SHORT_DESC { get; set; }
        public object STRUCTURE_STATUS_LONG_DESC { get; set; }
        public object TERR_IFNSFL { get; set; }
        public string STRUCTURE { get; set; }
    }


}
