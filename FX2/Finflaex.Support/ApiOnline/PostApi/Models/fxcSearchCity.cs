﻿namespace Finflaex.Support.ApiOnline.PostApi.Models
{
    public class fxcSearchCity
    {
        public string status { get; set; }
        public string access { get; set; }
        public string access_status { get; set; }
        public string executed_time { get; set; }
        public Source source { get; set; }
        public Content[] content { get; set; }
    }

    public class Source
    {
        public string city { get; set; }
        public string limit { get; set; }
        public string highlight { get; set; }
        public string full_search { get; set; }
        public string ex { get; set; }
        public string cf { get; set; }
    }

    public class Content
    {
        public Address region { get; set; }
        public Address area { get; set; }
        public Address city { get; set; }
        public string print_string { get; set; }
        public string cityid { get; set; }
    }

    public class Address
    {
        public string formalname { get; set; }
        public string shortname { get; set; }
        public string offname { get; set; }
        public string aolevel { get; set; }
        public string aoguid { get; set; }
        public string _long { get; set; }
        public string lat { get; set; }
        public Rect rect { get; set; }
    }

    public class Rect
    {
        public string lb_long { get; set; }
        public string lb_lat { get; set; }
        public string ru_long { get; set; }
        public string ru_lat { get; set; }
    }
}
