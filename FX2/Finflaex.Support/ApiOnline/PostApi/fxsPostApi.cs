﻿using System.Collections.Generic;
using Finflaex.Support.ApiOnline.PostApi.Models;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

namespace Finflaex.Support.ApiOnline.PostApi
{
    public static class fxsPostApi
    {
        private const string post_api_key = "bczr93bfi4c6bfrl";

        private static fxcDictSO UrlList = new fxcDictSO()
        {
            ["addressmaster"] = "http://www.addressmaster.ru/",
            ["post-api"] = "http://post-api.ru/api/",
        };

        public static fxcSearchCity SearchCity(
                string name,
                int limit = 10)
            => UrlList
                .GetString("post-api")
                .Append("v2/cbp.php")
                .Append($"?city={name.UrlEncode()}")
                .Append($"&apikey={post_api_key}")
                .Append($"&limit={limit}&full=1&hl=0&ex=1&cf=1").GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<fxcSearchCity>();

        public static IEnumerable<fxcParseAddress> ParseAddress(
            string address)
            => UrlList
                .GetString("addressmaster")
                .Append("AutoCompleteServlet")
                .Append($"?term={address.UrlEncode()}")
                .GetFormRequest()
                .FromJson<DataParseAddress>()
                .addressList;

        public static IEnumerable<fxcParseAddressDetails> ParseAddressDetails(
            string address_id = "", 
            string house_id = "",
            string range_id = "")
            => UrlList
                .GetString("addressmaster")
                .Append("AddressDetailsServlet")
                .Append($"?addressId={address_id}")
                .Append($"&houseId={house_id}")
                .Append($"&rangeId={range_id}")
                .GetFormRequest()
                .FromJson<DataParseAddressDetails>()
                .ADDRESS_DETAILS;

        public static IEnumerable<fxcParseHouse> ParseHouses(
                string address_id,
                int page_num = 0,
                int page_size = 300)
            => UrlList
                .GetString("addressmaster")
                .Append("AddressHousesServlet")
                .Append($"?addressId={address_id}")
                .Append($"&pageNum={page_num}")
                .Append($"&pageSize={page_size}")
                .FromJson<DataParseHouses>()
                .HOUSES;

        private class DataParseAddress
        {
            public IEnumerable<fxcParseAddress> addressList { get; set; }
        }

        private class DataParseHouses
        {
            public IEnumerable<fxcParseHouse> HOUSES { get; set; }
        }

        private class DataParseAddressDetails
        {
            public IEnumerable<fxcParseAddressDetails> ADDRESS_DETAILS { get; set; }
        }
    }
}
