﻿#region

using System.Collections.Generic;
using System.Linq;
using Finflaex.Support.ApiOnline.BasicData.Models;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.ApiOnline.BasicData
{
    public static class fxsZipCode
    {
        public const string url = @"http://basicdata.ru/api/json/zipcode/";

        public static fxcZipCode GetZipCodeInfo(object zipcode)
        {
            try
            {
                return $"{url}{zipcode}/"
                    .GetFormRequest()
                    .EncodeStringUnicode()
                    .FromJson<result>()
                    .data
                    .FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

        private class result
        {
            public IEnumerable<fxcZipCode> data { get; set; }
        }
    }
}