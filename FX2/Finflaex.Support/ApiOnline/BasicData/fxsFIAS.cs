﻿using System;
using System.Collections.Generic;
using Finflaex.Support.ApiOnline.BasicData.Models;
using Finflaex.Support.Reflection;

namespace Finflaex.Support.ApiOnline.BasicData
{
    public static class fxsFIAS
    {
        public static IEnumerable<fxcACTSTAT> GetACTSTAT()
        {
            return "http://basicdata.ru/api/json/fias/actstat/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcACTSTAT>>>()
                .data;
        }

        public static IEnumerable<fxcADDROBJ> GetADDROBJ(Guid? aoid)
        {
            var id = aoid == Guid.Empty ? string.Empty : aoid.ToString();
            return $"http://basicdata.ru/api/json/fias/addrobj/{id}"
                .GetFormRequest()
                .Replace("\\\"", "'")
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcADDROBJ>>>()
                .data;
        }

        public static IEnumerable<fxcCENTERST> GetCENTERST()
        {
            return "http://basicdata.ru/api/json/fias/centerst/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcCENTERST>>>()
                .data;
        }
        public static IEnumerable<fxcCURENTST> GetCURENTST()
        {
            return "http://basicdata.ru/api/json/fias/curentst/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcCURENTST>>>()
                .data;
        }

        public static IEnumerable<fxcESTSTAT> GetESTSTAT()
        {
            return "http://basicdata.ru/api/json/fias/eststat/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcESTSTAT>>>()
                .data;
        }

        public static IEnumerable<fxcHOUSE> GetHOUSE(Guid aoguid)
        {
            return "http://basicdata.ru/api/json/fias/house/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcHOUSE>>>()
                .data;
        }

        public static IEnumerable<fxcHOUSEINT> GetHOUSEINT(Guid aoguid)
        {
            return "http://basicdata.ru/api/json/fias/houseint/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcHOUSEINT>>>()
                .data;
        }

        public static IEnumerable<fxcHSTSTAT> GetHSTSTAT()
        {
            return "http://basicdata.ru/api/json/fias/hststat/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcHSTSTAT>>>()
                .data;
        }

        public static IEnumerable<fxcINTVSTAT> GetINTVSTAT()
        {
            return "http://basicdata.ru/api/json/fias/INTVSTAT/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcINTVSTAT>>>()
                .data;
        }

        public static IEnumerable<fxcLANDMARK> GetLANDMARK(Guid aoguid)
        {
            return "http://basicdata.ru/api/json/fias/LANDMARK/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcLANDMARK>>>()
                .data;
        }

        public static IEnumerable<fxcNORMDOC> GetNORMDOC(Guid aoguid)
        {
            return "http://basicdata.ru/api/json/fias/NORMDOC/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcNORMDOC>>>()
                .data;
        }

        public static IEnumerable<fxcOPERSTAT> GetOPERSTAT()
        {
            return "http://basicdata.ru/api/json/fias/OPERSTAT/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcOPERSTAT>>>()
                .data;
        }

        public static IEnumerable<fxcSOCRBASE> GetSOCRBASE()
        {
            return "http://basicdata.ru/api/json/fias/SOCRBASE/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcSOCRBASE>>>()
                .data;
        }

        public static IEnumerable<fxcSTRSTAT> GetSTRSTAT()
        {
            return "http://basicdata.ru/api/json/fias/STRSTAT/"
                .GetFormRequest()
                .EncodeStringUnicode()
                .FromJson<result<IEnumerable<fxcSTRSTAT>>>()
                .data;
        }

        private class result<T>
        {
            public T data { get; set; }
        }
    }
}