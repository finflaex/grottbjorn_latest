﻿using System.Collections.Generic;
using System.Linq;
using Finflaex.Support.ApiOnline.BasicData.Models;
using Finflaex.Support.Reflection;

namespace Finflaex.Support.ApiOnline.BasicData
{
    public static class fxsIFNS
    {
        public const string url = @"http://basicdata.ru/api/json/ifns/";

        public static fxcIFNS GetIfnsInfo(object zipcode)
        {
            try
            {
                return $"{url}{zipcode}/"
                    .GetFormRequest()
                    .EncodeStringUnicode()
                    .FromJson<result>()
                    .data
                    .FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

        private class result
        {
            public IEnumerable<fxcIFNS> data { get; set; }
        }
    }
}
