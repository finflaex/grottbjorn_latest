﻿namespace Finflaex.Support.ApiOnline.BasicData.Models
{
    public class fxcHOUSEINT
    {
        public string postalcode { get; set; }
        public string ifnsfl { get; set; }
        public string terrifnsfl { get; set; }
        public string ifnsul { get; set; }
        public string terrifnsul { get; set; }
        public string okato { get; set; }
        public string oktmo { get; set; }
        public string updatedate { get; set; }
        public int intstart { get; set; }
        public int intend { get; set; }
        public string houseintid { get; set; }
        public string intguid { get; set; }
        public string aoguid { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public int intstatus { get; set; }
        public string normdoc { get; set; }
        public int counter { get; set; }
    }

}
