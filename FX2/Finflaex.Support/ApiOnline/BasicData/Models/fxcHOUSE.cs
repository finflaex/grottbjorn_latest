﻿namespace Finflaex.Support.ApiOnline.BasicData.Models
{
    public class fxcHOUSE
    {
        public string postalcode { get; set; }
        public string ifnsfl { get; set; }
        public string terrifnsfl { get; set; }
        public string ifnsul { get; set; }
        public string terrifnsul { get; set; }
        public string okato { get; set; }
        public string oktmo { get; set; }
        public string updatedate { get; set; }
        public string housenum { get; set; }
        public int eststatus { get; set; }
        public string buildnum { get; set; }
        public string strucnum { get; set; }
        public int strstatus { get; set; }
        public string houseid { get; set; }
        public string houseguid { get; set; }
        public string aoguid { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public int statstatus { get; set; }
        public string normdoc { get; set; }
        public int counter { get; set; }
    }
}
