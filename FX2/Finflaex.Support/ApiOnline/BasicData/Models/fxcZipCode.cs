﻿namespace Finflaex.Support.ApiOnline.BasicData.Models
{
    public class fxcZipCode
    {
        public int zipcode { get; set; }
        public int number { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string dateUpdated { get; set; }
    }
}
