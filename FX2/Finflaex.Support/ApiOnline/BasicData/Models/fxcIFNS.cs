﻿namespace Finflaex.Support.ApiOnline.BasicData.Models
{
    /// <summary>
    /// rou - Информация о регистрирующем органе, на который возложены функции регистрации юридических лиц
    /// rof - Информация о регистрирующем органе, на который возложены функции регистрации индивидуальных предпринимателей
    /// </summary>
    public class fxcIFNS
    {
        /// <summary>
        /// код ИФНС
        /// </summary>
        public string ifns { get; set; }
        /// <summary>
        /// дополнительная информация
        /// </summary>
        public string comment { get; set; }
        /// <summary>
        /// наименование
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// ИНН получателя
        /// </summary>
        public string pay_inn { get; set; }
        /// <summary>
        /// КПП получателя
        /// </summary>
        public string pay_kpp { get; set; }
        /// <summary>
        /// получатель платежа
        /// </summary>
        public string pay_name { get; set; }
        /// <summary>
        /// банк получателя
        /// </summary>
        public string pay_bank_name { get; set; }
        /// <summary>
        /// БИК
        /// </summary>
        public string pay_bik { get; set; }
        /// <summary>
        /// корр.счет №
        /// </summary>
        public string pay_corraccount { get; set; }
        /// <summary>
        /// счет №
        /// </summary>
        public string pay_account { get; set; }
        /// <summary>
        /// адрес
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// телефон
        /// </summary>
        public string phone { get; set; }
        /// <summary>
        /// код
        /// </summary>
        public string code { get; set; }
        public string gni { get; set; }
        /// <summary>
        ///  дополнительная информация
        /// </summary>
        public string rof_comment { get; set; }
        /// <summary>
        /// наименование
        /// </summary>
        public string rof_name { get; set; }
        /// <summary>
        /// адрес
        /// </summary>
        public string rof_address { get; set; }
        /// <summary>
        /// телефон
        /// </summary>
        public string rof_phone { get; set; }
        /// <summary>
        /// код
        /// </summary>
        public string rof_code { get; set; }
        /// <summary>
        /// дополнительная информация
        /// </summary>
        public string rou_comment { get; set; }
        /// <summary>
        /// наименование
        /// </summary>
        public string rou_name { get; set; }
        /// <summary>
        /// адрес
        /// </summary>
        public string rou_address { get; set; }
        /// <summary>
        /// телефон
        /// </summary>
        public string rou_phone { get; set; }
        /// <summary>
        /// код
        /// </summary>
        public string rou_code { get; set; }
    }
}
