﻿namespace Finflaex.Support.ApiOnline.BasicData.Models
{
    public class fxcSOCRBASE
    {
        public int level { get; set; }
        public string scname { get; set; }
        public string socrname { get; set; }
        public int kod_t_st { get; set; }
    }

}
