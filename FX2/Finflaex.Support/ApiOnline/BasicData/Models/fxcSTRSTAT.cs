﻿namespace Finflaex.Support.ApiOnline.BasicData.Models
{
    public class fxcSTRSTAT
    {
        public int strstatid { get; set; }
        public string name { get; set; }
        public string shortname { get; set; }
    }

}
