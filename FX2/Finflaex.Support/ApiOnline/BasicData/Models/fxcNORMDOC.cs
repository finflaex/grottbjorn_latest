﻿namespace Finflaex.Support.ApiOnline.BasicData.Models
{
    public class fxcNORMDOC
    {
        public string normdocid { get; set; }
        public string docname { get; set; }
        public string docdate { get; set; }
        public string docnum { get; set; }
        public int doctype { get; set; }
        public int docimgid { get; set; }
    }

}
