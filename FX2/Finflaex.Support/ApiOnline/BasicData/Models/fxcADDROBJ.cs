﻿namespace Finflaex.Support.ApiOnline.BasicData.Models
{
    public class fxcADDROBJ
    {
        public string aoid { get; set; }
        public string formalname { get; set; }
        public string regioncode { get; set; }
        public string autocode { get; set; }
        public string areacode { get; set; }
        public string citycode { get; set; }
        public string ctarcode { get; set; }
        public string placecode { get; set; }
        public string streetcode { get; set; }
        public string extrcode { get; set; }
        public string sextcode { get; set; }
        public string offname { get; set; }
        public string postalcode { get; set; }
        public string ifnsfl { get; set; }
        public string terrifnsfl { get; set; }
        public string ifnsul { get; set; }
        public string terrifnsul { get; set; }
        public string okato { get; set; }
        public string oktmo { get; set; }
        public string updatedate { get; set; }
        public string shortname { get; set; }
        public int aolevel { get; set; }
        public string parentguid { get; set; }
        public string aoguid { get; set; }
        public string previd { get; set; }
        public string nextid { get; set; }
        public string code { get; set; }
        public string plaincode { get; set; }
        public int actstatus { get; set; }
        public int centstatus { get; set; }
        public int operstatus { get; set; }
        public int currstatus { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string normdoc { get; set; }
    }
}
