﻿namespace Finflaex.Support.ApiOnline.BasicData.Models
{
    public class fxcLANDMARK
    {
        public string location { get; set; }
        public string postalcode { get; set; }
        public string ifnsfl { get; set; }
        public string terrifnsfl { get; set; }
        public string ifnsul { get; set; }
        public string terrifnsul { get; set; }
        public string okato { get; set; }
        public string oktmo { get; set; }
        public string updatedate { get; set; }
        public string landid { get; set; }
        public string landguid { get; set; }
        public string aoguid { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string normdoc { get; set; }
    }

}
