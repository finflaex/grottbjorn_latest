﻿using System;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

namespace Finflaex.Support.ApiOnline.BasicData
{
    public static class fxsProizvodstvCallendar
    {
        public const string url = @"http://basicdata.ru/api/json/calend/";

        private static fxcDictSO _data = null;

        private static fxcDictSO data
        {
            get
            {
                if (_data == null)
                {
                    try
                    {
                        _data = $"{url}"
                            .GetFormRequest()
                            .EncodeStringUnicode()
                            .FromJson<result>()
                            .data;
                    }
                    catch { }
                }

                return _data;
            }
        }

        public static TypeDay GetStatusDay(DateTime value)
        {
            var year = value.Year.ToString();
            var month = value.Month.ToString();
            var day = value.Day.ToString();

            var read_year = data?.GetJObjectToObject<fxcDictSO>(year);
            if (read_year == null) goto exit;
            var read_month = read_year?.GetJObjectToObject<fxcDictSO>(month);
            if (read_month == null) goto exit;
            var read_day = read_month?.GetJObjectToObject<Day>(day);
            if (read_day != null) return read_day.isWorking;

            exit:
            var dayweek = value.DayOfWeek.AsType<int>();
            switch (dayweek)
            {
                case 0:
                case 6:
                case 7:
                    return TypeDay.notWork;
                default:
                    return TypeDay.work;
            }
        }

        private class result
        {
            public fxcDictSO data { get; set; }
        }


        public class Day
        {
            public TypeDay isWorking { get; set; }
        }

        public enum TypeDay : int
        {
            work = 0,
            notWork = 2,
            shortDay = 3
        }
    }
}
