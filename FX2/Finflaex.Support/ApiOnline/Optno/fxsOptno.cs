﻿using System;
using System.Collections.Generic;
using Finflaex.Support.Reflection;

namespace Finflaex.Support.ApiOnline.Optno
{
    public class fxsOptno
    {
        private static Guid key = "b322399d-36e5-4008-a2dd-55a63afcff94".ToGuid();
        private const string url = "http://optno.ru/api/";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">название банка</param>
        /// <param name="bik">БИК банка</param>
        /// <param name="kc">кор.счет банка</param>
        /// <returns></returns>
        public static IEnumerable<fxcBankInfo> GetBankInfos(
                string name = null,
                string bik = null,
                string kc = null)
            => url
                .Append("banks/")
                .Append($"?value={name ?? bik ?? kc}")
                .Append("&kind=" + (name != null ? "name" : bik ?? kc))
                .Append("&type=json")
                .Append($"&user_id={key:D}")
                .GetFormRequest()
                .FromJson<IEnumerable<fxcBankInfo>>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone">Номер телефона (6-10 цифр)</param>
        /// <returns></returns>
        public static IEnumerable<fxcPhoneInfo> GetPhoneInfos(
                string phone)
            => url
                .Append("phones/")
                .Append($"?value={phone}")
                .Append("&type=json")
                .Append($"&user_id={key:D}")
                .GetFormRequest()
                .FromJson<IEnumerable<fxcPhoneInfo>>();

        public static IEnumerable<fxcAddressInfo> GetAddressInfos(
                string postcode = "",
                string ifns = "",
                string okato = "")
            => url
                .Append("address/")
                .Append($"?postcode={postcode}")
                .Append($"?ifns={ifns}")
                .Append($"?okato={okato}")
                .Append("&type=json")
                .Append($"&user_id={key:D}")
                .GetFormRequest()
                .FromJson<IEnumerable<fxcAddressInfo>>();

        public static IEnumerable<fxcAddressInfo> GetAddressInfos(
                string name,
                string parent_id = "",
                int count = 5)
            => url
                .Append("addressfull/")
                .Append($"?name={name}")
                .Append($"?parent_id={parent_id}")
                .Append($"?count={count}")
                .Append("&type=json")
                .Append($"&user_id={key:D}")
                .GetFormRequest()
                .FromJson<IEnumerable<fxcAddressInfo>>();

        public static fxcDateInfo GetDateInfos(
                DateTime data)
            => url
                .Append("calendar/")
                .Append($"?present_date={data:dd.MM.yyyy}")
                .Append("&type=json")
                .Append($"&user_id={key:D}")
                .GetFormRequest()
                .FromJson<fxcDateInfo>();

        public static fxcIfnsInfo GetIfnsInfos(
                string code)
            => url
                .Append("ifns/")
                .Append($"?code={code}")
                .Append("&type=json")
                .Append($"&user_id={key:D}")
                .GetFormRequest()
                .FromJson<fxcIfnsInfo>();

        public static IEnumerable<fxcOkved1Info> GetOkved1Infos(
                string code)
            => url
                .Append("okved/")
                .Append($"?code={code}")
                .Append("&type=json")
                .Append($"&user_id={key:D}")
                .GetFormRequest()
                .FromJson<IEnumerable<fxcOkved1Info>>();

        public static string GetRubToString(
                string num)
            => url
                .Append("numerals/")
                .Append($"?num={num}")
                .Append("&type=json")
                .Append($"&user_id={key:D}")
                .GetFormRequest()
                .Trim('"');

        //todo Очистка текста от персональных данных
    }


    public class fxcOkved1Info
    {
        public string code { get; set; }
        public string name { get; set; }
        public string comment { get; set; }
    }


    public class fxcIfnsInfo
    {
        public string code { get; set; }
        public string name { get; set; }
        public string gni { get; set; }
        public string spro_f { get; set; }
        public string spro_u { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string comment { get; set; }
        public string recipient_inn { get; set; }
        public string recipient_kc { get; set; }
        public string recipient_bank { get; set; }
        public string recipient_kpp { get; set; }
        public string recipient_name { get; set; }
        public string recipient_rc { get; set; }
        public string rof_name { get; set; }
        public string rof_address { get; set; }
        public string rof_code { get; set; }
        public string rof_phone { get; set; }
        public string rof_comment { get; set; }
        public string rou_name { get; set; }
        public string rou_address { get; set; }
        public string rou_code { get; set; }
        public string rou_phone { get; set; }
        public string rou_comment { get; set; }
        public DateTime date_update { get; set; }
    }


    public class fxcDateInfo
    {
        public DateTime present_date { get; set; }
        public string event_date { get; set; }
        public string tax_event { get; set; }
        public string professional_holiday { get; set; }
        public string fiveday_week { get; set; }
        public string sixday_week { get; set; }
    }



    public class fxcAddressInfo
    {
        public int id { get; set; }
        public string address { get; set; }
        public string postcode { get; set; }
        public string ifns { get; set; }
        public string okato { get; set; }
        public string oktmo { get; set; }
    }


    public class fxcPhoneInfo
    {
        public string min_number { get; set; }
        public string max_number { get; set; }
        public string operator_name { get; set; }
    }


    public class fxcBankInfo
    {
        public string name { get; set; }
        public string city_name { get; set; }
        public string bik { get; set; }
        public string kc { get; set; }
        public string address { get; set; }
        public string rkc { get; set; }
        public string phone { get; set; }
        public string okpo { get; set; }
        public string status { get; set; }
        public DateTime date_update { get; set; }
    }

}
