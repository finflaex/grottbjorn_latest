﻿#region

using System.Collections.Generic;
using System.Linq;
using Finflaex.Support.Types;

#endregion

namespace Finflaex.Support.JSON
{
    public class fxcWebixDataTable : fxcDictSO
    {
        public enum type_select
        {
            row
        }

        private List<object> _columns;
        private List<object> _data;
        private readonly fxcDictSO _event;

        public fxcWebixDataTable()
        {
            this["view"] = "datatable";
            this["columns"] = _columns = new List<object>();
            this["data"] = _data = new List<object>();
            this["on"] = _event = new fxcDictSO();
        }

        public object id
        {
            set { this["id"] = value; }
            get { return this["id"]; }
        }

        public type_select select
        {
            set { this["select"] = value.ToString(); }
        }

        public bool blockselect
        {
            set { this["blockselect"] = value; }
        }

        public bool resizeColumn
        {
            set { this["resizeColumn"] = value; }
        }

        public bool headermenu
        {
            set { this["headermenu"] = value; }
        }

        public bool multiselect
        {
            set { this["multiselect"] = value; }
        }

        public IEnumerable<object> columns
        {
            get { return _columns; }
            set { _columns = value?.ToList() ?? new List<object>(); }
        }

        public IEnumerable<object> data
        {
            get { return _data; }
            set { _data = value?.ToList() ?? new List<object>(); }
        }

        public object on => _event;

        public string onItemDblClick
        {
            set { _event["onItemDblClick"] = value.AsJsonFunction(); }
        }

        public fxcWebixDataTable AddColumn(fxcWebixColumnTable column)
        {
            _columns.Add(column);
            return this;
        }

        public fxcWebixDataTable ClearColumns()
        {
            _columns.Clear();
            return this;
        }

        public fxcWebixDataTable AddItem(object item)
        {
            _columns.Add(item);
            return this;
        }

        public fxcWebixDataTable ClearData()
        {
            _columns.Clear();
            return this;
        }
    }
}