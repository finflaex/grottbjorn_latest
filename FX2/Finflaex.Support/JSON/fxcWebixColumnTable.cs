﻿#region

using System.Collections.Generic;
using Finflaex.Support.Types;

#endregion

namespace Finflaex.Support.JSON
{
    public class fxcWebixColumnTable : fxcDictSO
    {
        public enum type_adjust
        {
            both
        }

        public enum type_filter
        {
            multiSelectFilter,
            textFilter,
            numberFilter
        }

        public enum type_sort
        {
            @string,
            @int
        }

        private readonly List<object> _header;

        public fxcWebixColumnTable()
        {
            this["header"] = _header = new List<object>();
        }

        public object id
        {
            set { this["id"] = value; }
        }

        public bool headermenu
        {
            set { this["hedaermenu"] = value; }
        }

        public string caption
        {
            set { _header.Add(value); }
        }

        public type_adjust adjust
        {
            set { this["adjust"] = value.ToString(); }
        }

        public type_filter filter
        {
            set { _header.Add(new {content = value.ToString()}); }
        }

        public type_sort sort
        {
            set { this["sort"] = value.ToString(); }
        }

        public bool hidden
        {
            set { this["hidden"] = value; }
        }

        public bool fillspace
        {
            set { this["fillspace"] = value; }
        }

        public int minWidth
        {
            set { this["minWidth"] = value; }
        }

        public int width
        {
            set { this["width"] = value; }
        }
    }
}