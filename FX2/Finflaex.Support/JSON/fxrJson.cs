﻿#region

using System.Text.RegularExpressions;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.JSON
{
    public static class fxrJson
    {
        public static string ToJsonObject(this object value)
        {
            var shablon = "(\"/\\*<finflaex>)(.*?)(</finflaex>\\*/\")";
            var result = value.ToJson();
            Regex.Replace(result, shablon, "$2").ActionOut(out result);
            Regex.Replace(result, "(\\\\r\\\\n|\\\\r|\\\\n)", string.Empty).ActionOut(out result);
            Regex.Replace(result, "(constructor)", "$init").ActionOut(out result);
            return result;
        }

        public static string AsJsonFunction(this string script)
            => $"/*<finflaex>{script}</finflaex>*/";
    }
}