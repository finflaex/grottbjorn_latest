﻿#region

using System.Net;
using System.Net.Mail;

#endregion

namespace Finflaex.Support.Mail
{
    public class fxcMail
    {
        private readonly SmtpClient smtp;

        public fxcMail(string login, string pass, string host, int port = 25, bool ssl = false)
        {
            smtp = new SmtpClient
            {
                Host = host,
                Port = port,
                EnableSsl = ssl,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(login, pass)
            };
        }

        public void Send(string from, string to, string title, string body)
        {
            using (var message = new MailMessage(from, to)
            {
                Subject = title,
                Body = body
            })
            {
                lock (smtp)
                {
                    smtp.Send(message);
                }
            }
        }

        public void Send(MailAddress from, MailAddress to, string title, string body, bool html = false)
        {
            using (var message = new MailMessage(from, to)
            {
                Subject = title,
                Body = body,
                IsBodyHtml = html,
            })
            {
                lock (smtp)
                {
                    try
                    {
                        smtp.Send(message);
                    }
                    catch
                    {
                        //smtp.Host = "188.226.16.110";
                        //smtp.Send(message);
                    }
                }
            }
        }
    }
}