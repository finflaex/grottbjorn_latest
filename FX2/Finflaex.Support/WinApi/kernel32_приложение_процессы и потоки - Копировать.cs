﻿#region

using System;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace Finflaex.Support.WinApi
{
    partial class kernel32
    {
        /// <summary>
        ///     позволяет получить полный путь для указанного модуля.
        /// </summary>
        /// <param name="hModule">
        ///     Дескриптор модуля, чей путь мы хотим узнать.
        ///     Если этот параметр равен NULL, GetModuleFileName возвратит путь текущего модуля.
        /// </param>
        /// <param name="buffer">
        ///     Указатель на буфер, который будет содержать строку с полным путём модуля.
        ///     Если длина пути превышает размер, указанный в параметре length, то функция вернёт путь,
        ///     но он будет обрезан до length символа и может не содержать в конце нулевого символа.
        /// </param>
        /// <param name="length">Размер буфера</param>
        /// <returns>
        ///     Если функция выполнена успешно, то возвращаемое значение, это длина строки.
        ///     Если при выполнении функции возникнет ошибка, то возвращаемое значение будет равно нулю.
        /// </returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int GetModuleFileName(
            IntPtr hModule,
            StringBuilder buffer,
            int length);
    }
}