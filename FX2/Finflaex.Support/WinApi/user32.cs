﻿#region

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace Finflaex.Support.WinApi
{
    public static class user32
    {
        [DllImport("user32.dll")]
        public static extern IntPtr GetMenu(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern IntPtr GetSubMenu(IntPtr hMenu, int nPos);

        [DllImport("user32.dll")]
        public static extern int GetMenuItemCount(IntPtr hMenu);

        [DllImport("user32.dll")]
        public static extern int GetMenuString(IntPtr hMenu, uint uIDItem, StringBuilder lpString, int nMaxCount,
            uint uFlag);

        [DllImport("user32.dll")]
        public static extern uint GetMenuItemID(IntPtr hMenu, int nPos);

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("User32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern long GetWindowText(IntPtr hwnd, StringBuilder lpString, long cch);

        public static string GetCaptionOfWindow(IntPtr hwnd)
        {
            var caption = "";
            StringBuilder windowText = null;
            try
            {
                var max_length = GetWindowTextLength(hwnd);
                windowText = new StringBuilder("", max_length + 5);
                GetWindowText(hwnd, windowText, max_length + 2);

                if (!string.IsNullOrEmpty(windowText.ToString()) && !string.IsNullOrWhiteSpace(windowText.ToString()))
                    caption = windowText.ToString();
            }
            catch (Exception ex)
            {
                caption = ex.Message;
            }
            finally
            {
                windowText = null;
            }
            return caption;
        }

        public static IntPtr[] FindQuikWindow(string name)
        {
            var processes = Process.GetProcessesByName(name);
            var result = new IntPtr[processes.Length];

            for (var i = 0; i < processes.Length; i++)
                result[i] = processes[i].MainWindowHandle;

            return result;
        }
    }
}