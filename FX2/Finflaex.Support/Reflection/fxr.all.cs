﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Formatting = Newtonsoft.Json.Formatting;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        /// <summary>
        ///     Приводит объект к указанному типу
        /// </summary>
        /// <typeparam name="R">тип</typeparam>
        /// <param name="this">объект</param>
        /// <returns></returns>
        public static R AsType<R>(this object @this)
        {
            try
            {
                return (R) @this;
            }
            catch
            {
                return default(R);
            }
        }

        public static R CastType<R>(this object @this)
        {
            return @this.ToJson().FromJson<R>();
        }

        /// <summary>
        ///     Создает новый объект указанного типа с текущим объектом в качестве параметра
        /// </summary>
        /// <typeparam name="R">тип</typeparam>
        /// <param name="this">объект</param>
        /// <returns></returns>
        public static R Instance<R>(this object @this)
        {
            return Activator.CreateInstance(typeof(R), @this).AsType<R>();
        }

        public static TThis ActionOut<TThis>(this TThis @this, out TThis out_value)
        {
            out_value = @this;
            return @this;
        }

        public static R Instance<R>(this object @this, params object[] args)
        {
            var param = new List<object> {@this};
            param.AddRange(args);
            return Activator.CreateInstance(typeof(R), param.ToArray()).AsType<R>();
        }

        public static T Action<T>(this T @this, Action<T> action)
        {
            action.Invoke(@this);
            return @this;
        }

        public static T Action<T>(this T @this, Action action)
        {
            action.Invoke();
            return @this;
        }

        public static T Action<T, R>(this T @this, Func<T, R> func)
        {
            func.Invoke(@this);
            return @this;
        }

        //public static T Action<T>(this T @this, Func<T,T> action)
        //{
        //    action.Invoke(@this);
        //    return @this;
        //}

        //public static T Action<T, R>(this T @this, Func<R> action)
        //{
        //    action();
        //    return @this;
        //}

        public static T Action<T, V, R>(this T @this, V value, Func<V, R> action)
        {
            action(value);
            return @this;
        }

        public static T Action<T, V>(this T @this, V value, Action<V, T> action)
        {
            action(value, @this);
            return @this;
        }

        public static string ToJson(this object @this, bool format = false)
        {
            return JsonConvert.SerializeObject(@this, format ? Formatting.Indented : Formatting.None);
        }

        public static T FromJson<T>(this string @this, params object[] param)
        {
            var qwe = param.OfType<JsonConverter>().ToArray();

            //try
            //{
            //    var result = Activator.CreateInstance<T>();
            //    JsonConvert.PopulateObject(@this, result);
            //    return result.AsType<T>();
            //}
            //catch { }
            try
            {
                return @this.IsNullOrEmpty()
                    ? default(T)
                    : JsonConvert.DeserializeObject<T>(@this, qwe);
            }
            catch
            {

            }
            return default(T);
        }

        public static object FromJson(this string @this)
        {
            return JsonConvert.DeserializeObject(@this);
        }

        public static R IfNotNull<T, R>(this T @this, Func<T, R> func)
            where T : class
        {
            return @this == null ? default(R) : func(@this);
        }

        public static R FuncSelect<T, R>(this T @this, Func<T, R> func) => func.Invoke(@this);

        public static R FuncSelect<T, R>(this T @this, Func<R> func) => func.Invoke();

        public static T FuncWhere<T>(this T @this, bool flag, Func<T, T> func)
            => flag ? func.Invoke(@this) : @this;

        public static T FuncWhere<T>(this T @this, Func<T, bool> where, Func<T, T> func)
            => @this.FuncWhere(where(@this), func);

        public static T FuncIfNull<T>(this T @this, Func<T> func)
            => @this.IsNull().FuncSelect(func);

        public static R retSwitch<T, R>(this T @this, Dictionary<T, R> dictionary, R @default = default(R))
            => dictionary.ContainsKey(@this) ? dictionary[@this] : @default;

        public static T retIfNull<T>(
            this T @this,
            T default_value = default(T))
            where T : class
            => @this ?? default_value;


        public static R FuncIfFalse<R>(this bool @this, Func<R> func, R default_value = default(R))
            => @this ? default_value : func();

        public static R FuncIfTrue<R>(this bool @this, Func<R> func, R default_value = default(R))
            => @this ? func() : default_value;

        public static R FuncWhere<R>(this bool flag, Func<R> if_true = null, Func<R> if_false = null)
        {
            if (flag)
                return if_true.IsNull() ? default(R) : if_true();
            return if_false.IsNull() ? default(R) : if_false();
        }

        public static R FuncSelect<T, V, R>(this T @this, V param, Func<V, R> func) => func(param);
        public static R FuncSelect<T, V, R>(this T @this, V param, Func<T, V, R> func) => func(@this, param);
        public static T FuncSelect<T>(this T @this, T value) => value;
        public static T FuncSelect<T>(this object @this, T value) => value;

        public static T Clone<T>(this T @this)
            where T : class
        {
            if (@this == null) return null;
            return typeof(T)
                .GetMethods(BindingFlags.NonPublic | BindingFlags.Instance)
                .FirstOrDefault(v => v.Name == "MemberwiseClone")?
                .Invoke(@this, null)
                .AsType<T>();
        }

        /// <summary>
        /// Клонирование только простых свойств
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this"></param>
        /// <returns></returns>
        public static T CloneSimple<T>(this T @this)
            where T : class
        {
            if (@this == null) return null;

            var result = Activator.CreateInstance<T>();

            typeof(T)
                .GetProperties()
                .Where(v => v.PropertyType.IsPrimitive || v.PropertyType == typeof(string))
                .ForEach(v =>
                {
                    v.SetValue(result, v.GetValue(@this));
                });

            return result;
        }

        public static bool IsNull(this object @this)
        {
            return @this == null;
        }

        public static bool NotNull(this object @this)
        {
            return @this != null;
        }

        public static T ActionIfNull<T>(this T @this, Action action = null)
        {
            if (@this == null)
                action?.Invoke();
            return @this;
        }

        public static T ActionIfNotNull<T>(this T @this, Action action = null)
        {
            if (@this != null)
                action?.Invoke();
            return @this;
        }

        public static T ActionIfNotNull<T, R>(this T @this, Func<R> action = null)
        {
            if (@this != null)
                action?.Invoke();
            return @this;
        }

        public static T ActionIfNotNull<T>(this T @this, Action<T> action = null)
        {
            if (@this != null)
                action?.Invoke(@this);
            return @this;
        }

        public static T ActionIfNotNull<T, V>(this T @this, V value, Action<V> action = null)
        {
            if (@this != null)
                action?.Invoke(value);
            return @this;
        }

        public static T ActionIfNotNull<T, V, R>(this T @this, V value, Func<V, R> action = null)
        {
            if (@this != null)
                action?.Invoke(value);
            return @this;
        }

        public static R FuncIfNotNull<T, R>(this T @this, Func<T, R> action)
            where T : class
        {
            return @this != null ? action.Invoke(@this) : default(R);
        }


        public static T TryIfNull<T>(this T @this, string message = null)
        {
            if (@this == null)
                if (message == null)
                    throw new Exception();
                else
                    throw new Exception(message);
            return @this;
        }

        public static T TryIfNotNull<T>(this T @this, string message = null)
        {
            if (@this != null)
                if (message == null)
                    throw new Exception();
                else
                    throw new Exception(message);
            return @this;
        }

        public static T ActionWhere<T>(
            this T @this,
            bool where,
            Action<T> action,
            Action<T> else_action = null)
        {
            if (where)
                action?.Invoke(@this);
            else
                else_action?.Invoke(@this);
            return @this;
        }

        public static T ActionWhere<T, R>(
            this T @this,
            bool where,
            Func<T, R> action)
        {
            if (where)
                action.Invoke(@this);
            return @this;
        }

        public static T ActionWhere<T>(
            this T @this,
            Func<T, bool> where,
            Action<T> action,
            Action<T> else_action = null)
            => @this
                .ActionWhere(where(@this), action, else_action);

        public static T ActionWhere<T>(
            this T @this,
            Func<bool> where,
            Action<T> action,
            Action<T> else_action = null)
            => @this
                .ActionWhere(where(), action, else_action);

        public static T[] InArray<T>(this T @this)
            => new T[] {@this};

        public static List<T> InList<T>(this T @this)
            => new List<T> {@this};

        public static T ThrowIfNull<T>(
            this T @this,
            string message = "")
            => @this.ActionIfNull(() =>
            {
                throw new Exception(message);
            });

        public static bool AllNotNull(params object[] values)
            => values.All(v => v != null);

        public static bool OneNotNull(params object[] values)
            => values.Any(v => v != null);

        /// <summary>
        /// Возвращает ресурс который лежит в том же namespace по имени
        /// </summary>
        /// <param name="this"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static byte[] GetResource(
            this object @this,
            string name)
        {
            var asm = @this.GetType().Assembly;
            var obj_name_space = @this.GetType().Namespace ?? "none";
            var res_name = asm
                .GetManifestResourceNames()
                .Where(v => v.StartsWith(obj_name_space))
                .FirstOrDefault(v => v.ToLower().Contains(name.ToLower()));
            if (res_name == null)
            {
                return new byte[0];
            }
            return asm
                       .GetManifestResourceStream(res_name)?
                       .ToBytes()
                       .ToArray() ?? new byte[0];
        }

        public static byte[] GetResource<T>(string name)
        {
            var asm = typeof(T).Assembly;
            var res_name = asm
                .GetManifestResourceNames()
                .FirstOrDefault(v => v.ToLower().Contains(name.ToLower()));
            if (res_name == null)
            {
                return new byte[0];
            }
            return asm
                       .GetManifestResourceStream(res_name)?
                       .ToBytes()
                       .ToArray() ?? new byte[0];
        }

        /// <summary>
        /// Возвращает ресурс который лежит в том же namespace по имени
        /// </summary>
        /// <param name="this"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static byte[][] GetResources(
            this object @this,
            string name)
        {
            var asm = @this.GetType().Assembly;
            var obj_name_space = @this.GetType().Namespace ?? "none";
            return asm
                .GetManifestResourceNames()
                .Where(v => v.StartsWith(obj_name_space))
                .Where(v => v.ToLower().Contains(name.ToLower()))
                .Select(v => asm.GetManifestResourceStream(v).ToBytes())
                .ToArray();
        }
    }
}