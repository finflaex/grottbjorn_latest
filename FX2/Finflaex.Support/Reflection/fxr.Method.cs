﻿using System;
using System.Threading.Tasks;

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static Task<R> ToTask<R>(
            this Func<object[], R> act,
            params object[] param)
            => Task.Run(() => act(param));

        public static Task<object> ToTask(
            this Func<object[], object> act,
            params object[] param)
            => Task.Run(() => act(param));
    }
}