﻿#region

using System;
using Finflaex.Support.Service;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static void AsUser(this Action @this, string login, string domen, string password)
        {
            using (new fxcImpersonator(login, domen, password))
            {
                @this?.Invoke();
            }
        }

        public static R AsUser<R>(this Func<R> @this, string login, string domen, string password)
        {
            using (new fxcImpersonator(login, domen, password))
            {
                return @this.Invoke();
            }
        }
    }
}