﻿#region

using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static IQueryable<T> Include<T, R>(this DbSet<T> @this, Expression<Func<T, R>> act)
            where T : class
        {
            return @this.AsType<IQueryable<T>>().Include(act);
        }

        public static DbSet<T> Truncate<T>(this DbSet<T> @this)
            where T : class
        {
            @this.RemoveRange(@this);
            return @this;
        }
    }
}