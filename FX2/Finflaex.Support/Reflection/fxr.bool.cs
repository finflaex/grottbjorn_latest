﻿#region

using System;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static bool And(this bool @this, Func<bool> func)
            => @this.And(func.Invoke());

        public static bool And(this bool @this, bool value)
            => @this && value;

        public static bool Or(this bool @this, Func<bool> func)
            => @this.Or(func.Invoke());

        public static bool Or(this bool @this, bool value)
            => @this || value;

        public static bool ExcludingOr(this bool @this, Func<bool> func)
            => @this.ExcludingOr(func.Invoke());

        public static bool ExcludingOr(this bool @this, bool value)
            => @this ^ value;

        public static bool FFalseLTrue(this bool @this, bool value)
            => !@this && value;

        public static bool FFalseLTrue(this bool @this, Func<bool> func)
            => @this.FFalseLTrue(func.Invoke());

        public static bool IfTrue(this bool @this, Action action)
        {
            if (@this) action?.Invoke();
            return @this;
        }

        public static bool IfTrue(this bool? @this, bool default_value = false, Action action = null)
        {
            var read = @this ?? default_value;
            return read.IfTrue(action);
        }

        public static bool IfFalse(this bool @this, Action action)
        {
            if (!@this) action?.Invoke();
            return @this;
        }

        public static bool IfFalse(this bool? @this, bool default_value = false, Action action = null)
        {
            var read = @this ?? default_value;
            return read.IfFalse(action);
        }

        public static TResult SelectSingleIf<TResult>(
            this bool @this,
            Func<TResult> if_true = null,
            Func<TResult> if_false = null)
            where TResult : class
        => @this
            ? if_true?.Invoke()
            : if_false?.Invoke();

        public static bool OrNot(this bool @this, bool value)
            => @this || !value;

        public static bool AndNot(this bool @this, bool value)
            => @this && !value;

        public static bool Case(this bool @this, bool flag = false, Action act = null)
        {
            if (!@this && flag) act?.Invoke();
            return @this || flag;
        }

        public static bool Case(this bool @this, Func<bool> flag = null, Action act = null)
        {
            var result = flag?.Invoke() ?? false;
            if (!@this && result) act?.Invoke();
            return @this || result;
        }

        public static bool CaseAllNotNull(
                this bool @this,
                object val1 = null,
                object val2 = null,
                Action act = null)
            => @this.Case(AllNotNull(val1, val2), act);
    }
}