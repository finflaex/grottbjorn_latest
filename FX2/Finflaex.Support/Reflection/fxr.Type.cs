﻿#region

using System;
using System.Linq;
using System.Security.Policy;
using System.Web.Mvc;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static T CreateInstance<T>(
            this Type @this,
            params object[] param) => Activator
            .CreateInstance(@this, param)
            .AsType<T>();

        public static bool Is<TType>(this Type @this) => @this == typeof(TType);

        public static object Create(this Type @this) => Activator.CreateInstance(@this);
    }
}