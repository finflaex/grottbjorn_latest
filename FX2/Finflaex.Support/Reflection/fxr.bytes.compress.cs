﻿#region

using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static byte[] CompressGZip(this byte[] @this)
        {
            using (var result = new MemoryStream())
            {
                using (var target = new MemoryStream(@this))
                {
                    using (var compress = new GZipStream(result, CompressionLevel.Optimal))
                    {
                        target.CopyTo(compress);
                    }
                    return result.ToArray();
                }
            }
        }

        public static byte[] DecompressGZip(this byte[] @this)
        {
            using (var result = new MemoryStream())
            {
                using (var target = new MemoryStream(@this))
                {
                    using (var compress = new GZipStream(target, CompressionMode.Decompress))
                    {
                        compress.CopyTo(result);
                    }
                    return result.ToArray();
                }
            }
        }

        public static byte[] CompressDeflate(this byte[] @this)
        {
            using (var result = new MemoryStream())
            {
                using (var target = new MemoryStream(@this))
                {
                    using (var compress = new DeflateStream(result, CompressionLevel.Optimal))
                    {
                        target.CopyTo(compress);
                    }
                    return result.ToArray();
                }
            }
        }

        public static byte[] DecompressDeflate(this byte[] @this)
        {
            using (var result = new MemoryStream())
            {
                using (var target = new MemoryStream(@this))
                {
                    using (var compress = new DeflateStream(target, CompressionMode.Decompress))
                    {
                        compress.CopyTo(result);
                    }
                    return result.ToArray();
                }
            }
        }

        public static string ToBase64(this byte[] @this)
        {
            return Convert.ToBase64String(@this, Base64FormattingOptions.None);
        }

        public static byte[] FromBase64(this string @this)
        {
            return @this
                .Replace('-', '+')
                .Replace('_', '/')
                .FuncSelect(s =>
                {
                    switch (s.Length%4)
                    {
                        default:
                            return s;
                        case 2:
                            return s + "==";
                        case 3:
                            return s + "=";
                    }
                })
                .FuncSelect(Convert.FromBase64String);
        }

        public static byte[] Encrypt(this byte[] value, string password = "finflaex")
        {
            byte[] Results;
            var UTF8 = new UTF8Encoding();
            var HashProvider = new MD5CryptoServiceProvider();
            var TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(password));
            var TDESAlgorithm = new TripleDESCryptoServiceProvider
            {
                Key = TDESKey,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            try
            {
                var Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(value, 0, value.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return Results;
        }

        public static byte[] Decrypt(this byte[] value, string password = "finflaex")
        {
            byte[] Results;
            var UTF8 = new UTF8Encoding();
            var HashProvider = new MD5CryptoServiceProvider();
            var TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(password));
            var TDESAlgorithm = new TripleDESCryptoServiceProvider
            {
                Key = TDESKey,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            try
            {
                var Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = value.NotNull()
                    ? Decryptor.TransformFinalBlock(value, 0, value.Length)
                    : new byte[0];
            }
            catch
            {
                return new byte[0];
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return Results;
        }

        public static byte[] Sign(
            this byte[] @this,
            X509Certificate2 cert,
            Oid algoritm = null)
        {
            var contentInfo = new ContentInfo(@this);
            var signedCms = new SignedCms(contentInfo);
            var cmsSigner = new CmsSigner(cert);
            if (algoritm != null)
                cmsSigner.DigestAlgorithm = algoritm;

            signedCms.ComputeSignature(cmsSigner);
            return signedCms.Encode();
        }

        public static byte[] UnSign(
            byte[] @this)
        {
            var signedCms = new SignedCms();
            try
            {
                signedCms.Decode(@this);
                return signedCms.ContentInfo.Content;
            }
            catch (CryptographicException)
            {
                return null;
            }
        }
    }
}