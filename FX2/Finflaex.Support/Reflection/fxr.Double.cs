﻿#region

using System;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static DateTime ToDateTime(this double self)
        {
            return DateTime.FromOADate(self);
        }

        public static bool Compare(this double self, double value)
        {
            return Math.Abs(self - value) < 0.0000000001;
        }

        public static long ToLong(this double self)
        {
            return Convert.ToInt64(self);
        }

        public static decimal ToDecimal(this double self)
        {
            return Convert.ToDecimal(self);
        }

        public static int ToInt(this double self)
        {
            return Convert.ToInt32(self);
        }

        public static float ToFloat(this double self)
        {
            return Convert.ToSingle(self);
        }

        public static bool CompareExt<T>(this T self, T value)
        {
            return typeof(T) == typeof(double)
                ? Math.Abs(self.AsType<double>() - value.AsType<double>()) < 0.0000000001
                : typeof(T) == typeof(decimal)
                    ? Math.Abs(self.AsType<decimal>() - value.AsType<decimal>()) < 0.0000001M
                    : typeof(T) == typeof(float)
                        ? Math.Abs(self.AsType<float>() - value.AsType<float>()) < 0.0000001
                        : typeof(T) == typeof(string)
                            ? self.AsType<string>() == value.AsType<string>()
                            : self.Equals(value);
        }
    }
}