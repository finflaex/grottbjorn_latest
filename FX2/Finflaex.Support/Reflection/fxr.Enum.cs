﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        private static string GetCustomDescription(object objEnum)
        {
            var fi = objEnum.GetType().GetField(objEnum.ToString());
            var attributes = (DescriptionAttribute[]) fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : objEnum.ToString();
        }

        public static string Desc(this Enum value)
        {
            return GetCustomDescription(value);
        }

        /// <summary>
        ///     Сравнивает числовые значения
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool Is<T>(this Enum type, T value)
        {
            try
            {
                return (long) (object) type == (long) (object) value;
            }
            catch
            {
                return false;
            }
        }

        public static List<T> EnumToList<T>()
            where T : struct
        {
            var qwe = typeof(T).Create();
            if (qwe is Enum)
                return Enum.GetValues(typeof(T)).Cast<T>().ToList();
            throw new Exception("Указанный тип не является перечеслением");
        }
    }
}