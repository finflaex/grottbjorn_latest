﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Hosting;
using Finflaex.Support.MVC.Enum;
using Finflaex.Support.Types;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static bool FileExists(
            this string @this)
        {
            return File.Exists(@this);
        }

        public static bool DirectoryExists(
            this string @this)
        {
            return Directory.Exists(@this);
        }

        public static string GetDirectory(this string @this)
        {
            return Path.GetDirectoryName(@this);
        }

        public static string GetFName(this string @this)
        {
            return Path.GetFileNameWithoutExtension(@this);
        }

        public static string GetFExt(this string @this)
        {
            return Path.GetExtension(@this).RemoveStart(".");
        }

        public static string ToFile(this string @this, string path)
        {
            File.WriteAllText(path, @this);
            return @this;
        }

        public static string GetFormRequest(this string @this, Encoding encoding = null, fxcDictSO data = null)
        {
            try
            {
                var uri = @this;
                query:
                var request = WebRequest.Create(uri).AsType<HttpWebRequest>();
                request.UserAgent =
                    @"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36";

                request.Credentials = CredentialCache.DefaultCredentials;

                if (data != null)
                {
                    request.Method = "POST";
                    var form = data.Select(v => $"{v.Key}={v.Value}").Join("&").ToBytes(Encoding.ASCII);
                    request.ContentType = ContentTypes.ApplicationXWWWFormUrlencoded;
                    request.ContentLength = form.Length;
                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(form, 0, form.Length);
                    }
                }

                var response = request.GetResponse();
                if (response.Headers.AllKeys.Select(v => v.ToLower()).Contains("referer"))
                {
                    uri = response.Headers["Referer"];
                    goto query;
                }
                var dataStream = response.GetResponseStream();
                var reader = new StreamReader(dataStream, encoding ?? Encoding.UTF8);
                return reader.ReadToEnd();
            }
            catch (Exception err)
            {
                return string.Empty;
            }
        }

        public static byte[] GetWebRequest(this string @this, Action<HttpWebRequest> act = null, byte[] data = null)
        {
            try
            {
                var uri = @this;
                query:
                var request = WebRequest.Create(uri).AsType<HttpWebRequest>();
                //request.UserAgent =
                //    @"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36";

                //request.Credentials = CredentialCache.DefaultCredentials;

                if (data != null)
                {
                    request.Method = "POST";
                    request.ContentLength = data.Length;
                }

                act?.Invoke(request);

                if (data != null)
                {
                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                }

                var response = request.GetResponse();
                if (response.Headers.AllKeys.Select(v => v.ToLower()).Contains("referer"))
                {
                    uri = response.Headers["Referer"];
                    goto query;
                }
                return response.GetResponseStream().ReadBytes();
            }
            catch (Exception err)
            {
                return new byte[0];
            }
        }

        public static string[] GetCsvWebRequest(this string @this)
        {
            try
            {
                var request = WebRequest.Create(@this);
                request.Credentials = CredentialCache.DefaultCredentials;
                var response = request.GetResponse();
                return response.GetResponseStream().ReadLines().ToArray();
            }
            catch (Exception err)
            {
                return new string[0];
            }
        }

        public static string CreateDirectory(this string @this)
        {
            var directory = @this.GetDirectoryName();
            Directory.CreateDirectory(directory);
            return @this;
        }

        public static string ServerMapPath(this string @this)
        {
            return HostingEnvironment.MapPath(@this);
        }

        public static string WriteNewFile(this string @this, byte[] data)
        {
            using (var file = new FileStream(@this, FileMode.CreateNew))
            {
                file.Write(data, 0, data.Length);
            }
            return @this;
        }

        public static string WriteNewFile(this string @this, Stream data)
        {
            using (var file = new FileStream(@this, FileMode.CreateNew))
            {
                data.CopyTo(file);
            }
            return @this;
        }

        public static IEnumerable<string> ReadLines(
            this string @this,
            Encoding encoding = null)
        {
            using (var file = new FileStream(@this, FileMode.Open, FileAccess.Read))
            {
                using (var reader = encoding == null
                    ? new StreamReader(file)
                    : new StreamReader(file, encoding))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                        yield return line;
                }
            }
        }
    }
}