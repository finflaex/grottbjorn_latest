﻿using System;
using System.Data;
using System.IO;
using Excel;

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static DataSet ReadExcel(this string @this)
        {
            var ext = @this.GetFileExtension();

            var stream = File.Open(@this, FileMode.Open, FileAccess.Read);

            var reader = ext == "xls"
                ? ExcelReaderFactory.CreateBinaryReader(stream)
                : ext == "xlsx"
                    ? ExcelReaderFactory.CreateOpenXmlReader(stream)
                    : null;

            reader.ActionIfNull(() =>
            {
                throw new Exception("unknown format");
            });

            var result = reader.AsDataSet(true);

            reader.Close();

            return result;
        }

        public static DataSet XlsToDataSet(this Stream @this)
        {
            var reader = ExcelReaderFactory.CreateBinaryReader(@this).ThrowIfNull("unknown");
            var result = reader.AsDataSet(true);
            reader.Close();
            return result;
        }

        public static DataSet XlsxToDataSet(this Stream @this)
        {
            var reader = ExcelReaderFactory.CreateOpenXmlReader(@this).ThrowIfNull("unknown");
            var result = reader.AsDataSet(true);
            reader.Close();
            return result;
        }
    }
}