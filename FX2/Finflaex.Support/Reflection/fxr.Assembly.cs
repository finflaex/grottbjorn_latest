﻿#region

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static byte[] GetResource(
                this Assembly @this,
                string contains)
            => @this
                .GetManifestResourceNames()
                .Action(list =>
                {
                    
                })
                .FirstOrDefault(v => v.Contains(contains))?
                .FuncSelect(name => @this
                    .GetManifestResourceStream(name)
                    .CopyIn(new MemoryStream())
                    .ToArray());

        public static IEnumerable<byte[]> GetResources(
                this Assembly @this,
                string contains)
            => @this
                .GetManifestResourceNames()
                .Where(v => v.Contains(contains))
                .Select(name => @this
                    .GetManifestResourceStream(name)
                    .CopyIn(new MemoryStream())
                    .ToArray());

        public static IEnumerable<Resource> ListResources(this Assembly @this)
            => @this
                .GetManifestResourceNames()
                .Select(name => new Resource
                {
                    name = name,
                    value = @this
                        .GetManifestResourceStream(name)
                        .CopyIn(new MemoryStream())
                        .ToArray()
                });
    }

    public class Resource
    {
        public string name { get; set; }
        public byte[] value { get; set; }
    }
}