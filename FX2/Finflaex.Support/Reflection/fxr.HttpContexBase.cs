﻿#region

using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using Finflaex.Support.MVC;
using Finflaex.Support.MVC.Atributes;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        //public static T SetAuth<T>(
        //    this HttpContextBase @this,
        //    T data = null,
        //    bool persistent = false)
        //    where T : fxaAuth
        //{
        //    var result = data ?? Activator.CreateInstance<T>();

        //    var ticket = new FormsAuthenticationTicket(
        //        1,
        //        FormsAuthentication.FormsCookieName,
        //        DateTime.Now,
        //        persistent ? DateTime.MaxValue : DateTime.Now.Add(FormsAuthentication.Timeout),
        //        persistent,
        //        result.ToJson(),
        //        FormsAuthentication.FormsCookiePath);

        //    @this?.Response.Cookies.Set(new HttpCookie(FormsAuthentication.FormsCookieName)
        //    {
        //        Value = FormsAuthentication.Encrypt(ticket),
        //        Expires = ticket.Expiration,
        //        Shareable = false
        //    });

        //    return result;
        //}

        public static T SetFinflaexAuth<T>(
            this HttpContextBase @this,
            T data = null,
            bool persistent = false,
            TimeSpan? time = null,
            string password = null)
            where T : fxaAuth
        {
            time = time ?? FormsAuthentication.Timeout;
            data = data ?? Activator.CreateInstance<T>();
            try
            {
                data.Time = DateTime.Now.Add(time.Value);

                if (@this != null)
                {
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName)
                    {
                        Value = data.ToJson().ToBytes(Encoding.UTF8).Encrypt(password ?? "finflaex").ToBase64(),
                        Expires = data.Time,
                        Shareable = false,
                        Secure = false
                    };

                    if (@this.Response.Cookies.AllKeys.Contains(FormsAuthentication.FormsCookieName))
                    {
                        @this.Response.Cookies.Set(cookie);
                    }
                    else
                    {
                        @this.Response.Cookies.Add(cookie);
                    }
                }
            }
            catch
            {
                
            }

            return data;
        }

        public static T GetFinflaexAuth<T>(
            this HttpContextBase @this,
            string password = null)
            where T : fxaAuth
        {
            var cook = @this
                .Request
                .Cookies
                .Get(FormsAuthentication.FormsCookieName);
            try
            {
                return cook?.Value.IsNullOrEmpty() ?? true
                    ? @this.SetFinflaexAuth<T>()
                    : cook.Value
                        .FromBase64()
                        .Decrypt(password ?? "finflaex")
                        .ToString(Encoding.UTF8)
                        .FromJson<T>();
            }
            catch (Exception err)
            {
                return default(T);
            }
        }

        //public static T GetAuth<T>(this HttpContextBase @this)
        //    where T : fxaAuth
        //{
        //    HttpCookie cook = null;
        //    cook = @this.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
        //    try
        //    {
        //        return cook?.Value.IsNullOrEmpty() ?? true
        //            ? @this.SetAuth<T>()
        //            : FormsAuthentication.Decrypt(cook.Value)
        //                .UserData
        //                .FromJson<T>();
        //    }
        //    catch
        //    {
        //        return @this.GetFinflaexAuth<T>();
        //    }
        //}

        //public static bool VerifyAuth(
        //    this HttpContextBase @this,
        //    RolesAttribute role)
        //{
        //    var cook = @this.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
        //    try
        //    {
        //        return cook?.Value.IsNullOrEmpty() ?? true
        //            ? role.Anonymous
        //            : FormsAuthentication
        //                .Decrypt(cook.Value)
        //                .UserData
        //                .FromJson<fxaAuth>()?
        //                .Verificate(role) ?? true;
        //    }
        //    catch
        //    {
        //        return @this.VerifyFinflaexAuth(role);
        //    }
        //}

        public static bool VerifyFinflaexAuth(
            this HttpContextBase @this,
            RolesAttribute role,
            string password = null)
        {
            var cook = @this.Request
                .Cookies.Get(FormsAuthentication.FormsCookieName);
            return cook?.Value.IsNullOrEmpty() ?? true
                ? role.Anonymous
                : cook.Value
                    .FromBase64()
                    .Decrypt(password ?? "finflaex")
                    .ToString(Encoding.UTF8)
                    .FromJson<fxaAuth>()?
                    .Verificate(role)
                    ?? false;
        }

        public static Exception GetError(
            this HttpContext @this)
        {
            return @this.Server.GetLastError();
        }

        public static HttpContext SetCache<T>(
            this HttpContext @this,
            string name,
            T value,
            int minuts)
        {
            @this.Cache.Add(name,
                value,
                null,
                Cache.NoAbsoluteExpiration,
                TimeSpan.FromMinutes(minuts),
                CacheItemPriority.Normal,
                null
            );
            return @this;
        }

        public static T GetCache<T>(
            this HttpContext @this,
            string name)
        {
            return @this.Cache[name].AsType<T>();
        }
    }
}