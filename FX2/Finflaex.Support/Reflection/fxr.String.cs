﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using Finflaex.Support.Types;
using RazorEngine;
using Encoding = System.Text.Encoding;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static bool NotNullOrEmpty(this string @this)
        {
            return !@this.IsNullOrEmpty();
        }

        public static bool NotNullOrWhiteSpace(this string @this)
        {
            return !@this.IsNullOrWhiteSpace();
        }

        public static bool Equal(this string @this, string value)
        {
            return string.Equals(@this, value);
        }

        /// <summary>
        ///     InvariantCulture IgnoreCase
        /// </summary>
        /// <param name="this"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool EqualICIC(this string @this, string value)
        {
            return string.Equals(@this, value, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string RemoveStart(this string @this, string start)
        {
            return @this.StartsWith(start)
                ? @this.Remove(0, start.Length)
                : @this;
        }

        public static string RemoveEnd(this string @this, string end)
        {
            return @this.EndsWith(end)
                ? @this.Remove(@this.Length - end.Length)
                : @this;
        }

        public static string FirstUp(this string @this)
        {
            return @this.IsNullOrEmpty()
                ? string.Empty
                : @this[0].ToString().ToUpperInvariant()
                  + @this.Substring(1).ToLowerInvariant();
        }

        public static string FirstOnlyDown(this string @this)
        {
            return @this.IsNullOrEmpty()
                ? string.Empty
                : @this[0].ToString().ToLowerInvariant()
                  + @this.Substring(1);
        }

        public static Match[] DoRegexMatches(this string @this, string pattern, RegexOptions options = RegexOptions.None)
        {
            return new Regex(pattern, options)
                .Matches(@this)
                .Cast<Match>()
                .ToArray();
        }

        public static bool DoRegexIsMatch(this string @this, string pattern, RegexOptions options = RegexOptions.None)
        {
            return Regex.IsMatch(@this, pattern, options);
        }

        public static byte[] ToBytes_BlockCopy(this string value)
        {
            var bytes = new byte[value.Length*sizeof(char)];
            Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string ToString_BlockCopy(this byte[] bytes)
        {
            var characters = new char[bytes.Length/sizeof(char)];
            Buffer.BlockCopy(bytes, 0, characters, 0, bytes.Length);
            return new string(characters).Trim((char) 0);
        }

        public static string ToString(this byte[] @this,
            Encoding encoding)
        {
            if (@this == null)
            {
                return null;
            }
            if (encoding.Equals(Encoding.UTF8)
                && (@this.Length > 2)
                && (@this[0] == 0xEF)
                && (@this[1] == 0xBB)
                && (@this[2] == 0xBF))
                @this = @this.Skip(3).ToArray();
            var result = encoding.GetString(@this);
            return result;
        }

        public static byte[] ToBytes(this string @this, Encoding encoding)
        {
            return encoding.GetBytes(@this);
        }

        public static string UrlDecode(this string self, Encoding encoding = null)
        {
            if (encoding == null)
                return HttpUtility.UrlDecode(self);
            return HttpUtility.UrlDecode(self, encoding);
        }

        public static string UrlEncode(this string self, Encoding encoding = null)
        {
            if (encoding == null)
                return HttpUtility.UrlEncode(self);
            return HttpUtility.UrlEncode(self, encoding);

        }

        public static string HtmlDecode(this string self)
            => HttpUtility
                .HtmlDecode(self);

        public static string HtmlEncode(this string self)
            => HttpUtility
                .HtmlEncode(self);

        public static string UrlTokenEncode(this byte[] @this)
            => HttpServerUtility.UrlTokenEncode(@this);

        public static byte[] UrlTokenDecode(this string @this)
            => HttpServerUtility.UrlTokenDecode(@this);

        public static bool IsNullOrEmpty(this string self)
            => string
                .IsNullOrEmpty(self);

        public static bool IsNullOrWhiteSpace(this string self)
            => string
                .IsNullOrWhiteSpace(self);

        public static string IsNullOrEmpty(this string self,
                Func<string> func)
            => self
                .IsNullOrEmpty()
                ? func?.Invoke()
                : self;

        public static string IsNotNullOrWhiteSpace(this string self,
                Func<string> func)
            => string
                .IsNullOrWhiteSpace(self)
                ? self
                : func?.Invoke();

        public static string IsNotNullOrEmpty(this string self,
                Func<string> func)
            => self
                .IsNullOrEmpty()
                ? self
                : func?.Invoke();

        public static bool IsNotNullOrWhiteSpace(
            this string @this,
            Action<string> func = null)
        {
            var result = string.IsNullOrWhiteSpace(@this);
            if (!result) func?.Invoke(@this);
            return !result;
        }

        public static string retIfNullOrWhiteSpace(
                this string @this,
                string value = "")
            => string.IsNullOrWhiteSpace(@this) ? value : @this;

        public static bool IsNotNullOrEmpty(
            this string @this,
            Action<string> func = null)
        {
            var result = string.IsNullOrEmpty(@this);
            if (!result) func?.Invoke(@this);
            return result;
        }

        public static string IsNullOrWhiteSpace(this string self,
                Func<string> func)
            => string
                .IsNullOrWhiteSpace(self)
                ? func?.Invoke()
                : self;

        /// <summary>
        ///     извлекает полный путь к каталогу из переданного пути
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string GetDirectoryName(this string self)
        {
            return self
                .IsNullOrEmpty()
                ? string.Empty
                : Path.GetDirectoryName(self);
        }

        public static string GetServerDirectoryName(this string self)
        {
            var path = self.Replace("\\", "/");
            path = HttpContext.Current.Server.MapPath(path);
            return path.GetDirectoryNameEx();
        }

        /// <summary>
        ///     Расширенная версия добавляет слеш в конец при проверке
        /// </summary>
        /// <param name="this"></param>
        /// <returns>Результат без слеша в конце</returns>
        public static string GetDirectoryNameEx(this string @this)
        {
            var path = @this
                .GetFileExtension()
                .IsNullOrEmpty()
                ? (@this.EndsWith("\\") ? @this : @this.Append("\\"))
                : @this;

            var result = path.GetDirectoryName();

            return result ?? (
                       path.IsNullOrEmpty()
                           ? string.Empty
                           : path.Length <= 1
                               ? string.Empty
                               : (path.EndsWith("\\")
                                   ? path.Remove(path.Length - 1)
                                   : path)
                   );
        }

        public static string GetFileNameWithoutExtension(this string self)
            => self.IsNullOrEmpty() ? string.Empty : Path.GetFileNameWithoutExtension(self);

        public static string GetFileExtension(this string self)
            => self.IsNullOrEmpty() ? string.Empty : Path.GetExtension(self)?.TrimStart('.');

        public static string Encrypt(this string value, string password = "finflaex")
        {
            try
            {
                byte[] Results;
                var UTF8 = new UTF8Encoding();
                var HashProvider = new MD5CryptoServiceProvider();
                var TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(password));
                var TDESAlgorithm = new TripleDESCryptoServiceProvider
                {
                    Key = TDESKey,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };
                var DataToEncrypt = UTF8.GetBytes(value);
                try
                {
                    var Encryptor = TDESAlgorithm.CreateEncryptor();
                    Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
                }
                finally
                {
                    TDESAlgorithm.Clear();
                    HashProvider.Clear();
                }
                return Convert.ToBase64String(Results);
            }
            catch
            {
                return "";
            }
        }

        public static string Decrypt(this string value, string password = "finflaex")
        {
            try
            {
                byte[] Results;
                var UTF8 = new UTF8Encoding();
                var HashProvider = new MD5CryptoServiceProvider();
                var TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(password));
                var TDESAlgorithm = new TripleDESCryptoServiceProvider();
                TDESAlgorithm.Key = TDESKey;
                TDESAlgorithm.Mode = CipherMode.ECB;
                TDESAlgorithm.Padding = PaddingMode.PKCS7;
                var DataToDecrypt = Convert.FromBase64String(value);
                try
                {
                    var Decryptor = TDESAlgorithm.CreateDecryptor();
                    Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
                }
                finally
                {
                    TDESAlgorithm.Clear();
                    HashProvider.Clear();
                }
                return UTF8.GetString(Results);
            }
            catch
            {
                return "";
            }
        }

        public static string ToBase64(this string self)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(self));
        }

        public static Guid ToGuid(this string self) => self.ToGuid(Guid.Empty);

        public static DateTime? ToDateTime(this string self, params string[] mask)
        {
            try
            {
                return mask.Length == 0
                ? DateTime.Parse(self)
                : DateTime.ParseExact(self, mask, CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
            catch
            {
                return null;
            }
        }

        public static int ToInt(this string self)
        {
            return int.Parse(self);
        }

        public static decimal ToDecimal(this string self)
        {
            self = self
                .Replace(",", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator)
                .Replace(".", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);
            try
            {
                return decimal.Parse(self);
            }
            catch
            {
                decimal result = 0;
                decimal.TryParse(self, NumberStyles.Float, CultureInfo.CurrentCulture, out result);
                return result;
            }
        }

        public static long ToLong(this string self)
        {
            return self.IsNullOrEmpty() ? 0 : long.Parse(self);
        }

        public static string TrimEnd(this string self, string shablon)
        {
            var start = self.Length - shablon.Length;
            var inner = self.Substring(start);
            return inner == shablon
                ? self.Remove(start)
                : self;
        }

        public static T ToEnum<T>(this string self)
            where T : struct
        {
            var test = Activator.CreateInstance(typeof(T));
            if (test is Enum)
                try
                {
                    return Enum.Parse(typeof(T), self).AsType<T>();
                }
                catch (ArgumentException)
                {
                    var list = EnumToList<T>();
                    foreach (var item in list)
                    {
                        var flag = typeof(T)
                            .GetField(item.ToString())
                            .GetCustomAttributes(typeof(EnumParserAttribute), false)
                            .OfType<EnumParserAttribute>()
                            .Select(v => v.Name)
                            .Contains(self);

                        if (flag) return item;
                    }
                    return 0.AsType<T>();
                }
            throw new Exception("Указанный тип не является перечислением");
        }

        public static Stream ToStream(this string self)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(self);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static Regex ToRegex(this string self)
        {
            return new Regex(self);
        }

        public static string ToXml(this object self, params object[] param)
        {
            var nameSpace = param.OfType<XmlSerializerNamespaces>().FirstOrDefault();
            var ident = param.OfType<bool>().FirstOrDefault();
            var encoding = param.OfType<Encoding>().FirstOrDefault() ?? Encoding.UTF8;
            var act = param.OfType<Func<XmlWriter, XmlWriter>>().FirstOrDefault();

            var xmlSerializer = new XmlSerializer(self.GetType());

            var config = new XmlWriterSettings
            {
                Encoding = encoding,
                Indent = ident
            };

            var result = new StringBuilder();

            using (var writer = XmlWriter.Create(result, config))
            {
                var my = act?.Invoke(writer);
                if (nameSpace == null)
                    xmlSerializer.Serialize(my ?? writer, self);
                else
                    xmlSerializer.Serialize(my ?? writer, self, nameSpace);
            }

            return result.ToString().Replace("utf-16", "utf-8");

            //using (StringWriter textWriter = new fxcUtf8StringWriter())
            //{
            //    if (nameSpace != null)
            //    {
            //        xmlSerializer.Serialize(textWriter, self, nameSpace);
            //    }
            //    else
            //    {
            //        xmlSerializer.Serialize(textWriter, self);
            //    }
            //    return textWriter.ToString();
            //}
        }

        public static T FromXml<T>(this string self)
        {
            var serializer = new XmlSerializer(typeof(T));
            return serializer.Deserialize(self.ToStream()).AsType<T>();
        }

        public static string WriteInFile(this string self, string path)
        {
            using (var file = new StreamWriter(path))
            {
                var buf = self.ToCharArray();
                file.Write(buf, 0, buf.Length);
            }
            return self;
        }

        public static string ReadFromFile(this string self)
        {
            using (var file = new StreamReader(self))
            {
                return file.ReadToEnd();
            }
        }

        public static int LevenshteinDistance(this string self, string value)
        {
            if ((self == null) || (value == null)) return 0;
            var m = new int[self.Length + 1, value.Length + 1];

            for (var i = 0; i <= self.Length; i++)
                m[i, 0] = i;
            for (var j = 0; j <= value.Length; j++)
                m[0, j] = j;

            for (var i = 1; i <= self.Length; i++)
                for (var j = 1; j <= value.Length; j++)
                {
                    var diff = self[i - 1] == value[j - 1] ? 0 : 1;

                    m[i, j] = Math.Min(Math.Min(m[i - 1, j] + 1,
                            m[i, j - 1] + 1),
                        m[i - 1, j - 1] + diff);
                }
            return m[self.Length, value.Length];
        }

        public static double CompareExt(this string str1, string str2)
        {
            var z = str1.Length - str2.Length;
            if (z < 0)
                for (var i = z; i < 0; i++)
                    str1 += " ";
            if (z > 0)
                for (var i = z; i > 0; i--)
                    str2 += " ";
            var len = str1.Length;
            if (len != str2.Length)
                throw new NotSupportedException("Этот метод пока не поддерживает сравнение разных по длине строк.");
            if (len == 0)
                throw new ArgumentException("Строка не должна быть пустой.");
            var equalChars = 0;
            for (var i = 0; i < len; i++)
                if (str1[i] == str2[i])
                    equalChars++;
            return (double) equalChars/len;
        }

        public static string Append(this string self, object value)
            => self + value.ToString().IsNullOrEmpty(() => string.Empty);

        public static string Append(this string @this, Func<object> act)
            => @this.Append(act());

        public static string AppendWhere(this string self, bool flag, object value)
            => flag
                ? $"{self}{value}"
                : self;

        public static string AppendWhere(this string self, bool flag, Func<string> act)
            => flag
                ? $"{self}{act()}"
                : self;

        public static string Prepend(this string self, string value)
            => value.IsNullOrEmpty(() => string.Empty) + self;

        public static string Repeat(this string self, int count)
        {
            var result = new StringBuilder();
            count.For(index => result.Append(self));
            return result.ToString();
        }

        public static string CompleteAfter(this string self, string value, int count)
        {
            var cr = (count - (double) self.Length)/value.Length;
            return self + value.Repeat(cr.ToInt());
        }

        public static string CompleteBefore(this string self, string value, int count)
        {
            var cr = (count - (double) self.Length)/value.Length;
            return value.Repeat(cr.ToInt()) + self;
        }

        public static string Complete(this string self, string before, string after, int count)
        {
            var cr = (count - self.Length - before.Length - after.Length)/2;
            return before + " ".Repeat(cr) + self + " ".Repeat(cr) + after;
        }

        public static string CompleteBoth(this string self, string value, int count)
        {
            var cr = (count - self.Length)/value.Length/2;
            return value.Repeat(cr) + self + value.Repeat(cr);
        }

        public static string Both(this string self, string value)
        {
            return value + self + value;
        }

        public static string Align(this string self, int count, string before = null)
        {
            var result = new StringBuilder();
            while (self.Length > count)
            {
                result.AppendLine((before ?? string.Empty) + self.Substring(0, count));
                self = self.Substring(count);
            }
            result.Append((before ?? string.Empty) + self);
            return result.ToString();
        }

        public static string[] ToLines(this string self, StringSplitOptions options = StringSplitOptions.None)
        {
            return self.Split(new[] {"\r\n"}, options);
        }

        public static ActionResult ToActionResult(this string self)
        {
            return new ContentResult
            {
                Content = self
            };
        }

        public static bool Compare(this string @this, string value, bool IgnoreCase = true)
        {
            return
                IgnoreCase
                    ? string.Equals(@this, value, StringComparison.CurrentCultureIgnoreCase)
                    : @this == value;
        }

        public static string RazorParseText(this string template, object model = null)
        {
            return model == null ? Razor.Parse(template) : Razor.Parse(template, model);
        }

        public static string RazorParseFile(this string path, object model = null)
        {
            var template =
                main.ServerPath(path).ReadFromFile();
            return template.RazorParseText(model);
        }

        //public static string ToJsMin(this string @this)
        //{
        //    try
        //    {
        //        return Minifiers.JavaScript.Compress(@this);
        //    }
        //    catch
        //    {
        //        return @this;
        //    }
        //}

        //public static string ToCssMin(this string @this)
        //{
        //    try
        //    {
        //        return Minifiers.Css.Compress(@this);
        //    }
        //    catch
        //    {
        //        return @this;
        //    }
        //}

        //public static string ToHtmlMin(this string @this)
        //{
        //    try
        //    {
        //        var min = Minifiers.HtmlAdvanced.AsType<HtmlCompressor>();
        //        //min.RemoveComments = true;
        //        return min.Compress(@this);
        //    }
        //    catch
        //    {
        //        return @this;
        //    }
        //}

        public static string Wrap(this string @this, string value)
            => $"{value}{@this}{value}";
        public static string TagWrap(this string @this, string tagName, fxcDictSO attr = null)
        {
            var result = "";
            if (attr != null)
            {
                return attr
                    .Select(v => $"{v.Key}=\"{v.Value.ToString()}\"")
                    .Join(" ")
                    .Prepend($"<{tagName} ")
                    .Append($" >{@this}</{tagName}>");
            }
            return $"<{tagName}>{@this}</{tagName}>";
        }

        public static string StyleTagWrap(this string @this)
        {
            return @this.TagWrap("style");
        }

        public static string ScriptTagWrap(this string @this)
        {
            return @this.TagWrap("script");
        }

        public static string Replace(
            this string @this,
            Dictionary<string, string> data)
        {
            return @this == null
                ? null
                : data
                    .Aggregate(@this, (current, pair)
                        => Regex.Replace(current, pair.Key, pair.Value));
        }

        public static double ToDouble(this string @this)
        {
            try
            {
                return double.Parse(@this);
            }
            catch
            {
                return 0;
            }
        }

        public static string ClearExcept(this string @this, string pattern)
        {
            var result = @this.ToArray();
            var list = pattern.ToArray();

            return result.Where(v => list.Contains(v)).ToArray().FuncSelect(v => new string(v));
        }

        public static string Clear(this string @this, string pattern)
        {
            var result = @this.ToArray();
            var list = pattern.ToArray();

            return result.Where(v => !list.Contains(v)).ToArray().FuncSelect(v => new string(v));
        }

        public static string DiffHtml(this string @this, string value)
        {
            return HtmlDiff.HtmlDiff.Execute(@this, value);
        }

        public static string FromHtml(
            this string html)
        {
            html = html.retIfNullOrWhiteSpace();
            html = Regex.Replace(html, "<!--.*?-->", string.Empty);
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);

            var root = doc.DocumentNode;
            var sb = new StringBuilder();
            foreach (var node in root.DescendantNodesAndSelf())
            {
                if (!node.HasChildNodes)
                {
                    string text = node.InnerText;
                    if (!string.IsNullOrEmpty(text))
                        sb.AppendLine(text.Trim());
                }
            }

            return sb.ToString();
        }

        public static string ToHtml(this string text)
        {
            text = WebUtility.HtmlEncode(text);
            text = text.Replace("\r\n", "\r");
            text = text.Replace("\n", "\r");
            text = text.Replace("\r", "<br>\r\n");
            text = text.Replace("  ", " &nbsp;");
            return text;
        }
    }

    public class EnumParserAttribute : Attribute
    {
        public string Name { get; set; }
    }
}