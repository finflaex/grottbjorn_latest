﻿#region

using System.Threading.Tasks;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static T Await<T>(Task<T> @this) => @this.Result;
    }
}