﻿#region

using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using Finflaex.Support.MVC;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static string Controller(this ControllerContext @this)
            => @this.Value("controller");

        public static string Action(this ControllerContext @this)
            => @this.Value("action");

        public static string Value(this ControllerContext @this, string key)
        {
            var values = @this.RouteData.Values;
            return values.ContainsKey(key) ? values[key].AsType<string>() : null;
        }

        public static string GetCookie(this ControllerContext @this, string key)
        {
            return @this.HttpContext.Request.Cookies[key]?.Value;
        }

        public static void SetCookie(this ControllerContext @this, string key, string value)
        {
            Debug.Assert(@this.HttpContext.Response.Cookies != null, "@this.HttpContext.Response.Cookies != null");
            @this.HttpContext.Response.Cookies[key].Value = value;
        }

        public static T SetAuth<T>(
            this ControllerContext @this,
            T data = null,
            bool persistent = false)
            where T : class
        {
            return @this.SetAuth<T>();
        }

        public static T GetAuth<T>(this ControllerContext @this)
            where T : class
        {
            return @this.GetAuth<T>();
        }

        public static T ReadPostJson<T>(this ControllerContext @this)
        {
            var request = @this.HttpContext.Request;
            request.InputStream.Seek(0, SeekOrigin.Begin);
            var jsonStringData = new StreamReader(request.InputStream).ReadToEnd();
            return jsonStringData.FromJson<T>();
        }

        public static JsonResult JsonGet<T>(
            this Controller @this,
            T value)
        {
            return new JsonResult()
            {
                Data = value,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentEncoding = Encoding.UTF8,
            };
        }

        public static T DeserializeObject<T>(this Controller controller, string key) where T : class
        {
            var value = controller.HttpContext.Request.QueryString.Get(key);
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            return javaScriptSerializer.Deserialize<T>(value);
        }

        public static Guid GetSession(
            this Controller @this)
            => @this
                .HttpContext
                .GetFinflaexAuth<fxaAuth>()?
                .Session ?? Guid.Empty;

        public static fxaAuth GetAuthor(
                this Controller @this)
        {
            var result = @this.HttpContext.GetFinflaexAuth<fxaAuth>();
            if (result != null)
            {
                result.Time = DateTime.Now.Add(FormsAuthentication.Timeout);
            }
            @this.HttpContext.SetFinflaexAuth(result);
            return result;
        }
    }
}