﻿#region

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static Assembly ToAssembly(
                this byte[] @this)
            => Assembly.Load(@this);

        public static byte[] AsBytes<T>(this T @this)
        {
            if (@this == null) return null;
            using (var mem = new MemoryStream())
            {
                new BinaryFormatter().Serialize(mem, @this);
                return mem.ToArray();
            }
        }

        public static T AsObject<T>(this byte[] @this)
        {
            using (var mem = new MemoryStream(@this))
            {
                try
                {
                    return new BinaryFormatter().Deserialize(mem).AsType<T>();
                }
                catch
                {
                    return default(T);
                }
            }
        }

        public static bool[] ToBinary<T>(this T[] @this, Func<T, bool> compare)
        {
            var result = new bool[@this.Length];
            for (var x = 0; x < @this.Length; x++)
                result[x] = compare(@this[x]);
            return result;
        }

        public static bool[,] ToBinary<T>(this T[,] @this, Func<T, bool> compare)
        {
            var xl = @this.GetLength(0);
            var yl = @this.GetLength(1);
            var result = new bool[xl, yl];
            for (var x = 0; x < xl; x++)
                for (var y = 0; y < yl; y++)
                    result[x, y] = compare(@this[x, y]);
            return result;
        }

        public static T[,] ToLowerMeasurement<T>(
            this T[,,] @this,
            Func<T[], T> func,
            int im = 2)
        {
            var xl = @this.GetLength(im == 0 ? 1 : 0);
            var yl = @this.GetLength(im == 2 ? 1 : 2);
            var zl = @this.GetLength(im);
            var result = new T[xl, yl];
            for (var x = 0; x < xl; x++)
                for (var y = 0; y < yl; y++)
                    result[x, y] = Enumerable
                        .Range(0, zl)
                        .Select(z =>
                        {
                            switch (im)
                            {
                                case 0:
                                    return @this[z, x, y];
                                case 1:
                                    return @this[x, z, y];
                                default:
                                    return @this[x, y, z];
                            }
                        })
                        .ToArray()
                        .FuncSelect(func);
            return result;
        }

        public static T[] ToMono<T>(this T[,] @this)
        {
            var xl = @this.GetLength(0);
            var yl = @this.GetLength(1);
            var result = new T[@this.Length];
            for (var x = 0; x < xl; x++)
                for (var y = 0; y < yl; y++)
                    result[x*y] = @this[x, y];
            return result;
        }

        public static unsafe byte[] UnsafeCopy(this byte[] @this, int start = 0, int? count = null)
        {
            var size = count ?? @this.Length - start;
            var result = new byte[size];

            if ((@this == null) || (start < 0) || (size < 0) || (@this.Length < start + size))
                return new byte[0];

            // The following fixed statement pins the location of the src and dst objects
            // in memory so that they will not be moved by garbage collection.
            fixed (byte* pSrc = @this, pDst = result)
            {
                var ps = pSrc;
                var pd = pDst;

                // Loop over the count in blocks of 4 bytes, copying an integer (4 bytes) at a time:
                for (var i = 0; i < size/4; i++)
                {
                    *(int*) pd = *(int*) ps;
                    pd += 4;
                    ps += 4;
                }

                // Complete the copy by moving any bytes that weren't moved in blocks of 4:
                for (var i = 0; i < count%4; i++)
                {
                    *pd = *ps;
                    pd++;
                    ps++;
                }
            }

            return result;
        }

        private static unsafe bool UnsafeCompare(this byte[] a1, byte[] a2)
        {
            if ((a1 == null) || (a2 == null) || (a1.Length != a2.Length))
                return false;
            fixed (byte* p1 = a1, p2 = a2)
            {
                byte* x1 = p1, x2 = p2;
                var l = a1.Length;
                for (var i = 0; i < l/8; i++, x1 += 8, x2 += 8)
                    if (*(long*) x1 != *(long*) x2)
                        return false;
                if ((l & 4) != 0)
                {
                    if (*(int*) x1 != *(int*) x2) return false;
                    x1 += 4;
                    x2 += 4;
                }
                if ((l & 2) != 0)
                {
                    if (*(short*) x1 != *(short*) x2) return false;
                    x1 += 2;
                    x2 += 2;
                }
                if ((l & 1) != 0)
                    if (*x1 != *x2)
                        return false;
                return true;
            }
        }
    }
}