﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static Attribute[] GetAttributesOfProperty(this PropertyInfo self)
        {
            return Attribute.GetCustomAttributes(self);
        }

        public static object GetPropertyValue(this object self, string name)
           => self.GetType().GetRuntimeProperties().FirstOrDefault(v => v.Name == name)?.GetValue(self);

        public static object GetFieldValue(this object self, string name)
            => self.GetType().GetRuntimeFields().FirstOrDefault(v => v.Name == name)?.GetValue(self);

        public static IEnumerable<PropertyInfo> GetPropertiInfos<T>(this T self, params string[] names)
        {
            var type = self.GetType();
            var result = type.GetProperties();
            return names.Length > 0
                ? result.Where(v => names.Contains(v.Name))
                : result;
        }
    }
}