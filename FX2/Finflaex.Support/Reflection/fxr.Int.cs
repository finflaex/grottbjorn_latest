﻿#region

using System;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static int For(this int self, Action<int> action = null)
        {
            if (action != null)
                for (var i = 0; i < self; i++)
                    action(i);
            return self;
        }

        public static int For(this int self, Action action = null)
        {
            if (action != null)
                for (var i = 0; i < self; i++)
                    action();
            return self;
        }

        public static int Dec(this int self, int dec = 1)
        {
            return self - dec;
        }

        public static int Inc(this int self, int inc = 1)
        {
            return self + inc;
        }

        public static bool IsNoll(this int self)
            => self == 0;

        public static int IsNoll(this int self, Func<int> func)
            => self == 0 ? func?.Invoke() ?? 0 : self;
    }
}