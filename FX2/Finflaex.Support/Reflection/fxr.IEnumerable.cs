﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static void ForEach<T>(this IEnumerable<T> @this, Action<T> action)
        {
            if (@this != null)
            {
                foreach (var item in @this)
                    action(item);
            }
        }

        public static void ForEach<T>(this IEnumerable<T> @this, Action<int, T> action)
        {
            var forEach = @this as T[] ?? @this.ToArray();
            for (var i = 0; i < forEach.Length; i++)
                action(i, forEach[i]);
        }

        public static string Join(
            this IEnumerable<string> @this,
            string separator = null)
        {
            return string.Join($"{separator}", @this);
        }

        public static string Join(
            this IEnumerable<string> @this,
            char? separator = null)
        {
            return string.Join($"{separator}", @this);
        }

        public static IEnumerable<T> AddRange<T>(this IEnumerable<T> @this, IEnumerable<T> value)
        {
            var read = @this.ToList();
            read.AddRange(value);
            return read;
        }

        public static IEnumerable<T> Add<T>(this IEnumerable<T> self, T value)
        {
            var result = self as List<T> ?? self?.ToList() ?? new List<T>();
            result.Add(value);
            return result;
        }

        public static IEnumerable<TResult> SelectNew<TResult>(this IEnumerable @this)
            => @this.Cast<object>().Select(item => item.Instance<TResult>());

        public static TSource MaxBy<TSource, TKey>(
            this IEnumerable<TSource> source,
            Func<TSource, TKey> selector,
            IComparer<TKey> comparer)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (selector == null) throw new ArgumentNullException("selector");
            comparer = comparer ?? Comparer<TKey>.Default;

            using (var sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                    throw new InvalidOperationException("Sequence contains no elements");
                var max = sourceIterator.Current;
                var maxKey = selector(max);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, maxKey) > 0)
                    {
                        max = candidate;
                        maxKey = candidateProjected;
                    }
                }
                return max;
            }
        }

        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T> self)
        {
            return self.Where(v => v != null);
        }

        public static TSource MinBy<TSource, TKey>(
            this IEnumerable<TSource> source,
            Func<TSource, TKey> selector,
            IComparer<TKey> comparer)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (selector == null) throw new ArgumentNullException("selector");
            comparer = comparer ?? Comparer<TKey>.Default;

            using (var sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                    throw new InvalidOperationException("Sequence contains no elements");
                var min = sourceIterator.Current;
                var minKey = selector(min);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, minKey) < 0)
                    {
                        min = candidate;
                        minKey = candidateProjected;
                    }
                }
                return min;
            }
        }

        public static IEnumerable<T> Replace<T>(
            this IEnumerable<T> @this,
            Dictionary<T, T> dictionary)
            => @this.Select(v => dictionary.ContainsKey(v) ? dictionary[v] : v);

        public static IEnumerable<T> AddIfEmpty<T>(
            this IEnumerable<T> @this, T value)
            => @this.Any() ? @this : value.InArray();

        public static string InString(this IEnumerable<char> @this)
            => new string(@this.ToArray());
    }
}