﻿#region

using System.Drawing;
using System.Drawing.Imaging;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static Bitmap To32Argb(this Bitmap @this)
        {
            var rect = new Rectangle(0, 0, @this.Width, @this.Height);
            return @this.Clone(rect, PixelFormat.Format32bppArgb);
        }

        public static unsafe byte[,,] ToBytes(this Bitmap bmp)
        {
            int width = bmp.Width,
                height = bmp.Height;
            var res = new byte[4, height, width];
            var bd = bmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly,
                PixelFormat.Format32bppArgb);
            try
            {
                //    for (var h = 0; h < height; h++)
                //    {
                //        var curpos = (byte*) bd.Scan0 + h*bd.Stride;
                //        for (var w = 0; w < width; w++)
                //        {
                //            res[3, h, w] = *curpos++;
                //            res[2, h, w] = *curpos++;
                //            res[1, h, w] = *curpos++;
                //            res[0, h, w] = *curpos++;
                //        }
                //    }
                fixed (byte* _res = res)
                {
                    byte* _a = _res,
                        _r = _res + width*height,
                        _g = _res + 2*width*height,
                        _b = _res + 3*width*height;
                    for (var h = 0; h < height; h++)
                    {
                        var curpos = (byte*) bd.Scan0 + h*bd.Stride;
                        for (var w = 0; w < width; w++)
                        {
                            *_b++ = *curpos++;
                            *_g++ = *curpos++;
                            *_r++ = *curpos++;
                            *_a++ = *curpos++;
                        }
                    }
                }
            }
            finally
            {
                bmp.UnlockBits(bd);
            }
            return res;
        }
    }
}