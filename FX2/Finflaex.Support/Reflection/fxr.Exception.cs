﻿#region

using System;
using System.Diagnostics;
using System.Linq;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static string ToFinflaexString(this Exception @this)
            => ToFinflaexStringMethod(@this);

        private static string ToFinflaexStringMethod(Exception err)
            => err.InnerException != null
                ? ToFinflaexStringMethod(err.InnerException)
                : err.ToString();

        public static fxcExceptionTrace[] ToTraces(this Exception self)
        {
            return new StackTrace(self)
                .GetFrames()?
                .Select(frame => new fxcExceptionTrace(frame)
                {
                    File = frame.GetFileName(),
                    Column = frame.GetFileColumnNumber(),
                    Line = frame.GetFileLineNumber(),
                    Method = frame.GetMethod().ToString()
                })
                .ToArray();
        }
    }

    public class fxcExceptionTrace
    {
        public StackFrame frame;

        public fxcExceptionTrace(StackFrame frame)
        {
            this.frame = frame;
        }

        public int Column { get; internal set; }
        public string File { get; internal set; }
        public int Line { get; internal set; }
        public string Method { get; internal set; }
    }
}