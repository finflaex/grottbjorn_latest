﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Finflaex.Support.MVC;

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static string[] ReadFormCloneValues(
                this HttpRequestBase @this,
                string key)
            => @this.Form[key]
                .retIfNull("")
                .Split(','.InArray(), StringSplitOptions.RemoveEmptyEntries);

        public static string[] ReadFormArrayValues(
            this HttpRequestBase @this,
            string name, string property)
        {
            var result = new List<string>();

            @this.Form.AllKeys.ForEach(key =>
            {
                if (key.DoRegexIsMatch($@"^{name}\[\d+\]\.{property}$")
                    || key.DoRegexIsMatch($@"^{name}\[\d+\]$"))
                {
                    @this.Form[key].Action(result.Add);
                }
            });

            return result.ToArray();
        }

        public static T ReadPostJson<T>(this HttpRequestBase @this)
        {
            @this.InputStream.Seek(0, SeekOrigin.Begin);
            return new StreamReader(@this.InputStream).ReadToEnd().FromJson<T>();
        }

        public static fxcMvcFile[] ReadFiles(
            this HttpRequestBase @this)
        {
            var result = new List<fxcMvcFile>();

            @this.Files.Count.For(index =>
            {
                result.Add(new fxcMvcFile
                {
                    FullName = @this.Files[index].FileName,
                    Stream = @this.Files[index].InputStream,
                    Size = @this.Files[index].ContentLength
                });
            });

            return result.ToArray();
        }
    }
}