﻿#region

using System;
using System.Globalization;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        private static readonly long DatetimeMinTimeTicks =
            new DateTime(1970, 1, 1, 0, 0, 0).Ticks;

        public static long ToJSMilliseconds(this DateTime @this)
            => (@this.Ticks - DatetimeMinTimeTicks)/10000;

        public static DateTime FromJSMilliseconds(this long @this)
            => new DateTime(@this + DatetimeMinTimeTicks);

        public static DateTime FromRFC2822(this string @this)
        {
            int index;
            return DateTime.ParseExact(
                @this.IndexOf("(").ActionOut(out index) > -1
                    ? @this.Remove(index - 1)
                    : @this,
                "ddd MMM dd yyyy HH:mm:ss 'GMT'zzzzz",
                CultureInfo.InvariantCulture,
                DateTimeStyles.AdjustToUniversal);
        }

        public static string ToUniversalString(this DateTime @this)
            => $"{@this:yyyy-MM-dd HH:mm:ss}Z";
            
        public static DateTime GetStartWeek(this DateTime @this)
        {
            var result = @this;
            while (result.DayOfWeek != DayOfWeek.Monday)
            {
                result = result.AddDays(-1);
            }
            return result;
        }

        public static DateTime GetEndtWeek(this DateTime @this)
        {
            var result = @this;
            while (result.DayOfWeek != DayOfWeek.Sunday)
            {
                result = result.AddDays(1);
            }
            return result;
        }
    }
}
