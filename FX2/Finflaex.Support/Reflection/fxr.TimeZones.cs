﻿using System;
using System.Linq;

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static TimeZoneInfo ToTimeZoneInfo(this int @this)
            => TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(v => v.BaseUtcOffset.Hours == @this);

        public static DateTimeOffset ConvertTimeZone(
            this DateTimeOffset @this, int hours)
        {
            var timezone = hours.ToTimeZoneInfo();
            return TimeZoneInfo.ConvertTime(@this, timezone);
        }

        public static DateTimeOffset? ConvertTimeZone(
            this DateTimeOffset? @this, int hours)
        {
            if (@this == null) return null;
            var timezone = hours.ToTimeZoneInfo();
            return TimeZoneInfo.ConvertTime(@this.Value, timezone);
        }

        public static DateTimeOffset ToTZDateTimeOffset(
            this DateTime @this, int from)
        {
            return new DateTimeOffset(
                @this.Year,
                @this.Month,
                @this.Day,
                @this.Hour,
                @this.Minute,
                @this.Second,
                @this.Millisecond,
                new TimeSpan(from, 0, 0));
        }

        public static DateTimeOffset? ToTZDateTimeOffset(
            this DateTime? @this, int from)
        {
            return @this == null ? null : (DateTimeOffset?)new DateTimeOffset(
                @this.Value.Year,
                @this.Value.Month,
                @this.Value.Day,
                @this.Value.Hour,
                @this.Value.Minute,
                @this.Value.Second,
                @this.Value.Millisecond,
                new TimeSpan(from, 0, 0));
        }

        //public static DateTime? ConvertTimeZone(
        //    this DateTime? @this, int hours)
        //{
        //    if (@this == null) return null;
        //    var timezone = hours.ToTimeZoneInfo();
        //    return TimeZoneInfo.ConvertTime(@this.Value, timezone);
        //}

        public static DateTime ToTZDateTime(
            this DateTimeOffset? @this,
            int hours,
            DateTime? default_value = null)
        {
            if (@this == null) return default_value ?? DateTime.MinValue;

            return @this.Value.ToTZDateTime(hours);

            var otz = @this.Value.Offset.TotalHours.ToInt();
            var ntz = hours;

            var result = @this.Value.DateTime.AddHours((otz - ntz) * -1);

            //var old_timezone = @this.Value.Offset.TotalHours.ToInt().ToTimeZoneInfo();
            //var timezone = hours.ToTimeZoneInfo();
            //var result = TimeZoneInfo.ConvertTime(@this.Value.DateTime,old_timezone, timezone);
            return result;
        }

        public static DateTime ToTZDateTime(
            this DateTimeOffset @this,
            int hours)
        {
            var otz = @this.Offset.TotalHours.ToInt();
            var ntz = hours;
            var result = @this.DateTime.AddHours((otz - ntz) * -1);

            //var old_timezone = @this.Value.Offset.TotalHours.ToInt().ToTimeZoneInfo();
            //var timezone = hours.ToTimeZoneInfo();
            //var result = TimeZoneInfo.ConvertTime(@this.Value.DateTime,old_timezone, timezone);
            return result;
        }

        public static string ToMSK(this int @this)
        {
            if (@this < 3)
            {
                return $"MSK-{3 - @this}";
            }
            else if (@this == 3)
            {
                return $"MSK";
            }
            else
            {
                return $"MSK+{@this - 3}";
            }
        }

        public static string ToTZString(this DateTimeOffset? @this, int timezone) 
            => $"{@this.ToTZDateTime(timezone):yyyy-MM-dd HH:mm} ({timezone.ToMSK()})";

        public static string ToTZString(this DateTimeOffset @this, int timezone)
            => $"{@this.ToTZDateTime(timezone):yyyy-MM-dd HH:mm} ({timezone.ToMSK()})";
    }
}