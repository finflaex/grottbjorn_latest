﻿#region

using System.Collections.Generic;
using System.IO;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static byte[] ToFile(this byte[] @this, string path)
        {
            File.WriteAllBytes(path, @this);
            return @this;
        }

        public static byte[] FromFile(this string @this)
        {
            return File.ReadAllBytes(@this);
        }

        public static IEnumerable<string> EnumerableFiles(
            this string @this,
            string mask,
            bool topDirOnly = true)
        {
            var option = topDirOnly
                ? SearchOption.TopDirectoryOnly
                : SearchOption.AllDirectories;

            return Directory
                .EnumerateFiles(@this, mask, option);
        }
    }
}