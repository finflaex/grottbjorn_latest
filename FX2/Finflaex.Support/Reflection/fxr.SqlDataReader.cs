﻿#region

using System.Collections.Generic;
using System.Data.SqlClient;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static object[][] ReadValues(this SqlDataReader self)
        {
            var result = new List<object[]>();
            var size = self.FieldCount;
            while (self.Read())
            {
                var read = new object[size];
                self.GetValues(read);
                result.Add(read);
            }
            return result.ToArray();
        }
    }
}