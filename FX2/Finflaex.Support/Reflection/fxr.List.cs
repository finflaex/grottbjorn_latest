﻿#region

using System.Collections.Generic;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static List<T> AddRet<T>(this List<T> @this, T value)
        {
            @this.Add(value);
            return @this;
        }

        public static List<T> AddRangeRet<T>(this List<T> @this, IEnumerable<T> value)
        {
            @this.AddRange(value);
            return @this;
        }
    }
}