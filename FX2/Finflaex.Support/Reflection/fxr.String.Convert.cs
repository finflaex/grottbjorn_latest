﻿#region

using System;
using System.Globalization;
using System.Text.RegularExpressions;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static int ToInt(this string @this, int default_value = 0)
        {
            try
            {
                var result = int.Parse(@this);
                return result == 0 ? default_value : result;
            }
            catch
            {
                return default_value;
            }
        }

        public static float ToFloat(this string @this, float default_value = 0)
        {
            try
            {
                var result = float.Parse(@this.Replace(',', '.'), CultureInfo.InvariantCulture);
                return result == 0 ? default_value : result;
            }
            catch
            {
                return default_value;
            }
        }

        public static long ToLong(this string @this, long default_value = 0)
        {
            try
            {
                var result = long.Parse(@this);
                return result == 0 ? default_value : result;
            }
            catch
            {
                return default_value;
            }
        }

        public static Guid ToGuid(this string @this, Guid? default_value = null)
        {
            try
            {
                return Guid.Parse(@this);
            }
            catch
            {
                return default_value ?? Guid.Empty;
            }
        }

        public static Guid? ToNullableGuid(this string @this, Guid? default_value = null)
        {
            try
            {
                return Guid.Parse(@this);
            }
            catch
            {
                return default_value;
            }
        }

        public static DateTime ToDateTime(this string @this, string mask = null, DateTime? default_value = null)
        {
            try
            {
                if (mask == null)
                    return DateTime.Parse(@this);
                return DateTime.ParseExact(@this, mask, CultureInfo.InvariantCulture);
            }
            catch
            {
                return default_value ?? DateTime.MinValue;
            }
        }

        public static TimeSpan ToTimeSpan(this string @this, string mask = null, TimeSpan? default_value = null)
        {
            try
            {
                if (mask == null)return TimeSpan.Parse(@this);
                return TimeSpan.ParseExact(@this, mask, CultureInfo.InvariantCulture);
            }
            catch
            {
                return default_value ?? TimeSpan.MinValue;
            }
        }

        public static string EncodeStringUnicode(this string @this)
        {
            return Regex.Unescape(@this);

            //return @"(?i)\\[uU]([0-9a-f]{4})"
            //    .ToRegex()
            //    .Replace(@this, m =>
            //    {
            //        return Convert
            //            .ToInt32(m.Groups[1].Value, 16)
            //            .AsType<char>()
            //            .ToString();
            //    });
        }
    }
}