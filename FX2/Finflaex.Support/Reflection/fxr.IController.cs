﻿#region

using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using RazorEngine;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static string GetUriString<T>(this T @this)
            where T : IController
        {
            return typeof(T)
                .FullName
                .Split('.')
                .SkipWhile(v => v != "Areas")
                .Skip(1)
                .FirstOrDefault()
                .FuncSelect(area
                    => typeof(T)
                        .Name
                        .RemoveEnd("Controller")
                        .FuncSelect(controller =>
                        {
                            return area != null
                                ? $"/{area}/{controller}/"
                                : $"/{controller}/";
                        }));
        }

        public static string GetUriString(this MethodBase @this)
        {
            return @this
                .ReflectedType
                .FullName
                .Split('.')
                .SkipWhile(v => v != "Areas")
                .Skip(1)
                .FirstOrDefault()
                .FuncSelect(area
                    => @this
                        .ReflectedType
                        .Name
                        .RemoveEnd("Controller")
                        .FuncSelect(controller =>
                        {
                            return area != null
                                ? $"/{area}/{controller}/{@this.Name}"
                                : $"/{controller}/{@this.Name}";
                        }));
        }

        public static string ViewToString<T>(
            this T @this,
            string view,
            object model = null)
            where T : Controller
        {
            var ControllerContext = @this.ControllerContext;
            var ViewData = @this.ViewData;
            var TempData = @this.TempData;

            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, view);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                //viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public static string RenderPartialViewToString(this ControllerContext @this, string viewName, object model = null)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = @this.RouteData.GetRequiredString("action");

            var ViewData = @this.Controller.ViewData;
            var TempData = @this.Controller.TempData;
            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(@this, viewName);
                ViewContext viewContext = new ViewContext(@this, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public static string Render_PathView_ToString(this string viewName, object model = null)
        {
            string viewAbsolutePath = ServerMapPath(viewName);

            var viewSource = File.ReadAllText(viewAbsolutePath);

            string renderedText = Razor.Parse(viewSource, model);
            return renderedText;
        }

        public static string Render_TextView_ToString(this string viewText, object model = null)
        {
            string renderedText = Razor.Parse(viewText, model);
            return renderedText;
        }

        public static string Render_IncludeView_ToString(this string viewName, object model = null)
        {
            var asm = Assembly.GetCallingAssembly();
            var res = asm.ListResources().ToList();
            var text = asm.GetResource(viewName)
                .ToString(System.Text.Encoding.UTF8);

            string renderedText = Razor.Parse(text, model);
            return renderedText;
        }
    }
}