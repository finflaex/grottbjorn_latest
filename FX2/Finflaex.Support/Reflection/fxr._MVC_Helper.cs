﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Finflaex.Support.MVC;
using Finflaex.Support.Types;

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static MvcHtmlString SubmitButton(
                this HtmlHelper helper,
                string buttonText,
                object htmlAttributes = null)
            => $"<input type='submit' value='{buttonText}' "
                .AppendWhere(htmlAttributes != null, () => htmlAttributes
                    .ToJson()
                    .Trim('{', ' ', '}')
                    .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(item => item
                        .Split(':')
                        .FuncSelect(arr => $"{arr.FirstOrDefault()?.Trim('"')}='{arr.Skip(1).Join(":").Trim('"')}'"))
                    .Join(" "))
                .Append(" />")
                .Instance<MvcHtmlString>();

        public static MvcHtmlString Select(
                this HtmlHelper helper,
                string name,
                IEnumerable<SelectOption> items,
                object htmlAttributes = null)
            => $"<select name='{name}' "
                .AppendWhere(htmlAttributes != null, () => htmlAttributes
                    .ToJson()
                    .Trim('{', ' ', '}')
                    .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(item => item
                        .Split(':')
                        .FuncSelect(arr => $"{arr.FirstOrDefault()?.Trim('"')}='{arr.Skip(1).Join(":").Trim('"')}'"))
                    .Join(" "))
                .Append(">")
                .AppendWhere(items != null, () => items.Select(v=>v.ToString()).Join(string.Empty))
                .Append("</select>")
                .Instance<MvcHtmlString>();

        public static fxaAuth Author(
        this HtmlHelper helper)
            => helper.ViewContext.HttpContext.GetFinflaexAuth<fxaAuth>();

        public static string FullUrl(
            this UrlHelper @this,
            string area,
            string controller,
            string action,
            fxcDictSO param)
        {
            Uri requestUrl = @this.RequestContext.HttpContext.Request.Url;

            var param_string = param.Select(v => $"{v.Key}={v.Value}").Join("&").Prepend("?");

            return $"{requestUrl.Scheme}://{requestUrl.Authority}/{area}/{controller}/{action}{param_string}";
        }

        public static string Id(this HtmlHelper htmlHelper)
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("id"))
                return (string)routeValues["id"];
            else if (HttpContext.Current.Request.QueryString.AllKeys.Contains("id"))
                return HttpContext.Current.Request.QueryString["id"];

            return string.Empty;
        }

        public static string Controller(this HtmlHelper htmlHelper)
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("controller"))
                return (string)routeValues["controller"];

            return string.Empty;
        }

        public static string Action(this HtmlHelper htmlHelper)
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("action"))
                return (string)routeValues["action"];

            return string.Empty;
        }
    }

    public class SelectOption
    {
        public SelectOption(string text)
        {
            Text = text;
            Selected = false;
        }

        public SelectOption()
            : this(string.Empty)
        {
        }

        public string Text { get; set; }
        public object Value { get; set; }
        public bool Selected { get; set; }

        public override string ToString()
            => $"<option"
                .AppendWhere(Value != null, $" value='{Value}'")
                .AppendWhere(Selected, " selected='selected'")
                .Append($">{Text}</option>");
    }
}