﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static IDictionary<string, string> ToDictionary(this NameValueCollection col)
        {
            IDictionary<string, string> dict = new Dictionary<string, string>();
            foreach (var k in col.AllKeys)
            {
                dict.Add(k, col[k]);
            }
            return dict;
        }
    }
}