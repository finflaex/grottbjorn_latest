﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static R CopyIn<T, R>(this T @this, R target, int sizeBuffer = 4096)
            where T : Stream
            where R : Stream
        {
            @this.CopyTo(target);
            return target;
        }

        public static T CopyFrom<T, R>(this T @this, R target, int sizeBuffer = 4096)
            where T : Stream
            where R : Stream
        {
            target.CopyTo(@this);
            return @this;
        }

        public static T CopyFrom<T>(this T @this, byte[] target, int sizeBuffer = 4096)
            where T : Stream
        {
            new MemoryStream(target).CopyTo(@this);
            return @this;
        }

        public static IEnumerable<string> ReadLines<T>(
            this T @this,
            Encoding encoding = null)
            where T : Stream
        {
            if (encoding == null)
                encoding = Encoding.UTF8;
            using (var reader = new StreamReader(@this, encoding))
            {
                var result = new List<string>();
                string line;
                while ((line = reader.ReadLine()) != null)
                    result.Add(line);
                return result;
            }
        }

        public static byte[] ReadBytes(this Stream @this)
        {
            var buffer = new byte[16*1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = @this.Read(buffer, 0, buffer.Length)) > 0)
                    ms.Write(buffer, 0, read);
                return ms.ToArray();
            }
        }

        public static byte[] ToBytes<T>(this T @this)
            where T : Stream
        {
            var result = new byte[@this.Length];
            using (var reader = new BinaryReader(@this))
            {
                reader.Read(result, 0, Convert.ToInt32(@this.Length));
            }
            return result;
        }
    }
}