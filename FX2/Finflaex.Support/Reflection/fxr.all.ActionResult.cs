﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using Finflaex.Support.MVC;
using Finflaex.Support.MVC.Enum;
using Newtonsoft.Json;

#endregion

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        //public static JsonResult ToJsonResult(this object @this, bool get = true, Encoding encoding = null)
        //    => new JsonResult()
        //    {
        //        //ContentEncoding = encoding ?? Encoding.UTF8,
        //        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //        //ContentType = "application/json",
        //        Data = @this,
        //    };

        public static RawResult ToJsonResult(this object @this)
            => @this.ToJson().ToRawResult(ContentTypes.ApplicationJson);

        public static ActionResult ToViewResult(this object @this, string view_name = null)
        {
            var result = new ViewResult
            {
                ViewData =
                {
                    Model = @this
                }
            };
            if (view_name != null)
            {
                result.ViewName = view_name;
            }
            return result;
        }
    }
}