﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;

namespace Finflaex.Support.Reflection
{
    partial class fxr
    {
        public static MailMessage AddImage(
            this MailMessage @this,
            byte[] image,
            string name,
            string mediaType)
            => image
                .Instance<MemoryStream>()
                .Instance<LinkedResource>(mediaType)
                .Action(linked =>
                {
                    linked.ContentId = name;
                    linked.ContentType = new ContentType(mediaType);
                    @this.AlternateViews.FirstOrDefault()?.LinkedResources.Add(linked);
                })
                .FuncSelect(v => @this);

        public static MailMessage AddImageFile(
            this MailMessage @this,
            string path,
            string name,
            string mediaType = null)
            => @this.AddImage(
                path.FromFile(),
                name,
                mediaType ?? $"image/{path.GetFileExtension()}");

        public static MailMessage AddImageServerFile(
            this MailMessage @this,
            string path,
            string name,
            string mediaType = null)
            => @this.AddImageFile(path.ServerMapPath(), name, mediaType);

        public static MailMessage BodyToHtml(
            this MailMessage @this)
            => @this.AlternateViews
                .FirstOrDefault()
                .ActionIfNull(() =>
                {
                    AlternateView.CreateAlternateViewFromString(
                        @this.Body,
                        null,
                        MediaTypeNames.Text.Html)
                        .Action(@this.AlternateViews.Add);

                    @this.Body = string.Empty;
                })
                .FuncSelect(v => @this);
    }
}