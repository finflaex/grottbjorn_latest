﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Finflaex.Support.Reflection;

#endregion

namespace Finflaex.Support.Ocr
{
    public class fxcRecognize
    {
        private readonly Func<byte, bool> _compare;
        private readonly Dictionary<char, bool[]> _dictionary = new Dictionary<char, bool[]>();
        private readonly Func<byte[,,], byte[,]> _lover;

        public fxcRecognize(
            string shablon_name,
            Func<byte[,,], byte[,]> lover,
            Func<byte, bool> compare)
        {
            _compare = compare;
            _lover = lover;

            var text = Assembly
                .GetExecutingAssembly()
                .GetResource($"{shablon_name}.txt")
                .ToString(Encoding.UTF8);

            Assembly
                .GetExecutingAssembly()
                .GetResource($"{shablon_name}.png")
                .Instance<MemoryStream>()
                .Instance<Bitmap>()
                .To32Argb()
                .ToBytes()
                .FuncSelect(_lover)
                .ToBinary(_compare)
                .FuncSelect(arr =>
                {
                    var width = arr.GetLength(1);
                    var height = arr.GetLength(0);
                    var rect = new Rectangle(0, 0, width, height);
                    rect = CropHeight(rect, arr);
                    rect = CropWidth(rect, arr);
                    return Split(rect, arr)
                        .Select(r => Copy(r, arr).ToMono())
                        .ToArray();
                })
                .ForEach((index, arr) => {
                    try
                    {
                        _dictionary[text[index + 1]] = arr;
                    }
                    catch
                    {
                        
                    }
                });
        }

        public string Recognize(byte[] blob)
        {
            return blob
                .Instance<MemoryStream>()
                .FuncSelect(arr =>
                {
                    try
                    {
                        return new Bitmap(arr);
                    }
                    catch
                    {
                        return null;
                    }
                })?
                .To32Argb()
                .ToBytes()
                .FuncSelect(_lover)
                .ToBinary(_compare)
                .FuncSelect(arr =>
                {
                    var width = arr.GetLength(1);
                    var height = arr.GetLength(0);
                    var rect = new Rectangle(0, 0, width, height);
                    rect = CropHeight(rect, arr);
                    rect = CropWidth(rect, arr);
                    return Split(rect, arr)
                        .Select(r => Copy(r, arr).ToMono())
                        .ToArray();
                })
                .Select(arr =>
                {
                    return _dictionary
                        .ToArray()
                        .Select(v => new
                        {
                            name = v.Key,
                            proc = Compare(arr, v.Value)
                        })
                        .MaxBy(v => v.proc, null)
                        .name;
                })
                .ToArray()
                .Instance<string>();
        }

        //public static string[] ExtractTextFromImage(string filePath, Func<byte, bool> compare)
        //{
        //    filePath = filePath.IsNullOrEmpty() ? "png.png" : filePath;

        //    var read = new Bitmap(filePath)
        //        .To32Argb()
        //        .ToBytes()
        //        .ToLowerMeasurement(line => line[0], 0)
        //        .ToBinary(compare)
        //        .FuncSelect(arr =>
        //        {
        //            var width = arr.GetLength(1);
        //            var height = arr.GetLength(0);
        //            var rect = new Rectangle(0, 0, width, height);
        //            rect = CropHeight(rect, arr);
        //            rect = CropWidth(rect, arr);
        //            return Split(rect, arr)
        //                .Select(r => Copy(r, arr).ToMono()
        //                )
        //                .ToArray();
        //        });


        //    var target = read;

        //    return Enumerable.Range(0, target.Length).Select(z =>
        //    {
        //        return Enumerable.Range(0, target[z].GetLength(0)).Select(y =>
        //        {
        //            return Enumerable.Range(0, target[z].GetLength(1)).Select(x =>
        //            {
        //                return target[z][y, x] ? "*" : " ";
        //            })
        //            .Select(v => v.ToString())
        //            .Join("");
        //        })
        //        .Join("\r\n");
        //    })
        //    .ToArray();
        //}

        private static Rectangle CropWidth(Rectangle rect, bool[,] arr)
        {
            return Enumerable
                .Range(rect.Left, rect.Width)
                .Select(x => new
                {
                    index = x,
                    flag = Enumerable
                        .Range(rect.Top, rect.Height)
                        .Select(y => arr[y, x])
                        .Any(v => v)
                })
                .Where(item => item.flag)
                .Select(item => item.index)
                .FuncSelect(list =>
                {
                    var left = list.Min();
                    var right = list.Max();
                    var width = right - left + 1;
                    return new Rectangle(left, rect.Top, width, rect.Height);
                });
        }

        private static Rectangle CropHeight(Rectangle rect, bool[,] arr)
        {
            return Enumerable
                .Range(rect.Top, rect.Height)
                .Select(y => new
                {
                    index = y,
                    flag = Enumerable
                        .Range(rect.Left, rect.Width)
                        .Select(x => arr[y, x])
                        .Any(v => v)
                })
                .Where(item => item.flag)
                .Select(item => item.index)
                .FuncSelect(list =>
                {
                    var top = list.Min();
                    var bottom = list.Max();
                    var height = bottom - top + 1;
                    return new Rectangle(rect.Left, top, rect.Width, height);
                });
        }

        private static Rectangle[] Split(Rectangle rect, bool[,] arr)
        {
            var result = new List<Rectangle>();

            var pos = rect.Left;
            var prev = false;

            Enumerable
                .Range(rect.Left, rect.Width)
                .Select(x => new
                {
                    index = x,
                    flag = Enumerable
                        .Range(rect.Top, rect.Height)
                        .Select(y => arr[y, x])
                        .Any(v => v)
                })
                .ForEach(item =>
                {
                    if (!prev && item.flag)
                    {
                        pos = item.index;
                    }
                    else if (prev && !item.flag)
                    {
                        var calc = new Rectangle(pos, rect.Top, item.index - pos, rect.Height);
                        if (calc.Width > 0)
                            result.Add(CropHeight(calc, arr));
                    }
                    prev = item.flag;
                });
            if (prev)
            {
                var calc = new Rectangle(pos, rect.Top, rect.Right - pos, rect.Height);
                if (calc.Width > 0)
                    result.Add(CropHeight(calc, arr));
            }


            //var start = rect.Left;
            //for (int x = rect.Left; x <= rect.Right; x++)
            //{
            //    var flag = Enumerable.Range(rect.Top, rect.Height).Select(y => arr[y, x]).Any(compare);
            //    if (!flag)
            //    {
            //        if (start != (x + 1))
            //        {
            //            Crop(new Rectangle(start, rect.Top, x - start, rect.Height), compare, arr)
            //                .Action(result.Add);
            //        }
            //        start = x;
            //    }
            //}
            return result.ToArray();
        }

        private static T[,] Copy<T>(Rectangle rect, T[,] arr)
        {
            var result = new T[rect.Height, rect.Width];
            for (int ys = rect.Top, yd = 0; ys < rect.Bottom; ys++, yd++)
                for (int xs = rect.Left, xd = 0; xs < rect.Right; xs++, xd++)
                    result[yd, xd] = arr[ys, xs];
            return result;
        }

        private static double Compare<T>(T[] arr1, T[] arr2)
        {
            var sum_up = 0;
            if (arr1.Length != arr2.Length) return 0;
            double len = arr1.Length <= arr2.Length ? arr1.Length : arr2.Length;
            for (var i = 0; i < len; i++)
                if (arr1[i].Equals(arr2[i]))
                    sum_up++;
            return sum_up/(len/100);
        }
    }

    public class CharInfo
    {
        //Кол-во областей, на которые будут разделены символы, для составления последовательности яркостей (по горизонтали и вертикали)
        private const int XPoints = 4;
        private const int YPoints = 4;

        //Последовательность яркостей
        public int[] _hsbSequence;

        public CharInfo(Bitmap charBitmap, char letter)
        {
            Char = letter;

            CharBitmap = charBitmap;

            //Сжимаем наш символ в соответствии с кол-вом областей
            var resized = new Bitmap(charBitmap, XPoints, YPoints);

            _hsbSequence = new int[XPoints*YPoints];

            var i = 0;

            //И заполняем последовательность яркостями*10. Сама яркость, это double от 0.0(черное) до 1.0(белое)
            for (var y = 0; y < YPoints; y++)
                for (var x = 0; x < XPoints; x++)
                    _hsbSequence[i++] = (int) (resized.GetPixel(x, y).GetBrightness()*10);
        }

        //Символьное представление сущности
        public char Char { get; set; }

        //Графическое представление сущности
        public Bitmap CharBitmap { get; private set; }

        /// <summary>
        ///     Метод сравнения с другим символом, сравнивает последовательности яркостей
        /// </summary>
        /// <param name="charInfo"></param>
        /// <returns>Количество совпадений</returns>
        public int Compare(CharInfo charInfo)
        {
            var matches = 0;

            for (var i = 0; i < _hsbSequence.Length; i++)
                if (_hsbSequence[i] == charInfo._hsbSequence[i]) ++matches;

            return matches;
        }
    }
}