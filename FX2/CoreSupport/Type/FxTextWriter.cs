﻿#region

using System.Collections.Generic;
using System.IO;
using System.Text;

#endregion

namespace CoreSupport.Type
{
    public class FxTextWriter : TextWriter
    {
        public List<char> values = new List<char>();

        public override Encoding Encoding { get; }

        public override void Write(char value)
        {
            values.Add(value);
        }

        public override string ToString()
        {
            return new string(values.ToArray());
        }
    }
}