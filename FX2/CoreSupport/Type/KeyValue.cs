﻿#region

using System;

#endregion

namespace CoreSupport.Type
{
    public class GuidValue
    {
        public Guid Guid { get; set; }
        public string Value { get; set; }
    }

    public class KeyValue
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class MainValue : KeyValue
    {
        public Guid Guid { get; set; }
        public int ID { get; set; }
        public bool Flag { get; set; }
    }
}