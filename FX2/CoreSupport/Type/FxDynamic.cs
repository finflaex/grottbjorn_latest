﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using CoreSupport.Reflection;

#endregion

namespace CoreSupport.Type
{
    public class DictSO : FxDynamic<object>
    {
        public string GetString(string key) => this[key]?.ToString();
        public bool GetBool(string key) => (this[key] ?? false).AsType<bool>();
    }

    public class DictSS : FxDynamic<string>
    {
    }

    [Serializable]
    public class FxDynamic<TVal> : DynamicObject, IDictionary<string, TVal>
    {
        private readonly IDictionary<string, TVal> values = new Dictionary<string, TVal>();

        public TVal this[string key]
        {
            get => values.ContainsKey(key)
                ? values[key]
                : default(TVal);
            set => values[key] = value;
        }

        IEnumerator IEnumerable.GetEnumerator() => values.GetEnumerator();

        public IEnumerator<KeyValuePair<string, TVal>> GetEnumerator() => values.GetEnumerator();

        public void Add(KeyValuePair<string, TVal> item) => values[item.Key] = item.Value;

        public void Clear() => values.Clear();

        public bool Contains(KeyValuePair<string, TVal> item) => ContainsKey(item.Key);

        public void CopyTo(KeyValuePair<string, TVal>[] array, int arrayIndex) => values.CopyTo(array, arrayIndex);

        public bool Remove(KeyValuePair<string, TVal> item)
        {
            if (values.ContainsKey(item.Key))
            {
                values.Remove(item.Key);
                return true;
            }
            return false;
        }

        public int Count => values.Count;
        public bool IsReadOnly => values.IsReadOnly;
        public void Add(string key, TVal value) => values[key] = value;
        public bool ContainsKey(string key) => values.ContainsKey(key);
        public bool Remove(string key) => values.Remove(key);
        public bool TryGetValue(string key, out TVal value) => values.TryGetValue(key, out value);

        public ICollection<string> Keys => values.Keys;
        public ICollection<TVal> Values => values.Values;
        public void Set(KeyValuePair<string, TVal> item) => Add(item);
        public void Set(string key, TVal value) => values[key] = value;

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            this[binder.Name.ToLower()] = value.AsType<TVal>();
            return true;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = this[binder.Name.ToLower()];
            return true;
        }

        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            result = null;
            dynamic method = this[binder.Name];
            if (method != null)
                result = method((int) args[0]);
            return true;
        }
    }

    public static class fxrDynamic
    {
        public static DictSO ActionDynamic(
            this DictSO @this,
            Action<dynamic> act)
        {
            act((dynamic) @this);
            return @this;
        }
    }
}