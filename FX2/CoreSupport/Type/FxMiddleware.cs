﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoreSupport.DataBase.Base;
using CoreSupport.MVC.Authorization;
using CoreSupport.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

#endregion

namespace CoreSupport.Type
{
    public abstract class FxWsMiddleware<T, DB> : FxMiddleware<T, DB>
        where T : FxMiddleware<T, DB>
        where DB : fxiDbContext
    {
        private CancellationToken ct;
        private WebSocket socket;
        protected HttpContext context;

        public AuthRecord Author { get; set; }

        public override async Task Worker(HttpContext context)
        {
            this.context = context;

            if (!context.WebSockets.IsWebSocketRequest)
            {
                await Next.Invoke(context);
                return;
            }

            ct = context.RequestAborted;
            socket = await context.WebSockets.AcceptWebSocketAsync();

            Author = context.AuthRecord();

            await OnConnect();

            while (true)
            {
                try
                {
                    if (ct.IsCancellationRequested) break;
                    var response = await ReceiveStringAsync(socket, ct);
                    if (response.isEmpty() && socket.State != WebSocketState.Open) break;
                    await OnRecieve(response);
                }
                catch (Exception)
                {
                    //Debug.WriteLine(err.ToFinflaexString());
                }
            }

            await OnDisconnect();

            await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", ct);
            socket.Dispose();
        }

        public async Task Send(string data)
        {
            await SendStringAsync(socket, data, ct);
        }

        public async Task Send(object data)
        {
            await SendStringAsync(socket, data.ToJson(), ct);
        }

        public virtual Task OnRecieve(string data)
        {
            return Task.CompletedTask;
        }

        public virtual async Task OnConnect()
        {
            await Task.CompletedTask;
        }

        public virtual async Task OnDisconnect()
        {
            await Task.CompletedTask;
        }

        private static Task SendStringAsync(WebSocket socket, string data,
            CancellationToken ct = default(CancellationToken))
        {
            var buffer = Encoding.UTF8.GetBytes(data);
            var segment = new ArraySegment<byte>(buffer);
            return socket.SendAsync(segment, WebSocketMessageType.Text, true, ct);
        }

        private static async Task<string> ReceiveStringAsync(WebSocket socket,
            CancellationToken ct = default(CancellationToken))
        {
            var buffer = new ArraySegment<byte>(new byte[8192]);
            using (var ms = new MemoryStream())
            {
                WebSocketReceiveResult result;
                do
                {
                    ct.ThrowIfCancellationRequested();

                    result = await socket.ReceiveAsync(buffer, ct);
                    ms.Write(buffer.Array, buffer.Offset, result.Count);
                } while (!result.EndOfMessage);

                ms.Seek(0, SeekOrigin.Begin);
                if (result.MessageType != WebSocketMessageType.Text)
                    return null;

                using (var reader = new StreamReader(ms, Encoding.UTF8))
                {
                    return await reader.ReadToEndAsync();
                }
            }
        }
    }

    public abstract class FxMiddleware<T, DB>
        where T : FxMiddleware<T, DB>
        where DB : fxiDbContext
    {
        public DB db;
        protected RequestDelegate Next;

        public abstract Task Worker(HttpContext context);

        internal T Initialize(
            DB db,
            RequestDelegate next)
        {
            this.db = db;
            Next = next;
            return this.AsType<T>();
        }
    }

    public static class fxrMiddleware
    {
        public static IApplicationBuilder UseFxMiddleware<T, DB>(
            this IApplicationBuilder app,
            string map = null,
            params object[] param)
            where T : FxMiddleware<T, DB>
            where DB : fxiDbContext
        {
            var db = app.ApplicationServices.GetService<DB>();

            if (map.notEmpty())
                app.Map(map, app2 =>
                {
                    app2?.Use(next => Activator
                        .CreateInstance<T>()
                        .Initialize(db, next)
                        .Worker);
                });
            else
                app?.Use(next => Activator
                    .CreateInstance<T>()
                    .Initialize(db, next)
                    .Worker);
            return app;
        }
    }
}