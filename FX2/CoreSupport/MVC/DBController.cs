﻿#region

using System;
using CoreSupport.DataBase.Base;
using CoreSupport.MVC.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace CoreWeb
{
    public abstract class DBController<DB> : Controller
        where DB : fxiDbContext
    {
        public DB db;

        public DBController(DB db, IHttpContextAccessor httpContextAccessor)
        {
            Author = httpContextAccessor.HttpContext.AuthRecord();
            db.Context = httpContextAccessor.HttpContext;
            db.RouteData = ControllerContext.RouteData;
            this.db = db;
        }

        public AuthRecord Author { get; set; }
        public Guid AuthorID => Author?.Guid ?? Guid.Empty;
    }
}