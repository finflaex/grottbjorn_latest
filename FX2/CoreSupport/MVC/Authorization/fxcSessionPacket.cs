﻿namespace CoreSupport.MVC.Authorization
{
    public class fxcSessionPacket
    {
        public string data;
        public bool delete;
        public string id;
        public string key;
        public string method;
        public bool send;
    }
}