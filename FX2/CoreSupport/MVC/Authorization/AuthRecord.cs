﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using CoreSupport.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace CoreSupport.MVC.Authorization
{
    [Serializable]
    public class AuthRecord
    {
        public AuthRecord()
        {
            Roles = new List<string>();
            Guid = Guid.Empty;
            Session = Guid.Empty;
            Time = DateTime.Now;
            Roles = "guest".InArray();
        }

        public Guid Guid { get; set; }
        public Guid Session { get; set; }
        public DateTime Time { get; set; }

        public string Caption { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public bool Registered => Guid != Guid.Empty;
        public bool IsGod => Roles?.Contains("system") ?? false;
        public bool IsAdmin => IsGod || (Roles?.Contains("admin") ?? false);
        public bool IsGuest => Roles?.Contains("guest") ?? false;
        public int TimeZone { get; set; }


        public bool Verificate(params string[] roles)
            => IsGod || Registered && Roles.Intersect(roles).Any();

        #region verificate

        public bool Verificate(Func<string[], bool> action)
            => action(Roles.ToArray());

        #endregion
    }

    public static class fxrAuthRecord
    {
        public static AuthRecord AuthRecord(
            this Controller controller)
            => controller
                   .HttpContext?
                   .User
                   .Claims
                   .FirstOrDefault(v => v.Type == nameof(AuthRecord))?
                   .Value?
                   .FromJson<AuthRecord>() ?? new AuthRecord();

        public static AuthRecord AuthRecord(
            this HttpContext context)
            => context?
                   .User
                   .Claims
                   .FirstOrDefault(v => v.Type == nameof(AuthRecord))?
                   .Value?
                   .FromJson<AuthRecord>() ?? new AuthRecord();
    }
}