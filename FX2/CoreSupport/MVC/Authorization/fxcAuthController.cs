﻿#region

using System;
using CoreSupport.Reflection;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace CoreSupport.MVC.Authorization
{
    public class fxcAuthController : Controller
    {
        public AuthRecord UserAuth => HttpContext.OpenSessionItem().AuthRecord;

        public virtual void OnPing(fxcSessionPacket controller)
        {
        }

        //[HttpPost]
        //[Authorize]
        //public async Task<ActionResult> FeedBack() => await Task.Run(() =>
        //{
        //    GetType()
        //        .Assembly
        //        .GetTypes()
        //        .Where(v => v.GetInterface("fxiFeedBackController", false) != null)
        //        .Select(Activator.CreateInstance)
        //        .OfType<fxiFeedBackController>()
        //        .ForEach(controller => controller.OnPing(this));

        //    Request
        //        .Post<FeedBackPacket[]>()
        //        .ForEach(packet =>
        //        {
        //            packet.auth = UserAuth.Guid;
        //            if (packet.delete)
        //            {
        //                SessionBase.Item(UserAuth.Guid).Remove(packet.id);
        //            }
        //            else if (packet.read)
        //            {
        //                SessionBase.Item(UserAuth.Guid).Packets.FirstOrDefault().IfNotNull(record =>
        //                {
        //                    record.send = false;
        //                });
        //            }
        //            else
        //            {
        //                packet.read = true;
        //                typeof(T)
        //                    .GetMethods(BindingFlags.Public |
        //                                BindingFlags.NonPublic |
        //                                BindingFlags.Static |
        //                                BindingFlags.Instance)
        //                    .Where(method =>
        //                    {
        //                        var attrs = method
        //                            .GetCustomAttributes<fatFeedBackKey>()
        //                            .OfType<fatFeedBackKey>()
        //                            .WhereNotNull();

        //                        var flag = attrs.Any(attr
        //                            =>
        //                        {
        //                            return (
        //                                       (attr.Name == null && packet.method == null) ||
        //                                       (attr.Name == null && method.Name.Equals(packet.method,
        //                                           StringComparison.InvariantCultureIgnoreCase)) ||
        //                                       (attr.Name != null && attr.Name.Equals(packet.method,
        //                                            StringComparison.InvariantCultureIgnoreCase))
        //                                   )
        //                                   &&
        //                                   (
        //                                       (attr.Key == null && packet.key == null) ||
        //                                       (attr.Key != null && attr.Key.Equals(packet.key,
        //                                            StringComparison.InvariantCultureIgnoreCase))
        //                                   );
        //                        });

        //                        return (attrs.Any() && flag)
        //                               || (!attrs.Any() && method.Name.Equals(packet.method, StringComparison.InvariantCultureIgnoreCase));
        //                    })
        //                    .ActionIfAny(methods =>
        //                    {
        //                        packet.delete = true;
        //                        SessionBase.Item(UserAuth.Guid).Add(packet);
        //                    })
        //                    .Take(1)
        //                    .ForEach(method =>
        //                    {
        //                        if (method.IsStatic)
        //                        {
        //                            Task.Run(() =>
        //                            {
        //                                method.Invoke(this, new[] { packet });
        //                            });
        //                        }
        //                        else
        //                        {
        //                            method.Invoke(this, new[] { packet });
        //                        }
        //                    });
        //            }
        //        });

        //    ActionResult result = new EmptyResult();

        //    HttpContext.OpenSessionItem().Lock(list =>
        //    {
        //        var send = new List<fxcSessionPacket>();

        //        list.Where(v => v.send).ForEach(item =>
        //        {
        //            if (item.send)
        //            {
        //                item.send = false;
        //                send.Add(item);
        //            }
        //        });

        //        result = send.ToJsonResult();
        //        list.Where(v => v.delete).ForEach(list.Remove);
        //    });

        //    return result;
        //});

        public static void Send(Guid auth, string method, object data)
        {
            fxsSession.OpenSessionItem(auth).Packets.Add(new fxcSessionPacket
            {
                data = data.ToJson(),
                method = method,
                key = null,
                delete = false,
                id = Guid.NewGuid().ToString("N"),
                send = true
            });
        }

        public static void Redirect(Guid auth, string path) => Send(auth, "redirect", new {path});

        public static class Kendo
        {
            public static void Action(
                Guid auth,
                string target,
                string type,
                string action,
                object value = null)
                => Send(auth, "kendo", new
                {
                    target,
                    type,
                    action,
                    value
                });

            public static void AddTemplate(
                Guid auth,
                string target,
                string template,
                object data)
                => Send(auth, "add_template", new
                {
                    target,
                    template,
                    data
                });

            public static void SetTemplate(
                Guid auth,
                string target,
                string template,
                object data)
                => Send(auth, "set_template", new
                {
                    target,
                    template,
                    data
                });

            public static class TreeList
            {
                public static void Read(
                    Guid auth,
                    string target)
                    => Script.Eval(auth,
                        $@"$('{target}').data('kendoTreeList').dataSource.read()");
            }

            public static class Grid
            {
                public static void Read(
                    Guid auth,
                    string target)
                    => Script.Eval(auth,
                        $@"$('{target}').data('kendoGrid').dataSource.read()");
            }
        }

        public static class JQuery
        {
            public static void Action(
                Guid auth,
                string target,
                string action,
                params object[] value)
                => Send(auth, "jquery", new
                {
                    target,
                    action,
                    value
                });

            public static void Load(Guid auth, string target, string url)
                => Action(auth, target, "load", url);

            public static void Empty(Guid auth, string target)
                => Action(auth, target, "empty");

            public static void ToggleClass(Guid auth, string target, params string[] @class)
                => Action(auth, target, "toggleClass", @class);

            public static void Disabled(Guid auth, string target)
                => Action(auth, target, "prop", "disabled", true);

            public static void Enabled(Guid auth, string target)
                => Action(auth, target, "prop", "disabled", false);

            public static void Remove(Guid auth, string target)
                => Action(auth, target, "remove");

            public static void Value(Guid auth, string target, string value = "")
                => Action(auth, target, "val", value);

            public static void Html(Guid auth, string target, string value)
                => Action(auth, target, "html", value);

            public static void Append(Guid auth, string target, string value)
                => Action(auth, target, "append", value);

            public static void Data(Guid auth, string target, string key, string value)
                => Action(auth, target, "data", key, value);

            public static void RemoveAttr(Guid auth, string target, string key)
                => Action(auth, target, "removeAttr", key);

            public static void Css(Guid auth, string target, string key, string value)
                => Action(auth, target, "css", key, value);

            public static void Display(Guid auth, string target, string value)
                => Css(auth, target, "display", value);

            public static void Attr(Guid auth, string target, string key, string value)
                => Action(auth, target, "attr", key, value);

            public static void ID(Guid auth, string target, string value)
                => Attr(auth, target, "id", value);
        }

        public static class Script
        {
            public static void Eval(
                Guid auth,
                string script)
                => Send(auth, "evalScript", new
                {
                    script
                });
        }
    }
}