﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using CoreSupport.DataBase.Base;
using CoreSupport.DataBase.Table;
using CoreSupport.Reflection;
using CoreWeb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace CoreSupport.MVC.Authorization
{
    public static class fxsSession
    {
        private static readonly Dictionary<Guid, fxcSessionItem> data = new Dictionary<Guid, fxcSessionItem>();

        public static fxcSessionItem OpenSessionItem(Guid id)
        {
            var result = new fxcSessionItem();
            if (data.TryGetValue(id, out result)) return result;
            data.Add(id, result);
            return result;
        }

        public static fxcSessionItem OpenSessionItem
            (this Controller controller)
        {
            var id = controller.AuthRecord().Guid;
            var result = new fxcSessionItem();
            if (data.TryGetValue(id, out result)) return result;
            data.Add(id, result);
            return result;
        }

        public static fxcSessionItem OpenSessionItem
            (this HttpContext context)
        {
            var id = context.AuthRecord().Guid;
            var result = new fxcSessionItem();
            if (data.TryGetValue(id, out result)) return result;
            data.Add(id, result);
            return result;
        }

        public static fxcSessionItem CloseSessionItem
            <DB, TUser, TRole, TLink, TSession>
            (this DBController<DB> controller)
            where DB : fxiDBAuthContext<TUser, TRole, TLink, TSession>
            where TUser : fxcUser<TUser, TRole, TLink, TSession>
            where TRole : fxcRole<TUser, TRole, TLink, TSession>
            where TLink : fxcUserRole<TUser, TRole, TLink, TSession>
            where TSession : fxcSession<TUser, TRole, TLink, TSession>
        {
            controller
                .AuthRecord()
                .if_NOT_NULL(v => data.Remove(v.Guid));

            controller
                .HttpContext
                .Authentication
                .SignOutAsync("Cookies")
                .Wait();

            return new fxcSessionItem();
        }

        public static fxcSessionItem CreateSessionItem
            <DB, TUser, TRole, TLink, TSession>
            (this DBController<DB> controller, string login, string password)
            where DB : fxiDBAuthContext<TUser, TRole, TLink, TSession>
            where TUser : fxcUser<TUser, TRole, TLink, TSession>
            where TRole : fxcRole<TUser, TRole, TLink, TSession>
            where TLink : fxcUserRole<TUser, TRole, TLink, TSession>
            where TSession : fxcSession<TUser, TRole, TLink, TSession>
        {
            var query_user =
                from user in controller.db.Users
                where user.Login == login
                select user;

            if (!query_user.Any())
                throw new Exception("логин неверный");

            var encrypt = password.Encrypt();

            var query_password =
                from user in query_user
                where user.Password == encrypt
                select user;

            if (!query_password.Any())
                throw new Exception("пароль неверный");

            var query = from user in query_password
                select new
                {
                    user.ID,
                    user.Login,
                    user.TimeZone,
                    roles = from link in user.RoleLinks
                    select link.RoleKey
                };

            var read = query.FirstOrDefault();

            var session_id = Guid.Empty;

            if (read != null)
            {
                var session = typeof(TSession).Create().AsType<TSession>();
                session.UserID = read.ID;
                controller.db.Sessions.Add(session);
                session_id = session.ID;
            }

            var result = new fxcSessionItem(
                read == null
                    ? new AuthRecord()
                    : new AuthRecord
                    {
                        Guid = read.ID,
                        Roles = read.roles.ToArray(),
                        Caption = read.Login,
                        Session = session_id,
                        TimeZone = read.TimeZone
                    });

            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, login),
                new Claim(nameof(AuthRecord), result.AuthRecord.ToJson(), "")
            };
            read.roles.ForEach(role => { claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role)); });
            // создаем объект ClaimsIdentity
            var id = new ClaimsIdentity(
                claims,
                "ApplicationCookie",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки

            controller
                .HttpContext
                .Authentication
                .SignInAsync("Cookies", new ClaimsPrincipal(id))
                .Wait();

            data.Add(read?.ID ?? Guid.Empty, result);

            return result;
        }
    }
}