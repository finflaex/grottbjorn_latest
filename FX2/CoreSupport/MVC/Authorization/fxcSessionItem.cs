﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using CoreSupport.Reflection;
using CoreSupport.Type;

#endregion

namespace CoreSupport.MVC.Authorization
{
    public class fxcSessionItem
    {
        public readonly object Locker;

        private AuthRecord _authRecord;
        public DictSO Data;

        public List<fxcSessionPacket> Packets;

        public fxcSessionItem()
        {
            Packets = new List<fxcSessionPacket>();
            Locker = new object();
            Data = new DictSO();
            AuthRecord = new AuthRecord();
        }

        public fxcSessionItem(AuthRecord auth) : this()
        {
            AuthRecord = auth;
        }

        public AuthRecord AuthRecord
        {
            get { return _authRecord ?? new AuthRecord(); }
            set { _authRecord = value; }
        }

        public fxcSessionItem Lock(Action<List<fxcSessionPacket>> act)
        {
            lock (Locker)
            {
                act(Packets);
            }
            return this;
        }

        public fxcSessionItem RemovePacket(string id)
            => Lock(list => { list.FirstOrDefault(v => v.id == id).if_NOT_NULL(list.Remove); });

        public fxcSessionItem AddPacket(fxcSessionPacket packet) => Lock(list => { list.Add(packet); });
    }
}