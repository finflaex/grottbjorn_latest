﻿#region

using CoreSupport.DataBase.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace CoreWeb
{
    public abstract class DBView<DB> : ViewComponent
        where DB : FxDbContext<DB>
    {
        public readonly DB db;

        public DBView(DB db, IHttpContextAccessor httpContextAccessor)
        {
            db.Context = httpContextAccessor.HttpContext;
            db.RouteData = ViewContext.RouteData;
            this.db = db;
        }

        public virtual IViewComponentResult Invoke(
            params object[] param)
            => View(db);
    }
}