﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using CoreSupport.MVC.Authorization;
using CoreSupport.Type;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static string Controller(
            this ViewContext @this)
        {
            return @this.RouteData.Values["controller"].ToString();
        }

        public static string Action(
            this ViewContext @this)
        {
            return @this.RouteData.Values["action"].ToString();
        }

        public static R IfPath<R>(
            this ViewContext @this,
            string area,
            string controller,
            string action,
            R value,
            R default_value = default(R))
        {
            var current_area = @this.RouteData.Values.ContainsKey("area")
                ? @this.RouteData.Values["area"].ToString()
                : string.Empty;
            var current_controller = @this.RouteData.Values.ContainsKey("controller")
                ? @this.RouteData.Values["controller"].ToString()
                : string.Empty;
            var current_action = @this.RouteData.Values.ContainsKey("action")
                ? @this.RouteData.Values["action"].ToString()
                : string.Empty;

            return current_action.Equals(action, StringComparison.CurrentCultureIgnoreCase)
                   && current_area.Equals(area, StringComparison.CurrentCultureIgnoreCase)
                   && current_controller.Equals(controller, StringComparison.CurrentCultureIgnoreCase)
                ? value
                : default_value;
        }

        public static R IfNotPath<R>(
            this ViewContext @this,
            string area,
            string controller,
            string action,
            R value,
            R default_value = default(R))
        {
            var current_area = @this.RouteData.Values.ContainsKey("area")
                ? @this.RouteData.Values["area"].ToString()
                : string.Empty;
            var current_controller = @this.RouteData.Values.ContainsKey("controller")
                ? @this.RouteData.Values["controller"].ToString()
                : string.Empty;
            var current_action = @this.RouteData.Values.ContainsKey("action")
                ? @this.RouteData.Values["action"].ToString()
                : string.Empty;

            return current_action.Equals(action, StringComparison.CurrentCultureIgnoreCase)
                   && current_area.Equals(area, StringComparison.CurrentCultureIgnoreCase)
                   && current_controller.Equals(controller, StringComparison.CurrentCultureIgnoreCase)
                ? default_value
                : value;
        }

        public static string ToText(
            this Func<object, HelperResult> act)
        {
            var writer = new FxTextWriter();
            act.Invoke(null).WriteTo(writer, HtmlEncoder.Default);
            return writer.ToString();
        }

        public static string ToText(
            this HelperResult act)
        {
            var writer = new FxTextWriter();
            act.WriteTo(writer, HtmlEncoder.Default);
            return writer.ToString();
        }

        public static async Task SignInAsync(
            this HttpContext @this,
            AuthRecord record)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, record.Caption),
                new Claim(nameof(AuthRecord), record.ToJson(), "")
            };
            claims.AddRange(record.Roles.Select(role => new Claim(ClaimTypes.Role, role)));

            var id = new ClaimsIdentity(
                claims,
                "ApplicationCookie",
                ClaimTypes.Name,
                ClaimTypes.Role);

            await @this
                .Authentication
                .SignInAsync("Cookies", new ClaimsPrincipal(id));
        }

        public static async Task SignOutAsync(
            this HttpContext @this)
        {
            await @this
                .Authentication
                .SignOutAsync("Cookies");
        }
    }
}