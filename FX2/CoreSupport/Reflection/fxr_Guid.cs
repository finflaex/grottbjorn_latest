﻿#region

using System;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static Guid ToGuid(
            this string @this)
        {
            var result = Guid.Empty;
            Guid.TryParse(@this, out result);
            return result;
        }

        public static Guid ToGuid(
            this int @this,
            int sub_id = 0,
            int level1 = 0,
            int level2 = 0,
            int level3 = 0)
        {
            return new Guid($"{sub_id:00000000}-{level1:0000}-{level2:0000}-{level3:0000}-{@this:000000000000}");
        }

        public static int GetInt(this Guid @this, int default_value = 0)
            => @this.ToString("D").Split('-')[4].ToInt(default_value);

        public static int GetSubInt(this Guid @this, int default_value = 0)
            => @this.ToString("D").Split('-')[0].ToInt(default_value);

        public static int GetIntLevel1(this Guid @this, int default_value = 0)
            => @this.ToString("D").Split('-')[1].ToInt(default_value);

        public static int GetIntLevel2(this Guid @this, int default_value = 0)
            => @this.ToString("D").Split('-')[2].ToInt(default_value);

        public static int GetIntLevel3(this Guid @this, int default_value = 0)
            => @this.ToString("D").Split('-')[3].ToInt(default_value);

        public static Guid OutInt(this Guid @this, out int value, int default_value = 0)
        {
            value = @this.GetInt(default_value);
            return @this;
        }

        public static Guid OutSubInt(this Guid @this, out int value, int default_value = 0)
        {
            value = @this.GetSubInt(default_value);
            return @this;
        }


        public static Guid OutIntLevel1(this Guid @this, out int value, int default_value = 0)
        {
            value = @this.GetIntLevel1(default_value);
            return @this;
        }


        public static Guid OutIntLevel2(this Guid @this, out int value, int default_value = 0)
        {
            value = @this.GetIntLevel2(default_value);
            return @this;
        }


        public static Guid OutIntLevel3(this Guid @this, out int value, int default_value = 0)
        {
            value = @this.GetIntLevel3(default_value);
            return @this;
        }

        public static bool IsNullOrEmpty(this Guid? @this)
        {
            return @this == null || @this == Guid.Empty;
        }

        public static bool IsEmpty(this Guid @this)
        {
            return @this == Guid.Empty;
        }

        public static bool NotNullOrEmpty(this Guid? @this)
        {
            return @this != null || @this != Guid.Empty;
        }

        public static bool NotEmpty(this Guid @this)
        {
            return @this != Guid.Empty;
        }
    }
}