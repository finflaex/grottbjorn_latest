﻿#region

using System;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static object Create(this System.Type @this, params object[] param)
        {
            return Activator.CreateInstance(@this, param);
        }

        public static T Create<T>(params object[] param)
            where T : class
        {
            return Activator.CreateInstance(typeof(T), param).AsType<T>();
        }
    }
}