﻿#region

using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static string ToJson(
            this object @this,
            bool format = false)
        {
            return JsonConvert.SerializeObject(@this, format ? Formatting.Indented : Formatting.None);
        }

        public static T FromJson<T>(
            this string @this)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            return JsonConvert.DeserializeObject<T>(@this);
        }

        public static IActionResult ToJsonResult(
            this object @this)
        {
            return new ContentResult
            {
                Content = @this.ToJson(),
                ContentType = "application/json"
            };
        }

        public static IActionResult ToHtmlResult(
            this string @this)
        {
            return new ContentResult
            {
                Content = @this,
                ContentType = "text/html",
                StatusCode = 200
            };
        }

        public static IActionResult ToErrorResult(
            this string @this)
        {
            return new ContentResult
            {
                Content = @this,
                ContentType = "text/html",
                StatusCode = 500
            };
        }

        public static async Task<IActionResult> ToJsonResultAsync<T>(
            this Task<T> @this)
        {
            var result = await @this;
            return result.ToJsonResult();
        }

        public static string FromHtml(
            this string html)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);

            var root = doc.DocumentNode;
            var sb = new StringBuilder();
            foreach (var node in root.DescendantNodesAndSelf())
            {
                if (!node.HasChildNodes)
                {
                    string text = node.InnerText;
                    if (!string.IsNullOrEmpty(text))
                        sb.AppendLine(text.Trim());
                }
            }

            return sb.ToString();
        }

        public static string ToHtml(this string text)
        {
            text = WebUtility.HtmlEncode(text);
            text = text.Replace("\r\n", "\r");
            text = text.Replace("\n", "\r");
            text = text.Replace("\r", "<br>\r\n");
            text = text.Replace("  ", " &nbsp;");
            return text;
        }
    }
}