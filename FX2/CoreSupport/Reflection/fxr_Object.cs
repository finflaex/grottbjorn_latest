﻿namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static R AsType<R>(this object @this)
        {
            if (@this is R)
                return (R) @this;
            return default(R);
        }

        public static T[] InArray<T>(this T @this) => new[] {@this};
    }
}