﻿using System;

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static string ToFinflaexString(this Exception @this)
            => ToFinflaexStringMethod(@this);

        private static string ToFinflaexStringMethod(Exception err)
            => err.InnerException != null
                ? ToFinflaexStringMethod(err.InnerException)
                : err.ToString();
    }
}