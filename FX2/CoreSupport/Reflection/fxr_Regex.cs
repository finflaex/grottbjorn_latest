﻿#region

using System.Linq;
using System.Text.RegularExpressions;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static Regex ToRegex(this string self)
            => new Regex(self);

        public static Match[] DoRegexMatches(
            this string @this,
            string pattern,
            RegexOptions options = RegexOptions.None)
            => new Regex(pattern, options)
                .Matches(@this)
                .Cast<Match>()
                .ToArray();

        public static bool DoRegexIsMatch(
            this string @this,
            string pattern,
            RegexOptions options = RegexOptions.None)
            => Regex.IsMatch(@this, pattern, options);
    }
}