﻿#region

using System;
using System.Linq;
using System.Text;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static string FirstUp(this string @this)
        {
            if (@this != null && @this.Length > 1)
            {
                var first = @this.Take(1).InString().ToUpper();
                var other = @this.Skip(1).InString();
                return $"{first}{other}";
            }
            return @this;
        }

        public static string FirstLower(this string @this)
        {
            if (@this != null && @this.Length > 1)
            {
                var first = @this.Take(1).InString().ToLower();
                var other = @this.Skip(1).InString();
                return $"{first}{other}";
            }
            return @this;
        }

        public static string Append(this string @this, string value) => $"{@this}{value}";
        public static string Prepend(this string @this, string value) => $"{value}{@this}";

        public static string PrependWhere(this string @this, bool flag, string value)
            => flag ? @this.Prepend(value) : @this;

        public static string AppendWhere(this string @this, bool flag, string value)
            => flag ? @this.Append(value) : @this;

        public static string RemoveStart(this string @this, string start)
        {
            return @this.StartsWith(start)
                ? @this.Remove(0, start.Length)
                : @this;
        }

        public static string RemoveEnd(this string @this, string end)
        {
            return @this.EndsWith(end)
                ? @this.Remove(@this.Length - end.Length)
                : @this;
        }

        public static string Repeat(this string @this, int count)
        {
            var result = new StringBuilder();
            count.For(index => result.Append(@this));
            return result.ToString();
        }

        public static string[] SplitNotEmpty(this string @this, params char[] separator)
        {
            return @this.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] SplitWord(this string @this)
        {
            return @this
                .DoRegexMatches(@"\s*([A-zА-я-\d]+)\s*")
                .Select(v => v.Groups[0].Value)
                .ToArray();
        }
    }
}