﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static void ForEach<T>(
            this IEnumerable<T> @this,
            Action<T> action)
        {
            var array = @this.ToArray();
            foreach (var item in array)
                action(item);
        }

        public static void ForEachAsync<T>(
            this IEnumerable<T> @this,
            Action<T> action)
        {
            var array = @this.ToArray();
            foreach (var item in array)
            {
                Task.Run(() =>
                {
                    action(item);
                });
            }
        }

        public static void ForEach<T, R>(
            this IEnumerable<T> @this,
            Func<T, R> action)
        {
            var array = @this.ToArray();
            foreach (var item in array)
                action(item);
        }

        public static void ForEach<T>(this IEnumerable<T> @this, Action<int, T> action)
        {
            var forEach = @this as T[] ?? @this.ToArray();
            for (var i = 0; i < forEach.Length; i++)
                action(i, forEach[i]);
        }

        public static T[] Add<T>(this T[] @this, params T[] param)
        {
            var result = @this.ToList();
            param.ForEach(result.Add);
            return result.ToArray();
        }

        public static IEnumerable<T> ActionSaveLinkInDB<T>(
            this IEnumerable<T> wont,
            IEnumerable<T> has,
            Action<T> delete,
            Action<T> add)
        {
            var has_array = has?.ToArray() ?? new T[0];
            var wont_array = wont?.ToArray() ?? new T[0];

            has_array.Except(wont_array).ForEach(delete);
            wont_array.Except(has_array).ForEach(add);

            return has;
        }

        public static string InString(this IEnumerable<char> @this) => new string(@this.ToArray());
    }
}