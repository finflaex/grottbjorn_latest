﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static T if_NOT_NULL<T>(this T @this, Action<T> act)
        {
            if (@this != null)
                act(@this);
            return @this;
        }

        public static T if_NOT_NULL<T, R>(this T @this, Func<T, R> act)
        {
            if (@this != null)
                act(@this);
            return @this;
        }

        public static T if_NULL<T>(this T @this, Action<T> act)
        {
            if (@this == null)
                act(@this);
            return @this;
        }

        public static T if_NULL<T>(this T @this, Action act)
        {
            if (@this == null)
                act();
            return @this;
        }

        public static TThis throwIfNULL<TThis>(
            this TThis @this,
            string message = null)
        {
            if (@this != null) return @this;
            if (message == null)
                throw new Exception();
            else
                throw new Exception(message);
        }

        public static bool if_TRUE(this bool @this, Action act)
        {
            if (@this)
                act();
            return @this;
        }

        public static bool if_FALSE(this bool @this, Action act)
        {
            if (!@this)
                act();
            return @this;
        }

        public static IEnumerable<T> if_ANY<T>(
            this IEnumerable<T> @this,
            Action<IEnumerable<T>> act)
        {
            @this.Any().if_TRUE(() => act(@this));
            return @this;
        }

        public static IEnumerable<T> if_NOT_ANY<T>(
            this IEnumerable<T> @this,
            Action<IEnumerable<T>> act)
        {
            @this.Any().if_FALSE(() => act(@this));
            return @this;
        }

        public static T ret_if_NULL<T>(this T @this, Func<T> result)
        {
            return @this == null ? result() : @this;
        }

        public static T ret_if_NULL<T>(this T @this, T result)
        {
            return @this == null ? result : @this;
        }

        public static R ret<T, R>(
            this T @this, Func<T, R> func)
            => func(@this);

        public static T default_if_NULL<T>(this T @this)
        {
            return @this.ret_if_NULL(() => default(T));
        }

        public static T create_if_NULL<T>(this T @this)
        {
            return @this.ret_if_NULL(Activator.CreateInstance<T>);
        }

        public static async Task act<T>(this Task<T> @this, Action<T> act)
        {
            act?.Invoke(await @this);
        }
    }
}