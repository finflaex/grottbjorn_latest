﻿#region

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static int For(this int self, Action<int> action = null)
        {
            if (action != null)
                for (var i = 0; i < self; i++)
                    action(i);
            return self;
        }

        public static int For(this int self, Action action = null)
        {
            if (action != null)
                for (var i = 0; i < self; i++)
                    action();
            return self;
        }

        public static int For(this int self, int start, Action<int> action = null)
        {
            if (action != null)
                for (var i = start; i <= self; i++)
                    action(i);
            return self;
        }

        public static int For(this int self, int start, Action action = null)
        {
            if (action != null)
                for (var i = start; i <= self; i++)
                    action();
            return self;
        }

        public static int Dec(this int self, int dec = 1)
        {
            return self - dec;
        }

        public static int Inc(this int self, int inc = 1)
        {
            return self + inc;
        }

        public static IEnumerable<R> Select<R>(this int @this, Func<int, R> act) => new int[@this].Select(act);

        public static TimeZoneInfo ToTimeZoneInfo(this int @this)
            => TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(v => v.BaseUtcOffset.Hours == @this);
    }
}