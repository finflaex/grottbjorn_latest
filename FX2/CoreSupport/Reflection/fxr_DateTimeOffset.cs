﻿using System;

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static DateTimeOffset ConvertTimeZone(
            this DateTimeOffset @this, int hours)
        {
            var timezone = hours.ToTimeZoneInfo();
            return TimeZoneInfo.ConvertTime(@this, timezone);
        }
    }
}