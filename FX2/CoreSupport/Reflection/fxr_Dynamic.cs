﻿#region

using System.Reflection;
using CoreSupport.Type;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static DictSO ToDictSO(
            this object @this)
        {
            var result = new DictSO();

            @this
                .GetType()
                .GetProperties()
                .ForEach(prop =>
                {
                    try
                    {
                        result[prop.Name] = prop.GetValue(@this);
                    }
                    catch
                    {
                    }
                });

            return result;
        }
    }
}