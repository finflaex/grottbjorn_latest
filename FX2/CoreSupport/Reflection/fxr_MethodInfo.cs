﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static bool IsAsync(this MethodInfo method)
        {
            System.Type attType = typeof(AsyncStateMachineAttribute);
            var attrib = (AsyncStateMachineAttribute)method.GetCustomAttribute(attType);
            return (attrib != null);
        }
    }
}