﻿#region

using System;
using System.Text;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        #region CRYPT

        public static string ToBase64(this byte[] @this)
        {
            return Convert.ToBase64String(@this);
        }

        public static byte[] FromBase64(this string @this)
        {
            var result = @this.Replace('-', '+').Replace('_', '/');
            switch (result.Length % 4)
            {
                default:
                    return Convert.FromBase64String(result);
                case 2:
                    return Convert.FromBase64String(result + "==");
                case 3:
                    return Convert.FromBase64String(result + "=");
            }
        }

        #endregion

        public static byte[] ToBytes(this string @this, Encoding encoding = null) 
            => encoding.ret_if_NULL(Encoding.UTF8).GetBytes(@this);

        public static string FromBytes(this byte[] @this, Encoding encoding = null) 
            => encoding.ret_if_NULL(Encoding.UTF8).GetString(@this);
    }
}