﻿#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using CoreSupport.Type;

#endregion

namespace CoreSupport.Reflection
{
    partial class fxr
    {
        public static int ToInt(this string @this, int default_value = 0)
        {
            int result = default_value;
            int.TryParse(@this
                    .Replace(".", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator)
                    .Replace(",", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator),
                NumberStyles.Float,
                CultureInfo.CurrentCulture,
                out result);
            return result;
        }

        public static float ToFloat(this string @this, float default_value = 0)
        {
            float result = default_value;
            float.TryParse(@this
                    .Replace(".", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator)
                    .Replace(",", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator),
                NumberStyles.Float,
                CultureInfo.CurrentCulture,
                out result);
            return result;
        }

        public static long ToLong(this string @this, long default_value = 0)
        {
            long result = default_value;
            long.TryParse(@this,
                NumberStyles.Float,
                CultureInfo.CurrentCulture,
                out result);
            return result;
        }

        public static decimal ToDecimal(this string @this, decimal default_value = 0)
        {
            decimal result = default_value;
            decimal.TryParse(@this
                    .Replace(".", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator)
                    .Replace(",", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator),
                NumberStyles.Float,
                CultureInfo.CurrentCulture,
                out result);
            return result;
        }

        public static Guid ToGuid(this string @this, Guid? default_value = null)
        {
            try
            {
                return Guid.Parse(@this);
            }
            catch
            {
                return default_value ?? Guid.Empty;
            }
        }

        public static Guid? ToNullableGuid(this string @this, Guid? default_value = null)
        {
            try
            {
                var result = Guid.Parse(@this);
                return result.IsEmpty() ? default_value : result;
            }
            catch
            {
                return default_value;
            }
        }

        public static DateTime ToDateTime(this string @this, string mask = null, DateTime? default_value = null)
        {
            try
            {
                if (mask == null)
                    return DateTime.Parse(@this);
                return DateTime.ParseExact(@this, mask, CultureInfo.InvariantCulture);
            }
            catch
            {
                return default_value ?? DateTime.MinValue;
            }
        }

        public static DateTime? ToDateTime(this string self, params string[] mask)
        {
            try
            {
                return mask.Length == 0
                    ? DateTime.Parse(self)
                    : DateTime.ParseExact(self, mask, CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
            catch
            {
                return null;
            }
        }

        public static TimeSpan ToTimeSpan(this string @this, string mask = null, TimeSpan? default_value = null)
        {
            try
            {
                if (mask == null) return TimeSpan.Parse(@this);
                return TimeSpan.ParseExact(@this, mask, CultureInfo.InvariantCulture);
            }
            catch
            {
                return default_value ?? TimeSpan.MinValue;
            }
        }

        public static string EncodeStringUnicode(this string @this)
        {
            return Regex.Unescape(@this);
        }

        public static string Join(
            this IEnumerable<string> @this, string separator = null)
        {
            var result = new StringBuilder();
            var array = @this.ToArray();

            array.Count().For(i =>
            {
                result.Append(array[i]);
                if (separator != null && i < array.Count() - 1)
                    result.Append(separator);
            });

            return result.ToString();
        }

        public static string IfNotEmpty(
            this string @this,
            Action<string> act)
        {
            if (!string.IsNullOrWhiteSpace(@this))
                act(@this);

            return @this;
        }

        public static string retIfNotEmpty(
            this string @this,
            Func<string, string> act)
        {
            return string.IsNullOrWhiteSpace(@this)
                ? @this
                : act(@this);
        }

        public static bool isEmpty(
            this string @this)
        {
            return string.IsNullOrWhiteSpace(@this);
        }

        public static bool notEmpty(
            this string @this)
        {
            return !string.IsNullOrWhiteSpace(@this);
        }

        public static byte[] Encrypt(this byte[] value, string password = "finflaex-vadim perevalov")
        {
            byte[] Results;
            var UTF8 = new UTF8Encoding();
            var TDESAlgorithm = TripleDES.Create();
            TDESAlgorithm.Key = UTF8.GetBytes(password);
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            try
            {
                var Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(value, 0, value.Length);
            }
            finally
            {
                TDESAlgorithm.Dispose();
            }
            return Results;
        }

        public static byte[] Decrypt(this byte[] value, string password = "finflaex-vadim perevalov")
        {
            byte[] Results;
            var UTF8 = new UTF8Encoding();
            var TDESAlgorithm = TripleDES.Create();
            TDESAlgorithm.Key = UTF8.GetBytes(password);
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            try
            {
                var Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = value != null
                    ? Decryptor.TransformFinalBlock(value, 0, value.Length)
                    : new byte[0];
            }
            catch
            {
                return new byte[0];
            }
            finally
            {
                TDESAlgorithm.Dispose();
            }
            return Results;
        }

        public static string Encrypt(this string value, string password = "finflaex-vadim perevalov")
        {
            if (value == null) return string.Empty;
            var UTF8 = new UTF8Encoding();
            var DataToEncrypt = UTF8.GetBytes(value);
            var Results = DataToEncrypt.Encrypt(password);
            return Convert.ToBase64String(Results);
        }

        public static string Decrypt(this string value, string password = "finflaex-vadim perevalov")
        {
            if (value == null) return string.Empty;
            var UTF8 = new UTF8Encoding();
            var DataToDecrypt = Convert.FromBase64String(value);
            var Results = DataToDecrypt.Decrypt(password);
            return UTF8.GetString(Results);
        }

        public static string Wrap(this string @this, string value)
            => $"{value}{@this}{value}";

        public static string WrapTag(this string @this, string tagName, object attr = null)
        {
            var result = new StringBuilder();
            result.Append($"<{tagName}");
            attr?.GetType().GetProperties().ForEach(prop => result.Append($" {prop.Name.ToLower()}='{prop.GetValue(attr)}'"));
            result.Append($">{@this}</{tagName}>");
            return result.ToString();
        }

        public static string StyleTagWrap(this string @this)
        {
            return @this.WrapTag("style");
        }

        public static string ScriptTagWrap(this string @this)
        {
            return @this.WrapTag("script");
        }

        /// <summary>
        /// Слово с дефисами
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsWord(this string @this)
        {
            var pattern = @"^[a-z0-9-]+$";
            return @this
                .ToLower()
                .DoRegexIsMatch(pattern);
        }

        /// <summary>
        /// URL ссылка
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsUrl(this string @this)
        {
            var pattern = @"^(https?:\/\/)?(www\.)?([\w\d\-]+)(\.[\w\d\-]{2,})+(\/[\w\-]+)*(\.[\w\d\-]{2,})?([?|&][a-zA-Z\d_%.]+\=?[a-zA-Z\d_%.]*)*$";

            return @this
                .ToLower()
                .DoRegexIsMatch(pattern);
        }

        /// <summary>
        /// URL ссылка
        /// содержит домены 1 и 2 уровня
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsUrlShort(this string @this)
        {
            var pattern = @"^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$";

            return @this
                .ToLower()
                .DoRegexIsMatch(pattern);
        }

        /// <summary>
        //// Шестнадцатеричный цвет
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsHexColor(this string @this)
        {
            var pattern = @"^#?([a-f0-9]{6}|[a-f0-9]{3})$";

            return @this
                .ToLower()
                .DoRegexIsMatch(pattern);
        }

        public static bool IsXmlTag(this string @this)
        {
            var pattern = @"^<([a-z]+)([^>]+)*(?:>(.*)<\/\1>|\s+\/>)$";

            return @this
                .ToLower()
                .DoRegexIsMatch(pattern);
        }

        private static readonly string email_pattern = @"^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$";

        public static bool IsEmail(this string @this)
        {
            var pattern = @"^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$";

            return @this
                .ToLower()
                .DoRegexIsMatch(pattern);
        }

        public static string ToEMail(this string @this)
        {
            var value = @this.ToLower();
            return email_pattern.ToRegex().Replace(value, "$1@$2.$3");
        }

        private static readonly string ip_pattern = 4
                .Select(i => "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)")
                .Join(@"\.")
                .Prepend("^")
                .Append("$");

        public static bool IsIP(this string @this)
        {
            return @this
                .RemoveWhereNotIn("0123456789.")
                .DoRegexIsMatch(ip_pattern);
        }

        public static string ToIP(this string @this)
        {
            var value = @this.RemoveWhereNotIn("0123456789.");
            return phone_pattern.ToRegex().Replace(value, "$1.$2.$3.$4");
        }

        private static readonly string phone_pattern = @"[\- \(\)]{0,2}(\d)"
                    .Repeat(10)
                    .Prepend(@"(8|\+7|7)?")
                    .Prepend("^")
                    .Append("$");

        public static bool IsPhone(this string @this)
        {
            return @this
                .RemoveWhereNotIn("0123456789+")
                .DoRegexIsMatch(phone_pattern);
        }

        public static string ToPhone(this string @this)
        {
            var value = @this.RemoveWhereNotIn("0123456789+");
            return phone_pattern.ToRegex().Replace(value, "$2$3$4$5$6$7$8$9$10$11");
        }

        public static string RemoveWhereNotIn(this string @this, string pattern)
        {
            var result = @this.ToArray();
            var list = pattern.ToArray();

            return result.Where(v => list.Contains(v)).InString();
        }

        public static string RemoveWhereIn(this string @this, string pattern)
        {
            var result = @this.ToArray();
            var list = pattern.ToArray();

            return result.Where(v => !list.Contains(v)).InString();
        }

        public static string EncodingConvert(this string @this, Encoding from, Encoding to = null)
        {
            var bytes = from.GetBytes(@this);
            return (to ?? Encoding.UTF8).GetString(bytes);
        }
    }
}