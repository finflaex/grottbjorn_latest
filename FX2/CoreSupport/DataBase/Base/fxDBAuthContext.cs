﻿#region

using CoreSupport.DataBase.Table;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#endregion

namespace CoreSupport.DataBase.Base
{
    public class fxDBAuthContext<TThis, TUser, TRole, TLink, TSession>
        : FxDbContext<TThis>
            , fxiDBAuthContext<TUser, TRole, TLink, TSession>
        where TUser : fxcUser<TUser, TRole, TLink, TSession>
        where TRole : fxcRole<TUser, TRole, TLink, TSession>
        where TLink : fxcUserRole<TUser, TRole, TLink, TSession>
        where TSession : fxcSession<TUser, TRole, TLink, TSession>
        where TThis : fxDBAuthContext<TThis, TUser, TRole, TLink, TSession>
    {
        public DbSet<TLink> LinkUserRole { get; set; }
        public DbSet<TUser> Users { get; set; }
        public DbSet<TRole> Roles { get; set; }
        public DbSet<TSession> Sessions { get; set; }

        protected override void OnModelCreating(ModelBuilder db)
        {
            base.OnModelCreating(db);

            db.Entity<TLink>()
                .HasKey(v => new {v.UserID, v.RoleKey});
            db.Entity<TLink>()
                .HasOne(v => v.User)
                .WithMany(v => v.RoleLinks)
                .HasForeignKey(v => v.UserID)
                .OnDelete(DeleteBehavior.Cascade);
            db.Entity<TLink>()
                .HasOne(v => v.Role)
                .WithMany(v => v.UserLinks)
                .HasForeignKey(v => v.RoleKey)
                .OnDelete(DeleteBehavior.Cascade);
            db.Entity<TUser>()
                .HasMany(v => v.Sessions)
                .WithOne(v => v.User)
                .HasForeignKey(v => v.UserID)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}