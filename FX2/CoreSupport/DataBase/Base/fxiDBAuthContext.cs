﻿#region

using CoreSupport.DataBase.Table;
using Microsoft.EntityFrameworkCore;

#endregion

namespace CoreSupport.DataBase.Base
{
    public interface fxiDBAuthContext<TUser, TRole, TLink, TSession>
        : fxiDbContext
        where TUser : fxcUser<TUser, TRole, TLink, TSession>
        where TRole : fxcRole<TUser, TRole, TLink, TSession>
        where TLink : fxcUserRole<TUser, TRole, TLink, TSession>
        where TSession : fxcSession<TUser, TRole, TLink, TSession>
    {
        DbSet<TRole> Roles { get; set; }
        DbSet<TSession> Sessions { get; set; }
        DbSet<TUser> Users { get; set; }
    }
}