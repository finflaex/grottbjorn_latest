﻿#region

using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using CoreSupport.DataBase.Table;
using CoreSupport.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

#endregion

namespace CoreSupport.DataBase.Base
{
    public abstract class FxDbContext<TThis> : DbContext,
        fxiDbContext
        where TThis : FxDbContext<TThis>
    {
        protected virtual void Worker(Guid AuthorGuid)
        {
            ChangeTracker
                .Entries()
                .ForEach(entry =>
                {
                    if (entry.Entity is fxiCreateAuthorIdTable && entry.State == EntityState.Added)
                        entry.Entity.AsType<fxiCreateAuthorIdTable>().CreateAuthorID = AuthorGuid;
                    if (entry.Entity is fxiUpdateAuthorIdTable &&
                        (entry.State == EntityState.Added || entry.State == EntityState.Modified))
                        entry.Entity.AsType<fxiUpdateAuthorIdTable>().UpdateAuthorID = AuthorGuid;
                    if (entry.Entity is fxiDateCreateTable && entry.State == EntityState.Added)
                        entry.Entity.AsType<fxiDateCreateTable>().DateCreate = CurrentTime;
                    if (entry.Entity is fxiDateModifyTable &&
                        (entry.State == EntityState.Added || entry.State == EntityState.Modified))
                        entry.Entity.AsType<fxiDateModifyTable>().DateModify = CurrentTime;
                    if (entry.Entity is fxiStartEndTable)
                    {
                        var value = entry.Entity.AsType<fxiStartEndTable>();
                        if (value.Start == null)
                            value.Start = DateTimeOffset.MinValue;
                        if (value.End == null)
                            value.End = DateTimeOffset.MaxValue;
                    }
                });
        }

        #region SYSTEM

        private DateTimeOffset? _current_time;

        public HttpContext Context { get; set; }

        public DateTimeOffset CurrentTime
        {
            get => _current_time ?? DateTimeOffset.Now;
            set => _current_time = value;
        }

        public RouteData RouteData { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(
            //    "Data Source=mars;Integrated Security=False;User ID=sa;Password=411;Database=CoreDB;"
            //    //$@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=CoreDB;Integrated Security=True;"
            //    );
        }

        #endregion

        #region METHODS

        public TThis Commit(Guid? AuthorGuid = null)
        {
            Worker(AuthorGuid ?? Guid.Empty);
            SaveChanges();
            return this.AsType<TThis>();
        }

        private readonly object locker = new object();

        public async Task<TThis> CommitAsync(Guid? AuthorGuid = null)
        {
            Worker(AuthorGuid ?? Guid.Empty);
            await SaveChangesAsync();
            return this.AsType<TThis>();
        }

        public async Task<TThis> Transaction(Func<Task> act = null)
        {
            using (var t = await GetTransaction)
            {
                try
                {
                    await act?.Invoke();
                    t.Commit();
                }
                catch(Exception error)
                {
                    t.Rollback();
                    throw error;
                }
            }
            return this.AsType<TThis>();
        }

        public Task<IDbContextTransaction> GetTransaction => Database.BeginTransactionAsync();

        //public TThis Rollback()
        //{
        //    ChangeTracker.Entries().ForEach(v => v.Reload());
        //    return this.AsType<TThis>();
        //}

        #endregion
    }
}