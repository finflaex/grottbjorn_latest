﻿#region

using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

#endregion

namespace CoreSupport.DataBase.Base
{
    public interface fxiDbContext
    {
        HttpContext Context { get; set; }
        DateTimeOffset CurrentTime { get; set; }
        RouteData RouteData { get; set; }
    }
}