#region

using System.ComponentModel.DataAnnotations;

#endregion

namespace CoreSupport.DataBase.Table
{
    public interface fxiCaptionTable
    {
        [Display(Name = "Название")]
        string Caption { get; set; }
    }
}