﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreSupport.DataBase.Table
{
    public interface fxiMultiValueTable
    {
        bool BoolValue { get; set; }
        int IntValue { get; set; }
        byte[] RawValue { get; set; }
        string StringValue { get; set; }
    }
}
