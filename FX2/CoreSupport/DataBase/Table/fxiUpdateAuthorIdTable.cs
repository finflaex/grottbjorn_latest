#region

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace CoreSupport.DataBase.Table
{
    public interface fxiUpdateAuthorIdTable
    {
        [Display(Name = "��������")]
        Guid? UpdateAuthorID { get; set; }
    }
}