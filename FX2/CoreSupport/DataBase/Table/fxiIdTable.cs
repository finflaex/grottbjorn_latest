#region

using System.ComponentModel.DataAnnotations;

#endregion

namespace CoreSupport.DataBase.Table
{
    public interface fxiIdTable<TKey>
    {
        [Key]
        TKey ID { get; set; }
    }
}