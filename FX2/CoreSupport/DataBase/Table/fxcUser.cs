﻿#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#endregion

namespace CoreSupport.DataBase.Table
{
    public class fxcUser<TUser, TRole, TLink, TSession>
        : fxaGuidTable
        where TUser : fxcUser<TUser, TRole, TLink, TSession>
        where TRole : fxcRole<TUser, TRole, TLink, TSession>
        where TLink : fxcUserRole<TUser, TRole, TLink, TSession>
        where TSession : fxcSession<TUser, TRole, TLink, TSession>
    {
        public fxcUser()
        {
            RoleLinks = new HashSet<TLink>();
            Sessions = new HashSet<TSession>();
        }

        [Required]
        public string Login { get; set; }

        public string Password { get; set; }

        public virtual ICollection<TLink> RoleLinks { get; set; }
        public virtual ICollection<TSession> Sessions { get; set; }
        public int TimeZone { get; set; }
    }
}