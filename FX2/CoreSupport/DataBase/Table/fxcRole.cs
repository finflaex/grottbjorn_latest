﻿#region

using System.Collections.Generic;

#endregion

namespace CoreSupport.DataBase.Table
{
    public class fxcRole<TUser, TRole, TLink, TSession>
        : fxaKeyTable
            , fxiDescriptionTable
        where TUser : fxcUser<TUser, TRole, TLink, TSession>
        where TRole : fxcRole<TUser, TRole, TLink, TSession>
        where TLink : fxcUserRole<TUser, TRole, TLink, TSession>
        where TSession : fxcSession<TUser, TRole, TLink, TSession>
    {
        public fxcRole()
        {
            UserLinks = new HashSet<TLink>();
        }

        public virtual ICollection<TLink> UserLinks { get; set; }

        public string Description { get; set; }
    }
}