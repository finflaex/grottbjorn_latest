#region

using System;
using System.Collections.Generic;

#endregion

namespace CoreSupport.DataBase.Table
{
    public interface fxiHierarchyTable<T> : fxiGuidTable
        where T : fxiHierarchyTable<T>
    {
        Guid? ParentID { get; set; }
        T Parent { get; set; }
        ICollection<T> Childs { get; set; }
    }
}