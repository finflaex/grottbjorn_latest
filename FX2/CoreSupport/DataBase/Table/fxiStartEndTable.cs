﻿#region

using System;
using System.Linq;

#endregion

namespace CoreSupport.DataBase.Table
{
    public interface fxiStartEndTable
    {
        DateTimeOffset? Start { get; set; }
        DateTimeOffset? End { get; set; }
    }

    public static class fxrStartEndTable{
        public static IQueryable<T> Actual<T>(
            this IQueryable<T> @this,
            DateTimeOffset? now = null)
            where T : fxiStartEndTable
        {
            return 
                from read in @this
                let time = now ?? DateTimeOffset.Now
                where read.Start < time
                where read.End > time
                select read;
        }
    }
}