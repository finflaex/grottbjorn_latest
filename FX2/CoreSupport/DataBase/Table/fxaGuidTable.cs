﻿#region

using System;

#endregion

namespace CoreSupport.DataBase.Table
{
    public abstract class fxaGuidTable : fxiGuidTable
    {
        protected fxaGuidTable()
        {
            ID = Guid.NewGuid();
        }

        public virtual Guid ID { get; set; }
    }
}