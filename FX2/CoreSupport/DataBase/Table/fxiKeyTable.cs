#region

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace CoreSupport.DataBase.Table
{
    public interface fxiKeyTable
    {
        string Key { get; set; }
    }
}