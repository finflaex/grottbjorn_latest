﻿#region

using System;

#endregion

namespace CoreSupport.DataBase.Table
{
    public class fxcUserRole<TUser, TRole, TLink, TSession>
        where TUser : fxcUser<TUser, TRole, TLink, TSession>
        where TRole : fxcRole<TUser, TRole, TLink, TSession>
        where TLink : fxcUserRole<TUser, TRole, TLink, TSession>
        where TSession : fxcSession<TUser, TRole, TLink, TSession>
    {
        public fxcUserRole()
        {
        }

        public fxcUserRole(TRole role) : this()
        {
            Role = role;
        }

        public virtual TUser User { get; set; }
        public virtual TRole Role { get; set; }

        public Guid UserID { get; set; }
        public string RoleKey { get; set; }
    }
}