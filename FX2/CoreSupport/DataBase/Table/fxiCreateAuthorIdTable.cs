#region

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace CoreSupport.DataBase.Table
{
    public interface fxiCreateAuthorIdTable
    {
        [Display(Name = "���������")]
        Guid? CreateAuthorID { get; set; }
    }
}