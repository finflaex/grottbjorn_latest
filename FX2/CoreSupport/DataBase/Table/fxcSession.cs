﻿#region

using System;

#endregion

namespace CoreSupport.DataBase.Table
{
    public class fxcSession<TUser, TRole, TLink, TSession>
        : fxaGuidTable
        where TUser : fxcUser<TUser, TRole, TLink, TSession>
        where TRole : fxcRole<TUser, TRole, TLink, TSession>
        where TLink : fxcUserRole<TUser, TRole, TLink, TSession>
        where TSession : fxcSession<TUser, TRole, TLink, TSession>
    {
        public Guid UserID { get; set; }
        public virtual TUser User { get; set; }
    }
}