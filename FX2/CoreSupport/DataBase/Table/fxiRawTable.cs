namespace CoreSupport.DataBase.Table
{
    public interface fxiRawTable
    {
        byte[] Raw { get; set; }
    }
}