using System.ComponentModel.DataAnnotations;

namespace CoreSupport.DataBase.Table
{
    public interface fxiTypeTable<TType>
    {
        [Display(Name = "���")]
        TType Type { get; set; }
    }
}