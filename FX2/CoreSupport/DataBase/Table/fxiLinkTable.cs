﻿#region

using System;

#endregion

namespace CoreSupport.DataBase.Table
{
    public interface fxiLinkTable<TSrs, TDst, TType>
        where TSrs : fxiGuidTable
        where TDst : fxiGuidTable
    {
        Guid SourceID { get; set; }
        Guid DestinationID { get; set; }
        TType Type { get; set; }
        TSrs Source { get; set; }
        TDst Desctination { get; set; }
    }
}