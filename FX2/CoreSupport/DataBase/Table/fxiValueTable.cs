#region

using System.ComponentModel.DataAnnotations;

#endregion

namespace CoreSupport.DataBase.Table
{
    public interface fxiValueTable<TVal>
    {
        [Display(Name = "��������")]
        TVal Value { get; set; }
    }
}