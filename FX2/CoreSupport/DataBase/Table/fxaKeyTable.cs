﻿#region

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;

#endregion

namespace CoreSupport.DataBase.Table
{
    public abstract class fxaKeyTable : fxiKeyTable
    {
        [Key]
        public string Key { get; set; }
    }
}