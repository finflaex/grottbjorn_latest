﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using CoreSupport.DataBase.Table;

#endregion

namespace CoreSupport.DataBase.Monotable
{
    public class CoreRecord
        : fxaGuidTable
    {
    }

    public class CoreChild<TRoot>
        : CoreRecord
        where TRoot : CoreRecord
    {
        [ForeignKey(nameof(Root))]
        public Guid RootID { get; set; }

        public virtual TRoot Root { get; set; }
    }

    public interface ISourcesRecord<TSrc>
        where TSrc : CoreRecord
    {
        ICollection<TSrc> Sources { get; set; }
    }

    public interface IDestinationRecord<TSrc>
        where TSrc : CoreRecord
    {
        ICollection<TSrc> Destinations { get; set; }
    }

    public class TypeRecord
        : fxaGuidTable
    {
    }

    public class LinkRecord<TSrc, TDst, TType>
        : fxaGuidTable
            , fxiStartEndTable
        where TSrc : CoreRecord
        where TDst : CoreRecord
        where TType : TypeRecord
    {
        [ForeignKey(nameof(Source))]
        public Guid SourceID { get; set; }

        public virtual TSrc Source { get; set; }

        [ForeignKey(nameof(Destination))]
        public Guid DestinationID { get; set; }

        public virtual TDst Destination { get; set; }

        [ForeignKey(nameof(Type))]
        public Guid? TypeID { get; set; }

        public virtual TType Type { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
    }
}