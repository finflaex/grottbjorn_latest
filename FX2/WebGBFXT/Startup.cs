﻿#region

using Microsoft.Owin;
using Owin;
using WebGBFXT;
using WebGBFXT.Controllers;

#endregion

[assembly: OwinStartup(typeof(Startup))]

namespace WebGBFXT
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            app.MapSignalR<WebSocket>("/ws");
        }
    }
}