﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseDBFX.Base;
using Finflaex.Support.Reflection;

namespace WebGBFXT.Models
{
    public class HelpAccess
    {
        public HelpAccess(HtmlHelper<object> html)
        {
            var auth = html.Author();

            reg = auth.Registered;
            admin = auth.Verificate(dbroles.admin);
            oper = auth.Verificate(dbroles.@operator) && !auth.Verificate(dbroles.consultant) || auth.Verificate(dbroles.admin);
            cons = auth.Verificate(dbroles.consultant) || auth.Verificate(dbroles.admin);

            oper_manager = auth.Verificate(dbroles.@operator) && !auth.Verificate(dbroles.consultant) && auth.Verificate(dbroles.manager) || auth.Verificate(dbroles.admin);
            cons_manager = auth.Verificate(dbroles.consultant) && auth.Verificate(dbroles.manager) || auth.Verificate(dbroles.admin);

            manager = auth.Verificate(dbroles.manager) || auth.Verificate(dbroles.admin);
            controller = auth.Verificate(dbroles.cik) || auth.Verificate(dbroles.admin);
        }

        public bool reg { get; set; }
        public bool admin { get; set; }
        public bool oper { get; set; }
        public bool cons { get; set; }
        public bool oper_manager { get; set; }
        public bool cons_manager { get; set; }
        public bool manager { get; set; }
        public bool controller { get; set; }
    }
}