﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Security;
using Finflaex.Support.MVC;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;
using Microsoft.AspNet.SignalR;

#endregion

namespace WebGBFXT.Controllers
{
    /// <summary>
    /// Вебсокеты
    /// </summary>
    public class WebSocket : fxaWebSocket<WebSocket>
    {
        public static readonly
            Dictionary<Guid, Action<RequestRecord>>
            ConnectList = new
                Dictionary<Guid, Action<RequestRecord>>();

        public static readonly
            List<RequestRecord>
            RequestList = new
                List<RequestRecord>();

        /// <summary>
        /// При получении пакета
        /// </summary>
        /// <param name="request"></param>
        /// <param name="connectionId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        protected override Task OnReceived(
            IRequest request,
            string connectionId,
            string data)
        {
            Debug.WriteLine("recived");

            var read = data.FromJson<fxcDictSO>();

            RequestList
                .Where(v => v.ConnectionID == connectionId)
                .ToArray()
                .ForEach(record =>
                {
                    Debug.WriteLine($"{record.MethodList.Count}");

                    read.GetString("method")
                        .IsNotNullOrWhiteSpace(value =>
                        {
                            record.MethodList.ContainsKey(value)
                                .IfTrue(() =>
                                {
                                    read.GetJObjectToObject<fxcDictSO>("data")
                                        .ActionIfNotNull(
                                            result => { record.MethodList[value](record, result); });
                                });
                        });
                });

            return base.OnReceived(request, connectionId, data);
        }

        /// <summary>
        /// При подключении
        /// </summary>
        /// <param name="request"></param>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        protected override Task OnConnected(IRequest request, string connectionId)
        {
            Debug.WriteLine("connect");

            var auth = request.GetCookie<fxaAuth>(FormsAuthentication.FormsCookieName)?.Author;

            if (auth != null)
            {
                var record = new RequestRecord
                {
                    user = auth.Value,
                    ConnectionID = connectionId,
                    connect = Connection
                };

                RequestList.Add(record);

                ConnectList.ContainsKey(auth.Value)
                    .IfTrue(() =>
                    {
                        ConnectList[auth.Value](record);
                        ConnectList.Remove(auth.Value);
                    });

                return base.OnConnected(request, connectionId);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// При отключении
        /// </summary>
        /// <param name="request"></param>
        /// <param name="connectionId"></param>
        /// <param name="stopCalled"></param>
        /// <returns></returns>
        protected override Task OnDisconnected(IRequest request, string connectionId, bool stopCalled)
        {
            Debug.WriteLine("disconnect");

            var user = RequestList.FirstOrDefault(v => v.ConnectionID == connectionId);
            RequestList.Remove(user);
            return base.OnDisconnected(request, connectionId, stopCalled);
        }

        public class RequestRecord
        {
            public readonly Dictionary<string, Action<RequestRecord, fxcDictSO>> MethodList 
                = new Dictionary<string, Action<RequestRecord, fxcDictSO>>();

            public string ConnectionID { get; set; }

            public Guid user { get; set; }

            public IConnection connect { private get; set; }

            public RequestRecord AddMethod(string key, Action<RequestRecord, fxcDictSO> act)
            {
                MethodList[key] = act;
                return this;
            }

            public RequestRecord Send(object data)
            {
                connect.Send(ConnectionID, data.ToJson());
                return this;
            }
        }
    }

    /// <summary>
    /// Помощь по сокетам
    /// </summary>
    public static class WebSocketHelper
    {
        /// <summary>
        /// получение сокета
        /// </summary>
        /// <param name="this"></param>
        /// <param name="id"></param>
        /// <param name="act"></param>
        public static void GetWS(this object @this, Guid id, Action<WebSocket.RequestRecord> act)
        {
            Debug.WriteLine("wait");

            var list = WebSocket.RequestList
                .Where(v => v.user == id)
                .Select(v => v)
                .ToArray();

            if (list.Any())
                list.ForEach(record => Task.Run(() => { act(record); }));
            else
                WebSocket.ConnectList[id] = act;
        }

        /// <summary>
        /// Ожидание сокета
        /// </summary>
        /// <param name="this"></param>
        /// <param name="id"></param>
        /// <param name="act"></param>
        public static void WaitWS(this object @this, Guid id, Action<WebSocket.RequestRecord> act)
        {
            Debug.WriteLine("wait");
            WebSocket.ConnectList[id] = act;
        }
    }
}