﻿#region

using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using Finflaex.Support.MVC;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using WebGBFXT.Models;

#endregion

namespace WebGBFXT.Controllers
{
    /// <summary>
    /// Стартовая страница
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles]
        // GET: Home
        public ActionResult Index() => View();

        /// <summary>
        /// Форма ввода пароля
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Unreg(LoginModel model) => View(model);

        /// <summary>
        /// Выход
        /// </summary>
        /// <returns></returns>
        [Roles]
        public ActionResult LogOut()
        {
            HttpContext.SetFinflaexAuth(new fxaAuth
            {
                Login = "unknown",
                Roles = new string[0],
                Caption = "Гость",
                Author = Guid.Empty,
                Session = Guid.Empty,
                is_god = false
            });
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Вход
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Login(LoginModel model)
            => await this.func(gb =>
            {
                model.path = model.path.retIfNullOrWhiteSpace("~/Home/Index");

                var login = gb.db.Logins
                    .Where(v => v.Name == model.login)
                    .Include(v => v.Roles)
                    .ToList()
                    .FirstOrDefault(v => v.Password == model.password);

                if (login == null)
                    return RedirectToAction("Unreg", model)
                        .AsType<ActionResult>();

                var auth = HttpContext.SetFinflaexAuth(new fxaAuth
                {
                    Login = login.Name,
                    Roles = login.Roles.Select(v => v.Key).ToArray(),
                    Caption = login.Caption,
                    Author = login.ID,
                    Session = Guid.Empty
                });

                if (model.path == @"/Home/Login")
                    model.path = @"/Home/Index";

                return Redirect(model.path);
            });

        public ViewResult PartialNavbar()
        {
            return View();
        }

        public ViewResult PartialHeadbar()
        {
            return View();
        }

        public ViewResult PartialFooter()
        {
            return View();
        }

        public ViewResult PartialRight()
        {
            return View();
        }

        public async Task<ActionResult> Help() => await this.func(View);
    }
}