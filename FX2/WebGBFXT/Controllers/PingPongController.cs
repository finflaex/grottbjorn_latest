﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BaseDBFX.Base;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;

namespace WebGBFXT.Controllers
{
    public class PingPongController : Controller
    {
        public static stack connection = new stack();

        [HttpPost]
        public async Task<ActionResult> Index(string data)
            => await this.func(gb =>
            {
                if (data.IsNullOrWhiteSpace())
                    goto exit;

                var read_array = data.FromJson<Item[]>();

                if (read_array.Length == 0)
                    goto exit;

                foreach (var read in read_array)
                {
                    var result = new Item();

                    if (read.ping) goto exit;

                    if (read.data != null)
                    {
                        connection
                            .Request(gb.AuthorGuid, read.data)
                            .ForEach(v =>
                            {
                                connection[gb.AuthorGuid].Add(v);
                            });
                    }

                    if (read.delete)
                    {
                        connection.delete(gb.AuthorGuid, read.id);
                    }
                    else if (result != null)
                    {
                        result.index = read.index;
                        result.delete = true;
                        connection[gb.AuthorGuid].Add(result);
                    }
                }

                exit:
                if (connection[gb.AuthorGuid].Count > 0)
                {
                    return connection[gb.AuthorGuid].ToJsonResult().Action(() =>
                    {
                        connection.clear(gb.AuthorGuid);
                    });
                }
                Thread.Sleep(1000);
                return new Item { pong = true }.InArray().ToJsonResult();
            });

        public class Item
        {
            public Item()
            {
                id = Guid.NewGuid();
            }
            public Guid id { get; set; }
            public bool ping { get; set; }
            public long index { get; set; }
            public fxcDictSO data { get; set; }
            public bool delete { get; set; }
            public bool pong { get; set; }
        }

        public class Method
        {
            public string key { get; set; }
            public Func<fxcDictSO, fxcDictSO> method { get; set; }
        }

        public class stack
        {
            private static Dictionary<Guid?, List<Item>> arr
                = new Dictionary<Guid?, List<Item>>();

            private static Dictionary<Guid?, List<Method>> func
                = new Dictionary<Guid?, List<Method>>();

            public stack Recieve(Guid? id, string key, Func<fxcDictSO, fxcDictSO> act)
            {
                if (!func.ContainsKey(id) || func[id] == null)
                {
                    func[id] = new List<Method>();
                }

                func[id].Add(new Method
                {
                    key = key,
                    method = act
                });
                return this;
            }

            public Item[] Request(Guid? id, fxcDictSO data)
            {
                var key = data.GetString("method");
                if (id.NotNullOrEmpty() && data.NotNull() && key.NotNullOrEmpty())
                {
                    if (!func.ContainsKey(id) || func[id] == null)
                    {
                        func[id] = new List<Method>();
                    }

                    var result = new List<Item>();

                    func[id].Where(v=>v.key == key).Select(v=>v.method).ForEach(method =>
                    {
                        var read = method(data);
                        if (read != null)
                        {
                            result.Add(new Item
                            {
                                data = read
                            });
                        }
                    });

                    return result.ToArray();
                }
                return null;
            }

            public stack Send(Guid key, fxcDictSO data)
            {
                if (!arr.ContainsKey(key)) {
                    arr[key] = new List<Item>();
                }
                arr[key].Add(new Item
                {
                    data = data,
                });

                return this;
            }

        public List<Item> this[Guid key]
            {
                get
                {
                    if (arr == null)
                    {
                        arr = new Dictionary<Guid?, List<Item>>();
                    }
                    if (!arr.ContainsKey(key))
                    {
                        arr[key] = new List<Item>();
                    }

                    return arr[key];
                }
            }

            public void clear(Guid key)
            {
                arr[key].Where(v => v.delete).ToArray().ForEach(item =>
                {
                    arr[key].Remove(item);
                });
            }

            public void delete(Guid key, Guid readId)
            {
                arr[key].FirstOrDefault(v => v.id == readId).ActionIfNotNull(item =>
                {
                    arr[key].Remove(item);
                });
            }

            public stack Empty(Guid? id)
            {
                if (!func.ContainsKey(id) || func[id] == null)
                {
                    func[id] = new List<Method>();
                }
                func[id].Clear();
                return this;
            }
        }
    }

    public static class PingPongHelper
    {
        public static Guid Send(
            this Guid key, fxcDictSO data)
        {
            PingPongController.connection.Send(key, data);
            return key;
        }

        public static Guid Run(
            this Guid key, string method, Func<fxcDictSO, fxcDictSO> act)
        {
            PingPongController.connection.Recieve(key, method, act);
            return key;
        }
    }
}