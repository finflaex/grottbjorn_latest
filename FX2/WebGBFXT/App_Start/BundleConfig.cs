﻿#region

using System.Web.Optimization;

#endregion

namespace WebGBFXT
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/style/standart").Include(
                "~/Content/bootstrap.css",
                "~/dashboard/font-awesome/css/font-awesome.css",
                "~/dashboard/css/animate.css",
                "~/dashboard/css/plugins/codemirror/codemirror.css",
                "~/dashboard/css/plugins/codemirror/ambiance.css",
                "~/dashboard/css/style.css"));

            bundles.Add(new ScriptBundle("~/script/standart").Include(
                "~/dashboard/js/jquery-2.1.1.js",
                "~/dashboard/js/bootstrap.js"));
        }
    }
}