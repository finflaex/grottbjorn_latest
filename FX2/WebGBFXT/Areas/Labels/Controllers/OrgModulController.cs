﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Labels.Controllers
{
    /// <summary>
    /// Модуль меток
    /// </summary>
    [NoCache]
    [Roles]
    public class OrgModulController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Index(Guid? id)
        {
            if (id == null)
            {
                return Content("нулевой идентификатор");
            }

            return View(new ActionData(this)
            {
                GuidValue = new MainValue
                {
                    id = id.Value
                }
            });
        }

        /// <summary>
        /// Выбор одной метки
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ActionResult> ChangeSingleLabel(MainValue model)
            => await this.act(gb =>
            {
                var type = model.value.RemoveStart("label_").ToEnum<TypeLabel>();

                var read = (
                        from org in gb.db.Organizations
                        where org.ID == model.id
                        select new
                        {
                            old = (
                                from old in org.Labels
                                where old.Type == type
                                select old
                            )
                            .FirstOrDefault(),
                            org,
                            label = (
                                from label in gb.db.Labels
                                where label.ID == model.target
                                select label
                            )
                            .FirstOrDefault()
                        })
                    .FirstOrDefault();

                if (read.label == null || read.label.ID == Guid.Empty)
                {
                    read.org.Labels.Remove(read.old);
                }
                else if (read.old != null && read.old.ID != read.label.ID)
                {
                    read.org.Labels.Remove(read.old);
                    read.org.Labels.Add(read.label);
                }
                else if (read.old == null)
                {
                    read.org.Labels.Add(read.label);
                }

                gb.Save();
            });

        /// <summary>
        /// Выбор нескольких меток
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ActionResult> ChangeMultiLabel(MainValue model)
            => await this.act(gb =>
            {
                var type = model.value.RemoveStart("label_").ToEnum<TypeLabel>();

                if (model.guids == null) model.guids = new Guid[0];

                var read = (
                        from org in gb.db.Organizations
                        where org.ID == model.id
                        select new
                        {
                            org,
                            add = (from label in gb.db.Labels
                                where type != TypeLabel.user || label.UserID == gb.AuthorGuid
                                where label.Type == type
                                where model.guids.Contains(label.ID)
                                select label)
                            .AsEnumerable(),
                            old = (from label in org.Labels
                                where type != TypeLabel.user || label.UserID == gb.AuthorGuid
                                where label.Type == type
                                select label)
                            .AsEnumerable()
                        })
                    .First();

                read.add
                    .Select(v => v.ID)
                    .Except(read.old.Select(v => v.ID))
                    .Select(v => read.add.First(a => a.ID == v))
                    .ToArray()
                    .ForEach(v => read.org.Labels.Add(v));

                read.old
                    .Select(v => v.ID)
                    .Except(read.add.Select(v => v.ID))
                    .Select(v => read.old.First(o => o.ID == v))
                    .ToArray()
                    .ForEach(v => read.org.Labels.Remove(v));

                gb.Save();
            });
    }
}