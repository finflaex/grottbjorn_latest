﻿#region

using System.Web.Mvc;

#endregion

namespace WebGBFXT.Areas.Labels
{
    public class LabelsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Labels"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Labels_default",
                "Labels/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}