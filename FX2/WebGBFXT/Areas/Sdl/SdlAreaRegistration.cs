﻿#region

using System.Web.Mvc;

#endregion

namespace WebGBFXT.Areas.Sdl
{
    public class SdlAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Sdl"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Sdl_default",
                "Sdl/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}