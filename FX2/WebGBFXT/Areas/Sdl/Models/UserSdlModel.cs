﻿#region

using System;
using System.ComponentModel;

#endregion

namespace WebGBFXT.Areas.Sdl.Models
{
    public class UserSdlModel
    {
        public Guid? ID { get; set; }

        [DisplayName("Наименование")]
        public string Caption { get; set; }

        [DisplayName("Логин")]
        public string Login { get; set; }

        [DisplayName("Пароль")]
        public string Password { get; set; }

        [DisplayName("Телефон")]
        public string Phone { get; set; }

        [DisplayName("Почта")]
        public string EMail { get; set; }
    }
}