﻿#region

using System;
using System.ComponentModel;

#endregion

namespace WebGBFXT.Areas.Sdl.Models
{
    [Serializable]
    public class SdlOrgInfoModel
    {
        public const string DefaultName = "Закрытое акционерное общество «Среднеуральский брокерский центр»";
        public const string DefaultAddress = "620062, г. Екатеринбург, пр. Ленина, 101/2";
        public const string DefaultINN = "6660040152";
        public const string DefaultOGRN = "1026604938370";

        public SdlOrgInfoModel()
        {
            Name = DefaultName;
            Address = DefaultAddress;
            INN = DefaultINN;
            OGRN = DefaultOGRN;
        }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Адрес")]
        public string Address { get; set; }

        [DisplayName("ИНН")]
        public string INN { get; set; }

        [DisplayName("ОГРН")]
        public string OGRN { get; set; }
    }
}