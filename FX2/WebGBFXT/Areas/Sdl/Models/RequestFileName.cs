﻿#region

using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Finflaex.Support.Reflection;
using WebGBFXT.Areas.Sdl.Models;

#endregion

namespace modCikCb.Models
{
    public class RequestFileName
    {
        private readonly string value;
        private string[] _matches;
        protected Regex regex;

        public RequestFileName(string value)
        {
            //regex = request.ToRegex();
            this.value = value;
        }

        //public static string request => @"(["
        //                                + fxr.EnumToList<TypeMember>().Skip(1).Select(v =>
        //                                    typeof (TypeMember)
        //                                        .GetField(v.ToString())?
        //                                        .GetCustomAttributes(typeof (EnumParserAttribute), false)
        //                                        .OfType<EnumParserAttribute>()
        //                                        .FirstOrDefault()?
        //                                        .Name ?? "?"
        //                                    ).Join(",") +
        //                                @"]{1})(\d{13})_(\d{6})_(["
        //                                + fxr.EnumToList<TypeRequest>().Skip(1).Select(v =>
        //                                    typeof (TypeRequest)
        //                                        .GetField(v.ToString())
        //                                        .GetCustomAttributes(typeof (EnumParserAttribute), false)
        //                                        .OfType<EnumParserAttribute>()
        //                                        .FirstOrDefault()?
        //                                        .Name ?? "?"
        //                                    ).Join(",") +
        //                                @"]{1})_(\d{4})";

        protected string[] matches =>
            _matches
            ??
            (_matches =
                regex.Matches(value)
                    .Cast<Match>()
                    .Take(1)
                    .SelectMany(v => v.Groups.Cast<Group>())
                    .Select(v => v.Value)
                    .ToArray());

        public bool Complited => regex.IsMatch(value);

        public string StringSourceTypeMember
        {
            get
            {
                try
                {
                    return matches[0];
                }
                catch (Exception error)
                {
                    return "?";
                }
            }
        }

        public string _StringSourceTypeMember { get; set; }

        public TypeMember SourceTypeMember
        {
            get { return StringSourceTypeMember?.ToEnum<TypeMember>() ?? TypeMember.Unknown; }
            set
            {
                _StringSourceTypeMember =
                    typeof(TypeMember).GetField(value.ToString())
                        .GetAttributesOfProperty(nameof(EnumParserAttribute))
                        .OfType<EnumParserAttribute>()
                        .FirstOrDefault()?
                        .Name ?? "?";
            }
        }

        private long? _SourceOGRN { get; set; }

        public long SourceOGRN
        {
            get { return _SourceOGRN ?? (matches.Length > 0 ? matches[1].ToLong() : 0); }
            set { _SourceOGRN = value; }
        }

        public string SourceStringFullID
        {
            get
            {
                return StringSourceTypeMember
                       + SourceOGRN.ToString("0000000000000");
            }
        }

        public virtual DateTime Date
            => matches.Length > 0 ? matches[2].ToDateTime("ddMMyy").AsType<DateTime>() : DateTime.Now.Date;

        public string StringTypeRequest => matches.Length > 0 ? matches[3] : "?";

        public TypeRequest Type => StringTypeRequest.ToEnum<TypeRequest>();

        public int Number => matches.Length > 0 ? matches[4].ToInt() : 0;
        public static implicit operator string(RequestFileName value) => value.ToString();
        public static implicit operator RequestFileName(string value) => new RequestFileName(value);

        public override string ToString()
        {
            var result = new StringBuilder();
            result.Append(SourceStringFullID);
            result.Append("_");
            result.Append(Date.ToString("ddMMyy"));
            result.Append("_");
            result.Append(StringTypeRequest);
            result.Append("_");
            result.Append(Number.ToString("0000"));
            return result.ToString();
        }
    }
}