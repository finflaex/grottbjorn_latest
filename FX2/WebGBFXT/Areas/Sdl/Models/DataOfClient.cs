﻿#region

using System.Collections.Generic;
using BaseDBFX.Table;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Sdl.Models
{
    public class DataOfClient
    {
        public int? ClientID { get; set; }
        public int? InstrumentID { get; set; }
        public string Ticker { get; set; }
        public string InstrumentTypeName { get; set; }


        public string INN { get; set; }
        public int InstrumentTypeID { get; set; }
        public string CategoryID { get; set; }
        public string IssuerName { get; set; }
        public string ISINCode { get; set; }
        public decimal Amount { get; set; }
        public double Nominal { get; set; }
        public double Rate { get; set; }
        public double PriceAmount { get; set; }

        public DataWhereResult Compare()
        {
            var result = new DataWhereResult();

            //if (INN.IsNullOrWhiteSpace())
            //{
            //    result.FlagErrorINN = true;
            //    result.Message.Add("Поле \"Инн эмитента\" пустое");
            //}
            //else
            //{
            //    result.FlagINN = true;
            //}

            if (IssuerName.IsNullOrWhiteSpace())
            {
                result.FlagErrorIssuerName = true;
                result.Message.Add("Поле \"Эмитент\" пустое");
            }
            else
            {
                result.FlagIssuerName = true;
            }

            if (InstrumentID == null || InstrumentID == 0)
            {
                result.FlagErrorInstrumentID = true;
                result.Message.Add("Поле \"Код инструмента\" не корректно");
            }
            else
            {
                result.FlagInstrumentID = true;
            }

            if (ISINCode.IsNullOrWhiteSpace())
            {
                result.FlagErrorIsinCode = true;
                result.Message.Add("поле \"Код ЦБ\" пустое");
            }
            else
            {
                result.FlagIsinCode = true;
            }

            if (InstrumentTypeID != 91 && PriceAmount.CompareExt(0))
            {
                result.FlagErrorPriceAmount = true;
                result.Message.Add("поле \"Стоимость по формуле для видов ЦБ\" пустое");
            }
            else
            {
                result.FlagPriceAmount = true;
            }

            return result;
        }
    }

    public class DataWhereResult
    {
        public DataWhereResult()
        {
            Message = new List<string>();
        }

        public List<string> Message { get; set; }

        //public bool FlagINN;
        //public bool FlagErrorINN { get; set; }

        public bool FlagErrorIssuerName { get; set; }
        public bool FlagIssuerName { get; set; }

        public bool FlagErrorInstrumentID { get; set; }
        public bool FlagInstrumentID { get; set; }

        public bool FlagErrorIsinCode { get; set; }
        public bool FlagIsinCode { get; set; }

        public bool FlagErrorPriceAmount { get; set; }
        public bool FlagPriceAmount { get; set; }

        public bool Flag
            => 
            //FlagINN && 
            FlagIssuerName
               && FlagInstrumentID
               && FlagIsinCode
               && FlagPriceAmount;

        public bool FlagWarning
        {
            get
            {
                var next = 
                    //(FlagINN || FlagErrorINN) && 
                        (FlagIssuerName || FlagErrorIssuerName)
                           && (FlagInstrumentID || FlagErrorInstrumentID)
                           && (FlagIsinCode || FlagErrorIsinCode)
                           && (FlagPriceAmount || FlagErrorPriceAmount);

                return next;
            }
        }
    }
}