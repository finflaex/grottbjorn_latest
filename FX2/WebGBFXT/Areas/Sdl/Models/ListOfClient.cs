﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Finflaex.Support.Reflection;
using WebGBFXT.Areas.Sdl.Controllers;

#endregion

namespace WebGBFXT.Areas.Sdl.Models
{
    public class ListOfClient
    {
        public int? ClientID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Document { get; set; }
        public string DocumentType { get; set; }
        public DateTime? DateOfBirth { get; set; }

        public ClientWhereResult Compare(PersonInfo personInfo)
        {
            var result = new ClientWhereResult
            {
                Client = this
            };

            #region Surname

            if (LastName.IsNullOrWhiteSpace())
            {
                result.FlagErrorSurname = true;
                result.Message.Add("Фамилия пустая");
            }
            else
            {
                var v1 = ClearString(LastName);
                var v2 = ClearString(personInfo.FIOD.Surname);
                var c = v1.CompareExt(v2);
                if (c > 0.5 && c < 1)
                {
                    result.FlagErrorSurname = true;
                    result.Message.Add("Фамилия не совпадает полностью");
                }
                else if (c.CompareExt(1))
                {
                    if (v1 == v2)
                    {
                        result.FlagSurname = true;
                    }
                    else
                    {
                        result.FlagErrorSurname = true;
                        result.Message.Add("Фамилия не совпадает полностью");
                    }
                }
            }

            #endregion

            #region Name

            if (FirstName.IsNullOrWhiteSpace())
            {
                result.Message.Add("Имя пустое");
                result.FlagErrorName = true;
            }
            else
            {
                var v1 = ClearString(FirstName);
                var v2 = ClearString(personInfo.FIOD.Name);
                var c = v1.CompareExt(v2);
                if (c > 0.5 && c < 1)
                {
                    result.FlagErrorName = true;
                    result.Message.Add("Имя не совпадает полностью");
                }
                else if (c.CompareExt(1))
                {
                    if (v1 == v2)
                    {
                        result.FlagName = true;
                    }
                    else
                    {
                        result.FlagErrorName = true;
                        result.Message.Add("Имя не совпадает полностью");
                    }
                }
            }

            #endregion

            #region Patronymic

            var not_empty_main_patronymic = MiddleName.IsNotNullOrWhiteSpace();
            var not_empty_query_patronymic = personInfo.FIOD.Patronymic.IsNotNullOrWhiteSpace();

            if (not_empty_main_patronymic && !not_empty_query_patronymic)
            {
                result.FlagErrorPatronymic = true;
                result.Message.Add("Запрашиваемое отчество пустое");
            }
            else if (!not_empty_main_patronymic && not_empty_query_patronymic)
            {
                result.FlagErrorPatronymic = true;
                result.Message.Add("Отчество пустое");
            }
            else if (not_empty_main_patronymic && not_empty_query_patronymic)
            {
                var v1 = ClearString(MiddleName);
                var v2 = ClearString(personInfo.FIOD.Patronymic);
                var c = v1.CompareExt(v2);
                if (c > 0.5 && c < 1)
                {
                    result.FlagErrorPatronymic = true;
                    result.Message.Add("Отчество не совпадает полностью");
                }
                else if (c.CompareExt(1))
                {
                    if (v1 == v2)
                    {
                        result.FlagPatronymic = true;
                    }
                    else
                    {
                        result.FlagErrorPatronymic = true;
                        result.Message.Add("Отчество не совпадает полностью");
                    }
                }
            }
            else
            {
                result.FlagPatronymic = true;
            }

            #endregion

            #region Document

            if (personInfo.Document.Type == DocumentType)
            {
                result.FlagDocType = true;
            }
            else
            {
                result.FlagErrorDocType = false;
                result.Message.Add("Тип документа не совпадает");
            }

            var docNum_query = personInfo.Document.Serial.Append(personInfo.Document.Number);

            if (Document.IsNullOrWhiteSpace())
            {
                result.FlagErrorDocument = true;
                result.Message.Add("Серия и номер документа не указаны");
            }
            else
            {
                var v1 = ClearNumber(Document);
                var v2 = ClearNumber(docNum_query);
                var c = v1.CompareExt(v2);
                if (result.FlagDocType && c > 0.5 && c < 1)
                {
                    result.FlagErrorDocument = true;
                    result.Message.Add("Серия и номер документа не совпадают полностью");
                }
                else if (c.CompareExt(1))
                {
                    if (v1 == v2)
                    {
                        result.FlagDocument = true;
                    }
                    else
                    {
                        result.FlagErrorPatronymic = true;
                        result.Message.Add("Серия и номер документа не совпадают полностью");
                    }
                }
            }

            #endregion

            #region Date Birht

            var date_birthday_query = personInfo.FIOD.DateBirthday.ToDateTime("dd.MM.yyyy");

            if (DateOfBirth.IsNull() && date_birthday_query.NotNull())
            {
                result.FlagErrorDateBirth = true;
                result.Message.Add("Не указана дата рождения");
            }
            else if (DateOfBirth.NotNull() && date_birthday_query.NotNull())
            {
                var v1 = $"{DateOfBirth:dd.MM.yyyy}";
                var v2 = personInfo.FIOD.DateBirthday;
                var c = v1.CompareExt(v2);
                if (c.CompareExt(1))
                {
                    result.FlagDateBirth = true;
                }
                else if (c > 0.39 && c < 1)
                {
                    result.FlagErrorDateBirth = true;
                    result.Message.Add("Дата рождения не совпадают полностью");
                }
                else
                {
                    var old = date_birthday_query - DateTime.Today;

                    if (old.TotalDays > 25550 /* 70x365 */)
                    {
                        result.FlagErrorDateBirth = true;
                        result.Message.Add("Человеку больше 70 лет");
                    }
                    else if (old.TotalDays > 6570 /* 18x365 */)
                    {
                        result.FlagErrorDateBirth = true;
                        result.Message.Add("Человеку меньше 18 лет");
                    }
                    else
                    {
                        if (v1 == v2)
                        {
                            result.FlagDateBirth = true;
                        }
                        else
                        {
                            result.FlagErrorDateBirth = true;
                            result.Message.Add("Дата рождения не совпадают полностью");
                        }
                    }
                }
            }
            else
            {
                result.FlagDateBirth = true;
            }

            #endregion

            return result;
        }

        public static string ClearString(string value)
        {
            return value
                    .ToLower()
                    .Replace(new Dictionary<string, string>
                    {
                        ["ё"] = "e",
                        ["й"] = "и",
                        ["a"] = "а",
                        ["b"] = "б",
                        ["v"] = "в",
                        ["g"] = "г",
                        ["d"] = "д",
                        ["e"] = "е",
                        ["z"] = "з",
                        ["i"] = "и",
                        ["k"] = "к",
                        ["l"] = "л",
                        ["m"] = "м",
                        ["n"] = "н",
                        ["o"] = "о",
                        ["p"] = "п",
                        ["r"] = "р",
                        ["s"] = "с",
                        ["t"] = "т",
                        ["u"] = "у"
                    })
                    .ClearExcept("абвгдежзиклмнопрстуфхцчшщъыьэюя")
                ;
        }

        public static string ClearNumber(string value)
        {
            return value
                .ToLower()
                .Replace(new Dictionary<string, string>
                {
                    ["ё"] = "e",
                    ["й"] = "и",
                    ["a"] = "а",
                    ["b"] = "б",
                    ["v"] = "в",
                    ["g"] = "г",
                    ["d"] = "д",
                    ["e"] = "е",
                    ["z"] = "з",
                    ["i"] = "и",
                    ["k"] = "к",
                    ["l"] = "л",
                    ["m"] = "м",
                    ["n"] = "н",
                    ["o"] = "о",
                    ["p"] = "п",
                    ["r"] = "р",
                    ["s"] = "с",
                    ["t"] = "т",
                    ["u"] = "у"
                })
                .ClearExcept("0123456789абвгдежзиклмнопрстуфхцчшщъыьэюяabcdefghijklmnopqrstuvwxyz");
        }
    }

    public class ClientWhereResult
    {
        public ClientWhereResult()
        {
            Message = new List<string>();
        }

        public ListOfClient Client { get; set; }

        public bool Flag
            => FlagSurname
               && FlagName
               && FlagPatronymic
               && FlagDocType
               && FlagDocument
               && FlagDateBirth;

        public bool FlagWarning
        {
            get
            {
                //var name = (
                //    (FlagSurname && FlagName)
                //    || (FlagSurname && FlagErrorName)
                //    || (FlagErrorSurname && FlagName)
                //    || (FlagErrorSurname && FlagErrorName)
                //);

                //var doc = (
                //    (FlagDocType && FlagDocument)
                //    || (FlagDocType && FlagErrorDocument)
                //    || (!FlagDocType && FlagDocument)
                //);

                //var birth = (
                //    FlagDateBirth || FlagErrorDateBirth
                //);

                //return name && doc && birth;

                var next = (FlagSurname || FlagErrorSurname)
                           && (FlagName || FlagErrorName)
                           && (FlagPatronymic || FlagErrorPatronymic)
                           && (FlagDocument || FlagErrorDocument)
                           && (FlagDateBirth || FlagErrorDateBirth)
                           && (FlagDocType || FlagErrorDocType);

                return next;
            }
        }

        public bool FlagMessages => Message.Any();

        public bool FlagNext => Flag || FlagWarning;

        public List<string> Message { get; set; }

        public bool FlagSurname { get; set; }
        public bool FlagErrorSurname { get; set; }

        public bool FlagName { get; set; }
        public bool FlagErrorName { get; set; }

        public bool FlagPatronymic { get; set; }
        public bool FlagErrorPatronymic { get; set; }

        public bool FlagDocument { get; set; }
        public bool FlagErrorDocument { get; set; }

        public bool FlagDocType { get; set; }

        public bool FlagDateBirth { get; set; }
        public bool FlagErrorDateBirth { get; set; }
        public bool FlagErrorDocType { get; set; }
    }
}