﻿#region

using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Sdl.Models
{
    public enum TypeRequest
    {
        Unknown = 0,

        [EnumParser(Name = "Z")] Packet = 1,

        [EnumParser(Name = "P")] Personal = 2,

        [EnumParser(Name = "K")] Quitantion = 3
    }
}