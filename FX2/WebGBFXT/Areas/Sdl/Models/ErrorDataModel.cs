using System.Collections.Generic;

namespace WebGBFXT.Areas.Sdl.Models
{
    public class ErrorDataModel
    {
        public string Query_Surname { get; set; }
        public string Query_Name { get; set; }
        public string Query_Patronymic { get; set; }
        public string Query_DateBirthday { get; set; }
        public string Query_DocType { get; set; }
        public string Query_DocNumber { get; set; }
        public string Data_ClientID { get; set; }
        public string Data_InstrumentID { get; set; }
        public string Data_IssuerInn { get; set; }
        public string Data_IssuerName { get; set; }
        public string Data_IsinCode { get; set; }
        public string Data_Ticker { get; set; }
        public string Data_InstrumentTypeName { get; set; }
        public int Data_InstrumentTypeID { get; set; }
        public string Data_CategoryID { get; set; }
        public string Data_Amount { get; set; }
        public string Data_Nominal { get; set; }
        public string Data_Rate { get; set; }
        public string Data_PriceAmount { get; set; }
        public IEnumerable<string> Messages { get; set; }
    }
}