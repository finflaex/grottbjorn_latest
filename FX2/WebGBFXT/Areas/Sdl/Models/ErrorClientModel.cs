using System.Collections.Generic;

namespace WebGBFXT.Areas.Sdl.Models
{
    public class ErrorClientModel
    {
        public string Query_Surname { get; set; }
        public string Query_Name { get; set; }
        public string Query_Patronymic { get; set; }
        public string Query_DateBirthday { get; set; }
        public string Query_DocType { get; set; }
        public string Query_DocNumber { get; set; }
        public string Client_Surname { get; set; }
        public string Client_Name { get; set; }
        public string Client_Patroymic { get; set; }
        public string Client_DateBirthday { get; set; }
        public string Client_DocType { get; set; }
        public string Client_DocNumber { get; set; }
        public IEnumerable<string> Messages { get; set; }
        public string Client_Code { get; set; }
    }
}