using System.Collections.Generic;
using Antlr.Runtime.Misc;

namespace WebGBFXT.Areas.Sdl.Models
{
    public class ErrorSdlModel
    {
        public ErrorSdlModel()
        {
            Clients = new List<ErrorClientModel>();
            Data = new ListStack<ErrorDataModel>();
        }
        public List<ErrorClientModel> Clients { get; set; }
        public List<ErrorDataModel> Data { get; set; }
    }
}