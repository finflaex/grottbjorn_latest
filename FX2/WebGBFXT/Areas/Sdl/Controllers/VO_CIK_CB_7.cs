﻿#region

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Sdl.Controllers
{
    /// <remarks />
    [GeneratedCode("xsd", "4.6.1055.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot("Файл", Namespace = "", IsNullable = false)]
    public class RequestFile
    {
        [XmlElement("Запрос")]
        public QueryInfo Query { get; set; }

        [XmlElement("ИнфОрг")]
        public OraganizationInfo OraganizationInfo { get; set; }

        /// <remarks />
        [XmlElement("Персона")]
        public Person[] Persons { get; set; }

        /// <remarks />
        [XmlAttribute("ВерсФорм")]
        public string FormatVersion { get; set; }

        /// <remarks />
        [XmlAttribute("ТипИнф")]
        public string DataType { get; set; }

        /// <remarks />
        [XmlAttribute("ВерсПрог")]
        public string ProgVersion { get; set; }

        /// <remarks />
        [XmlAttribute("ИдФайл")]
        public string ___FileName { get; set; }

        //}
        //    set { ___FileName = value.ToString(); }
        //    get { return new RequestFileName(___FileName); }
        //{
        //public RequestFileName FileName

        //[XmlIgnore]
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class QueryInfo
    {
        [XmlAttribute("Ид")]
        public string ___Guid { get; set; }

        [XmlIgnore]
        public Guid Guid
        {
            get { return Guid.ParseExact(___Guid, "N"); }
            set { ___Guid = value.ToString("N").ToUpper(); }
        }

        [XmlAttribute("Дата")]
        public string ___Date { get; set; }

        [XmlIgnore]
        public DateTime Date
        {
            get { return ___Date.ToDateTime("dd.MM.yyyy").AsType<DateTime>(); }
            set { ___Date = value.ToString("dd.MM.yyyy"); }
        }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class OraganizationInfo
    {
        [XmlAttribute("Название")]
        public string Name { get; set; }

        [XmlAttribute("Адрес")]
        public string Address { get; set; }

        [XmlAttribute("ИНН")]
        public string ___INN { get; set; }

        [XmlIgnore]
        public long INN
        {
            get { return ___INN.ToLong(); }
            set { ___INN = value.ToString(); }
        }

        [XmlAttribute("ОГРН")]
        public string ___OGRN { get; set; }

        [XmlIgnore]
        public long OGRN
        {
            get { return ___OGRN.ToLong(); }
            set { ___OGRN = value.ToString(); }
        }

        [XmlAttribute("ФИО")]
        public string FIO { get; set; }

        [XmlAttribute("Телефон")]
        public string Phone { get; set; }

        [XmlAttribute("ЭлПочта")]
        public string Email { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class Person
    {
        [XmlElement("ПерсИнфо")]
        public PersonInfo PersonInfo { get; set; }

        [XmlElement("ЦенныеБумаги")]
        public Securities[] Securities { get; set; }

        [XmlElement("Счета")]
        public Account[] Account { get; set; }

        [XmlElement("СлужИнфо")]
        public ServiceInfo ServiceInfo { get; set; }

        [XmlAttribute("Ид")]
        public string ___Guid { get; set; }

        [XmlIgnore]
        public Guid Guid
        {
            get { return ___Guid.ToGuid(); }
            set { ___Guid = value.ToString("N").ToUpper(); }
        }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class PersonInfo
    {
        [XmlElement("ФИОД")]
        public FIOD FIOD { get; set; }

        [XmlElement("Документ")]
        public Document Document { get; set; }

        [XmlElement("Адрес")]
        public Address Address { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class FIOD
    {
        [XmlAttribute("Фамилия")]
        public string Surname { get; set; }

        [XmlAttribute("Имя")]
        public string Name { get; set; }

        [XmlAttribute("Отчество")]
        public string Patronymic { get; set; }

        [XmlAttribute("ДатаРожд")]
        public string DateBirthday { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class Document
    {
        [XmlAttribute("КодВидДок")]
        public string Type { get; set; }

        [XmlAttribute("Серия")]
        public string Serial { get; set; }

        [XmlAttribute("Номер")]
        public string Number { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class Address
    {
        [XmlAttribute("КодСубъекта")]
        public string RegionCode { get; set; }

        [XmlAttribute("НеконфАдрес")]
        public string AddressNonConf { get; set; }

        [XmlAttribute("КонфАдрес")]
        public string AddressConf { get; set; }

        [XmlAttribute("МестоРождения")]
        public string PlcaeBirthday { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class Securities
    {
        [XmlElement("Организация")]
        public Organization Organization { get; set; }

        [XmlElement("Бумага")]
        public Paper Paper { get; set; }

        [XmlElement("ИдБумаги")]
        public string PaperID { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class Organization
    {
        [XmlAttribute("Название")]
        public string Name { get; set; }

        [XmlAttribute("ИННОрг")]
        public string INN { get; set; }

        [XmlAttribute("ПИФ")]
        public string PIF { get; set; }

        [XmlAttribute("Статус")]
        public string Status { get; set; }

        [XmlAttribute("ДоляПроцент")]
        public string SharePercentage { get; set; }

        [XmlAttribute("ДоляЧислитель")]
        public string ShareNumerator { get; set; }

        [XmlAttribute("ДоляЗнаменатель")]
        public string ShareDenominator { get; set; }

        [XmlAttribute("КодСубОрг")]
        public string SubOrganization { get; set; }

        [XmlAttribute("НеконфАдрОрг")]
        public string AddressNonConf { get; set; }

        [XmlAttribute("КонфАдрОрг")]
        public string AddressConf { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class Paper
    {
        [XmlAttribute("КодВидБум")]
        public string Type { get; set; }

        [XmlAttribute("КодКатег")]
        public string Category { get; set; }

        [XmlAttribute("Колво")]
        public string Count { get; set; }

        [XmlAttribute("КолвоЧисл")]
        public string CountNumerator { get; set; }

        [XmlAttribute("КолвоЗнам")]
        public string CountDenominator { get; set; }

        [XmlAttribute("Стоимость")]
        public decimal Price { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class Account
    {
        [XmlElement("Счет")]
        public Score Score { get; set; }

        [XmlElement("КредитОрг")]
        public CreditOrganization CreditOrganization { get; set; }

        [XmlAttribute("ИдСчета")]
        public string ScoreID { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class Score
    {
        [XmlAttribute("ВидСчета")]
        public string Type { get; set; }

        [XmlAttribute("НомерСчета")]
        public string Number { get; set; }

        [XmlAttribute("Остаток")]
        public decimal Balance { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class CreditOrganization
    {
        [XmlAttribute("НаимКредит")]
        public string Name { get; set; }

        [XmlAttribute("КодСубКред")]
        public string RegionCode { get; set; }

        [XmlAttribute("НеконфАдрКред")]
        public string AddressNonConf { get; set; }

        [XmlAttribute("КонфАдрКред")]
        public string AddressConf { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class ServiceInfo
    {
        [XmlElement("ИДИнфо")]
        public CompanyIDS CompanyIDS { get; set; }

        [XmlElement("Наименование")]
        public CompanyInfos CompanyInfos { get; set; }

        [XmlAttribute("ВРНКанд", DataType = "integer")]
        public string Number { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class CompanyIDS
    {
        [XmlAttribute("КодСубъекта")]
        public string RegionCode { get; set; }

        [XmlAttribute("Кампания", DataType = "integer")]
        public string CompanyID { get; set; }

        [XmlAttribute("Система")]
        public byte SystemID { get; set; }
    }

    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class CompanyInfos
    {
        [XmlAttribute("Выборы")]
        public string CompanyName { get; set; }

        [XmlAttribute("Субъект")]
        public string RegionName { get; set; }

        [XmlAttribute("ДатаСвед")]
        public string Date { get; set; }
    }
}