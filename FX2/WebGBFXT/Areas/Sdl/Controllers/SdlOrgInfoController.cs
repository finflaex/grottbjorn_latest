﻿#region

using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using WebGBFXT.Areas.Sdl.Models;

#endregion

namespace WebGBFXT.Areas.Sdl.Controllers
{
    /// <summary>
    ///     Информация по нашей организации указываемая в ответе ЦИК
    /// </summary>
    [Roles(dbroles.admin, dbroles.cik)]
    public class SdlOrgInfoController : Controller
    {
        /// <summary>
        ///     Возвращаем представление
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        ///     Сохраняем данные
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ActionResult> SaveOrgInfo(
            SdlOrgInfoModel model)
            => await this.act(gb =>
            {
                gb.db.Files
                    .AsNoTracking()
                    .FirstOrDefault(file => file.Type == TypeFile.SdlOrgInfo)
                    .retIfNull(new RawFile
                    {
                        Type = TypeFile.SdlOrgInfo,
                        Raw = new byte[0]
                    })
                    .Action(file =>
                    {
                        file.Raw = model.AsBytes();
                        gb.db.Files.AddOrUpdate(file);
                        gb.Save();
                    });
            });
    }
}