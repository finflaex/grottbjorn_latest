﻿#region

using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Sdl.Models;

#endregion

namespace WebGBFXT.Areas.Sdl.Controllers
{
    /// <summary>
    ///     Управление контролерами
    /// </summary>
    [Roles(dbroles.admin, dbroles.cik)]
    public class UserSdlController : Controller
    {
        /// <summary>
        ///     Возвращаем представление
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        ///     Получение списка контролеров
        /// </summary>
        /// <param name="request"></param>
        /// <param name="closed"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                return (
                        from role in gb.db.Roles
                        where role.Key == dbroles.cik
                        from user in role.Logins
                        select user)
                    .AsEnumerable()
                    .Select(v => new UserSdlModel
                    {
                        ID = v.ID,
                        Caption = v.Caption,
                        Login = v.Name,
                        Password = v.Password,
                        EMail = v.InternalEMailAddress,
                        Phone = v.InternalPhoneNumber
                    })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        ///     Создание контролера
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public async Task<ActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            UserSdlModel model)
            => await this.json(gb =>
            {
                var read = (
                        from role in gb.db.Roles
                        where role.Key == dbroles.cik
                        from dep in gb.db.Departaments
                        where dep.Key == Departament.SdlDepartament
                        select new {role, dep})
                    .FirstOrDefault()
                    .ThrowIfNull("роль или подразделение не найдено");

                var user = new Login
                {
                    Caption = model.Caption,
                    Password = model.Password,
                    Roles = read.role.InArray(),
                    Departaments = read.dep.InArray(),
                    InternalEMailAddress = model.EMail,
                    InternalPhoneNumber = model.Phone
                };

                gb.db.Add(user);
                gb.Save();

                model.ID = user.ID;

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        ///     Обновление контролера
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            UserSdlModel model)
            => await this.json(gb =>
            {
                var user = gb.db.Logins
                    .Find(model.ID)
                    .ThrowIfNull("пользователь не найден");

                user.Caption = model.Caption;
                user.Name = model.Login;
                user.Password = model.Password;
                user.InternalPhoneNumber = model.Phone;
                user.InternalEMailAddress = model.EMail;

                gb.Save();

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        ///     Удаление контролера
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            UserSdlModel model)
            => await this.json(gb =>
            {
                gb.db.Logins.Find(model.ID).ActionIfNotNull(user =>
                {
                    gb.db.Logins.Remove(user);
                    gb.Save();
                });

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });
    }
}