﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Finflaex.Support.XML;
using WebGBFXT.Areas.Sdl.Models;
using WebGBFXT.Controllers;

#endregion

namespace WebGBFXT.Areas.Sdl.Controllers
{
    [Roles(dbroles.admin, dbroles.cik)]
    public class CikUploadController : Controller
    {
        public async Task<ActionResult> Index() => await this.func(View);

        [HttpPost]
        public async Task<ActionResult> Upload(
            IEnumerable<HttpPostedFileBase> files)
            => await this.act(gb =>
            {
                var file = files.WhereNotNull().FirstOrDefault();
                string request_file = null;
                try
                {
                    request_file = new StreamReader(file.InputStream, Encoding.UTF8).ReadToEnd();
                }
                catch
                {
                    
                }

                this.GetWS(gb.AuthorGuid, socket =>
                {
                    if (file == null)
                    {
                        socket.Error("Ошибка загрузки файла");
                        return;
                    }

                    var query = gb.db.Files
                        .Where(v => v.Type == TypeFile.SdlOrgInfo)
                        .ToArrayAsync();

                    query.Wait();

                    var orgInfo = query
                        .Result
                        .Select(v => v.Raw)
                        .FirstOrDefault()
                        .retIfNull(new byte[0])
                        .AsObject<SdlOrgInfoModel>()
                        .retIfNull(new SdlOrgInfoModel());

                    try
                    {
                        ErrorSdlModel debug;

                        var request = Validate(gb, socket, file, orgInfo, request_file, out debug);

                        var source_name = @"(\w)(\d{13})_(\d{2})(\d{2})(\d{2})_(\w)_(\d{4})[.](\w{3})"
                            .ToRegex()
                            .Matches(file.FileName)
                            .Cast<Match>()
                            .SelectMany(v => v.Groups.OfType<Group>())
                            .Select(v => v.Value)
                            .ToArray()
                            .FuncSelect(g => new
                            {
                                full_ogrn = $"{g[1]}{g[2]}",
                                ogrn = g[2],
                                date = new DateTime(g[5].ToInt() + 2000, g[4].ToInt(), g[3].ToInt()),
                                number = g[7],
                                name = $"{g[1]}{g[2]}_{g[3]}{g[4]}{g[5]}_{g[6]}_{g[7]}",
                                ext = g[8]
                            });

                        request.___FileName = $"D{orgInfo.OGRN}_{DateTime.UtcNow.AddHours(3):ddMMyy_HHmmss}_Z_{source_name.number}";
                        request.ProgVersion = "finflaex 2.0";

                        if (debug.Clients.Any() || debug.Data.Any())
                        {
                            var debug_xml = new RawFile
                            {
                                Raw = debug.ToJson().ToBytes(Encoding.UTF8),
                                Name = request.___FileName.Append(".see").Remove(0, 1).Prepend("W"),
                                Type = TypeFile.Cik
                            };

                            gb.db.Files.Add(debug_xml);
                            gb.Save();
                        }

                        if (!request.Persons.Any())
                            throw new EmptyError
                            {
                                FullSourceOGRN = source_name.full_ogrn,
                                FullMainOGRN = $"D{orgInfo.OGRN}",
                                ResultFileName =
                                    $"D{orgInfo.OGRN}_{DateTime.UtcNow.AddHours(3):ddMMyy_HHmmss}_K_{source_name.number}_1000_{source_name.full_ogrn}",
                                SourceFileName = source_name.name
                            };

                        var xml = new RawFile
                        {
                            Raw = request.ToXml().ToBytes(Encoding.UTF8),
                            Name = request.___FileName.Append(".xml"),
                            Type = TypeFile.Cik
                        };
                        gb.db.Files.Add(xml);
                        gb.Save();
                        socket.File(xml);
                    }
                    catch (FileNameErrorFormatException)
                    {
                        socket.Error("Имя файла неверное");
                    }
                    catch (FileErrorEncodingException err)
                    {
                        var xml = new RawFile
                        {
                            Raw = new Quitance
                                {
                                    AcknowledgementType = FileAcknowledgementType.Item8,
                                    ResultCode = "1002",
                                    ResultText = "Невозможно расшифровать файл обмена",
                                    To = err.FullSourceOGRN,
                                    From = err.FullMainOGRN,
                                    MessageID = err.ResultFileName,
                                    CorrelationMessageID = err.SourceFileName
                                }
                                .ToXml()
                                .ToBytes(Encoding.UTF8),
                            Name = err.ResultFileName.Append(".xml"),
                            Type = TypeFile.Cik
                        };
                        gb.db.Files.Add(xml);
                        gb.Save();
                        socket.File(xml);
                    }
                    catch (FileErrorSignException err)
                    {
                        var xml = new RawFile
                        {
                            Raw = new Quitance
                                {
                                    AcknowledgementType = FileAcknowledgementType.Item8,
                                    ResultCode = "1001",
                                    ResultText = "Некорректная электронная подпись",
                                    To = err.FullSourceOGRN,
                                    From = err.FullMainOGRN,
                                    MessageID = err.ResultFileName,
                                    CorrelationMessageID = err.SourceFileName
                                }
                                .ToXml()
                                .ToBytes(Encoding.UTF8),
                            Name = err.ResultFileName.Append(".xml"),
                            Type = TypeFile.Cik
                        };
                        gb.db.Files.Add(xml);
                        gb.Save();
                        socket.File(xml);
                    }
                    catch (XsdErrorValidation err)
                    {
                        var xml = new RawFile
                        {
                            Raw = new Quitance
                                {
                                    AcknowledgementType = FileAcknowledgementType.Item8,
                                    ResultCode = "1003",
                                    ResultText = err.Source,
                                    To = err.FullSourceOGRN,
                                    From = err.FullMainOGRN,
                                    MessageID = err.ResultFileName,
                                    CorrelationMessageID = err.SourceFileName
                                }
                                .ToXml()
                                .ToBytes(Encoding.UTF8),
                            Name = err.ResultFileName.Append(".xml"),
                            Type = TypeFile.Cik
                        };
                        gb.db.Files.Add(xml);
                        gb.Save();
                        socket.File(xml);
                    }
                    catch (XsdOutError err)
                    {
                        var xml = new RawFile
                        {
                            Raw = new Quitance
                                {
                                    AcknowledgementType = FileAcknowledgementType.Item8,
                                    ResultCode = "1003",
                                    ResultText = err.Source,
                                    To = err.FullSourceOGRN,
                                    From = err.FullMainOGRN,
                                    MessageID = err.ResultFileName,
                                    CorrelationMessageID = err.SourceFileName
                                }
                                .ToXml()
                                .ToBytes(Encoding.UTF8),
                            Name = err.ResultFileName.Append(".xml"),
                            Type = TypeFile.Cik
                        };
                        gb.db.Files.Add(xml);
                        gb.Save();
                        socket.File(xml);
                    }
                    catch (EmptyError err)
                    {
                        var xml = new RawFile
                        {
                            Raw = new Quitance
                                {
                                    AcknowledgementType = FileAcknowledgementType.Item10,
                                    ResultCode = "1000",
                                    ResultText = "Совпадения отсутствуют",
                                    To = err.FullSourceOGRN,
                                    From = err.FullMainOGRN,
                                    MessageID = err.ResultFileName,
                                    CorrelationMessageID = err.SourceFileName
                                }
                                .ToXml()
                                .ToBytes(Encoding.UTF8),
                            Name = err.ResultFileName.Append(".xml"),
                            Type = TypeFile.Cik
                        };
                        gb.db.Files.Add(xml);
                        gb.Save();
                        socket.File(xml);
                    }
                });
            });

        private RequestFile Validate(
            ActionData gb,
            WebSocket.RequestRecord socket,
            HttpPostedFileBase file,
            SdlOrgInfoModel orgInfo,
            string requestFile,
            out ErrorSdlModel debug)
        {
            socket.Clear();


            if (!@"(\w)(\d{13})_(\d{2})(\d{2})(\d{2})_(\w)_(\d{4})[.](\w{3})".ToRegex().IsMatch(file.FileName))
                throw new FileNameErrorFormatException();

            var upload = new RawFile
            {
                Raw = requestFile.ToBytes(Encoding.UTF8),
                Name = file.FileName,
                Type = TypeFile.Cik
            };
            gb.db.Files.Add(upload);
            gb.Save();

            var source_name = @"(\w)(\d{13})_(\d{2})(\d{2})(\d{2})_(\w)_(\d{4})[.](\w{3})"
                .ToRegex() 
                .Matches(file.FileName)
                .Cast<Match>()
                .SelectMany(v => v.Groups.OfType<Group>())
                .Select(v => v.Value)
                .ToArray()
                .FuncSelect(g => new
                {
                    full_ogrn = $"{g[1]}{g[2]}",
                    ogrn = g[2],
                    date = new DateTime(g[5].ToInt() + 2000, g[4].ToInt(), g[3].ToInt()),
                    number = g[7],
                    name = $"{g[1]}{g[2]}_{g[3]}{g[4]}{g[5]}_{g[6]}_{g[7]}",
                    ext = g[8]
                });

            if (source_name.ext == "enc")
                throw new FileErrorEncodingException
                {
                    FullSourceOGRN = source_name.full_ogrn,
                    FullMainOGRN = $"D{orgInfo.OGRN}",
                    ResultFileName =
                        $"D{orgInfo.OGRN}_{DateTime.UtcNow.AddHours(3):ddMMyy_HHmmss}_K_{source_name.number}_1002_{source_name.full_ogrn}",
                    SourceFileName = source_name.name
                };
            if (source_name.ext == "sig")
                throw new FileErrorSignException
                {
                    FullSourceOGRN = source_name.full_ogrn,
                    FullMainOGRN = $"D{orgInfo.OGRN}",
                    ResultFileName =
                        $"D{orgInfo.OGRN}_{DateTime.UtcNow.AddHours(3):ddMMyy_HHmmss}_K_{source_name.number}_1001_{source_name.full_ogrn}",
                    SourceFileName = source_name.name
                };

            var VO_CIK_CB_7 = this.GetResource("VO_CIK_CB_7").ToString(Encoding.UTF8);

            try
            {
                var body_validate = new fxcXmlValidator(VO_CIK_CB_7, requestFile);

                // проверяем соответствует ли файл всем правилам
                if (body_validate.Errors.Any())
                {
                    // если не соответствует то тыкаем носом в места несоответствия
                    var message = body_validate.Errors
                        .Select(error => $"[{error.LineNumber},{error.LinePosition},{error.Message}]")
                        .Join(";");

                    throw new XsdErrorValidation
                    {
                        Source = message,
                        FullSourceOGRN = source_name.full_ogrn,
                        FullMainOGRN = $"D{orgInfo.OGRN}",
                        ResultFileName =
                            $"D{orgInfo.OGRN}_{DateTime.UtcNow.AddHours(3):ddMMyy_HHmmss}_K_{source_name.number}_1003_{source_name.full_ogrn}",
                        SourceFileName = source_name.name
                    };
                }
            }
            catch (Exception err)
            {
                throw new XsdErrorValidation
                {
                    Source = err.Message,
                    FullSourceOGRN = source_name.full_ogrn,
                    FullMainOGRN = $"D{orgInfo.OGRN}",
                    ResultFileName =
                        $"D{orgInfo.OGRN}_{DateTime.UtcNow.AddHours(3):ddMMyy_HHmmss}_K_{source_name.number}_1003_{source_name.full_ogrn}",
                    SourceFileName = source_name.name
                };
            }

            // парсим файл
            var request = requestFile.FromXml<RequestFile>();
            // грузим запрос списка клиентов
            var CikCbQuery_ListClients = this
                .GetResource("CikCbQuery_ListClients")
                .ToString(Encoding.UTF8);
            // грузим запрос данных по клиенту
            var CikCbQuery_DataOfClient = this
                .GetResource("CikCbQuery_DataOfClient")
                .ToString(Encoding.UTF8);

            // строка подключения для выполнения запросов
            var connection_string =
                "Data Source=10.0.0.120;Integrated Security=False;User ID=WebMars;Password=webmars111;";

            var user = gb.db.Logins.Find(gb.AuthorGuid);

            var debugLog = new ErrorSdlModel();

            #region Result

            using (var cdb = new MyDbContext(connection_string))
            {
                request.OraganizationInfo = new OraganizationInfo
                {
                    Email = user.InternalEMailAddress,
                    Phone = user.InternalPhoneNumber,
                    FIO = user.Caption,
                    INN = orgInfo.INN.ToLong(),
                    OGRN = orgInfo.OGRN.ToLong(),
                    Name = orgInfo.Name,
                    Address = orgInfo.Address
                };

                request.Persons = cdb.Database
                    .SqlQuery<ListOfClient>(CikCbQuery_ListClients)
                    .ToArray()
                    .Distinct()
                    .SelectMany(client => request.Persons.Select(person => new {person, client}))
                    .AsParallel()
                    .Select(v => new
                    {
                        v.person,
                        v.client,
                        compare = v.client.Compare(v.person.PersonInfo)
                    })
                    .Where(v => v.compare.FlagNext)
                    .Select(v => new 
                    {
                        v.person,
                        v.client,
                        v.compare,
                        query_document = v.person.PersonInfo.Document.Serial
                            .Append(v.person.PersonInfo.Document.Number)
                            .FuncSelect(ListOfClient.ClearNumber),
                        main_document = ListOfClient.ClearNumber(v.client.Document),
                        main_date_birthday = $"{v.client.DateOfBirth:dd.MM.yyyy}"
                    })
                    .Where(v =>
                    {
                        if (v.compare.FlagWarning && !v.compare.Flag)
                        {
                            var error = new ErrorClientModel
                            {
                                Query_Surname = v.person.PersonInfo.FIOD.Surname,
                                Query_Name = v.person.PersonInfo.FIOD.Name,
                                Query_Patronymic = v.person.PersonInfo.FIOD.Patronymic,
                                Query_DateBirthday = v.person.PersonInfo.FIOD.DateBirthday,
                                Query_DocType = v.person.PersonInfo.Document.Type,
                                Query_DocNumber = v.query_document,
                                Client_Code = v.client.ClientID.ToString(),
                                Client_Surname = v.client.LastName,
                                Client_Name = v.client.FirstName,
                                Client_Patroymic = v.client.MiddleName,
                                Client_DateBirthday = v.main_date_birthday,
                                Client_DocType = v.client.DocumentType,
                                Client_DocNumber = v.main_document,
                                Messages = v.compare.Message
                            };
                            debugLog.Clients.Add(error);
                            socket.WarningClient(error);
                        }
                        return v.compare.Flag;
                    })
                    .Select(v =>
                    {
                        return new
                        {
                            v.person,
                            v.client,
                            data = cdb.Database
                                .SqlQuery<DataOfClient>(CikCbQuery_DataOfClient,
                                    new SqlParameter("Date",
                                        v.person.ServiceInfo.CompanyInfos.Date.ToDateTime("dd.MM.yyyy")),
                                    new SqlParameter("ClientID", v.client.ClientID))
                                .Select(d => new 
                                {
                                    item = d,
                                    compare = d.Compare()
                                })
                                .Where(d =>
                                {
                                    if (d.compare.FlagWarning && !d.compare.Flag)
                                    {
                                        var error = new ErrorDataModel
                                        {
                                            Query_Surname = v.person.PersonInfo.FIOD.Surname,
                                            Query_Name = v.person.PersonInfo.FIOD.Name,
                                            Query_Patronymic = v.person.PersonInfo.FIOD.Patronymic,
                                            Query_DateBirthday = v.person.PersonInfo.FIOD.DateBirthday,
                                            Query_DocType = v.person.PersonInfo.Document.Type,
                                            Query_DocNumber = v.query_document,
                                            Data_ClientID = d.item.ClientID.ToString(),
                                            Data_InstrumentID = d.item.InstrumentID.ToString(),
                                            Data_IssuerInn = d.item.INN,
                                            Data_IssuerName = d.item.IssuerName,
                                            Data_IsinCode = d.item.ISINCode,
                                            Data_Ticker = d.item.Ticker,
                                            Data_InstrumentTypeName = d.item.InstrumentTypeName,
                                            Data_InstrumentTypeID = d.item.InstrumentTypeID,
                                            Data_CategoryID = d.item.CategoryID,
                                            Data_Amount = d.item.Amount.ToString(),
                                            Data_Nominal = d.item.Nominal.ToString(),
                                            Data_Rate = d.item.Rate.ToString(),
                                            Data_PriceAmount = d.item.PriceAmount.ToString(),
                                            Messages = d.compare.Message
                                        };
                                        debugLog.Data.Add(error);
                                        socket.WarningData(error);
                                    }
                                    return d.compare.Flag;
                                })
                                .Select(d => new Securities
                                {
                                    PaperID = d.item.ISINCode,
                                    Organization = new Organization
                                    {
                                        Name = d.item.IssuerName,
                                        INN = d.item.INN
                                    },
                                    Paper = new Paper
                                    {
                                        Price =
                                            (decimal)
                                            (d.item.InstrumentTypeID == 91 ? d.item.Nominal : d.item.PriceAmount),
                                        Count = d.item.Amount.ToString(CultureInfo.InvariantCulture),
                                        Type = d.item.InstrumentTypeID.ToString(),
                                    }
                                })
                                .ToArray()
                        };
                    })
                    .Select(v => new Person
                    {
                        PersonInfo = v.person.PersonInfo,
                        Guid = v.person.Guid,
                        ServiceInfo = v.person.ServiceInfo,
                        Account = v.person.Account,
                        Securities = v.data
                    })
                    .ToArray();
            }

            #endregion

            debug = debugLog;

            return request;
        }
    }

    public static class helper
    {
        public static void Clear(
            this WebSocket.RequestRecord @this)
        {
            @this.Send(new
            {
                method = "clear"
            });
        }

        public static void Error(
            this WebSocket.RequestRecord @this,
            string message)
        {
            @this.Send(new
            {
                method = "error",
                message
            });
        }

        public static void WarningClient(
            this WebSocket.RequestRecord @this,
            object message)
        {
            @this.Send(new
            {
                method = "warning_client",
                message
            });
        }

        public static void WarningData(
            this WebSocket.RequestRecord @this,
            object message)
        {
            @this.Send(new
            {
                method = "warning_data",
                message
            });
        }

        public static void File(
            this WebSocket.RequestRecord @this,
            RawFile file)
        {
            @this.Send(new
            {
                method = "file",
                file.ID,
                file.Name
            });
        }
    }

    public class MyDbContext : DbContext
    {
        public MyDbContext(string con) : base(con)
        {
        }
    }

    public class FileNameErrorFormatException : Exception
    {
    }

    public class FileErrorEncodingException : Exception
    {
        public string FullSourceOGRN { get; set; }
        public string FullMainOGRN { get; set; }
        public string ResultFileName { get; set; }
        public string SourceFileName { get; set; }
    }

    public class FileErrorSignException : Exception
    {
        public string FullSourceOGRN { get; set; }
        public string FullMainOGRN { get; set; }
        public string ResultFileName { get; set; }
        public string SourceFileName { get; set; }
    }

    public class XsdErrorValidation : Exception
    {
        public string FullSourceOGRN { get; set; }
        public string FullMainOGRN { get; set; }
        public string ResultFileName { get; set; }
        public string SourceFileName { get; set; }
    }

    public class XsdOutError : Exception
    {
        public string FullSourceOGRN { get; set; }
        public string FullMainOGRN { get; set; }
        public string ResultFileName { get; set; }
        public string SourceFileName { get; set; }
    }

    public class EmptyError : Exception
    {
        public string FullSourceOGRN { get; set; }
        public string FullMainOGRN { get; set; }
        public string ResultFileName { get; set; }
        public string SourceFileName { get; set; }
    }
}