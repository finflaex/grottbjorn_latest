SELECT 
	C.ClientID,
	C.LastName,
	C.FirstName,
	C.MiddleName,
	D.SeriesNumber + D.DocumentNumber as Document,
    case D.DocumentTypeID
        when 1 then '21'
        when 2 then '7'
        when 16 then '10'
    end as DocumentType,
	C.DateOfBirth
  FROM [Clients].[dbo].[Clients] C
  left join [Clients].[dbo].[Documents] D on D.ClientID = C.ClientID
  where
  PersonTypeID = 1
  and ClientTypeID = 2
  and D.DocumentTypeID in (1,2,16)
  and C.Status = 1
  order by C.DateOfBirth