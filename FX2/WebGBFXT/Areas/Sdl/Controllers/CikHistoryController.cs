﻿#region

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

#endregion

namespace WebGBFXT.Areas.Sdl.Controllers
{
    [Roles(dbroles.admin, dbroles.cik)]
    public class CikHistoryController : Controller
    {
        public async Task<ActionResult> Index() => await this.func(View);

        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                return (
                        from file in gb.db.Files
                        where file.Type == TypeFile.Cik
                        from user in gb.db.Logins
                        where user.ID == file.CreateAuthorID
                        select new
                        {
                            file.ID,
                            file.Name,
                            file.DateCreate,
                            user.Caption
                        })
                    .AsEnumerable()
                    .Select(v => new CikNameModel(v.ID, v.Name, v.DateCreate, v.Caption))
                    .ToDataSourceResult(request);
            });

        public async Task<ActionResult> ShowFile(Guid id)
            => await this.func(gb =>
            {
                gb.GuidValue = new MainValue
                {
                    id = id
                };
                return View(gb);
            });

        public class CikNameModel
        {
            public CikNameModel(Guid id, string value, DateTimeOffset create, string user)
            {
                var arr =
                    @"^(\D)(\d{13})[_](\d{6})(?:[_](\d{6}))?[_](\D)[_](\d{4})(?:[_](\d{4}))?(?:_([F])(\d{13}))?[.](\w{3})"
                        .ToRegex()
                        .Matches(value)
                        .Cast<Match>()
                        .SelectMany(v => v.Groups.Cast<Group>())
                        .Select(v => v.Value)
                        .ToArray();

                ID = id;
                User = user;
                FileName = arr.FirstOrDefault();
                SourcePerfix = arr.Skip(1).FirstOrDefault();
                SourceOgrn = arr.Skip(2).FirstOrDefault();
                Date = arr.Skip(3)
                    .Take(2)
                    .WhereNotNull()
                    .Join("_")
                    .FuncSelect(qwe => qwe)
                    .ToDateTime("ddMMyy_", "ddMMyy_HHmmss");
                Type = arr.Skip(5).FirstOrDefault();
                Number = arr.Skip(6).FirstOrDefault();
                Code = arr.Skip(7).FirstOrDefault();
                DestinationPerfix = arr.Skip(8).FirstOrDefault();
                DestinationOgrn = arr.Skip(9).FirstOrDefault();
                Ext = arr.Skip(10).FirstOrDefault();
                CreateDateOnly = create.DateTime.Date;
                CreateTime = new DateTime().Add(create.DateTime - create.DateTime.Date);
            }

            public string FileName { get; set; }

            public Guid ID { get; set; }

            public string Create { get; set; }

            public DateTime CreateTime { get; set; }

            [DisplayName("Дата обработки")]
            public DateTime CreateDateOnly { get; set; }

            public string Ext { get; set; }

            public string DestinationOgrn { get; set; }

            public string DestinationPerfix { get; set; }

            public string Code { get; set; }

            public string Type { get; set; }

            [DisplayName("Дата запроса")]
            public DateTime? Date { get; set; }

            public string SourceOgrn { get; set; }

            public string SourcePerfix { get; set; }

            public string Number { get; set; }

            public bool IsRequest => SourcePerfix == "F";
            public bool IsQuitance => SourcePerfix == "D" && Code.NotNullOrWhiteSpace();
            public bool IsResponse => SourcePerfix == "D" && Code.IsNullOrWhiteSpace();
            public bool IsDebug => SourcePerfix == "W" && Ext == "see";

            [DisplayName("Имя")]
            public string CikName => $"{Date.Value.Year}_{Number}";

            [DisplayName("Ответственный")]
            public string User { get; set; }

            [DisplayName("Тип")]
            public string TypeString => IsRequest
                ? "Запрос"
                : IsQuitance
                    ? "Квитанция"
                    : IsResponse
                        ? "Ответ"
                        : IsDebug
                            ? "Лог"
                            : "Неизвестно";
        }
    }
}