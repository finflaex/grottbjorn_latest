﻿#region

using System.Web.Mvc;

#endregion

namespace WebGBFXT.Areas.GlobalEvents
{
    public class GlobalEventsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "GlobalEvents"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "GlobalEvents_default",
                "GlobalEvents/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}