﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Organization.Models;

#endregion

namespace WebGBFXT.Areas.GlobalEvents.Controllers
{
    /// <summary>
    /// Таблица мероприятий
    /// </summary>
    [NoCache]
    public class PlanController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Чтение списка мероприятий
        /// </summary>
        /// <param name="request"></param>
        /// <param name="allView"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            bool allView)
            => await this.json(gb =>
            {
                //var filter = (
                //        from user in gb.db.Logins
                //        where user.ID == gb.AuthorGuid
                //        from file in user.Files
                //        where file.Type == TypeFile.Config
                //        where file.Name == Login.Configs.ConsultantEventConfig
                //        select file
                //    )
                //    .FirstOrDefault()
                //    .retIfNull(new RawFile { Raw = new byte[0] })
                //    .FuncSelect(raw => raw.Raw.AsObject<ConsultantEventConfig>())
                //    .retIfNull(new ConsultantEventConfig());

                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    let isconsultant = user.Roles.Any(role => role.Key == dbroles.consultant)
                    from dep in (
                        from dep in gb.db.Departaments
                        where dep.Parent.Key == Departament.ConsultantDepartament
                        where allView || !isconsultant
                        select dep
                    )
                    .Concat(
                        from dep in user.Departaments
                        where dep.Parent.Key == Departament.ConsultantDepartament
                        where isconsultant
                        select dep
                    )
                    .Distinct()
                    from task in dep.Events
                    where task.Type == EventType.@event
                    orderby task.Start
                    select new TableEventModel
                    {
                        ID = task.ID,
                        Caption = task.Caption,
                        Comment = task.Description,
                        Departament = dep.Caption,
                        _Start = task.Next.Min(v => v.Start),
                        _End = task.Next.Max(v => v.End)
                        //Users = task.Next.SelectMany(v => v.Users).Distinct()
                    };

                return query
                    .AsEnumerable()
                    .Select(v =>
                    {
                        v.Start = v._Start?.DateTime;
                        v.End = v._End?.DateTime;
                        v.Comment = v.Comment.HtmlDecode().FromHtml();
                        return v;
                    })
                    .OrderByDescending(v => v.Start)
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Получение списка назначенных встреч в мероприятии
        /// </summary>
        /// <param name="request"></param>
        /// <param name="eventid">ид мероприятия</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> ReadTrip(
            [DataSourceRequest] DataSourceRequest request,
            Guid? eventid)
            => await this.json(gb =>
            {
                return (
                    from e in gb.db.Events
                    where e.ID == eventid
                    from task in e.Next
                    from task_event in task.Prev.Next
                    from task_meeting in task_event.Next
                    select new SchedullerTripModel
                    {
                        Description = task_meeting.Description,
                        ID = task_meeting.ID,
                        Type = task_meeting.Type,
                        _Start = task_meeting.Start ?? task_event.Start,
                        _End = task_meeting.End ?? task_event.End,
                        OrgName = task_meeting.Organization.ShortName ?? task_meeting.Organization.FullName,
                        ResID = (
                            from a in task_event.Address
                            where a.Type == AddressType.region
                            select a.TimeZone
                        ).FirstOrDefault()
                    })
                    .AsEnumerable()
                    .Select(v =>
                    {
                        v.Start = v._Start.ToTZDateTime(v.ResID);
                        v.End = v.Start.AddHours(3).ToTZDateTimeOffset(v.ResID).DateTime;
                        if (v.Type == EventType.eventSeminar)
                            v.OrgName = "Семинар";
                        v.Description = v.Description.HtmlDecode().FromHtml();
                        return v;
                    })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Получение списка организаций могущих принять участие в мероприятии
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Table(Guid? id)
            => await this.func(gb =>
            {
                if (id.IsNullOrEmpty())
                    return "error id".ToActionResult();

                ViewData["info"] = (
                    from task in gb.db.Events
                    where task.ID == id
                    select new TableEventModel
                    {
                        ID = task.ID,
                        Caption = task.Caption,
                        Comment = task.Description,
                        Departament = task.Departaments.FirstOrDefault().Caption,
                        _Start = task.Next.Min(v => v.Start),
                        _End = task.Next.Max(v => v.End)
                        //Users = task.Next.SelectMany(v => v.Users).Distinct()
                    }
                ).First();

                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    from root in gb.db.Events
                    where root.ID == id
                    from task in root.Next
                    where !task.Closed
                    from adr in task.Address
                    where adr.Type == task.Address.Select(a => a.Type).Max()
                    from org in adr.Organizations
                    from org_event in org.Events
                    where org_event.Type == EventType.progress
                    where !org_event.Closed
                    where org_event.End > gb.Now
                    where org_event.Users.Contains(user)
                    //from interest in org.Interests.DefaultIfEmpty()
                    //where task.Next.FirstOrDefault().Activities.Contains(interest.Activity.ActivityValue)
                    //where interest.Type != InterestType.notinterested
                    //where interest.Type != InterestType.conducted
                    select new TableEventModelItem
                    {
                        ID = org.ID,
                        OrgName = org.ShortName ?? org.FullName,
                        OrgAddress = org.FormatAddress
                    };

                var read = query.ToArray();
                return View(read);
            });

        /// <summary>
        /// Получение представления календаря мероприятия
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Calendar(Guid? id)
            => await this.func(gb =>
            {
                var read = (
                        from e in gb.db.Events
                        where e.ID == id
                        select new MainValue
                        {
                            id = e.ID,
                            value = e.Caption
                        }
                    )
                    .SingleOrDefault();

                if (read == null)
                    return Content("error event id").AsType<ActionResult>();

                return View(read);
            });

        public class TableEventModelIndex
        {
            public Login user { get; set; }
            public bool is_consultant { get; set; }
            public IEnumerable<TableEventModel> events { get; set; }
        }

        public class TableEventModel
        {
            public string Caption { get; set; }
            public string Comment { get; set; }
            public DateTime? Start { get; set; }
            public DateTime? End { get; set; }
            public IEnumerable<Login> Users { get; set; }
            public Guid ID { get; set; }
            public string Departament { get; set; }
            public DateTimeOffset? _Start { get; set; }
            public DateTimeOffset? _End { get; set; }
        }

        public class TableEventModelItem
        {
            public string OrgName { get; set; }
            public string OrgAddress { get; set; }
            public Guid ID { get; set; }
        }
    }
}