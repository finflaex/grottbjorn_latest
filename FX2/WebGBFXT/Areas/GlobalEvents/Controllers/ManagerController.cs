﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Controllers;

#endregion

namespace WebGBFXT.Areas.GlobalEvents.Controllers
{
    /// <summary>
    /// Администрирование мероприятий
    /// </summary>
    [NoCache]
    public class ManagerController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        public async Task<ActionResult> Index()
            => await this.func(gb =>
            {
                var query_accaunts =
                    from user in gb.db.Logins
                    where user.ID == gb.db.AuthorID
                    from dep in user.Departaments
                    where dep.Parent.Key == Departament.ConsultantDepartament
                    from acc in dep.Users
                    select new
                    {
                        Guid = acc.ID,
                        Value = acc.Caption
                    };

                ViewData["accaunts"] = query_accaunts
                    .ToArray()
                    .Select(v => new KeyValue
                    {
                        Value = v.Value,
                        Key = v.Guid.ToString()
                    })
                    .ToArray();

                ViewData["direction"] = gb
                    .db
                    .HelpActivities
                    .OrderBy(v => v.Order)
                    .Select(v => new KeyValue
                    {
                        Value = v.Caption,
                        Key = v.Key
                    })
                    .ToArray();

                return View();
            });

        /// <summary>
        /// Получение списка областей
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        public async Task<ActionResult> ReadRegion()
            => await this.json(gb =>
            {
                return gb.db
                    .Addresses
                    .Where(v => v.Type == AddressType.region)
                    .OrderBy(v => v.Name)
                    .Select(v => new GuidValue
                    {
                        Guid = v.ID,
                        Value = v.Name + " " + v.Suffix
                    })
                    .ToArray();
            });

        /// <summary>
        /// Получения адреса
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        public async Task<ActionResult> ReadAddress(Guid? id)
            => await this.json(gb =>
            {
                //using (var fdb = new DBFXFias.FiasDB())
                //{
                //    var sid = id.ToString();
                //    return fdb
                //        .AddrObj
                //        .Where(v=>v.PARENTGUID == sid)
                //        .Where(v=>v.AOLEVEL < 7)
                //        .Select(v=> new
                //        {
                //            id = v.AOGUID,
                //            name = v.FORMALNAME + " " + v.SHORTNAME
                //        })
                //        .OrderBy(v => v.name)
                //        .AsEnumerable()
                //        .Select(v => new GuidValue
                //        {
                //            Guid = v.id.ToGuid(),
                //            Value = v.name
                //        })
                //        .ToJsonResult();
                //}

                return gb.db
                    .Addresses
                    .Where(v => v.ParentID == id)
                    .Where(v => v.Type < AddressType.street)
                    .OrderBy(v => v.Name)
                    .Select(v => new GuidValue
                    {
                        Guid = v.ID,
                        Value = v.Name + " " + v.Suffix
                    })
                    .ToArray();
            });

        /// <summary>
        /// Получение списка мероприятий
        /// </summary>
        /// <param name="request"></param>
        /// <param name="closed"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            bool closed)
            => await this.json(gb =>
            {
                return (
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    from dep in user.Departaments
                    where dep.Parent.Key == Departament.ConsultantDepartament
                    from task in dep.Events
                    where closed || !task.Closed
                    where task.Type == EventType.@event
                    //where task.Start > gb.db.Now
                    select new EventModel
                    {
                        guid = task.ID,
                        caption = task.Caption,
                        description = task.Description,
                        start = task.Next.Min(v => v.Start),
                        end = task.Next.Max(v => v.End),
                        closed = task.Closed
                    })
                    .AsEnumerable()
                    .Select(v =>
                    {
                        v.start = v.start.ConvertTimeZone(gb.TimeZone);
                        v.end = v.end.ConvertTimeZone(gb.TimeZone);
                        v.description = v.description.HtmlDecode().FromHtml();
                        return v;
                    })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Создание мероприятия
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public async Task<ActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            EventModel model)
            => await this.json(gb =>
            {
                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    from dep in user.Departaments
                    where dep.Parent.Key == Departament.ConsultantDepartament
                    select dep;

                var departament = query.FirstOrDefault().ThrowIfNull("Пользователь не консультант !!!");

                var task = new Event
                {
                    Type = EventType.@event,
                    Caption = model.caption,
                    Description = model.description,
                    Closed = model.closed
                };

                departament.Events.Add(task);

                gb.db.Commit();

                model.guid = task.ID;

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Обновление мероприятия
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            EventModel model)
            => await this.json(gb =>
            {
                var task = gb.db
                    .Events
                    .Find(model.guid)
                    .ThrowIfNull("Мероприятие не найдено");

                task.Caption = model.caption;
                task.Description = model.description;
                task.Closed = model.closed;

                gb.db.Commit();

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Удаление мероприятия
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            EventModel model)
            => await this.json(gb =>
            {
                var task = gb.db
                    .Events
                    .Include(v => v.Next)
                    .FirstOrDefault(v => v.ID == model.guid)
                    .ThrowIfNull("Мероприятие не найдено");

                task.Next.ToArray().ForEach(child => { gb.db.Delete(child); });

                gb.db.Delete(task).Commit();

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Получение списка точек проведения мероприятий
        /// </summary>
        /// <param name="request"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> ReadItem(
            [DataSourceRequest] DataSourceRequest request,
            Guid? parent)
            => await this.json(gb =>
            {
                var tz = gb.db.Logins.Find(gb.AuthorGuid)?.TimeZone ?? 0;

                return (
                    from item in gb.db.Events
                    where item.Prev.ID == parent
                    orderby item.Start
                    select new
                    {
                        item.ID,
                        adress =
                        from adr in item.Address
                        select new
                        {
                            adr.Type,
                            adr.ID
                        },
                        item.Start,
                        item.End,
                        direction =
                        from act in item.Next
                            .Where(v => v.Type == EventType.eventSeminar)
                            .SelectMany(v => v.Activities)
                            .Distinct()
                        orderby act.Order
                        select new
                        {
                            act.Caption,
                            act.Key
                        },
                        accaunts =
                        from acc in item.Users
                        where acc.Roles.Any(v => v.Key == dbroles.consultant)
                        select new
                        {
                            acc.Caption,
                            acc.ID
                        },
                        item.Description,
                        seminar = item.Next.FirstOrDefault(v => v.Type == EventType.eventSeminar)
                    })
                    .AsEnumerable()
                    .Select(read =>
                    {
                        var result = new EventModelItem
                        {
                            guid = read.ID,
                            accauntNames = read.accaunts.Select(v => v.Caption).Join(", "),
                            accaunts = read.accaunts.Select(v => v.ID.ToString()),
                            description = read.Description,
                            directionNames = read.direction.Select(v => v.Caption).Join(", "),
                            direction = read.direction.Select(v => v.Key),
                            end = read.End.ToTZDateTime(tz),
                            start = read.Start.ToTZDateTime(tz),
                            item1 = read.adress.OrderBy(v => v.Type).Take(1).First().ID,
                            item2 = read.adress.OrderBy(v => v.Type).Skip(1).Take(1).FirstOrDefault()?.ID,
                            item3 = read.adress.OrderBy(v => v.Type).Skip(2).Take(1).FirstOrDefault()?.ID,
                            parent = parent.Value,
                            start_seminar = read.seminar?.Start.ToTZDateTime(tz),
                            end_seminar = read.seminar?.End.ToTZDateTime(tz),
                            seminar = read.seminar != null
                        };

                        result.address = gb.db.FullAddress(result.item3 ?? result.item2 ?? result.item1);

                        return result;
                    })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Создание точки проведения мероприятия
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public async Task<ActionResult> CreateItem(
            [DataSourceRequest] DataSourceRequest request,
            EventModelItem model)
            => await this.json(gb =>
            {
                model.accaunts = Request.ReadFormArrayValues("accaunts", "Key");
                model.direction = Request.ReadFormArrayValues("direction", "Key");

                var arr_adr = new[]
                {
                    model.item1,
                    model.item2,
                    model.item3
                };

                var arr_acc = model
                    .accaunts
                    .Select(v => v.ToGuid())
                    .ToArray();

                var query =
                    from parent in gb.db.Events
                    where parent.ID == model.parent
                    select new
                    {
                        parent,
                        address =
                        from adr in gb.db.Addresses
                        where arr_adr.Contains(adr.ID)
                        select adr,
                        dirs =
                        from dir in gb.db.HelpActivities
                        where model.direction.Contains(dir.Key)
                        select dir,
                        accs =
                        from acc in gb.db.Logins
                        where arr_acc.Contains(acc.ID)
                        select acc
                    };

                var read = query.FirstOrDefault()
                    .ThrowIfNull("Мероприятие не найдено");

                var task = new Event
                {
                    Prev = read.parent,
                    Start = model.start,
                    End = model.end,
                    Address = read.address.ToArray(),
                    Users = read.accs.ToArray(),
                    Type = EventType.eventItem,
                    Description = model.description
                };

                if (model.seminar)
                {
                    var seminar = new Event
                    {
                        Prev = task,
                        Start = model.start_seminar,
                        End = model.end_seminar,
                        Activities = read.dirs.ToArray(),
                        Users = read.accs.ToArray(),
                        Description = model.description,
                        Type = EventType.eventSeminar
                    };
                    gb.db.Add(seminar);
                }
                else
                {
                    gb.db.Add(task);
                }

                model.address = task.Address.OrderBy(v => v.Type).Select(v => $"{v.Name} {v.Suffix}.").Join(", ");
                model.accauntNames = task.Users.OrderByDescending(v => v.Caption).Select(v => v.Caption).Join(", ");
                model.directionNames = task.Activities.OrderByDescending(v => v.Order)
                    .Select(v => v.Caption)
                    .Join(", ");

                gb.db.Commit();

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Обновление точки проведения мероприятия
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public async Task<ActionResult> UpdateItem(
            [DataSourceRequest] DataSourceRequest request,
            EventModelItem model)
            => await this.json(gb =>
            {
                model.accaunts = Request.ReadFormArrayValues("accaunts", "Key");
                model.direction = Request.ReadFormArrayValues("direction", "Key");

                var arr_adr = new[]
                {
                    model.item1,
                    model.item2,
                    model.item3
                };

                var arr_acc = model
                    .accaunts
                    .Select(v => v.ToGuid())
                    .ToArray();

                var query =
                    from task in gb.db.Events
                    where task.ID == model.guid
                    select new
                    {
                        task,
                        seminar = task.Next.FirstOrDefault(v => v.Type == EventType.eventSeminar),
                        address =
                        from adr in gb.db.Addresses
                        where arr_adr.Contains(adr.ID)
                        select adr,
                        dirs =
                        from dir in gb.db.HelpActivities
                        where model.direction.Contains(dir.Key)
                        select dir,
                        accs =
                        from acc in gb.db.Logins
                        where arr_acc.Contains(acc.ID)
                        select acc
                    };

                var read = query.FirstOrDefault().ThrowIfNull("Мероприятие не найдено");

                read.task.Start = model.start;
                read.task.End = model.end;
                read.task.Address.Clear();
                read.task.Address.AddRange(read.address);
                read.task.Users.Clear();
                read.task.Users.AddRange(read.accs);
                read.task.Description = model.description;
                if (model.seminar)
                {
                    if (read.seminar != null)
                    {
                        read.seminar.Activities.Clear();
                        read.seminar.Activities.AddRange(read.dirs);
                        read.seminar.Start = model.start_seminar;
                        read.seminar.End = model.end_seminar;
                        read.seminar.Users.Clear();
                        read.seminar.Users.AddRange(read.accs);
                        read.seminar.Description = model.description;
                    }
                    else
                    {
                        var seminar = new Event
                        {
                            Prev = read.task,
                            Start = model.start_seminar,
                            End = model.end_seminar,
                            Activities = read.dirs.ToArray(),
                            Users = read.accs.ToArray(),
                            Description = model.description,
                            Type = EventType.eventSeminar
                        };
                        gb.db.Add(seminar);
                    }
                }
                else if (read.seminar != null)
                {
                    read.task.Next.Remove(read.seminar);
                    gb.db.Delete(read.seminar);
                }

                model.address = read.address.ToArray()
                    .OrderBy(v => v.Type)
                    .Select(v => $"{v.Name} {v.Suffix}.")
                    .Join(", ");
                model.accauntNames = read.accs.ToArray()
                    .OrderByDescending(v => v.Caption)
                    .Select(v => v.Caption)
                    .Join(", ");
                model.directionNames = read.dirs.ToArray()
                    .OrderByDescending(v => v.Order)
                    .Select(v => v.Caption)
                    .Join(", ");

                gb.db.Commit();

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Удаление точки проведения мероприятия
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant, dbroles.@operator)]
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public async Task<ActionResult> DestroyItem(
            [DataSourceRequest] DataSourceRequest request,
            EventModelItem model)
            => await this.func(gb =>
            {
                var query1 = from item in gb.db.Events
                    where item.ID == model.guid
                    select item;

                var query2 = from item in gb.db.Events
                    where item.ID == model.guid
                    from meeting in item.Next
                    select meeting;

                var query3 = from item in gb.db.Events
                    where item.ID == model.guid
                    from meeting in item.Next
                    from seminar in meeting.Next
                    select seminar;

                gb.db.BeginTracking();

                query3.Concat(query2).Concat(query1).ToArray().ForEach(read => { gb.db.Delete(read); });

                gb.db.EndTracking();
                gb.db.Commit();

                return model
                    .InArray()
                    .ToDataSourceResult(request)
                    .ToJsonResult();
            });


        public class EventModel
        {
            public string description { get; set; }
            public string caption { get; set; }
            public Guid guid { get; set; }
            public DateTimeOffset? start { get; set; }
            public DateTimeOffset? end { get; set; }
            public bool closed { get; set; }
        }

        public class EventModelItem
        {
            public IEnumerable<string> direction;
            public bool seminar { get; set; }
            public string address { get; set; }
            public Guid item1 { get; set; }
            public Guid? item2 { get; set; }
            public Guid? item3 { get; set; }

            public string item2str
            {
                get { return (item2 ?? Guid.Empty).ToString(); }
                set { item2 = value.ToGuid(); }
            }

            public string item3str
            {
                get { return (item3 ?? Guid.Empty).ToString(); }
                set { item3 = value.ToGuid(); }
            }

            public Guid guid { get; set; }
            public Guid parent { get; set; }
            public DateTime start { get; set; }
            public DateTime end { get; set; }

            public DateTime? start_seminar { get; set; }
            public DateTime? end_seminar { get; set; }

            public string description { get; set; }
            public IEnumerable<string> accaunts { get; set; }
            public string accauntNames { get; set; }
            public string directionNames { get; set; }
        }
    }
}