﻿#region

using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Administration.Models;

#endregion

namespace WebGBFXT.Areas.Administration.Controllers
{

    /// <summary>
    /// Таблица администрирования подразделений
    /// </summary>
    [NoCache]
    [Roles(dbroles.admin)]
    public class DepartamentController : Controller
    {
        /// <summary>
        /// Получение представления таблицы подразделений
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
            => await this.func(View);

        /// <summary>
        /// Создание нового подразделения
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">подразделение</param>
        /// <returns></returns>
        public async Task<ActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            DepartamentTableItem item)
            => await this.json(gb =>
            {
                var record = gb.db.Add(new Departament
                {
                    Caption = item.caption,
                    Description = item.description ?? string.Empty,
                    OldID = item.oldid
                });

                gb.db.Commit();

                item.id = record.ID.ToString("N");

                if (item.parentId == null)
                    item.parentId = string.Empty;

                return item
                    .InArray()
                    .ToTreeDataSourceResult(request, ModelState);
            });


        /// <summary>
        /// Чтение списка подразделений
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                return gb.db
                    .Departaments
                    .Select(v => new
                    {
                        v.ID,
                        v.Caption,
                        v.Description,
                        v.ParentID,
                        v.OldID
                    })
                    .AsEnumerable()
                    .Select(v => new DepartamentTableItem
                    {
                        caption = v.Caption,
                        id = v.ID.ToString("N"),
                        parentId = v.ParentID?.ToString("N") ?? string.Empty,
                        description = v.Description,
                        oldid = v.OldID
                    })
                    .ToTreeDataSourceResult(
                        request,
                        v => v.id,
                        v => v.parentId,
                        v => v);
            });

        /// <summary>
        /// Обновление данных о подразделении
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">подразделение</param>
        /// <returns></returns>
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            DepartamentTableItem item)
            => await this.json(gb =>
            {
                var id = item.id.ToGuid();
                var record = gb.db.Departaments.Find(id);

                if (record != null)
                {
                    record.Caption = item.caption ?? string.Empty;
                    record.Description = item.description ?? string.Empty;
                    record.ParentID = item.parentId.ToNullableGuid();
                    record.OldID = item.oldid;
                    gb.db.Commit();
                }

                if (item.parentId == null)
                    item.parentId = string.Empty;

                return item
                    .InArray()
                    .ToTreeDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Удаление подразделения
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">подразделение</param>
        /// <returns></returns>
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            DepartamentTableItem item)
            => await this.json(gb =>
            {
                var id = item.id.ToGuid();

                gb.db.Departaments.Find(id).ActionIfNotNull(record =>
                {
                    gb.db.Delete(record);
                    gb.db.Commit();
                });

                return item
                    .InArray()
                    .ToTreeDataSourceResult(request, ModelState);
            });
    }
}