﻿#region

using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Administration.Models;

#endregion

namespace WebGBFXT.Areas.Administration.Controllers
{
    /// <summary>
    /// Администрирование пользователей
    /// </summary>
    [NoCache]
    public class UsersController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Создание пользователя
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">пользователь</param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            UserTableItem item)
            => await this.json(gb =>
            {
                var key_roles = item.Roles?.Select(v => v.Key).ToArray() ?? new string[0];
                var key_departaments = item.Departaments?.Select(v => v.Key.ToGuid()).ToArray() ?? new Guid[0];

                var roles = gb.db.Roles.Where(v => key_roles.Contains(v.Key)).ToArray();
                var departaments = gb.db.Departaments.Where(v => key_departaments.Contains(v.ID)).ToArray();

                var record = new Login
                {
                    Caption = item.caption,
                    Name = item.login,
                    Password = item.password,
                    Roles = roles,
                    TimeZone = item.TimeZone,
                    Departaments = departaments,
                    InternalEMailAddress = item.email,
                    InternalPhoneNumber = item.phone,
                    OldID = item.oldid
                };

                gb.db.Add(record);
                gb.db.Commit();

                item.id = record.ID.ToString("N");

                return item
                    .InArray()
                    .ToTreeDataSourceResult(request, ModelState);
            });


        /// <summary>
        /// Чтение списка пользователей
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                return gb.db
                    .Logins
                    .Include(v => v.Roles)
                    .Include(v => v.Departaments)
                    .AsEnumerable()
                    .Select(v => new UserTableItem
                    {
                        id = v.ID.ToString("N"),
                        login = v.Name,
                        password = v.Password,
                        caption = v.Caption,
                        Roles = v.Roles.Select(r => new KeyValue
                        {
                            Key = r.Key,
                            Value = r.Caption
                        }),
                        Departaments = v.Departaments.Select(d => new KeyValue
                        {
                            Key = d.ID.ToString("N"),
                            Value = d.Caption
                        }),
                        email = v.InternalEMailAddress,
                        phone = v.InternalPhoneNumber,
                        oldid = v.OldID,
                        TimeZone = v.TimeZone
                    })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Обновления данных пользователя
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">данные пользователя</param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            UserTableItem item)
            => await this.json(gb =>
            {
                var id = item.id.ToGuid();
                var key_roles = item.Roles?.Select(v => v.Key).ToArray() ?? new string[0];
                var key_departaments = item.Departaments?.Select(v => v.Key.ToGuid()).ToArray() ?? new Guid[0];

                var roles = gb.db.Roles.Where(v => key_roles.Contains(v.Key)).ToList();
                var departaments = gb.db.Departaments.Where(v => key_departaments.Contains(v.ID)).ToList();

                gb.db.Logins.Find(id).ActionIfNotNull(record =>
                {
                    record.Caption = item.caption ?? string.Empty;
                    record.Roles.Clear();
                    record.Roles = roles;
                    record.Departaments.Clear();
                    record.Departaments = departaments;
                    record.Name = item.login;
                    record.Password = item.password;
                    record.InternalEMailAddress = item.email;
                    record.InternalPhoneNumber = item.phone;
                    record.OldID = item.oldid;
                    record.TimeZone = item.TimeZone;

                    gb.db.Commit();
                });

                return item
                    .InArray()
                    .ToTreeDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Удаление пользователя
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">пользователь</param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            DepartamentTableItem item)
            => await this.json(gb =>
            {
                var id = item.id.ToGuid();
                var record = gb.db.Logins.Include(v => v.Events).FirstOrDefault(v => v.ID == id);

                if (record != null)
                {
                    foreach (var task in record.Events)
                        gb.db.Events.Remove(task);
                    gb.db.Commit();
                    gb.db.Delete(record);
                    gb.db.Commit();
                }

                return item
                    .InArray()
                    .ToTreeDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Получение списка ролей
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> ReadRoles()
            => await this.json(gb =>
            {
                return gb.db
                    .Roles
                    .Select(v => new KeyValue
                    {
                        Key = v.Key,
                        Value = v.Caption
                    })
                    .ToArray();
            });

        /// <summary>
        /// Получение списка подразделений
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> ReadDepartaments()
            => await this.json(gb =>
            {
                return gb.db
                    .Departaments
                    .AsEnumerable()
                    .Select(v => new KeyValue
                    {
                        Key = v.ID.ToString("N"),
                        Value = v.Caption
                    })
                    .ToArray();
            });
    }
}