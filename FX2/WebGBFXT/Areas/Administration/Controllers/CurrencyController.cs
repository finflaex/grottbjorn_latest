﻿#region

using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

#endregion

namespace WebGBFXT.Areas.Administration.Controllers
{
    /// <summary>
    /// Список валют
    /// </summary>
    [NoCache]
    [Roles(dbroles.admin)]
    public class CurrencyController : Controller
    {
        /// <summary>
        /// Получение представления списка валют
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Чтение списка валют
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                return gb.db
                    .HelpCurrency
                    .OrderBy(v => v.Order)
                    .Select(v => new
                    {
                        v.Name,
                        v.Order,
                        v.Key
                    })
                    .AsEnumerable()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Обновление списка валют
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            HelpCurrency item)
            => await this.json(gb =>
            {
                gb.db.HelpCurrency.AddOrUpdate(item);
                gb.db.Commit();

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });
    }
}