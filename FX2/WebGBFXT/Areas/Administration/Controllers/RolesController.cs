﻿#region

using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Administration.Models;

#endregion

namespace WebGBFXT.Areas.Administration.Controllers
{
    /// <summary>
    /// Администрирование ролей
    /// </summary>
    [NoCache]
    public class RolesController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Создание роли
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">роль</param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            TableItem item)
            => await this.json(gb =>
            {
                var record = gb.db.Add(new Role
                {
                    Key = item.key,
                    Caption = item.caption,
                    Description = item.description ?? string.Empty
                });

                gb.db.Commit();

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Получение списка ролей
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                return gb.db
                    .Roles
                    .Select(v => new TableItem
                    {
                        key = v.Key,
                        caption = v.Caption,
                        description = v.Description
                    })
                    .AsEnumerable()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Обновление роли
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            TableItem item)
            => await this.json(gb =>
            {
                gb.db.Roles.Find(item.key).ActionIfNotNull(record =>
                {
                    record.Caption = item.caption ?? string.Empty;
                    record.Description = item.description ?? string.Empty;
                    record.Key = item.key;
                    gb.db.Commit();
                });

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Удаление роли
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">роль</param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            TableItem item)
            => await this.json(gb =>
            {
                gb.db.Roles.Find(item.key).ActionIfNotNull(record =>
                {
                    gb.db.Delete(record);
                    gb.db.Commit();
                });

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });
    }
}