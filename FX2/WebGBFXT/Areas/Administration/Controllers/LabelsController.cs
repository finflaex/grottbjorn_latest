﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.Attributes;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Administration.Models;

#endregion

namespace WebGBFXT.Areas.Administration.Controllers
{
    /// <summary>
    /// Администрирование таблицы меток
    /// </summary>
    [NoCache]
    [Roles(dbroles.admin)]
    public class LabelsController : Controller
    {
        /// <summary>
        /// Получение представления таблицы меток
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Создание метки
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">метка</param>
        /// <returns></returns>
        public async Task<ActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            TableLabelItem item)
            => await this.json(gb =>
            {
                var record = gb.db.Add(new Label
                {
                    Caption = item.Caption,
                    Description = item.Description,
                    Order = item.Order,
                    Type = item.Type,
                    UserID = item.UserID
                });

                gb.db.Commit();

                item.ID = record.ID;

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Чтение списка меток
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                return gb.db.Labels
                    .Select(v => new TableLabelItem
                    {
                        ID = v.ID,
                        Type = v.Type,
                        Caption = v.Caption,
                        UserID = v.UserID ?? Guid.Empty,
                        Order = v.Order,
                        Description = v.Description
                    })
                    .AsEnumerable()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Обновление метки
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">метка</param>
        /// <returns></returns>
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            TableLabelItem item)
            => await this.json(gb =>
            {
                gb.db.Labels.Find(item.ID).ActionIfNotNull(record =>
                {
                    record.Caption = item.Caption;
                    record.Description = item.Description;
                    record.Order = item.Order;
                    record.Type = item.Type;
                    record.UserID = item.UserID;
                    gb.db.Commit();
                });

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Удаление метки
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">метка</param>
        /// <returns></returns>
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            TableLabelItem item)
            => await this.json(gb =>
            {
                gb.db.Labels.Find(item.ID).ActionIfNotNull(record =>
                {
                    gb.db.Delete(record);
                    gb.db.Commit();
                });

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Получение списка типов меток
        /// </summary>
        /// <returns></returns>
        public ActionResult GetTypes()
        {
            return Enum
                .GetValues(typeof(TypeLabel))
                .Cast<TypeLabel>()
                .Select(v => new MainValue
                {
                    index = v.AsType<int>(),
                    value = v.AtValue()
                })
                .ToJsonResult();
        }
    }
}