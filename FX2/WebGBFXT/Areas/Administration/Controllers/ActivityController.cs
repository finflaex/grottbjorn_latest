﻿#region

using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Administration.Models;

#endregion

namespace WebGBFXT.Areas.Administration.Controllers
{
    /// <summary>
    /// Таблица активности подразделений в областях
    /// </summary>
    [NoCache]
    [Roles(dbroles.admin, dbroles.manager)]
    public class ActivityController : Controller
    {
        /// <summary>
        /// Получение стартового представления
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
            => await this.func(gb =>
            {
                var query =
                    from dep in gb.db.Departaments
                    where dep.Key == Departament.ConsultantDepartament
                    from child in gb.db.Departaments
                    where child.ParentID == dep.ID
                    select new
                    {
                        key = child.ID,
                        caption = child.Caption
                    };


                ViewData["departament"] = query
                    .AsEnumerable()
                    .Select(v => new TableItem
                    {
                        key = v.key.ToString(),
                        caption = v.caption
                    })
                    .Add(new TableItem
                    {
                        caption = " - пусто - ",
                        key = Guid.Empty.ToString()
                    })
                    .OrderBy(v => v.caption)
                    .ToArray();

                return View();
            });

        /// <summary>
        /// Получение списка областей
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> ReadAddress(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                return gb.db
                    .Addresses
                    .Where(v => v.Type == AddressType.region)
                    .OrderBy(v => v.Name)
                    .AsEnumerable()
                    .Select(v => new TableItem
                    {
                        key = v.ID.ToString(),
                        caption = $"{v.Name} {v.Suffix}."
                    })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Получение активности подразделений по области
        /// </summary>
        /// <param name="request"></param>
        /// <param name="id">ид области</param>
        /// <returns></returns>
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            Guid id)
            => await this.json(gb =>
            {
                var list = (
                        from adr in gb.db.Addresses
                        where adr.ID == id
                        from act in adr.Activity
                        orderby act.ActivityValue.Order
                        select new
                        {
                            id = act.ID,
                            name = act.ActivityValue.Caption,
                            key = act.ActivityValue.Key,
                            dep = act.DepartamentID,
                            order = act.ActivityValue.Order
                        })
                    .ToArray();

                var dirs = gb.db.HelpActivities
                    .AsNoTracking()
                    .ToArray();

                var news = dirs
                    .Select(v => v.Key)
                    .Except(list
                        .Select(v => v.key));

                var adds = dirs
                    .Where(v => news.Contains(v.Key))
                    .Select(v => new ActivityTableItem
                    {
                        DirectionName = v.Caption,
                        DirectionKey = v.Key,
                        ID = null,
                        DepartamentKey = Guid.Empty.ToString(),
                        AddressKey = id.ToString(),
                        Order = v.Order
                    });

                return list
                    .Select(v => new ActivityTableItem
                    {
                        ID = v.id.ToString(),
                        DepartamentKey = (v.dep ?? Guid.Empty).ToString(),
                        DirectionKey = v.key,
                        DirectionName = v.name,
                        AddressKey = id.ToString()
                    })
                    .Concat(adds)
                    .ToArray()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Обновление активности подразделений по области
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            ActivityTableItem model)
            => await this.act(gb =>
            {
                var id = model.ID?.ToGuid() ?? Guid.NewGuid();
                var read_dep = model.DepartamentKey.ToGuid();
                Guid? dep = null;
                if (read_dep.NotEmpty()) dep = read_dep;
                var adr = model.AddressKey.ToGuid();

                var activity = new Activity
                {
                    ID = id,
                    AddressID = adr,
                    DepartamentID = dep,
                    ActivityID = model.DirectionKey
                };

                gb.db.Activities.AddOrUpdate(activity);
                gb.db.Commit();

                model.ID = activity.ID.ToString();
            });
    }
}