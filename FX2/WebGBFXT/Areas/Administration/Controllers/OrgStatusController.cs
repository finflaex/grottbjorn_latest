﻿#region

using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Administration.Models;

#endregion

namespace WebGBFXT.Areas.Administration.Controllers
{
    /// <summary>
    /// Администрирование статусов организации
    /// </summary>
    [NoCache]
    public class OrgStatusController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Создание статуса
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">статус</param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            TableItem item)
            => await this.json(gb =>
            {
                var record = gb.db.Add(new OrgStatus
                {
                    Key = item.key,
                    Caption = item.caption,
                    Description = item.description ?? string.Empty
                });

                gb.db.Commit();

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Получения списка статусов
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                return gb.db
                    .OrgStatuses
                    .Select(v => new TableItem
                    {
                        key = v.Key,
                        caption = v.Caption,
                        description = v.Description
                    })
                    .AsEnumerable()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Обновление статуса
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">статус</param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            TableItem item)
            => await this.json(gb =>
            {
                gb.db.OrgStatuses.Find(item.key).ActionIfNotNull(record =>
                {
                    record.Caption = item.caption ?? string.Empty;
                    record.Description = item.description ?? string.Empty;
                    record.Key = item.key;
                    gb.db.Commit();
                });

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Удаление статуса
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item">статус</param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            TableItem item)
            => await this.json(gb =>
            {
                gb.db.OrgStatuses.Find(item.key).ActionIfNotNull(record =>
                {
                    gb.db.Delete(record);
                    gb.db.Commit();
                });

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });
    }
}