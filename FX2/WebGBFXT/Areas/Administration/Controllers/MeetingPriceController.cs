﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Administration.Models;

#endregion

namespace WebGBFXT.Areas.Administration.Controllers
{
    /// <summary>
    /// Администрирование таблицы автоматической оценки встреч
    /// </summary>
    [NoCache]
    public class MeetingPriceController : Controller
    {
        /// <summary>
        /// Получение представления таблицы
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Получение списка подразделений
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> ReadDepartaments()
            => await this.json(gb =>
            {
                return gb.db
                    .Departaments
                    .Where(v => v.Key == Departament.ConsultantDepartament)
                    .SelectMany(v => v.Childs)
                    .Select(v => new GuidValue
                    {
                        Guid = v.ID,
                        Value = v.Caption
                    })
                    .ToArray();
            });

        /// <summary>
        /// Получение оценок по подразделению
        /// </summary>
        /// <param name="request"></param>
        /// <param name="depid">ид подразделения</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin)]
        public async Task<ActionResult> ReadPrice(
            [DataSourceRequest] DataSourceRequest request,
            Guid? depid)
            => await this.json(gb =>
            {
                return gb.db
                    .Departaments
                    .Where(v => v.ID == depid)
                    .SelectMany(v => v.Prices)
                    .Select(year => new
                    {
                        year.ID,
                        year.Value,
                        LprAndRaz = year.Prices
                            .OrderByDescending(v => v.DateCreate)
                            .Where(v => v.HasLPR && v.PorogRaz)
                            .Select(v => v.Value)
                            .DefaultIfEmpty(0)
                            .FirstOrDefault(),
                        LprOrRaz = year.Prices
                            .OrderByDescending(v => v.DateCreate)
                            .Where(v => v.HasLPR ^ v.PorogRaz)
                            .Select(v => v.Value)
                            .DefaultIfEmpty(0)
                            .FirstOrDefault(),
                        NotLprAndNotRaz = year.Prices
                            .OrderByDescending(v => v.DateCreate)
                            .Where(v => !v.HasLPR && !v.PorogRaz)
                            .Select(v => v.Value)
                            .DefaultIfEmpty(0)
                            .FirstOrDefault()
                    })
                    .OrderBy(v => v.Value)
                    .AsEnumerable()
                    .Select(rec => new GridPriceModel
                    {
                        ID = rec.ID.ToString("N"),
                        Value = rec.Value,
                        LprAndRaz = rec.LprAndRaz,
                        LprOrRaz = rec.LprOrRaz,
                        NotLprAndNotRaz = rec.NotLprAndNotRaz
                    })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Создание оценки подраздления
        /// </summary>
        /// <param name="request"></param>
        /// <param name="models">оценка</param>
        /// <param name="depid">ид подразделения</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin)]
        public async Task<ActionResult> CreatePrice(
            [DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")] IEnumerable<GridPriceModel> models,
            Guid? depid)
            => await this.json(gb =>
            {
                if (depid.IsNullOrEmpty())
                    return "error depid".ToTextResult();

                foreach (var item in models)
                {
                    var year = new MeetingYear
                    {
                        DepartamentID = depid.Value,
                        Value = item.Value
                    };

                    item.ID = year.ID.ToString();

                    year.Prices.Add(new MeetingPrice
                    {
                        Value = item.LprAndRaz,
                        HasLPR = true,
                        PorogRaz = true
                    });

                    year.Prices.Add(new MeetingPrice
                    {
                        Value = item.LprOrRaz,
                        HasLPR = true,
                        PorogRaz = false
                    });

                    year.Prices.Add(new MeetingPrice
                    {
                        Value = item.NotLprAndNotRaz,
                        HasLPR = false,
                        PorogRaz = false
                    });

                    gb.db.Add(year);
                }

                gb.db.Commit();

                return models.ToDataSourceResult(request);
            });

        /// <summary>
        /// Удаление оценок
        /// </summary>
        /// <param name="request"></param>
        /// <param name="models">оценки</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin)]
        public async Task<ActionResult> DestroyPrice(
            [DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")] IEnumerable<GridPriceModel> models)
            => await this.json(gb =>
            {
                models.ForEach(item =>
                {
                    var id = item.ID.ToGuid();
                    var year = gb.db.MeetingYears.Find(id);
                    gb.db.Delete(year);
                });

                gb.db.Commit();

                return models.ToDataSourceResult(request);
            });

        /// <summary>
        /// Обновление оценок
        /// </summary>
        /// <param name="request"></param>
        /// <param name="models">оценки</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin)]
        public async Task<ActionResult> UpdatePrice(
            [DataSourceRequest] DataSourceRequest request,
            [Bind(Prefix = "models")] IEnumerable<GridPriceModel> models)
            => await this.json(gb =>
            {
                models.ForEach(item =>
                {
                    var id = item.ID.ToGuid();

                    var year = gb.db
                        .MeetingYears
                        .Include(v => v.Prices)
                        .First(v => v.ID == id);

                    year.Value = item.Value;

                    var value2 = year
                        .Prices
                        .First(v => v.HasLPR && v.PorogRaz);

                    value2.Value = item.LprAndRaz;

                    var value1 = year
                        .Prices
                        .First(v => v.HasLPR && !v.PorogRaz);

                    value1.Value = item.LprOrRaz;

                    var value0 = year
                        .Prices
                        .First(v => v.HasLPR && !v.PorogRaz);

                    value0.Value = item.NotLprAndNotRaz;
                });

                gb.db.Commit();

                return models.ToDataSourceResult(request);
            });

        /// <summary>
        /// Чтение разовых объемов подразделение
        /// </summary>
        /// <param name="depid">ид подразделения</param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> ReadRazCurrencyValue(
            Guid? depid)
            => await this.json(gb =>
            {
                return gb.db
                    .Departaments
                    .Where(v => v.ID == depid)
                    .Select(v => v.MinCurrencyRaz)
                    .DefaultIfEmpty(0)
                    .FirstOrDefault()
                    .FuncSelect(v => new
                    {
                        value = v
                    });
            });

        /// <summary>
        /// Запись разовых объемов подразделения
        /// </summary>
        /// <param name="depid">ид подразделения</param>
        /// <param name="value">объем</param>
        /// <returns></returns>
        [Roles(dbroles.admin)]
        public async Task<ActionResult> WriteRazCurrencyValue(
            Guid? depid, decimal value = 0)
            => await this.act(gb =>
            {
                gb.db
                    .Departaments
                    .Find(depid)
                    .ActionIfNotNull(dep =>
                    {
                        dep.MinCurrencyRaz = value;
                        gb.db.Commit();
                    });
            });
    }
}