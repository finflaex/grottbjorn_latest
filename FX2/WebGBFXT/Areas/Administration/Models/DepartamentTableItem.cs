namespace WebGBFXT.Areas.Administration.Models
{
    public class DepartamentTableItem
    {
        public string id { get; set; }
        public string caption { get; set; }
        public string description { get; set; }
        public string parentId { get; set; }
        public int oldid { get; set; }
    }
}