﻿namespace WebGBFXT.Areas.Administration.Models
{
    public class GridYearItem
    {
        public string ID { get; set; }
        public decimal Value { get; set; }
    }
}