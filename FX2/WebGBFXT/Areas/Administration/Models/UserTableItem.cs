#region

using System.Collections.Generic;
using Finflaex.Support.MVC.Models;

#endregion

namespace WebGBFXT.Areas.Administration.Models
{
    public class UserTableItem
    {
        public string id { get; set; }
        public string caption { get; set; }
        public IEnumerable<KeyValue> Roles { get; set; }
        public IEnumerable<KeyValue> Departaments { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public int oldid { get; set; }
        public int TimeZone { get; set; }
    }
}