#region

using System;
using BaseDBFX.Table;

#endregion

namespace WebGBFXT.Areas.Administration.Models
{
    public class TableItem
    {
        public string key { get; set; }
        public string caption { get; set; }
        public string description { get; set; }
        public int order { get; set; }
    }

    public class TableLabelItem
    {
        public Guid? ID { get; set; }
        public string Caption { get; set; }
        public TypeLabel Type { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
        public Guid? UserID { get; set; }
    }
}