﻿namespace WebGBFXT.Areas.Administration.Models
{
    public class ActivityTableItem
    {
        public string ID { get; set; }
        public string AddressKey { get; set; }
        public string DirectionKey { get; set; }
        public string DirectionName { get; set; }
        public string DepartamentKey { get; set; }
        public int Order { get; set; }
    }
}