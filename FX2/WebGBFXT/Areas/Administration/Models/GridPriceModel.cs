﻿namespace WebGBFXT.Areas.Administration.Models
{
    public class GridPriceModel
    {
        public string ID { get; set; }
        public decimal Value { get; set; }
        public decimal LprAndRaz { get; set; }
        public decimal LprOrRaz { get; set; }
        public decimal NotLprAndNotRaz { get; set; }
    }
}