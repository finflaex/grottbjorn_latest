﻿#region

using System.Collections.Generic;

#endregion

namespace WebGBFXT.Areas.Operator.Models
{
    public class QuestMessageModel
    {
        public string address;
        public string id;
        public string inn;
        public string name;
        public IEnumerable<string> users;
    }
}