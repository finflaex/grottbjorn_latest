﻿#region

using System;
using Kendo.Mvc.UI;

#endregion

namespace WebGBFXT.Areas.Operator.Models
{
    public class Meeting_Event_Model : ISchedulerEvent
    {
        public Meeting_Event_Model()
        {
            Title = "";
            Description = "";
        }

        public string ID { get; set; }


        public string AccID { get; set; }
        public string RecordID { get; set; }
        public string TypeKey { get; set; }
        public string OrgName { get; set; }
        public string DepartamentID { get; set; }
        public bool Notify { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsAllDay { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string StartTimezone { get; set; }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }
    }
}