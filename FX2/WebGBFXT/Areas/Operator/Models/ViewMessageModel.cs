﻿#region

using System.Collections.Generic;

#endregion

namespace WebGBFXT.Areas.Operator.Models
{
    public class ViewMessageModel
    {
        public string name { get; set; }
        public string fullname { get; set; }
        public string address { get; set; }
        public string inn { get; set; }
        public IEnumerable<string> users { get; set; }
    }
}