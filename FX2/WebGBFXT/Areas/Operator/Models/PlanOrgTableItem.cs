﻿#region

using System;
using BaseDBFX.Table;

#endregion

namespace WebGBFXT.Areas.Operator.Models
{
    public class PlanOrgTableItem
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public EventType Type { get; set; }
        public string TypeString { get; set; }
        public string ClientCode { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public Guid ID { get; set; }
        public string Description { get; set; }
        public string Accaunt { get; set; }
        public DateTime? DateTime { get; set; }
        public DateTime? DateTimeFilter { get; set; }
        public string Inn { get; set; }
        public string Status { get; internal set; }
        public string FullName { get; set; }
        public string SearchName { get; set; }
        public string OGRN { get; set; }

        public DateTime? LastDate { get; set; }
        public DateTimeOffset? LastDateOffset { get; set; }
        public DateTimeOffset? NextDateOffset { get; set; }
        public DateTime? NextDate { get; set; }
        public EventType? LastSourceType { get; set; }
        public EventType? NextSourceType { get; set; }
        public string LastType { get; set; }
        public string NextType { get; set; }
        public Guid EventID { get; set; }
    }
}