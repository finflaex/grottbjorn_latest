﻿namespace WebGBFXT.Areas.Operator.Models
{
    public class Meeting_Index_Model
    {
        public string DepartamentID { get; set; }
        public string OrgID { get; set; }
        public string RecordID { get; set; }
    }
}