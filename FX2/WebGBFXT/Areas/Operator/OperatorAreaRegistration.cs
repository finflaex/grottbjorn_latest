﻿#region

using System.Web.Mvc;

#endregion

namespace WebGBFXT.Areas.Operator
{
    public class OperatorAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Operator"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Operator_default",
                "Operator/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}