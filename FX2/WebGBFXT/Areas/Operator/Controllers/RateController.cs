﻿#region

using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

#endregion

namespace WebGBFXT.Areas.Operator.Controllers
{
    /// <summary>
    /// Таблица напоминаний оценки встреч
    /// </summary>
    [NoCache]
    [Roles]
    public class RateController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Чтение списка напоминаний
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                return (
                        from user in gb.db.Logins
                        where user.ID == gb.AuthorGuid
                        from task in user.Events
                        where task.Type == EventType.meetingRate
                        where !task.Closed
                        orderby task.Start
                        select new
                        {
                            Name = task.Organization.ShortName ?? task.Organization.FullName,
                            Address = task.Organization.Address
                                          .Where(a => a.Type == AddressType.region)
                                          .Select(a => a.Name)
                                          .FirstOrDefault()
                                      ?? task.Organization.FormatAddress
                                      ?? task.Organization.RawAddress,
                            task.Organization.Description,
                            task.Organization.ID
                        })
                    .ToDataSourceResult(request);
            });
    }
}