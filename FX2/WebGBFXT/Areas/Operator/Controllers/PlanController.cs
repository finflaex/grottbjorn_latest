﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Models;
using BaseDBFX.Table;
using Finflaex.Support.Attributes;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Operator.Models;

#endregion

namespace WebGBFXT.Areas.Operator.Controllers
{
    /// <summary>
    /// План обзвона
    /// </summary>
    [NoCache]
    [Roles]
    public class PlanController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Чтение списка событий
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                var db = gb.db;
                db.Database.CommandTimeout = 180;

                var tz = db.Logins.Find(gb.AuthorGuid)?.TimeZone ?? 0;

                var cfg = (
                              from user in db.Logins
                              where user.ID == db.AuthorID
                              from file in user.Files
                              where file.Type == TypeFile.Config
                              where file.Name == Login.Configs.OperatorPlanConfig
                              select file)
                          .FirstOrDefault()
                          ?
                          .Raw.AsObject<OperatorPlanConfig>()
                          ?? new OperatorPlanConfig
                          {
                              Accaunt = db.AuthorID,
                              Types = new List<EventType>(),
                              Start = DateTime.Now,
                              End = DateTime.Now
                          };

                var start = cfg.Start.Date.ToTZDateTimeOffset(gb.TimeZone);
                var end = cfg.End.Date.AddDays(1).AddMilliseconds(-1).ToTZDateTimeOffset(gb.TimeZone);

                var admin_roles = new[]
                {
                    dbroles.admin,
                    dbroles.developer
                };

                if (!cfg.Types.Any())
                    cfg.Types = new[]
                    {
                        EventType.unknown,
                        EventType.callUp,
                        EventType.callTransfer,
                        EventType.meetingRate,
                        EventType.meetingEvent,
                        EventType.meeting,
                        EventType.callSeminar
                    };

                return (
                        from author in db.Logins
                        where author.ID == db.AuthorID
                        let manager = author.Roles.Any(v => v.Key == dbroles.manager)
                        let consultant = author.Roles.Any(v => v.Key == dbroles.consultant)
                        let admin = author.Roles.Select(v => v.Key).Intersect(admin_roles).Any()
                        let filter = admin || manager && !consultant
                        from user in db.Logins
                        where filter ? cfg.Accaunt == user.ID : user.ID == db.AuthorID
                        from task in user.Events
                        where cfg.Types.Contains(task.Type)
                        where task.Closed == false
                        where task.Start != null
                        where (task.Notify ?? task.Start) >= start
                        where (task.Notify ?? task.Start) <= end
                        where
                        task.NotifyConsultant && user.Roles.Select(v => v.Key).Contains(dbroles.consultant)
                        ||
                        !task.NotifyConsultant

                        // иванкова 15.08.2017 14:46
                        // where task.Organization.CloseInfo == null

                        select new
                        {
                            Name = task.Organization.ShortName ?? task.Organization.FullName,
                            task.Organization.ID,
                            EventID = task.ID,
                            task.Type,
                            Date = task.Notify ?? task.Start,
                            Address = task.Organization.Address
                                          .Where(v => v.Type == AddressType.region)
                                          .Select(v => v.Name)
                                          .FirstOrDefault()
                                      ?? task.Organization.FormatAddress
                                      ?? task.Organization.RawAddress,
                            task.Description
                        })
                    .AsEnumerable()
                    .Select(v => new PlanOrgTableItem
                    {
                        Name = v.Name,
                        ID = v.ID,
                        EventID = v.EventID,
                        Type = v.Type,
                        Date = v.Date.ConvertTimeZone(tz)?.ToString("dd MMMM yyyy"),
                        Time = v.Date.ConvertTimeZone(tz)?.ToString("HH:mm"),
                        //DateTime = v.Date.ConvertTimeZone(tz)?.DateTime,
                        DateTimeFilter = v.Date.ConvertTimeZone(tz)?.DateTime.Date,
                        Address = v.Address,
                        Description = v.Description.HtmlDecode().FromHtml()
                    })
                    .OrderBy(v => v.DateTimeFilter)
                    .ThenBy(v => v.Time)
                    .ToDataSourceResult(request, item =>
                    {
                        item.TypeString = item.Type.AtValue();
                        return item;
                    });
            });

        /// <summary>
        /// Сохранение пользовательских настроек
        /// </summary>
        /// <param name="cfg">настройки</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> SaveConfig(
            OperatorPlanConfig cfg)
            => await this.act(gb =>
            {
                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    select new
                    {
                        user,
                        file = user.Files
                            .Where(file => file.Type == TypeFile.Config)
                            .FirstOrDefault(file => file.Name == Login.Configs.OperatorPlanConfig)
                    };

                var read = query.First();

                if (read.file == null)
                    read.user.Files.Add(new RawFile
                    {
                        Raw = cfg.AsBytes(),
                        Type = TypeFile.Config,
                        Name = Login.Configs.OperatorPlanConfig
                    });
                else
                    read.file.Raw = cfg.AsBytes();
                gb.Save();
            });
    }
}