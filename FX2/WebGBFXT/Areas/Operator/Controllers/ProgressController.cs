﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Models;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Operator.Models;

#endregion

namespace WebGBFXT.Areas.Operator.Controllers
{
    /// <summary>
    /// Организации - в обработке
    /// </summary>
    [NoCache]
    public class ProgressController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Чтение списка организаций
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [NoCache]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                var db = gb.db;

                var query = db.OrgsFiltered(db);

                var isconsultant = gb.Author.Verificate(dbroles.consultant);

                return (from org in query
                        orderby org.ShortName
                        select new PlanOrgTableItem
                        {
                            Inn = org.Inn,
                            Name = org.ShortName,
                            FullName = org.FullName,
                            OGRN = org.Ogrn,
                            Address = org.Address
                                          .Where(a => a.Type == AddressType.region)
                                          .Select(a => a.Name)
                                          .FirstOrDefault()
                                      ?? org.FormatAddress
                                      ?? org.RawAddress,
                            ClientCode = org.ClientCode,
                            Description = org.Description,
                            Accaunt = (
                                    from progr in org.Events
                                    where !progr.Closed
                                    where progr.Start < db.Now
                                    where progr.End == null || progr.End > db.Now
                                    where progr.Type == EventType.progress
                                    from acc in progr.Users
                                    select acc.Caption
                                )
                                .FirstOrDefault(),
                            ID = org.ID,
                            Status = org
                                .Labels
                                .Where(v => v.Type == TypeLabel.work_step)
                                .Select(v => v.Caption)
                                .FirstOrDefault()
                        })
                    .AsEnumerable()
                    .Select(v =>
                    {
                        v.SearchName = new[]
                            {
                                v.Name.retIfNullOrWhiteSpace(null),
                                v.FullName.retIfNullOrWhiteSpace(null)
                            }
                            .WhereNotNull()
                            .Join(" | ")
                            .HtmlEncode();

                        v.Name = v.Name ?? v.FullName;

                        v.Name = v.Name.retIfNullOrWhiteSpace("отсутствует");
                        v.Status = v.Status ?? "";

                        //v.LastDate = v.LastDateOffset.ToTZNullDateTime(gb.TimeZone);
                        //v.NextDate = v.NextDateOffset.ToTZNullDateTime(gb.TimeZone);

                        //v.LastType = v.LastSourceType?.AtValue();
                        //v.NextType = v.NextSourceType?.AtValue();

                        return v;
                    })
                    //.Distinct()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Удаление организации
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            PlanOrgTableItem item)
            => await this.json(gb =>
            {
                gb.db.OrgDelete(item.ID);

                return item
                    .InArray()
                    .ToTreeDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Удаление всех организаций
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> DestroyAll(Guid? id)
            => await this.func(gb =>
            {
                var db = gb.db;

                var query = db.OrgsMainFiltered(db);


                var orgs = query.Select(v => v.ID).ToArray();

                orgs.ForEach(organization => { gb.db.OrgDelete(organization); });


                return RedirectToAction("Index");
            });

        /// <summary>
        /// Сохранение пользовательских настроек
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> SaveConfig(
            string model)
            => await this.act(gb =>
            {
                var cfg = model.FromJson<OperatorProgressConfig>() ?? new OperatorProgressConfig();

                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    select new
                    {
                        user,
                        file = user.Files
                            .Where(file => file.Type == TypeFile.Config)
                            .FirstOrDefault(file => file.Name == Login.Configs.OperatorProgressFilter)
                    };

                var read = query.First();

                if (read.file == null)
                    read.user.Files.Add(new RawFile
                    {
                        Raw = cfg.AsBytes(),
                        Type = TypeFile.Config,
                        Name = Login.Configs.OperatorProgressFilter
                    });
                else
                    read.file.Raw = cfg.AsBytes();
                gb.Save();
            });

        /// <summary>
        /// Сохранение положения блока фильтры (открыт/закрыт)
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> SaveOpenConfig(
            bool flag)
            => await this.act(gb =>
            {
                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    select new
                    {
                        user,
                        file = user.Files
                            .Where(file => file.Type == TypeFile.Config)
                            .FirstOrDefault(file => file.Name == Login.Configs.OperatorProgressOpenFilter)
                    };

                var read = query.First();

                var value = new OperatorProgressOpenConfig
                {
                    Open = !flag
                }.AsBytes();

                if (read.file == null)
                    read.user.Files.Add(new RawFile
                    {
                        Raw = value,
                        Type = TypeFile.Config,
                        Name = Login.Configs.OperatorProgressOpenFilter
                    });
                else
                    read.file.Raw = value;
                gb.Save();
            });

        /// <summary>
        /// Форма передачи организаций
        /// </summary>
        /// <param name="guids"></param>
        /// <returns></returns>
        [HttpPost]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> TransferForm(IEnumerable<Guid> guids)
            => await this.func(gb =>
            {
                if (guids == null)
                    throw new Exception();
                gb.GuidValue = new MainValue {guids = guids};
                return View(gb);
            });

        /// <summary>
        /// Действие передачи организаций
        /// </summary>
        /// <param name="acc">кому</param>
        /// <param name="orgs">кого</param>
        /// <param name="call">в план обзвона</param>
        /// <param name="date">дата в плане обзвона</param>
        /// <param name="comment">комментарий</param>
        /// <returns></returns>
        [HttpPost]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> TransferAction(
            Guid? acc,
            Guid?[] orgs,
            bool? call,
            DateTime? date,
            string comment)
            => await this.act(gb =>
            {
                (
                        from e in gb.db.Events
                        where orgs.Contains(e.OrgID)
                        where e.Type == EventType.progress || e.Type == EventType.edit
                        where e.Start < gb.Now
                        where e.End > gb.Now
                        where !e.Closed
                        select e
                    )
                    .AsEnumerable()
                    .ForEach(e =>
                    {
                        e.Closed = true;
                        e.End = gb.Now;
                    });

                gb.db.Commit();

                var user = gb.db.Logins.Find(acc);

                orgs.Select(v => new Event
                    {
                        OrgID = v,
                        Users = user.InArray(),
                        Start = gb.Now,
                        End = DateTimeOffset.MaxValue,
                        Type = EventType.progress
                    })
                    .ForEach(v => gb.db.Events.Add(v));

                if (call ?? false)
                    orgs.Select(v => new Event
                        {
                            OrgID = v,
                            Users = user.InArray(),
                            Start = gb.Now,
                            Notify = date?.ToTZDateTimeOffset(user.TimeZone) ?? gb.Now,
                            Description = comment?.HtmlDecode() ?? string.Empty,
                            End = DateTimeOffset.MaxValue,
                            Type = EventType.callTransfer
                        })
                        .ForEach(v =>
                        {
                            var contact = (
                                    from org in gb.db.Organizations
                                    where org.ID == v.OrgID
                                    from rec in org.Contacts
                                    select rec
                                )
                                .FirstOrDefault();

                            if (contact == null)
                                v.Record = new ContactRecord
                                {
                                    OrganizationID = v.OrgID
                                };
                            else
                                v.Record = contact;

                            gb.db.Events.Add(v);
                        });

                gb.db.Commit();
            });

        /// <summary>
        /// Загрузка пользовательской конфигурации
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Roles(dbroles.admin, dbroles.@operator)]
        public Task<ActionResult> LoadTableConfig()
            => this.func(gb =>
            {
                var cfg = (
                        from user in gb.db.Logins
                        where user.ID == gb.AuthorGuid
                        from file in user.Files
                        where file.Type == TypeFile.Config
                        where file.Name == Login.Configs.OperatorProgressFilter
                        select file)
                    .FirstOrDefault();

                try
                {
                    cfg.TryIfNull();

                    var filter = cfg.Raw.AsObject<OperatorProgressConfig>();

                    filter.TryIfNull();
                    filter.PersistState.TryIfNull();

                    return filter.PersistState.FromJson<fxcDictSO>()
                        .Action(v => { v["AutoSave"] = filter?.AutoSave ?? false; })
                        .ToJsonResult();
                }
                catch
                {
                    return false.ToJsonResult();
                }
            });

        /// <summary>
        /// Экспорт в Excel
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="base64"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            if (!this.GetAuthor().IsGod)
                throw new Exception("error access");

            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        public async Task<ActionResult> PartialLinks(Guid? id) => await this.func(gb =>
        {
            gb.GuidValue = new MainValue
            {
                id = id.Value
            };
            return View(gb);
        });

        public async Task<ActionResult> PartialContacts(Guid? id) => await this.func(gb =>
        {
            gb.GuidValue = new MainValue
            {
                id = id.Value
            };
            return View(gb);
        });

        public async Task<ActionResult> PartialLastEvents(Guid? id) => await this.func(gb =>
        {
            gb.GuidValue = new MainValue
            {
                id = id.Value
            };
            return View(gb);
        });

        public async Task<ActionResult> PartialSearchPhones(Guid? id) => await this.func(gb =>
        {
            gb.GuidValue = new MainValue
            {
                id = id.Value
            };
            return View(gb);
        });
    }
}