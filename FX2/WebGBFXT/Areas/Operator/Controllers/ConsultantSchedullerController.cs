﻿#region

using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.Attributes;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Organization.Models;

#endregion

namespace WebGBFXT.Areas.Operator.Controllers
{
    /// <summary>
    /// Календарь консультантов
    /// </summary>
    [NoCache]
    [Roles]
    public class ConsultantSchedullerController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator, dbroles.consultant)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Чтение списка событий
        /// </summary>
        /// <param name="request"></param>
        /// <param name="depid"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator, dbroles.consultant)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            Guid? depid)
            => await this.json(gb =>
            {
                return (
                        from dep in gb.db.Departaments
                        where dep.ID == depid
                        from task in dep.Events
                        where task.Type == EventType.meeting
                              || task.Type == EventType.meetingConfirm
                              || task.Type == EventType.meetingClose
                        select new
                        {
                            task.Description,
                            task.ID,
                            task.Type,
                            task.Start,
                            task.End,
                            OrganizationID = task.OrgID,
                            DepartamentID = dep.ID,
                            OrganizationName = task.Organization.ShortName ?? task.Organization.FullName,
                            Notify = DbFunctions.TruncateTime(task.Notify) != DbFunctions.TruncateTime(task.Start),
                            NotifyTime = task.Notify ?? task.Start,
                            task.NotifyConsultant,
                            Address = from adr in task.Organization.Address
                            where adr.Type < AddressType.street
                            orderby adr.Type
                            select new
                            {
                                adr.Name,
                                adr.Suffix
                            },
                            Accaunts = (from e in task.Organization.Events
                                where e.Type == EventType.progress
                                where !e.Closed
                                where e.Start < gb.Now
                                where e.End > gb.Now
                                select e.Users)
                            .SelectMany(v => v)
                            .Distinct()
                            .Select(v => v.Caption)
                        }
                    )
                    .AsEnumerable()
                    .Select(item => new SchedullerIndexModel
                    {
                        Description = item.Description.HtmlDecode().FromHtml(),
                        ID = item.ID,
                        Type = item.Type,
                        Start = item.Start.ToTZDateTime(gb.TimeZone),
                        End = item.End.ToTZDateTime(gb.TimeZone),
                        OrganizationID = item.OrganizationID,
                        DepartamentID = item.DepartamentID,
                        OrganizationName = item.OrganizationName,
                        Notify = item.Notify,
                        NotifyTime = item.NotifyTime.ToTZDateTime(gb.TimeZone),
                        NotifyConsultant = item.NotifyConsultant,
                        Title = item.Type.AtValue(),
                        Region = item.Address.Select(v => $"{v.Name} {v.Suffix}").Join(", "),
                        Accaunts = item.Accaunts.Join(", ")
                    })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Чтение списка подразделений
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator, dbroles.consultant)]
        public async Task<ActionResult> ReadDepartaments()
            => await this.json(gb =>
            {
                return (
                    from root in gb.db.Departaments
                    where root.Key == Departament.ConsultantDepartament
                    from dep in root.Childs
                    orderby dep.Caption
                    select new GuidValue
                    {
                        Guid = dep.ID,
                        Value = dep.Caption
                    })
                    .ToArray();
            });
    }
}