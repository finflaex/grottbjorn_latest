﻿#region

using System.Web.Mvc;

#endregion

namespace WebGBFXT.Areas.Intersect
{
    public class IntersectAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Intersect"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Intersect_default",
                "Intersect/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}