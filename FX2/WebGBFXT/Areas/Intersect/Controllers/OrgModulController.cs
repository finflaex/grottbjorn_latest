﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Intersect.Controllers
{
    /// <summary>
    /// Модуль пересечений
    /// </summary>
    [NoCache]
    [Roles]
    public class OrgModulController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Index(Guid id)
        {
            return View(new ActionData(this)
            {
                GuidValue = new MainValue
                {
                    id = id
                }
            });
        }

        /// <summary>
        /// Объединение дубликатов
        /// </summary>
        /// <param name="id"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public async Task<ActionResult> FromOperatorToConsultant(Guid? id, Guid? target)
            => await this.func(gb =>
            {
                var query =
                    from old in gb.db.Organizations
                    where old.ID == id
                    from org in gb.db.Organizations
                    where org.ID == target
                    select new
                    {
                        old,
                        org,
                        old_acc = (from task in old.Events
                            where !task.Closed
                            where task.Start < gb.Now
                            where task.End > gb.Now
                            where task.Type == EventType.progress
                            from user in task.Users
                            select user)
                        .FirstOrDefault(),
                        author = (from user in gb.db.Logins
                            where user.ID == gb.AuthorGuid
                            select user)
                        .FirstOrDefault(),
                        new_acc = (from task in org.Events
                            where !task.Closed
                            where task.Start < gb.Now
                            where task.End > gb.Now
                            where task.Type == EventType.progress
                            from user in task.Users
                            select user)
                        .FirstOrDefault(),
                        old_records = from rec in old.Contacts
                        select rec,
                        old_events = from task in old.Events
                        select task
                    };

                var read = query.FirstOrDefault();

                if (read == null)
                    return null;

                read.old_records.ToArray().ForEach(rec => { rec.OrganizationID = read.org.ID; });

                read.old_events.ToArray()
                    .ForEach(task =>
                    {
                        switch (task.Type)
                        {
                            case EventType.callUp:
                            case EventType.unknown:
                                task.Closed = true;
                                task.Description += "<br>Стало неактивным после объединения.";
                                task.OrgID = read.org.ID;
                                task.Users.Clear();
                                task.Users.Add(read.new_acc);
                                break;
                            case EventType.edit:
                            case EventType.progress:
                                gb.db.Delete(task);
                                break;
                            //case EventType.skype:
                            case EventType.rateConsultant:
                            case EventType.@event:
                            case EventType.eventItem:
                            case EventType.seminarPromised:
                            case EventType.eventSeminar:
                            case EventType.meetingEvent:
                            case EventType.sendMail:
                            case EventType.meeting:
                            case EventType.call:
                            case EventType.meetingConfirm:
                            case EventType.meetingCancel:
                            case EventType.meetingClose:
                            case EventType.meetingRate:
                            case EventType.comment:
                            case EventType.seminarVisited:
                            case EventType.seminarCanceled:
                            case EventType.seminarSkiped:
                                task.OrgID = read.org.ID;
                                task.Users.Clear();
                                task.Users.Add(read.new_acc);
                                break;
                        }
                    });

                read.org.Description += "<br> Объединение дубликатов";
                read.org.Description += $"<br> {gb.Now:yyyy-MM-dd}";
                read.org.Description += "<br> Инициатор: " + read.author?.Caption;
                read.org.Description += "<br> Владелец: " + read.old_acc?.Caption;
                read.org.Description += "<br>" + read.old.Description;

                gb.Save();

                gb.db.OrgDelete(read.old.ID);

                return RedirectToAction("Index", "Progress", new
                {
                    area = "Operator"
                });
            });

        /// <summary>
        /// Запрос дубликата у операторов
        /// </summary>
        /// <param name="id"></param>
        /// <param name="target"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> ClaimOperator(
            Guid? id, Guid? target, string comment)
            => await this.act(gb =>
            {
                id.ThrowIfNull();
                target.ThrowIfNull();
                comment.ThrowIfNull();

                var query =
                    from current in gb.db.Organizations
                    where current.ID == id
                    select new
                    {
                        INN = current.Inn,
                        Name = current.ShortName ?? current.FullName,
                        Accaunt = (
                            from login in gb.db.Logins
                            where login.ID == gb.AuthorGuid
                            select login
                        )
                        .FirstOrDefault(),

                        opermanager = (
                            from dep in gb.db.Departaments
                            where dep.Key == Departament.OperatorDepartament
                            from user in dep.Users
                            where user.Roles.Any(v => v.Key == dbroles.manager)
                            select user
                        )
                        .Concat(
                            from login in gb.db.Logins
                            where login.Roles.Any(v => v.Key == dbroles.developer)
                            select login
                        )
                        .Distinct(),

                        accaunts = (
                            from e in current.Events
                            where !e.Closed
                            where e.Start < gb.Now
                            where e.End > gb.Now
                            where e.Type == EventType.progress
                            select e
                        )
                        .SelectMany(e => e.Users)
                        .Distinct(),
                        target = (
                            from org in gb.db.Organizations
                            where org.ID == target
                            select new
                            {
                                org,
                                haslabel = org.Labels.Any(v => v.Description == "claim"),
                                label = gb.db.Labels.FirstOrDefault(v => v.Description == "claim"),
                                org.ID,
                                Name = org.ShortName ?? org.FullName,
                                Accaunts = (
                                    from e in org.Events
                                    where !e.Closed
                                    where e.Start < gb.Now
                                    where e.End > gb.Now
                                    where e.Type == EventType.progress
                                    select e
                                )
                                .SelectMany(e => e.Users)
                                .Distinct()
                            }
                        )
                        .FirstOrDefault()
                    };

                var read = query.FirstOrDefault();

                read.ThrowIfNull();
                read.Accaunt.ThrowIfNull();
                read.target.ThrowIfNull();
                read.target.label.ThrowIfNull();

                if (!read.target.haslabel)
                {
                    read.target.org.Labels.Add(read.target.label);
                    gb.Save();
                }

                read.opermanager.ForEach(manager =>
                {
                    gdb.send_mail(read.Accaunt, manager, "Требование дубликата", $@"
Добрый день, {manager.Caption}!<br>
<br>
Сообщаю, что за мной числится следующая организация:<br>
<br>
ИНН: {read.INN}<br>
Наименование: {read.Name}<br>
Ответственные: {read.accaunts.Select(v => v.Caption).Join(", ")}<br>
<br>
В пересечениях у операторов находится следующая организация c таким же ИНН:<br>
<br>
Наименование: {read.target.Name}<br>
Ответственные: {read.target.Accaunts.Select(v => v.Caption).Join(", ")}<br>
<a href='http://mars:2017/Organization/Card?id={read.target.ID:N}'>ссылка</a>
<br>
<br>
Содержание требования:<br>
{comment.ToHtml()}
");
                });
            });
    }
}