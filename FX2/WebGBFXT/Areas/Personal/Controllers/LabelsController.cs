﻿#region

using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Administration.Models;

#endregion

namespace WebGBFXT.Areas.Personal.Controllers
{
    /// <summary>
    /// Персональные метки
    /// </summary>
    [NoCache]
    [Roles]
    public class LabelsController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
            => await this.func(View);

        /// <summary>
        /// Создание метки
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [Roles]
        public async Task<ActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            TableLabelItem item)
            => await this.json(gb =>
            {
                var record = gb.db.Add(new Label
                {
                    Caption = item.Caption,
                    Description = item.Description,
                    Order = 0,
                    Type = TypeLabel.user,
                    UserID = gb.AuthorGuid
                });

                gb.db.Commit();

                item.ID = record.ID;

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Чтение списка меток
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Roles]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                var db = gb.db;

                return (
                        from user in db.Logins
                        where user.ID == gb.AuthorGuid
                        from label in user.Labels
                        where label.Type == TypeLabel.user
                        orderby label.Caption
                        select new TableLabelItem
                        {
                            ID = label.ID,
                            Caption = label.Caption,
                            Description = label.Description
                        })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Обновление метки
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [Roles]
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            TableLabelItem item)
            => await this.json(gb =>
            {
                var record = gb.db.Labels.Find(item.ID);

                if (record != null)
                {
                    record.Caption = item.Caption;
                    record.Description = item.Description;
                    gb.db.Commit();
                }

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });

        /// <summary>
        /// Удаление метки
        /// </summary>
        /// <param name="request"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [Roles]
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            TableLabelItem item)
            => await this.json(gb =>
            {
                gb.db.Labels.Find(item.ID).ActionIfNotNull(record =>
                {
                    gb.db.Delete(record);
                    gb.db.Commit();
                });

                return item
                    .InArray()
                    .ToDataSourceResult(request, ModelState);
            });
    }
}