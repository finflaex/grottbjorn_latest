﻿#region

using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Personal.Controllers
{
    /// <summary>
    /// Сообщения
    /// </summary>
    [NoCache]
    [Roles]
    public class MessageController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
            => await this.func(gb =>
            {
                var model = gb.db
                    .User
                    .RecivedMessages
                    .OrderByDescending(v => v.DateCreate)
                    .AsEnumerable()
                    .Select(v => new MessageItem
                    {
                        date = v.DateCreate.ToString("yyyy.MM.dd HH:mm"),
                        author = v.AuthorService ?? v.Author?.Caption ?? "неизвестно",
                        id = v.ID.ToString("N"),
                        caption = v.Caption,
                        content = v.Description,
                        subcaption = ""
                    })
                    .ToArray();

                model.FirstOrDefault().ActionIfNotNull(read => { read.active = true; });

                return View(model);
            });
    }

    public class MessageItem
    {
        public bool active;
        public string date { get; set; }
        public string author { get; set; }
        public string id { get; set; }
        public string caption { get; set; }
        public string content { get; set; }
        public string subcaption { get; set; }
    }
}