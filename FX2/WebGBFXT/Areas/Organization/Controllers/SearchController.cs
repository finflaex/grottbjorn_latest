﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.ApiOnline.BasicData;
using Finflaex.Support.ApiOnline.OrgInfo;
using Finflaex.Support.ApiOnline.OrgInfo.Models;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Person = BaseDBFX.Table.Person;
using Post = BaseDBFX.Table.Post;

#endregion

namespace WebGBFXT.Areas.Organization.Controllers
{
    /// <summary>
    /// Форма поиска в интернете
    /// </summary>
    [NoCache]
    [Roles]
    public class SearchController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Index(Guid? id)
            => await this.func(gb =>
            {
                if (id.IsNullOrEmpty())
                    return "Ошибка".ToActionResult();

                var model = gb.db
                    .Organizations
                    .Where(v => v.ID == id)
                    .Select(v => new IndexModel
                    {
                        OrgID = v.ID,
                        Name = v.ShortName ?? v.FullName,
                        Inn = v.Inn,
                        AddressRegion = v.Address
                            .Where(a => a.Type == AddressType.region)
                            .Select(a => a.ID)
                            .FirstOrDefault()
                    })
                    .First();

                model.Name = model
                                 .Name?
                                 .Replace("'", string.Empty)
                                 .Replace("\"", string.Empty)
                                 .Replace("  ", " ")
                                 .Replace("ООО", string.Empty)
                                 .Replace("`", string.Empty)
                                 .Replace("ЗАО", string.Empty)
                                 .Replace("ОАО", string.Empty)
                                 .Replace("АО ", string.Empty)
                                 .Trim()
                             ?? string.Empty
                    ;

                model.Inn = model.Inn?.Trim();

                return View(model);
            });

        /// <summary>
        /// Поиск в ЕГРЮЛ
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Search(IndexModel model)
            => await this.func(gb =>
            {
                var read = fxsOrgInfo
                    .SearchOrg(model.Inn, null, model.Name)
                    .Select(fxsOrgInfo.LoadOrg)
                    .Where(v
                        => model.AddressRegion.IsEmpty()
                           || v.address?.region?.guid.ToGuid() == model.AddressRegion)
                    .Select(v => new OrgModel
                    {
                        Address = v.address?.fullHouseAddress,
                        Name = v.name,
                        Close = v.closeInfo != null,
                        INN = v.inn,
                        WorkID = v.id,
                        ID = model.OrgID,
                        Activities = v.mainOkved2?.name
                    })
                    .ToArray();

                if (read.Any())
                    return View(read);

                return "В интернете пусто или набрано неверно".ToActionResult();
            });

        /// <summary>
        /// Загрузка списка областей
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> LoadRegion()
            => await this.json(gb =>
            {
                return fxsFIAS
                    .GetADDROBJ(null)
                    .Select(v => new GuidValue
                    {
                        Guid = v.aoguid.ToGuid(),
                        Value = v.formalname
                    })
                    .FuncSelect(arr => new List<GuidValue>()
                        .AddRet(new GuidValue
                        {
                            Guid = Guid.Empty,
                            Value = "По всей России..."
                        })
                        .AddRangeRet(arr));
            });

        /// <summary>
        /// Поиск дубликатов
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Concurency(OrgModel model)
            => await this.func(gb =>
            {
                var list = (
                        from org in gb.db.Organizations
                        where org.ID != model.ID
                        where org.WorkID == model.WorkID
                              || model.INN != null && org.Inn == model.INN
                        select new OrgModel
                        {
                            ID = model.ID,
                            Address = org.RawAddress,
                            SearchID = org.ID,
                            Name = org.FullName ?? org.ShortName,
                            INN = org.Inn,
                            WorkID = org.WorkID,
                            AccID = (
                                    from task in org.Events
                                    where task.Type == EventType.progress
                                    from user in task.Users
                                    select user.ID)
                                .FirstOrDefault(),
                            AccName = (
                                    from task in org.Events
                                    where task.Type == EventType.progress
                                    from user in task.Users
                                    select user.Caption)
                                .FirstOrDefault()
                        })
                    .ToArray();

                ViewData["org"] = model;
                return View(list);
            });

        /// <summary>
        /// Загрузка данных организации
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> LoadOrg(OrgModel model)
            => await this.func(gb =>
            {
                var loadOrg = fxsOrgInfo.LoadOrg(model.WorkID);

                if (loadOrg == null)
                    return "Организация не найдена".ToActionResult();

                var org = gb.db.Organizations.Find(model.ID);

                try
                {
                    LoadOrgHelp(model.WorkID, gb.db, loadOrg, org);
                }
                catch (Exception err)
                {
                    var admin = 1.ToGuid();
                    var read = gb.db.Logins.Find(admin);
                    gdb.send_mail(read, read, "Error Search Loading", err.ToFinflaexString(), true);
                }

                return View(model);
            });

        private static async void LoadOrgHelp(
            string wid,
            DBFX db,
            OrgData loadOrg,
            BaseDBFX.Table.Organization org
        )
        {
            //var taskFounders = fxsOrgInfo.LoadOrgFoundersAsync(loadOrg);
            var taskOrgEmployes = fxsOrgInfo.LoadOrgEmployeesAsync(loadOrg);
            //var taskEstablisheds = fxsOrgInfo.LoadOrgChildsAsync(loadOrg);

            //Task.WaitAll(
            //    //taskFounders,
            //    taskOrgEmployes,
            //    //taskEstablisheds
            //);

            db.TryCommit($"{MethodBase.GetCurrentMethod().Name} - save contact pipls");

            org.Work = true;
            org.WorkID = wid;
            org.ParseAddress(db, loadOrg.address?.fullHouseAddress);

            org.HouseNumber = loadOrg.address?.house;
            org.ZipCode = loadOrg.address?.postalIndex;
            org.ShortName = loadOrg.shortName;
            org.FullName = loadOrg.name;
            org.Inn = loadOrg.inn;
            org.Ogrn = loadOrg.ogrn;
            org.OgrnDate = loadOrg.ogrnDate;
            org.Kpp = loadOrg.kpp;

            LoadCloseInfo(db, loadOrg, org);
            db.TryCommit($"{MethodBase.GetCurrentMethod().Name} - save close info");

            LoadOkopf(db, loadOrg, org);
            db.TryCommit($"{MethodBase.GetCurrentMethod().Name} - save okopf");

            org.WorkDate = loadOrg.lastUpdateDate;
            LoadFss(db, loadOrg, org);
            db.TryCommit($"{MethodBase.GetCurrentMethod().Name} - save fss");

            //LoadForeignRegistration(db, loadOrg, org);

            LoadMainOkved2(db, loadOrg, org);
            db.TryCommit($"{MethodBase.GetCurrentMethod().Name} - save mainokved2");

            LoadListOkved2(db, loadOrg, org);
            db.TryCommit($"{MethodBase.GetCurrentMethod().Name} - save listokved2");

            //var loadFounders = await taskFounders;
            //org.Founders.ToList().ForEach(item => db.Delete(item));
            //LoadFounders(db, org, loadFounders);
            //db.TryCommit($"{MethodBase.GetCurrentMethod().Name} - save founders");

            var loadPersons = await taskOrgEmployes;
            LoadContacts(db, org, loadPersons);
            db.TryCommit($"{MethodBase.GetCurrentMethod().Name} - save contacts");

            //var loadEstablisheds = await taskEstablisheds;
            //LoadEstablisheds(db, loadEstablisheds, org);
            //db.TryCommit($"{MethodBase.GetCurrentMethod().Name} - save establisheds");
        }

        /// <summary>
        /// Загрузка контактных лиц
        /// </summary>
        /// <param name="db"></param>
        /// <param name="org"></param>
        /// <param name="loadPersons"></param>
        private static void LoadContacts(DBFX db, BaseDBFX.Table.Organization org, OrgEmployee[] loadPersons)
        {
            if (loadPersons == null) return;
            foreach (var person in loadPersons)
            {
                var rec_person = db
                    .Persons
                    .FirstOrDefault(v
                        => v.WorkID == person.person.id
                           || v.Inn != null && v.Inn == person.person.inn);

                if (rec_person == null)
                    rec_person = new Person
                    {
                        FirstName = person.person.firstName,
                        FullName = person.person.fullName,
                        Inn = person.person.inn,
                        MiddleName = person.person.middleName,
                        SurName = person.person.surName,
                        WorkID = person.person.id
                    };

                var post_person = db
                    .Posts
                    .FirstOrDefault(v
                        => v.Caption == person.postName);

                if (post_person == null)
                    post_person = new Post();

                post_person.Caption = person.postName.ToLower();
                post_person.WorkID = person.post.id;

                var record_founder = (
                        from read_pers in db.Persons
                        where read_pers.ID == rec_person.ID
                        from read_record in db.ContactRecords
                        where read_record.OrganizationID == org.ID
                        where read_record.Persons.Contains(read_pers)
                        select read_record)
                    .FirstOrDefault();

                if (record_founder == null)
                    record_founder = new ContactRecord();
                if (!record_founder.Posts.Contains(post_person))
                    record_founder.Posts.Add(post_person);
                if (!record_founder.Persons.Contains(rec_person))
                    record_founder.Persons.Add(rec_person);

                if (!org.Contacts.Contains(record_founder))
                    org.Contacts.Add(record_founder);
            }
        }

        /// <summary>
        /// Загрузка ОКВЭД2
        /// </summary>
        /// <param name="db"></param>
        /// <param name="load"></param>
        /// <param name="org"></param>
        private static void LoadListOkved2(DBFX db, OrgData load, BaseDBFX.Table.Organization org)
        {
            org.ListOkved2.Clear();

            if (load.okved2 != null && load.okved2.Any())
            {
                var ids = load.okved2.Select(v => v.code).ToArray();

                db.Okved2s.Where(v => ids.Contains(v.Code)).ToArray().ForEach(read => { org.ListOkved2.Add(read); });
            }
            else
            {
                org.ListOkved2.Clear();
            }
        }

        /// <summary>
        /// Загрузка основного ОКВЭД2
        /// </summary>
        /// <param name="db"></param>
        /// <param name="load"></param>
        /// <param name="org"></param>
        private static void LoadMainOkved2(DBFX db, OrgData load, BaseDBFX.Table.Organization org)
        {
            if (load.mainOkved2 != null)
                org.MainOkved2 = db.Okved2s.FirstOrDefault(v => v.Code == load.mainOkved2.code);
        }

        /// <summary>
        /// Загрузка информации о ликвидации
        /// </summary>
        /// <param name="db"></param>
        /// <param name="load"></param>
        /// <param name="org"></param>
        private static void LoadCloseInfo(DBFX db, OrgData load, BaseDBFX.Table.Organization org)
        {
            if (load.closeInfo != null)
            {
                var type = db.OrgCloseTypes.Find(load.closeInfo.closeReason.id);
                if (type == null)
                    db.OrgCloseTypes.Add(new OrgCloseType
                    {
                        ID = load.closeInfo.closeReason.id,
                        FullName = load.closeInfo.closeReason.name,
                        ShortName = load.closeInfo.closeReason.shortName,
                        Code = load.closeInfo.closeReason.code
                    });

                org.CloseInfo = new OrgCloseInfo
                {
                    Date = load.closeInfo.date,
                    ReasonID = load.closeInfo.closeReason.id
                };
            }
        }

        /// <summary>
        /// Загрузка ОКОПФ
        /// </summary>
        /// <param name="db"></param>
        /// <param name="load"></param>
        /// <param name="org"></param>
        private static void LoadOkopf(DBFX db, OrgData load, BaseDBFX.Table.Organization org)
        {
            if (load.okopf != null)
            {
                var id = load.okopf.code.FuncWhere(v => v.Length < 5,
                    v => { return v.Append("00000").Take(5).ToArray().FuncSelect(arr => new string(arr)); });
                org.Okopf = db.Okopfs.FirstOrDefault(v => v.Code == id);
            }
        }

        /// <summary>
        /// Загрузка данных ФСС
        /// </summary>
        /// <param name="db"></param>
        /// <param name="load"></param>
        /// <param name="org"></param>
        private static void LoadFss(DBFX db, OrgData load, BaseDBFX.Table.Organization org)
        {
            if (load.fssRegistration != null)
            {
                var id = load.fssRegistration.fss.id.ToInt();
                var fss = db.Fsses.Find(id);
                if (fss == null)
                    fss = new Fss
                    {
                        ID = id,
                        Caption = load.fssRegistration.fss.name,
                        Code = load.fssRegistration.fss.code
                    };
                var reg = org.FssRegistration;
                if (reg == null)
                    reg = org.FssRegistration = new FssRegistration();
                org.FssRegistration.Fss = fss;
                org.FssRegistration.Date = load
                    .fssRegistration.registrationDate.ToDateTime("yyyy-MM-ddTHH:mm:ss.fff");
                org.FssRegistration.Number = load.fssRegistration.number;
            }
        }

        public class IndexModel
        {
            public Guid OrgID { get; set; }
            public string Name { get; set; }
            public string Inn { get; set; }
            public Guid AddressRegion { get; set; }
        }

        public class OrgModel
        {
            public string Name { get; set; }
            public string INN { get; set; }
            public string Address { get; set; }
            public bool Close { get; set; }
            public Guid ID { get; set; }
            public string WorkID { get; set; }
            public Guid AccID { get; set; }
            public string AccName { get; set; }
            public Guid SearchID { get; set; }
            public string Activities { get; set; }
        }
    }
}