﻿#region

using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.ApiOnline.BasicData;
using Finflaex.Support.Attributes;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Organization.Models;

#endregion

namespace WebGBFXT.Areas.Organization.Controllers
{
    /// <summary>
    /// Форма назначения встречи
    /// </summary>
    [NoCache]
    [Roles]
    public class SchedullerController : Controller
    {
        /// <summary>
        /// Получить представление
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Index(Guid? id)
            => await this.func(gb =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

                if (id.IsNullOrEmpty())
                    return Content("ошибка").AsType<ViewResult>();

                var db = gb.db;

                var query = (
                    from record in db.ContactRecords
                    where record.ID == id
                    from ints in record.Organization.Interests
                    where ints.Type != InterestType.notinterested
                    select new SchedullerQuestModel
                    {
                        RecordID = record.ID,
                        InterestID = ints.ID,
                        OrganizationID = record.OrganizationID,
                        DepartamentID = ints.Activity.DepartamentID,
                        Key = ints.Activity.ActivityValue.Key,
                        Caption = ints.Activity.ActivityValue.Caption,
                        OrgName = record.Organization.ShortName ?? record.Organization.FullName,
                        Comment = ints.Activity.Departament.Caption,
                        Type = 1
                    })
                    .ToArray();

                var query2 = (
                        from record in db.ContactRecords
                        where record.ID == id
                        from adr in record.Organization.Address
                        from task in adr.Events
                        where task.Type == EventType.eventItem
                        where task.End > db.Now
                        select new SchedullerQuestModel
                        {
                            RecordID = record.ID,
                            OrganizationID = record.OrganizationID,
                            EventID = task.ID,
                            Caption = task.Prev.Caption,
                            OrgName = record.Organization.ShortName ?? record.Organization.FullName,
                            Comment = task.Prev.Description,
                            Type = 2,
                            _Start = task.Start,
                            _End = task.End
                        })
                    .Distinct()
                    .AsEnumerable()
                    .Select(v =>
                    {
                        v.Start = v._Start.ToTZDateTime(gb.TimeZone);
                        v.End = v._End.ToTZDateTime(gb.TimeZone);
                        return v;
                    })
                    .ToArray();

                var read = query.Concat(query2)
                    .SelectMany(v =>
                    {
                        if (v.Type == 1)
                            return v.InArray();
                        return new[]
                        {
                            v,
                            new SchedullerQuestModel
                            {
                                RecordID = v.RecordID,
                                OrganizationID = v.OrganizationID,
                                EventID = v.EventID,
                                Caption = v.Caption,
                                OrgName = v.OrgName,
                                Comment = v.Comment,
                                Start = v.Start,
                                End = v.End,
                                Type = 3
                            }
                        };
                    })
                    .ToArray();

                if (read.Any())
                    return View("Quest", read);
                return View("Error");
            });

        /// <summary>
        /// Получить представление календаря
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        [ValidateInput(false)]
        public async Task<ActionResult> ViewScheduller(string model)
            => await this.func(gb =>
            {
                var read = model.FromJson<SchedullerQuestModel>();

                switch (read.Type)
                {
                    case 1:
                        return View(read);
                    case 2:
                        return View("EventConfirm", read);
                    case 3:
                        return View("BusinessTrip", read);
                    default:
                        return "error".ToActionResult();
                }
            });

        /// <summary>
        /// Получить списко событий
        /// </summary>
        /// <param name="request"></param>
        /// <param name="recordid"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            Guid? recordid,
            string key)
            => await this.json(gb =>
            {
                var types = new[]
                {
                    EventType.meeting,
                    EventType.meetingClose,
                    EventType.meetingConfirm
                };

                return (
                    from record in gb.db.ContactRecords
                    where record.ID == recordid
                    from interest in record.Organization.Interests
                    where interest.Activity.ActivityValue.Key == key
                    from task in interest.Activity.Departament.Events
                    where types.Contains(task.Type)
                    select new
                    {
                        task.Description,
                        task.ID,
                        task.Type,
                        task.Start,
                        task.End,
                        OrganizationID = task.OrgID,
                        RecordID = record.ID,
                        interest.Activity.DepartamentID,
                        NotEdit = record.Organization != task.Organization,
                        OrganizationName = task.Organization.ShortName ?? task.Organization.FullName,
                        Notify = DbFunctions.TruncateTime(task.Notify) != DbFunctions.TruncateTime(task.Start),
                        NotifyTime = task.Notify ?? task.Start,
                        task.NotifyConsultant,
                        Address = from adr in task.Organization.Address
                        where adr.Type < AddressType.street
                        orderby adr.Type
                        select new
                        {
                            adr.Name,
                            adr.Suffix
                        },
                        Accaunts = (from e in task.Organization.Events
                            where e.Type == EventType.progress
                            where !e.Closed
                            where e.Start < gb.Now
                            where e.End > gb.Now
                            select e.Users)
                        .SelectMany(v => v)
                        .Distinct()
                        .Select(v => v.Caption),
                        TimeZone = record.Organization
                            .Address
                            .OrderByDescending(v => v.Type)
                            .Select(v => v.TimeZone)
                            .FirstOrDefault(v => v != 0)
                    })
                    .AsEnumerable()
                    .Select(item => new SchedullerIndexModel
                    {
                        Description = item.Description.HtmlDecode().FromHtml(),
                        ID = item.ID,
                        Type = item.Type,
                        Start = item.Start.ToTZDateTime(item.TimeZone),
                        End = item.End.ToTZDateTime(item.TimeZone),
                        OrganizationID = item.OrganizationID,
                        RecordID = item.RecordID,
                        DepartamentID = item.DepartamentID,
                        NotEdit = item.NotEdit,
                        OrganizationName = item.OrganizationName,
                        Notify = item.Notify,
                        NotifyTime = item.NotifyTime.ToTZDateTime(gb.TimeZone),
                        NotifyConsultant = item.NotifyConsultant,
                        Title = item.Type.AtValue(),
                        Region = item.Address.Select(v => $"{v.Name} {v.Suffix}").Join(", "),
                        Accaunts = item.Accaunts.Join(", ")
                    })
                    .ToDataSourceResult(request);
            });

        //[AcceptVerbs(HttpVerbs.Post)]
        //[Roles(dbroles.admin, dbroles.@operator)]
        //public async Task<ActionResult> Read(
        //    [DataSourceRequest] DataSourceRequest request,
        //    Guid? recordid,
        //    string key)
        //    => await this.async(lazy =>
        //    {
        //        var db = lazy.Value;


        //        var read = db
        //            .ContactRecords
        //            .Where(v => v.ID == recordid)
        //            .SelectMany(v => v.Organization.Interests.Select(intr => new
        //            {
        //                v.Organization,
        //                intr.Activity
        //            }))
        //            .Where(v => v.Activity.ActivityValue.Key == key)
        //            .Select(v => new
        //            {
        //                orgid = v.Organization.ID,
        //                depid = v.Activity.DepartamentID,
        //                list = v.Activity.Departament.Events
        //                    .Where(e
        //                        => e.Type == EventType.meeting
        //                           || e.Type == EventType.meetingClose
        //                           || e.Type == EventType.meetingConfirm)
        //            })
        //            .FirstOrDefault();

        //        return read.list
        //            .Select(v => new SchedullerModel
        //            {
        //                Description = v.Description ?? string.Empty,
        //                ID = v.ID.ToString(),
        //                Title = v.Type.AtValue(),
        //                Start = v.Start.Value,
        //                End = v.End.Value,
        //                OrganizationID = read.orgid.ToString(),
        //                IsAllDay = false,
        //                RecordID = recordid.Value.ToString(),
        //                DepartamentID = read.depid.ToString(),
        //                NotEdit = read.orgid != v.OrgID,
        //                OrgName = v.Organization.ShortName
        //                    .retIfNullOrWhiteSpace(v.Organization.FullName)
        //                    .retIfNullOrWhiteSpace("не указано"),
        //                Notify = v.Notify?.Date != v.Start?.Date,
        //                NotifyTime = v.Notify ?? v.Start,
        //                Consultant = v.NotifyConsultant
        //            })
        //            .ToArray()
        //            .ToDataSourceResult(request)
        //            .ToJsonResult();
        //    });

            /// <summary>
            /// Создать событие
            /// </summary>
            /// <param name="request"></param>
            /// <param name="model"></param>
            /// <returns></returns>
        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            SchedullerIndexModel model)
            => await this.func(gb =>
            {
                model.ID = gdb.add_meeting(
                    gb.db,
                    model.OrganizationID.Value,
                    model.RecordID.Value,
                    model.DepartamentID.Value,
                    model.InterestID.Value,
                    model.Start,
                    model.NotifyTime,
                    model.Description,
                    model.NotifyConsultant,
                    model.Notify);

                return model
                    .InArray()
                    .ToDataSourceResult(request)
                    .ToJsonResult();
            });

        //[ValidateInput(false)]
        //[AcceptVerbs(HttpVerbs.Post)]
        //[Roles(dbroles.admin, dbroles.@operator)]
        //public async Task<ActionResult> Create(
        //    [DataSourceRequest] DataSourceRequest request,
        //    SchedullerModel model)
        //    => await this.func(gb =>
        //    {
        //        model.ID = gdb.add_meeting(
        //                gb.db,
        //                model.OrganizationID.ToGuid(),
        //                model.RecordID.ToGuid(),
        //                model.DepartamentID.ToGuid(),
        //                model.InterestID.ToGuid(),
        //                model.Start,
        //                model.NotifyTime,
        //                model.Description,
        //                model.Consultant,
        //                model.Notify)
        //            .ToString();

        //        return model
        //            .InArray()
        //            .ToDataSourceResult(request)
        //            .ToJsonResult();
        //    });

            /// <summary>
            /// Установить время напоминания
            /// </summary>
            /// <param name="model"></param>
            /// <param name="task"></param>
        private static void SetNotify(SchedullerModel model, Event task)
        {
            var date = model.Start;
            if (model.Notify)
            {
                fxsProizvodstvCallendar.TypeDay status;
                do
                {
                    date = date.AddDays(-1);
                    status = fxsProizvodstvCallendar.GetStatusDay(date);
                } while (status == fxsProizvodstvCallendar.TypeDay.notWork);
            }
            else
            {
                date = model.Start.Date.AddHours(8);
            }
            task.Notify = date.Date
                .AddHours(model.NotifyTime?.Hour ?? 8)
                .AddMinutes(model.NotifyTime?.Minute ?? 0);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //[Roles(dbroles.admin, dbroles.@operator)]
        //[ValidateInput(false)]
        //public async Task<ActionResult> Update(
        //    [DataSourceRequest] DataSourceRequest request,
        //    SchedullerModel model)
        //    => await this.func(gb =>
        //    {
        //        if (model.End < model.Start)
        //        {
        //            model.End = model.Start.AddHours(3);
        //        }

        //        gdb.set_meeting(
        //            gb.db,
        //            model.ID.ToGuid(),
        //            model.Start,
        //            model.End,
        //            model.NotifyTime,
        //            model.Description,
        //            model.Consultant,
        //            model.Notify);

        //        return model
        //            .InArray()
        //            .ToDataSourceResult(request)
        //            .ToJsonResult();
        //    });

            /// <summary>
            /// Обновить событие
            /// </summary>
            /// <param name="request"></param>
            /// <param name="model"></param>
            /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.@operator)]
        [ValidateInput(false)]
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            SchedullerIndexModel model)
            => await this.json(gb =>
            {
                if (model.End < model.Start)
                    model.End = model.Start.AddHours(3);

                gdb.set_meeting(
                    gb.db,
                    model.ID,
                    model.Start,
                    model.End,
                    model.NotifyTime,
                    model.Description,
                    model.NotifyConsultant,
                    model.Notify);

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });

        //[AcceptVerbs(HttpVerbs.Post)]
        //[Roles(dbroles.admin, dbroles.@operator)]
        //public async Task<ActionResult> Destroy(
        //    [DataSourceRequest] DataSourceRequest request,
        //    SchedullerModel model)
        //    => await this.async(lazy =>
        //    {
        //        var db = lazy.Value;

        //        var task = db.Events.Find(model.ID.ToGuid());
        //        db.Delete(task);
        //        db.Commit();

        //        return model
        //            .InArray()
        //            .ToDataSourceResult(request)
        //            .ToJsonResult();
        //    });

            /// <summary>
            /// Удалить событие
            /// </summary>
            /// <param name="request"></param>
            /// <param name="model"></param>
            /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            SchedullerIndexModel model)
            => await this.json(gb =>
            {
                gb.db.Events.Find(model.ID).ActionIfNotNull(task =>
                {
                    gb.db.Delete(task);
                    gb.db.Commit();
                });

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Календарь встреч по скайпу
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Skype(Guid? id)
            => await this.func(gb =>
            {
                if (id.IsNullOrEmpty())
                    return Content("ошибка").AsType<ViewResult>();

                var query =
                    from record in gb.db.ContactRecords
                    where record.ID == id
                    from dep in gb.db.Departaments
                    where dep.Key == Departament.SkypeDepartament
                    select new SchedullerQuestModel
                    {
                        RecordID = record.ID,
                        OrganizationID = record.OrganizationID,
                        DepartamentID = dep.ID,
                        OrgName = record.Organization.ShortName ?? record.Organization.FullName,
                        Caption = dep.Caption
                    };

                var read = query.FirstOrDefault();

                return View("Skype", read);
            });

        /// <summary>
        /// Список встреч по скайпу
        /// </summary>
        /// <param name="request"></param>
        /// <param name="recordid"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> ReadSkype(
            [DataSourceRequest] DataSourceRequest request,
            Guid? recordid)
            => await this.json(gb =>
            {
                var db = gb.db;
                var query = (
                        from record in db.ContactRecords
                        where record.ID == recordid
                        from dep in db.Departaments
                        where dep.Key == Departament.SkypeDepartament
                        select new
                        {
                            orgid = record.OrganizationID,
                            depid = dep.ID,
                            tasks = dep.Events
                        }
                    ).AsNoTracking()
                    .FirstOrDefault();

                return query.tasks
                    .Select(v => new SchedullerModel
                    {
                        Description = v.Description ?? string.Empty,
                        ID = v.ID.ToString(),
                        Title = v.Type.AtValue(),
                        _Start = v.Start,
                        _End = v.End,
                        OrganizationID = query.orgid.ToString(),
                        IsAllDay = false,
                        RecordID = recordid.Value.ToString(),
                        DepartamentID = query.depid.ToString(),
                        NotEdit = query.orgid != v.OrgID,
                        OrgName = v.Organization.ShortName
                            .retIfNullOrWhiteSpace(v.Organization.FullName)
                            .retIfNullOrWhiteSpace("не указано"),
                        Notify = v.Notify?.Date != v.Start?.Date,
                        _NotifyTime = v.Notify ?? v.Start,
                        Consultant = v.NotifyConsultant
                    })
                    .AsEnumerable()
                    .Select(v =>
                    {
                        v.Start = v._Start.ToTZDateTime(gb.TimeZone);
                        v.End = v._End.ToTZDateTime(gb.TimeZone);
                        v.NotifyTime = v._NotifyTime.ToTZDateTime(gb.TimeZone);
                        return v;
                    })
                    .ToDataSourceResult(request);
            });

        //[ValidateInput(false)]
        //[AcceptVerbs(HttpVerbs.Post)]
        //[Roles(dbroles.admin, dbroles.@operator)]
        //public async Task<ActionResult> CreateSkype(
        //    [DataSourceRequest] DataSourceRequest request,
        //    SchedullerModel model)
        //    => await this.async(lazy =>
        //    {
        //        var db = lazy.Value;

        //        model.End = model.Start.AddHours(3);

        //        var task = new Event
        //        {
        //            Description = model.Description,
        //            End = model.End,
        //            Start = model.Start,
        //            OrgID = model.OrganizationID.ToGuid(),
        //            RecordID = model.RecordID.ToGuid(),
        //            Type = EventType.skype
        //        };

        //        SetNotify(model, task);

        //        task.Departaments.Add(db.Departaments.Find(model.DepartamentID.ToGuid()));
        //        if (!model.Consultant)
        //            task.Users.Add(db.User);

        //        db.Add(task);
        //        db.Commit();

        //        model.ID = task.ID.ToString();

        //        return model
        //            .InArray()
        //            .ToDataSourceResult(request)
        //            .ToJsonResult();
        //    });

            /// <summary>
            /// Подтвердить встречу
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> EventConfirm(SchedullerQuestModel data)
            => await this.act(gb =>
            {
                gdb.add_event(
                    gb.db,
                    data.EventID.Value,
                    data.OrganizationID.Value,
                    data.RecordID,
                    data.Comment);
            });

        /// <summary>
        /// Получение списка встреч
        /// </summary>
        /// <param name="request"></param>
        /// <param name="recordid"></param>
        /// <param name="eventid"></param>
        /// <param name="orgid"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> ReadTrip(
            [DataSourceRequest] DataSourceRequest request,
            Guid? recordid,
            Guid? eventid,
            Guid? orgid)
            => await this.json(gb =>
            {
                return (
                    from task in gb.db.Events
                    where task.ID == eventid
                    from record in gb.db.ContactRecords
                    where record.ID == recordid
                    from task_event in task.Prev.Next
                    from task_meeting in task_event.Next
                    select new
                    {
                        task_meeting.Description,
                        task_meeting.ID,
                        task_meeting.Type,
                        _Start = task_meeting.Start ?? task_event.Start,
                        _End = task_meeting.End ?? task_event.End,
                        OrganizationID = task_meeting.OrgID,
                        NotifyTime = task_meeting.Notify,
                        task_meeting.NotifyConsultant,
                        RecordID = record.ID,
                        NotEdit = task_meeting.OrgID.Value != orgid || task_meeting.Type == EventType.eventSeminar,
                        OrgName = task_meeting.Organization.ShortName ?? task_meeting.Organization.FullName,
                        ResID = task_meeting.Organization
                            .Address
                            .Where(v => v.Type == AddressType.region)
                            .Select(v => v.TimeZone)
                            .FirstOrDefault(),
                        eventid = task.ID
                    })
                    .AsEnumerable()
                    .Select(item => new SchedullerIndexModel
                    {
                        Description = item.Description.HtmlDecode().FromHtml(),
                        ID = item.ID,
                        Type = item.Type,
                        Start = item._Start.ToTZDateTime(item.ResID),
                        End = item._End.ToTZDateTime(item.ResID),
                        OrganizationID = item.OrganizationID,
                        RecordID = item.RecordID,
                        NotEdit = item.NotEdit,
                        OrganizationName = item.OrgName,
                        NotifyTime = item.NotifyTime.ToTZDateTime(item.ResID),
                        NotifyConsultant = item.NotifyConsultant,
                        Title = item.Type.AtValue(),
                        Notify = item.NotifyTime?.Date != item._Start?.Date,
                        Region = item.eventid.ToString()
                    })
                    .Select(v =>
                    {
                        if (v.Type == EventType.eventSeminar)
                            v.OrganizationName = "Семинар";
                        return v;
                    })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Создание встречи
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        [ValidateInput(false)]
        public async Task<ActionResult> CreateTrip(
            [DataSourceRequest] DataSourceRequest request,
            SchedullerIndexModel model)
            => await this.json(gb =>
            {
                model.ID = gdb.add_trip(
                    gb.db,
                    model.Region.ToGuid(),
                    model.Start,
                    model.Description,
                    model.RecordID.Value,
                    model.NotifyTime,
                    model.NotifyConsultant
                );

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });
    }
}