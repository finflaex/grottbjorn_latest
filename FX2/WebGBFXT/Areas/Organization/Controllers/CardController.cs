﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using DBFXFias;
using Finflaex.Support.Attributes;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using WebGBFXT.Areas.Organization.Models;
using WebGBFXT.Controllers;

#endregion

namespace WebGBFXT.Areas.Organization.Controllers
{
    /// <summary>
    /// Карточка организации
    /// </summary>
    [NoCache]
    [Roles]
    public partial class CardController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public ActionResult Index(Guid? id)
        {
            var gb = new ActionData(this);

            if (id.IsNullOrEmpty())
            {
                Response.Redirect($"{Request.Path}?{nameof(id)}={Guid.NewGuid():N}");
                return null;
            }

            var model = (
                    from org in gb.db.Organizations
                    where org.ID == id
                    select new BaseOrgModel
                    {
                        ID = org.ID.ToString(),
                        OrgTitle = org.ShortName ?? org.FullName,
                        OrgShortName = org.ShortName ?? string.Empty,
                        OrgFullName = org.FullName ?? string.Empty,
                        OrgInn = org.Inn ?? string.Empty,
                        OrgComment = org.Description ?? string.Empty,
                        OrgAddress = org.FormatAddress
                                     ?? org.RawAddress
                                     ?? string.Empty,
                        OrgAddressLegal = org.LegalAddress,
                        OrgAddressPhyzical = org.PhysicalAdress,
                        OrgClose = org.CloseInfo != null,
                        OrgVerify = org.Work,
                        OrgClientCode = org.ClientCode,
                        Editors = (from task in org.Events
                                   where task.Type == EventType.edit
                                   where !task.Closed
                                   where task.Start < gb.Now
                                   where task.End == null || task.End > gb.Now
                                   from user in task.Users
                                   select user.ID)
                            .Distinct(),
                        LegalCity = from addr in org.Address
                                    where addr.Type < AddressType.street
                                    orderby addr.Type
                                    select new GuidValue
                                    {
                                        Guid = addr.ID,
                                        Value = addr.Suffix + " " + addr.Name
                                    },
                        PhyzicCity = from addr in org.PhysicAddresses
                                     where addr.Type < AddressType.street
                                     orderby addr.Type
                                     select new GuidValue
                                     {
                                         Guid = addr.ID,
                                         Value = addr.Suffix + " " + addr.Name
                                     },
                        LegalStreet = org.LegalAddress,
                        PhyzicStreet = org.PhysicalAdress
                    })
                .FirstOrDefault();

            if (model != null)
            {
                this.WaitWS(gb.AuthorGuid, socket =>
                {
                    socket
                        .AddMethod("AddPost", AddPost)
                        .AddMethod("AddPerson", AddPerson)
                        .AddMethod("AddContact", AddContact)
                        .AddMethod("OrgShortName", OrgShortName)
                        .AddMethod("OrgFullName", OrgFullName)
                        .AddMethod("OrgInn", OrgInn)
                        .AddMethod("OrgAddress", OrgAddress)
                        .AddMethod("OrgAddressLegal", OrgAddressLegal)
                        .AddMethod("OrgAddressPhyzical", OrgAddressPhyzical)
                        .AddMethod("OrgComment", OrgComment)
                        .AddMethod("AddNewPost", AddNewPost)
                        .AddMethod("AddNewPerson", AddNewPerson)
                        .AddMethod("AddNewContact", AddNewContact)
                        .AddMethod("EditPost", EditPost)
                        .AddMethod("EditPerson", EditPerson)
                        .AddMethod("EditContact", EditContact)
                        .AddMethod("DelPost", DelPost)
                        .AddMethod("DelPerson", DelPerson)
                        .AddMethod("DelContact", DelContact)
                        .AddMethod("EventComment", EventComment)
                        .AddMethod("AddEventComment", AddEventComment)
                        .AddMethod("AddEventBell", AddEventBell)
                        .AddMethod("EventDate", EventDate)
                        .AddMethod("EventTime", EventTime)
                        .AddMethod("SetEventType", SetEventType)
                        .AddMethod("SetEventClose", SetEventClose)
                        .AddMethod("SetInterestType", SetInterestType)
                        .AddMethod("SetInterestComment", SetInterestComment)
                        .AddMethod("SetYearCurrency", SetYearCurrency)
                        .AddMethod("SetSingleCurrency", SetSingleCurrency)
                        .AddMethod("SetStatusCurrency", SetStatusCurrency)
                        .AddMethod("SetMeetingConfirm", SetMeetingConfirm)
                        .AddMethod("SetMeetingCancel", SetMeetingCancel)
                        .AddMethod("SetRateValue", SetRateValue)
                        .AddMethod("SetRateAnswer", SetRateAnswer)
                        .AddMethod("OrgClientCode", OrgClientCode)
                        .AddMethod("EventResultComment", EventResultComment)
                        .AddMethod("SetEventPlanAccaunt", SetEventPlanAccaunt)
                        ;
                    ShowInterests(id, socket, gb.db);
                    ShowOkved(id, socket, gb.db);
                    ShowRecord(id, socket, gb.db);
                    ShowPosts(id, socket, gb.db);
                    ShowPersons(id, socket, gb.db);
                    ShowContacts(id, socket, gb.db);
                    ShowEvents(id, socket, gb.db);
                    ShowFiles(id, socket, gb.db);
                });

                ViewData["InterestType"] = Enum
                    .GetValues(typeof(InterestType))
                    .Cast<InterestType>()
                    .Select(v => new IdValue
                    {
                        Id = (int)v,
                        Value = v.AtValue()
                    })
                    .ToArray();

                ViewData["db"] = gb.db;


                return View("ShowOrg", model);
            }


            this.WaitWS(gb.AuthorGuid, socket =>
            {
                socket
                    .AddMethod("AddOrg", AddOrg)
                    .AddMethod("AddOrgForce", AddOrgForce)
                    ;
            });
            model = new BaseOrgModel(id.Value)
            {
                OrgTitle = "Добавление новой организации"
            };
            return View("NewOrg", model);
        }

        /// <summary>
        /// Получение файла
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.cik)]
        public async Task<ActionResult> ReadFile(Guid? id)
            => await this.func(gb =>
            {
                var file = gb.db.Files.Find(id);

                if (file != null)
                {
                    var stream = new MemoryStream(file.Raw);
                    return new FileStreamResult(stream, Response.ContentType)
                    .AsType<ActionResult>();
                }

                return new EmptyResult().AsType<ActionResult>();
            });

        ///// <summary>
        ///// Получение файла
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[Roles(dbroles.admin, dbroles.@operator)]
        //public async Task<ActionResult> ReadFile(string id)
        //    => await this.func(gb =>
        //    {
        //        var file = gb.db.Files.FirstOrDefault(v => v.Name == id);

        //        if (file != null)
        //        {
        //            var stream = new MemoryStream(file.Raw);
        //            return new FileStreamResult(stream, Response.ContentType)
        //            .AsType<ActionResult>();
        //        }

        //        return new EmptyResult().AsType<ActionResult>();
        //    });

        /// <summary>
        /// Получение файла по пути
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> ReadFilePath(string path)
            => await this.func(lazy =>
                fxr.AsUser(() =>
                {
                    if (System.IO.File.Exists(path))
                    {
                        var mas = System.IO.File.ReadAllBytes(path);
                        long fSize = mas.Length;
                        long startbyte = 0;
                        var endbyte = fSize - 1;
                        var statusCode = 200;
                        if (Request.Headers["Range"] != null)
                        {
                            var range = Request.Headers["Range"].Split('=', '-');
                            startbyte = Convert.ToInt64(range[1]);
                            if (range.Length > 2 && range[2] != "") endbyte = Convert.ToInt64(range[2]);
                            if (startbyte != 0 || endbyte != fSize - 1 || range.Length > 2 && range[2] == "")
                                statusCode = 206;
                        }
                        var desSize = endbyte - startbyte + 1;
                        Response.StatusCode = statusCode;
                        Response.ContentType = "audio/mp3";
                        Response.AddHeader("Content-Accept", Response.ContentType);
                        Response.AddHeader("Content-Length", desSize.ToString());
                        Response.AddHeader("Content-Range", $"bytes {startbyte}-{endbyte}/{fSize}");

                        var stream = new MemoryStream(mas, (int) startbyte, (int) desSize);

                        return new FileStreamResult(stream, Response.ContentType);
                    }

                    return null;
                }, "asterisk_read", "sbc", "GAL0Xu1t"));

        /// <summary>
        /// Выбор пользователей имеющих право редактировать организацию
        /// </summary>
        /// <param name="id"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public async Task<ActionResult> ChangeEditors(Guid? id, Guid[] values)
            => await this.act(gb =>
            {
                if (id.IsNullOrEmpty()) return;

                var edit = (
                        from org in gb.db.Organizations
                        where org.ID == id
                        from task in org.Events
                        where task.Type == EventType.edit
                        select task)
                    .Include(v => v.Users)
                    .FirstOrDefault();

                if (edit == null)
                    edit = gb.db.Add(new Event
                    {
                        Start = DateTimeOffset.Now,
                        OrgID = id,
                        Type = EventType.edit
                    });

                var rem = edit.Users.Select(v => v.ID).Except(values);
                var add = values.Where(v => v != Guid.Empty).Except(edit.Users.Select(v => v.ID));

                edit.Users.Where(v => rem.Contains(v.ID))
                    .ToArray()
                    .ForEach(v => edit.Users.Remove(v));

                gb.db.Logins.Where(v => add.Contains(v.ID))
                    .ToArray()
                    .ForEach(v => edit.Users.Add(v));

                gb.Save();
            });

        /// <summary>
        /// Выбор ответственного
        /// </summary>
        /// <param name="OrgID"></param>
        /// <param name="AccID"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> ChangeAccaunted(
            Guid? OrgID, Guid? AccID)
            => await this.act(gb =>
            {
                var query =
                    from org in gb.db.Organizations
                    where org.ID == OrgID
                    select new
                    {
                        org,
                        acc = (from acc in gb.db.Logins
                            where acc.ID == AccID
                            select acc)
                        .FirstOrDefault(),
                        old = (from old in org.Events
                            where !old.Closed
                            where old.Start < gb.Now
                            where old.End > gb.Now
                            where old.Type == EventType.progress
                            select old)
                        .FirstOrDefault()
                    };

                var read = query.FirstOrDefault();

                if (read == null || read.org == null) return;

                if (read.old != null)
                {
                    read.old.Closed = true;
                    read.old.End = DateTimeOffset.Now;
                }
                read.org.Events.Add(new Event
                {
                    Type = EventType.progress,
                    Start = DateTimeOffset.Now,
                    End = DateTimeOffset.MaxValue,
                    Users = read.acc.InArray()
                });
                gb.Save();
            });

        /// <summary>
        /// Изменение физического адреса организации
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Guids"></param>
        /// <returns></returns>
        public async Task<ActionResult> PhyzicCityChange(
            Guid? ID,
            IEnumerable<Guid> Guids)
            => await this.func(gb =>
            {
                if (Guids == null)
                    return new HttpStatusCodeResult(409);
                var org = gb.db.Organizations.Find(ID);
                using (var fias = new FiasDB())
                {
                    var guids = Guids.Select(v => v.ToString());
                    var list = (
                            from addr in fias.AddrObj
                            where guids.Contains(addr.AOGUID)
                            select addr
                        )
                        .AsEnumerable()
                        .Select(v => new Address
                        {
                            ID = v.AOGUID.ToGuid(),
                            Name = v.FORMALNAME,
                            ParentID = v.PARENTGUID.ToGuid(),
                            Suffix = v.SHORTNAME,
                            Type = (AddressType) v.AOLEVEL.ToInt()
                        });

                    var has = (
                            from addr in gb.db.Addresses
                            where Guids.Contains(addr.ID)
                            select addr.ID)
                        .ToArray();

                    return Content("").AsType<ActionResult>();
                }
            });

        /// <summary>
        /// Изменение юридического адреса организации
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Guids"></param>
        /// <returns></returns>
        public async Task<ActionResult> LegalCityChange(
            Guid? ID,
            IEnumerable<Guid> Guids)
            => await this.func(gb =>
            {
                if (Guids == null)
                    return new HttpStatusCodeResult(409);

                var query =
                    from org in gb.db.Organizations
                    where org.ID == ID
                    let has = org.Address
                    let has_id = has.Select(v => v.ID)
                    let add = gb.db.Addresses
                        .Where(v => Guids.Contains(v.ID))
                        .Where(v => !has_id.Contains(v.ID))
                    let del = has
                        .Where(v => !Guids.Contains(v.ID))
                    let load = Guids
                        .Where(v => gb.db.Addresses.All(a => a.ID != v))
                    select new
                    {
                        org,
                        add,
                        del,
                        load
                    };

                var read = query.Include(v => v.add).FirstOrDefault();

                if (read == null)
                    return new HttpStatusCodeResult(409);

                foreach (var addr in read.add)
                    read.org.Address.Add(addr);

                foreach (var addr in read.del)
                    read.org.Address.Remove(addr);


                using (var fias = new FiasDB())
                {
                    var guids = read.load.Select(v => v.ToString());
                    (
                            from addr in fias.AddrObj
                            where guids.Contains(addr.AOGUID)
                            select addr
                        )
                        .AsEnumerable()
                        .Select(v => new Address
                        {
                            ID = v.AOGUID.ToGuid(),
                            Name = v.FORMALNAME,
                            ParentID = v.PARENTGUID.ToGuid(),
                            Suffix = v.SHORTNAME,
                            Type = (AddressType) v.AOLEVEL.ToInt()
                        })
                        .ForEach(addr => { read.org.Address.Add(addr); });
                }

                //gb.db.Commit();

                return Content("").AsType<ActionResult>();
            });

        /// <summary>
        /// Сделать беспризорником
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> GiveBack(Guid? id)
            => await this.act(gb =>
            {
                var query =
                    from e in gb.db.Events
                    where e.Start < gb.Now
                    where e.End > gb.Now
                    where !e.Closed
                    where e.OrgID == id
                    where e.Type == EventType.edit || e.Type == EventType.progress
                    select e;

                query.AsEnumerable()
                    .ForEach(v =>
                    {
                        v.Closed = true;
                        v.End = gb.Now;
                    });

                gb.db.Commit();
            });

        /// <summary>
        /// Удалить событие
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> RemoveEvent(Guid? id)
            => await this.act(gb =>
            {
                var query =
                    from e in gb.db.Events
                    where e.ID == id
                    select e;

                var read = query.FirstOrDefault();

                gb.db.Events.Remove(read);

                gb.db.Commit();
            });
    }
}