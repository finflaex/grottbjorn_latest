﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Models;
using BaseDBFX.Table;
using Finflaex.Support.Attributes;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json.Converters;
using WebGBFXT.Areas.Operator.Models;

#endregion

namespace WebGBFXT.Areas.Organization.Controllers
{
    /// <summary>
    /// Таблица встреч
    /// </summary>
    [Roles(dbroles.consultant, dbroles.@operator, dbroles.admin)]
    public class TableEventController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [NoCache]
        public ActionResult Index()
        {
            ViewBag.gb = new ActionData(this);
            return View(new ActionData(this));
        }

        /// <summary>
        /// Сохранение положения блока фильтров
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        public async Task<ActionResult> SaveOpenConfig(
            bool flag)
            => await this.act(gb =>
            {
                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    select new
                    {
                        user,
                        file = user.Files
                            .Where(file => file.Type == TypeFile.Config)
                            .FirstOrDefault(file => file.Name == Login.Configs.ConsultantMeetingOpenConfig)
                    };

                var read = query.First();

                var value = new ConsultantMeetingOpenConfig
                {
                    Open = !flag
                }.AsBytes();

                if (read.file == null)
                    read.user.Files.Add(new RawFile
                    {
                        Raw = value,
                        Type = TypeFile.Config,
                        Name = Login.Configs.ConsultantMeetingOpenConfig
                    });
                else
                    read.file.Raw = value;
                gb.Save();
            });

        /// <summary>
        /// Получение списка встреч
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [NoCache]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
        {
            var show_types = new[]
            {
                EventType.meeting,
                EventType.meetingConfirm,
                EventType.meetingEvent
            };

            return await this.json(gb =>
            {
                var db = gb.db;

                var filter = (
                        from user in gb.db.Logins
                        where user.ID == gb.AuthorGuid
                        from file in user.Files
                        where file.Type == TypeFile.Config
                        where file.Name == Login.Configs.ConsultantMeetingConfig
                        select file
                    )
                    .FirstOrDefault()
                    .retIfNull(new RawFile {Raw = new byte[0]})
                    .FuncSelect(raw => raw.Raw.AsObject<ConsultantMeetingConfig>())
                    .retIfNull(new ConsultantMeetingConfig());

                if (filter.Users == null) filter.Users = new Guid[0];
                if (filter.Types == null) filter.Types = new EventType[0];
                if (filter.Region == null) filter.Region = new Guid[0];

                var hasuser = !filter.Users.Any();
                var hastype = !filter.Types.Any();
                var hasadr = !filter.Region.Any();
                var types = !hastype
                    ? filter.Types
                    : new[]
                    {
                        EventType.meeting,
                        EventType.meetingConfirm,
                        EventType.meetingCancel,
                        EventType.meetingClose,
                        EventType.meetingEvent,
                        EventType.eventSeminar
                    };
                var hasname = filter.Name.IsNullOrWhiteSpace();

                if (filter.Start < new DateTime(2000, 1, 1))
                    filter.Start = new DateTime(2000, 1, 1);
                if (filter.End < new DateTime(2000, 1, 1))
                    filter.End = new DateTime(2000, 1, 1);
                var start = filter.Start.Date;
                var end = filter.End.Date.AddDays(1);

                var deps = filter.Departaments ?? new Guid[0];

                var query1 =
                    from dep in db.Departaments
                    where deps.Contains(dep.ID)
                    where dep.Parent.Key == Departament.ConsultantDepartament
                    from task in dep.Events
                    where task.Type != EventType.eventSeminar
                    select task;

                var query2 = filter.OtherSeminar
                    ? (
                        from task in db.Events
                        where task.Type == EventType.eventSeminar
                        select task
                    )
                    : (
                        from dep in db.Departaments
                        where dep.Parent.Key == Departament.ConsultantDepartament
                        where deps.Contains(dep.ID)
                        from task in dep.Events
                        where task.Type == EventType.eventSeminar
                        select task
                    );

                var query =
                    from task in query1.Concat(query2).Distinct()
                    //from task in query2
                    where types.Contains(task.Type)
                    where !task.Closed
                    where task.Start > start
                    where task.End < end
                    where hasadr
                          || task.Organization.Address.Select(v => v.ID).Intersect(filter.Region).Any()
                          || task.Prev.Address.Select(v => v.ID).Intersect(filter.Region).Any()
                    where hasuser || task.Users
                              .Select(v => v.ID)
                              .Intersect(filter.Users)
                              .Any()
                    where hasname
                          || task.Prev.Prev.Caption.Contains(filter.Name)
                          || task.Organization.FullName.Contains(filter.Name)
                          || task.Organization.ShortName.Contains(filter.Name)
                          || task.Organization.Inn.Contains(filter.Name)
                    select new
                    {
                        task.ID,
                        task.OrgID,
                        Start = task.Type != EventType.eventSeminar ? task.Start : task.Prev.Start,
                        Name = task.Organization.ShortName ?? task.Organization.FullName ?? task.Prev.Prev.Caption,
                        Address1 = task.Organization.Address,
                        Address2 = task.Prev.Address,
                        Accaunts = task.Users.Where(v => v.Roles.Any(r => r.Key == dbroles.consultant)),
                        task.Type
                    };

                return query
                    .OrderBy(v => v.Start)
                    .AsEnumerable()
                    .Select(v => new EventModel
                    {
                        ID = v.OrgID,
                        EventID = v.ID,
                        Start = v.Start.ToTZDateTime(gb.TimeZone),
                        Name = v.Name,
                        Address = (v.Type != EventType.eventSeminar ? v.Address1 : v.Address2)
                            .Where(a => a.Type > AddressType.region || a.Suffix == "г")
                            .Where(a => a.Type < AddressType.street)
                            .OrderBy(a => a.Type)
                            .Select(a => $"{a.Name} {a.Suffix}")
                            .Join(", "),
                        Account = v.Accaunts.Select(acc => acc.Caption)?.Join(", ") ?? string.Empty,
                        Accounts = v.Accaunts.Select(acc => acc.ID),
                        Type = v.Type.AtValue(),
                        MSK = gb.TimeZone.ToMSK(),
                        ShowEdit = show_types.Contains(v.Type)
                    })
                    .ToDataSourceResult(request);
            });
        }

        /// <summary>
        /// Сохранение пользовательских настроек
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> SaveConfig(
            string model)
            => await this.act(gb =>
            {
                var cfg = model.FromJson<ConsultantMeetingConfig>(new IsoDateTimeConverter
                {
                    DateTimeFormat = "dd.MM.yyyy"
                });

                if (cfg.Start < new DateTime(2000, 1, 1))
                    cfg.Start = new DateTime(2000, 1, 1);
                if (cfg.End < new DateTime(2000, 1, 1))
                    cfg.End = new DateTime(2000, 1, 1);

                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    select new
                    {
                        user,
                        file = user.Files
                            .Where(file => file.Type == TypeFile.Config)
                            .FirstOrDefault(file => file.Name == Login.Configs.ConsultantMeetingConfig)
                    };

                var read = query.First();

                if (read.file == null)
                    read.user.Files.Add(new RawFile
                    {
                        Raw = cfg.AsBytes(),
                        Type = TypeFile.Config,
                        Name = Login.Configs.ConsultantMeetingConfig
                    });
                else
                    read.file.Raw = cfg.AsBytes();
                gb.Save();
            });

        /// <summary>
        /// Загрузка пользовательских настроек
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> LoadTableConfig()
            => await this.func(gb =>
            {
                var cfg = (
                        from user in gb.db.Logins
                        where user.ID == gb.AuthorGuid
                        from file in user.Files
                        where file.Type == TypeFile.Config
                        where file.Name == Login.Configs.ConsultantMeetingConfig
                        select file)
                    .FirstOrDefault();

                try
                {
                    cfg.TryIfNull();

                    var filter = cfg.Raw.AsObject<ConsultantMeetingConfig>();

                    filter.TryIfNull();
                    filter.PersistState.TryIfNull();

                    return filter.PersistState.FromJson<fxcDictSO>()
                        .Action(v => { v["AutoSave"] = filter?.AutoSave ?? false; })
                        .ToJsonResult();
                }
                catch
                {
                    return false.ToJsonResult();
                }
            });

        /// <summary>
        /// Обновление встречи
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            EventModel model)
            => await this.json(gb =>
            {
                model.Accounts = Request
                    .Form
                    .AllKeys
                    .Where(v => v.DoRegexIsMatch(@"^Accounts\[\d+\](.ID)?()$"))
                    .Select(v => Request.Form[v])
                    .Select(v => v.ToGuid())
                    .ToArray();

                var query = (
                        from meeting in gb.db.Events
                        where meeting.ID == model.EventID
                        let has_users = (
                            from user in meeting.Users
                            where user.Roles.Any(v=>v.Key == dbroles.consultant)
                            select user
                        )
                        let nid_users = (
                            from user in gb.db.Logins
                            where model.Accounts.Contains(user.ID)
                            where user.Roles.Any(v => v.Key == dbroles.consultant)
                            select user
                        )
                        select new
                        {
                            meeting,
                            add_users = nid_users.Except(has_users),
                            del_users = has_users.Except(nid_users),
                            interest = meeting
                                .Activities
                                .Select(v => v.Caption),
                            clientTimeZone = meeting
                                .Organization
                                .Address
                                .OrderByDescending(v => v.Type)
                                .Select(v => v.TimeZone)
                                .FirstOrDefault(v => v != 0),
                            orgName = meeting.Organization.ShortName ?? meeting.Organization.FullName,
                            orgAddress = meeting.Organization.FormatAddress,
                            author = (
                                from auth in gb.db.Logins
                                where auth.ID == gb.AuthorGuid
                                select auth.Caption
                            )
                            .FirstOrDefault(),
                            contact = (
                                from record in gb.db.ContactRecords
                                where record.ID == meeting.RecordID
                                select new
                                {
                                    posts = record.Posts.Select(v => v.Caption),
                                    persons = record.Persons.Select(v => v.FullName ?? v.RawName),
                                    contacts = record.Contacts.Select(v => v.ParseValue ?? v.RawValue)
                                }
                            )
                            .FirstOrDefault()
                        }
                    )
                    .FirstOrDefault();

                query.ThrowIfNull("null query meeting with accaunts");

                var from = gb.db.Logins.Find(gb.db.AuthorID);
                foreach (var user in query.del_users)
                {
                    var text = $@"
Добрый день {user.Caption}!

Сообщаю, что вас сняли со встречи, на которую вы были назначены:
-----------------------------
Дата: {query.meeting.Start.ToTZDateTime(query.clientTimeZone):dd MMMM yyyyг. ddd HH:mm} ({
                            query.clientTimeZone.ToMSK()
                        })

Наименование: {query.orgName}.
Адрес организации: {query.orgAddress}.

Выполнивший действие: {query.author}.
---------------------------- -
Комментарий к действию:
{model.Description.HtmlDecode().FromHtml()}
".Replace("\r\n", "<br>");

                    query.meeting.Users.Remove(user);
                    gdb.send_mail(from, user, "Вас сняли со встречи", text);
                }

                foreach (var user in query.add_users)
                {
                    var text = $@"
Добрый день {user.Caption}!

Сообщаю, что вас назначили на встречу:
-----------------------------
Дата: {query.meeting.Start.ToTZDateTime(query.clientTimeZone):dd MMMM yyyyг. ddd HH:mm} ({
                            query.clientTimeZone.ToMSK()
                        })

Наименование: {query.orgName}.
Адрес организации: {query.orgAddress}.

Должность: {query.contact.posts.Join(", ")}.
Имя: {query.contact.persons.Join(", ")}.
Контакт: {query.contact.contacts.Join(", ")}.

Выполнивший действие: {query.author}.
---------------------------- -
Комментарий к встрече:
{query.meeting.Description.HtmlDecode().FromHtml()}
Комментарий к действию:
{model.Description.HtmlDecode().FromHtml()}
".Replace("\r\n", "<br>");

                    query.meeting.Users.Add(user);
                    gdb.send_mail(from, user, "Вы назначены на встречу", text);
                }

                gb.Save();

                model.Account = query.meeting.Users.Select(v => v.Caption).Join(", ");

                return model.InArray().ToDataSourceResult(request);
            });

        public class EventModel
        {
            public Guid? ID { get; set; }
            public string Address { get; set; }
            public string Name { get; set; }
            public DateTime Start { get; set; }
            public string Account { get; set; }
            public string Comment { get; set; }
            public string Type { get; set; }
            public Guid? EventID { get; set; }
            public string MSK { get; set; }

            public IEnumerable<Guid> Accounts { get; set; }
            public string Description { get; set; }
            public bool ShowEdit { get; set; }
        }
    }
}