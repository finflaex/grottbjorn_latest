﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.Attributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;
using TelephonDBFX.Base;
using WebGBFXT.Controllers;

#endregion

namespace WebGBFXT.Areas.Organization.Controllers
{
    /// <summary>
    /// Расширение для карточки организации
    /// </summary>
    partial class CardController
    {
        /// <summary>
        /// Проверка при добавлении новой организации
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private void AddOrg(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var shortName = data.GetString("shortName").HtmlDecode().FromHtml().Trim();
            var fullName = data.GetString("fullName").HtmlDecode().FromHtml().Trim();
            var inn = data.GetString("inn").HtmlDecode().FromHtml().Trim();

            var shortFlag = shortName.NotNullOrWhiteSpace();
            var fullFlag = fullName.NotNullOrWhiteSpace();
            var flagInn = inn.NotNullOrWhiteSpace();

            gdb.action(socket.user, db =>
            {
                var query = db.Organizations.AsQueryable();

                if (flagInn)
                    query = query.Where(v => v.Inn == inn);
                else if (shortFlag || fullFlag)
                    query = query.Where(v
                        => shortFlag && v.ShortName.Contains(shortName)
                           || shortFlag && v.FullName.Contains(shortName)
                           || fullFlag && v.ShortName.Contains(fullName)
                           || fullFlag && v.FullName.Contains(fullName)
                    );

                if (query.Any())
                    socket.Send(new
                    {
                        method = "result",
                        list = query.Select(org => new
                            {
                                id = org.ID,
                                name = org.ShortName ?? org.FullName,
                                address = org.FormatAddress ?? org.RawAddress,
                                inn = org.Inn,
                                accaunt = org
                                    .Events
                                    .Where(task => task.Type == EventType.progress)
                                    .Where(task => !task.Closed)
                                    .SelectMany(task => task.Users)
                                    .Select(v => v.Caption)
                            })
                            .ToArray()
                            .Select(v => new
                            {
                                v.id,
                                v.name,
                                v.address,
                                v.inn,
                                accaunt = v.accaunt.Join(", ")
                            })
                    });
                else
                    AddOrgForce(socket, data);
            });
        }

        /// <summary>
        /// Добавление новой организации
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private void AddOrgForce(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var shortName = data.GetString("shortName").HtmlDecode().FromHtml().Trim();
            var fullName = data.GetString("fullName").HtmlDecode().FromHtml().Trim();
            var inn = data.GetString("inn").HtmlDecode().FromHtml().Trim();
            var id = data.GetString("id").ToGuid();

            this.act(gb =>
            {
                gb.orgedit.create("создана пользователем", org =>
                {
                    org.ID = id;
                    shortName.NotNullOrWhiteSpace().IfTrue(() => { org.ShortName = shortName; });
                    fullName.NotNullOrWhiteSpace()
                        .IfTrue(() =>
                        {
                            org.ShortName = fullName;
                            org.FullName = fullName;
                        });
                    inn.NotNullOrWhiteSpace().IfTrue(() => { org.Inn = inn; });
                });

                socket.Send(new
                {
                    method = "reload"
                });
            });
        }

        /// <summary>
        /// Добавить должность
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void AddPost(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var recordID = data.GetString("record").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            var flag = value.IsNullOrWhiteSpace();
            if (flag) return;

            gdb.action(socket.user, db =>
            {
                value = db.Replace(value).ToLower();

                var post = db.Posts.FirstOrDefault(v => v.Caption == value);
                if (post == null)
                    post = new Post
                    {
                        Caption = value
                    };

                var record = db.ContactRecords.Find(recordID);
                record.Posts.Add(post);
                db.Commit();

                socket.Send(new
                {
                    record = recordID,
                    method = "add_post",
                    data = new
                    {
                        id = post.ID,
                        value = post.Caption
                    }
                });
            });
        }

        /// <summary>
        /// Добавить контактное лицо
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void AddPerson(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var recordID = data.GetString("record").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            var flag = value.IsNullOrWhiteSpace();
            if (flag) return;

            gdb.action(socket.user, db =>
            {
                var person = new Person
                {
                    FullName = value
                };

                var record = db.ContactRecords.Find(recordID);
                record.Persons.Add(person);
                db.Commit();

                socket.Send(new
                {
                    record = recordID,
                    method = "add_person",
                    data = new
                    {
                        id = person.ID,
                        value = person.FullName
                    }
                });
            });
        }

        /// <summary>
        /// Добавить контакт
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void AddContact(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var record = data.GetString("record").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            var flag = value.IsNullOrWhiteSpace();
            if (flag) return;

            gdb.action(socket.user, db =>
            {
                var contact = new Contact
                {
                    Value = value,
                    RecordID = record
                };
                db.Add(contact);
                db.Commit();

                socket.Send(new
                {
                    record,
                    method = "add_contact",
                    data = new
                    {
                        id = contact.ID,
                        value = contact.Value,
                        type = contact.Type
                    }
                });
            });
        }

        /// <summary>
        /// Изменить краткое наименование
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void OrgShortName(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var org = db.Organizations.Find(id);
                org.ShortName = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Изменить полное наименование
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void OrgFullName(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var org = db.Organizations.Find(id);
                org.FullName = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Изменить ИНН
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void OrgInn(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var org = db.Organizations.Find(id);
                org.Inn = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Изменить адрес организации
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void OrgAddress(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var org = db.Organizations.Find(id);
                org.ParseAddress(db, value);
                //org.RawAddress = value;
                db.Commit();

                socket.Send(new
                {
                    method = "set_address",
                    value = org.FormatAddress,
                    rawValue = org.RawAddress
                });

                ShowInterests(id, socket, db);
            });
        }

        /// <summary>
        /// Изменить физический адрес организации
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void OrgAddressPhyzical(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var org = db.Organizations.Find(id);
                org.PhysicalAdress = value;
                db.Commit();

                socket.Send(new
                {
                    method = "set_address_phyzical",
                    value = org.PhysicalAdress
                });
            });
        }

        /// <summary>
        /// Изменить юридический адрес организации
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void OrgAddressLegal(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var org = db.Organizations.Find(id);
                org.LegalAddress = value;
                db.Commit();

                socket.Send(new
                {
                    method = "set_address_legal",
                    value = org.LegalAddress
                });
            });
        }

        /// <summary>
        /// Изменить комментарий к организации
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void OrgComment(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var org = db.Organizations.Find(id);
                org.Description = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Изменить код клиента
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void OrgClientCode(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value");
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var org = db.Organizations.Find(id);
                org.ClientCode = value;
                db.Commit();
            });
        }

        /// <summary>
        /// добавить новую должность
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void AddNewPost(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty() || value.IsNullOrWhiteSpace()) return;

            gdb.action(socket.user, db =>
            {
                value = db.Replace(value).ToLower();

                var post = db.Posts.FirstOrDefault(v => v.Caption == value);

                if (post == null)
                    post = new Post
                    {
                        Caption = value
                    };

                var record = new ContactRecord
                {
                    OrganizationID = id
                };
                record.Posts.Add(post);
                db.Add(record);
                db.Commit();

                socket.Send(new
                {
                    method = "add_record",
                    data = new
                    {
                        id = record.ID
                    }
                });

                socket.Send(new
                {
                    record = record.ID,
                    method = "add_post",
                    data = new
                    {
                        id = post.ID,
                        value = post.Caption
                    }
                });
            });
        }

        /// <summary>
        /// Добавить новое контактное лицо
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void AddNewPerson(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty() || value.IsNullOrWhiteSpace()) return;

            gdb.action(socket.user, db =>
            {
                var record = new ContactRecord
                {
                    OrganizationID = id
                };
                var person = new Person
                {
                    FullName = value
                };
                record.Persons.Add(person);
                db.Add(record);
                db.Commit();

                socket.Send(new
                {
                    method = "add_record",
                    data = new
                    {
                        id = record.ID
                    }
                });

                socket.Send(new
                {
                    record = record.ID,
                    method = "add_person",
                    data = new
                    {
                        id = person.ID,
                        value = person.FullName
                    }
                });
            });
        }

        /// <summary>
        /// Добавить новый контакт
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void AddNewContact(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty() || value.IsNullOrWhiteSpace()) return;

            gdb.action(socket.user, db =>
            {
                var record = new ContactRecord
                {
                    OrganizationID = id
                };
                db.Add(record);
                var contact = new Contact
                {
                    Value = value,
                    Record = record
                };
                db.Add(contact);
                db.Commit();

                socket.Send(new
                {
                    method = "add_record",
                    data = new
                    {
                        id = record.ID
                    }
                });

                socket.Send(new
                {
                    record = record.ID,
                    method = "add_contact",
                    data = new
                    {
                        id = contact.ID,
                        value = contact.Value,
                        type = contact.Type
                    }
                });
            });
        }

        /// <summary>
        /// Редактировать должность
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void EditPost(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var post = db.Posts.Find(id);

                value = db.Replace(value).ToLower();

                post.Caption = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Редактировать контактное лицо
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void EditPerson(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var person = db.Persons.Find(id);
                person.FullName = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Редактировать контакт
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void EditContact(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var contact = db.Contacts.Find(id);
                contact.Value = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Удалить должность
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void DelPost(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var recordid = data.GetString("recordid").ToGuid();
            var id = data.GetString("id").ToGuid();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var query =
                    from record in db.ContactRecords
                    where record.ID == recordid
                    from post in record.Posts
                    where post.ID == id
                    select new
                    {
                        record,
                        post,
                        flag = record.Contacts.Any()
                               || record.Persons.Any()
                               || record.Posts.Any(v => v.ID != id)
                    };

                var read = query.FirstOrDefault();
                read.record.Posts.Remove(read.post);
                if (!read.flag)
                {
                    read.record.Establisheds.ToArray().ForEach(established => { db.Delete(established); });
                    read?.record?.Comments.ToArray().ForEach(comment => { db.Delete(comment); });
                    read?.record?.Events.ToArray().ForEach(task => { db.Delete(task); });
                    db.Delete(read?.record);
                }

                db.Commit();

                if (read?.flag ?? false)
                    socket.Send(new
                    {
                        method = "delete",
                        data = new
                        {
                            selector = $"[id='{id}']"
                        }
                    });
                else
                    socket.Send(new
                    {
                        method = "delete",
                        data = new
                        {
                            selector = $"[data-record='{read?.record.ID}']"
                        }
                    });
            });
        }

        /// <summary>
        /// Удалить контактное лицо
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void DelPerson(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var recordid = data.GetString("recordid").ToGuid();
            var id = data.GetString("id").ToGuid();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var query =
                    from record in db.ContactRecords
                    where record.ID == recordid
                    from person in record.Persons
                    where person.ID == id
                    select new
                    {
                        record,
                        person,
                        flag = record.Contacts.Any()
                               || record.Posts.Any()
                               || record.Persons.Any(v => v.ID != id)
                    };

                var read = query.FirstOrDefault();
                read.record.Persons.Remove(read.person);
                if (!read.flag)
                {
                    read.record.Establisheds.ToArray().ForEach(established => { db.Delete(established); });
                    read?.record?.Comments.ToArray().ForEach(comment => { db.Delete(comment); });
                    read?.record?.Events.ToArray().ForEach(task => { db.Delete(task); });
                    db.Delete(read?.record);
                }

                db.Commit();

                if (read?.flag ?? false)
                    socket.Send(new
                    {
                        method = "delete",
                        data = new
                        {
                            selector = $"[id='{id}']"
                        }
                    });
                else
                    socket.Send(new
                    {
                        method = "delete",
                        data = new
                        {
                            selector = $"[data-record='{read?.record.ID}']"
                        }
                    });
            });
        }

        /// <summary>
        /// Удалить контакт
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void DelContact(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var
                    read = (
                            from contact in db.Contacts
                            where contact.ID == id
                            select new
                            {
                                contact,
                                record = contact.Record,
                                flag = contact.Record.Contacts.Any(v => v.ID != id)
                                       || contact.Record.Persons.Any()
                                       || contact.Record.Posts.Any()
                            })
                        .FirstOrDefault();

                db.Delete(read?.contact);
                if (!read?.flag ?? false)
                {
                    read.record.Establisheds.ToArray().ForEach(established => { db.Delete(established); });
                    read?.record?.Comments.ToArray().ForEach(comment => { db.Delete(comment); });
                    read?.record?.Events.ToArray().ForEach(task => { db.Delete(task); });
                    db.Delete(read?.record);
                }
                db.Commit();

                if (read?.flag ?? false)
                    socket.Send(new
                    {
                        method = "delete",
                        data = new
                        {
                            selector = $"[id='{id}']"
                        }
                    });
                else
                    socket.Send(new
                    {
                        method = "delete",
                        data = new
                        {
                            selector = $"[data-record='{read?.record.ID}']"
                        }
                    });
            });
        }

        /// <summary>
        /// Изменить комментарий к событию
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void EventComment(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var query =
                    from @event in db.Events
                    where @event.ID == id
                    select new
                    {
                        @event,
                        Result = @event.Type != EventType.meetingRate ? null : @event.Prev.MeetingResult
                    };

                var read = query.FirstOrDefault();

                if (read.@event.Type == EventType.meetingRate)
                {
                    var rate = read.Result.Rate;

                    if (rate == null)
                        rate = read.Result.Rate = new ConsultantRate
                        {
                            Bonus = 150
                        };

                    read.@event.Closed = true;
                    rate.Description = value;
                    db.Commit();
                }
                else
                {
                    read.@event.Description = value;
                    db.Commit();
                }
                gdb.set_meeting_comment(db, id);
            });
        }

        /// <summary>
        /// Изменить комментарий к результату встречи
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void EventResultComment(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").HtmlDecode().FromHtml().Trim();

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                db.MeetingResults.Find(id)
                    .ActionIfNotNull(result =>
                    {
                        result.Description = value;
                        db.Commit();
                    });
            });
        }

        /// <summary>
        /// Установить оценку встречи
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetRateValue(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").ToInt();

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var query =
                    from @event in db.Events
                    where @event.ID == id
                    select new
                    {
                        @event,
                        Result = @event.Type != EventType.meetingRate ? null : @event.Prev.MeetingResult
                    };

                var read = query.FirstOrDefault();

                var rate = read.Result.Rate;

                if (rate == null)
                    rate = read.Result.Rate = new ConsultantRate
                    {
                        Bonus = 100
                    };
                read.@event.Closed = true;
                rate.Rating = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Установить ответ на вопрос "ответил ли консультант на все вопросы"
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetRateAnswer(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetBool("value");

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var query =
                    from @event in db.Events
                    where @event.ID == id
                    select new
                    {
                        @event,
                        Result = @event.Type != EventType.meetingRate ? null : @event.Prev.MeetingResult,
                        id_for_mail = @event.Prev.ID
                    };

                var read = query.FirstOrDefault();

                var rate = read.Result.Rate;

                if (rate == null)
                    rate = read.Result.Rate = new ConsultantRate
                    {
                        Bonus = 100
                    };

                read.@event.Closed = true;
                rate.OkAnswer = value;
                db.Commit();

                gdb.set_meeting_result(db, id);
            });
        }

        /// <summary>
        /// Добавить комментарий
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void AddEventComment(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("record").ToGuid();

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var query =
                    from record in db.ContactRecords
                    where record.ID == id
                    select new
                    {
                        record = record.ID,
                        org = record.OrganizationID
                    };

                var read = query.FirstOrDefault();

                var task = new Event
                {
                    OrgID = read.org,
                    Start = db.Now.DateTime,
                    RecordID = read.record,
                    Type = EventType.comment
                };

                task.Users.Add(db.User);

                db.Add(task);
                db.Commit();

                socket.Send(new
                {
                    record = id,
                    method = "add_event",
                    data = new
                    {
                        id = task.ID,
                        author = db.User.Caption,
                        comment = string.Empty,
                        date = task.Start.Value.ToString("yyyy-MM-dd HH:mm"),
                        task = new object[0],
                        type = task.Type
                    }
                });
            });
        }

        /// <summary>
        /// Добавить звонок
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void AddEventBell(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("record").ToGuid();
            var date = data.GetString("date");
            var time = data.GetString("time");
            var comment = data.GetString("comment");

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var query =
                    from record in db.ContactRecords
                    where record.ID == id
                    select new
                    {
                        record = record.ID,
                        org = record.OrganizationID,
                        tzclient = record.Organization.Address.OrderByDescending(v => v.Type)
                            .Select(v => v.TimeZone)
                            .FirstOrDefault(v => v != 0),
                        tzuser = db.Logins.Where(v => v.ID == db.AuthorID).Select(v => v.TimeZone).FirstOrDefault()
                    };

                var read = query.FirstOrDefault();

                var task = new Event
                {
                    OrgID = read.org,
                    Start = db.Now.DateTime,
                    Notify = db.Now.DateTime,
                    RecordID = read.record,
                    Type = EventType.callUp
                };

                task.Users.Add(db.User);

                db.Add(task);
                db.Commit();

                socket.Send(new
                {
                    record = id,
                    method = "add_event",
                    data = new
                    {
                        id = task.ID,
                        author = db.User.Caption,
                        comment = string.Empty,
                        date_edit = task.DateModify.ToTZString(read.tzuser),
                        start = task.Start.ToTZString(read.tzclient),
                        msk = read.tzuser.ToMSK(),
                        date = task.Notify.ToTZDateTime(read.tzuser).ToString("dd.MM.yyyy"),
                        time = task.Notify.ToTZDateTime(read.tzclient).ToString("HH:mm"),
                        task = new object[0],
                        type = task.Type
                    }
                });
            });
        }

        /// <summary>
        /// Изменить дату события
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void EventDate(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value");

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var tz = (
                        from user in db.Logins
                        where user.ID == db.AuthorID
                        select user.TimeZone)
                    .FirstOrDefault();

                var task = db.Events.Find(id);
                if (task.Notify != null)
                    task.Notify = value.ToDateTime("dd.MM.yyyy")
                        .ToTZDateTimeOffset(tz)
                        .Add(task.Notify.Value.TimeOfDay);
                else
                    task.Notify = value.ToDateTime("dd.MM.yyyy").ToTZDateTimeOffset(tz).AddHours(8);
                db.Commit();
            });
        }

        /// <summary>
        /// Изменить время события
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void EventTime(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value");

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var tz = (
                        from user in db.Logins
                        where user.ID == db.AuthorID
                        select user.TimeZone)
                    .FirstOrDefault();

                var task = db.Events.Find(id);
                if (task.Notify != null)
                    task.Notify = task.Notify.Value.Date.ToTZDateTimeOffset(tz).Add(value.ToTimeSpan("g"));
                else
                    task.Notify = db.Now.Date.ToTZDateTimeOffset(tz).Add(value.ToTimeSpan("g"));
                db.Commit();
            });
        }

        /// <summary>
        /// Изменить тип события
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetEventType(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").ToInt();

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var task = db.Events.Find(id);
                task.Type = (EventType) value;
                db.Commit();
            });
        }

        /// <summary>
        /// Указать у кого событие будет отображаться в плане обзвона
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetEventPlanAccaunt(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").FromJson<Guid[]>();

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var query =
                    from e in db.Events
                    where e.ID == id
                    let has = e.Users
                    let skip_user = has.Where(v => value.Contains(v.ID))
                    select new
                    {
                        task = e,
                        remove_user = has
                            .Where(v => !value.Contains(v.ID)),
                        add_user = db.Logins
                            .Where(v => value.Contains(v.ID))
                            .Where(v => !skip_user.Contains(v))
                    };

                var read = query.FirstOrDefault();

                if (read.IsNull()) return;

                var remove_user = read.remove_user.ToArray();
                var add_user = read.add_user.ToArray();

                if (!remove_user.Any() && !add_user.Any()) return;

                foreach (var user in remove_user)
                    read.task.Users.Remove(user);

                foreach (var user in add_user)
                    read.task.Users.Add(user);

                db.Commit();
            });
        }

        /// <summary>
        /// Закрыть событие
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetEventClose(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.Get<bool>("value");

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var task = db.Events.Find(id);
                task.Closed = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Показать список интересов
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socket"></param>
        /// <param name="db"></param>
        private static void ShowInterests(Guid? id, WebSocket.RequestRecord socket, DBFX db)
        {
            var dirs = (
                    from dir in db.HelpActivities
                    select new
                    {
                        dir.Key,
                        dir.Caption,
                        Type = 0,
                        Description = string.Empty,
                        dir.Order
                    })
                .ToArray();

            var list = (
                    from interest in db.OrgInterest
                    where interest.OrgID == id
                    orderby interest.Activity.ActivityValue.Order
                    select new
                    {
                        interest.Activity.ActivityValue.Key,
                        interest.Activity.ActivityValue.Caption,
                        Type = (int) interest.Type,
                        interest.Description,
                        interest.Activity.ActivityValue.Order
                    })
                .ToArray();

            var has = list.Select(v => v.Key);
            var news = dirs.Select(v => v.Key).Except(has);
            var adds = dirs.Where(v => news.Contains(v.Key));

            list.Concat(adds)
                .OrderBy(v => v.Order)
                .ForEach(read =>
                {
                    socket.Send(new
                    {
                        method = "add_interest",
                        data = read
                    });
                });

            ShowCurrency(id, socket, db);
        }

        /// <summary>
        /// Показать список валют
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socket"></param>
        /// <param name="db"></param>
        private static void ShowCurrency(Guid? id, WebSocket.RequestRecord socket, DBFX db)
        {
            (
                    from org in db.Organizations
                    where org.ID == id
                    from currency in db.HelpCurrency
                    where currency.Order > 0
                    orderby currency.Order
                    select new
                    {
                        cur = currency,
                        val = org
                            .Currency
                            .DefaultIfEmpty()
                            .FirstOrDefault(v => v.CurrencyValue.Key == currency.Key)
                    }
                )
                .AsNoTracking()
                .ToArray()
                .Select(temp => new
                {
                    year = temp.val?.Year ?? 0,
                    single = temp.val?.Single ?? 0,
                    status = (int) (temp.val?.Type ?? InterestType.unknown),
                    caption = temp.cur.Name,
                    key = temp.cur.Key
                })
                .ForEach(read =>
                {
                    socket.Send(new
                    {
                        method = "show_currency",
                        data = read
                    });
                });
        }

        /// <summary>
        /// Установить значение интереса
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetInterestType(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = (InterestType) data.Get<string>("value").ToInt();
            var key = data.GetString("key");

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var record = (
                        from read in db.OrgInterest
                        where read.OrgID == id
                              && read.Activity.ActivityValue.Key == key
                        select read
                    )
                    .FirstOrDefault();

                if (record == null)
                {
                    var read = (
                            from org in db.Organizations
                            where org.ID == id
                            from adr in org.Address
                            where adr.Type == AddressType.region
                            from act in adr.Activity
                            where act.ActivityValue.Key == key
                            select act.ID
                        )
                        .FirstOrDefault();

                    if (read.IsEmpty()) return;

                    record = db.Add(new OrgInterest
                    {
                        ActivityID = read,
                        Description = string.Empty,
                        OrgID = id
                    });
                }

                record.Type = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Комментировать интерес
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetInterestComment(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.Get<string>("value");
            var key = data.GetString("key");

            if (id.IsNull() || id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var record = (
                        from read in db.OrgInterest
                        where read.OrgID == id
                              && read.Activity.ActivityValue.Key == key
                        select read
                    )
                    .FirstOrDefault();

                if (record == null)
                {
                    var read = (
                            from org in db.Organizations
                            where org.ID == id
                            from adr in org.Address
                            where adr.Type == AddressType.region
                            from act in adr.Activity
                            where act.ActivityValue.Key == key
                            select act.ID
                        )
                        .FirstOrDefault();

                    if (read.IsEmpty()) return;

                    record = db.Add(new OrgInterest
                    {
                        ActivityID = read,
                        Description = string.Empty,
                        OrgID = id
                    });
                }

                record.Description = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Отобразить список должностей
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socket"></param>
        /// <param name="db"></param>
        private static void ShowPosts(Guid? id, WebSocket.RequestRecord socket, DBFX db)
        {
            var query =
                from record in db.ContactRecords
                where record.OrganizationID == id
                from post in record.Posts
                select new
                {
                    record = record.ID,
                    method = "add_post",
                    data = new
                    {
                        id = post.ID,
                        value = post.Caption,
                        verify = post.WorkID != null
                    }
                };

            query.ToArray().ForEach(v => socket.Send(v));
        }

        /// <summary>
        /// Отобразить списко контактных лиц
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socket"></param>
        /// <param name="db"></param>
        private static void ShowPersons(Guid? id, WebSocket.RequestRecord socket, DBFX db)
        {
            var query =
                from record in db.ContactRecords
                where record.OrganizationID == id
                from person in record.Persons
                select new
                {
                    record = record.ID,
                    method = "add_person",
                    data = new
                    {
                        id = person.ID,
                        value = person.FullName,
                        verify = person.WorkID != null
                    }
                };

            query.ToArray().ForEach(v => socket.Send(v));
        }

        /// <summary>
        /// Отобразить список контактов
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socket"></param>
        /// <param name="db"></param>
        private static void ShowContacts(Guid? id, WebSocket.RequestRecord socket, DBFX db)
        {
            var query =
                from record in db.ContactRecords
                where record.OrganizationID == id
                let consultant = (from user in db.Logins
                    where user.ID == db.AuthorID
                    from role in user.Roles
                    where role.Key == dbroles.consultant
                    select role)
                .Any()
                from contact in record.Contacts
                select new
                {
                    record = record.ID,
                    method = "add_contact",
                    data = new
                    {
                        id = contact.ID,
                        value = (contact.Type == ContactType.phone && !consultant ? "98" : "") +
                                (contact.ParseValue ?? contact.RawValue),
                        type = contact.Type
                    }
                };

            query.ToArray().ForEach(v => socket.Send(v));
        }

        /// <summary>
        /// Отобразить события
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socket"></param>
        /// <param name="db"></param>
        /// <param name="rec"></param>
        private static void ShowEvents(Guid? id, WebSocket.RequestRecord socket, DBFX db, Guid? rec = null)
        {
            var tz = db.Logins.Find(db.AuthorID)?.TimeZone ?? 0;
            var ctz = db.Organizations
                .Where(v => v.ID == id)
                .SelectMany(v => v.Address)
                .OrderByDescending(v => v.Type)
                .Select(v => v.TimeZone)
                .FirstOrDefault(v => v != 0);

            var types = new[]
            {
                EventType.call,
                EventType.comment,
                EventType.unknown,
                EventType.callUp,
                EventType.callSeminar,
                EventType.callTransfer,

                EventType.meeting,
                EventType.meetingEvent,
                EventType.meetingConfirm,
                EventType.meetingCancel,
                EventType.meetingRate,
                EventType.meetingClose,

                EventType.rateConsultant,

                EventType.seminarPromised,
                EventType.seminarCanceled,
                EventType.seminarSkiped,
                EventType.seminarVisited,

                EventType.sendMail
            };

            var query = (
                from record in db.ContactRecords
                where record.OrganizationID == id || record.ID == rec
                from task in record.Events
                where types.Contains(task.Type)
                select new
                {
                    record = record.ID,
                    method = "add_event",
                    data = new
                    {
                        id = task.ID,
                        author = (
                            from user in db.Logins
                            where user.ID == task.UpdateAuthorID
                            select user.Caption
                        ).FirstOrDefault(),
                        author_result = (
                            from user in db.Logins
                            where user.ID == task.MeetingResult.CreateAuthorID
                            select user.Caption
                        ).FirstOrDefault(),
                        start = task.Start,
                        modifi = task.DateModify,
                        notify = task.Notify,
                        type = task.Type,
                        flag = task.Closed,
                        comment = task.Description,
                        rating = task.Prev != null
                            ? task.Prev.MeetingResult != null
                                ? task.Prev.MeetingResult.Rate
                                : null
                            : null,
                        result = task.MeetingResult,
                        accaunts = task.Users.Select(v => v.ID)
                    }
                })
                .ToArray();

            var phone_list = (
                    from record in db.ContactRecords
                    where record.OrganizationID == id || record.ID == rec
                    from contact in record.Contacts
                    where contact.Type == ContactType.phone
                    select new
                    {
                        contact.ParseValue,
                        record.ID
                    }
                )
                .ToArray();

            //var phones = phone_list
            //    .SelectMany(v =>
            //    {
            //        var result = new List<string>();
            //        result.Add(v.ParseValue);
            //        return result;
            //    })
            //    .ToArray();

            var phones = phone_list.Select(v => v.ParseValue).ToArray();

            var query_phone = new TDBFX()
                .TELEFON
                .Where(v => phones.Any(p=>v.lastdata.Contains(p)))
                .AsEnumerable()
                .Select(read => new
                {
                    record = phone_list.Where(v => read.lastdata.Contains(v.ParseValue)).Select(v => v.ID).FirstOrDefault(),
                    method = "add_event",
                    data = new
                    {
                        id = Guid.NewGuid(),
                        author = read.src.ToString(),
                        author_result =
                        $@"\\asterisk\records\{read.src}\{read.calldate:yyyy-MM-dd}\{read.src}_{read.dst}_0_{
                                read.calldate
                            :yyyy-MM-dd_HH-mm-ss}.mp3",
                        start = (DateTimeOffset?) read.calldate.ToTZDateTimeOffset(5),
                        modifi = read.calldate.ToTZDateTimeOffset(5),
                        notify = (DateTimeOffset?) DateTimeOffset.Now,
                        type = EventType.call,
                        flag = read.disposition == "ANSWERED",
                        comment = read.dst.RemoveStart("98"),
                        rating = (ConsultantRate) null,
                        result = (MeetingResult) null,
                        accaunts = (IEnumerable<Guid>) null
                    }
                })
                .ToArray();


            query
                .ToArray()
                .Concat(query_phone)
                .OrderBy(v => v.data.start)
                .Select(v => new
                {
                    v.record,
                    v.method,
                    data = new
                    {
                        v.data.id,
                        v.data.author,
                        v.data.author_result,
                        comment = v.data.rating?.Description.HtmlDecode().FromHtml() ??
                                  v.data.comment.HtmlDecode().FromHtml(),
                        title = v.data.type.AtValue(),
                        date_edit = v.data.modifi.ToTZString(tz),
                        start = v.data.start.ToTZString(ctz),
                        msk = tz.ToMSK(),
                        date = (v.data.notify ?? v.data.start).ToTZDateTime(tz).ToString("dd.MM.yyyy"),
                        time = (v.data.notify ?? v.data.start).ToTZDateTime(tz).ToString("HH:mm"),
                        notify = v.data.notify.ToTZDateTime(tz).ToString("yyyy-MM-dd HH:mm"),
                        v.data.flag,
                        v.data.type,
                        answerOK = v.data.rating?.OkAnswer ?? false
                            ? "checked=\"checked\""
                            : "",
                        rating = v.data.rating?.Rating ?? 0,
                        result = v.data.result == null
                            ? null
                            : new
                            {
                                Description = v.data.result.Description.HtmlDecode().FromHtml(),
                                v.data.result.ID,
                                Title =
                                $@"{
                                        v.data.result.DateCreate
                                    :yyyy-MM-dd} встреча оценена в {v.data.result.Bonus:F} руб. ({
                                        v.data.author_result
                                    })"
                            },
                        v.data.accaunts
                    }
                })
                .ForEach(v => { socket.Send(v); });
        }

        /// <summary>
        /// Отобразить список файлов
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socket"></param>
        /// <param name="db"></param>
        private static void ShowFiles(Guid? id, WebSocket.RequestRecord socket, DBFX db)
        {
            var query =
                from record in db.ContactRecords
                where record.OrganizationID == id
                from task in record.Events
                from file in task.PathFiles
                where file.Type == TypeFile.unknown
                //|| (file.Type == TypeFile.PhoneRecord && file.Size > 0)
                select new
                {
                    record = record.ID,
                    task = task.ID,
                    method = "add_file",
                    data = new
                    {
                        id = file.ID,
                        type = file.Type,
                        number = file.Path
                    }
                };

            var tz = (from user in db.Logins
                    where user.ID == db.AuthorID
                    select user.TimeZone)
                .FirstOrDefault();

            query.AsEnumerable()
                .Select(v => new
                {
                    v.record,
                    v.task,
                    v.method,
                    data = new
                    {
                        v.data.id,
                        v.data.type,
                        number = v.data.number
                            .DoRegexMatches(@"^\\\\\w+\\\w+\\\d+\\[\d-]+\\(\d{4})_98(\d{10})_0_([\d-_]+)\.mp3$")
                            .FuncSelect(list =>
                            {
                                var source = list[0]
                                    .Groups[1]
                                    .Value;
                                var target = list[0]
                                    .Groups[2]
                                    .Value
                                    .ToLong()
                                    .ToString("+7(###) ###-##-##");
                                var date = list[0]
                                    .Groups[3]
                                    .Value
                                    .ToDateTime("yyyy-MM-dd_HH-mm-ss");
                                return $"{date.ToTZDateTimeOffset(5).ToTZString(tz)}: ({source}) {target}";
                            })
                    }
                })
                .ForEach(v => { socket.Send(v); });

            var query2 =
                from record in db.ContactRecords
                where record.OrganizationID == id
                from task in record.Events
                from file in task.Files
                where file.Type == TypeFile.EMail
                select new
                {
                    record = record.ID,
                    task = task.ID,
                    method = "add_file",
                    data = new
                    {
                        id = file.ID,
                        type = file.Type
                    }
                };

            query2.ToArray().ForEach(v => socket.Send(v));
        }

        /// <summary>
        /// Отобразить карточки контактов
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socket"></param>
        /// <param name="db"></param>
        private static void ShowRecord(Guid? id, WebSocket.RequestRecord socket, DBFX db)
        {
            var query =
                from record in db.ContactRecords
                where record.OrganizationID == id
                select new
                {
                    method = "add_record",
                    data = new
                    {
                        id = record.ID
                    }
                };

            query.ToArray().ForEach(v => socket.Send(v));
        }

        /// <summary>
        /// Отобразить блок ОКВЭД
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socket"></param>
        /// <param name="db"></param>
        private static void ShowOkved(Guid? id, WebSocket.RequestRecord socket, DBFX db)
        {
            (
                    from org in db.Organizations
                    where org.ID == id
                    select new
                    {
                        method = "add_okved",
                        main = true,
                        data = new
                        {
                            code = org.MainOkved2.Code,
                            value = org.MainOkved2.Caption
                        }
                    }
                )
                .FirstOrDefault()
                .Action(read => { socket.Send(read); });

            (
                    from org in db.Organizations
                    where org.ID == id
                    from okved in org.ListOkved2
                    orderby okved.Code
                    select new
                    {
                        method = "add_okved",
                        data = new
                        {
                            code = okved.Code,
                            value = okved.Caption
                        }
                    })
                .ToArray()
                .ForEach(read => { socket.Send(read); });
        }

        /// <summary>
        /// Отобразить статус валюты
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetStatusCurrency(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = (InterestType) data.GetString("value").ToInt();
            var key = data.GetString("key");
            if (id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var read = db.Currencies
                    .FirstOrDefault(v => v.CurrencyKey == key && v.OrganizationID == id);

                if (read == null)
                    db.Add(new Currency
                    {
                        CurrencyKey = key,
                        Type = value,
                        OrganizationID = id
                    });
                else
                    read.Type = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Отобразить годовые обороты по валюте
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetYearCurrency(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").ToDecimal();
            var key = data.GetString("key");
            if (id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var read = db.Currencies
                    .FirstOrDefault(v => v.CurrencyKey == key && v.OrganizationID == id);

                if (read == null)
                    db.Add(new Currency
                    {
                        CurrencyKey = key,
                        Year = value,
                        OrganizationID = id
                    });
                else
                    read.Year = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Отобразить разовые объемы по валюте
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetSingleCurrency(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            var value = data.GetString("value").ToDecimal();
            var key = data.GetString("key");
            if (id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var read = db.Currencies
                    .FirstOrDefault(v => v.CurrencyKey == key && v.OrganizationID == id);

                if (read == null)
                    db.Add(new Currency
                    {
                        CurrencyKey = key,
                        Single = value,
                        OrganizationID = id
                    });
                else
                    read.Single = value;
                db.Commit();
            });
        }

        /// <summary>
        /// Подтвердить встречу
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetMeetingConfirm(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            if (id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var query =
                    from task in db.Events
                    where task.ID == id
                    select new
                    {
                        task,
                        tasks =
                        from tasks in task.Organization.Events
                        where !tasks.Closed
                        where tasks.Type == EventType.progress
                        where !tasks.Users.Select(v => v.Roles.Select(r => r.Key).Contains(dbroles.consultant)).Any()
                        select tasks
                    };

                var read = query.FirstOrDefault();

                read.task.Type = EventType.meetingConfirm;

                foreach (var task in read.tasks)
                    task.Closed = true;

                //task.Users.Remove(user);

                db.Commit();

                gdb.confirm_meeting(db, read.task.ID);
            });
        }

        /// <summary>
        /// Отменить встречу
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="data"></param>
        private static void SetMeetingCancel(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            if (id.IsEmpty()) return;

            gdb.action(socket.user, db =>
            {
                var task = db.Events.Find(id);
                task.Type = EventType.meetingCancel;
                task.Closed = true;
                db.Commit();

                gdb.cancel_meeting(db, task.ID);
            });
        }

        /// <summary>
        /// Получить списко пользователей
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Editors(string id)
            => await this.func(gb =>
            {
                return gb
                    .db
                    .Logins
                    .OrderBy(v => v.Caption)
                    .Select(v => new GuidValue
                    {
                        Value = v.Caption,
                        Guid = v.ID
                    })
                    .AsEnumerable()
                    .ToJsonResult();
            });

        private static void FromAsterisk(WebSocket.RequestRecord socket, fxcDictSO data)
        {
            var id = data.GetString("id").ToGuid();
            if (id.IsEmpty()) return;


            gdb.action(socket.user, db =>
            {
                var task = db.Events.Find(id);
                task.Type = EventType.meetingCancel;
                task.Closed = true;
                db.Commit();
            });
        }
    }
}