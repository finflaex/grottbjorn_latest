﻿#region

using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Operator.Models;

#endregion

namespace WebGBFXT.Areas.Organization.Controllers
{
    /// <summary>
    /// Пересечения
    /// </summary>
    public class FilterController : Controller
    {
        /// <summary>
        /// Получить представление
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Получить списко организаций
        /// </summary>
        /// <param name="request"></param>
        /// <param name="OrgFilter"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            string OrgFilter)
            => await this.json(gb =>
            {
                if (OrgFilter.IsNullOrWhiteSpace())
                    return new PlanOrgTableItem[0];

                return (
                        from org in gb.db.Organizations
                        where org.Inn.Contains(OrgFilter)
                              || org.ShortName.Contains(OrgFilter)
                              || org.FullName.Contains(OrgFilter)
                        select new
                        {
                            Name = org.ShortName ?? org.FullName,
                            Accaunts = (from task in org.Events
                                where !task.Closed
                                where task.Start < gb.Now
                                where task.End > gb.Now
                                where task.Type == EventType.progress
                                from user in task.Users
                                select user.Caption)
                            .Distinct(),
                            Status = org.CloseInfo == null ? string.Empty : "Ликвидирована",
                            Address = org.FormatAddress ?? org.RawAddress,
                            INN = org.Inn,
                            org.ID
                        }
                    )
                    .AsEnumerable()
                    .Select(v => new PlanOrgTableItem
                    {
                        ID = v.ID,
                        Name = v.Name,
                        Accaunt = v.Accaunts.Join(", "),
                        Address = v.Address,
                        Inn = v.INN,
                        Status = v.Status
                    })
                    .ToDataSourceResult(request);
            });
    }
}