﻿#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.ApiOnline.UserFile;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;
using WebGBFXT.Controllers;

#endregion

namespace WebGBFXT.Areas.Organization.Controllers
{
    /// <summary>
    /// Импорт из файла
    /// </summary>
    [NoCache]
    [Roles]
    public class FileBaseController : Controller
    {
        /// <summary>
        /// Получить представление
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Загрузить файл
        /// </summary>
        /// <param name="files"></param>
        /// <param name="region"></param>
        /// <returns></returns>
        [HttpPost]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Upload(
            IEnumerable<HttpPostedFileBase> files,
            Guid region)
            => await this.act(gb =>
            {
                var addr = gb.db.Addresses.Find(region);

                files.ForEach(file =>
                {
                    switch (file.FileName.GetFileExtension())
                    {
                        case "csv":
                            file.InputStream
                                .ReadLines(Encoding.GetEncoding(1251))
                                .Select(v => v.Replace(';', '|'))
                                .ToArray()
                                .Action(v => Parse(gb.db, v, addr));
                            break;
                        case "xls":
                            file.InputStream
                                .XlsToDataSet()
                                .Tables
                                .Cast<DataTable>()
                                .ForEach(table =>
                                {
                                    table
                                        .Rows
                                        .Cast<DataRow>()
                                        .Select(v => v.ItemArray.Select(s => s.ToString()).Join("|"))
                                        .ToArray()
                                        .Action(v => Parse(gb.db, v, addr));
                                });
                            break;
                        default:
                            throw new Exception("неверный формат");
                    }
                });
            });

        /// <summary>
        /// Распарсить строки файла
        /// </summary>
        /// <param name="db"></param>
        /// <param name="lines"></param>
        /// <param name="addr"></param>
        private void Parse(DBFX db, string[] lines, Address addr)
        {
            var header = lines.FirstOrDefault();
            var body = lines
                .Skip(1)
                .Where(v => v[0] != '?')
                .ToArray();

            this.GetWS(db.AuthorID, socket =>
            {
                socket.Send(new
                {
                    method = "show_row_count",
                    data = new
                    {
                        count = body.Length
                    }
                });

                socket
                    .AddMethod("AddOrgForce", AddOrgForce(addr))
                    ;

                new UserFileHeader(header)
                    .Parse(body)
                    .Action(list =>
                    {
                        socket.Send(new
                        {
                            method = "show_org_count",
                            data = new
                            {
                                count = list.Length
                            }
                        });
                    })
                    .ForEach((index, read) =>
                    {
                        if (read.inn.IsNullOrWhiteSpace()
                            && read.short_name.IsNullOrWhiteSpace()
                            && read.name.IsNullOrWhiteSpace()
                            && !read.person.Any())
                            return;

                        var flag_inn = read.inn.NotNullOrWhiteSpace();
                        var flag_short_name = read.short_name.NotNullOrWhiteSpace();
                        var flag_name = read.name.NotNullOrWhiteSpace();

                        var list = (
                                from org in db.Organizations
                                where flag_inn && read.inn == org.Inn
                                      || flag_short_name && org.ShortName == read.short_name
                                      || flag_name && org.FullName == read.name
                                      || flag_name && org.ShortName == read.name
                                      || flag_short_name && org.FullName == read.short_name
                                select new
                                {
                                    org.ShortName,
                                    org.FullName,
                                    org.Inn,
                                    org.RawAddress,
                                    acc = (
                                        from task in org.Events
                                        where !task.Closed
                                        where task.Type == EventType.progress
                                        where task.Start < db.Now
                                        where task.End > db.Now
                                        select task.Users
                                        into users
                                        from user in users
                                        select user.Caption
                                    )
                                    .FirstOrDefault()
                                }
                            )
                            .ToArray();

                        if (list.Any())
                            socket.Send(new
                            {
                                method = "dubl_org",
                                data = new
                                {
                                    id = Guid.NewGuid(),
                                    index,
                                    list = list.Select(v => new
                                    {
                                        name = v.ShortName ?? v.FullName,
                                        inn = v.Inn,
                                        address = v.RawAddress,
                                        accaunt = v.acc
                                    })
                                },
                                save = read.ToJson()
                            });
                        else
                            AddOrganization(db, socket, read, addr, index);
                    });
            });
        }

        /// <summary>
        /// Добавить дубликат организации
        /// </summary>
        /// <param name="addr"></param>
        /// <returns></returns>
        private Action<WebSocket.RequestRecord, fxcDictSO> AddOrgForce(Address addr)
            => (socket, data) =>
            {
                var save = data.GetString("save").FromJson<UserFileOrgModel>();

                gdb.action(socket.user, db => { AddOrganization(db, socket, save, addr, -1); });
            };

        /// <summary>
        /// Проверить организацию на дубликаты
        /// </summary>
        /// <param name="db"></param>
        /// <param name="socket"></param>
        /// <param name="read"></param>
        /// <param name="addr"></param>
        /// <param name="index"></param>
        private void AddOrganization(
            DBFX db,
            WebSocket.RequestRecord socket,
            UserFileOrgModel read,
            Address addr,
            int index)
        {
            var chars = new[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
            var org = new BaseDBFX.Table.Organization
            {
                ClientCode = read.client_code,
                Inn = read.inn?.Where(v => chars.Contains(v)).ToArray().FuncSelect(v => new string(v)),
                FullName = read.name ?? read.short_name,
                ShortName = read.short_name ?? read.name,
                Description = read.comment,
                RawAddress = read.address
            };

            //org.ParseAddress(db, read.address);

            ParseOrgContacts(db, read, org);

            var @event = new Event
            {
                Description = "Добавлено в обработку из файла пользователя",
                Start = db.Now.Date,
                End = DateTimeOffset.MaxValue,
                Type = EventType.progress
            };
            @event.Users.Add(db.User);
            org.Events.Add(@event);

            db.Add(org);

            //db.Organizations.Add(org);
            db.Commit();

            addr = db.Addresses.Find(addr.ID);
            org.Address.Add(addr);

            db.Commit();

            //socket.Send(new
            //{
            //    method = "dubl_org",
            //    data = new
            //    {
            //        index = index,
            //        name = org.ShortName ?? org.FullName,
            //        inn = org.Inn,
            //        address = org.RawAddress,
            //    },
            //    save = read.ToJson()
            //});
        }

        /// <summary>
        /// Получить списко контактов
        /// </summary>
        /// <param name="db"></param>
        /// <param name="read"></param>
        /// <param name="org"></param>
        private static void ParseOrgContacts(
            DBFX db,
            UserFileOrgModel read,
            BaseDBFX.Table.Organization org)
        {
            org.Comments.Add(new Comment
            {
                Type = CommentType.history,
                Description = "Добавлено из файла пользователя"
            });

            if (read.comment != null)
                org.Comments.Add(new Comment
                {
                    Type = CommentType.comment,
                    Description = read.comment
                });

            read.person.ForEach(value =>
            {
                var record = new ContactRecord
                {
                    OrganizationID = org.ID
                };

                if (value.post != null)
                {
                    var string_post = db.Replace(value.post).ToLower();
                    var post = db.Posts.FirstOrDefault(v => v.Caption == string_post);
                    if (post == null)
                        post = new Post
                        {
                            Caption = string_post
                        };
                    record.Posts.Add(post);

                    record.Comments.Add(new Comment
                    {
                        Type = CommentType.history,
                        Description = $"({value.post}) - импорт из файла пользователя"
                    });

                    if (value.post_comment != null)
                        record.Comments.Add(new Comment
                        {
                            Type = CommentType.comment,
                            Description = value.post_comment
                        });
                }

                if (value.fio != null)
                {
                    record.Persons.Add(new Person
                    {
                        RawName = value.fio,
                        FullName = value
                            .fio
                            .Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                            .Select(v => v.Trim().FirstUp())
                            .Join(" ")
                    });

                    record.Comments.Add(new Comment
                    {
                        Type = CommentType.history,
                        Description = $"({value.fio}) - импорт из файла пользователя"
                    });

                    if (value.fio_comment != null)
                        record.Comments.Add(new Comment
                        {
                            Type = CommentType.comment,
                            Description = value.fio_comment
                        });
                }

                value.contact.ForEach(contact =>
                {
                    record.Contacts.Add(new Contact
                    {
                        Value = contact.value
                    });

                    record.Comments.Add(new Comment
                    {
                        Type = CommentType.history,
                        Description = $"({contact.value}) - импорт из файла пользователя"
                    });

                    if (contact.comment != null)
                        record.Comments.Add(new Comment
                        {
                            Type = CommentType.comment,
                            Description = contact.comment
                        });

                    if (contact.call != null)
                    {
                        var @event = new Event
                        {
                            Organization = org,
                            Type = EventType.callUp,
                            Description = contact.call.comment,
                            Start = contact.call.date,
                            OrgID = org.ID,
                            RecordID = record.ID,
                            Closed = contact.call.date.Date < db.Now.Date
                        };
                        @event.Users.Add(db.User);

                        record.Comments.Add(new Comment
                        {
                            Type = CommentType.history,
                            Description =
                                $"Звонок ({contact.call.date:yyyy.MM.dd HH:mm}) - импорт из файла пользователя"
                        });

                        db.Add(@event);
                    }

                    if (contact.meeting != null)
                    {
                        var @event = new Event
                        {
                            Organization = org,
                            Type = EventType.meeting,
                            Description = contact.meeting.comment,
                            Start = contact.meeting.date,
                            OrgID = org.ID,
                            RecordID = record.ID,
                            Closed = contact.meeting.date.Date < db.Now.Date
                        };

                        @event.Users.Add(db.User);

                        record.Comments.Add(new Comment
                        {
                            Type = CommentType.history,
                            Description =
                                $"Встреча ({contact.meeting.date:yyyy.MM.dd HH:mm}) - импорт из файла пользователя"
                        });

                        db.Add(@event);
                    }
                });

                db.Add(record);
            });
        }

        /// <summary>
        /// Установить область
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> ReadRegion()
            => await this.json(gb =>
            {
                return gb.db
                    .Addresses
                    .Where(v => v.Type == AddressType.region)
                    .Select(v => new GuidValue
                    {
                        Guid = v.ID,
                        Value = v.Name + " " + v.Suffix + "."
                    })
                    .OrderBy(v => v.Value)
                    .AsEnumerable();
            });
    }
}