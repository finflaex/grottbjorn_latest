﻿#region

using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Organization.Controllers
{
    /// <summary>
    /// Форма отправки почтовых сообщений
    /// </summary>
    [NoCache]
    [Roles]
    public class MailController : Controller
    {
        /// <summary>
        /// Получить представление
        /// </summary>
        /// <param name="recid"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator, dbroles.consultant)]
        public async Task<ActionResult> Index(Guid? recid)
            => await this.func(gb =>
            {
                if (recid == null) return "error".ToActionResult();

                var model = new MailModel
                {
                    RecordID = recid.Value,
                    Body = Assembly
                        .GetAssembly(typeof(MailController))
                        .GetResource("WebGBFXT.Shablons.shablon.htm")
                        .ToString(Encoding.Unicode)
                };

                return View(model);
            });

        /// <summary>
        /// Отправить сообщение
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator, dbroles.consultant)]
        public async Task<ActionResult> SendMail(MailModel model)
            => await this.act(gb =>
            {
                var query =
                    from contact in gb.db.Contacts
                    where contact.ID == model.RecordID
                    select new
                    {
                        contact,
                        person = contact.Record.Persons.FirstOrDefault(),
                        organization = contact.Record.Organization,
                        record = contact.Record,
                        user = gb.db.Logins.FirstOrDefault(v => v.ID == gb.AuthorGuid)
                    };

                var read = query.FirstOrDefault();

                var task = new Event
                {
                    Organization = read.organization,
                    Start = gb.Now.DateTime,
                    End = gb.Now.DateTime,
                    Record = read.record,
                    Type = EventType.sendMail,
                    Users = read.user.InArray(),
                    Contact = read.contact,
                    Files = new RawFile
                        {
                            Raw = model.Body.ToBytes(Encoding.UTF8),
                            Type = TypeFile.EMail
                        }
                        .InArray()
                };

                read.contact.Events.Add(task);

                gb.db.Add(task);
                gb.Save();

                var mail = (
                        from user in gb.db.Logins
                        where user.ID == gb.AuthorGuid
                        select user.InternalEMailAddress
                    )
                    .FirstOrDefault()
                    .retIfNullOrWhiteSpace("operator@grottbjorn.com");

                gdb.send_mail(mail, read.person, read.contact, "Grottbjorn", model.Body.HtmlDecode());
            });

        /// <summary>
        /// Отобразить сообщение
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator, dbroles.consultant)]
        public async Task<ActionResult> Show(Guid? id)
            => await this.func(gb =>
            {
                var file = gb.db.Files.Find(id);

                return file
                    .Raw
                    .ToString(Encoding.UTF8)
                    .HtmlDecode()
                    .ToActionResult();
            });

        public class MailModel
        {
            public Guid RecordID { get; set; }
            public string Body { get; set; }
        }
    }
}