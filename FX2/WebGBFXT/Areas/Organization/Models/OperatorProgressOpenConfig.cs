﻿#region

using System;

#endregion

namespace WebGBFXT.Areas.Operator.Models
{
    [Serializable]
    public class OperatorProgressOpenConfig
    {
        public bool Open { get; set; }
    }

    [Serializable]
    public class ConsultantMeetingOpenConfig
    {
        public bool Open { get; set; }
    }
}