﻿namespace WebGBFXT.Areas.Organization.Models
{
    public class BaseSchedullerModel
    {
        public string RecordID { get; set; }
        public string OrganizationID { get; set; }
        public string DepartamentID { get; set; }
        public string InterestID { get; set; }
        public string Key { get; set; }
        public string OrgName { get; set; }
    }
}