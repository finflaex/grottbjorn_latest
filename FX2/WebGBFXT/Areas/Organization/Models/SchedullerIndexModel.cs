﻿#region

using System;
using BaseDBFX.Table;
using Kendo.Mvc.UI;

#endregion

namespace WebGBFXT.Areas.Organization.Models
{
    public class SchedullerIndexModel : ISchedulerEvent
    {
        public DateTimeOffset? _end;
        public DateTimeOffset? _notify;
        public DateTimeOffset? _start;

        public Guid ID { get; set; }
        public EventType Type { get; set; }
        public Guid? OrganizationID { get; set; }
        public Guid? RecordID { get; set; }
        public Guid? DepartamentID { get; set; }
        public bool NotEdit { get; set; }
        public string OrganizationName { get; set; }
        public bool Notify { get; set; }
        public DateTime? NotifyTime { get; set; }
        public bool NotifyConsultant { get; set; }
        public Guid? InterestID { get; set; }
        public string Region { get; set; }
        public string Accaunts { get; set; }

        #region standart

        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsAllDay { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string StartTimezone { get; set; }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }

        #endregion
    }
}