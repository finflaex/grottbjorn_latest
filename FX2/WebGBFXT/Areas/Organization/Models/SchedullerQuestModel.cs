﻿#region

using System;

#endregion

namespace WebGBFXT.Areas.Organization.Models
{
    public class SchedullerQuestModel
    {
        public Guid RecordID { get; set; }
        public Guid InterestID { get; set; }
        public Guid? OrganizationID { get; set; }
        public Guid? DepartamentID { get; set; }
        public string Key { get; set; }
        public string OrgName { get; set; }
        public Guid? EventID { get; set; }
        public int Type { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }

        public string Caption { get; set; }
        public string Comment { get; set; }
        public DateTimeOffset? _Start { get; set; }
        public DateTimeOffset? _End { get; set; }
    }
}