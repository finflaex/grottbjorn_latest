﻿#region

using System;
using System.Collections.Generic;
using Finflaex.Support.MVC.Models;

#endregion

namespace WebGBFXT.Areas.Organization.Models
{
    public class BaseOrgModel
    {
        public BaseOrgModel()
        {
            OrgTitle = "- не указано -";
            OrgShortName = string.Empty;
            OrgFullName = string.Empty;
            OrgInn = string.Empty;
            OrgAddress = string.Empty;
            OrgComment = string.Empty;
            OrgClose = false;
            OrgVerify = false;
        }

        public BaseOrgModel(Guid id)
            : this()
        {
            ID = id.ToString();
        }

        public bool OrgVerify { get; set; }

        public bool OrgClose { get; set; }

        public string OrgTitle { get; set; }
        public string OrgShortName { get; set; }
        public string OrgFullName { get; set; }
        public string OrgInn { get; set; }
        public string OrgAddress { get; set; }
        public string OrgAddressPhyzical { get; set; }
        public string OrgAddressLegal { get; set; }
        public string OrgComment { get; set; }
        public string ID { get; set; }
        public string OrgClientCode { get; set; }
        public IEnumerable<Guid> Editors { get; set; }

        public IEnumerable<GuidValue> LegalCity { get; set; }
        public IEnumerable<GuidValue> PhyzicCity { get; set; }
        public string LegalStreet { get; set; }
        public string PhyzicStreet { get; set; }
    }
}