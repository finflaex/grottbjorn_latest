﻿#region

using System;
using System.Collections.Generic;
using BaseDBFX.Table;
using Kendo.Mvc.UI;

#endregion

namespace WebGBFXT.Areas.Organization.Models
{
    public class SchedullerModel : ISchedulerEvent
    {
        public SchedullerModel()
        {
            Title = "";
            Description = "";
        }

        public Guid AccountID { get; set; }

        public string ID { get; set; }
        public string OrganizationID { get; set; }
        public string RecordID { get; set; }
        public string DepartamentID { get; set; }
        public string InterestID { get; set; }
        public bool Notify { get; set; }
        public string OrgName { get; set; }
        public bool NotEdit { get; set; }
        public bool Consultant { get; set; }
        public int ResID { get; set; }
        public IEnumerable<string> Accaunts { get; set; }
        public bool Confirm { get; set; }
        public DateTime? NotifyTime { get; set; }
        public string AccauntNames { get; set; }
        public DateTimeOffset? _Start { get; set; }
        public DateTimeOffset? _End { get; set; }
        public DateTimeOffset? _NotifyTime { get; set; }
        public string Region { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsAllDay { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public string StartTimezone { get; set; }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }
    }

    public class SchedullerTripModel : ISchedulerEvent
    {
        public SchedullerTripModel()
        {
            Title = "";
            Description = "";
        }

        public Guid AccountID { get; set; }

        public Guid ID { get; set; }
        public Guid OrganizationID { get; set; }
        public Guid RecordID { get; set; }
        public Guid EventID { get; set; }
        public string OrgName { get; set; }
        public bool NotEdit { get; set; }
        public int ResID { get; set; }
        public bool Confirm { get; set; }
        public DateTime? NotifyTime { get; set; }
        public string AccauntNames { get; set; }
        public EventType Type { get; set; }
        public DateTimeOffset? _Start { get; set; }
        public DateTimeOffset? _End { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsAllDay { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string StartTimezone { get; set; }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }
    }
}