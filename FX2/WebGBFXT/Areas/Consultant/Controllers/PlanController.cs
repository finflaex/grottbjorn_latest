﻿#region

using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.Attributes;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Organization.Models;
using Finflaex.Support.MVC.Models;

#endregion

namespace WebGBFXT.Areas.Consultant.Controllers
{
    /// <summary>
    /// План встреч
    /// </summary>
    [NoCache]
    public class PlanController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="recordID"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant)]
        public async Task<ActionResult> Index(Guid? recordID) 
            => await this.func(gb=>
        {
            gb.GuidValue = new MainValue
            {
                id = recordID ?? Guid.Empty
            };
            return View(gb);
        });

        /// <summary>
        /// Получение списка событий
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.consultant)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.func(gb =>
            {
                var db = gb.db;

                var cities = new[]
                {
                    Guid.Parse("8DEA00E3-9AAB-4D8E-887C-EF2AAA546456"), // новосибирск
                    Guid.Parse("7B6DE6A5-86D0-4735-B11A-499081111AF8"), // владивосток
                    Guid.Parse("7DFA745E-AA19-4688-B121-B655C11E482F"), // краснодар
                    Guid.Parse("0C5B2444-70A0-4932-980C-B4DC0D3F02B5"), // москва
                    Guid.Parse("555E7D61-D9A7-4BA6-9770-6CAA8198C483"), // нижний новгород
                    Guid.Parse("C2DEB16A-0330-4F05-821F-1D09C93331E6"), // санкт петербург
                    Guid.Parse("C1CFE4B9-F7C2-423C-ABFA-6ED1C05A15C5"), // ростов на дону
                    Guid.Parse("2763C110-CB8B-416A-9DAC-AD28A55B4402") // екатеринбург
                };

                var query =
                    from user in db.Logins
                    where user.ID == db.AuthorID
                    from task in user.Events
                    where task.Type == EventType.meeting
                          || task.Type == EventType.meetingConfirm || task.Type == EventType.meetingClose ||
                          task.Type == EventType.meetingEvent
                    //select new
                    //{
                    //    task,
                    //    task.Organization
                    //}
                    select new
                    {
                        main = task
                            .Organization
                            .Address
                            .Select(v => v.ID)
                            .Any(v => cities.Contains(v)),

                        task.Description,
                        task.ID,
                        task.Type,
                        task.Start,
                        task.End,
                        OrganizationID = task.OrgID,
                        task.RecordID,
                        OrganizationName = task.Organization.ShortName ?? task.Organization.FullName,
                        Notify = DbFunctions.TruncateTime(task.Notify) != DbFunctions.TruncateTime(task.Start),
                        NotifyTime = task.Notify ?? task.Start,
                        task.NotifyConsultant,
                        Address = from adr in task.Organization.Address
                        where adr.Type < AddressType.street
                        orderby adr.Type
                        select new
                        {
                            adr.Name,
                            adr.Suffix
                        },
                        Accaunts = (from e in task.Organization.Events
                            where e.Type == EventType.progress
                            where !e.Closed
                            where e.Start < gb.Now
                            where e.End > gb.Now
                            select e.Users)
                        .SelectMany(v => v)
                        .Distinct()
                        .Select(acc => new
                        {
                            acc.ID,
                            acc.Caption
                        })
                    };

                //return query
                //    .AsEnumerable()
                //    .Select(v => new SchedullerModel
                //    {
                //        ID = v.task.ID.ToString("N"),
                //        Description = v.task.Description.HtmlDecode().FromHtml(),
                //        Title = v.task.Type.AtValue(),
                //        RecordID = v.task.RecordID.Value.ToString("N"),
                //        ResID = (int)v.task.Type,
                //        _Start = v.task.Start,
                //        IsAllDay = v.task.IsAllDay,
                //        OrgName = v.Organization.ShortName
                //            .retIfNullOrWhiteSpace(v.Organization.FullName)
                //            .retIfNullOrWhiteSpace("не указано"),
                //        Notify = v.task.Notify?.Date != v.task.Start?.Date,
                //        OrganizationID = v.task.OrgID.Value.ToString("N"),
                //        Confirm = v.task.Start < db.Now && v.task.Type != EventType.meetingClose,
                //    })
                //    .Select(v =>
                //    {
                //        v.Start = v._Start.ToTZDateTime(gb.TimeZone);
                //        if (v.Start.Hour < 8)
                //        {
                //            v.Start =  v.Start.Date.AddHours(8);
                //            v.End = v.Start.AddHours(3);
                //        }
                //        else
                //        {
                //            v.End = v.Start.AddHours(3);
                //        }
                //        return v;
                //    })
                //    .ToDataSourceResult(request)
                //    .ToJsonResult();

                return query
                    .AsEnumerable()
                    .Select(v => new SchedullerModel
                    {
                        Description = v.Description.HtmlDecode().FromHtml(),
                        ID = v.ID.ToString(),
                        Title = v.Type.AtValue(),
                        //ResID = (int)v.Type,
                        ResID = v.main ? 100 + (int) v.Type : (int) v.Type,
                        Start = v.Start.ToTZDateTime(gb.TimeZone),
                        End = (v.End ?? v.Start?.AddHours(3)).ToTZDateTime(gb.TimeZone),
                        OrganizationID = v.OrganizationID.ToString(),
                        IsAllDay = false,
                        RecordID = v.RecordID.ToString(),
                        OrgName = v.OrganizationName,
                        Accaunts = v.Accaunts.Select(a => a.ID.ToString("N")).ToArray(),
                        AccauntNames = v.Accaunts.Select(a => a.Caption).Join(", "),
                        Confirm = v.Type == EventType.meetingConfirm || v.Type == EventType.meetingClose,
                        Region = v.Address.Select(a => $"{a.Name} {a.Suffix}").Join(", ")
                    })
                    .ToDataSourceResult(request)
                    .ToJsonResult();
            });

        /// <summary>
        /// Обновление события
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model">событие</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.@operator)]
        [ValidateInput(false)]
        public async Task<ActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            SchedullerIndexModel model)
            => await this.func(gb =>
            {
                if (model.End < model.Start)
                    model.End = model.Start.AddHours(3);

                gdb.set_meeting(
                    gb.db,
                    model.ID,
                    model.Start,
                    model.End,
                    model.NotifyTime,
                    model.Description.FromHtml(),
                    model.NotifyConsultant,
                    model.Notify);

                return model
                    .InArray()
                    .ToDataSourceResult(request)
                    .ToJsonResult();
            });

        /// <summary>
        /// Удаление события
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model">событие</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            SchedullerIndexModel model)
            => await this.json(gb =>
            {
                gb.db.Events.Find(model.ID).ActionIfNotNull(task =>
                {
                    gb.db.Delete(task);
                    gb.db.Commit();
                });

                return model
                    .InArray()
                    .ToDataSourceResult(request);
            });
    }
}