﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.Attributes;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebGBFXT.Areas.Organization.Models;

#endregion

namespace WebGBFXT.Areas.Consultant.Controllers
{
    /// <summary>
    /// Управление подразделением
    /// </summary>
    [NoCache]
    public class ManagerPlanController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="recordID"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant)]
        public async Task<ActionResult> Index(Guid? recordID)
            => await this.func(gb =>
            {
                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    from dep in user.Departaments
                    where dep.Parent.Key == Departament.ConsultantDepartament
                    from acc in dep.Users
                    select new
                    {
                        Guid = acc.ID,
                        Value = acc.Caption
                    };

                ViewData["accaunts"] = query
                    .Distinct()
                    .AsEnumerable()
                    .Select(v => new KeyValue
                    {
                        Value = v.Value,
                        Key = v.Guid.ToString("N")
                    })
                    .ToArray();

                return View(gb);
            });

        /// <summary>
        /// Получение списка событий
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.consultant)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.func(gb =>
            {
                var db = gb.db;

                var tz = db.Logins.Find(gb.AuthorGuid)?.TimeZone ?? 0;

                var cities = new[]
                {
                    Guid.Parse("8DEA00E3-9AAB-4D8E-887C-EF2AAA546456"), // новосибирск
                    Guid.Parse("7B6DE6A5-86D0-4735-B11A-499081111AF8"), // владивосток
                    Guid.Parse("7DFA745E-AA19-4688-B121-B655C11E482F"), // краснодар
                    Guid.Parse("0C5B2444-70A0-4932-980C-B4DC0D3F02B5"), // москва
                    Guid.Parse("555E7D61-D9A7-4BA6-9770-6CAA8198C483"), // нижний новгород
                    Guid.Parse("C2DEB16A-0330-4F05-821F-1D09C93331E6"), // санкт петербург
                    Guid.Parse("C1CFE4B9-F7C2-423C-ABFA-6ED1C05A15C5"), // ростов на дону
                    Guid.Parse("2763C110-CB8B-416A-9DAC-AD28A55B4402") // екатеринбург
                };

                var query =
                    from auth in db.Logins
                    where auth.ID == db.AuthorID
                    from dep in auth.Departaments
                    where dep.Parent.Key == Departament.ConsultantDepartament
                    //from user in dep.Users
                    from task in dep.Events
                    where task.Type == EventType.meeting
                          || task.Type == EventType.meetingConfirm
                          || task.Type == EventType.meetingClose
                          || task.Type == EventType.meetingCancel
                    select new
                    {
                        main = task
                            .Organization
                            .Address
                            .Select(v => v.ID)
                            .Any(v => cities.Contains(v)),

                        task.Description,
                        task.ID,
                        task.Type,
                        task.Start,
                        task.End,
                        OrganizationID = task.OrgID,
                        task.RecordID,
                        DepartamentID = dep.ID,
                        OrganizationName = task.Organization.ShortName ?? task.Organization.FullName,
                        Notify = DbFunctions.TruncateTime(task.Notify) != DbFunctions.TruncateTime(task.Start),
                        NotifyTime = task.Notify ?? task.Start,
                        task.NotifyConsultant,
                        Address = from adr in task.Organization.Address
                        where adr.Type < AddressType.street
                        orderby adr.Type
                        select new
                        {
                            adr.Name,
                            adr.Suffix
                        },
                        //Accaunts = (from e in task.Organization.Events
                        //    where e.Type == EventType.progress
                        //    where !e.Closed
                        //    where e.Start < gb.Now
                        //    where e.End > gb.Now
                        //    select e.Users)
                        //.SelectMany(v => v)
                        //.Distinct()
                        Accaunts = task.Users
                            .Where(v => v.Roles.Any(r => r.Key == dbroles.consultant))
                            .Select(acc => new
                            {
                                acc.ID,
                                acc.Caption
                            })
                    };

                return query
                    .AsEnumerable()
                    .Select(v => new SchedullerModel
                    {
                        Description = v.Description.HtmlDecode().FromHtml(),
                        ID = v.ID.ToString(),
                        Title = v.Type.AtValue(),
                        ResID = v.main ? 100 + (int) v.Type : (int) v.Type,
                        Start = v.Start.ToTZDateTime(tz),
                        End = (v.End ?? v.Start?.AddHours(3)).ToTZDateTime(tz),
                        OrganizationID = v.OrganizationID.ToString(),
                        IsAllDay = false,
                        RecordID = v.RecordID.ToString(),
                        DepartamentID = v.DepartamentID.ToString(),
                        OrgName = v.OrganizationName,
                        Accaunts = v.Accaunts.Distinct().Select(a => a.ID.ToString("N")).ToArray(),
                        AccauntNames = v.Accaunts.Select(a => a.Caption).Join(", "),
                        Confirm = v.Type == EventType.meetingClose,
                        Region = v.Address.Select(a => $"{a.Name} {a.Suffix}").Join(", ")
                    })
                    .ToDataSourceResult(request)
                    .ToJsonResult();
            });

        /// <summary>
        /// Получение списка сотрудников подразделения
        /// </summary>
        /// <returns></returns>
        //[Roles(dbroles.admin, dbroles.consultant)]
        //public async Task<ActionResult> ReadAccount()
        //    => await this.json(gb =>
        //    {
        //        var query =
        //            from user in gb.db.Logins
        //            where user.ID == gb.db.AuthorID
        //            from dep in user.Departaments
        //            where dep.Parent.Key == Departament.ConsultantDepartament
        //            from acc in dep.Users
        //            select new
        //            {
        //                Guid = acc.ID,
        //                Value = acc.Caption
        //            };

        //        return query
        //            .AsEnumerable()
        //            .Select(v => new KeyValue
        //            {
        //                Value = v.Value,
        //                Key = v.Guid.ToString("N")
        //            })
        //            .ToArray();
        //    });

        /// <summary>
        /// Обновление события
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Roles(dbroles.admin, dbroles.consultant)]
        [ValidateInput(false)]
        public async Task<ActionResult> Update()
            => await this.act(gb =>
            {
                var model = Request.ReadPostJson<UpdateModel>();

                var query = (
                        from meeting in gb.db.Events
                        where meeting.ID == model.ID
                        let has_users = (
                            from user in meeting.Users
                            where user.Roles.Any(v => v.Key == dbroles.consultant)
                            select user
                        )
                        let nid_users = (
                            from user in gb.db.Logins
                            where model.Accaunts.Contains(user.ID)
                            where user.Roles.Any(v => v.Key == dbroles.consultant)
                            select user
                        )
                        select new
                        {
                            meeting,
                            add_users = nid_users.Except(has_users),
                            del_users = has_users.Except(nid_users)
                        }
                    )
                    .FirstOrDefault();

                query.ThrowIfNull("null query meeting with accaunts");

                query.meeting.Description = model.Description.HtmlDecode() ?? string.Empty;

                foreach (var user in query.del_users)
                    query.meeting.Users.Remove(user);

                foreach (var user in query.add_users)
                    query.meeting.Users.Add(user);

                gb.Save();

                //
                // 29.08 передача организации после подтверждения
                // 
                //var read = Request.ReadPostJson<UpdateModel>();

                //var db = lazy.Value;
                //var task = db.Events.Find(read.ID);

                //task.Description = read.Description ?? String.Empty;
                ////task.End = model.End;
                //task.Users
                //    .Where(v=>v.Roles.Any(r=>r.Key == dbroles.consultant))
                //    .Where(v=>!read.Accaunts.Contains(v.ID))
                //    .ToArray()
                //    .ForEach(user =>
                //    {
                //        task.Users.Remove(user);
                //    });

                //db.Commit();

                //db.Logins
                //    .Where(v => read.Accaunts.Contains(v.ID))
                //    .ToArray()
                //    .Action(arr =>
                //    {
                //        if (arr.Any())
                //        {
                //            var old = task
                //                .Organization
                //                .Events
                //                .Where(v => v.Type == EventType.progress)
                //                .Where(v => !v.Closed)
                //                .FirstOrDefault(v => v.End > db.Now)
                //                .ActionIfNotNull(progress =>
                //                {
                //                    progress.End = db.Now.DateTime;
                //                    progress.Closed = true;
                //                });

                //            db.Add(new Event
                //            {
                //                Prev = old,
                //                OrgID = task.OrgID,
                //                Start = db.Now.DateTime,
                //                End = DateTime.MaxValue,
                //                Type = EventType.progress,
                //                Users = arr.FirstOrDefault().InArray(),
                //                Description = "Назначен менеджером во время назначения встречи",
                //            });
                //        }
                //    })
                //    .ForEach(user =>
                //    {
                //        task.Users.Add(user);
                //    });

                //db.Commit();

                gdb.set_meeting_progress(gb.db, model.ID);
            });

        private class UpdateModel
        {
            public IEnumerable<Guid> Accaunts { get; set; }
            public string Description { get; set; }
            public Guid ID { get; set; }
        }
        //    {
        //    => await this.async(lazy =>
        //    IEnumerable<Guid> accaunts)
        //public async Task<ActionResult> SelectAccaunt(

        //[Roles(dbroles.admin, dbroles.consultant)]
        //        var qwe = accaunts;
        //    });
    }
}