﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using Finflaex.Support.ApiOnline.PostApi;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Consultant.Controllers
{
    /// <summary>
    /// Форма закрытия встречи
    /// </summary>
    [NoCache]
    public class MeetingResultController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant)]
        public async Task<ActionResult> Index(Guid? id)
            => await this.func(gb =>
            {
                var query =
                    from e in gb.db.Events
                    where e.ID == id
                    select new MeetingModel
                    {
                        ID = e.MeetingResult.ID,
                        TaskID = e.ID,
                        LPR = e.MeetingResult != null && e.MeetingResult.LPR,
                        Bonus = e.MeetingResult == null ? 0 : e.MeetingResult.Bonus,
                        Comment = e.MeetingResult == null ? string.Empty : e.MeetingResult.Description,
                        Cost = e.MeetingResult == null ? 0 : e.MeetingResult.Cost,
                        Distance = e.MeetingResult == null ? 0 : e.MeetingResult.Distance
                    };

                var read = query.FirstOrDefault().retIfNull(new MeetingModel
                {
                    TaskID = id.Value
                });

                read.Comment = read.Comment.HtmlDecode();

                return View(read);
            });

        /// <summary>
        /// Расчет дистанции между двумя адресами
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant)]
        public ActionResult DistanceCalc(DistanceModel model)
        {
            var read = fxsGoogle.DistanceAddress(model.Address1, model.Address2);
            var result = new DistanceResult
            {
                start = read?.origin_addresses?.FirstOrDefault() ?? string.Empty,
                end = read?.destination_addresses?.FirstOrDefault() ?? string.Empty,
                distance = read?.rows?.FirstOrDefault()?.elements?.FirstOrDefault()?.distance?.text ?? string.Empty
            };
            return View(result);
        }

        /// <summary>
        /// Запись результата встречи
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.consultant)]
        public async Task<ActionResult> MeetingResult(MeetingModel model)
            => await this.func(data =>
            {
                gdb.close_meeting(data.db
                    , model.TaskID);

                gdb.add_meeting_result(data.db
                    , model.TaskID
                    , model.Comment
                    , model.Bonus
                    , model.Cost
                    , model.LPR
                    , model.Distance);

                //var db = lazy.Value;

                //var task = db.Events.Find(model.TaskID);

                //db.Delete(task.MeetingResult);

                //task.Type = EventType.meetingClose;

                //db.Add(new MeetingResult
                //{
                //    Description = model.Comment,
                //    Bonus = model.Bonus,
                //    Cost = model.Cost,
                //    LPR = model.LPR,
                //    Distance = model.Distance,
                //    Record = task.Record,
                //    Event = task,
                //});

                //task
                //    .Organization
                //    .Events
                //    .Where(v => v.Type == EventType.progress)
                //    .Where(v => v.Closed)
                //    .Where(v => v.Users.Any(u => u.Roles.Any(r => r.Key == dbroles.@operator) && u.Roles.All(r => r.Key != dbroles.consultant)))
                //    .OrderByDescending(v => v.End)
                //    .Take(1)
                //    .SelectMany(v => v.Users)
                //    .Where(v => v.Roles.Any(r => r.Key == dbroles.@operator))
                //    .FirstOrDefault(v => v.Roles.All(r => r.Key != dbroles.consultant))?
                //    .Events
                //    .Add(new Event
                //    {
                //        Organization = task.Organization,
                //        Start = db.Now.DateTime,
                //        Notify = db.Now.DateTime,
                //        Record = task.Record,
                //        Type = EventType.rateConsultant,
                //        Prev = task,
                //    });

                //db.Commit();

                return View();
            });

        public class IndexModel
        {
            public Guid taskid { get; set; }
        }

        public class DistanceModel
        {
            public string Address1 { get; set; }
            public string Address2 { get; set; }
        }

        public class DistanceResult
        {
            public string start { get; set; }
            public string end { get; set; }
            public string distance { get; set; }
        }

        public class MeetingModel
        {
            public Guid? ID { get; set; }
            public Guid TaskID { get; set; }
            public bool LPR { get; set; }
            public int Distance { get; set; }
            public decimal Cost { get; set; }
            public decimal Bonus { get; set; }
            public string Comment { get; set; }
        }
    }
}