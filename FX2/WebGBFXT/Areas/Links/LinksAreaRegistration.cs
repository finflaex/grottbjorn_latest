﻿#region

using System.Web.Mvc;

#endregion

namespace WebGBFXT.Areas.Links
{
    public class LinksAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Links"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Links_default",
                "Links/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}