﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Links.Controllers
{
    /// <summary>
    /// Модуль связей
    /// </summary>
    [NoCache]
    [Roles]
    public class OrgModulController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Index(Guid id)
        {
            return View(new ActionData(this)
            {
                GuidValue = new MainValue
                {
                    id = id
                }
            });
        }

        /// <summary>
        /// Добавить новую связь
        /// </summary>
        /// <param name="id">ид связи</param>
        /// <param name="type">тип связи</param>
        /// <param name="target">с кем связь</param>
        /// <param name="primary">от кого связь</param>
        /// <param name="comment">комментарий</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Add(
            Guid? id,
            TypeLink type,
            Guid? target,
            bool? primary,
            string comment)
            => await this.func(gb =>
            {
                if (id == null)
                {
                    Response.StatusCode = 409;
                    return Content("Выберите организацию для которой устанавливается связь");
                }
                if (target == null)
                {
                    Response.StatusCode = 409;
                    return Content("Выберите организацию с которой устанавливается связь");
                }

                switch (type)
                {
                    case TypeLink.up:

                        var query_up_sister =
                            from link in gb.db.Links
                            where link.Type == TypeLink.up
                            where link.FirstID == target
                            where link.SecondID != null
                            let list = (from sublink in link.Second.LinksChilds
                                    .Concat(link.Second.LinksParents)
                                where sublink.Type != TypeLink.other
                                select new[] {sublink.FirstID, sublink.SecondID})
                            .SelectMany(v => v)
                            where !list.Contains(id)
                            select link.SecondID;

                        query_up_sister
                            .AsEnumerable()
                            .ForEach(second =>
                            {
                                gb.db.Add(new Link
                                {
                                    FirstID = id.Value,
                                    SecondID = second,
                                    Type = TypeLink.link
                                });
                            });

                        var query_up_main =
                            from link in gb.db.Links
                            where link.Type == TypeLink.other
                            where link.FirstID == id && link.SecondID == target
                                  || link.FirstID == target && link.SecondID == id
                            select link;

                        query_up_main.Any()
                            .IfFalse(() =>
                            {
                                gb.db.Add(new Link
                                {
                                    FirstID = target.Value,
                                    SecondID = id.Value,
                                    Description = comment,
                                    Type = type
                                });
                            });

                        break;
                    case TypeLink.other:

                        gb.db.Add(new Link
                        {
                            FirstID = target.Value,
                            SecondID = id.Value,
                            Description = comment,
                            Type = type
                        });

                        break;
                    case TypeLink.down:

                        var query_dowm_sister =
                            from link in gb.db.Links
                            where link.Type == TypeLink.up
                            where link.FirstID == id
                            where link.SecondID != null
                            let list = (from sublink in link.Second.LinksChilds
                                    .Concat(link.Second.LinksParents)
                                where sublink.Type != TypeLink.other
                                select new[] {sublink.FirstID, sublink.SecondID})
                            .SelectMany(v => v)
                            where !list.Contains(target)
                            select link.SecondID;

                        query_dowm_sister
                            .AsEnumerable()
                            .ForEach(second =>
                            {
                                gb.db.Add(new Link
                                {
                                    FirstID = target.Value,
                                    SecondID = second,
                                    Type = TypeLink.link
                                });
                            });

                        var query_dowm_main =
                            from link in gb.db.Links
                            where link.Type != TypeLink.other
                            where link.FirstID == id && link.SecondID == target
                                  || link.FirstID == target && link.SecondID == id
                            select link;

                        query_dowm_main.Any()
                            .IfFalse(() =>
                            {
                                gb.db.Add(new Link
                                {
                                    FirstID = id.Value,
                                    SecondID = target.Value,
                                    Description = comment,
                                    Type = TypeLink.up
                                });
                            });

                        break;
                    default:
                        Response.StatusCode = 409;
                        return Content("Неверный тип связи");
                }
                gb.Save();

                return new HttpStatusCodeResult(200).AsType<ActionResult>();
            });

        /// <summary>
        /// Удалить связь
        /// </summary>
        /// <param name="id">ид связи</param>
        /// <returns></returns>
        public async Task<ActionResult> Delete(
            Guid? id)
            => await this.func(gb =>
            {
                if (id == null) return new HttpStatusCodeResult(409);

                var query =
                    from link in gb.db.Links
                    where link.ID == id
                    select new
                    {
                        link,
                        links =
                        from sublink in gb.db.Links
                        where link.Type == TypeLink.up
                        where sublink.Type == TypeLink.link
                        where sublink.FirstID == link.SecondID
                              || sublink.SecondID == link.SecondID
                        select sublink
                    };

                var read = query.First();

                gb.db.Delete(read.link);
                read.links.ToArray().ForEach(v => gb.db.Delete(v));
                gb.Save();

                return new HttpStatusCodeResult(200);
            });

        /// <summary>
        /// Обновить комментарий связи
        /// </summary>
        /// <param name="id">ид связи</param>
        /// <param name="comment">комментарий</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Comment(
            Guid? id, string comment)
            => await this.act(gb =>
            {
                gb.db.Links.Find(id).ActionIfNotNull(link =>
                {
                    link.Description = comment.HtmlDecode().FromHtml();
                    gb.Save();
                });
            });
    }
}