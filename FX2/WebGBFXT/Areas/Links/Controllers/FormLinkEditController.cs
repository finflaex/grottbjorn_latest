﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;

#endregion

namespace WebGBFXT.Areas.Links.Controllers
{
    /// <summary>
    /// Расширение модуля связей
    /// </summary>
    [NoCache]
    [Roles]
    public class FormLinkEditController : Controller
    {
        /// <summary>
        /// Поиск организации
        /// </summary>
        /// <param name="orgFilter"></param>
        /// <returns></returns>
        public async Task<ActionResult> SearchOrg(string orgFilter)
            => await this.json(gb =>
            {
                var query =
                    from org in gb.db.Organizations
                    where org.ShortName.Contains(orgFilter)
                          || org.FullName.Contains(orgFilter)
                          || org.Inn.Contains(orgFilter)
                          || org.Ogrn.Contains(orgFilter)
                    select new
                    {
                        org.ID,
                        Caption = org.ShortName ?? org.FullName,
                        Address = org.FormatAddress ?? org.RawAddress,
                        INN = org.Inn,
                        Accaunt = (
                            from task in org.Events
                            where !task.Closed
                            where task.Type == EventType.progress
                            where task.Start < gb.Now
                            where task.End > gb.Now
                            from user in task.Users
                            select user.Caption)
                        .FirstOrDefault()
                    };

                return query.Take(10).AsEnumerable();
            });

        /// <summary>
        /// Добавить вышестоящую организацию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> AddUpLink(
            Guid? id)
            => await this.func(gb =>
            {
                if (id == null) return null;

                gb.GuidValue = new MainValue
                {
                    id = id.Value
                };
                return View(gb);
            });

        /// <summary>
        /// Добавить сестринскую организацию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> AddLink(
            Guid? id)
            => await this.func(gb =>
            {
                if (id == null) return null;

                gb.GuidValue = new MainValue
                {
                    id = id.Value
                };
                return View(gb);
            });

        /// <summary>
        /// Добавить нижетоящую организацию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> AddDownLink(
            Guid? id)
            => await this.func(gb =>
            {
                if (id == null) return null;

                gb.GuidValue = new MainValue
                {
                    id = id.Value
                };
                return View(gb);
            });
        
        /// <summary>
        /// Добавить связь
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> AddOtherLink(
            Guid? id)
            => await this.func(gb =>
            {
                if (id == null) return null;

                gb.GuidValue = new MainValue
                {
                    id = id.Value
                };
                return View(gb);
            });
    }
}