﻿#region

using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using Finflaex.Support.MVC.Atributes;

#endregion

namespace WebGBFXT.Areas.Forms.Controllers
{
    [NoCache]
    public class TestController : Controller
    {
        [Roles]
        public async Task<ActionResult> Index() => await this.func(View);
    }
}