﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using DBFXFias;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

#endregion

namespace WebGBFXT.Areas.Forms.Controllers
{
    /// <summary>
    /// Форма работы с адресом
    /// </summary>
    [NoCache]
    [Roles]
    public class AddressController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(AddressResult data)
            => await this.func(gb => View(data ?? new AddressResult()));

        /// <summary>
        /// Получение адреса
        /// </summary>
        /// <param name="request"></param>
        /// <param name="data">список родительских элементов</param>
        /// <returns></returns>
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            IEnumerable<string> data)
            => await this.json(gb =>
            {
                using (var fdb = new FiasDB())
                {
                    //fdb.Database.CommandTimeout = 60000;

                    string parent = null;

                    if (data.Count() != 1 || data.FirstOrDefault() != "")
                        parent = (
                                from last in fdb.AddrObj
                                where data.Contains(last.AOGUID)
                                orderby last.AOLEVEL descending
                                select last.AOGUID)
                            .FirstOrDefault();

                    return (
                            from addr in fdb.AddrObj
                            where addr.PARENTGUID == parent
                            select addr
                        )
                        .Take(100)
                        .AsEnumerable()
                        .Select(v => new GuidValue
                        {
                            Guid = v.AOGUID.ToGuid(),
                            Value = $"{v.FORMALNAME} {v.SHORTNAME}."
                        })
                        .ToDataSourceResult(request);
                }
            });

        [HttpPost]
        public async Task<ActionResult> AddressRequest(string id, AddressResult data)
            => await this.json(gb =>
            {
                var qwe = data;
                var asd = id;
                return new object();
            });

        public class AddressResult
        {
            public AddressResult()
            {
                Guids = new List<Guid>();
                Other = string.Empty;
                RequestUrl = string.Empty;
            }

            public IEnumerable<Guid> Guids { get; set; }
            public string Other { get; set; }
            public string RequestUrl { get; set; }
        }
    }
}