﻿#region

using System.Web.Mvc;

#endregion

namespace WebGBFXT.Areas.Seminar
{
    public class SeminarAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Seminar"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Seminar_default",
                "Seminar/{controller}/{action}/{id}",
                new {action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}