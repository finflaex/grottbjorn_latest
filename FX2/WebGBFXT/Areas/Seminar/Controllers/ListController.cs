﻿#region

using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Models;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Seminar.Controllers
{
    /// <summary>
    /// Таблица семинаров
    /// </summary>
    [Roles(dbroles.admin, dbroles.@operator, dbroles.consultant)]
    public class ListController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index() => await this.func(gb => View(gb));

        /// <summary>
        /// Сохранение пользовательских настроек
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> SaveConfigDepartaments(
            SeminarListConfig model)
            => await this.func(gb =>
            {
                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    select new
                    {
                        user,
                        file = user.Files
                            .Where(file => file.Type == TypeFile.Config)
                            .FirstOrDefault(file => file.Name == Login.Configs.SeminarListConfig)
                    };

                var read = query.First();

                if (read.file == null)
                    read.user.Files.Add(new RawFile
                    {
                        Raw = model.AsBytes(),
                        Type = TypeFile.Config,
                        Name = Login.Configs.SeminarListConfig
                    });
                else
                    read.file.Raw = model.AsBytes();
                gb.Save();

                return View("Table", gb);
            });
    }
}