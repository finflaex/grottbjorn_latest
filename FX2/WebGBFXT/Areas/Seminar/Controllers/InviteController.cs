﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Models;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Seminar.Controllers
{
    /// <summary>
    /// Таблица могущих прийти на семинар
    /// </summary>
    public class InviteController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(Guid? id)
            => await this.func(gb =>
            {
                if (id == null)
                    return RedirectToAction("Index", "List", new
                        {
                            area = "Seminar"
                        })
                        .AsType<ActionResult>();
                gb.GuidValue = new MainValue
                {
                    id = id.Value
                };
                return View(gb);
            });

        /// <summary>
        /// Сохранение пользовательских настроек
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> SaveConfigLabels(
            Guid? id,
            SeminarInviteConfig model)
            => await this.func(gb =>
            {
                var query =
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    select new
                    {
                        user,
                        file = user.Files
                            .Where(file => file.Type == TypeFile.Config)
                            .FirstOrDefault(file => file.Name == Login.Configs.SeminarInviteConfig)
                    };

                var read = query.First();

                if (read.file == null)
                    read.user.Files.Add(new RawFile
                    {
                        Raw = model.AsBytes(),
                        Type = TypeFile.Config,
                        Name = Login.Configs.SeminarInviteConfig
                    });
                else
                    read.file.Raw = model.AsBytes();
                gb.Save();

                if (id == null)
                    return RedirectToAction("Index", "List", new
                        {
                            area = "Seminar"
                        })
                        .AsType<ActionResult>();
                gb.GuidValue = new MainValue
                {
                    id = id.Value
                };
                return View("Table", gb);
            });

        /// <summary>
        /// Приглашение
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> WillCome(
            WillComeModel model)
            => await this.json(gb =>
            {
                var read = (
                        from seminar in gb.db.Events
                        where seminar.ID == model.seminar
                        from org in gb.db.Organizations
                        where org.ID == model.organization
                        from e in org.Events
                        where e.Type == EventType.progress
                        where !e.Closed
                        where e.Start < gb.Now
                        where e.End > gb.Now
                        select new
                        {
                            e.Users,
                            seminar
                        }
                    )
                    .FirstOrDefault();

                var start = model
                    .datetime
                    .ToDateTime("dd.MM.yyyy HH:mm", DateTime.Now)
                    .ToTZDateTimeOffset(gb.TimeZone);

                gb.db.Events.Add(new Event
                {
                    OrgID = model.organization,
                    RecordID = model.record,
                    Prev = read.seminar,
                    Users = read.Users,
                    Start = start,
                    Description = model.comment,
                    Type = model.flag == 1
                        ? EventType.seminarPromised
                        : model.flag == 2
                            ? EventType.seminarCanceled
                            : EventType.callSeminar
                });

                gb.Save();

                return new {OK = true};

                //return new
                //{
                //    message = "<a class='btn btn-danger fa fa-close' onclick='popUpClose()'></a> данные не обработаны"
                //};
            });

        public class WillComeModel
        {
            public Guid seminar { get; set; }
            public Guid organization { get; set; }
            public Guid record { get; set; }
            public string comment { get; set; }
            public int flag { get; set; }
            public string datetime { get; set; }
        }
    }
}