﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Seminar.Controllers
{
    /// <summary>
    /// Таблица бывших на семинаре
    /// </summary>
    public class VisitedController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(Guid? id)
            => await this.func(gb =>
            {
                if (id == null)
                    return RedirectToAction("Index", "List", new
                        {
                            area = "Seminar"
                        })
                        .AsType<ActionResult>();
                gb.GuidValue = new MainValue
                {
                    id = id.Value
                };
                return View(gb);
            });

        /// <summary>
        /// Оказалось не были
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> ActionInvited(
            InvitedModel model)
            => await this.json(gb =>
            {
                var query =
                    from e in gb.db.Events
                    where e.ID == model.invited
                    select e;
                var invited = query.FirstOrDefault();
                if (invited == null)
                    return new
                    {
                        message = "<a class='btn btn-danger fa fa-close' onclick='popUpClose()'></a>"
                            .Append("Приглашение не найдено")
                    };
                switch (model.flag)
                {
                    case 1:
                        invited.Type = EventType.seminarSkiped;
                        invited.Description = model.comment;
                        break;
                }

                gb.Save();
                return new {OK = true};
            });

        public class InvitedModel
        {
            public Guid seminar { get; set; }
            public Guid org { get; set; }
            public Guid rec { get; set; }
            public Guid invited { get; set; }
            public int flag { get; set; }
            public string comment { get; set; }
            public decimal bonus { get; set; }
        }
    }
}