﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Seminar.Controllers
{
    /// <summary>
    /// Таблица отказавшихсф прийти на семинар
    /// </summary>
    public class CanceledController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(Guid? id)
            => await this.func(gb =>
            {
                if (id == null)
                    return RedirectToAction("Index", "List", new
                        {
                            area = "Seminar"
                        })
                        .AsType<ActionResult>();
                gb.GuidValue = new MainValue
                {
                    id = id.Value
                };
                return View(gb);
            });

        /// <summary>
        /// Передумал
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ActionResult> ActionInvited(
            IvitedModel model)
            => await this.json(gb =>
            {
                var query =
                    from e in gb.db.Events
                    where e.ID == model.invited
                    select e;
                var invited = query.FirstOrDefault();
                if (invited == null)
                    return new
                    {
                        message = "<a class='btn btn-danger fa fa-close' onclick='popUpClose()'></a>"
                            .Append("Приглашение не найдено")
                    };
                switch (model.flag)
                {
                    case 1:
                        invited.Type = EventType.seminarPromised;
                        invited.Description = model.comment;
                        break;
                    case 2:
                        gb.db.Events.Remove(invited);
                        break;
                }

                gb.Save();
                return new {OK = true};
            });

        public class IvitedModel
        {
            public Guid seminar { get; set; }
            public Guid org { get; set; }
            public Guid rec { get; set; }
            public Guid invited { get; set; }
            public int flag { get; set; }
            public string comment { get; set; }
        }
    }
}