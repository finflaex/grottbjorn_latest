﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;

#endregion

namespace WebGBFXT.Areas.Seminar.Controllers
{
    /// <summary>
    /// Таблица приглашенных на семинар
    /// </summary>
    public class InvitedController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(Guid? id)
            => await this.func(gb =>
            {
                if (id == null)
                    return RedirectToAction("Index", "List", new
                        {
                            area = "Seminar"
                        })
                        .AsType<ActionResult>();
                gb.GuidValue = new MainValue
                {
                    id = id.Value
                };
                return View(gb);
            });

        /// <summary>
        /// Приглашение
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> ActionInvited(
            InvitedModel model)
            => await this.json(gb =>
            {
                var query =
                    from e in gb.db.Events
                    where e.ID == model.invited
                    select e;
                var invited = query.FirstOrDefault();
                if (invited == null)
                    return new
                    {
                        message = "<a class='btn btn-danger fa fa-close' onclick='popUpClose()'></a>"
                            .Append("Приглашение не найдено")
                    };
                switch (model.flag)
                {
                    case 1:
                        invited.Type = EventType.seminarCanceled;
                        invited.Description = model.comment;
                        break;
                    case 2:
                        gb.db.Events.Remove(invited);
                        break;
                    case 3:
                        invited.Type = EventType.seminarSkiped;
                        invited.Description = model.comment;
                        break;
                    case 4:
                        invited.Type = EventType.seminarVisited;

                        var read = (
                                from invite in gb.db.Events
                                where invite.ID == model.invited
                                let progress_list = (
                                    from progress in invite.Organization.Events
                                    where !progress.Closed
                                    where progress.Start < gb.Now
                                    where progress.End > gb.Now
                                    where progress.Type == EventType.progress
                                    select progress
                                )
                                from user in progress_list.SelectMany(v => v.Users)
                                let is_operator = (
                                    from dep in user.Departaments
                                    where dep.Key == Departament.OperatorDepartament
                                    select dep
                                )
                                .Any()
                                select new
                                {
                                    invite.OrgID,
                                    progress_list,
                                    @operator = user,
                                    is_operator,
                                    author = from author in gb.db.Logins
                                    where author.ID == gb.AuthorGuid
                                    select author
                                }
                            )
                            .FirstOrDefault();

                        if (read != null && read.is_operator)
                        {
                            invited.MeetingResult = new MeetingResult
                            {
                                Bonus = model.bonus,
                                Description = model.comment,
                                Consultants = (
                                        from invite in gb.db.Events
                                        where invite.ID == model.seminar
                                        from user in invite.Users
                                        from dep in user.Departaments
                                        where dep.Parent.Key == Departament.ConsultantDepartament
                                        select user
                                    )
                                    .ToArray()
                            };
                            foreach (var progress in read.progress_list)
                            {
                                progress.Closed = true;
                                progress.End = gb.Now;
                            }
                            gb.db.Events.Add(new Event
                            {
                                Type = EventType.progress,
                                Users = read.author.ToArray(),
                                OrgID = read.OrgID,
                                Description = "Участник семинара",
                                Start = gb.Now
                            });
                        }
                        break;
                }

                gb.Save();
                return new {OK = true};
            });

        public class InvitedModel
        {
            public Guid seminar { get; set; }
            public Guid org { get; set; }
            public Guid rec { get; set; }
            public Guid invited { get; set; }
            public int flag { get; set; }
            public string comment { get; set; }
            public decimal bonus { get; set; }
        }
    }
}