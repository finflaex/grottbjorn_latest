﻿#region

using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;

#endregion

namespace WebGBFXT.Areas.Reports.Controllers
{
    /// <summary>
    /// Статистика по операторам
    /// </summary>
    public class ManagerOperatorStatisticsController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index() => await this.func(View);
    }
}