﻿#region

using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

#endregion

namespace WebGBFXT.Areas.Reports.Controllers
{
    /// <summary>
    /// Пивот таблица результатов операторов
    /// </summary>
    [NoCache]
    [Roles]
    public class ManagerOperatorController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator, dbroles.manager)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Чтение результатов
        /// </summary>
        /// <param name="request"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator, dbroles.manager)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            DateTime? date)
            => await this.json(gb =>
            {
                var mainDate = date ?? DateTime.Now;
                var start = new DateTime(mainDate.Year, mainDate.Month, 1);
                var end = start.AddMonths(1);

                Thread.CurrentThread.CurrentCulture = new CultureInfo("RU-ru");

                return
                    (
                        from role in gb.db.Roles
                        where role.Key == dbroles.@operator
                        from user in role.Logins
                        from task in gb.db.Events.DefaultIfEmpty()
                        where task.Type == EventType.meetingClose
                        where task.Start >= start && task.End < end
                        where task.CreateAuthorID == user.ID
                        where task.MeetingResult != null
                        orderby task.Start descending
                        select new
                        {
                            userName = user.Caption,
                            start = task.Start,
                            closeBonus = task.MeetingResult.Bonus,
                            rateBonus = task.MeetingResult.Rate == null ? 0 : task.MeetingResult.Rate.Bonus,
                            orgName = task.Organization.ShortName ?? task.Organization.FullName,
                            depName = (
                                from dep in task.Departaments
                                where dep.Parent.Key == Departament.ConsultantDepartament
                                select dep.Caption
                            )
                            .FirstOrDefault()
                        }
                    )
                    .AsEnumerable()
                    .SelectMany(item => new[]
                    {
                        new
                        {
                            bonus = item.closeBonus,
                            item.start,
                            item.userName,
                            OrgName = item.orgName,
                            Rate = 0,
                            Departament = item.depName,
                            Meetings = 1
                        },
                        item.rateBonus == 0
                            ? null
                            : new
                            {
                                bonus = item.rateBonus,
                                item.start,
                                item.userName,
                                OrgName = item.orgName,
                                Rate = 1,
                                Departament = item.depName,
                                Meetings = 0
                            }
                    })
                    .Where(v => v != null)
                    .Select(item => new ReportManagerOperatorModel
                    {
                        Bonus = item.bonus,
                        Date = item.start.ToTZDateTime(gb.TimeZone),
                        NotPaid = item.Rate == 0 && item.bonus == 0 ? 1 : 0,
                        Paid = item.Rate == 0 && item.bonus == 0 ? 0 : 1,
                        Operator = item.userName,
                        OrgName = item.OrgName,
                        Rate = item.Rate,
                        Departament = item.Departament,
                        Meetings = item.Meetings
                    })
                    .ToDataSourceResult(request);
            });

        /// <summary>
        /// Выгрузка в Excel
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="base64"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator, dbroles.manager)]
        public async Task<ActionResult> Excel(string contentType, string base64, string fileName)
            => await this.func(gb =>
            {
                var fileContents = Convert.FromBase64String(base64);
                return File(fileContents, contentType, fileName);
            });

        public class ReportManagerOperatorModel
        {
            public string Operator { get; set; }
            public DateTime? Date { get; set; }
            public string OrgName { get; set; }
            public string Departament { get; set; }
            public decimal Bonus { get; set; }
            public int Paid { get; set; }
            public int NotPaid { get; set; }
            public int Rate { get; set; }
            public int Meetings { get; set; }
        }
    }
}