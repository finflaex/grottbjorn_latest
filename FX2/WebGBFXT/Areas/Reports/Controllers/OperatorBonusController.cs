﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.Attributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

#endregion

namespace WebGBFXT.Areas.Reports.Controllers
{
    /// <summary>
    /// Таблица бонусов операторов
    /// </summary>
    public class OperatorBonusController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Чтение списка бонусов
        /// </summary>
        /// <param name="request"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            DateTime? dateFrom, DateTime? dateTo)
            => await this.json(gb =>
            {
                var start = dateFrom ?? gb.Now.DateTime.GetStartWeek();
                var end = dateTo ?? gb.Now.DateTime.GetEndtWeek().AddDays(1);

                var query_meetingClose = from e in gb.db.Events
                    where e.Type == EventType.meetingClose
                    where !e.Closed
                    where e.Start > start
                    where e.Start < end
                    let user = (
                        from u in gb.db.Logins
                        where u.ID == e.CreateAuthorID
                        select u
                    )
                    .FirstOrDefault()
                    where user.Departaments.Any(v => v.Key == Departament.OperatorDepartament)
                    let dep = (
                        from dep in e.Departaments
                        select dep.Caption
                    )
                    .FirstOrDefault()
                    select new QueryModel
                    {
                        OrgID = e.OrgID,
                        Operator = user.Caption,
                        Date = e.Start,
                        OrgName = e.Organization.ShortName ?? e.Organization.FullName,
                        Departament = dep,
                        Type = e.Type,
                        Bonus = e.MeetingResult == null ? 0 : e.MeetingResult.Bonus
                    };

                var query_meetingConfirm = from e in gb.db.Events
                    where e.Type == EventType.meetingConfirm
                    where !e.Closed
                    where e.Start > start
                    where e.Start < end
                    let user = (
                        from u in gb.db.Logins
                        where u.ID == e.CreateAuthorID
                        select u
                    )
                    .FirstOrDefault()
                    where user.Departaments.Any(v => v.Key == Departament.OperatorDepartament)
                    let dep = (
                        from dep in e.Departaments
                        select dep.Caption
                    )
                    .FirstOrDefault()
                    select new QueryModel
                    {
                        OrgID = e.OrgID,
                        Operator = user.Caption,
                        Date = e.Start,
                        OrgName = e.Organization.ShortName ?? e.Organization.FullName,
                        Departament = dep,
                        Type = e.Type,
                        Bonus = e.MeetingResult == null ? 0 : e.MeetingResult.Bonus
                    };

                var query_meetingCancel = from e in gb.db.Events
                    where e.Type == EventType.meetingCancel
                    where e.Start > start
                    where e.Start < end
                    let user = (
                        from u in gb.db.Logins
                        where u.ID == e.CreateAuthorID
                        select u
                    )
                    .FirstOrDefault()
                    where user.Departaments.Any(v => v.Key == Departament.OperatorDepartament)
                    let dep = (
                        from dep in e.Departaments
                        select dep.Caption
                    )
                    .FirstOrDefault()
                    select new QueryModel
                    {
                        OrgID = e.OrgID,
                        Operator = user.Caption,
                        Date = e.Start,
                        OrgName = e.Organization.ShortName ?? e.Organization.FullName,
                        Departament = dep,
                        Type = e.Type,
                        Bonus = 0
                    };

                var query_meetingRate = from e in gb.db.Events
                    where e.Type == EventType.meetingClose
                    where !e.Closed
                    where e.Start > start
                    where e.Start < end
                    let user = (
                        from u in gb.db.Logins
                        where u.ID == e.CreateAuthorID
                        select u
                    )
                    .FirstOrDefault()
                    where user.Departaments.Any(v => v.Key == Departament.OperatorDepartament)
                    where e.MeetingResult != null
                    where e.MeetingResult.Rate != null
                    let dep = (
                        from dep in e.Departaments
                        select dep.Caption
                    )
                    .FirstOrDefault()
                    select new QueryModel
                    {
                        OrgID = e.OrgID,
                        Operator = user.Caption,
                        Date = e.Start,
                        OrgName = e.Organization.ShortName ?? e.Organization.FullName,
                        Departament = dep,
                        Type = EventType.meetingRate,
                        Bonus = e.MeetingResult.Rate.Bonus
                    };

                var query_seminarVisited = from e in gb.db.Events
                    where e.Type == EventType.seminarVisited
                    where !e.Closed
                    where e.Start > start
                    where e.Start < end
                    let user = (
                        from u in gb.db.Logins
                        where u.ID == e.CreateAuthorID
                        select u
                    )
                    .FirstOrDefault()
                    where user.Departaments.Any(v => v.Key == Departament.OperatorDepartament)
                    let dep = (
                        from dep in e.Departaments
                        select dep.Caption
                    )
                    .FirstOrDefault()
                    select new QueryModel
                    {
                        OrgID = e.OrgID,
                        Operator = user.Caption,
                        Date = e.Start,
                        OrgName = e.Organization.ShortName ?? e.Organization.FullName,
                        Departament = dep,
                        Type = EventType.meetingRate,
                        Bonus = e.MeetingResult == null ? 0 : e.MeetingResult.Bonus
                    };

                return query_meetingClose
                    .Concat(query_meetingConfirm)
                    .Concat(query_meetingCancel)
                    .Concat(query_meetingRate)
                    .Concat(query_seminarVisited)
                    .AsEnumerable()
                    .Select(read => new ReportOperatorBonusModel
                    {
                        OrgID = read.OrgID.Value,
                        Type = read.Type.AtValue(),
                        Departament = read.Departament,
                        Bonus = read.Bonus,
                        OrgName = read.OrgName,
                        Operator = read.Operator,
                        Date = read.Date.ToTZDateTime(gb.TimeZone).Date
                    })
                    .ToDataSourceResult(request);
            });

        public class ReportOperatorBonusModel
        {
            public string Operator { get; set; }
            public DateTime? Date { get; set; }
            public string OrgName { get; set; }
            public string Departament { get; set; }
            public decimal Bonus { get; set; }
            public string Type { get; set; }
            public Guid OrgID { get; set; }
        }
    }

    public class QueryModel
    {
        public string Operator { get; set; }
        public DateTimeOffset? Date { get; set; }
        public string OrgName { get; set; }
        public string Departament { get; set; }
        public EventType Type { get; set; }
        public decimal Bonus { get; set; }
        public Guid? OrgID { get; set; }
    }
}