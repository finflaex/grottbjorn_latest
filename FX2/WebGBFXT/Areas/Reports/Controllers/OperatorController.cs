﻿#region

using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.MVC.Atributes;
using Finflaex.Support.Reflection;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

#endregion

namespace WebGBFXT.Areas.Reports.Controllers
{
    /// <summary>
    /// Персональные рузальтаты встреч
    /// </summary>
    [NoCache]
    [Roles]
    public class OperatorController : Controller
    {
        /// <summary>
        /// Получение представления
        /// </summary>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Index() => await this.func(View);

        /// <summary>
        /// Получение результатов
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Roles(dbroles.admin, dbroles.@operator)]
        public async Task<ActionResult> Read(
            [DataSourceRequest] DataSourceRequest request)
            => await this.json(gb =>
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("RU-ru");

                return (
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    from task_meeting_confirm in gb.db.Events
                    where task_meeting_confirm.Type == EventType.meetingClose
                    where task_meeting_confirm.CreateAuthorID == gb.AuthorGuid
                    orderby task_meeting_confirm.Start descending
                    select new ReportUserBonusModel
                    {
                        ID = task_meeting_confirm.ID,
                        OrgName = task_meeting_confirm
                                      .Organization
                                      .ShortName ?? task_meeting_confirm
                                      .Organization
                                      .FullName,
                        Region = task_meeting_confirm
                            .Organization
                            .Address
                            .Where(v => v.Type == AddressType.region)
                            .Select(v => v.Name + " " + v.Suffix)
                            .FirstOrDefault(),
                        Comment = task_meeting_confirm.MeetingResult.Description,
                        Bonus = task_meeting_confirm.MeetingResult.Bonus,
                        Date = task_meeting_confirm.Start
                    })
                    .AsEnumerable()
                    .Select(v =>
                    {
                        v.Comment = v.Comment.HtmlDecode().FromHtml();
                        v.Month = v.Date.Value.ToString("MMMM yyyy г.");
                        return v;
                    })
                    .ToDataSourceResult(request);
            });

        public class ReportUserBonusModel
        {
            public string OrgName { get; set; }
            public string Region { get; set; }
            public decimal? Bonus { get; set; }
            public string Comment { get; set; }
            public DateTimeOffset? Date { get; set; }
            public string Month { get; set; }
            public Guid ID { get; set; }
        }
    }
}