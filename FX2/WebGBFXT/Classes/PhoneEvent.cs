﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Finflaex.Support.IO;
using Finflaex.Support.Reflection;

namespace WebGBFXT.Classes
{
    public class PhoneEvent
    {
        public Dictionary<string, fcDirectory> phon_list = new Dictionary<string, fcDirectory>();
        public Dictionary<string, fcDirectory> date_list = new Dictionary<string, fcDirectory>();
        public Dictionary<string, fcDirectory> call_list = new Dictionary<string, fcDirectory>();

        public PhoneEvent(string root_path)
        {
            Root = root_path
                .Trim('\\')
                .Prepend("\\\\")
                .Append("\\");
        }

        public string Root { get; set; }

        public event Action<PhoneCall> Call;

        public IEnumerable<string> ViewedPhones
        {
            set
            {
                foreach (var phone in value)
                {
                    AddPhonList(phone);
                }
            }
        }

        private void AddPhonList(string phone)
        {
            var path = $"{Root}{phone}\\";
            if (!Directory.Exists(path))
            {
                var dir = new fcDirectory(Root);
                dir.OnWatcher += arg =>
                {
                    if (arg.Name == phone)
                    {
                        phon_list.Remove(phone);
                        AddDateList(phone);
                    }
                };
                phon_list.Add(phone, dir);
            }
            else
            {
                AddDateList(phone);
            }
        }

        private void AddDateList(string phone)
        {
            var dir = new fcDirectory($"{Root}{phone}\\");
            dir.OnWatcher += arg =>
            {
                AddCallList($"{Root}{phone}\\{arg.Name}\\", phone);
            };
            date_list.Add(phone, dir);

            var path = $"{Root}{phone}\\{DateTime.Now:yyyy-MM-dd}\\";
            if (Directory.Exists(path))
            {
                AddCallList(path, phone);
            }
        }

        private void AddCallList(string path, string phone)
        {
            fcDirectory dir;
            try
            {
                dir = new fcDirectory(path);
            }
            catch
            {
                return;
            }
            dir.OnWatcher += arg =>
            {
                arg.FullPath
                    .DoRegexMatches(@"^\\\\ASTERISK\\records\\\d*\\[\d-]+\\(\d+)_(\d+)_0_([\d-]+_[\d-]+)\.mp3$")
                    .FuncSelect(v => new PhoneCall
                    {
                        who = v[0].Groups[1].Value,
                        whom = v[0].Groups[2].Value,
                        date = v[0].Groups[3].Value.ToDateTime("yyyy-MM-dd_HH-mm-ss"),
                        path = arg.FullPath,
                    })
                    .Action(v =>
                    {
                        Call?.Invoke(v);
                    });
            };
            if (call_list.ContainsKey(phone))
            {
                call_list.Remove(phone);
            }
            call_list.Add(phone, dir);
        }

        public class PhoneCall
        {
            public DateTime date { get; set; }
            public string who { get; set; }
            public string whom { get; set; }
            public string path { get; set; }
        }
    }
}