﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using CoreDB.Base;
using CoreDB.Repository;

#endregion

namespace TestConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Task.Run(async () =>
                {
                    using (var db = new DataDb())
                    using (var rep = new UserRepository(db))
                    {
                        var user = await rep.CreateUser(null);
                        var pass = await rep.SetPassword(null, user, "password");


                        var read = await rep.GetPassword(user);

                        Console.WriteLine(read);
                        Console.ReadKey();
                    }
                })
                .Wait();

            //Console.WriteLine("end");
            //Console.ReadKey();
        }
    }
}