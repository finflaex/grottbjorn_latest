﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeDom.CSPROJ
{
    /* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */
    using System;
    using System.Xml.Serialization;
    using System.Collections.Generic;
    namespace Xml2CSharp
    {
        [XmlRoot(ElementName = "Import", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
        public class Import
        {
            [XmlAttribute(AttributeName = "Project")]
            public string Project { get; set; }
            [XmlAttribute(AttributeName = "Condition")]
            public string Condition { get; set; }
        }

        [XmlRoot(ElementName = "Configuration", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
        public class Configuration
        {
            [XmlAttribute(AttributeName = "Condition")]
            public string Condition { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Platform", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
        public class Platform
        {
            [XmlAttribute(AttributeName = "Condition")]
            public string Condition { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "PropertyGroup", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
        public class PropertyGroup
        {
            [XmlElement(ElementName = "Configuration", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public Configuration Configuration { get; set; }
            [XmlElement(ElementName = "Platform", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public Platform Platform { get; set; }
            [XmlElement(ElementName = "ProjectGuid", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string ProjectGuid { get; set; }
            [XmlElement(ElementName = "OutputType", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string OutputType { get; set; }
            [XmlElement(ElementName = "RootNamespace", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string RootNamespace { get; set; }
            [XmlElement(ElementName = "AssemblyName", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string AssemblyName { get; set; }
            [XmlElement(ElementName = "TargetFrameworkVersion", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string TargetFrameworkVersion { get; set; }
            [XmlElement(ElementName = "FileAlignment", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string FileAlignment { get; set; }
            [XmlElement(ElementName = "PlatformTarget", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string PlatformTarget { get; set; }
            [XmlElement(ElementName = "DebugSymbols", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string DebugSymbols { get; set; }
            [XmlElement(ElementName = "DebugType", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string DebugType { get; set; }
            [XmlElement(ElementName = "Optimize", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string Optimize { get; set; }
            [XmlElement(ElementName = "OutputPath", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string OutputPath { get; set; }
            [XmlElement(ElementName = "DefineConstants", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string DefineConstants { get; set; }
            [XmlElement(ElementName = "ErrorReport", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string ErrorReport { get; set; }
            [XmlElement(ElementName = "WarningLevel", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public string WarningLevel { get; set; }
            [XmlAttribute(AttributeName = "Condition")]
            public string Condition { get; set; }
        }

        [XmlRoot(ElementName = "Reference", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
        public class Reference
        {
            [XmlAttribute(AttributeName = "Include")]
            public string Include { get; set; }
        }

        [XmlRoot(ElementName = "ItemGroup", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
        public class ItemGroup
        {
            [XmlElement(ElementName = "Reference", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public List<Reference> Reference { get; set; }
            [XmlElement(ElementName = "Compile", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public List<Compile> Compile { get; set; }
        }

        [XmlRoot(ElementName = "Compile", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
        public class Compile
        {
            [XmlAttribute(AttributeName = "Include")]
            public string Include { get; set; }
        }

        [XmlRoot(ElementName = "Project", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
        public class Project
        {
            [XmlElement(ElementName = "Import", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public List<Import> Import { get; set; }
            [XmlElement(ElementName = "PropertyGroup", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public List<PropertyGroup> PropertyGroup { get; set; }
            [XmlElement(ElementName = "ItemGroup", Namespace = "http://schemas.microsoft.com/developer/msbuild/2003")]
            public List<ItemGroup> ItemGroup { get; set; }
            [XmlAttribute(AttributeName = "ToolsVersion")]
            public string ToolsVersion { get; set; }
            [XmlAttribute(AttributeName = "xmlns")]
            public string Xmlns { get; set; }
        }

    }
}

