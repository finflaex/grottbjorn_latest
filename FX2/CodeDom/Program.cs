﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using CodeDom.CSPROJ.Xml2CSharp;
using Microsoft.CSharp;

internal static class Program
{
    private static void Main(params string[] args)
    {
        var catalog = args.FirstOrDefault();
        if (catalog == null) return;

        catalog = catalog.Trim().TrimEnd('\\') + "\\";

        if (!Directory.Exists(catalog)) return;

        var config = Directory.GetFiles(catalog, "*.csproj", SearchOption.AllDirectories)
            .Select(v=>new FileStream(v, FileMode.Open))
            .Select(XmlReader.Create)
            .Select(v=> new XmlSerializer(typeof(Project)).Deserialize(v) as Project)
            .SelectMany(v=>v.ItemGroup)
            .ToArray();

        var References = config
            .SelectMany(v => v.Reference)
            .Select(v => v.Include.Split(',').FirstOrDefault()?.Trim())
            .Where(v => v != null)
            .Select(v =>
            {
                return Directory
                           .GetFiles(catalog, "*.dll", SearchOption.AllDirectories)
                           .FirstOrDefault(file => Path.GetFileNameWithoutExtension(file) == v) ??
                       $"{v}.dll";
            })
            .ToArray();

        var Compiles = config
            .SelectMany(v => v.Compile)
            .Select(v => catalog + v.Include)
            .Where(v=>Path.GetFileName(v) != "AssemblyInfo.cs")
            .Select(File.ReadAllText)
            .Select(text => new
            {
                usings = Regex
                    .Matches(text, @"(using\s[\w\.]*;[\r\n]+)")
                    .Cast<Match>()
                    .Select(match => match.Groups[0].Value)
                    .ToArray(),
                code = Regex.Replace(text, @"(using\s[\w\.]*;[\r\n]+)", string.Empty)
            })
            .ToArray();

        var usings = string.Join("", Compiles.SelectMany(v => v.usings).Distinct().ToArray());
        var code = string.Join("\r\n", Compiles.Select(v=>v.code).ToArray());
        var source = string.Join("\r\n", new[] {usings, code});

        var dirname = catalog.Split(new[] { '\\' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
        //Directory.SetCurrentDirectory($@"{catalog}\bin\Debug\");

        foreach (var asm in References.Where(File.Exists))
        {
            var fname = Path.GetFileName(asm);
            try
            {
                File.Copy(asm, fname, true);
            }
            catch
            {

            }
        }

        {
            var log = new StringBuilder();
            log.AppendLine(source);
            log.AppendLine("================== EXECUTE INFO ==================");
            log.AppendLine($"Путь к проекту: {catalog}");
            log.AppendLine();

            log.AppendLine("================== REFERENCES ==================");
            foreach (var reference in References)
            {
                log.AppendLine($"{reference}");
            }
            File.WriteAllText($"{dirname}{DateTime.Now:yyyy_MM_dd_HH_mm_ss}.txt", log.ToString());
        }

        using (var provider = new CSharpCodeProvider(new Dictionary<string, string>
        {
            { "CompilerVersion", "v4.0" }
        }))
        {
            var path = $"{Guid.NewGuid():N}.dll";

            var refs = new List<string>(References);
            refs.Add("Microsoft.CSharp.dll");
            References = refs
                .Select(Path.GetFileName)
                //.Where(v => !File.Exists(v))
                .ToArray();

            var results = provider
                .CompileAssemblyFromSource(new CompilerParameters(References)
                {
                    GenerateExecutable = false,
                    OutputAssembly = path,
                    
                }, source);

            if (results.Errors.Count > 0)
            {
                var log = new StringBuilder();
                log.AppendLine(source);
                log.AppendLine("================== ERROR INFO ==================");
                log.AppendLine($"Путь к проекту: {catalog}");
                log.AppendLine($"Количество ошибок: {results.Errors.Count}");
                foreach (CompilerError err in results.Errors)
                {
                    log.AppendLine($"линия {err.Line} колонка {err.Column}");
                    log.AppendLine($"{err.ErrorText}");
                    log.AppendLine();
                }
                log.AppendLine("================== REFERENCES ==================");
                foreach (var reference in References)
                {
                    log.AppendLine($"{reference}");
                }
                dirname = catalog.Split(new[] {'\\'}, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
                File.WriteAllText($"{dirname}{DateTime.Now:yyyy_MM_dd_HH_mm_ss}.txt", log.ToString());
                return;
            }

            byte[] dllBytes = File.ReadAllBytes(path);
            Assembly asmDll = Assembly.Load(dllBytes, null);
            File.Delete(path);

            var methods = asmDll
                .GetTypes()
                .Where(v => v.Name == "Program")
                .SelectMany(v => v.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic))
                .Where(v => v.Name == "Main");

            foreach (var method in methods)
            {
                try
                {
                    method.Invoke(null, new object[] {args.Skip(1).ToArray()});
                }
                catch (Exception error)
                {
                    var log = new StringBuilder();
                    log.AppendLine(source);
                    log.AppendLine("================== EXECUTE Error INFO ==================");
                    log.AppendLine();
                    log.AppendLine(error.ToString());
                    File.WriteAllText($"{dirname}{DateTime.Now:yyyy_MM_dd_HH_mm_ss}.txt", log.ToString());
                }
            }
        }
    }
}


//using System;
//using System.CodeDom.Compiler;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Reflection;
//using Microsoft.CSharp;

//namespace CodeDom
//{
//    internal class Program
//    {
//        private static void Main(string[] args)
//        {
//            // Source code для компиляции 
//            string source =
//                @"
//using System.Collections.Generic;
//using System.Linq;
//namespace Foo 
//{ 
//    public class Bar 
//    { 
//        static void Main(string[] args) 
//        { 
//            Bar.SayHello(); 
//            System.Console.ReadKey();
//        } 

//        public static void SayHello() 
//        { 
//            System.Console.WriteLine(""Hello World""); 
//        } 
//    } 
//} 
//            ";

//            var path = "D:\\Foo.dll";

//            // Настройки компиляции 
//            Dictionary<string, string> providerOptions = new Dictionary<string, string>
//            {
//                {"CompilerVersion", "v3.5"}
//            };
//            CSharpCodeProvider provider = new CSharpCodeProvider(providerOptions);

//            CompilerParameters compilerParams = new CompilerParameters
//            {
//                OutputAssembly = path,
//                GenerateExecutable = false,
//            };

//            compilerParams.ReferencedAssemblies.Add("System.Core.Dll");

//            // Компиляция 
//            CompilerResults results = provider.CompileAssemblyFromSource(compilerParams, source);

//            // Выводим информацию об ошибках 
//            Console.WriteLine("Number of Errors: {0}", results.Errors.Count);
//            foreach (CompilerError err in results.Errors)
//            {
//                Console.WriteLine("ERROR {0}", err.ErrorText);
//            }

//            Assembly assembly = Assembly.LoadFile(path);
//            Type type = assembly.GetType("Foo.Bar");
//            MethodInfo method = type.GetMethod("SayHello");
//            method.Invoke(null, null);

//            Console.ReadKey();
//        }
//    }
//}