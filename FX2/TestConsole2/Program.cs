﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseDBFX.Base;
using BaseDBFX.Table;
using Finflaex.Support.ApiOnline.AddressMaster;
using Finflaex.Support.ApiOnline.OrgInfo;
using Finflaex.Support.Attributes;
using Finflaex.Support.Reflection;
using Finflaex.Support.Types;
using TelephonDBFX.Base;
using System.Text.RegularExpressions;

namespace TestConsole2
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = @"SIP\/Trunk_1\/(\d+),,tTr".ToRegex();

            start:

            var read = Console.ReadLine();

            Console.WriteLine(read.PhoneParse().ToJson(true));
            goto start;

            //using (var file = new FileStream("result.csv", FileMode.Create))
            //using (var writer = new StreamWriter(file))
            //{
            //    new TDBFX()
            //        .TELEFON
            //        .Where(v => v.disposition == "ANSWERED")
            //        .Select(v=>v.dst)
            //        .Where(v => v.Length > 4)
            //        .Distinct()
            //        .AsEnumerable()
            //        .Select(v => v.PhoneParse())
            //        .Distinct()
            //        .ForEach(read =>
            //        {
            //            if (read is string)
            //            {
            //                Console.WriteLine(read);
            //                writer.WriteLine(read);
            //            }
            //            else
            //            {
            //                writer.WriteLine($"\"{read}\";\"{read.RawPhone}\";\"{read.RusRegionCode}\";\"{read.RusPhoneNumber}\";\"{read.CountryCode}\";\"{read.ZagranPhoneNumber}\";\"{read.Parse}\"");
            //                Console.WriteLine(read.ToJson(true));
            //            }
            //        });
            //}

            //new TDBFX()
            //    .TELEFON
            //    .Where(v => v.lastdata.Contains("358104601"))
            //    .Distinct()
            //    //.Where(v=>!v.value.StartsWith("810"))
            //    //.Where(v=>v.Value.Length != 11)
            //    .ForEach(read =>
            //    {
            //        if (read is string)
            //        {
            //            Console.WriteLine(read);
            //        }
            //        else
            //        {
            //            Console.WriteLine(read.ToJson(true));
            //        }
            //    });


            //using (var db = new DBFX())
            //{
            //    foreach (var contact in db.Contacts)
            //    {
            //        contact.Value = contact.RawValue;
            //    }
            //    db.Commit();
            //}


            //var reg = (@"^9?(8|7)?(\d{3})?(\d{7})$").ToRegex();
            //var reg_country = (@"^9{0,2}?8?(10|19|29|09|99)?(358|373|374|375|380|383|770|771|772|777|983|984|987|988|992|993|994|996|998|989)(\d{6,10})$").ToRegex();

            //var phones = new TDBFX()
            //    .TELEFON
            //    .Where(v => v.disposition == "ANSWERED")
            //    .Where(v=>v.dst.Contains("312444865"))
            //    //.Where(v=>v.lastdata.Contains("37410593333"))
            //    //.Select(v => v.dst)
            //    .Where(v => v.dst.Length > 4)
            //    .Distinct()
            //    //.ToArray()
            //    //.Where(v => !reg.IsMatch(v.dst))
            //    .AsEnumerable()
            //    //.Where(v => !reg_country.IsMatch(v.dst) && !reg.IsMatch(v.dst))
            //    .ToArray();

            //foreach (var phone in phones)
            //{
            //    Console.WriteLine(phone.ToJson(true));
            //}

            //File.WriteAllLines("phones.txt", phones);

            //using (var db = new DBFX())
            //{
            //    var query = (
            //            from dep in db.Departaments
            //            where dep.Key == Departament.OperatorDepartament
            //            from user in dep.Users
            //            select new
            //            {
            //                user.Caption,
            //                user.InternalPhoneNumber
            //            })
            //        .ToArray();

            //        var result = new List<string>();
            //        var date = new DateTime(2017,3,1);

            //        foreach (var info in query)
            //        {
            //            var tquery = TDBFX
            //                .action(tdb =>
            //                {
            //                    tdb.Database.CommandTimeout = 1000;
            //                    return tdb
            //                        .TELEFON
            //                        .Where(v => v.src == info.InternalPhoneNumber)
            //                        .Where(v => v.calldate > date)
            //                        .Select(v => v.dst)
            //                        .Distinct()
            //                        .ToArray()
            //                        .Where(v=>v.Length > 4)
            //                        .Select(v=>v.RemoveStart("98"))
            //                        .ToArray();
            //                });

            //            var has_telephone = (
            //                from contact in db.Contacts
            //                where contact.Type == ContactType.phone
            //                where tquery.Contains(contact.ParseValue)
            //                select contact.ParseValue
            //            )
            //            .Distinct()
            //            .ToArray();

            //            var none = tquery.Except(has_telephone);

            //            result.Add("");
            //            Console.WriteLine();

            //            result.Add(info.Caption);
            //            result.Add($"Количество: {none.Count()} шт.");
            //            Console.WriteLine(info.Caption);
            //            foreach (var phone in none)
            //            {
            //                result.Add($"{phone} - отсутствует в ЦРМ");
            //                Console.WriteLine(phone);
            //            }
            //        }

            //        File.WriteAllLines("none_in_crm.txt", result);
            //    }

            //while (true)
            //{
            //    var read = Console.ReadLine();

            //    var list = fxsOrgInfo.OrgContacts(read);

            //    foreach (var item in list)
            //    {
            //        Console.WriteLine(item.ToJson(true));
            //    }
            //}


            //using (var db = new DBFX())
            //{
            //    var query = from org in db.Organizations
            //        let progress = (
            //            from e in org.Events
            //            where e.Type == EventType.progress
            //            where !e.Closed
            //            where e.Start < db.Now
            //            where e.End == null || e.End > db.Now
            //            select e
            //        )
            //        where !progress.Any()
            //        let old_progress = (
            //            from e in org.Events
            //            where e.Type == EventType.progress
            //            orderby e.Start
            //            from user in e.Users
            //            select new
            //            {
            //                e.CreateAuthorID,
            //                e.Start,
            //                e.UpdateAuthorID,
            //                e.End,
            //                user.Caption,
            //                e.Type,
            //                e.Description
            //            }
            //        )
            //        where old_progress.Any()
            //        from old in old_progress
            //        select new
            //        {
            //            name = org.ShortName ?? org.FullName,
            //            inn = org.Inn,
            //            acc = old_progress,
            //            create_auth = (
            //                from user in db.Logins
            //                where user.ID == old.CreateAuthorID
            //                select user
            //            )
            //            .FirstOrDefault(),
            //            update_auth = (
            //                from user in db.Logins
            //                where user.ID == old.UpdateAuthorID
            //                select user
            //            )
            //            .FirstOrDefault(),
            //            old.Start,
            //            old.End,
            //            old.Type,
            //            old.Description
            //        };

            //    var result = new StringBuilder();
            //    result.AppendLine($"inn;наименование;кому принадлежала;дата начала события;создавший событие;дата окончания события;завершивший событие;тип события;коментарий к событию");
            //    foreach (var org in query)
            //    {
            //        var text = $"{org.inn};{org.name};{org.acc.Select(v=>v.Caption).Join(",")};{org.Start};{org.create_auth?.Caption};{org.End};{org.update_auth?.Caption};{org.Type.AtValue()};{org.Description}";
            //        Console.WriteLine(text);
            //        result.AppendLine(text);
            //    }

            //    File.WriteAllText("log.csv", result.ToString(), Encoding.UTF8);
            //}

            //var path = @"\\10.0.0.120\ReportClient\Reports\";
            //var pattern = @"([\w]{1}[\d]{4})[^\d]*\d{0,4}[^\d]*((\d{2})\.?(\d{2})\.?(\d{4}))?[^\d]*(\d{2})\.?(\d{2})\.?(\d{4}).*\.([\w\.]*)";

            //var dir = new DirectoryInfo(path);

            //var files = dir.EnumerateFiles("*.*", SearchOption.AllDirectories);

            ////var count = 0;

            ////using (var log = new FileStream("log.txt", FileMode.Create))
            ////using (var writer = new StreamWriter(log))

            //var locker = new object();

            //using (var db = new FilePathDB())
            //{
            //    var count = 0;

            //    Parallel.ForEach(files.Where(v => v.Extension != ".db"), file =>
            //    {
            //        //var flag = false;
            //        //lock (locker)
            //        //{
            //        //    flag = db.FilePaths.Any(v => v.Path == file.FullName);
            //        //}

            //        //if (flag)return;
            //        try
            //        {
            //            var parse_dir = file.FullName.RemoveStart(path).Split('\\');
            //            var reg = file.Name.DoRegexMatches(pattern);
            //            var record = new FilePath
            //            {
            //                Root = parse_dir[0],
            //                DirYear = parse_dir[1],
            //                DirMonth = parse_dir[2],
            //                FullFileName = file.Name,
            //                Path = file.FullName,
            //                DirectoryPath = file.DirectoryName.Append("\\"),
            //                FileName = file.Name.RemoveEnd(file.Extension),
            //                ClientCode = reg[0].Groups[1].Value,

            //                DateStart = reg[0].Groups[3].Value.ToTryInt(),
            //                MonthStart = reg[0].Groups[4].Value.ToTryInt(),
            //                YearStart = reg[0].Groups[5].Value.ToTryInt(),
            //                DateEnd = reg[0].Groups[6].Value.ToTryInt(),
            //                MonthEnd = reg[0].Groups[7].Value.ToTryInt(),
            //                YearEnd = reg[0].Groups[8].Value.ToTryInt(),

            //                Ext = file.Extension.TrimStart('.'),
            //                DateCreate = file.LastWriteTime,
            //                Size = file.Length

            //            };

            //            lock (locker) db.FilePaths.Add(record);

            //            count++;
            //            if (count >= 1000)
            //            {
            //                lock (locker) db.SaveChanges();
            //                count = 0;
            //            }
            //        }
            //        catch
            //        {

            //        }
            //    });

            //    db.SaveChanges();
            //}

            var qwe = fxsAddressMaster
                .ParseAddress("680038,Хабаровский Край,Хабаровск Город,Серышева Улица,ДОМ 31")
                .FirstOrDefault();

            Console.WriteLine("end");
            Console.ReadLine();
        }
    }

    public static class fxsPhone
    {
        private static IEnumerable<string> RusCodeOut => new[]
        {
            "998",
            "98",
            "[789]",
            "810"
        };

        private static IEnumerable<string> ZagranCodeOut => new[]
        {
            "99810",
            "9810",
            "98",
            "810",
            "9",
            "09",
            "10",
            "19",
            "29"
        };

        private static IEnumerable<string> RusRegionCode => new[]
        {
            @"[3456789]\d{2}"
        };

        private static IEnumerable<string> RusPhone => new[]
        {
            @"\d{7}"
        };

        private static IEnumerable<string> CountryCode => new[]
        {
            "1"   ,
            "7"   ,
            "2[017]"  ,
            "3[0123469]"  ,
            "4[013456789]"  ,
            "5[12345678]"  ,
            "6[0123456"  ,
            "8[1246]"  ,
            "9[0123458]"  ,
            "212" ,
            "2[23456][0123456789]" ,
            "284" ,
            "29[1789]" ,
            "34[05]" ,
            "35[0123456789]" ,
            "37[012345678]" ,
            "38[015679]" ,
            "42[01]" ,
            "441" ,
            "473" ,
            "5[09][0123456789]" ,
            "649" ,
            "664" ,
            "6[78][0123456789]" ,
            "69[012]" ,
            "758" ,
            "767" ,
            "78[47]" ,
            "809" ,
            "85[02356]" ,
            "86[89]" ,
            "876" ,
            "88[06]" ,
            "96[0123456789]" ,
            "97[1234567]" ,
            "99[234569]" ,
            "993" ,
            "994" ,
            "995" ,
            "996" ,
            "998"
        };

        public static IEnumerable<string> ZagranPhoneNumber => new[]
        {
            @"\d{6,11}"
        };

        public static string RusRegexPattern => $@"^(?:{RusCodeOut.Join("|")})?({RusRegionCode.Join("|")})?({RusPhone.Join("|")})$";
        public static Regex RusRegex => new Regex(RusRegexPattern);

        public static string ZagranRegexPattern => $@"^(?:{ZagranCodeOut.Join("|")})?({CountryCode.Join("|")})({ZagranPhoneNumber.Join("|")})$";

        public static string AllRegexPattern => $@"(?:{ZagranRegexPattern})|(?:{RusRegexPattern}$)";

        public static fxcPhone PhoneParse(this string @this)
            => @this
                .ClearExcept("0123456789")
                .DoRegexMatches(AllRegexPattern)
                .FuncSelect(matches =>
                {
                    var result = new fxcPhone
                    {
                        RawPhone = @this
                    };
                    if (result.Parse = matches.Length == 1)
                    {
                        var groups = matches[0]
                            .Groups.Cast<Group>()
                            .Select(v=>v.Value);

                        result.RusRegionCode = groups.Skip(3).FirstOrDefault().retIfNullOrWhiteSpace(null);
                        result.RusPhoneNumber = groups.Skip(4).FirstOrDefault().retIfNullOrWhiteSpace(null);
                        result.CountryCode = groups.Skip(1).FirstOrDefault().retIfNullOrWhiteSpace(null);
                        result.ZagranPhoneNumber = groups.Skip(2).FirstOrDefault().retIfNullOrWhiteSpace(null);

                        if (result.IsRus && result.RusRegionCode == null)
                        {
                            result.RusRegionCode = "343";
                        }
                    }
                    return result;
                });
    }

    public class fxcPhone
    {
        public static implicit operator string(fxcPhone value) => value.ToString();

        public bool IsRus => RusPhoneNumber != null;
        public bool Parse { get; internal set; }
        public string CountryCode { get; internal set; }
        public string RusPhoneNumber { get; internal set; }
        public string RusRegionCode { get; internal set; }
        public string ZagranPhoneNumber { get; internal set; }
        public string RawPhone { get; internal set; }

    public override string ToString()
            => Parse
                ? IsRus
                    ? $"{RusRegionCode}{RusPhoneNumber}"
                    : $"{CountryCode}{ZagranPhoneNumber}"
                : RawPhone;
    }
}
