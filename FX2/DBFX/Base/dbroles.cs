﻿namespace BaseDBFX.Base
{
    public static class dbroles
    {
        public const string developer = "developer";
        public const string admin = "admin";
        public const string manager = "manager";
        public const string @operator = "operator";
        public const string consultant = "consultant";
        public const string expanded = "expanded";
        public const string lock_org_delete = "lock_org_delete";
        public const string cik = "cik";
    }
}