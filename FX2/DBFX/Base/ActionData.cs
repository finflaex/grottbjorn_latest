﻿#region

using System;
using System.Web.Mvc;
using BaseDBFX.Methods;
using Finflaex.Support.MVC;
using Finflaex.Support.MVC.Models;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Base
{
    public class ActionData
    {
        private readonly Controller controller;

        /// <summary>
        ///     Класс для хранения методов работы с карточкой организации
        /// </summary>
        //public readonly OrganizationCard orgcard;
        public readonly OrganizationEdit orgedit;

        public readonly int TimeZone;

        private fxaAuth _author;

        /// <summary>
        ///     Инициализатор базы данных
        /// </summary>
        public DBFX db;

        /// <summary>
        ///     Инициализация по умолчанию
        /// </summary>
        public ActionData()
        {
            orgedit = new OrganizationEdit(this);
            //orgcard = new OrganizationCard(this);

            if (db == null)
                db = new DBFX
                {
                    AuthorID = Guid.Empty
                };
        }

        /// <summary>
        ///     Инициализация для конкретного пользователя
        /// </summary>
        /// <param name="AuthorID"></param>
        public ActionData(Guid AuthorID)
            : this()
        {
            db = new DBFX
            {
                AuthorID = AuthorID
            };

            TimeZone = db.Logins.Find(AuthorGuid)?.TimeZone ?? 0;
        }

        /// <summary>
        ///     Инициализация из контроллера
        /// </summary>
        /// <param name="controller"></param>
        public ActionData(Controller controller)
            : this()
        {
            this.controller = controller;
            db = new DBFX
            {
                author = controller.GetAuthor()
            };
            try
            {
                TimeZone = db.Logins.Find(AuthorGuid)?.TimeZone ?? 0;
            }
            catch
            {
            }
        }

        /// <summary>
        ///     Возвращаем ИД пользователя
        /// </summary>
        public Guid AuthorGuid => Author?.Author ?? db.AuthorID;

        public fxaAuth Author => _author ?? (_author = controller?.GetAuthor());

        /// <summary>
        ///     Возвращаем текущее время
        /// </summary>
        public DateTimeOffset Now => db.Now;

        public MainValue GuidValue { get; set; }

        /// <summary>
        ///     Сохранение изменений в базе данных
        /// </summary>
        public void Save()
        {
            db?.Commit();
        }
    }
}