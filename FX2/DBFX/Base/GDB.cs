﻿#region

using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Base
{
    public static partial class gdb
    {
        // отложенная инициализация БД
        // отключено для ускорения и уменьшения потребления памяти
        //public static Task<R> async<R>(
        //    this Controller controller,
        //    Func<Lazy<DBFX>, R> act)
        //    where R : class
        //    => Task.Run(() =>
        //    {
        //        var db = new Lazy<DBFX>(() =>
        //        {
        //            var result = new DBFX();
        //            result.author = controller.GetAuthor();
        //            return result;
        //        });

        //        return act(db) ?? new EmptyResult().AsType<R>();
        //    });


        /// <summary>
        /// Работа с БД из контроллера
        /// </summary>
        /// <typeparam name="R"></typeparam>
        /// <param name="controller"></param>
        /// <param name="act"></param>
        /// <returns></returns>
        public static Task<ActionResult> func<R>(
            this Controller controller,
            Func<ActionData, R> act)
            where R : ActionResult
            => Task.Run(() =>
                act(new ActionData(controller))
                ?? new EmptyResult().AsType<ActionResult>());

        /// <summary>
        /// Работа с БД из контроллера
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="act"></param>
        /// <returns>результат как ActionResult</returns>
        public static Task<ActionResult> act(
            this Controller controller,
            Action<ActionData> act)
            => Task.Run(() =>
            {
                act(new ActionData(controller));
                return new EmptyResult().AsType<ActionResult>();
            });

        /// <summary>
        /// Работа с БД из контроллера
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="act"></param>
        /// <returns>результат в JSON</returns>
        public static Task<ActionResult> json(
            this Controller controller,
            Func<ActionData, object> act)
            => Task.Run(() => act(new ActionData(controller)).ToJsonResult().AsType<ActionResult>());


        /// <summary>
        /// Работа с БД из websocket
        /// </summary>
        /// <param name="user"></param>
        /// <param name="act"></param>
        public static void action(
            Guid user,
            Action<DBFX> act)
        {
            using (var db = new DBFX {AuthorID = user})
            {
                act(db);
            }
        }
    }
}