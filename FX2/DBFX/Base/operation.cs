﻿#region

using System;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using BaseDBFX.Table;
using Finflaex.Support.ApiOnline.BasicData;
using Finflaex.Support.Mail;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Base
{
    partial class gdb
    {
        #region config

        private const string mail_server = "10.0.0.10";
        private const string mail_login = "crm.service@grottbjorn.com";
        private const string mail_password = "";

        private static fxcMail mail => new fxcMail(mail_login, mail_password, mail_server);

        #endregion config

        #region meeting

        /// <summary>
        ///     Закрыть встречу
        /// </summary>
        /// <param name="db">база данных</param>
        /// <param name="meeting_id">ид события</param>
        public static void close_meeting(
            DBFX db, 
            Guid meeting_id)
        {
            var query =
                from meeting in db.Events
                where meeting.ID == meeting_id
                select new
                {
                    meeting,
                    meeting.Organization,
                    progress = from progress in meeting.Organization.Events
                               where !progress.Closed
                               where progress.Start < db.Now
                               where progress.End > db.Now
                               where progress.Type == EventType.progress
                               select progress,
                    author = from user in db.Logins
                             where user.ID == db.AuthorID
                             select user
                };

            //var query =
            //    from meeting in db.Events
            //    where meeting.ID == meeting_id
            //    select meeting;

            query.FirstOrDefault().ActionIfNotNull(read =>
            {
                read.meeting.Type = EventType.meetingClose;
                foreach (var progress in read.progress)
                {
                    progress.Closed = true;
                    progress.End = db.Now;
                }
                read.Organization.Events.Add(new Event
                {
                    Type = EventType.progress,
                    Start = db.Now,
                    End = DateTimeOffset.MaxValue,
                    Users = read.author.ToArray()
                });
                db.Commit();
            });
        }

        /// <summary>
        /// Отправка письма клиенту
        /// </summary>
        /// <param name="from">от кого</param>
        /// <param name="person">клиент</param>
        /// <param name="contact">email клиента</param>
        /// <param name="title">тема</param>
        /// <param name="body">содержание</param>
        /// <param name="html">формат</param>
        public static void send_mail(
            string from,
            Person person, 
            Contact contact, 
            string title, 
            string body,
            bool html = true)
        {
            if (contact?.ParseValue != null)
            {
                var name = person?.FullName;

                name.IsNullOrWhiteSpace().IfTrue(() =>
                {
                    name = "Уважаемый клиент";
                });

                mail.Send(
                    new MailAddress(from, "Grottbjorn"),
                    new MailAddress(contact.ParseValue, name),
                    title, body, html);
            }
        }

        /// <summary>
        /// Отправка письма пользователю
        /// </summary>
        /// <param name="from">от юзера</param>
        /// <param name="user">юзеру</param>
        /// <param name="title">тема</param>
        /// <param name="body">содержание</param>
        /// <param name="html">формат</param>
        public static void send_mail(
            Login from,
            Login user,
            string title,
            string body,
            bool html = true)
        {
            if (user?.InternalEMailAddress != null)
                mail.Send(
                    new MailAddress(from.InternalEMailAddress, "Финансовое ателье 'GROTTBJORN'"),
                    new MailAddress(user.InternalEMailAddress, user.Caption),
                    title, body, html);
        }

        /// <summary>
        ///     Добавить результат для встречи
        /// </summary>
        /// <param name="db">база данных</param>
        /// <param name="meeting_id">ид события</param>
        /// <param name="result_comment">комментарий</param>
        /// <param name="result_bonus">бонус для оператора</param>
        /// <param name="result_cost">затраты на встречу</param>
        /// <param name="result_lpr">присутствие лпр</param>
        /// <param name="result_distance">расстояние до места встречи</param>
        public static void add_meeting_result(
            DBFX db,
            Guid meeting_id,
            string result_comment,
            decimal result_bonus,
            decimal result_cost,
            bool result_lpr,
            int result_distance)
        {
            var result = (
                from meeting in db.Events
                where meeting.ID == meeting_id
                where meeting.MeetingResult != null
                select meeting.MeetingResult
                ).FirstOrDefault();

            var adminid = 1.ToGuid();

            var query =
                from meeting in db.Events
                where meeting.ID == meeting_id
                select new
                {
                    meeting,
                    consultant = (
                        from user in db.Logins
                        where user.ID == db.AuthorID
                        select user
                    )
                    .FirstOrDefault(),
                    @operator = (
                        from user in db.Logins
                        where user.ID == meeting.CreateAuthorID
                        where user.Roles.Select(v=>v.Key).All(v=>v != dbroles.consultant)
                        //from progress in meeting.Organization.Events
                        //where progress.Type == EventType.progress
                        //where !progress.Closed
                        //where progress.End > db.Now
                        //from user in progress.Prev.Users
                        //where !user.Roles.Select(v => v.Key).Contains(dbroles.consultant)
                        select user
                    )
                    .FirstOrDefault()
                };

            var read = query.First();

            //var value = result ?? db.Add(new MeetingResult());
            //value.RecordID = read.meeting.RecordID;
            //value.ID = meeting_id;
            //value.Description = result_comment;
            //value.Bonus = result?.Bonus ?? result_bonus;
            //value.Cost = result_cost;
            //value.LPR = result_lpr;
            //value.Distance = result_distance;
            //value.Consultants.Add(read.consultant);
            //value.Operator = read.@operator;
            //read.meeting.MeetingResult = value;
            //db.Commit();

            read.meeting.MeetingResult = new MeetingResult
            {
                RecordID = read.meeting.RecordID,
                ID = meeting_id,
                Description = result_comment,
                Bonus = result?.Bonus ?? result_bonus,
                Cost = result_cost,
                LPR = result_lpr,
                Distance = result_distance,
                Consultants = read.consultant.InArray(),
                Operator = read.@operator,
            };
            db.Commit();

            if (result != null) return;

            if (read.@operator == null) return;

            var from = db.Logins.Find(db.AuthorID);
            send_mail(from, read.@operator, $"Премия", $@"
Добрый день {read.@operator.Caption}!

Сообщаю что назначенная встреча была закрыта:

-----------------------------
Дата: {read.meeting.Start:dd MMMM yyyyг. ddd HH:mm}
Наименование: {read.meeting.Organization.ShortName ?? read.meeting.Organization.FullName}.
Адрес организации: {read.meeting.Organization.FormatAddress}.

Консультант: {read.consultant.Caption}.

Бонус: <strong>{read.meeting.MeetingResult.Bonus:### ###.00 руб.}</strong>

-----------------------------

{read.meeting.MeetingResult.Description.HtmlDecode().FromHtml()}
".Replace("\r\n", "<br>"));

            add_call_rate(db, meeting_id);
        }

        /// <summary>
        /// Добавить контроль качества
        /// </summary>
        /// <param name="db"></param>
        /// <param name="meeting_id"></param>
        public static void add_call_rate(
            DBFX db, 
            Guid meeting_id)
        {
            var query =
                from meeting in db.Events
                where meeting.ID == meeting_id
                select new
                {
                    @operator = meeting.MeetingResult.Operator,
                    orgid = meeting.OrgID,
                    recid = meeting.RecordID,
                    meeting
                };

            var read = query.FirstOrDefault();

            if (read?.@operator != null)
            {
                var task = new Event
                {
                    OrgID = read.orgid,
                    Start = db.Now.DateTime,
                    Notify = db.Now.DateTime,
                    End = DateTime.MaxValue,
                    RecordID = read.recid,
                    Type = EventType.meetingRate,
                    Prev = read.meeting
                };

                task.Users.Add(read.@operator);
                db.Add(task);
                db.Commit();
            }
        }

        /// <summary>
        /// Создать встречу
        /// </summary>
        /// <param name="db"></param>
        /// <param name="orgid"></param>
        /// <param name="recid"></param>
        /// <param name="depid"></param>
        /// <param name="intid"></param>
        /// <param name="_start"></param>
        /// <param name="time"></param>
        /// <param name="comment"></param>
        /// <param name="for_consultant"></param>
        /// <param name="yesterday"></param>
        /// <returns></returns>
        public static Guid add_meeting(
            DBFX db,
            Guid orgid,
            Guid recid,
            Guid depid,
            Guid intid,
            DateTime _start,
            DateTime? time,
            string comment,
            bool for_consultant,
            bool yesterday)
        {
            var query =
                from org in db.Organizations
                where org.ID == orgid
                select new
                {
                    author = (
                        from author in db.Logins
                        where author.ID == db.AuthorID
                        select author
                    )
                    .FirstOrDefault(),
                    is_consultant = (
                        from author in db.Logins
                        where author.ID == db.AuthorID
                        from role in author.Roles
                        select role.Key
                    )
                    .Contains(dbroles.consultant),
                    departament = (
                        from dep in db.Departaments
                        where dep.ID == depid
                        select dep
                    )
                    .FirstOrDefault(),
                    managers = from dep in db.Departaments
                    where dep.ID == depid
                    from manager in dep.Users
                    from role in manager.Roles
                    where role.Key == dbroles.manager
                    select manager,

                    interest = (
                        from interest in db.OrgInterest
                        where interest.ID == intid
                        select interest
                    )
                    .FirstOrDefault(),

                    region = (
                        from adr in org.Address
                        where adr.Type == AddressType.region
                        select new
                        {
                            name = adr.Name + " " + adr.Suffix,
                            timezone = adr.TimeZone
                        } 
                    )
                    .FirstOrDefault(),

                    orgname = org.ShortName ?? org.FullName,
                    instname = (
                        from inst in db.OrgInterest
                        where inst.ID == intid
                        select inst.Activity.ActivityValue.Caption
                    )
                    .FirstOrDefault(),

                    orgadr = org.FormatAddress,
                    contact = (
                        from record in db.ContactRecords
                        where record.ID == recid
                        select new
                        {
                            posts = record.Posts.Select(v=>v.Caption),
                            persons = record.Persons.Select(v=>v.FullName ?? v.RawName),
                            contacts = record.Contacts.Select(v=>v.ParseValue ?? v.RawValue)
                        }
                    )
                    .FirstOrDefault(),
                    clientTimeZone = org.Address
                        .OrderByDescending(v => v.Type)
                        .Select(v => v.TimeZone)
                        .FirstOrDefault(v => v != 0)
                };

            var read = query.FirstOrDefault();

            var start = _start.ToTZDateTimeOffset(read?.region.timezone ?? 0);

            var task = new Event
            {
                Type = EventType.meeting,
                OrgID = orgid,
                RecordID = recid,
                Start = start,
                End = start.AddHours(3),
                Description = comment,
                NotifyConsultant = for_consultant
            };

            read.departament.ActionIfNotNull(task.Departaments.Add);
            read.interest.ActionIfNotNull(task.Interests.Add);

            var managers = read.managers.ToArray();

            if (read.is_consultant)
            {
                task.Users.Add(read.author);
                managers.Add(read.author);
            }
            else
            {
                if (for_consultant)
                {
                    // todo если для консультантов и не консультант
                }
                else
                {
                    task.Users.Add(read.author);
                }
            }

            SetNotify(task, start, yesterday, time);

            db.Add(task);
            db.Commit();

            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            var from = db.Logins.Find(db.AuthorID);
            managers.ForEach(user =>
            {
                send_mail(from, user, $"{read.region.name}, {start:yyyy-MM-dd}, {read.orgname}", $@"
Добрый день {user.Caption}!

Сообщаю что в Ваше подразделение назначена новая неподтвержденная встреча:
-----------------------------
Дата: {start.Date.ToTZDateTimeOffset(read.clientTimeZone):dd MMMM yyyyг. ddd HH:mm} ({read.clientTimeZone.ToMSK()})
Дата: {start:dd MMMM yyyyг. ddd HH:mm} (в целях совместимости)
Цель: {read.instname}.

Наименование: {read.orgname}.
Адрес организации: {read.orgadr}.

Должность: {read.contact.posts.Join(", ")}.
Имя: {read.contact.persons.Join(", ")}.
Контакт: {read.contact.contacts.Join(", ")}.

Назначивший: {read.author.Caption}.
-----------------------------
{comment}
".Replace("\r\n", "<br>"));
            });

            return task.ID;
        }

        /// <summary>
        /// Изменить встречу
        /// </summary>
        /// <param name="db"></param>
        /// <param name="eventid"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="time"></param>
        /// <param name="comment"></param>
        /// <param name="for_consultant"></param>
        /// <param name="yesterday"></param>
        public static void set_meeting(
            DBFX db,
            Guid eventid,
            DateTime start,
            DateTime end,
            DateTime? time,
            string comment,
            bool for_consultant,
            bool yesterday)
        {
            var query =
                from @event in db.Events
                where @event.ID == eventid
                from adr in @event.Organization.Address
                where adr.Type == AddressType.region
                select new
                {
                    @event,
                    org = @event.Organization.ShortName ?? @event.Organization.FullName,
                    region = adr.Name + " " + adr.Suffix + ".",
                    adr = @event.Organization.FormatAddress,
                    oper = db.Logins.FirstOrDefault(v => v.ID == db.AuthorID),
                    users = (
                        from user in @event.Users
                        where user.Roles.Any(v => v.Key == dbroles.consultant)
                        select user
                    )
                    .Concat(
                        from dep in @event.Departaments
                        from user in dep.Users
                        where user.Roles.Any(v => v.Key == dbroles.manager)
                        where user.Roles.Any(v => v.Key == dbroles.consultant)
                        select user
                    )
                    .Distinct(),
                    clientTimeZone = @event.Organization.Address
                        .OrderByDescending(v=>v.Type)
                        .Select(v => v.TimeZone)
                        .FirstOrDefault(v => v != 0)
                };

            var read = query.FirstOrDefault();

            if(read == null) return;

            read.@event.Start = start.ToTZDateTimeOffset(read.clientTimeZone);
            read.@event.End = end.ToTZDateTimeOffset(read.clientTimeZone);
            read.@event.Description = comment;
            read.@event.NotifyConsultant = for_consultant;

            SetNotify(read.@event, start, yesterday, time);

            db.Commit();

            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            var from = db.Logins.Find(db.AuthorID);
            read.users.ForEach(user =>
            {
                send_mail(from, user, $"!!! {read.region}, {start:yyyy-MM-dd}, {read.org}", $@"
Добрый день {user.Caption}!

Сообщаю что встреча была обновлена:
-----------------------------
Дата: {start.Date.ToTZDateTimeOffset(read.clientTimeZone):dd MMMM yyyyг. ddd HH:mm} ({read.clientTimeZone.ToMSK()})
Дата: {start:dd MMMM yyyyг. ddd HH:mm} (в целях совместимости)
Цель: {read.@event.Interests.FirstOrDefault()?.Activity.ActivityValue.Caption}.

Наименование: {read.org}.
Адрес организации: {read.adr}.

Должность: {read.@event.Record.Posts.Select(v => v.Caption).Join(", ")}.
Имя: {read.@event.Record.Persons.Select(v => v.FullName ?? v.RawName).Join(", ")}.
Контакт: {read.@event.Record.Contacts.Select(v => v.ParseValue ?? v.RawValue).Join(", ")}.

Обновивший: {read.oper.Caption}.
-----------------------------
{comment}
"
.Replace("\r\n","<br>"));
            });
        }

        /// <summary>
        /// Подтвердить встречу
        /// </summary>
        /// <param name="db"></param>
        /// <param name="eventid"></param>
        public static void confirm_meeting(
            DBFX db,
            Guid eventid)
        {
            var query =
                from @event in db.Events
                where @event.ID == eventid
                from adr in @event.Organization.Address
                where adr.Type == AddressType.region
                select new
                {
                    @event,
                    org = @event.Organization.ShortName ?? @event.Organization.FullName,
                    region = adr.Name + " " + adr.Suffix + ".",
                    adr = @event.Organization.FormatAddress,
                    oper = db.Logins.FirstOrDefault(v => v.ID == db.AuthorID),
                    users = (
                        from user in @event.Users
                        where user.Roles.Any(v => v.Key == dbroles.consultant)
                        select user
                    )
                    .Concat(
                        from dep in @event.Departaments
                        from user in dep.Users
                        where user.Roles.Any(v => v.Key == dbroles.manager)
                        where user.Roles.Any(v => v.Key == dbroles.consultant)
                        select user
                    )
                    .Distinct(),
                    clientTimeZone = @event.Organization.Address
                        .OrderByDescending(v => v.Type)
                        .Select(v => v.TimeZone)
                        .FirstOrDefault(v => v != 0)
                };

            var read = query.FirstOrDefault();

            if (read == null) return;

            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            var from = db.Logins.Find(db.AuthorID);
            read.users.ForEach(user =>
            {
                send_mail(from, user, $"!!! {read.region}, {read.@event.Start:yyyy-MM-dd}, {read.org}", $@"
Добрый день {user.Caption}!

Сообщаю что встреча была подтверждена:
-----------------------------
Дата: {read.@event.Start.ToTZString(read.clientTimeZone)}
Цель: {read.@event.Interests.FirstOrDefault()?.Activity.ActivityValue.Caption}.

Наименование: {read.org}.
Адрес организации: {read.adr}.

Должность: {read.@event.Record.Posts.Select(v => v.Caption).Join(", ")}.
Имя: {read.@event.Record.Persons.Select(v => v.FullName ?? v.RawName).Join(", ")}.
Контакт: {read.@event.Record.Contacts.Select(v => v.ParseValue ?? v.RawValue).Join(", ")}.

Обновивший: {read.oper.Caption}.
-----------------------------
{read.@event.Description}
"
                    .Replace("\r\n", "<br>"));
            });
        }

        /// <summary>
        /// Отменить встречу
        /// </summary>
        /// <param name="db"></param>
        /// <param name="eventid"></param>
        public static void cancel_meeting(
            DBFX db,
            Guid eventid)
        {
            var query =
                from @event in db.Events
                where @event.ID == eventid
                from adr in @event.Organization.Address
                where adr.Type == AddressType.region
                select new
                {
                    @event,
                    org = @event.Organization.ShortName ?? @event.Organization.FullName,
                    region = adr.Name + " " + adr.Suffix + ".",
                    adr = @event.Organization.FormatAddress,
                    oper = db.Logins.FirstOrDefault(v => v.ID == db.AuthorID),
                    users = (
                        from user in @event.Users
                        where user.Roles.Any(v => v.Key == dbroles.consultant)
                        select user
                    )
                    .Concat(
                        from dep in @event.Departaments
                        from user in dep.Users
                        where user.Roles.Any(v => v.Key == dbroles.manager)
                        where user.Roles.Any(v => v.Key == dbroles.consultant)
                        select user
                    )
                    .Distinct(),
                    clientTimeZone = @event.Organization.Address
                        .OrderByDescending(v => v.Type)
                        .Select(v => v.TimeZone)
                        .FirstOrDefault(v => v != 0)
                };

            var read = query.FirstOrDefault();

            if (read == null) return;

            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            var from = db.Logins.Find(db.AuthorID);
            read.users.ForEach(user =>
            {
                send_mail(from, user, $"!!! {read.region}, {read.@event.Start:yyyy-MM-dd}, {read.org}", $@"
Добрый день {user.Caption}!

Сообщаю что встреча была отменена:
-----------------------------
Дата: {read.@event.Start.ToTZString(read.clientTimeZone)}
Цель: {read.@event.Interests.FirstOrDefault()?.Activity.ActivityValue.Caption}.

Наименование: {read.org}.
Адрес организации: {read.adr}.

Должность: {read.@event.Record.Posts.Select(v => v.Caption).Join(", ")}.
Имя: {read.@event.Record.Persons.Select(v => v.FullName ?? v.RawName).Join(", ")}.
Контакт: {read.@event.Record.Contacts.Select(v => v.ParseValue ?? v.RawValue).Join(", ")}.

Обновивший: {read.oper.Caption}.
-----------------------------
{read.@event.Description}
"
                    .Replace("\r\n", "<br>"));
            });
        }

        /// <summary>
        /// Изменить время оповещения
        /// </summary>
        /// <param name="task"></param>
        /// <param name="start"></param>
        /// <param name="yesterday"></param>
        /// <param name="time"></param>
        private static void SetNotify(
            Event task, 
            DateTimeOffset start,
            bool yesterday,
            DateTime? time)
        {
            var date = start;
            if (yesterday)
            {
                fxsProizvodstvCallendar.TypeDay status;
                do
                {
                    date = date.AddDays(-1);
                    status = fxsProizvodstvCallendar.GetStatusDay(date.Date);
                } while (status == fxsProizvodstvCallendar.TypeDay.notWork);

            }
            else
            {
                date = start.Date.AddHours(8);
            }
            task.Notify = date.Date
                .AddHours(time?.Hour ?? 8)
                .AddMinutes(time?.Minute ?? 0);
        }

        #endregion meeting

        #region event

        /// <summary>
        /// Добавление обещания быть на семинаре
        /// </summary>
        /// <param name="db"></param>
        /// <param name="eventid"></param>
        /// <param name="orgid"></param>
        /// <param name="recid"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static Guid add_event(
            DBFX db, 
            Guid eventid, 
            Guid orgid, 
            Guid recid, 
            string description)
        {
            var query =
                from task in db.Events
                where task.ID == eventid
                from org in db.Organizations
                where org.ID == orgid
                from contact in db.ContactRecords
                where contact.ID == recid
                select new
                {
                    task,
                    taskname = task.Prev.Caption,
                    users = from user in task.Users
                            select user,
                    deps = from user in task.Users
                           from dep in user.Departaments
                           where dep.Parent.Key == Departament.ConsultantDepartament
                           select dep,
                    taskadr = from adr in task.Address
                              select adr,
                    orgadr = from adr in org.Address
                             select adr,
                    orgname = org.ShortName ?? org.FullName,
                    contact,
                    oper = db.Logins.FirstOrDefault(v=>v.ID == db.AuthorID)
                };

            var read = query.FirstOrDefault();

            var @event = new Event
            {
                Type = EventType.seminarPromised,
                Prev = read.task,
                OrgID = orgid,
                RecordID = recid,
                Description = description.HtmlDecode()
            };

            @event.Users.AddRange(read.users);
            @event.Departaments.AddRange(read.deps);

            db.Add(@event);
            db.Commit();

            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            var from = db.Logins.Find(db.AuthorID);
            read.users.ForEach(user =>
            {
                send_mail(from, user, $"{read.task.Caption}, {read.orgname}", $@"
Добрый день {user.Caption}!

Сообщаю что представитель организации обещал быть на семинаре:
-----------------------------
Семинар: {read.taskname}.
Место проведения: {read.taskadr.OrderBy(v => v.Type).Select(v => $"{v.Name} {v.Suffix}").Join(", ")}.

Наименование: {read.orgname}.
Адрес организации: {read.orgadr.OrderBy(v=>v.Type).Select(v=>$"{v.Name} {v.Suffix}").Join(", ")}.

Должность: {read.contact.Posts.Select(v => v.Caption).Join(", ")}.
Имя: {read.contact.Persons.Select(v => v.FullName ?? v.RawName).Join(", ")}.
Контакт: {read.contact.Contacts.Select(v => v.ParseValue ?? v.RawValue).Join(", ")}.

Назначивший: {read.oper.Caption}.
-----------------------------
{description}
".Replace("\r\n", "<br>"));
            });

            return @event.ID;
        }

        #endregion

        #region trip

        /// <summary>
        /// Добавление встречи в командировке
        /// </summary>
        /// <param name="db"></param>
        /// <param name="eventid"></param>
        /// <param name="_start"></param>
        /// <param name="comment"></param>
        /// <param name="recid"></param>
        /// <param name="time"></param>
        /// <param name="for_consultant"></param>
        /// <returns></returns>
        public static Guid add_trip(
            DBFX db, 
            Guid eventid,
            DateTime _start, 
            string comment,
            Guid recid,
            DateTime? time,
            bool for_consultant)
        {
            var query =
                from @event in db.Events
                where @event.ID == eventid
                from contact in db.ContactRecords
                where contact.ID == recid
                select new
                {
                    @event,
                    contact,
                    org = contact.Organization,
                    users = (from user in @event.Users
                        select user)
                    .Distinct(),
                    dep = (from user in @event.Users
                        from dep in user.Departaments
                        where dep.Parent.Key == Departament.ConsultantDepartament
                        select dep)
                    .Distinct(),
                    region = contact
                        .Organization
                        .Address
                        .Where(v => v.Type == AddressType.region)
                        .Select(v => v.Name + " " + v.Suffix)
                        .FirstOrDefault(),
                    oper = db
                        .Logins
                        .Where(v => v.ID == db.AuthorID)
                        .Select(v => v.Caption)
                        .FirstOrDefault(),
                    clientTimeZone = @event.Address
                        .OrderByDescending(v => v.Type)
                        .Select(v => v.TimeZone)
                        .FirstOrDefault(v => v != 0)
                };

            var read = query.FirstOrDefault();

            var start = _start.ToTZDateTimeOffset(read?.clientTimeZone ?? 0);

            var task = new Event
            {
                Type = EventType.meetingEvent,
                OrgID = read.contact.OrganizationID,
                RecordID = recid,
                Start = start,
                End = start.AddHours(3),
                Description = comment,
                Prev = read.@event,
                NotifyConsultant = for_consultant
            };

            read.dep.ForEach(task.Departaments.Add);
            //read.ints.ActionIfNotNull(task.Interests.Add);
            read.users.ForEach(task.Users.Add);
            SetNotify(task, start, false, time);

            db.Add(task);
            db.Commit();

            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            var from = db.Logins.Find(db.AuthorID);
            read.users.ForEach(user =>
            {
                send_mail(from, user, $"{read.region}, {start:yyyy-MM-dd}, {read.org.ShortName ?? read.org.FullName}", $@"
Добрый день {user.Caption}!

Сообщаю что Вам назначена новая неподтвержденная встреча:
-----------------------------
Дата: {start:dd MMMM yyyyг. ddd HH:mm}

Наименование: {read.org.ShortName ?? read.org.FullName}.
Адрес организации: {read.org.FormatAddress}.

Должность: {read.contact.Posts.Select(v => v.Caption).Join(", ")}.
Имя: {read.contact.Persons.Select(v => v.FullName ?? v.RawName).Join(", ")}.
Контакт: {read.contact.Contacts.Select(v => v.ParseValue ?? v.RawValue).Join(", ")}.

Назначивший: {read.oper}.
-----------------------------
{comment}
".Replace("\r\n", "<br>"));
            });

            return task.ID;

        }

        #endregion

        /// <summary>
        /// Изменить комментарий к встрече
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id"></param>
        public static void set_meeting_comment(DBFX db, Guid id)
        {
            var query =
                from @event in db.Events
                where @event.ID == id
                where @event.Start > db.Now
                where @event.Type == EventType.meeting || @event.Type == EventType.meetingConfirm
                from adr in @event.Organization.Address
                where adr.Type == AddressType.region
                select new
                {
                    @event,
                    org = @event.Organization.ShortName ?? @event.Organization.FullName,
                    region = adr.Name + " " + adr.Suffix + ".",
                    adr = @event.Organization.FormatAddress,
                    oper = db.Logins.FirstOrDefault(v => v.ID == db.AuthorID),
                    users = (
                        from user in @event.Users
                        where user.Roles.Any(v => v.Key == dbroles.consultant)
                        select user
                    )
                    .Concat(
                        from dep in @event.Departaments
                        from user in dep.Users
                        where user.Roles.Any(v => v.Key == dbroles.manager)
                        where user.Roles.Any(v => v.Key == dbroles.consultant)
                        select user
                    )
                    .Distinct(),
                    clientTimeZone = @event.Organization.Address
                        .OrderByDescending(v => v.Type)
                        .Select(v => v.TimeZone)
                        .FirstOrDefault(v => v != 0)
                };

            var read = query.FirstOrDefault();

            if (read == null) return;

            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            var from = db.Logins.Find(db.AuthorID);
            read.users.ForEach(user =>
            {
                send_mail(from, user, $"!!! {read.region}, {read.org}", $@"
Добрый день {user.Caption}!

Комментарий к встрече был обновлен:
-----------------------------
Дата: {read.@event.Start.ToTZString(read.clientTimeZone)})
Цель: {read.@event.Interests.FirstOrDefault()?.Activity.ActivityValue.Caption}.

Наименование: {read.org}.
Адрес организации: {read.adr}.

Должность: {read.@event.Record.Posts.Select(v => v.Caption).Join(", ")}.
Имя: {read.@event.Record.Persons.Select(v => v.FullName ?? v.RawName).Join(", ")}.
Контакт: {read.@event.Record.Contacts.Select(v => v.ParseValue ?? v.RawValue).Join(", ")}.

Обновивший: {read.oper.Caption}.
-----------------------------
{read.@event.Description}
"
                    .Replace("\r\n", "<br>"));
            });
        }

        /// <summary>
        /// Указать ответственных за встречу
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id"></param>
        public static void set_meeting_progress(DBFX db, Guid id)
        {
            var query =
                from @event in db.Events
                where @event.ID == id
                from adr in @event.Organization.Address
                where adr.Type == AddressType.region
                select new
                {
                    @event,
                    org = @event.Organization.ShortName ?? @event.Organization.FullName,
                    region = adr.Name + " " + adr.Suffix + ".",
                    adr = @event.Organization.FormatAddress,
                    oper = db.Logins.FirstOrDefault(v => v.ID == db.AuthorID),
                    users = from user in @event.Users
                    where user.Roles.Any(v => v.Key == dbroles.consultant)
                    select user,
                    clientTimeZone = @event.Organization.Address
                        .OrderByDescending(v => v.Type)
                        .Select(v => v.TimeZone)
                        .FirstOrDefault(v => v != 0)
                };

            var read = query.FirstOrDefault();

            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            var from = db.Logins.Find(db.AuthorID);
            read?.users.ForEach(user =>
            {
                send_mail(from, user, $"!!! {read.region}, {read.org}", $@"
Добрый день {user.Caption}!

За вами закрепили встречу:
-----------------------------
Дата: {read.@event.Start.ToTZString(read.clientTimeZone)})
Цель: {read.@event.Interests.FirstOrDefault()?.Activity.ActivityValue.Caption}.

Наименование: {read.org}.
Адрес организации: {read.adr}.

Должность: {read.@event.Record.Posts.Select(v => v.Caption).Join(", ")}.
Имя: {read.@event.Record.Persons.Select(v => v.FullName ?? v.RawName).Join(", ")}.
Контакт: {read.@event.Record.Contacts.Select(v => v.ParseValue ?? v.RawValue).Join(", ")}.

Обновивший: {read.oper.Caption}.
-----------------------------
{read.@event.Description}
"
                    .Replace("\r\n", "<br>"));
            });
        }

        /// <summary>
        /// Указать результат встречи
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id"></param>
        public static void set_meeting_result(DBFX db, Guid id)
        {
            var query =
                from @event in db.Events
                where @event.ID == id
                from adr in @event.Organization.Address
                where adr.Type == AddressType.region
                select new
                {
                    @event = @event.Prev,
                    @event.Prev.MeetingResult.Rate,
                    org = @event.Prev.Organization.ShortName ?? @event.Prev.Organization.FullName,
                    region = adr.Name + " " + adr.Suffix + ".",
                    adr = @event.Organization.FormatAddress,
                    oper = db.Logins.FirstOrDefault(v => v.ID == db.AuthorID),
                    users = from user in @event.Prev.Users
                    where user.Roles.Any(v => v.Key == dbroles.consultant)
                    select user,
                    clientTimeZone = @event.Prev.Organization.Address
                        .OrderByDescending(v => v.Type)
                        .Select(v => v.TimeZone)
                        .FirstOrDefault(v => v != 0)
                };

            var read = query.FirstOrDefault();

            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

            var all_answer = read?.Rate.OkAnswer ?? false ? "ДА" : "НЕТ";

            var from = db.Logins.Find(db.AuthorID);
            read?.users.ForEach(user =>
            {
                send_mail(from, user, $"!!! {read.region}, {read.org}", $@"
Добрый день {user.Caption}!

Оценка проведенной Вами встречи:
-----------------------------
Дата: {read.@event.Start.ToTZString(read.clientTimeZone)})
Цель: {read.@event.Interests.FirstOrDefault()?.Activity.ActivityValue.Caption}.

Наименование: {read.org}.
Адрес организации: {read.adr}.

Должность: {read.@event.Record.Posts.Select(v => v.Caption).Join(", ")}.
Имя: {read.@event.Record.Persons.Select(v => v.FullName ?? v.RawName).Join(", ")}.
Контакт: {read.@event.Record.Contacts.Select(v => v.ParseValue ?? v.RawValue).Join(", ")}.

Оценка: {read.Rate.Rating}
Ответил на все вопросы: {all_answer}
Комментарий к оценке: {read.Rate.Description}

Обновивший: {read.oper.Caption}.
-----------------------------
{read.@event.Description}
"
                    .Replace("\r\n", "<br>"));
            });
        }
    }

    public static class fxrDataDB
    {
        /// <summary>
        /// Обработка ошибки сохранения в БД (на почту разработчику)
        /// </summary>
        /// <param name="db"></param>
        /// <param name="error_message"></param>
        public static void TryCommit(
            this DBFX db,
            string error_message = null)
        {
            try
            {
                db.Commit();
            }
            catch (Exception err)
            {
                var id = 1.ToGuid();
                var user = db.Logins.Find(id);
                gdb.send_mail(user,user,"COMMIT ERROR",$@"
{error_message}
{err.ToFinflaexString()}
");
                db.Rollback();
            }
        }
    }
}