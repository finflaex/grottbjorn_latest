﻿#region

using System;
using System.Data.Entity;
using System.Linq;
using BaseDBFX.Models;
using BaseDBFX.Table;
using Finflaex.Support.DB2.Bases;
using Finflaex.Support.MVC;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Base
{
    public class DBFX : fxaDbContext<DBFX>
    {
        private fxaAuth _author;

        private Login _user;

        public DBFX()
            : this(
                //$@"Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True;Initial Catalog={nameof(DBFX)};"
                // $@"Data Source=mars;Integrated Security=False;User ID=sa;Password=411;Database={nameof(DBFX)}_test;"
                //$@"Data Source=mars;Integrated Security=False;User ID=sa;Password=411;Database={nameof(DBFX)};"
                $@"Data Source=kent;Integrated Security=False;User ID=dbfx;Password=dbfx123db;Database={nameof(DBFX)};"
            )
        {
        }

        public DBFX(string connect)
            : base(connect)
        {
        }

        public Login User => _user ?? (_user = Logins.Find(AuthorID));

        public fxaAuth author
        {
            get { return _author; }
            set
            {
                _author = value;
                AuthorID = _author?.Author ?? Guid.Empty;
            }
        }

        public DbSet<Login> Logins { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<ContactRecord> ContactRecords { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Departament> Departaments { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Replace> Replaces { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<HelpActivity> HelpActivities { get; set; }
        public DbSet<OrgStatus> OrgStatuses { get; set; }
        public DbSet<PathFile> PathFiles { get; set; }
        public DbSet<RawFile> Files { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<OrgInterest> OrgInterest { get; set; }
        public DbSet<MeetingYear> MeetingYears { get; set; }
        public DbSet<MeetingPrice> MeetingPrices { get; set; }
        public DbSet<MeetingResult> MeetingResults { get; set; }
        public DbSet<HelpCurrency> HelpCurrency { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<OrgCloseInfo> OrgClosedInfos { get; set; }
        public DbSet<OrgCloseType> OrgCloseTypes { get; set; }
        public DbSet<Okopf> Okopfs { get; set; }
        public DbSet<Fss> Fsses { get; set; }
        public DbSet<FssRegistration> FssRegistrations { get; set; }
        public DbSet<Okved2> Okved2s { get; set; }
        public DbSet<OrgFounder> Founders { get; set; }
        public DbSet<Oksm> Country { get; set; }
        public DbSet<ForeignRegistration> ForeignRegistrations { get; set; }
        public DbSet<ConsultantRate> ConsultantRates { get; set; }

        public DbSet<Label> Labels { get; set; }
        public DbSet<Link> Links { get; set; }

        /// <summary>
        ///     http://andrey.moveax.ru/post/mvc3-in-depth-entity-framework-05-fluent-api
        /// </summary>
        /// <param name="db"></param>
        protected override void OnModelCreating(DbModelBuilder db)
        {
            base.OnModelCreating(db);

            db.Configurations
                .Add(new MapLogin())
                .Add(new MapRoles())
                .Add(new MapOrganization())
                .Add(new MapContactRecord())
                .Add(new MapAddress())
                .Add(new MapOrgStatus())
                .Add(new MapOrgDirection())
                .Add(new MapPathFile())
                .Add(new MapActivity())
                .Add(new MapContract())
                .Add(new MapMeetingYear())
                .Add(new MapMeetingPrice())
                .Add(new MapMeetingResult())
                .Add(new MapCurrency())
                .Add(new MapOrgCloseInfo())
                .Add(new MapOrgCloseType())
                .Add(new MapOkopf())
                .Add(new MapFss())
                .Add(new MapFssRegistration())
                .Add(new MapOkved2())
                .Add(new MapOrgFounder())
                .Add(new MapOksm())
                .Add(new MapForeignRegistration())
                .Add(new MapRawFile())
                .Add(new MapConsultantRate())
                //.Add(new MapHelpTable())
                //.Add(new MapSession())
                //.Add(new MapHelpPost())
                .Add(new MapPost())
                .Add(new MapLabel())
                .Add(new MapLink())
                ;
        }

        public static void Initialize(DBFX db)
        {
            MapOkopf.Initialize(db);
            MapOkved2.Initialize(db);
            MapOksm.Initialize(db);
            MapHelpCurrency.Initialize(db);
            MapPost.Initialize(db);
            MapReplace.Initialize(db);
            MapRoles.Initialize(db);
            MapLogin.Initialize(db);
            MapOrgStatus.Initialize(db);
            MapOrgDirection.Initialize(db);
            MapDepartament.Initialize(db);
            MapAddress.Initialize(db);
            //MapHelpTable.Initialize(db);
            MapLabel.Initialize(db);
        }

        public string Replace(string value)
        {
            try
            {
                var search = value?.Trim(' ', '\r', '\n', '\t').ToLower();
                var read = Replaces.Include(v => v.Parent).FirstOrDefault(v => v.Key == search);
                return read?.Parent != null ? read.Parent.Value : value;
            }
            catch
            {
                return value;
            }
        }

        public void OrgDelete(Guid itemId)
        {
            Organizations.Find(itemId)
                .ActionIfNotNull(organization =>
                {
                    organization.Events.ToArray().ForEach(task => { Delete(task); });
                    organization.Contacts.ToArray()
                        .ForEach(record =>
                        {
                            record.Comments.ToArray().ForEach(comment => { Delete(comment); });
                            record.Contacts.ToArray().ForEach(contact => { Delete(contact); });
                            record.Persons.ToArray()
                                .ForEach(person =>
                                {
                                    Founders.Where(v => v.PersonID == person.ID)
                                        .ToArray()
                                        .ForEach(founder => { Delete(founder); });
                                    Delete(person);
                                });
                            record.Posts.ToArray().ForEach(post => { Delete(post); });
                            Delete(record);
                        });
                    organization.Founders.ToArray().ForEach(founder => { Delete(founder); });
                    Commit();
                    Delete(organization);
                    Commit();
                });
        }

        /// <summary>
        /// Планировалось только для удаления
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public IQueryable<Organization> OrgsMainFiltered(DBFX db)
        {
            var cfg = (
                          from user in db.Logins
                          where user.ID == db.AuthorID
                          from file in user.Files
                          where file.Type == TypeFile.Config
                          where file.Name == Login.Configs.OperatorProgressFilter
                          select file)
                      .FirstOrDefault()
                      ?
                      .Raw.AsObject<OperatorProgressConfig>()
                      ?? new OperatorProgressConfig();

            var user_guids = cfg.Users ?? new Guid[0];
            var addr_guids = cfg.Region ?? new Guid[0];
            var labl_guids = cfg.Labels ?? new Guid[0];
            var not_lbl_guids = cfg.NotLabels ?? new Guid[0];

            var all = !cfg.Users?.Any() ?? true;

            var main_query =
                from auth in db.Logins
                where auth.ID == db.AuthorID
                where all || user_guids.Contains(auth.ID)
                from task in auth.Events
                where !task.Closed
                where task.Start < db.Now
                where task.End == null || task.End > db.Now
                where task.Type == EventType.progress
                select task.Organization;

            var query = main_query
                .Where(v => v.CloseInfo == null);

            var plan_types = new[]
            {
                EventType.callUp, EventType.meeting, EventType.unknown
            };

            switch (cfg.NotPlan)
            {
                case 1:
                    query =
                        from org in query
                        where !(
                                from task in org.Events
                                where plan_types.Contains(task.Type)
                                where !task.Closed
                                where task.Start < db.Now
                                where task.End == null || task.End > db.Now
                                select task)
                            .Any()
                        select org;
                    break;
                case 2:
                    query =
                        from org in query
                        where (
                            from task in org.Events
                            where plan_types.Contains(task.Type)
                            where !task.Closed
                            where task.Start < db.Now
                            where task.End == null || task.End > db.Now
                            select task)
                        .Any()
                        select org;
                    break;
                default:
                    break;
            }

            labl_guids.Any()
                .IfTrue(() =>
                {
                    query =
                        from org in query
                        where labl_guids.Intersect(org.Labels.Select(v => v.ID)).Any()
                        select org;
                });

            not_lbl_guids.Any()
                .IfTrue(() =>
                {
                    query =
                        from org in query
                        where !not_lbl_guids.Intersect(org.Labels.Select(v => v.ID)).Any()
                        select org;
                });

            addr_guids.Any()
                .IfTrue(() =>
                {
                    query =
                        from org in query
                        from addr in org.Address
                        where addr_guids.Contains(addr.ID)
                        select org;
                });

            cfg.Name.IsNullOrWhiteSpace()
                .IfFalse(() =>
                {
                    query =
                        from org in query
                        where org.ShortName.Contains(cfg.Name)
                              || org.FullName.Contains(cfg.Name)
                              || org.Inn.Contains(cfg.Name)
                        select org;
                });


            cfg.Comment.IsNullOrWhiteSpace()
                .IfFalse(() =>
                {
                    query =
                        from org in query
                        where org.Description.Contains(cfg.Comment)
                        select org;
                });


            cfg.Client.IsNullOrWhiteSpace()
                .IfFalse(() =>
                {
                    query =
                        from org in query
                        where org.ClientCode.Contains(cfg.Client)
                        select org;
                });

            cfg.Contact.IsNullOrWhiteSpace()
                .IfFalse(() =>
                {
                    query =
                        from org in query
                        let contacts = org.Contacts.SelectMany(v => v.Contacts)
                            .SelectMany(v => new[]
                            {
                                v.ParseValue,
                                v.RawValue
                            })
                        let persons = org.Contacts.SelectMany(v => v.Persons)
                            .SelectMany(v => new[]
                            {
                                v.FirstName,
                                v.FullName,
                                v.MiddleName,
                                v.RawName,
                                v.SurName,
                                v.Inn
                            })
                        let posts = org.Contacts.SelectMany(v => v.Posts)
                            .SelectMany(v => new[]
                            {
                                v.Caption,
                                v.Description
                            })
                        where contacts
                            .Concat(persons)
                            .Concat(posts)
                            .Where(v => v != null && v != "")
                            .Any(v => v.Contains(cfg.Contact))
                        select org;
                });

            cfg.Okved.IsNullOrWhiteSpace()
                .IfFalse(() =>
                {
                    query =
                        (from org in query
                            from okved in org.ListOkved2
                            where okved.Code.Contains(cfg.Okved)
                                  || okved.Caption.Contains(cfg.Okved)
                            select org)
                        .Concat(from org in query
                            where org.MainOkved2.Code.Contains(cfg.Okved)
                                  || org.MainOkved2.Caption.Contains(cfg.Okved)
                            select org)
                        .Distinct();
                });

            cfg.Forgotten.IfTrue(() =>
            {
                var act_date = db.Now.AddMonths(-2);

                query =
                    from org in query
                    let comment = (
                        from e in org.Events
                        where e.Type == EventType.comment
                        orderby e.DateCreate descending
                        select e.DateCreate
                    )
                    .FirstOrDefault()
                    where comment < act_date
                    select org;
            });

            return query;
        }

        public IQueryable<Organization> OrgsFiltered(DBFX db)
        {
            var cfg = (
                          from user in db.Logins
                          where user.ID == db.AuthorID
                          from file in user.Files
                          where file.Type == TypeFile.Config
                          where file.Name == Login.Configs.OperatorProgressFilter
                          select file)
                      .FirstOrDefault()
                      ?
                      .Raw.AsObject<OperatorProgressConfig>()
                      ?? new OperatorProgressConfig();

            var user_guids = cfg.Users ?? new Guid[0];
            var addr_guids = cfg.Region ?? new Guid[0];
            var labl_guids = cfg.Labels ?? new Guid[0];
            var not_lbl_guids = cfg.NotLabels ?? new Guid[0];

            var all = !cfg.Users?.Any() ?? true;

            var main_query =
                from auth in db.Logins
                where auth.ID == db.AuthorID
                where all || user_guids.Contains(auth.ID)
                from task in auth.Events
                where !task.Closed
                where task.Start < db.Now
                where task.End == null || task.End > db.Now
                where task.Type == EventType.progress
                select task.Organization;

            var roles = new[]
            {
                dbroles.admin,
                dbroles.developer
            };

            var manager_query =
                from auth in db.Logins
                where auth.ID == db.AuthorID
                let manager = auth.Roles.Select(v => v.Key).Contains(dbroles.manager)
                let consultant = auth.Roles.Select(v => v.Key).Contains(dbroles.consultant)
                let expanded = auth.Roles.Select(v => v.Key).Contains(dbroles.expanded)
                let admin = auth.Roles.Select(v => v.Key).Intersect(roles).Any()
                from dep in db.Departaments
                where
                admin || manager &&
                consultant &&
                dep.Parent.Key == Departament.ConsultantDepartament &&
                auth.Departaments.Contains(dep) || manager &&
                !consultant &&
                dep.Key == Departament.OperatorDepartament || cfg.ShowLikvidation &&
                manager &&
                !consultant &&
                dep.Parent.Key == Departament.ConsultantDepartament || expanded &&
                dep.Key == Departament.OperatorDepartament
                from user in dep.Users
                where all || user_guids.Contains(user.ID)
                from task in user.Events
                where !task.Closed
                where task.Start < db.Now
                where task.End == null || task.End > db.Now
                where task.Type == EventType.progress
                select task.Organization;

            var edit_query =
                from auth in db.Logins
                where auth.ID == db.AuthorID
                from task in auth.Events
                where !task.Closed
                where task.Start < db.Now
                where task.End == null || task.End > db.Now
                where task.Type == EventType.edit
                from progress in task.Organization.Events
                where !progress.Closed
                where progress.Start < db.Now
                where progress.End == null || progress.End > db.Now
                where progress.Type == EventType.progress
                from user in progress.Users
                where all || user_guids.Contains(user.ID)
                select task.Organization;

            var not_acc_query =
                from org in db.Organizations
                where
                !(from e in org.Events
                    where !e.Closed
                    where e.Start < db.Now
                    where e.End == null || e.End > db.Now
                    where e.Type == EventType.progress
                    select e).Any()
                select org;

            var query = cfg.NotAcc ? not_acc_query : main_query.Concat(edit_query).Concat(manager_query).Distinct();
            //var query = manager_query;

            query = query.Where(v => cfg.ShowLikvidation ? v.CloseInfo != null : v.CloseInfo == null);

            var plan_types = new[]
            {
                EventType.callUp, EventType.meeting, EventType.unknown
            };

            switch (cfg.NotPlan)
            {
                case 1:
                    query =
                        from org in query
                        where !(
                                from task in org.Events
                                where plan_types.Contains(task.Type)
                                where !task.Closed
                                //where task.Start < db.Now
                                //where task.End > db.Now
                                select task)
                            .Any()
                        select org;
                    break;
                case 2:
                    query =
                        from org in query
                        where (
                            from task in org.Events
                            where plan_types.Contains(task.Type)
                            where !task.Closed
                            //where task.Start < db.Now
                            //where task.End > db.Now
                            select task)
                        .Any()
                        select org;
                    break;
                default:
                    break;
            }

            switch (cfg.Verify)
            {
                case 1:
                    query =
                        from org in query
                        where !org.Work
                        select org;
                    break;
                case 2:
                    query =
                        from org in query
                        where org.Work
                        select org;
                    break;
                default:
                    break;
            }

            labl_guids.Any()
                .IfTrue(() =>
                {
                    query =
                        from org in query
                        where labl_guids.Intersect(org.Labels.Select(v => v.ID)).Any()
                        select org;
                });

            not_lbl_guids.Any()
                .IfTrue(() =>
                {
                    query =
                        from org in query
                        where !not_lbl_guids.Intersect(org.Labels.Select(v => v.ID)).Any()
                        select org;
                });

            addr_guids.Any()
                .IfTrue(() =>
                {
                    query =
                        from org in query
                        from addr in org.Address
                        where addr_guids.Contains(addr.ID)
                        select org;
                });

            cfg.Name.IsNullOrWhiteSpace()
                .IfFalse(() =>
                {
                    cfg.Name = cfg.Name.HtmlDecode().FromHtml().Trim();

                    query =
                        from org in query
                        where org.ShortName.Contains(cfg.Name)
                              || org.FullName.Contains(cfg.Name)
                              || org.Inn.Contains(cfg.Name)
                        select org;
                });


            cfg.Comment.IsNullOrWhiteSpace()
                .IfFalse(() =>
                {
                    cfg.Comment = cfg.Comment.HtmlDecode().FromHtml().Trim();
                    query =
                        from org in query
                        where org.Description.Contains(cfg.Comment)
                        select org;
                });


            cfg.Client.IsNullOrWhiteSpace()
                .IfFalse(() =>
                {
                    cfg.Client = cfg.Client.HtmlDecode().FromHtml().Trim();
                    query =
                        from org in query
                        where org.ClientCode.Contains(cfg.Client)
                        select org;
                });

            cfg.Contact.IsNullOrWhiteSpace()
                .IfFalse(() =>
                {
                    cfg.Contact = cfg.Contact.HtmlDecode().FromHtml().Trim();

                    query =
                        from org in query
                        let contacts = org.Contacts.SelectMany(v => v.Contacts)
                            .SelectMany(v => new[]
                            {
                                v.ParseValue,
                                v.RawValue
                            })
                        let persons = org.Contacts.SelectMany(v => v.Persons)
                            .SelectMany(v => new[]
                            {
                                v.FirstName,
                                v.FullName,
                                v.MiddleName,
                                v.RawName,
                                v.SurName,
                                v.Inn
                            })
                        let posts = org.Contacts.SelectMany(v => v.Posts)
                            .SelectMany(v => new[]
                            {
                                v.Caption,
                                v.Description
                            })
                        where contacts
                            .Concat(persons)
                            .Concat(posts)
                            .Where(v => v != null && v != "")
                            .Where(v => v.Contains(cfg.Contact))
                            .Any()
                        select org;

                    //query =
                    //    from org in query
                    //    from record in org.Contacts
                    //    from contact in record.Contacts
                    //    from person in record.Persons
                    //    from post in record.Posts
                    //    where contact.RawValue.Contains(cfg.Contact)
                    //          || contact.ParseValue.Contains(cfg.Contact)
                    //          || person.FullName.Contains(cfg.Contact)
                    //          || person.RawName.Contains(cfg.Contact)
                    //          || person.FirstName.Contains(cfg.Contact)
                    //          || person.MiddleName.Contains(cfg.Contact)
                    //          || person.SurName.Contains(cfg.Contact)
                    //          || post.Caption.Contains(cfg.Contact)
                    //    select org;
                });

            cfg.Okved.IsNullOrWhiteSpace()
                .IfFalse(() =>
                {
                    cfg.Okved = cfg.Okved.HtmlDecode().FromHtml().Trim();
                    query =
                        (from org in query
                            from okved in org.ListOkved2
                            where okved.Code.Contains(cfg.Okved)
                                  || okved.Caption.Contains(cfg.Okved)
                            select org)
                        .Concat(from org in query
                            where org.MainOkved2.Code.Contains(cfg.Okved)
                                  || org.MainOkved2.Caption.Contains(cfg.Okved)
                            select org)
                        .Distinct();
                });

            cfg.Forgotten.IfTrue(() =>
            {
                var act_date = db.Now.AddMonths(-2);

                query =
                    from org in query
                    let comment = (
                        from e in org.Events
                        where e.Type == EventType.comment
                        orderby e.DateCreate descending
                        select e.DateCreate
                    )
                    .FirstOrDefault()
                    where comment < act_date
                    select org;
            });

            return query;
        }
    }
}