﻿#region

using System;
using System.Linq;
using BaseDBFX.Base;
using BaseDBFX.Table;

#endregion

namespace BaseDBFX.Methods
{
    public class OrganizationEdit
    {
        private readonly ActionData gb;

        public OrganizationEdit(ActionData actionGb)
        {
            gb = actionGb;
        }

        public Organization create(
            string event_message,
            Action<Organization> action)
        {
            // запрос необходимых данных из базы
            var read = (
                    from user in gb.db.Logins
                    where user.ID == gb.AuthorGuid
                    select new
                    {
                        user,
                        is_operator
                        = user.Roles.Any(r => r.Key == dbroles.@operator)
                          && user.Roles.All(r => r.Key != dbroles.consultant),
                        is_consultant
                        = user.Roles.Any(r => r.Key == dbroles.@operator)
                          && user.Roles.Any(r => r.Key == dbroles.consultant)
                    })
                .First();


            // создаем объект
            var result = new Organization();

            // создаем событие принадлежности
            var @event = new Event
            {
                Type = EventType.progress,
                Description = event_message,
                Start = gb.Now.DateTime,
                End = DateTime.MaxValue
            };

            // Назначем ответственным текущего пользователя
            @event.Users.Add(read.user);

            // добавляем событие организации
            result.Events.Add(@event);

            // задаем свойства объекта
            action?.Invoke(result);

            // сохраняем объект в базу
            result = gb.db.Add(result);

            // применяем изменения
            gb.Save();

            // возвращаем созданный объект
            return result;
        }

        public bool delete(
            Guid? id)
        {
            // ищем объект в базе
            var read = gb.db.Organizations.Find(id);

            // если объект не найден 
            if (read == null)
                // возвращает отрицательный результат
                return false;

            // удаляем объект из базы
            gb.db.Delete(read);

            // сохраняем изменения
            gb.Save();

            // возвращаем утвердительный результат
            return true;
        }
    }
}