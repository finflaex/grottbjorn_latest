﻿#region

using System;
using System.Linq;
using BaseDBFX.Base;

#endregion

namespace BaseDBFX.Methods
{
    public class OrganizationCard
    {
        private readonly ActionData gb;

        public OrganizationCard(ActionData actionGb)
        {
            gb = actionGb;
        }

        /// <summary>
        ///     Возвращаем данные для указанной организации
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Возможен NULL</returns>
        public ShowOrgModel show(Guid? id)
        {
            var query =
                from org in gb.db.Organizations
                where org.ID == id
                select new ShowOrgModel
                {
                    OrgName = org.ShortName ?? org.FullName,
                    IsClosed = org.CloseInfo != null,
                    IsVerify = org.WorkID != null && org.WorkID != string.Empty,
                };

            var read = query.FirstOrDefault();

            return read;
        }
    }

    /// <summary>
    ///     данные организации
    /// </summary>
    public class ShowOrgModel
    {
        public string OrgName { get; set; }
        public bool IsClosed { get; set; }
        public bool IsVerify { get; set; }
    }
}