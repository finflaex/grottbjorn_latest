﻿#region

using System;
using BaseDBFX.Base;
using Finflaex.Support.Reflection;
using System.Linq;
using BaseDBFX.Table;
using NUnit.Framework;

#endregion

namespace BaseDBFX.Methods
{
    [TestFixture]
    public class TestOrganization
    {
        private ActionData data;
        private bool old_flag_db_edit;

        [OneTimeSetUp]
        public void BeforeAllTests()
        {
            //old_flag_db_edit = DBFX.debug;
            //DBFX.debug = true;

            data = new ActionData();
        }

        [Test]
        public void TestOrgEdit()
        {
            var id = Guid.NewGuid();

            var org = data.orgedit.create("создана автоматическим тестом",edit =>
            {
                edit.ID = id;
            });

            Assert.NotNull(org, "организация не создана");

            var base_org = data.db.Organizations.Find(id);

            Assert.NotNull(base_org, "организации нет в базе");

            var event_progress = base_org.Events.FirstOrDefault(v => v.Type == EventType.progress);

            Assert.NotNull(event_progress, "событие привязки не найдено");

            if (data.AuthorGuid == Guid.Empty)
            {
                Assert.Null(event_progress.End, "пользователь неизвестен");
            }

            var flag = data.orgedit.delete(id);

            Assert.True(flag, "Организация не удалена");
        }

        [OneTimeTearDown]
        public void AfterAllTest()
        {
            //DBFX.debug = old_flag_db_edit;
        }
    }
}