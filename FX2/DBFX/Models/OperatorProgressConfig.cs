﻿#region

using System;
using System.Collections.Generic;
using BaseDBFX.Table;

#endregion

namespace BaseDBFX.Models
{
    [Serializable]
    public class SeminarInviteConfig
    {
        public IEnumerable<Guid> Contains { get; set; }
        public IEnumerable<Guid> NotContains { get; set; }
    }

    [Serializable]
    public class SeminarListConfig
    {
        public IEnumerable<Guid> Departaments { get; set; }
        public bool Close { get; set; }
    }

    [Serializable]
    public class OperatorProgressConfig
    {
        public string Name { get; set; }
        public string Comment { get; set; }
        public string Client { get; set; }
        public IEnumerable<Guid> Region { get; set; }
        public IEnumerable<Guid> Users { get; set; }
        public IEnumerable<Guid> Labels { get; set; }
        public IEnumerable<Guid> NotLabels { get; set; }
        public int NotPlan { get; set; }
        public bool NotAcc { get; set; }
        public string Contact { get; set; }
        public string Okved { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public bool ShowLikvidation { get; set; }

        public string PersistState { get; set; }
        public bool AutoSave { get; set; }
        public int Verify { get; set; }
        public bool Forgotten { get; set; }
    }

    [Serializable]
    public class ConsultantMeetingConfig
    {
        public ConsultantMeetingConfig()
        {
            Start = DateTimeOffset.UtcNow.Date;
            End = DateTimeOffset.UtcNow.Date;
        }

        public string Name { get; set; }

        public IEnumerable<Guid> Region { get; set; }
        public IEnumerable<Guid> Users { get; set; }
        public IEnumerable<Guid> Departaments { get; set; }
        public IEnumerable<EventType> Types { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public bool AutoSave { get; set; }
        public string PersistState { get; set; }
        public bool OtherSeminar { get; set; }
    }

    [Serializable]
    public class ConsultantEventConfig
    {
        public bool ShowAll { get; set; }
    }

    [Serializable]
    public class OperatorPlanConfig
    {
        public Guid Accaunt { get; set; }
        public IEnumerable<EventType> Types { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}