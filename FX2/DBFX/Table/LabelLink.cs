﻿#region

using System;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class LabelLink : fxaGuidTable
    {
        public Label Label { get; set; }
        public Guid LabelID { get; set; }
    }
}