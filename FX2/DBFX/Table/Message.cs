﻿#region

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class Message : fxaGuidTable,
        fxiCaptionTable,
        fxiUpdateAuthorIdTable,
        fxiDateCreateTable,
        fxiDescriptionTable
    {
        public Guid? TargetID { get; set; }

        public bool View { get; set; }
        public bool History { get; set; }

        [ForeignKey(nameof(UpdateAuthorID))]
        public Login Author { get; set; }

        [ForeignKey(nameof(TargetID))]
        public Login Target { get; set; }

        public string AuthorService { get; set; }
        public string Caption { get; set; }
        public DateTimeOffset DateCreate { get; set; }
        public string Description { get; set; }
        public Guid? UpdateAuthorID { get; set; }
    }

    public class MapMessage : EntityTypeConfiguration<Message>
    {
        public MapMessage()
        {
            HasOptional(v => v.Author).WithMany(v => v.SentMessages).HasForeignKey(v => v.UpdateAuthorID);
            HasOptional(v => v.Target).WithMany(v => v.RecivedMessages).HasForeignKey(v => v.UpdateAuthorID);
        }
    }
}