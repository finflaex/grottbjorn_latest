﻿#region

using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using BaseDBFX.Base;
using Finflaex.Support.ApiOnline.MosRU;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Table
{
    public class Oksm : fxaIntTable
    {
        public Oksm()
        {
            ForeignRegistrations = new HashSet<ForeignRegistration>();
        }

        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string Code { get; set; }
        public string ALFA2 { get; set; }
        public string ALFA3 { get; set; }
        public virtual ICollection<ForeignRegistration> ForeignRegistrations { get; set; }
        public HelpCurrency Currency { get; set; }
    }

    public class MapOksm : EntityTypeConfiguration<Oksm>
    {
        public MapOksm()
        {
            HasOptional(v => v.Currency)
                .WithOptionalPrincipal(v => v.Country)
                .WillCascadeOnDelete(false);

            HasMany(v => v.ForeignRegistrations)
                .WithOptional(v => v.Country)
                .HasForeignKey(v => v.CountryID)
                .WillCascadeOnDelete(false);
        }

        public static void Initialize(DBFX db)
        {
            ApiMosRu
                .Countries
                .Select(v => v.Cells)
                .Select(v => new Oksm
                {
                    ID = v.global_id,
                    FullName = v.FULLNAME,
                    ShortName = v.SHORTNAME,
                    Code = v.CODE,
                    ALFA2 = v.ALFA2,
                    ALFA3 = v.ALFA3
                })
                .ForEach(
                    read =>
                    {
                        db.Country.Any(v => v.Code == read.Code).IfFalse(() => { db.Country.AddOrUpdate(read); });
                    });
            db.SaveChanges();
        }
    }
}