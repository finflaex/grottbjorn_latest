﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class MeetingYear : fxaGuidTable
    {
        public MeetingYear()
        {
            Prices = new List<MeetingPrice>();
        }

        public decimal Value { get; set; }
        public virtual ICollection<MeetingPrice> Prices { get; set; }
        public virtual Departament Departament { get; set; }
        public Guid DepartamentID { get; set; }
    }

    public class MapMeetingYear : EntityTypeConfiguration<MeetingYear>
    {
        public MapMeetingYear()
        {
            HasRequired(v => v.Departament)
                .WithMany(v => v.Prices)
                .HasForeignKey(v => v.DepartamentID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Prices)
                .WithRequired(v => v.Year)
                .HasForeignKey(v => v.YearID)
                .WillCascadeOnDelete(true);
        }
    }
}