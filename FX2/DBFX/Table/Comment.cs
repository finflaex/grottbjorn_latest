﻿#region

using System;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class Comment : fxaGuidTable,
        fxiCaptionTable,
        fxiDescriptionTable,
        fxiDateCreateTable,
        fxiUpdateAuthorIdTable
    {
        public CommentType Type { get; set; }
        public Guid? RecordID { get; set; }
        public Guid? OrganizationID { get; set; }

        public virtual Login Author { get; set; }
        public virtual ContactRecord Record { get; set; }
        public virtual Organization Organization { get; set; }
        public string Caption { get; set; }


        public DateTimeOffset DateCreate { get; set; }
        public string Description { get; set; }
        public Guid? UpdateAuthorID { get; set; }
    }

    public class MapComment : EntityTypeConfiguration<Comment>
    {
        public MapComment()
        {
            HasOptional(v => v.Record).WithMany(v => v.Comments).HasForeignKey(v => v.RecordID);
            HasOptional(v => v.Author).WithMany(v => v.MyComments).HasForeignKey(v => v.UpdateAuthorID);
            HasOptional(v => v.Organization).WithMany(v => v.Comments).HasForeignKey(v => v.OrganizationID);
        }
    }

    public enum CommentType
    {
        unknown = 0,
        history = 1,
        comment = 2
    }
}