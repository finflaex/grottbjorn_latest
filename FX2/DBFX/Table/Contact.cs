﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.Attributes;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.Phone;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Table
{
    public class Contact : fxaGuidTable
    {
        public Contact()
        {
            Type = ContactType.none;
        }

        public Guid? RecordID { get; set; }
        public virtual ContactRecord Record { get; set; }

        public ContactType Type { get; set; }

        public string RawValue { get; set; }
        public string ParseValue { get; set; }

        [NotMapped]
        public string Value
        {
            get
            {
                return Type == ContactType.none
                    ? null
                    : Type == ContactType.unknown
                        ? RawValue
                        : ParseValue;
            }
            set
            {
                value = value.retIfNullOrWhiteSpace().Trim();



                //var code_out = new[]
                //{
                //    "09",
                //    "10",
                //    "19",
                //    "29",
                //    "99"
                //};

                //var code_country = new[]
                //{
                //    "358",
                //    "373",
                //    "374",
                //    "375",
                //    "380",
                //    "383",
                //    "770",
                //    "771",
                //    "772",
                //    "777",
                //    "983",
                //    "984",
                //    "987",
                //    "988",
                //    "992",
                //    "993",
                //    "994",
                //    "996",
                //    "998",
                //    "989"
                //};

                //var phone_regex_country_pattern = $@"^9{{0,2}}?8?({code_out.Join("|")})?({code_country.Join("|")})(\d{{6,10}})$";

                //var phone_regex_pattern = @"^9?(8|7)?(\d{3})?(\d{7})$";

                var mail_regex_pattern = @"([A-Za-z0-9._%+-]+)@([A-Za-z0-9.-]+)\.([A-Za-z]{2,6})"
                    .Prepend(@"[\r\n\t\W]*")
                    .Append(@"[\r\n\t\W]*")
                    .Prepend("^")
                    .Append("$");

                //var clear = value.ClearExcept("0123456789");

                ////var main_phone = clear.DoRegexIsMatch(phone_regex_pattern);
                ////var country_phone = clear.DoRegexIsMatch(phone_regex_country_pattern);

                ////if (main_phone || country_phone)
                ////{
                ////    if ()
                ////}

                false
                    .Case(value.DoRegexIsMatch(mail_regex_pattern), () =>
                    {
                        Type = ContactType.email;
                        ParseValue = mail_regex_pattern.ToRegex().Replace(value, "$1@$2.$3");
                        RawValue = value;
                    })
                    .Case(value.IsPhoneNumber(), () =>
                    {
                        Type = ContactType.phone;
                        ParseValue = value.PhoneParse();
                        RawValue = value;
                    })
                    //.Case(clear.DoRegexIsMatch(phone_regex_pattern), () =>
                    //{
                    //    Type = ContactType.phone;
                    //    var matches = phone_regex_pattern.ToRegex().Matches(clear);
                    //    RawValue = ParseValue = $"{matches[0].Groups[2].Value.retIfNullOrWhiteSpace("343")}{matches[0].Groups[3].Value}";
                    //})
                    //.Case(clear.DoRegexIsMatch(phone_regex_country_pattern), () =>
                    //{
                    //    Type = ContactType.phone;
                    //    var matches = phone_regex_country_pattern.ToRegex().Matches(clear);
                    //    RawValue = ParseValue = $"{matches[0].Groups[2].Value.retIfNullOrWhiteSpace("343")}{matches[0].Groups[3].Value}";
                    //    RawValue = ParseValue = phone_regex_country_pattern.ToRegex().Replace(clear, "$2$3");
                    //})
                    .IfFalse(() =>
                    {
                        Type = ContactType.unknown;
                        RawValue = value.Trim('\r', '\n', '\t', ' ');
                    });
            }
        }

        public virtual ICollection<Event> Events { get; set; }
    }

    public class MapContact : EntityTypeConfiguration<Contact>
    {
        public MapContact()
        {
            HasOptional(v => v.Record)
                .WithMany(v => v.Contacts)
                .HasForeignKey(v => v.RecordID);

            HasMany(v => v.Events)
                .WithOptional(v => v.Contact)
                .HasForeignKey(v => v.ContactID)
                .WillCascadeOnDelete(false);
        }
    }

    public enum ContactType
    {
        [fatValue("Не контакт")] none = -1,
        [fatValue("Неизвестно")] unknown = 0,
        [fatValue("Телефон")] phone = 1,
        [fatValue("Почта")] email = 2
    }
}