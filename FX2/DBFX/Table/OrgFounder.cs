﻿#region

using System;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class OrgFounder : fxaGuidTable
    {
        public string WorkID { get; set; }
        public Guid? OrganizationID { get; set; }
        public virtual Organization Organization { get; set; }

        public Guid TargetID { get; set; }
        public Organization Target { get; set; }

        public Guid? PersonID { get; set; }
        public virtual ContactRecord Person { get; set; }
        public decimal Price { get; set; }
        public bool OwnerRussia { get; set; }
    }

    public class MapOrgFounder : EntityTypeConfiguration<OrgFounder>
    {
        public MapOrgFounder()
        {
            HasRequired(v => v.Target)
                .WithMany(v => v.Founders)
                .HasForeignKey(v => v.TargetID)
                .WillCascadeOnDelete(false);

            HasOptional(v => v.Organization)
                .WithMany(v => v.Establisheds)
                .HasForeignKey(v => v.OrganizationID)
                .WillCascadeOnDelete(true);

            HasOptional(v => v.Person)
                .WithMany(v => v.Establisheds)
                .HasForeignKey(v => v.PersonID)
                .WillCascadeOnDelete(false);
        }
    }
}