﻿#region

using System;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.Attributes;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class Link : fxaGuidTable
        , fxiDescriptionTable
    {
        public Guid? FirstID { get; set; }
        public Organization First { get; set; }

        public Guid? SecondID { get; set; }
        public Organization Second { get; set; }
        public TypeLink Type { get; set; }

        public string Description { get; set; }
    }

    public enum TypeLink
    {
        unknown = 0,
        [fatValue("Вышестоящая")] up = 1,
        [fatValue("Сестринская")] link = 2,
        [fatValue("Нижестоящая")] down = 3,
        [fatValue("Партнер")] other = 4
    }

    public class MapLink : EntityTypeConfiguration<Link>
    {
        public MapLink()
        {
            HasOptional(v => v.First)
                .WithMany(v => v.LinksChilds)
                .HasForeignKey(v => v.FirstID)
                .WillCascadeOnDelete(false);

            HasOptional(v => v.Second)
                .WithMany(v => v.LinksParents)
                .HasForeignKey(v => v.SecondID)
                .WillCascadeOnDelete(false);
        }
    }
}