﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class MeetingResult : fxaGuidTable
        , fxiDateCreateTable
        , fxiCreateAuthorIdTable
        , fxiUpdateAuthorIdTable
        , fxiDateModifyTable
        , fxiDescriptionTable
    {
        public MeetingResult()
        {
            Consultants = new HashSet<Login>();
        }

        public Guid? OperatorID { get; set; }

        public Event Event { get; set; }

        public int Distance { get; set; }
        public decimal Cost { get; set; }
        public bool LPR { get; set; }
        public decimal Bonus { get; set; }
        //public decimal BonusComment { get; set; }

        //public DateTime Date { get; set; }
        public virtual ICollection<Login> Consultants { get; set; }
        public virtual Login Operator { get; set; }
        public virtual ContactRecord Record { get; set; }
        public Guid? RecordID { get; set; }
        public virtual ConsultantRate Rate { get; set; }
        public Guid? CreateAuthorID { get; set; }

        public DateTimeOffset DateCreate { get; set; }

        public DateTimeOffset DateModify { get; set; }
        public string Description { get; set; }
        public Guid? UpdateAuthorID { get; set; }
    }

    public class MapMeetingResult : EntityTypeConfiguration<MeetingResult>
    {
        public MapMeetingResult()
        {
            HasRequired(v => v.Event)
                .WithOptional(v => v.MeetingResult)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Consultants)
                .WithMany(v => v.ConsultantMeetingResults);

            HasOptional(v => v.Record)
                .WithMany(v => v.MeetingResults)
                .HasForeignKey(v => v.RecordID)
                .WillCascadeOnDelete(false);

            HasOptional(v => v.Operator)
                .WithMany(v => v.OperatorMeetingResults)
                .HasForeignKey(v => v.OperatorID)
                .WillCascadeOnDelete(false);

            HasOptional(v => v.Rate)
                .WithRequired(v => v.Result)
                .WillCascadeOnDelete(true);
        }
    }
}