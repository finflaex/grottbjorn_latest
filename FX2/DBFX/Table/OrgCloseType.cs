﻿#region

using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class OrgCloseType : fxaIntTable
    {
        public OrgCloseType()
        {
            OrgClosed = new HashSet<OrgCloseInfo>();
        }

        public string FullName { get; set; }
        public string ShortName { get; set; }
        public int Code { get; set; }
        public virtual ICollection<OrgCloseInfo> OrgClosed { get; set; }
    }

    public class MapOrgCloseType : EntityTypeConfiguration<OrgCloseType>
    {
        public MapOrgCloseType()
        {
            HasMany(v => v.OrgClosed)
                .WithRequired(v => v.Reason)
                .HasForeignKey(v => v.ReasonID)
                .WillCascadeOnDelete(true);
        }
    }
}