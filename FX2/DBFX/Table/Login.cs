﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using BaseDBFX.Base;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Table
{
    public class Login : fxaGuidTable
    {
        public Login()
        {
            Roles = new HashSet<Role>();
            MyComments = new HashSet<Comment>();
            Departaments = new HashSet<Departament>();
            Events = new HashSet<Event>();
            ConsultantMeetingResults = new HashSet<MeetingResult>();
            Labels = new HashSet<Label>();
        }

        public string Caption { get; set; }
        public string Name { get; set; }
        public int OldID { get; set; }

        public int TimeZone { get; set; }
        public bool TimeClient { get; set; }

        [NotMapped]
        public string Password
        {
            get
            {
                if (RawPassword != null && RawPassword.Any())
                    return RawPassword.Decrypt().ToString(Encoding.UTF8);
                return string.Empty;
            }
            set
            {
                if (value.IsNullOrWhiteSpace())
                    RawPassword = string.Empty.ToBytes(Encoding.UTF8).Encrypt();
                else
                    RawPassword = value.ToBytes(Encoding.UTF8).Encrypt();
            }
        }

        [Column("Password")]
        public byte[] RawPassword { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<Comment> MyComments { get; set; }
        public virtual ICollection<Departament> Departaments { get; set; }
        public virtual ICollection<Message> SentMessages { get; set; }
        public virtual ICollection<Message> RecivedMessages { get; set; }
        public virtual ICollection<Event> Events { get; set; }

        public int InternalBaseID { get; set; }
        public string InternalPhoneNumber { get; set; }
        public string InternalEMailAddress { get; set; }
        public virtual ICollection<MeetingResult> ConsultantMeetingResults { get; set; }
        public virtual ICollection<MeetingResult> OperatorMeetingResults { get; set; }
        public virtual ICollection<RawFile> Files { get; set; }
        public virtual ICollection<Label> Labels { get; set; }

        public static class Configs
        {
            public static string OperatorProgressFilter => "OperatorProgressFilter";
            public static string OperatorProgressOpenFilter => "OperatorProgressOpenFilter";
            public static string OperatorPlanConfig => "OperatorPlanConfig";
            public static string ConsultantMeetingConfig => "ConsultantMeetingConfig";
            public static string ConsultantMeetingOpenConfig => "ConsultantMeetingOpenConfig";

            public static string ConsultantEventConfig => "ConsultantEventConfig";


            public static string SeminarListConfig => "SeminarListConfig";
            public static string SeminarInviteConfig => "SeminarInviteConfig";
        }
    }

    public class MapLogin : EntityTypeConfiguration<Login>
    {
        public MapLogin()
        {
            HasMany(v => v.Roles)
                .WithMany(v => v.Logins);

            HasMany(v => v.MyComments)
                .WithOptional(v => v.Author)
                .HasForeignKey(v => v.UpdateAuthorID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Departaments)
                .WithMany(v => v.Users);

            HasMany(v => v.SentMessages)
                .WithOptional(v => v.Author)
                .HasForeignKey(v => v.UpdateAuthorID)
                .WillCascadeOnDelete(false);

            HasMany(v => v.RecivedMessages)
                .WithOptional(v => v.Target)
                .HasForeignKey(v => v.TargetID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Events)
                .WithMany(v => v.Users);

            HasMany(v => v.ConsultantMeetingResults)
                .WithMany(v => v.Consultants);

            HasMany(v => v.OperatorMeetingResults)
                .WithOptional(v => v.Operator)
                .HasForeignKey(v => v.OperatorID)
                .WillCascadeOnDelete(false);

            HasMany(v => v.Files)
                .WithOptional(v => v.User)
                .HasForeignKey(v => v.UserID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Labels)
                .WithOptional(v => v.User)
                .HasForeignKey(v => v.UserID)
                .WillCascadeOnDelete(true);
        }

        public static void Initialize(DBFX db)
        {
            db.Logins.AddOrUpdate(new Login
            {
                ID = Guid.Empty,
                Caption = "Неизвестный"
            });

            var developer_roles = new[] {dbroles.developer, dbroles.admin};

            var roles = db.Roles.Where(v => developer_roles.Contains(v.Key)).ToList();

            db.Logins.AddOrUpdate(new Login
            {
                ID = 1.ToGuid(),
                Caption = "Разработчик",
                Name = "developer",
                Password = "developer",
                Roles = roles
            });

            db.Commit();
        }
    }
}