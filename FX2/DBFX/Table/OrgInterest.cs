﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.Attributes;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class OrgInterest : fxaGuidTable
        , fxiUpdateAuthorIdTable
        , fxiDateCreateTable
        , fxiDateModifyTable
        , fxiDescriptionTable
    {
        public OrgInterest()
        {
            Events = new HashSet<Event>();
        }

        public Guid OrgID { get; set; }

        [ForeignKey(nameof(OrgID))]
        public virtual Organization Organization { get; set; }

        public InterestType Type { get; set; }


        public Guid ActivityID { get; set; }

        [ForeignKey(nameof(ActivityID))]
        public virtual Activity Activity { get; set; }

        public virtual ICollection<Event> Events { get; set; }

        public Guid? AuthorID { get; set; }
        public DateTimeOffset DateCreate { get; set; }
        public DateTimeOffset DateModify { get; set; }
        public string Description { get; set; }
        public Guid? UpdateAuthorID { get; set; }
    }

    public enum InterestType
    {
        [fatValue("Неизвестно")] unknown = 0,
        [fatValue("Интересует")] interested = 1,
        [fatValue("Не интересует")] notinterested = 2,
        [fatValue("Договор")] conducted = 3
    }

    public class MapContract : EntityTypeConfiguration<OrgInterest>
    {
        public MapContract()
        {
            HasMany(v => v.Events)
                .WithMany(v => v.Interests);

            HasRequired(v => v.Activity)
                .WithMany(v => v.OrgInterests)
                .HasForeignKey(v => v.ActivityID)
                .WillCascadeOnDelete(true);

            HasRequired(v => v.Organization)
                .WithMany(v => v.Interests)
                .HasForeignKey(v => v.OrgID)
                .WillCascadeOnDelete(true);
        }
    }
}