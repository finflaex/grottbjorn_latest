﻿#region

using System;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class ForeignRegistration : fxaGuidTable
    {
        public long? CountryID { get; set; }
        public Oksm Country { get; set; }
        public DateTime? Date { get; set; }
        public string Address { get; set; }
        public string Number { get; set; }
        public Organization Organization { get; set; }
    }

    public class MapForeignRegistration : EntityTypeConfiguration<ForeignRegistration>
    {
        public MapForeignRegistration()
        {
            HasOptional(v => v.Country)
                .WithMany(v => v.ForeignRegistrations)
                .HasForeignKey(v => v.CountryID)
                .WillCascadeOnDelete(false);

            HasRequired(v => v.Organization)
                .WithOptional(v => v.ForeignRegistration)
                .WillCascadeOnDelete(true);
        }
    }
}