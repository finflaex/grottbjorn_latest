﻿#region

using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using BaseDBFX.Base;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class Okved2 : fxaIntTable
        , fxiDescriptionTable
        , fxiCaptionTable
    {
        public Okved2()
        {
            MainOrganizations = new HashSet<Organization>();
            Organizations = new HashSet<Organization>();
        }

        public string Razdel { get; set; }
        public string Code { get; set; }
        public virtual ICollection<Organization> MainOrganizations { get; set; }
        public virtual ICollection<Organization> Organizations { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
    }

    public class MapOkved2 : EntityTypeConfiguration<Okved2>
    {
        public MapOkved2()
        {
            HasMany(v => v.MainOrganizations)
                .WithOptional(v => v.MainOkved2)
                .WillCascadeOnDelete(false);

            HasMany(v => v.Organizations)
                .WithMany(v => v.ListOkved2);
        }

        public static void Initialize(DBFX db)
        {
            //ApiMosRu
            //    .Okved2s
            //    .Select(v => v.Cells)
            //    .Select(v => new Okved2
            //    {
            //        ID = v.global_id,
            //        Description = v.Nomdescr,
            //        Caption = v.Name,
            //        Razdel = v.Razdel,
            //        Code = v.Kod,
            //    })
            //    .ForEach(read =>
            //    {
            //        db.Okved2s.Any(v => v.Code == read.Code).IfFalse(() =>
            //        {
            //            db.Okved2s.AddOrUpdate(read);
            //        });
            //    });
            //db.SaveChanges();
        }
    }
}