﻿#region

using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using BaseDBFX.Base;
using Finflaex.Support.ApiOnline.MosRU;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Table
{
    public class HelpCurrency : fxaKeyTable
        , fxiOrderTable
    {
        public string NumCode { get; set; }
        public string Name { get; set; }
        public Oksm Country { get; set; }
        public virtual ICollection<Currency> OrgCurrency { get; set; }
        public int Order { get; set; }
    }

    public class MapHelpCurrency : EntityTypeConfiguration<HelpCurrency>
    {
        public MapHelpCurrency()
        {
            HasOptional(v => v.Country)
                .WithOptionalDependent(v => v.Currency)
                .WillCascadeOnDelete(false);

            HasMany(v => v.OrgCurrency)
                .WithRequired(v => v.CurrencyValue)
                .HasForeignKey(v => v.CurrencyKey)
                .WillCascadeOnDelete(false);
        }

        public static void Initialize(DBFX db)
        {
            var countries = db.Country.ToArray();

            ApiMosRu
                .Currency
                .Select(v => v.Cells)
                .Select(v => new HelpCurrency
                {
                    Key = v.STRCODE,
                    Name = v.NAME,
                    Country =
                        countries.FirstOrDefault(
                            c =>
                                c.ShortName != null && c.ShortName.Contains(v.COUNTRY) ||
                                c.FullName != null && c.FullName.Contains(v.COUNTRY)),
                    NumCode = v.CODE
                })
                .ForEach(read =>
                {
                    var old = db.HelpCurrency.Find(read.Key);

                    if (old == null)
                    {
                        db.HelpCurrency.Add(read);
                    }
                    else
                    {
                        old.Name = read.Name;
                        old.NumCode = read.NumCode;
                        read.Country.ActionIfNotNull(() => { old.Country = read.Country; });
                    }
                });

            db.SaveChanges();

            //var read = fxsCBR
            //    .CursCurrencyDaily()
            //    .ToJson()
            //    .FromJson<fxcDictSO>();

            //read.Select(v => v.Key)
            //    .Select(key => read.GetJObjectToObject<ValuteItem>(key))
            //    .ForEach(item =>
            //    {
            //        var @new = new HelpCurrency
            //        {
            //            CharCode = item.CharCode,
            //            Key = item.ID,
            //            Name = item.Name,
            //            NumCode = item.NumCode,
            //        };
            //        var old = db.HelpCurrency.FirstOrDefault(v => v.Key == @new.Key);
            //        if (old == null)
            //        {
            //            db.Add(@new);
            //        }
            //        else
            //        {
            //            old.CharCode = @new.CharCode;
            //            old.Name = @new.Name;
            //            old.NumCode = @new.NumCode;
            //        }
            //    });

            //db.Commit();
        }
    }
}