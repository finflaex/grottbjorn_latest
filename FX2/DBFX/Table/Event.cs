﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.Attributes;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class Event : fxaGuidTable,
        fxiUpdateAuthorIdTable,
        fxiCreateAuthorIdTable,
        fxiDateCreateTable,
        fxiDateModifyTable,
        fxiCaptionTable,
        fxiDescriptionTable,
        fxiOrderTable
    {
        public Event()
        {
            Users = new HashSet<Login>();
            Departaments = new HashSet<Departament>();
            PathFiles = new HashSet<PathFile>();
            Interests = new HashSet<OrgInterest>();
            Next = new HashSet<Event>();
            Address = new HashSet<Address>();
            Activities = new HashSet<HelpActivity>();
            Files = new HashSet<RawFile>();
        }

        public virtual ContactRecord Record { get; set; }
        public Guid? RecordID { get; set; }
        public virtual Organization Organization { get; set; }
        public Guid? OrgID { get; set; }

        public bool IsAllDay { get; set; }
        public DateTimeOffset? Notify { get; set; }
        public bool NotifyConsultant { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public virtual ICollection<Login> Users { get; set; }
        public virtual ICollection<Departament> Departaments { get; set; }
        public EventType Type { get; set; }
        public bool Closed { get; set; }
        public virtual ICollection<PathFile> PathFiles { get; set; }
        public virtual ICollection<OrgInterest> Interests { get; set; }
        public virtual MeetingResult MeetingResult { get; set; }
        public virtual Event Prev { get; set; }
        public virtual ICollection<Event> Next { get; set; }
        public virtual ICollection<Address> Address { get; set; }
        public virtual ICollection<HelpActivity> Activities { get; set; }
        public virtual ICollection<RawFile> Files { get; set; }
        public virtual Contact Contact { get; set; }
        public Guid? ContactID { get; set; }
        public string Caption { get; set; }

        public string Description { get; set; }
        public int Order { get; set; }

        #region System

        public Guid? UpdateAuthorID { get; set; }
        public Guid? CreateAuthorID { get; set; }
        public DateTimeOffset DateCreate { get; set; }
        public DateTimeOffset DateModify { get; set; }

        #endregion
    }

    public enum EventType
    {
        [fatValue("Все")] all = -1,

        [fatValue("Другое")] unknown = 0,
        [fatValue("Звонок")] call = 10,
        [fatValue("Позвонить")] callUp = 11,
        [fatValue("Добавленные")] callTransfer = 12,
        [fatValue("Семинар")] callSeminar = 13,

        [fatValue("Назначена встреча")] meeting = 20,
        [fatValue("Подтвержденная встреча")] meetingConfirm = 21,
        [fatValue("Отмененная встреча")] meetingCancel = 22,
        [fatValue("Закрытая встреча")] meetingClose = 23,
        [fatValue("Контроль качества")] meetingRate = 24,

        [fatValue("Обработка")] progress = 4,
        [fatValue("Может редактировать")] edit = 41,

        [fatValue("Комментарий")] comment = 5,

        //[fatValue("Скайп")] skype = 6,

        [fatValue("Оценить консультанта")] rateConsultant = 7,

        [fatValue("Мероприятие")] @event = 80,
        [fatValue("Коммандировка")] eventItem = 81,
        [fatValue("обещал быть на семинаре")] seminarPromised = 82,
        [fatValue("Семинар")] eventSeminar = 83,
        [fatValue("Встреча в коммандировке")] meetingEvent = 84,
        [fatValue("не будет на семинаре")] seminarCanceled = 85,
        [fatValue("не был на семинаре")] seminarSkiped = 86,
        [fatValue("был на семинаре")] seminarVisited = 87,

        [fatValue("Отправлено письмо")] sendMail = 90
    }

    public class MapTask : EntityTypeConfiguration<Event>
    {
        public MapTask()
        {
            HasOptional(v => v.Organization)
                .WithMany(v => v.Events)
                .HasForeignKey(v => v.OrgID)
                .WillCascadeOnDelete(false);

            HasOptional(v => v.Record)
                .WithMany(v => v.Events)
                .HasForeignKey(v => v.RecordID)
                .WillCascadeOnDelete(false);

            HasMany(v => v.Departaments)
                .WithMany(v => v.Events);

            HasMany(v => v.Users)
                .WithMany(v => v.Events);

            HasMany(v => v.PathFiles)
                .WithRequired(v => v.Event)
                .HasForeignKey(v => v.EventID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Interests)
                .WithMany(v => v.Events);

            HasOptional(v => v.MeetingResult)
                .WithRequired(v => v.Event)
                .WillCascadeOnDelete(true);

            HasOptional(v => v.Prev)
                .WithMany(v => v.Next)
                .WillCascadeOnDelete(false);

            HasMany(v => v.Address)
                .WithMany(v => v.Events);

            HasMany(v => v.Activities)
                .WithMany(v => v.Events);

            HasMany(v => v.Files)
                .WithOptional(v => v.Event)
                .HasForeignKey(v => v.EventID)
                .WillCascadeOnDelete(true);

            HasOptional(v => v.Contact)
                .WithMany(v => v.Events)
                .HasForeignKey(v => v.ContactID)
                .WillCascadeOnDelete(false);
        }
    }
}