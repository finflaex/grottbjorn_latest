﻿#region

using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using BaseDBFX.Base;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Table
{
    public class HelpActivity : fxaKeyTable
        , fxiDescriptionTable
        , fxiOrderTable
    {
        public const string CurrencyKey = "currency";
        public const string CornKey = "corn";

        public HelpActivity()
        {
            Activity = new HashSet<Activity>();
            Events = new HashSet<Event>();
        }

        public string Caption { get; set; }

        public virtual ICollection<Activity> Activity { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public string Description { get; set; }

        public int Order { get; set; }
    }

    public class MapOrgDirection : EntityTypeConfiguration<HelpActivity>
    {
        public MapOrgDirection()
        {
            HasMany(v => v.Activity)
                .WithRequired(v => v.ActivityValue)
                .HasForeignKey(v => v.ActivityID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Events)
                .WithMany(v => v.Activities);
        }

        public static void Initialize(DBFX db)
        {
            db.HelpActivities.Any(v => v.Key == HelpActivity.CurrencyKey)
                .IfFalse(() =>
                {
                    db.Add(new HelpActivity
                    {
                        Caption = "Валюта",
                        Key = "currency",
                        Description = "Валютный рынок"
                    });
                });

            db.HelpActivities.Any(v => v.Key == HelpActivity.CornKey)
                .IfFalse(() =>
                {
                    db.Add(new HelpActivity
                    {
                        Caption = "Зерно",
                        Key = "corn",
                        Description = "Рынок зерна"
                    });
                });
        }
    }
}