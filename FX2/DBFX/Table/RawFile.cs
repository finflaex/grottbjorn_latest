﻿#region

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class RawFile : fxaGuidTable,
        fxiCreateAuthorIdTable,
        fxiDateCreateTable,
        fxiDateModifyTable
    {
        public Guid? EventID { get; set; }

        [Column("Raw")]
        public byte[] _raw { get; set; }

        [NotMapped]
        public byte[] Raw
        {
            get { return _raw; }
            set
            {
                Size = value.Length;
                _raw = value;
            }
        }

        public int Size { get; set; }
        public virtual Event Event { get; set; }
        public string Name { get; set; }
        public TypeFile Type { get; set; }
        public Login User { get; set; }
        public Guid? UserID { get; set; }
        public Guid? CreateAuthorID { get; set; }
        public DateTimeOffset DateCreate { get; set; }
        public DateTimeOffset DateModify { get; set; }
    }

    public class MapRawFile : EntityTypeConfiguration<RawFile>
    {
        public MapRawFile()
        {
            HasOptional(v => v.Event)
                .WithMany(v => v.Files)
                .HasForeignKey(v => v.EventID)
                .WillCascadeOnDelete(true);

            HasOptional(v => v.User)
                .WithMany(v => v.Files)
                .HasForeignKey(v => v.UserID)
                .WillCascadeOnDelete(true);
        }
    }
}