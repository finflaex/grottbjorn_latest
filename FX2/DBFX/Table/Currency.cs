﻿#region

using System;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class Currency : fxaGuidTable
        , fxiDescriptionTable
    {
        public string CurrencyKey { get; set; }
        public Guid OrganizationID { get; set; }
        public HelpCurrency CurrencyValue { get; set; }
        public InterestType Type { get; set; }
        public decimal Year { get; set; }
        public decimal Single { get; set; }
        public Organization Organization { get; set; }
        public string Description { get; set; }
    }

    public class MapCurrency : EntityTypeConfiguration<Currency>
    {
        public MapCurrency()
        {
            HasRequired(v => v.Organization)
                .WithMany(v => v.Currency)
                .HasForeignKey(v => v.OrganizationID)
                .WillCascadeOnDelete(true);

            HasRequired(v => v.CurrencyValue)
                .WithMany(v => v.OrgCurrency)
                .HasForeignKey(v => v.CurrencyKey)
                .WillCascadeOnDelete(false);
        }
    }
}