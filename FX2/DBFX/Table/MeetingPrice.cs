﻿#region

using System;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class MeetingPrice : fxaGuidTable
        , fxiDateCreateTable
    {
        public decimal Value { get; set; }

        public bool HasLPR { get; set; }
        public bool PorogRaz { get; set; }

        public virtual MeetingYear Year { get; set; }
        public Guid YearID { get; set; }
        public DateTimeOffset DateCreate { get; set; }
    }

    public class MapMeetingPrice : EntityTypeConfiguration<MeetingPrice>
    {
        public MapMeetingPrice()
        {
            HasRequired(v => v.Year)
                .WithMany(v => v.Prices)
                .HasForeignKey(v => v.YearID)
                .WillCascadeOnDelete(true);
        }
    }
}