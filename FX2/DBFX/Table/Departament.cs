﻿#region

using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using BaseDBFX.Base;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Table
{
    public class Departament : fxaHierarchyGuidTable<Departament>,
        fxiCaptionTable,
        fxiDescriptionTable
    {
        public const string ConsultantDepartament = "consultants";
        public const string SkypeDepartament = "skype";
        public const string OperatorDepartament = "operators";
        public const string SdlDepartament = "sdl";

        public Departament()
        {
            Users = new HashSet<Login>();
            Events = new HashSet<Event>();
            Activity = new HashSet<Activity>();
            Prices = new HashSet<MeetingYear>();
        }

        public string Key { get; set; }
        public int OldID { get; set; }
        public virtual ICollection<Login> Users { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<Activity> Activity { get; set; }
        public virtual ICollection<MeetingYear> Prices { get; set; }
        public decimal MinCurrencyRaz { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
    }

    public class MapDepartament : EntityTypeConfiguration<Departament>
    {
        public MapDepartament()
        {
            HasMany(v => v.Users)
                .WithMany(v => v.Departaments);

            HasMany(v => v.Childs)
                .WithOptional(v => v.Parent)
                .HasForeignKey(v => v.ParentID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Events)
                .WithMany(v => v.Departaments);

            HasMany(v => v.Activity)
                .WithOptional(v => v.Departament)
                .HasForeignKey(v => v.DepartamentID)
                .WillCascadeOnDelete(false);

            HasMany(v => v.Prices)
                .WithRequired(v => v.Departament)
                .HasForeignKey(v => v.DepartamentID)
                .WillCascadeOnDelete(true);
        }


        public static void Initialize(DBFX db)
        {
            var root_consultant = db.Departaments
                .FirstOrDefault(v => v.Key == Departament.ConsultantDepartament)
                .FuncSelect(dep =>
                {
                    if (dep == null)
                    {
                        dep = db.Departaments.Add(new Departament
                        {
                            Key = Departament.ConsultantDepartament,
                            Description = "Отдел консультантов",
                            Caption = "Консультанты"
                        });
                        db.Commit();
                    }

                    return dep;
                });

            db.Departaments
                .FirstOrDefault(v => v.Key == Departament.SkypeDepartament)
                .ActionIfNull(() =>
                {
                    db.Departaments.Add(new Departament
                    {
                        Key = Departament.SkypeDepartament,
                        Description = "Отдел консультантов по Skype",
                        Caption = "Skype консультанты",
                        Parent = root_consultant
                    });
                    db.Commit();
                });

            db.Departaments
                .FirstOrDefault(v => v.Key == Departament.OperatorDepartament)
                .ActionIfNull(() =>
                {
                    db.Departaments.Add(new Departament
                    {
                        Key = Departament.OperatorDepartament,
                        Description = "Отдел операторов",
                        Caption = "Операторы"
                    });
                    db.Commit();
                });
        }
    }
}