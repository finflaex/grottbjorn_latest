﻿#region

using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration;
using BaseDBFX.Base;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class OrgStatus : fxaKeyTable
        , fxiDescriptionTable
    {
        public OrgStatus()
        {
            Organizations = new HashSet<Organization>();
        }

        public string Caption { get; set; }

        public virtual ICollection<Organization> Organizations { get; set; }
        public string Description { get; set; }
    }

    public class MapOrgStatus : EntityTypeConfiguration<OrgStatus>
    {
        public MapOrgStatus()
        {
            HasMany(v => v.Organizations).WithMany(v => v.Statuses);
        }

        public static void Initialize(DBFX db)
        {
            db.OrgStatuses.AddOrUpdate(new OrgStatus
            {
                Caption = "Архив",
                Key = "archive",
                Description = "Архивировано"
            });

            db.OrgStatuses.AddOrUpdate(new OrgStatus
            {
                Caption = "Свободно",
                Key = "free",
                Description = "Ожидает обработки"
            });

            db.OrgStatuses.AddOrUpdate(new OrgStatus
            {
                Caption = "Звонок",
                Key = "calling",
                Description = "В плане обзвона"
            });

            db.OrgStatuses.AddOrUpdate(new OrgStatus
            {
                Caption = "Встреча",
                Key = "meeting",
                Description = "Назначена встреча"
            });
        }
    }
}