﻿#region

using System;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class FssRegistration : fxaGuidTable
    {
        public DateTime? Date { get; set; }
        public string Number { get; set; }
        public long FssID { get; set; }
        public virtual Fss Fss { get; set; }
        public virtual Organization Organization { get; set; }
    }

    public class MapFssRegistration : EntityTypeConfiguration<FssRegistration>
    {
        public MapFssRegistration()
        {
            HasRequired(v => v.Fss)
                .WithMany(v => v.Registrations)
                .HasForeignKey(v => v.FssID)
                .WillCascadeOnDelete(true);

            HasRequired(v => v.Organization)
                .WithOptional(v => v.FssRegistration)
                .WillCascadeOnDelete(true);
        }
    }
}