﻿#region

using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration;
using BaseDBFX.Base;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class Replace : fxaKeyTable
    {
        public Replace()
        {
            Childs = new HashSet<Replace>();
        }

        public Replace(string key)
            : this()
        {
            Key = key;
        }

        public Replace(string key, string value)
            : this(key)
        {
            Value = value;
        }

        public string Group { get; set; }
        public string ParentID { get; set; }

        public virtual Replace Parent { get; set; }
        public virtual ICollection<Replace> Childs { get; set; }

        public string Value { get; set; }
    }

    public class MapReplace : EntityTypeConfiguration<Replace>
    {
        public MapReplace()
        {
            HasKey(v => new {v.Key, v.Group});
            HasMany(v => v.Childs).WithOptional(v => v.Parent).HasForeignKey(v => v.ParentID);
        }

        public static void Initialize(DBFX db)
        {
            db.Replaces.AddOrUpdate(new Replace
            {
                Key = "финансовый директор",
                Value = "Финансовый директор",
                Childs = new[]
                {
                    new Replace("фин")
                }
            });

            db.Replaces.AddOrUpdate(new Replace
            {
                Key = "генеральный директор",
                Value = "Генеральный директор",
                Childs = new[]
                {
                    new Replace("гд"),
                    new Replace("гендир"),
                    new Replace("ген дир"),
                    new Replace("ген. дир.")
                }
            });

            db.Replaces.AddOrUpdate(new Replace
            {
                Key = "главный бухгалтер",
                Value = "Главный бухгалтер",
                Childs = new[]
                {
                    new Replace("гб"),
                    new Replace("главбух"),
                    new Replace("глав бух"),
                    new Replace("глав. бух."),
                    new Replace("главбухгалтер"),
                    new Replace("глав бухгалтер"),
                    new Replace("глав. бухгалтер.")
                }
            });

            db.Replaces.AddOrUpdate(new Replace
            {
                Key = "бухгалтер",
                Value = "Бухгалтер",
                Childs = new[]
                {
                    new Replace("бух"),
                    new Replace("бух.")
                }
            });

            db.Commit();
        }
    }
}