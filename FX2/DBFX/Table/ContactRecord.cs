﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class ContactRecord : fxaGuidTable
        , fxiOrderTable
    {
        public ContactRecord()
        {
            Contacts = new HashSet<Contact>();
            Posts = new HashSet<Post>();
            Persons = new HashSet<Person>();
            Comments = new HashSet<Comment>();
            Events = new HashSet<Event>();
            MeetingResults = new HashSet<MeetingResult>();
            Establisheds = new HashSet<OrgFounder>();
        }

        public Guid? OrganizationID { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Person> Persons { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<MeetingResult> MeetingResults { get; set; }
        public virtual ICollection<OrgFounder> Establisheds { get; set; }
        public int Order { get; set; }
    }

    public class MapContactRecord : EntityTypeConfiguration<ContactRecord>
    {
        public MapContactRecord()
        {
            HasOptional(v => v.Organization)
                .WithMany(v => v.Contacts)
                .HasForeignKey(v => v.OrganizationID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Contacts)
                .WithOptional(v => v.Record)
                .HasForeignKey(v => v.RecordID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Posts)
                .WithMany(v => v.Records);

            HasMany(v => v.Persons)
                .WithMany(v => v.Records);

            HasMany(v => v.Comments)
                .WithOptional(v => v.Record)
                .HasForeignKey(v => v.RecordID)
                .WillCascadeOnDelete(false);

            HasMany(v => v.Events)
                .WithOptional(v => v.Record)
                .HasForeignKey(v => v.RecordID)
                .WillCascadeOnDelete(false);

            HasMany(v => v.MeetingResults)
                .WithOptional(v => v.Record)
                .HasForeignKey(v => v.RecordID)
                .WillCascadeOnDelete(false);

            HasMany(v => v.Establisheds)
                .WithOptional(v => v.Person)
                .HasForeignKey(v => v.PersonID)
                .WillCascadeOnDelete(false);
        }
    }
}