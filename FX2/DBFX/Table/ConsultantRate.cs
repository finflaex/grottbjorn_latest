﻿#region

using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class ConsultantRate
        : fxaGuidTable
            , fxiDescriptionTable
    {
        public bool OkAnswer { get; set; }
        public int Rating { get; set; }


        public virtual MeetingResult Result { get; set; }
        public decimal Bonus { get; set; }
        public string Description { get; set; }
    }

    public class MapConsultantRate
        : EntityTypeConfiguration<ConsultantRate>
    {
        public MapConsultantRate()
        {
            HasRequired(v => v.Result)
                .WithOptional(v => v.Rate)
                .WillCascadeOnDelete(true);
        }
    }
}