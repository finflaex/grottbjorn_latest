﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration;
using BaseDBFX.Base;
using Finflaex.Support.Attributes;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;
using Finflaex.Support.Reflection;
using fxiCaptionTable = Finflaex.Support.DB.Tables.Interfaces.fxiCaptionTable;
using fxiDescriptionTable = Finflaex.Support.DB2.Tables.Interfaces.fxiDescriptionTable;

#endregion

namespace BaseDBFX.Table
{
    public class Label : fxaGuidTable
        , fxiCaptionTable
        , fxiDescriptionTable
        , fxiOrderTable
    {
        public Label()
        {
            Organizations = new HashSet<Organization>();
        }

        public virtual Login User { get; set; }
        public Guid? UserID { get; set; }
        public TypeLabel Type { get; set; }
        public virtual ICollection<Organization> Organizations { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
    }

    public enum TypeLabel
    {
        [fatValue("Неизвестно")] unknown = 0,
        [fatValue("Статус в системе")] status = 1,
        [fatValue("Метки пользователя")] user = 2,
        [fatValue("Статус клиента")] work_step = 3,
        [fatValue("Деятельность")] org_direction = 4,
        [fatValue("Продукт")] org_product = 5,
        [fatValue("Операции")] org_operation = 6,
        [fatValue("Страны")] country = 7
    }

    public class MapLabel : EntityTypeConfiguration<Label>
    {
        public MapLabel()
        {
            HasMany(v => v.Organizations)
                .WithMany(v => v.Labels);

            HasOptional(v => v.User)
                .WithMany(v => v.Labels)
                .HasForeignKey(v => v.UserID)
                .WillCascadeOnDelete(true);
        }

        public static void Initialize(DBFX db)
        {
            db.Labels.AddOrUpdate(new Label
            {
                ID = Guid.Empty,
                Caption = "- пусто -"
            });

            db.Labels.AddOrUpdate(new Label
            {
                ID = 1001.ToGuid(),
                Type = TypeLabel.status,
                Order = 0,
                Caption = "не обработана",
                Description = "Добавлена в базу."
            });

            db.Labels.AddOrUpdate(new Label
            {
                ID = 1002.ToGuid(),
                Type = TypeLabel.status,
                Order = 0,
                Caption = "в обработке",
                Description = "Ведется работа."
            });

            db.Labels.AddOrUpdate(new Label
            {
                ID = 1003.ToGuid(),
                Type = TypeLabel.status,
                Order = 0,
                Caption = "не обрабатывать",
                Description = "Исключена из обработки."
            });
        }
    }
}