﻿#region

using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using BaseDBFX.Base;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class Post : fxaGuidTable,
        fxiCaptionTable,
        fxiDescriptionTable
    {
        public const string Founder = "founder";

        public Post()
        {
            Records = new HashSet<ContactRecord>();
        }

        public virtual ICollection<ContactRecord> Records { get; set; }
        public string WorkID { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
    }

    public class MapPost : EntityTypeConfiguration<Post>
    {
        public MapPost()
        {
            HasMany(v => v.Records)
                .WithMany(v => v.Posts);
        }

        public static void Initialize(DBFX db)
        {
            var post = db.Posts.FirstOrDefault(v => v.WorkID == Post.Founder);
            if (post == null)
                post = new Post();

            post.Description = "юридическое или физическое лицо, создавшее организацию";
            post.Caption = "учредитель";
            post.WorkID = Post.Founder;

            db.Posts.AddOrUpdate(post);
            db.Commit();
        }
    }
}