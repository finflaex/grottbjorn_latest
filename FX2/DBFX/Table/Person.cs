﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class Person : fxaGuidTable
    {
        public Person()
        {
            Records = new HashSet<ContactRecord>();
        }

        public virtual ICollection<ContactRecord> Records { get; set; }

        public string WorkID { get; set; }
        public string RawName { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SurName { get; set; }
        public string Inn { get; set; }
        public DateTime? DateBirthDay { get; set; }
    }

    public class MapPerson : EntityTypeConfiguration<Person>
    {
        public MapPerson()
        {
            HasMany(v => v.Records)
                .WithMany(v => v.Persons);
        }
    }
}