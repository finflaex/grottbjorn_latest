﻿#region

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class Activity : fxaGuidTable
        , fxiDateCreateTable
        , fxiDateModifyTable
        , fxiDescriptionTable
    {
        public Activity()
        {
            OrgInterests = new HashSet<OrgInterest>();
        }

        public Guid? DepartamentID { get; set; }

        [ForeignKey(nameof(DepartamentID))]
        public virtual Departament Departament { get; set; }

        public Guid AddressID { get; set; }

        [ForeignKey(nameof(AddressID))]
        public virtual Address Address { get; set; }

        public string ActivityID { get; set; }

        [ForeignKey(nameof(ActivityID))]
        public virtual HelpActivity ActivityValue { get; set; }

        public virtual ICollection<OrgInterest> OrgInterests { get; set; }

        public DateTimeOffset DateCreate { get; set; }
        public DateTimeOffset DateModify { get; set; }
        public string Description { get; set; }
    }

    public class MapActivity : EntityTypeConfiguration<Activity>
    {
        public MapActivity()
        {
            HasRequired(v => v.Address)
                .WithMany(v => v.Activity)
                .HasForeignKey(v => v.AddressID)
                .WillCascadeOnDelete(true);

            HasRequired(v => v.ActivityValue)
                .WithMany(v => v.Activity)
                .HasForeignKey(v => v.ActivityID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.OrgInterests)
                .WithRequired(v => v.Activity)
                .HasForeignKey(v => v.ActivityID)
                .WillCascadeOnDelete(true);

            HasOptional(v => v.Departament)
                .WithMany(v => v.Activity)
                .HasForeignKey(v => v.DepartamentID)
                .WillCascadeOnDelete(false);
        }
    }
}