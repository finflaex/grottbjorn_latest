﻿#region

using System;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class PathFile : fxaGuidTable
    {
        public int PathOldID { get; set; }
        public string Path { get; set; }
        public TypeFile Type { get; set; }

        public Guid EventID { get; set; }
        public Event Event { get; set; }
        public int Size { get; set; }
        public DateTime? Date { get; set; }
    }

    public class MapPathFile : EntityTypeConfiguration<PathFile>
    {
        public MapPathFile()
        {
            HasRequired(v => v.Event).WithMany(v => v.PathFiles).HasForeignKey(v => v.EventID).WillCascadeOnDelete(true);
        }
    }

    public enum TypeFile
    {
        unknown = 0,
        PhoneRecord = 1,
        EMail = 2,
        Config = 3,
        SdlOrgInfo = 4,
        Cik = 5
    }
}