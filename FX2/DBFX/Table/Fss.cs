﻿#region

using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class Fss : fxaIntTable,
        fxiCaptionTable
    {
        public Fss()
        {
            Registrations = new HashSet<FssRegistration>();
        }

        public string Code { get; set; }
        public virtual ICollection<FssRegistration> Registrations { get; set; }
        public string Caption { get; set; }
    }

    public class MapFss : EntityTypeConfiguration<Fss>
    {
        public MapFss()
        {
            HasMany(v => v.Registrations)
                .WithRequired(v => v.Fss)
                .HasForeignKey(v => v.FssID)
                .WillCascadeOnDelete(true);
        }
    }
}