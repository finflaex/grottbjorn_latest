﻿#region

using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration;
using BaseDBFX.Base;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;

#endregion

namespace BaseDBFX.Table
{
    public class Role : fxaKeyTable
        , fxiDescriptionTable
    {
        public Role()
        {
            Logins = new HashSet<Login>();
        }

        public string Caption { get; set; }

        public virtual ICollection<Login> Logins { get; set; }
        public string Description { get; set; }
    }

    public class MapRoles : EntityTypeConfiguration<Role>
    {
        public MapRoles()
        {
            HasMany(v => v.Logins).WithMany(v => v.Roles);
        }

        public static void Initialize(DBFX db)
        {
            db.Roles.AddOrUpdate(new Role
            {
                Key = dbroles.developer,
                Caption = "Разработчик"
            });

            db.Roles.AddOrUpdate(new Role
            {
                Key = dbroles.admin,
                Caption = "Администратор"
            });

            db.Roles.AddOrUpdate(new Role
            {
                Key = dbroles.manager,
                Caption = "Менеджер"
            });

            db.Roles.AddOrUpdate(new Role
            {
                Key = dbroles.@operator,
                Caption = "Оператор"
            });

            db.Roles.AddOrUpdate(new Role
            {
                Key = dbroles.consultant,
                Caption = "Консультант"
            });

            db.Commit();
        }
    }
}