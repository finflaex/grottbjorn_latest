﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using BaseDBFX.Base;
using Finflaex.Support.ApiOnline.AddressMaster;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Table
{
    public class Organization : fxaGuidTable,
        fxiDescriptionTable
    {
        public Organization()
        {
            Contacts = new HashSet<ContactRecord>();
            Comments = new HashSet<Comment>();
            Address = new HashSet<Address>();
            Statuses = new HashSet<OrgStatus>();
            Events = new HashSet<Event>();
            Interests = new HashSet<OrgInterest>();
            Currency = new HashSet<Currency>();
            ListOkved2 = new HashSet<Okved2>();
            Founders = new HashSet<OrgFounder>();
            Establisheds = new HashSet<OrgFounder>();
            PhysicAddresses = new HashSet<Address>();

            LinksChilds = new HashSet<Link>();
            LinksParents = new HashSet<Link>();
        }

        public virtual ICollection<ContactRecord> Contacts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public string FullName { get; set; }
        public string ShortName { get; set; }
        public string Inn { get; set; }
        public string RawAddress { get; set; }
        public string FormatAddress { get; set; }
        public string PhysicalAdress { get; set; }
        public string LegalAddress { get; set; }
        public virtual ICollection<Address> Address { get; set; }
        public virtual ICollection<Address> PhysicAddresses { get; set; }
        public string HouseNumber { get; set; }
        public string ZipCode { get; set; }

        public bool Work { get; set; }
        public bool OfConsultant { get; set; }
        public string WorkID { get; set; }

        public virtual ICollection<OrgStatus> Statuses { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<OrgInterest> Interests { get; set; }

        public virtual ICollection<Currency> Currency { get; set; }

        public virtual OrgCloseInfo CloseInfo { get; set; }

        public long? OkopfID { get; set; }
        public virtual Okopf Okopf { get; set; }
        public string Ogrn { get; set; }
        public DateTime? OgrnDate { get; set; }
        public string Kpp { get; set; }
        public DateTime? WorkDate { get; set; }
        public virtual FssRegistration FssRegistration { get; set; }
        public virtual Okved2 MainOkved2 { get; set; }
        public virtual ICollection<Okved2> ListOkved2 { get; set; }
        public virtual ICollection<OrgFounder> Founders { get; set; }
        public virtual ICollection<OrgFounder> Establisheds { get; set; }
        public virtual ForeignRegistration ForeignRegistration { get; set; }
        public string ClientCode { get; set; }

        public virtual ICollection<Label> Labels { get; set; }

        public virtual ICollection<Link> LinksChilds { get; set; }
        public virtual ICollection<Link> LinksParents { get; set; }
        public string Description { get; set; }

        public Organization ParseAddress(DBFX db, string value)
        {
            value = value.retIfNullOrWhiteSpace();
            try
            {
                var address = fxsAddressMaster.ParseAddress(value).FirstOrDefault();
                FormatAddress = address?.FormatAddress;
                RawAddress = value;
                Address.Clear();
                db.TryCommit($"{MethodBase.GetCurrentMethod().Name} clear address");
                HouseNumber = address?.HOUSE_INFO?.HOUSE_NUMBER;
                ZipCode = address?.HOUSE_INFO?.ZIPCODE;
                Address parent = null;
                address.ADDRESS_INFO.ToAddresses().OrderBy(v => v.Type).ForEach(adr =>
                {
                    adr = db.Addresses.Find(adr.ID) ?? adr;
                    if (adr.Activity.Any())
                        adr.Activity.ForEach(act =>
                        {
                            var intr =
                                Interests.FirstOrDefault(v => v.Activity.ActivityValue.Key == act.ActivityValue.Key);
                            if (intr != null)
                                intr.Activity = act;
                            else
                                Interests.Add(new OrgInterest
                                {
                                    Activity = act
                                });
                        });
                    if (parent != null)
                        adr.Parent = parent;
                    Address.Add(adr);
                    db.TryCommit($"{MethodBase.GetCurrentMethod().Name} add address");
                    parent = adr;
                });
            }
            catch
            {
                Description = $"{Description} adr:{value}";
            }
            return this;
        }
    }

    public class MapOrganization : EntityTypeConfiguration<Organization>
    {
        public MapOrganization()
        {
            HasMany(v => v.Contacts)
                .WithOptional(v => v.Organization)
                .HasForeignKey(v => v.OrganizationID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Comments)
                .WithOptional(v => v.Organization)
                .HasForeignKey(v => v.OrganizationID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Events)
                .WithOptional(v => v.Organization)
                .HasForeignKey(v => v.OrgID)
                .WillCascadeOnDelete(false);

            HasMany(v => v.Address)
                .WithMany(v => v.Organizations);

            HasMany(v => v.Statuses)
                .WithMany(v => v.Organizations);

            HasMany(v => v.Interests)
                .WithRequired(v => v.Organization)
                .HasForeignKey(v => v.OrgID)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Currency)
                .WithRequired(v => v.Organization)
                .HasForeignKey(v => v.OrganizationID)
                .WillCascadeOnDelete(true);

            HasOptional(v => v.CloseInfo)
                .WithRequired(v => v.Organization)
                .WillCascadeOnDelete(true);

            HasOptional(v => v.Okopf)
                .WithMany(v => v.Organizations)
                .HasForeignKey(v => v.OkopfID)
                .WillCascadeOnDelete(false);

            HasOptional(v => v.FssRegistration)
                .WithRequired(v => v.Organization)
                .WillCascadeOnDelete(true);

            HasOptional(v => v.MainOkved2)
                .WithMany(v => v.MainOrganizations)
                .WillCascadeOnDelete(false);

            HasMany(v => v.ListOkved2)
                .WithMany(v => v.Organizations);

            HasMany(v => v.Founders)
                .WithRequired(v => v.Target)
                .HasForeignKey(v => v.TargetID)
                .WillCascadeOnDelete(false);

            HasMany(v => v.Establisheds)
                .WithOptional(v => v.Organization)
                .HasForeignKey(v => v.OrganizationID)
                .WillCascadeOnDelete(true);

            HasOptional(v => v.ForeignRegistration)
                .WithRequired(v => v.Organization)
                .WillCascadeOnDelete(true);

            HasMany(v => v.Labels)
                .WithMany(v => v.Organizations);

            HasMany(v => v.LinksChilds)
                .WithOptional(v => v.First)
                .HasForeignKey(v => v.FirstID)
                .WillCascadeOnDelete(false);

            HasMany(v => v.LinksParents)
                .WithOptional(v => v.Second)
                .HasForeignKey(v => v.SecondID)
                .WillCascadeOnDelete(false);
        }
    }
}