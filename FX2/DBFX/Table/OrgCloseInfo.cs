﻿#region

using System;
using System.Data.Entity.ModelConfiguration;
using Finflaex.Support.DB2.Tables.Abstracts;

#endregion

namespace BaseDBFX.Table
{
    public class OrgCloseInfo : fxaGuidTable
    {
        public long ReasonID { get; set; }
        public DateTime? Date { get; set; }
        public virtual OrgCloseType Reason { get; set; }
        public virtual Organization Organization { get; set; }
    }

    public class MapOrgCloseInfo : EntityTypeConfiguration<OrgCloseInfo>
    {
        public MapOrgCloseInfo()
        {
            HasRequired(v => v.Organization)
                .WithOptional(v => v.CloseInfo)
                .WillCascadeOnDelete(true);

            HasRequired(v => v.Reason)
                .WithMany(v => v.OrgClosed)
                .HasForeignKey(v => v.ReasonID)
                .WillCascadeOnDelete(true);
        }
    }
}