﻿#region

using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using BaseDBFX.Base;
using Finflaex.Support.ApiOnline.MosRU;
using Finflaex.Support.DB2.Tables.Abstracts;
using Finflaex.Support.DB2.Tables.Interfaces;
using Finflaex.Support.Reflection;

#endregion

namespace BaseDBFX.Table
{
    public class Okopf : fxaIntTable,
        fxiCaptionTable,
        fxiDescriptionTable
    {
        public Okopf()
        {
            Organizations = new HashSet<Organization>();
        }

        public string Suffix { get; set; }
        public string Code { get; set; }
        public virtual ICollection<Organization> Organizations { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
    }

    public class MapOkopf : EntityTypeConfiguration<Okopf>
    {
        public MapOkopf()
        {
            HasMany(v => v.Organizations)
                .WithOptional(v => v.Okopf)
                .HasForeignKey(v => v.OkopfID)
                .WillCascadeOnDelete(false);
        }

        public static void Initialize(DBFX db)
        {
            ApiMosRu
                .Okopfs
                .Select(v => v.Cells)
                .Select(v => new Okopf
                {
                    ID = v.global_id,
                    Caption = v.Name,
                    Code = v.Kod,
                    Description = v.NOMDESCR
                })
                .ForEach(
                    read => { db.Okopfs.Any(v => v.Code == read.Code).IfFalse(() => { db.Okopfs.AddOrUpdate(read); }); });
            db.SaveChanges();
        }
    }
}