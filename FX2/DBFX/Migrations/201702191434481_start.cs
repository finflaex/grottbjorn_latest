#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class start : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "OrganizationID", "dbo.Organizations");
            CreateTable(
                    "dbo.PathFiles",
                    c => new
                    {
                        ID = c.Guid(false),
                        PathOldID = c.Int(false),
                        Path = c.String(),
                        Type = c.Int(false),
                        EventID = c.Guid(false),
                        Size = c.Int(false)
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Events", t => t.EventID, true)
                .Index(t => t.EventID);

            AddColumn("dbo.Addresses", "Suffix", c => c.String());
            AddColumn("dbo.Organizations", "HouseNumber", c => c.String());
            AddColumn("dbo.Organizations", "ZipCode", c => c.String());
            AddColumn("dbo.Logins", "InternalBaseID", c => c.Int(false));
            AddColumn("dbo.Logins", "InternalPhoneNumber", c => c.String());
            AddColumn("dbo.Logins", "InternalEMailAddress", c => c.String());
            AddColumn("dbo.Events", "Notify", c => c.DateTime());
            AddForeignKey("dbo.Comments", "OrganizationID", "dbo.Organizations", "ID", true);
            DropColumn("dbo.Addresses", "ShortName");
        }

        public override void Down()
        {
            AddColumn("dbo.Addresses", "ShortName", c => c.String());
            DropForeignKey("dbo.Comments", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.PathFiles", "EventID", "dbo.Events");
            DropIndex("dbo.PathFiles", new[] {"EventID"});
            DropColumn("dbo.Events", "Notify");
            DropColumn("dbo.Logins", "InternalEMailAddress");
            DropColumn("dbo.Logins", "InternalPhoneNumber");
            DropColumn("dbo.Logins", "InternalBaseID");
            DropColumn("dbo.Organizations", "ZipCode");
            DropColumn("dbo.Organizations", "HouseNumber");
            DropColumn("dbo.Addresses", "Suffix");
            DropTable("dbo.PathFiles");
            AddForeignKey("dbo.Comments", "OrganizationID", "dbo.Organizations", "ID");
        }
    }
}