#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class time_zone : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logins", "TimeZone", c => c.Int(false));
        }

        public override void Down()
        {
            DropColumn("dbo.Logins", "TimeZone");
        }
    }
}