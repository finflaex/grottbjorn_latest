// <auto-generated />
namespace BaseDBFX.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class consultantRates : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(consultantRates));
        
        string IMigrationMetadata.Id
        {
            get { return "201703311128355_consultantRates"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
