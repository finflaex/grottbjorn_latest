#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class timzones : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Events", "Notify", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.Events", "Start", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.Events", "End", c => c.DateTimeOffset(precision: 7));
        }

        public override void Down()
        {
            AlterColumn("dbo.Events", "End", c => c.DateTime());
            AlterColumn("dbo.Events", "Start", c => c.DateTime());
            AlterColumn("dbo.Events", "Notify", c => c.DateTime());
        }
    }
}