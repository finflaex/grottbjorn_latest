#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class events : DbMigration
    {
        public override void Up()
        {
            RenameTable("dbo.EventDepartaments", "DepartamentEvents");
            DropPrimaryKey("dbo.DepartamentEvents");
            CreateTable(
                    "dbo.AddressEvents",
                    c => new
                    {
                        Address_ID = c.Guid(false),
                        Event_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.Address_ID, t.Event_ID})
                .ForeignKey("dbo.Addresses", t => t.Address_ID, true)
                .ForeignKey("dbo.Events", t => t.Event_ID, true)
                .Index(t => t.Address_ID)
                .Index(t => t.Event_ID);

            CreateTable(
                    "dbo.HelpActivityEvents",
                    c => new
                    {
                        HelpActivity_Key = c.String(false, 128),
                        Event_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.HelpActivity_Key, t.Event_ID})
                .ForeignKey("dbo.HelpActivities", t => t.HelpActivity_Key, true)
                .ForeignKey("dbo.Events", t => t.Event_ID, true)
                .Index(t => t.HelpActivity_Key)
                .Index(t => t.Event_ID);

            AddPrimaryKey("dbo.DepartamentEvents", new[] {"Departament_ID", "Event_ID"});
        }

        public override void Down()
        {
            DropForeignKey("dbo.HelpActivityEvents", "Event_ID", "dbo.Events");
            DropForeignKey("dbo.HelpActivityEvents", "HelpActivity_Key", "dbo.HelpActivities");
            DropForeignKey("dbo.AddressEvents", "Event_ID", "dbo.Events");
            DropForeignKey("dbo.AddressEvents", "Address_ID", "dbo.Addresses");
            DropIndex("dbo.HelpActivityEvents", new[] {"Event_ID"});
            DropIndex("dbo.HelpActivityEvents", new[] {"HelpActivity_Key"});
            DropIndex("dbo.AddressEvents", new[] {"Event_ID"});
            DropIndex("dbo.AddressEvents", new[] {"Address_ID"});
            DropPrimaryKey("dbo.DepartamentEvents");
            DropTable("dbo.HelpActivityEvents");
            DropTable("dbo.AddressEvents");
            AddPrimaryKey("dbo.DepartamentEvents", new[] {"Event_ID", "Departament_ID"});
            RenameTable("dbo.DepartamentEvents", "EventDepartaments");
        }
    }
}