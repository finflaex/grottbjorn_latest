#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class phyzik_address : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Organizations", "PhysicalAdress", c => c.String());
            AddColumn("dbo.Organizations", "LegalAddress", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.Organizations", "LegalAddress");
            DropColumn("dbo.Organizations", "PhysicalAdress");
        }
    }
}