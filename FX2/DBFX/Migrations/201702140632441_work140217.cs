#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class work140217 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.Addresses",
                    c => new
                    {
                        ID = c.Guid(false),
                        Name = c.String(),
                        ShortName = c.String(),
                        Type = c.Int(false),
                        ParentID = c.Guid()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Addresses", t => t.ParentID)
                .Index(t => t.ParentID);

            CreateTable(
                    "dbo.Organizations",
                    c => new
                    {
                        ID = c.Guid(false),
                        FullName = c.String(),
                        ShortName = c.String(),
                        Inn = c.String(),
                        Description = c.String(),
                        RawAddress = c.String(),
                        FormatAddress = c.String()
                    })
                .PrimaryKey(t => t.ID);

            CreateTable(
                    "dbo.Comments",
                    c => new
                    {
                        ID = c.Guid(false),
                        Caption = c.String(),
                        Description = c.String(),
                        Type = c.Int(false),
                        DateCreate = c.DateTimeOffset(false, 7),
                        AuthorID = c.Guid(),
                        RecordID = c.Guid(),
                        OrganizationID = c.Guid()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContactRecords", t => t.RecordID)
                .ForeignKey("dbo.Logins", t => t.AuthorID)
                .ForeignKey("dbo.Organizations", t => t.OrganizationID)
                .Index(t => t.AuthorID)
                .Index(t => t.RecordID)
                .Index(t => t.OrganizationID);

            CreateTable(
                    "dbo.Logins",
                    c => new
                    {
                        ID = c.Guid(false),
                        Caption = c.String(),
                        Name = c.String(),
                        OldID = c.Int(false),
                        Password = c.Binary()
                    })
                .PrimaryKey(t => t.ID);

            CreateTable(
                    "dbo.Departaments",
                    c => new
                    {
                        ID = c.Guid(false),
                        Key = c.String(),
                        OldID = c.Int(false),
                        Caption = c.String(),
                        Description = c.String(),
                        ParentID = c.Guid()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Departaments", t => t.ParentID)
                .Index(t => t.ParentID);

            CreateTable(
                    "dbo.Events",
                    c => new
                    {
                        ID = c.Guid(false),
                        RecordID = c.Guid(),
                        OrgID = c.Guid(),
                        AuthorID = c.Guid(),
                        DateCreate = c.DateTimeOffset(false, 7),
                        DateModify = c.DateTimeOffset(false, 7),
                        Caption = c.String(),
                        Description = c.String(),
                        IsAllDay = c.Boolean(false),
                        Start = c.DateTime(),
                        End = c.DateTime(),
                        Type = c.Int(false),
                        CallStatus = c.Int(false),
                        Closed = c.Boolean(false)
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContactRecords", t => t.RecordID)
                .ForeignKey("dbo.Organizations", t => t.OrgID, true)
                .Index(t => t.RecordID)
                .Index(t => t.OrgID);

            CreateTable(
                    "dbo.ContactRecords",
                    c => new
                    {
                        ID = c.Guid(false),
                        OrganizationID = c.Guid()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.OrganizationID, true)
                .Index(t => t.OrganizationID);

            CreateTable(
                    "dbo.Contacts",
                    c => new
                    {
                        ID = c.Guid(false),
                        RecordID = c.Guid(),
                        Type = c.Int(false),
                        Refer = c.Int(false),
                        RawValue = c.String(),
                        ParseValue = c.String()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContactRecords", t => t.RecordID, true)
                .Index(t => t.RecordID);

            CreateTable(
                    "dbo.People",
                    c => new
                    {
                        ID = c.Guid(false),
                        RecordID = c.Guid(),
                        RawName = c.String(),
                        FullName = c.String(),
                        DateBirthDay = c.DateTime()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContactRecords", t => t.RecordID, true)
                .Index(t => t.RecordID);

            CreateTable(
                    "dbo.Posts",
                    c => new
                    {
                        ID = c.Guid(false),
                        RecordID = c.Guid(),
                        Caption = c.String(),
                        Description = c.String()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContactRecords", t => t.RecordID, true)
                .Index(t => t.RecordID);

            CreateTable(
                    "dbo.Messages",
                    c => new
                    {
                        ID = c.Guid(false),
                        Caption = c.String(),
                        AuthorID = c.Guid(),
                        TargetID = c.Guid(),
                        DateCreate = c.DateTimeOffset(false, 7),
                        Description = c.String(),
                        View = c.Boolean(false),
                        History = c.Boolean(false),
                        AuthorService = c.String()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Logins", t => t.TargetID)
                .ForeignKey("dbo.Logins", t => t.AuthorID)
                .Index(t => t.AuthorID)
                .Index(t => t.TargetID);

            CreateTable(
                    "dbo.Roles",
                    c => new
                    {
                        Key = c.String(false, 128),
                        Caption = c.String(),
                        Description = c.String()
                    })
                .PrimaryKey(t => t.Key);

            CreateTable(
                    "dbo.OrgDirections",
                    c => new
                    {
                        Key = c.String(false, 128),
                        Caption = c.String(),
                        Description = c.String()
                    })
                .PrimaryKey(t => t.Key);

            CreateTable(
                    "dbo.OrgStatus",
                    c => new
                    {
                        Key = c.String(false, 128),
                        Caption = c.String(),
                        Description = c.String()
                    })
                .PrimaryKey(t => t.Key);

            CreateTable(
                    "dbo.Replaces",
                    c => new
                    {
                        Key = c.String(false, 128),
                        Group = c.String(),
                        ParentID = c.String(),
                        Value = c.String(),
                        Parent_Key = c.String(maxLength: 128)
                    })
                .PrimaryKey(t => t.Key)
                .ForeignKey("dbo.Replaces", t => t.Parent_Key)
                .Index(t => t.Parent_Key);

            CreateTable(
                    "dbo.EventDepartaments",
                    c => new
                    {
                        Event_ID = c.Guid(false),
                        Departament_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.Event_ID, t.Departament_ID})
                .ForeignKey("dbo.Events", t => t.Event_ID, true)
                .ForeignKey("dbo.Departaments", t => t.Departament_ID, true)
                .Index(t => t.Event_ID)
                .Index(t => t.Departament_ID);

            CreateTable(
                    "dbo.LoginDepartaments",
                    c => new
                    {
                        Login_ID = c.Guid(false),
                        Departament_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.Login_ID, t.Departament_ID})
                .ForeignKey("dbo.Logins", t => t.Login_ID, true)
                .ForeignKey("dbo.Departaments", t => t.Departament_ID, true)
                .Index(t => t.Login_ID)
                .Index(t => t.Departament_ID);

            CreateTable(
                    "dbo.LoginEvents",
                    c => new
                    {
                        Login_ID = c.Guid(false),
                        Event_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.Login_ID, t.Event_ID})
                .ForeignKey("dbo.Logins", t => t.Login_ID, true)
                .ForeignKey("dbo.Events", t => t.Event_ID, true)
                .Index(t => t.Login_ID)
                .Index(t => t.Event_ID);

            CreateTable(
                    "dbo.LoginRoles",
                    c => new
                    {
                        Login_ID = c.Guid(false),
                        Role_Key = c.String(false, 128)
                    })
                .PrimaryKey(t => new {t.Login_ID, t.Role_Key})
                .ForeignKey("dbo.Logins", t => t.Login_ID, true)
                .ForeignKey("dbo.Roles", t => t.Role_Key, true)
                .Index(t => t.Login_ID)
                .Index(t => t.Role_Key);

            CreateTable(
                    "dbo.OrganizationOrgDirections",
                    c => new
                    {
                        Organization_ID = c.Guid(false),
                        OrgDirection_Key = c.String(false, 128)
                    })
                .PrimaryKey(t => new {t.Organization_ID, t.OrgDirection_Key})
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, true)
                .ForeignKey("dbo.OrgDirections", t => t.OrgDirection_Key, true)
                .Index(t => t.Organization_ID)
                .Index(t => t.OrgDirection_Key);

            CreateTable(
                    "dbo.OrganizationOrgStatus",
                    c => new
                    {
                        Organization_ID = c.Guid(false),
                        OrgStatus_Key = c.String(false, 128)
                    })
                .PrimaryKey(t => new {t.Organization_ID, t.OrgStatus_Key})
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, true)
                .ForeignKey("dbo.OrgStatus", t => t.OrgStatus_Key, true)
                .Index(t => t.Organization_ID)
                .Index(t => t.OrgStatus_Key);

            CreateTable(
                    "dbo.AddressOrganizations",
                    c => new
                    {
                        Address_ID = c.Guid(false),
                        Organization_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.Address_ID, t.Organization_ID})
                .ForeignKey("dbo.Addresses", t => t.Address_ID, true)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, true)
                .Index(t => t.Address_ID)
                .Index(t => t.Organization_ID);
        }

        public override void Down()
        {
            DropForeignKey("dbo.Replaces", "Parent_Key", "dbo.Replaces");
            DropForeignKey("dbo.AddressOrganizations", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.AddressOrganizations", "Address_ID", "dbo.Addresses");
            DropForeignKey("dbo.OrganizationOrgStatus", "OrgStatus_Key", "dbo.OrgStatus");
            DropForeignKey("dbo.OrganizationOrgStatus", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.Events", "OrgID", "dbo.Organizations");
            DropForeignKey("dbo.OrganizationOrgDirections", "OrgDirection_Key", "dbo.OrgDirections");
            DropForeignKey("dbo.OrganizationOrgDirections", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.Comments", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.Messages", "AuthorID", "dbo.Logins");
            DropForeignKey("dbo.LoginRoles", "Role_Key", "dbo.Roles");
            DropForeignKey("dbo.LoginRoles", "Login_ID", "dbo.Logins");
            DropForeignKey("dbo.Messages", "TargetID", "dbo.Logins");
            DropForeignKey("dbo.Comments", "AuthorID", "dbo.Logins");
            DropForeignKey("dbo.LoginEvents", "Event_ID", "dbo.Events");
            DropForeignKey("dbo.LoginEvents", "Login_ID", "dbo.Logins");
            DropForeignKey("dbo.LoginDepartaments", "Departament_ID", "dbo.Departaments");
            DropForeignKey("dbo.LoginDepartaments", "Login_ID", "dbo.Logins");
            DropForeignKey("dbo.Posts", "RecordID", "dbo.ContactRecords");
            DropForeignKey("dbo.People", "RecordID", "dbo.ContactRecords");
            DropForeignKey("dbo.ContactRecords", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.Events", "RecordID", "dbo.ContactRecords");
            DropForeignKey("dbo.Contacts", "RecordID", "dbo.ContactRecords");
            DropForeignKey("dbo.Comments", "RecordID", "dbo.ContactRecords");
            DropForeignKey("dbo.EventDepartaments", "Departament_ID", "dbo.Departaments");
            DropForeignKey("dbo.EventDepartaments", "Event_ID", "dbo.Events");
            DropForeignKey("dbo.Departaments", "ParentID", "dbo.Departaments");
            DropForeignKey("dbo.Addresses", "ParentID", "dbo.Addresses");
            DropIndex("dbo.AddressOrganizations", new[] {"Organization_ID"});
            DropIndex("dbo.AddressOrganizations", new[] {"Address_ID"});
            DropIndex("dbo.OrganizationOrgStatus", new[] {"OrgStatus_Key"});
            DropIndex("dbo.OrganizationOrgStatus", new[] {"Organization_ID"});
            DropIndex("dbo.OrganizationOrgDirections", new[] {"OrgDirection_Key"});
            DropIndex("dbo.OrganizationOrgDirections", new[] {"Organization_ID"});
            DropIndex("dbo.LoginRoles", new[] {"Role_Key"});
            DropIndex("dbo.LoginRoles", new[] {"Login_ID"});
            DropIndex("dbo.LoginEvents", new[] {"Event_ID"});
            DropIndex("dbo.LoginEvents", new[] {"Login_ID"});
            DropIndex("dbo.LoginDepartaments", new[] {"Departament_ID"});
            DropIndex("dbo.LoginDepartaments", new[] {"Login_ID"});
            DropIndex("dbo.EventDepartaments", new[] {"Departament_ID"});
            DropIndex("dbo.EventDepartaments", new[] {"Event_ID"});
            DropIndex("dbo.Replaces", new[] {"Parent_Key"});
            DropIndex("dbo.Messages", new[] {"TargetID"});
            DropIndex("dbo.Messages", new[] {"AuthorID"});
            DropIndex("dbo.Posts", new[] {"RecordID"});
            DropIndex("dbo.People", new[] {"RecordID"});
            DropIndex("dbo.Contacts", new[] {"RecordID"});
            DropIndex("dbo.ContactRecords", new[] {"OrganizationID"});
            DropIndex("dbo.Events", new[] {"OrgID"});
            DropIndex("dbo.Events", new[] {"RecordID"});
            DropIndex("dbo.Departaments", new[] {"ParentID"});
            DropIndex("dbo.Comments", new[] {"OrganizationID"});
            DropIndex("dbo.Comments", new[] {"RecordID"});
            DropIndex("dbo.Comments", new[] {"AuthorID"});
            DropIndex("dbo.Addresses", new[] {"ParentID"});
            DropTable("dbo.AddressOrganizations");
            DropTable("dbo.OrganizationOrgStatus");
            DropTable("dbo.OrganizationOrgDirections");
            DropTable("dbo.LoginRoles");
            DropTable("dbo.LoginEvents");
            DropTable("dbo.LoginDepartaments");
            DropTable("dbo.EventDepartaments");
            DropTable("dbo.Replaces");
            DropTable("dbo.OrgStatus");
            DropTable("dbo.OrgDirections");
            DropTable("dbo.Roles");
            DropTable("dbo.Messages");
            DropTable("dbo.Posts");
            DropTable("dbo.People");
            DropTable("dbo.Contacts");
            DropTable("dbo.ContactRecords");
            DropTable("dbo.Events");
            DropTable("dbo.Departaments");
            DropTable("dbo.Logins");
            DropTable("dbo.Comments");
            DropTable("dbo.Organizations");
            DropTable("dbo.Addresses");
        }
    }
}