#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class work100317 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.People", "RecordID", "dbo.ContactRecords");
            DropForeignKey("dbo.Posts", "RecordID", "dbo.ContactRecords");
            DropIndex("dbo.People", new[] {"RecordID"});
            DropIndex("dbo.Posts", new[] {"RecordID"});
            CreateTable(
                    "dbo.OrgCloseInfoes",
                    c => new
                    {
                        ID = c.Guid(false),
                        ReasonID = c.Long(false),
                        Date = c.DateTime()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.OrgCloseTypes", t => t.ReasonID, true)
                .ForeignKey("dbo.Organizations", t => t.ID, true)
                .Index(t => t.ID)
                .Index(t => t.ReasonID);

            CreateTable(
                    "dbo.OrgCloseTypes",
                    c => new
                    {
                        ID = c.Long(false, true),
                        FullName = c.String(),
                        ShortName = c.String(),
                        Code = c.Int(false)
                    })
                .PrimaryKey(t => t.ID);

            CreateTable(
                    "dbo.OrgFounders",
                    c => new
                    {
                        ID = c.Guid(false),
                        WorkID = c.String(),
                        OrganizationID = c.Guid(),
                        TargetID = c.Guid(false),
                        PersonID = c.Guid(),
                        Price = c.Decimal(false, 18, 2),
                        OwnerRussia = c.Boolean(false)
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.OrganizationID)
                .ForeignKey("dbo.Organizations", t => t.TargetID, true)
                .ForeignKey("dbo.ContactRecords", t => t.PersonID)
                .Index(t => t.OrganizationID)
                .Index(t => t.TargetID)
                .Index(t => t.PersonID);

            CreateTable(
                    "dbo.Currencies",
                    c => new
                    {
                        ID = c.Guid(false),
                        CurrencyKey = c.String(false, 128),
                        OrganizationID = c.Guid(false),
                        Description = c.String(),
                        Type = c.Int(false),
                        Year = c.Decimal(false, 18, 2),
                        Single = c.Decimal(false, 18, 2)
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HelpCurrencies", t => t.CurrencyKey, true)
                .ForeignKey("dbo.Organizations", t => t.OrganizationID, true)
                .Index(t => t.CurrencyKey)
                .Index(t => t.OrganizationID);

            CreateTable(
                    "dbo.HelpCurrencies",
                    c => new
                    {
                        Key = c.String(false, 128),
                        NumCode = c.String(),
                        CharCode = c.String(),
                        Name = c.String(),
                        Order = c.Int(false)
                    })
                .PrimaryKey(t => t.Key);

            CreateTable(
                    "dbo.ForeignRegistrations",
                    c => new
                    {
                        ID = c.Guid(false),
                        CountryID = c.Long(),
                        Date = c.DateTime(),
                        Address = c.String(),
                        Number = c.String()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Oksms", t => t.CountryID)
                .ForeignKey("dbo.Organizations", t => t.ID, true)
                .Index(t => t.ID)
                .Index(t => t.CountryID);

            CreateTable(
                    "dbo.Oksms",
                    c => new
                    {
                        ID = c.Long(false, true),
                        Caption = c.String(),
                        Code = c.String()
                    })
                .PrimaryKey(t => t.ID);

            CreateTable(
                    "dbo.FssRegistrations",
                    c => new
                    {
                        ID = c.Guid(false),
                        Date = c.DateTime(),
                        Number = c.String(),
                        FssID = c.Long(false)
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Fsses", t => t.FssID, true)
                .ForeignKey("dbo.Organizations", t => t.ID, true)
                .Index(t => t.ID)
                .Index(t => t.FssID);

            CreateTable(
                    "dbo.Fsses",
                    c => new
                    {
                        ID = c.Long(false, true),
                        Caption = c.String(),
                        Code = c.String()
                    })
                .PrimaryKey(t => t.ID);

            CreateTable(
                    "dbo.Okved2",
                    c => new
                    {
                        ID = c.Long(false, true),
                        Code = c.String(),
                        Description = c.String(),
                        Caption = c.String(),
                        ParentID = c.Long()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Okved2", t => t.ParentID)
                .Index(t => t.ParentID);

            CreateTable(
                    "dbo.Okopfs",
                    c => new
                    {
                        ID = c.Long(false, true),
                        ParentID = c.Long(),
                        Caption = c.String(),
                        Suffix = c.String(),
                        Code = c.String()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Okopfs", t => t.ParentID)
                .Index(t => t.ParentID);

            CreateTable(
                    "dbo.ContactRecordPersons",
                    c => new
                    {
                        ContactRecord_ID = c.Guid(false),
                        Person_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.ContactRecord_ID, t.Person_ID})
                .ForeignKey("dbo.ContactRecords", t => t.ContactRecord_ID, true)
                .ForeignKey("dbo.People", t => t.Person_ID, true)
                .Index(t => t.ContactRecord_ID)
                .Index(t => t.Person_ID);

            CreateTable(
                    "dbo.ContactRecordPosts",
                    c => new
                    {
                        ContactRecord_ID = c.Guid(false),
                        Post_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.ContactRecord_ID, t.Post_ID})
                .ForeignKey("dbo.ContactRecords", t => t.ContactRecord_ID, true)
                .ForeignKey("dbo.Posts", t => t.Post_ID, true)
                .Index(t => t.ContactRecord_ID)
                .Index(t => t.Post_ID);

            CreateTable(
                    "dbo.Okved2Organization",
                    c => new
                    {
                        Okved2_ID = c.Long(false),
                        Organization_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.Okved2_ID, t.Organization_ID})
                .ForeignKey("dbo.Okved2", t => t.Okved2_ID, true)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, true)
                .Index(t => t.Okved2_ID)
                .Index(t => t.Organization_ID);

            AddColumn("dbo.HelpActivities", "Order", c => c.Int(false));
            AddColumn("dbo.Organizations", "Work", c => c.Boolean(false));
            AddColumn("dbo.Organizations", "OfConsultant", c => c.Boolean(false));
            AddColumn("dbo.Organizations", "WorkID", c => c.String());
            AddColumn("dbo.Organizations", "OkopfID", c => c.Long());
            AddColumn("dbo.Organizations", "Ogrn", c => c.String());
            AddColumn("dbo.Organizations", "OgrnDate", c => c.DateTime());
            AddColumn("dbo.Organizations", "Kpp", c => c.String());
            AddColumn("dbo.Organizations", "WorkDate", c => c.DateTime());
            AddColumn("dbo.Organizations", "MainOkved2_ID", c => c.Long());
            AddColumn("dbo.People", "WorkID", c => c.String());
            AddColumn("dbo.People", "FirstName", c => c.String());
            AddColumn("dbo.People", "MiddleName", c => c.String());
            AddColumn("dbo.People", "SurName", c => c.String());
            AddColumn("dbo.People", "Inn", c => c.String());
            AddColumn("dbo.Posts", "WorkID", c => c.String());
            CreateIndex("dbo.Organizations", "OkopfID");
            CreateIndex("dbo.Organizations", "MainOkved2_ID");
            AddForeignKey("dbo.Organizations", "MainOkved2_ID", "dbo.Okved2", "ID");
            AddForeignKey("dbo.Organizations", "OkopfID", "dbo.Okopfs", "ID");
            DropColumn("dbo.Organizations", "SingleCurrency");
            DropColumn("dbo.Organizations", "YearCurrency");
            DropColumn("dbo.MeetingResults", "BonusComment");
            DropColumn("dbo.MeetingResults", "Date");
            DropColumn("dbo.People", "RecordID");
        }

        public override void Down()
        {
            AddColumn("dbo.People", "RecordID", c => c.Guid());
            AddColumn("dbo.MeetingResults", "Date", c => c.DateTime(false));
            AddColumn("dbo.MeetingResults", "BonusComment", c => c.Decimal(false, 18, 2));
            AddColumn("dbo.Organizations", "YearCurrency", c => c.Decimal(false, 18, 2));
            AddColumn("dbo.Organizations", "SingleCurrency", c => c.Decimal(false, 18, 2));
            DropForeignKey("dbo.Organizations", "OkopfID", "dbo.Okopfs");
            DropForeignKey("dbo.Okopfs", "ParentID", "dbo.Okopfs");
            DropForeignKey("dbo.Okved2", "ParentID", "dbo.Okved2");
            DropForeignKey("dbo.Okved2Organization", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.Okved2Organization", "Okved2_ID", "dbo.Okved2");
            DropForeignKey("dbo.Organizations", "MainOkved2_ID", "dbo.Okved2");
            DropForeignKey("dbo.FssRegistrations", "ID", "dbo.Organizations");
            DropForeignKey("dbo.FssRegistrations", "FssID", "dbo.Fsses");
            DropForeignKey("dbo.ForeignRegistrations", "ID", "dbo.Organizations");
            DropForeignKey("dbo.ForeignRegistrations", "CountryID", "dbo.Oksms");
            DropForeignKey("dbo.Currencies", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.Currencies", "CurrencyKey", "dbo.HelpCurrencies");
            DropForeignKey("dbo.ContactRecordPosts", "Post_ID", "dbo.Posts");
            DropForeignKey("dbo.ContactRecordPosts", "ContactRecord_ID", "dbo.ContactRecords");
            DropForeignKey("dbo.ContactRecordPersons", "Person_ID", "dbo.People");
            DropForeignKey("dbo.ContactRecordPersons", "ContactRecord_ID", "dbo.ContactRecords");
            DropForeignKey("dbo.OrgFounders", "PersonID", "dbo.ContactRecords");
            DropForeignKey("dbo.OrgFounders", "TargetID", "dbo.Organizations");
            DropForeignKey("dbo.OrgFounders", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.OrgCloseInfoes", "ID", "dbo.Organizations");
            DropForeignKey("dbo.OrgCloseInfoes", "ReasonID", "dbo.OrgCloseTypes");
            DropIndex("dbo.Okved2Organization", new[] {"Organization_ID"});
            DropIndex("dbo.Okved2Organization", new[] {"Okved2_ID"});
            DropIndex("dbo.ContactRecordPosts", new[] {"Post_ID"});
            DropIndex("dbo.ContactRecordPosts", new[] {"ContactRecord_ID"});
            DropIndex("dbo.ContactRecordPersons", new[] {"Person_ID"});
            DropIndex("dbo.ContactRecordPersons", new[] {"ContactRecord_ID"});
            DropIndex("dbo.Okopfs", new[] {"ParentID"});
            DropIndex("dbo.Okved2", new[] {"ParentID"});
            DropIndex("dbo.FssRegistrations", new[] {"FssID"});
            DropIndex("dbo.FssRegistrations", new[] {"ID"});
            DropIndex("dbo.ForeignRegistrations", new[] {"CountryID"});
            DropIndex("dbo.ForeignRegistrations", new[] {"ID"});
            DropIndex("dbo.Currencies", new[] {"OrganizationID"});
            DropIndex("dbo.Currencies", new[] {"CurrencyKey"});
            DropIndex("dbo.OrgFounders", new[] {"PersonID"});
            DropIndex("dbo.OrgFounders", new[] {"TargetID"});
            DropIndex("dbo.OrgFounders", new[] {"OrganizationID"});
            DropIndex("dbo.OrgCloseInfoes", new[] {"ReasonID"});
            DropIndex("dbo.OrgCloseInfoes", new[] {"ID"});
            DropIndex("dbo.Organizations", new[] {"MainOkved2_ID"});
            DropIndex("dbo.Organizations", new[] {"OkopfID"});
            DropColumn("dbo.Posts", "WorkID");
            DropColumn("dbo.People", "Inn");
            DropColumn("dbo.People", "SurName");
            DropColumn("dbo.People", "MiddleName");
            DropColumn("dbo.People", "FirstName");
            DropColumn("dbo.People", "WorkID");
            DropColumn("dbo.Organizations", "MainOkved2_ID");
            DropColumn("dbo.Organizations", "WorkDate");
            DropColumn("dbo.Organizations", "Kpp");
            DropColumn("dbo.Organizations", "OgrnDate");
            DropColumn("dbo.Organizations", "Ogrn");
            DropColumn("dbo.Organizations", "OkopfID");
            DropColumn("dbo.Organizations", "WorkID");
            DropColumn("dbo.Organizations", "OfConsultant");
            DropColumn("dbo.Organizations", "Work");
            DropColumn("dbo.HelpActivities", "Order");
            DropTable("dbo.Okved2Organization");
            DropTable("dbo.ContactRecordPosts");
            DropTable("dbo.ContactRecordPersons");
            DropTable("dbo.Okopfs");
            DropTable("dbo.Okved2");
            DropTable("dbo.Fsses");
            DropTable("dbo.FssRegistrations");
            DropTable("dbo.Oksms");
            DropTable("dbo.ForeignRegistrations");
            DropTable("dbo.HelpCurrencies");
            DropTable("dbo.Currencies");
            DropTable("dbo.OrgFounders");
            DropTable("dbo.OrgCloseTypes");
            DropTable("dbo.OrgCloseInfoes");
            CreateIndex("dbo.Posts", "RecordID");
            CreateIndex("dbo.People", "RecordID");
            AddForeignKey("dbo.Posts", "RecordID", "dbo.ContactRecords", "ID", true);
            AddForeignKey("dbo.People", "RecordID", "dbo.ContactRecords", "ID", true);
        }
    }
}