#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class file_date : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PathFiles", "Date", c => c.DateTime());
        }

        public override void Down()
        {
            DropColumn("dbo.PathFiles", "Date");
        }
    }
}