#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class addr_timezone : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "TimeZone", c => c.Int(false));
        }

        public override void Down()
        {
            DropColumn("dbo.Addresses", "TimeZone");
        }
    }
}