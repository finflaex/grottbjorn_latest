#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class configs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RawFiles", "UserID", c => c.Guid());
            CreateIndex("dbo.RawFiles", "UserID");
            AddForeignKey("dbo.RawFiles", "UserID", "dbo.Logins", "ID", true);
        }

        public override void Down()
        {
            DropForeignKey("dbo.RawFiles", "UserID", "dbo.Logins");
            DropIndex("dbo.RawFiles", new[] {"UserID"});
            DropColumn("dbo.RawFiles", "UserID");
        }
    }
}