#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class consultantRates : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ConsultantResults", "Call_ID", "dbo.Events");
            DropForeignKey("dbo.ConsultantResults", "Meeting_ID", "dbo.Events");
            DropForeignKey("dbo.ConsultantResults", "ConsultantID", "dbo.Logins");
            DropForeignKey("dbo.ConsultantResults", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.Events", "ConsultantResult_ID", "dbo.ConsultantResults");
            DropForeignKey("dbo.Events", "EstimatingCall_ID", "dbo.ConsultantResults");
            DropIndex("dbo.Events", new[] {"ConsultantResult_ID"});
            DropIndex("dbo.Events", new[] {"EstimatingCall_ID"});
            DropIndex("dbo.ConsultantResults", new[] {"OrganizationID"});
            DropIndex("dbo.ConsultantResults", new[] {"ConsultantID"});
            DropIndex("dbo.ConsultantResults", new[] {"Call_ID"});
            DropIndex("dbo.ConsultantResults", new[] {"Meeting_ID"});
            CreateTable(
                    "dbo.ConsultantRates",
                    c => new
                    {
                        ID = c.Guid(false),
                        OkAnswer = c.Boolean(false),
                        Rating = c.Int(false),
                        Description = c.String(),
                        Bonus = c.Decimal(false, 18, 2)
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MeetingResults", t => t.ID, true)
                .Index(t => t.ID);

            AddColumn("dbo.MeetingResults", "OperatorID", c => c.Guid());
            CreateIndex("dbo.MeetingResults", "OperatorID");
            AddForeignKey("dbo.MeetingResults", "OperatorID", "dbo.Logins", "ID");
            DropColumn("dbo.Events", "ConsultantResult_ID");
            DropColumn("dbo.Events", "EstimatingCall_ID");
            DropTable("dbo.ConsultantResults");
        }

        public override void Down()
        {
            CreateTable(
                    "dbo.ConsultantResults",
                    c => new
                    {
                        ID = c.Guid(false),
                        OrganizationID = c.Guid(),
                        ConsultantID = c.Guid(),
                        Rate = c.Byte(false),
                        Call_ID = c.Guid(),
                        Meeting_ID = c.Guid()
                    })
                .PrimaryKey(t => t.ID);

            AddColumn("dbo.Events", "EstimatingCall_ID", c => c.Guid());
            AddColumn("dbo.Events", "ConsultantResult_ID", c => c.Guid());
            DropForeignKey("dbo.MeetingResults", "OperatorID", "dbo.Logins");
            DropForeignKey("dbo.ConsultantRates", "ID", "dbo.MeetingResults");
            DropIndex("dbo.ConsultantRates", new[] {"ID"});
            DropIndex("dbo.MeetingResults", new[] {"OperatorID"});
            DropColumn("dbo.MeetingResults", "OperatorID");
            DropTable("dbo.ConsultantRates");
            CreateIndex("dbo.ConsultantResults", "Meeting_ID");
            CreateIndex("dbo.ConsultantResults", "Call_ID");
            CreateIndex("dbo.ConsultantResults", "ConsultantID");
            CreateIndex("dbo.ConsultantResults", "OrganizationID");
            CreateIndex("dbo.Events", "EstimatingCall_ID");
            CreateIndex("dbo.Events", "ConsultantResult_ID");
            AddForeignKey("dbo.Events", "EstimatingCall_ID", "dbo.ConsultantResults", "ID");
            AddForeignKey("dbo.Events", "ConsultantResult_ID", "dbo.ConsultantResults", "ID");
            AddForeignKey("dbo.ConsultantResults", "OrganizationID", "dbo.Organizations", "ID");
            AddForeignKey("dbo.ConsultantResults", "ConsultantID", "dbo.Logins", "ID");
            AddForeignKey("dbo.ConsultantResults", "Meeting_ID", "dbo.Events", "ID");
            AddForeignKey("dbo.ConsultantResults", "Call_ID", "dbo.Events", "ID");
        }
    }
}