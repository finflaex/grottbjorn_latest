#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class mosru : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Okved2", "ParentID", "dbo.Okved2");
            DropForeignKey("dbo.Okopfs", "ParentID", "dbo.Okopfs");
            DropIndex("dbo.Okved2", new[] {"ParentID"});
            DropIndex("dbo.Okopfs", new[] {"ParentID"});
            AddColumn("dbo.HelpCurrencies", "Country_ID", c => c.Long());
            AddColumn("dbo.Oksms", "ShortName", c => c.String());
            AddColumn("dbo.Oksms", "FullName", c => c.String());
            AddColumn("dbo.Oksms", "ALFA2", c => c.String());
            AddColumn("dbo.Oksms", "ALFA3", c => c.String());
            AddColumn("dbo.Okved2", "Razdel", c => c.String());
            AddColumn("dbo.Okopfs", "Description", c => c.String());
            CreateIndex("dbo.HelpCurrencies", "Country_ID");
            AddForeignKey("dbo.HelpCurrencies", "Country_ID", "dbo.Oksms", "ID");
            DropColumn("dbo.Posts", "RecordID");
            DropColumn("dbo.HelpCurrencies", "CharCode");
            DropColumn("dbo.Oksms", "Caption");
            DropColumn("dbo.Okved2", "ParentID");
            DropColumn("dbo.Okopfs", "ParentID");
        }

        public override void Down()
        {
            AddColumn("dbo.Okopfs", "ParentID", c => c.Long());
            AddColumn("dbo.Okved2", "ParentID", c => c.Long());
            AddColumn("dbo.Oksms", "Caption", c => c.String());
            AddColumn("dbo.HelpCurrencies", "CharCode", c => c.String());
            AddColumn("dbo.Posts", "RecordID", c => c.Guid());
            DropForeignKey("dbo.HelpCurrencies", "Country_ID", "dbo.Oksms");
            DropIndex("dbo.HelpCurrencies", new[] {"Country_ID"});
            DropColumn("dbo.Okopfs", "Description");
            DropColumn("dbo.Okved2", "Razdel");
            DropColumn("dbo.Oksms", "ALFA3");
            DropColumn("dbo.Oksms", "ALFA2");
            DropColumn("dbo.Oksms", "FullName");
            DropColumn("dbo.Oksms", "ShortName");
            DropColumn("dbo.HelpCurrencies", "Country_ID");
            CreateIndex("dbo.Okopfs", "ParentID");
            CreateIndex("dbo.Okved2", "ParentID");
            AddForeignKey("dbo.Okopfs", "ParentID", "dbo.Okopfs", "ID");
            AddForeignKey("dbo.Okved2", "ParentID", "dbo.Okved2", "ID");
        }
    }
}