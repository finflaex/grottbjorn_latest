#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class labels2 : DbMigration
    {
        public override void Up()
        {
            RenameTable("dbo.OrganizationLabels", "LabelOrganizations");
            DropPrimaryKey("dbo.LabelOrganizations");
            AddPrimaryKey("dbo.LabelOrganizations", new[] {"Label_ID", "Organization_ID"});
        }

        public override void Down()
        {
            DropPrimaryKey("dbo.LabelOrganizations");
            AddPrimaryKey("dbo.LabelOrganizations", new[] {"Organization_ID", "Label_ID"});
            RenameTable("dbo.LabelOrganizations", "OrganizationLabels");
        }
    }
}