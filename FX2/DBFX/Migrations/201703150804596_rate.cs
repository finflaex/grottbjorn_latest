#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class rate : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Comments", "AuthorID", "UpdateAuthorID");
            RenameColumn("dbo.Messages", "AuthorID", "UpdateAuthorID");
            RenameIndex("dbo.Comments", "IX_AuthorID", "IX_UpdateAuthorID");
            RenameIndex("dbo.Messages", "IX_AuthorID", "IX_UpdateAuthorID");
            CreateTable(
                    "dbo.ConsultantResults",
                    c => new
                    {
                        ID = c.Guid(false),
                        OrganizationID = c.Guid(),
                        ConsultantID = c.Guid(),
                        Rate = c.Byte(false),
                        Call_ID = c.Guid(),
                        Meeting_ID = c.Guid()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Events", t => t.Call_ID)
                .ForeignKey("dbo.Events", t => t.Meeting_ID)
                .ForeignKey("dbo.Logins", t => t.ConsultantID)
                .ForeignKey("dbo.Organizations", t => t.OrganizationID)
                .Index(t => t.OrganizationID)
                .Index(t => t.ConsultantID)
                .Index(t => t.Call_ID)
                .Index(t => t.Meeting_ID);

            AddColumn("dbo.Events", "UpdateAuthorID", c => c.Guid());
            AddColumn("dbo.Events", "CreateAuthorID", c => c.Guid());
            AddColumn("dbo.Events", "ConsultantResult_ID", c => c.Guid());
            AddColumn("dbo.Events", "EstimatingCall_ID", c => c.Guid());
            AddColumn("dbo.Events", "Prev_ID", c => c.Guid());
            AddColumn("dbo.OrgInterests", "UpdateAuthorID", c => c.Guid());
            AddColumn("dbo.MeetingResults", "CreateAuthorID", c => c.Guid());
            AddColumn("dbo.MeetingResults", "UpdateAuthorID", c => c.Guid());
            CreateIndex("dbo.Events", "ConsultantResult_ID");
            CreateIndex("dbo.Events", "EstimatingCall_ID");
            CreateIndex("dbo.Events", "Prev_ID");
            AddForeignKey("dbo.Events", "ConsultantResult_ID", "dbo.ConsultantResults", "ID");
            AddForeignKey("dbo.Events", "EstimatingCall_ID", "dbo.ConsultantResults", "ID");
            AddForeignKey("dbo.Events", "Prev_ID", "dbo.Events", "ID");
            DropColumn("dbo.Events", "AuthorID");
            DropColumn("dbo.MeetingResults", "AuthorID");
        }

        public override void Down()
        {
            AddColumn("dbo.MeetingResults", "AuthorID", c => c.Guid());
            AddColumn("dbo.Events", "AuthorID", c => c.Guid());
            DropForeignKey("dbo.ConsultantResults", "OrganizationID", "dbo.Organizations");
            DropForeignKey("dbo.ConsultantResults", "ConsultantID", "dbo.Logins");
            DropForeignKey("dbo.Events", "Prev_ID", "dbo.Events");
            DropForeignKey("dbo.Events", "EstimatingCall_ID", "dbo.ConsultantResults");
            DropForeignKey("dbo.Events", "ConsultantResult_ID", "dbo.ConsultantResults");
            DropForeignKey("dbo.ConsultantResults", "Meeting_ID", "dbo.Events");
            DropForeignKey("dbo.ConsultantResults", "Call_ID", "dbo.Events");
            DropIndex("dbo.ConsultantResults", new[] {"Meeting_ID"});
            DropIndex("dbo.ConsultantResults", new[] {"Call_ID"});
            DropIndex("dbo.ConsultantResults", new[] {"ConsultantID"});
            DropIndex("dbo.ConsultantResults", new[] {"OrganizationID"});
            DropIndex("dbo.Events", new[] {"Prev_ID"});
            DropIndex("dbo.Events", new[] {"EstimatingCall_ID"});
            DropIndex("dbo.Events", new[] {"ConsultantResult_ID"});
            DropColumn("dbo.MeetingResults", "UpdateAuthorID");
            DropColumn("dbo.MeetingResults", "CreateAuthorID");
            DropColumn("dbo.OrgInterests", "UpdateAuthorID");
            DropColumn("dbo.Events", "Prev_ID");
            DropColumn("dbo.Events", "EstimatingCall_ID");
            DropColumn("dbo.Events", "ConsultantResult_ID");
            DropColumn("dbo.Events", "CreateAuthorID");
            DropColumn("dbo.Events", "UpdateAuthorID");
            DropTable("dbo.ConsultantResults");
            RenameIndex("dbo.Messages", "IX_UpdateAuthorID", "IX_AuthorID");
            RenameIndex("dbo.Comments", "IX_UpdateAuthorID", "IX_AuthorID");
            RenameColumn("dbo.Messages", "UpdateAuthorID", "AuthorID");
            RenameColumn("dbo.Comments", "UpdateAuthorID", "AuthorID");
        }
    }
}