#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class cfg2 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.RawFiles", new[] {"EventID"});
            AlterColumn("dbo.RawFiles", "EventID", c => c.Guid());
            CreateIndex("dbo.RawFiles", "EventID");
        }

        public override void Down()
        {
            DropIndex("dbo.RawFiles", new[] {"EventID"});
            AlterColumn("dbo.RawFiles", "EventID", c => c.Guid(false));
            CreateIndex("dbo.RawFiles", "EventID");
        }
    }
}