#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class test : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "ContactID", c => c.Guid());
            CreateIndex("dbo.Events", "ContactID");
            AddForeignKey("dbo.Events", "ContactID", "dbo.Contacts", "ID");
            DropColumn("dbo.Contacts", "Refer");
        }

        public override void Down()
        {
            AddColumn("dbo.Contacts", "Refer", c => c.Int(false));
            DropForeignKey("dbo.Events", "ContactID", "dbo.Contacts");
            DropIndex("dbo.Events", new[] {"ContactID"});
            DropColumn("dbo.Events", "ContactID");
        }
    }
}