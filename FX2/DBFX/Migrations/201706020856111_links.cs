#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class links : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.Links",
                    c => new
                    {
                        ID = c.Guid(false),
                        FirstID = c.Guid(),
                        SecondID = c.Guid(),
                        Description = c.String(),
                        Type = c.Int(false)
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.FirstID)
                .ForeignKey("dbo.Organizations", t => t.SecondID)
                .Index(t => t.FirstID)
                .Index(t => t.SecondID);
        }

        public override void Down()
        {
            DropForeignKey("dbo.Links", "SecondID", "dbo.Organizations");
            DropForeignKey("dbo.Links", "FirstID", "dbo.Organizations");
            DropIndex("dbo.Links", new[] {"SecondID"});
            DropIndex("dbo.Links", new[] {"FirstID"});
            DropTable("dbo.Links");
        }
    }
}