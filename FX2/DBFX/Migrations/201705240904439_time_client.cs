#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class time_client : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logins", "TimeClient", c => c.Boolean(false));
        }

        public override void Down()
        {
            DropColumn("dbo.Logins", "TimeClient");
        }
    }
}