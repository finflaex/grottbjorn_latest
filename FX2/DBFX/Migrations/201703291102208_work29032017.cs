#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class work29032017 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Currencies", "CurrencyKey", "dbo.HelpCurrencies");
            AddForeignKey("dbo.Currencies", "CurrencyKey", "dbo.HelpCurrencies", "Key");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Currencies", "CurrencyKey", "dbo.HelpCurrencies");
            AddForeignKey("dbo.Currencies", "CurrencyKey", "dbo.HelpCurrencies", "Key", true);
        }
    }
}