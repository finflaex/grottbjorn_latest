#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class activity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrganizationOrgDirections", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.OrganizationOrgDirections", "OrgDirection_Key", "dbo.OrgDirections");
            DropForeignKey("dbo.Events", "OrgID", "dbo.Organizations");
            DropIndex("dbo.OrganizationOrgDirections", new[] {"Organization_ID"});
            DropIndex("dbo.OrganizationOrgDirections", new[] {"OrgDirection_Key"});
            CreateTable(
                    "dbo.Activities",
                    c => new
                    {
                        ID = c.Guid(false),
                        DepartamentID = c.Guid(),
                        AddressID = c.Guid(false),
                        ActivityID = c.String(false, 128),
                        DateCreate = c.DateTimeOffset(false, 7),
                        DateModify = c.DateTimeOffset(false, 7),
                        Description = c.String()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HelpActivities", t => t.ActivityID, true)
                .ForeignKey("dbo.Addresses", t => t.AddressID, true)
                .ForeignKey("dbo.Departaments", t => t.DepartamentID)
                .Index(t => t.DepartamentID)
                .Index(t => t.AddressID)
                .Index(t => t.ActivityID);

            CreateTable(
                    "dbo.HelpActivities",
                    c => new
                    {
                        Key = c.String(false, 128),
                        Caption = c.String(),
                        Description = c.String()
                    })
                .PrimaryKey(t => t.Key);

            CreateTable(
                    "dbo.OrgInterests",
                    c => new
                    {
                        ID = c.Guid(false),
                        OrgID = c.Guid(false),
                        Type = c.Int(false),
                        ActivityID = c.Guid(false),
                        AuthorID = c.Guid(),
                        DateCreate = c.DateTimeOffset(false, 7),
                        DateModify = c.DateTimeOffset(false, 7),
                        Description = c.String()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.OrgID, true)
                .ForeignKey("dbo.Activities", t => t.ActivityID, true)
                .Index(t => t.OrgID)
                .Index(t => t.ActivityID);

            CreateTable(
                    "dbo.OrgInterestEvents",
                    c => new
                    {
                        OrgInterest_ID = c.Guid(false),
                        Event_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.OrgInterest_ID, t.Event_ID})
                .ForeignKey("dbo.OrgInterests", t => t.OrgInterest_ID, true)
                .ForeignKey("dbo.Events", t => t.Event_ID, true)
                .Index(t => t.OrgInterest_ID)
                .Index(t => t.Event_ID);

            AddForeignKey("dbo.Events", "OrgID", "dbo.Organizations", "ID");
            DropTable("dbo.OrgDirections");
            DropTable("dbo.OrganizationOrgDirections");
        }

        public override void Down()
        {
            CreateTable(
                    "dbo.OrganizationOrgDirections",
                    c => new
                    {
                        Organization_ID = c.Guid(false),
                        OrgDirection_Key = c.String(false, 128)
                    })
                .PrimaryKey(t => new {t.Organization_ID, t.OrgDirection_Key});

            CreateTable(
                    "dbo.OrgDirections",
                    c => new
                    {
                        Key = c.String(false, 128),
                        Caption = c.String(),
                        Description = c.String()
                    })
                .PrimaryKey(t => t.Key);

            DropForeignKey("dbo.Events", "OrgID", "dbo.Organizations");
            DropForeignKey("dbo.OrgInterests", "ActivityID", "dbo.Activities");
            DropForeignKey("dbo.Activities", "DepartamentID", "dbo.Departaments");
            DropForeignKey("dbo.Activities", "AddressID", "dbo.Addresses");
            DropForeignKey("dbo.OrgInterests", "OrgID", "dbo.Organizations");
            DropForeignKey("dbo.OrgInterestEvents", "Event_ID", "dbo.Events");
            DropForeignKey("dbo.OrgInterestEvents", "OrgInterest_ID", "dbo.OrgInterests");
            DropForeignKey("dbo.Activities", "ActivityID", "dbo.HelpActivities");
            DropIndex("dbo.OrgInterestEvents", new[] {"Event_ID"});
            DropIndex("dbo.OrgInterestEvents", new[] {"OrgInterest_ID"});
            DropIndex("dbo.OrgInterests", new[] {"ActivityID"});
            DropIndex("dbo.OrgInterests", new[] {"OrgID"});
            DropIndex("dbo.Activities", new[] {"ActivityID"});
            DropIndex("dbo.Activities", new[] {"AddressID"});
            DropIndex("dbo.Activities", new[] {"DepartamentID"});
            DropTable("dbo.OrgInterestEvents");
            DropTable("dbo.OrgInterests");
            DropTable("dbo.HelpActivities");
            DropTable("dbo.Activities");
            CreateIndex("dbo.OrganizationOrgDirections", "OrgDirection_Key");
            CreateIndex("dbo.OrganizationOrgDirections", "Organization_ID");
            AddForeignKey("dbo.Events", "OrgID", "dbo.Organizations", "ID", true);
            AddForeignKey("dbo.OrganizationOrgDirections", "OrgDirection_Key", "dbo.OrgDirections", "Key", true);
            AddForeignKey("dbo.OrganizationOrgDirections", "Organization_ID", "dbo.Organizations", "ID", true);
        }
    }
}