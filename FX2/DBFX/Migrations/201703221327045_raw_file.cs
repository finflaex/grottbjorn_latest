#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class raw_file : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.RawFiles",
                    c => new
                    {
                        ID = c.Guid(false),
                        EventID = c.Guid(false),
                        Raw = c.Binary(),
                        Size = c.Int(false),
                        CreateAuthorID = c.Guid(),
                        DateCreate = c.DateTimeOffset(false, 7),
                        DateModify = c.DateTimeOffset(false, 7),
                        Name = c.String(),
                        Type = c.Int(false)
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Events", t => t.EventID, true)
                .Index(t => t.EventID);
        }

        public override void Down()
        {
            DropForeignKey("dbo.RawFiles", "EventID", "dbo.Events");
            DropIndex("dbo.RawFiles", new[] {"EventID"});
            DropTable("dbo.RawFiles");
        }
    }
}