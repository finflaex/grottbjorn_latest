#region

using System.Data.Entity.Migrations;
using BaseDBFX.Base;

#endregion

namespace BaseDBFX.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DBFX>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DBFX context)
        {
            DBFX.Initialize(context);
        }
    }
}