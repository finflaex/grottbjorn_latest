#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class physicaddr : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.AddressOrganization1",
                    c => new
                    {
                        Address_ID = c.Guid(false),
                        Organization_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.Address_ID, t.Organization_ID})
                .ForeignKey("dbo.Addresses", t => t.Address_ID, true)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, true)
                .Index(t => t.Address_ID)
                .Index(t => t.Organization_ID);
        }

        public override void Down()
        {
            DropForeignKey("dbo.AddressOrganization1", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.AddressOrganization1", "Address_ID", "dbo.Addresses");
            DropIndex("dbo.AddressOrganization1", new[] {"Organization_ID"});
            DropIndex("dbo.AddressOrganization1", new[] {"Address_ID"});
            DropTable("dbo.AddressOrganization1");
        }
    }
}