#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class labels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.Labels",
                    c => new
                    {
                        ID = c.Guid(false),
                        UserID = c.Guid(),
                        Caption = c.String(),
                        Type = c.Int(false),
                        Order = c.Int(false),
                        Description = c.String()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Logins", t => t.UserID, true)
                .Index(t => t.UserID);

            CreateTable(
                    "dbo.OrganizationLabels",
                    c => new
                    {
                        Organization_ID = c.Guid(false),
                        Label_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.Organization_ID, t.Label_ID})
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, true)
                .ForeignKey("dbo.Labels", t => t.Label_ID, true)
                .Index(t => t.Organization_ID)
                .Index(t => t.Label_ID);
        }

        public override void Down()
        {
            DropForeignKey("dbo.OrganizationLabels", "Label_ID", "dbo.Labels");
            DropForeignKey("dbo.OrganizationLabels", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.Labels", "UserID", "dbo.Logins");
            DropIndex("dbo.OrganizationLabels", new[] {"Label_ID"});
            DropIndex("dbo.OrganizationLabels", new[] {"Organization_ID"});
            DropIndex("dbo.Labels", new[] {"UserID"});
            DropTable("dbo.OrganizationLabels");
            DropTable("dbo.Labels");
        }
    }
}