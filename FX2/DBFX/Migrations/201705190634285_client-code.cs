#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class clientcode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Organizations", "ClientCode", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.Organizations", "ClientCode");
        }
    }
}