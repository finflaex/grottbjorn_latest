#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class event_contact_order : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "Order", c => c.Int(false));
            AddColumn("dbo.ContactRecords", "Order", c => c.Int(false));
        }

        public override void Down()
        {
            DropColumn("dbo.ContactRecords", "Order");
            DropColumn("dbo.Events", "Order");
        }
    }
}