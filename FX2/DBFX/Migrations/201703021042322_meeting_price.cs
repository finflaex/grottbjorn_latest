#region

using System.Data.Entity.Migrations;

#endregion

namespace BaseDBFX.Migrations
{
    public partial class meeting_price : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "AuthorID", "dbo.Logins");
            DropForeignKey("dbo.Messages", "TargetID", "dbo.Logins");
            CreateTable(
                    "dbo.MeetingResults",
                    c => new
                    {
                        ID = c.Guid(false),
                        DateCreate = c.DateTimeOffset(false, 7),
                        AuthorID = c.Guid(),
                        DateModify = c.DateTimeOffset(false, 7),
                        Description = c.String(),
                        Distance = c.Int(false),
                        Cost = c.Decimal(false, 18, 2),
                        LPR = c.Boolean(false),
                        Bonus = c.Decimal(false, 18, 2),
                        BonusComment = c.Decimal(false, 18, 2),
                        Date = c.DateTime(false),
                        RecordID = c.Guid()
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Events", t => t.ID, true)
                .ForeignKey("dbo.ContactRecords", t => t.RecordID)
                .Index(t => t.ID)
                .Index(t => t.RecordID);

            CreateTable(
                    "dbo.MeetingYears",
                    c => new
                    {
                        ID = c.Guid(false),
                        Value = c.Decimal(false, 18, 2),
                        DepartamentID = c.Guid(false)
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Departaments", t => t.DepartamentID, true)
                .Index(t => t.DepartamentID);

            CreateTable(
                    "dbo.MeetingPrices",
                    c => new
                    {
                        ID = c.Guid(false),
                        Value = c.Decimal(false, 18, 2),
                        HasLPR = c.Boolean(false),
                        PorogRaz = c.Boolean(false),
                        YearID = c.Guid(false),
                        DateCreate = c.DateTimeOffset(false, 7)
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.MeetingYears", t => t.YearID, true)
                .Index(t => t.YearID);

            CreateTable(
                    "dbo.LoginMeetingResults",
                    c => new
                    {
                        Login_ID = c.Guid(false),
                        MeetingResult_ID = c.Guid(false)
                    })
                .PrimaryKey(t => new {t.Login_ID, t.MeetingResult_ID})
                .ForeignKey("dbo.Logins", t => t.Login_ID, true)
                .ForeignKey("dbo.MeetingResults", t => t.MeetingResult_ID, true)
                .Index(t => t.Login_ID)
                .Index(t => t.MeetingResult_ID);

            AddColumn("dbo.Organizations", "SingleCurrency", c => c.Decimal(false, 18, 2));
            AddColumn("dbo.Organizations", "YearCurrency", c => c.Decimal(false, 18, 2));
            AddColumn("dbo.Departaments", "MinCurrencyRaz", c => c.Decimal(false, 18, 2));
            AddColumn("dbo.Events", "NotifyConsultant", c => c.Boolean(false));
            AddForeignKey("dbo.Comments", "AuthorID", "dbo.Logins", "ID", true);
            AddForeignKey("dbo.Messages", "TargetID", "dbo.Logins", "ID", true);
            DropColumn("dbo.Events", "CallStatus");
        }

        public override void Down()
        {
            AddColumn("dbo.Events", "CallStatus", c => c.Int(false));
            DropForeignKey("dbo.Messages", "TargetID", "dbo.Logins");
            DropForeignKey("dbo.Comments", "AuthorID", "dbo.Logins");
            DropForeignKey("dbo.LoginMeetingResults", "MeetingResult_ID", "dbo.MeetingResults");
            DropForeignKey("dbo.LoginMeetingResults", "Login_ID", "dbo.Logins");
            DropForeignKey("dbo.MeetingPrices", "YearID", "dbo.MeetingYears");
            DropForeignKey("dbo.MeetingYears", "DepartamentID", "dbo.Departaments");
            DropForeignKey("dbo.MeetingResults", "RecordID", "dbo.ContactRecords");
            DropForeignKey("dbo.MeetingResults", "ID", "dbo.Events");
            DropIndex("dbo.LoginMeetingResults", new[] {"MeetingResult_ID"});
            DropIndex("dbo.LoginMeetingResults", new[] {"Login_ID"});
            DropIndex("dbo.MeetingPrices", new[] {"YearID"});
            DropIndex("dbo.MeetingYears", new[] {"DepartamentID"});
            DropIndex("dbo.MeetingResults", new[] {"RecordID"});
            DropIndex("dbo.MeetingResults", new[] {"ID"});
            DropColumn("dbo.Events", "NotifyConsultant");
            DropColumn("dbo.Departaments", "MinCurrencyRaz");
            DropColumn("dbo.Organizations", "YearCurrency");
            DropColumn("dbo.Organizations", "SingleCurrency");
            DropTable("dbo.LoginMeetingResults");
            DropTable("dbo.MeetingPrices");
            DropTable("dbo.MeetingYears");
            DropTable("dbo.MeetingResults");
            AddForeignKey("dbo.Messages", "TargetID", "dbo.Logins", "ID");
            AddForeignKey("dbo.Comments", "AuthorID", "dbo.Logins", "ID");
        }
    }
}