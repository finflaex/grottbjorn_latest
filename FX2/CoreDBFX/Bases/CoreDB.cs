#region

using System.Data.Entity;
using CoreDBFX.Tables;

#endregion

namespace CoreDBFX.Bases
{
    public class Core47 : DbContext
    {
        public Core47()
            : base(
                //$@"Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True;Initial Catalog={nameof(Core47)};"
                // $@"Data Source=mars;Integrated Security=False;User ID=sa;Password=411;Database={nameof(Core47)}_test;"
                //$@"Data Source=mars;Integrated Security=False;User ID=sa;Password=411;Database={nameof(Core47)};"
                $@"Data Source=kent;Integrated Security=False;User ID=dbfx;Password=dbfx123db;Database={nameof(Core47)};"
                  )
        {
        }

        public virtual DbSet<DynValue> DynValues { get; set; }
        public virtual DbSet<Key> Keys { get; set; }
        public virtual DbSet<Link> Links { get; set; }
        public virtual DbSet<Value> Values { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Key>()
                .HasMany(e => e.DynValues)
                .WithRequired(e => e.Key1)
                .HasForeignKey(e => e.ParentID);

            modelBuilder.Entity<Key>()
                .HasMany(e => e.Links)
                .WithRequired(e => e.Key)
                .HasForeignKey(e => e.ParentID);

            modelBuilder.Entity<Value>()
                .HasMany(e => e.DynValues)
                .WithOptional(e => e.Value)
                .HasForeignKey(e => e.tbValueID);
        }
    }
}