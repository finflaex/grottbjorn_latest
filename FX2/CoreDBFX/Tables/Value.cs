#region

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

#endregion

namespace CoreDBFX.Tables
{
    public class Value
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Value()
        {
            DynValues = new HashSet<DynValue>();
            Links = new HashSet<Link>();
        }

        public Guid ID { get; set; }

        public bool BoolValue { get; set; }

        public string Caption { get; set; }

        public Guid? CreateAuthorID { get; set; }

        public DateTimeOffset DateCreate { get; set; }

        [Required]
        public string Discriminator { get; set; }

        public int IntValue { get; set; }

        public string Key { get; set; }

        public int Order { get; set; }

        public byte[] RawValue { get; set; }

        public string StringValue { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DynValue> DynValues { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Link> Links { get; set; }
    }
}