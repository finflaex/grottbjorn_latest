#region

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

#endregion

namespace CoreDBFX.Tables
{
    public class Key
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Key()
        {
            DynValues = new HashSet<DynValue>();
            Links = new HashSet<Link>();
        }

        public Guid ID { get; set; }

        public Guid? CreateAuthorID { get; set; }

        public DateTimeOffset DateCreate { get; set; }

        [Required]
        public string Discriminator { get; set; }

        public DateTimeOffset? End { get; set; }

        [Column("Key")]
        public string Key1 { get; set; }

        public int Order { get; set; }

        public DateTimeOffset? Start { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DynValue> DynValues { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Link> Links { get; set; }
    }
}