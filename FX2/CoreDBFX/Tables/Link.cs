#region

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace CoreDBFX.Tables
{
    public class Link
    {
        public Guid ID { get; set; }

        public Guid? CreateAuthorID { get; set; }

        public DateTimeOffset DateCreate { get; set; }

        public DateTimeOffset DateModify { get; set; }

        [Required]
        public string Discriminator { get; set; }

        public DateTimeOffset? End { get; set; }

        public Guid ParentID { get; set; }

        public DateTimeOffset? Start { get; set; }

        public Guid? UpdateAuthorID { get; set; }

        public Guid ValueID { get; set; }

        public virtual Key Key { get; set; }

        public virtual Value Value { get; set; }
    }
}