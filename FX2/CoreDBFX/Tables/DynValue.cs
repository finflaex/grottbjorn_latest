#region

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace CoreDBFX.Tables
{
    public class DynValue
    {
        public Guid ID { get; set; }

        public bool BoolValue { get; set; }

        public string Caption { get; set; }

        public Guid? CreateAuthorID { get; set; }

        public DateTimeOffset DateCreate { get; set; }

        public DateTimeOffset DateModify { get; set; }

        [Required]
        public string Discriminator { get; set; }

        public DateTimeOffset? End { get; set; }

        public int IntValue { get; set; }

        public string Key { get; set; }

        public int Order { get; set; }

        public Guid ParentID { get; set; }

        public byte[] RawValue { get; set; }

        public DateTimeOffset? Start { get; set; }

        public string StringValue { get; set; }

        public Guid? UpdateAuthorID { get; set; }

        public Guid? tbValueID { get; set; }

        public virtual Key Key1 { get; set; }

        public virtual Value Value { get; set; }
    }
}