﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.X509;
using Security;
using Org.BouncyCastle.Utilities.IO.Pem;
using Org.BouncyCastle.Crypto;

namespace TestCertConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var sn = @"g:\dsWikis.xml.base64.sgn";

            var raw = File.ReadAllBytes(sn);
            var base64Content = Encoding
                .Default
                .GetString(raw)
                .Replace("-----BEGIN PKCS7-----", "")
                .Replace("-----END PKCS7-----", "")
                .Replace("\r", "").Replace("\n", "");
            byte[] decodedContent = Convert.FromBase64String(base64Content);

            

            Console.ReadKey();
        }
    }
}
