﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Security
{
    public class CSecurity
    {
        private byte[] iv;
        private byte[] key;
        private ASCIIEncoding TextConverter = new ASCIIEncoding();

        public CSecurity(string Key)
        {
            for (int i = Key.Length; i < 32; i++)
                Key += '0';
            this.key = GetTextBytes(Key);
            SHA256 SHA = SHA256.Create();
            this.iv = SHA.ComputeHash(this.key);
        }

        public byte[] GetTextBytes(string StrIn)
        {
            char[] arrChar = StrIn.ToCharArray();
            byte[] Result = System.Text.UnicodeEncoding.GetEncoding("windows-1251").GetBytes(arrChar);

            return Result;
        }

        public string GetTextString(byte[] ByteIn)
        {
            string Result = System.Text.UnicodeEncoding.GetEncoding("windows-1251").GetString(ByteIn);
            return Result;
        }

        public byte[] Encrypt(byte[] Data)
        {
            Rijndael RijndaelMngr = new RijndaelManaged();
            RijndaelMngr.Mode = CipherMode.CBC;
            RijndaelMngr.BlockSize = 256;
            RijndaelMngr.KeySize = 256;
            ICryptoTransform ICryptoTransf = RijndaelMngr.CreateEncryptor(this.key, this.iv);
            MemoryStream MemoryStrm = new MemoryStream();
            CryptoStream CryptoStrm = new CryptoStream(MemoryStrm, ICryptoTransf, CryptoStreamMode.Write);
            CryptoStrm.Write(Data, 0, Data.Length);
            CryptoStrm.FlushFinalBlock();
            byte[] Result = MemoryStrm.ToArray();
            CryptoStrm.Close();
            CryptoStrm.Clear();
            RijndaelMngr.Clear();

            return Result;
        }

        public byte[] Decrypt(byte[] Data)
        {
            Rijndael RijndaelMngr = new RijndaelManaged();
            RijndaelMngr.Mode = CipherMode.CBC;
            RijndaelMngr.BlockSize = 256;
            RijndaelMngr.KeySize = 256;
            ICryptoTransform ICryptoTransf = RijndaelMngr.CreateDecryptor(this.key, this.iv);
            MemoryStream MemoryStrm = new MemoryStream(Data);
            CryptoStream CryptoStrm = new CryptoStream(MemoryStrm, ICryptoTransf, CryptoStreamMode.Read);
            byte[] Result = new byte[Data.Length];
            CryptoStrm.Read(Result, 0, Result.Length);
            CryptoStrm.Close();
            CryptoStrm.Clear();
            RijndaelMngr.Clear();

            return Result;
        }
    }
}
