﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

namespace BaseGBFX.Tables
{
    public class Comment : fxaHistoryTable, fxiDescriptionTable, fxiAuthorTable
    {
        public CommentType Type { get; set; }
        public string Description { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual Person Person { get; set; }
        public virtual ContactDetails Contact { get; set; }
        public virtual Post Post { get; set; }
        public virtual Departament Departament { get; set; }
        public Guid Author { get; set; }

        [ForeignKey(nameof(Author))]
        public virtual Login UserAuthor { get; set; }
    }
}
