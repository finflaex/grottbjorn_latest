﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class Flat : fxaKeyTable
    {
        public string Number { get; set; }
        public House House { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
