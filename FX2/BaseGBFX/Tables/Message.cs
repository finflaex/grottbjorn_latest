﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

namespace BaseGBFX.Tables
{
    public class Message : fxaKeyTable
    {
        public bool Show { get; set; }
        public bool Actual { get; set; }

        public DateTime Date { get; set; }

        public Login AuthorUser { get; set; }
        public string AuthorService { get; set; }

        public Login TargetUser { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
    }
}
