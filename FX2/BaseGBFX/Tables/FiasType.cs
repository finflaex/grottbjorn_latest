﻿using System.ComponentModel;

namespace BaseGBFX.Tables
{
    public enum FiasType
    {
        [Description("регион")]
        Region = 1,
        [Description("автономный_округ")]
        Autonomous_Okrug = 2,
        [Description("район")]
        Area = 3,
        [Description("населённый пункт")]
        Settlement = 35,
        [Description("город")]
        City = 4,
        [Description("внутригородская территория")]
        IntracityArea = 5,
        [Description("населенный пункт")]
        Locality = 6,
        [Description("планировочная структура")]
        PlanningStructure = 65,
        [Description("улица")]
        Street = 7,
        [Description("земельный участок")]
        Section = 75,
        [Description("здание")]
        House = 8,
        [Description("корпус")]
        Housing = 9,
        [Description("комната")]
        Room = 90,
        [Description("угол комнаты")]
        CornerOfTheRoom = 91
    }
}