﻿#region

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.Reflection;

#endregion

namespace BaseGBFX.Tables
{
    public class Login : fxaHistoryTable, fxiCaptionTable
    {
        public Login()
        {
            Roles = new HashSet<Role>();
            Sessions = new HashSet<Session>();
            OrgAccaunted = new HashSet<LoginAccessOrganization>();
            TaskAccaunted = new HashSet<LoginAccessTaskEvent>();
            Configs = new HashSet<UserConfig>();
        }
        public string Name { get; set; }

        [Column("Password")]
        public byte[] SysPassword { get; set; }

        [NotMapped]
        public string Password
        {
            get { return SysPassword.Decrypt().ToString(Encoding.UTF8); }
            set { SysPassword = value.ToBytes(Encoding.UTF8).Encrypt(); }
        }

        public string Caption { get; set; }

        //public virtual Person Person { get; set; }
        public virtual ICollection<Session> Sessions { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<LoginAccessOrganization> OrgAccaunted { get; set; }
        public virtual ICollection<LoginAccessTaskEvent> TaskAccaunted { get; set; }
        public virtual Departament Departament { get; set; }
        public virtual ICollection<UserConfig> Configs { get; set; }
        public int OldId { get; set; }
        public virtual Person Person { get; set; }
        public bool Block { get; set; }

        public virtual ICollection<Message> SendingMessages { get; set; }
        public virtual ICollection<Message> RecieveMessages { get; set; }
    }
}