﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class CustomDeclaration : fxaKeyTable
    {
        public string Currency { get; set; }
        public float Curs { get; set; }
        public DateTime Date { get; set; }
        public bool Import { get; set; }
        public string Number { get; set; }
        public float Summ { get; set; }

        public virtual Organization Organization { get; set; }
    }
}
