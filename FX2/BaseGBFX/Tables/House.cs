﻿using System;
using System.Collections.Generic;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class House : fxaKeyTable
    {
        public House()
        {
            Organizations = new HashSet<Organization>();
            Flats = new HashSet<Flat>();
        }
        public string Number { get; set; }
        public string Building { get; set; }
        public Guid? AoGuid { get; set; }
        public string PostalCode { get; set; }

        public string _PHouseID { get; set; }

        public string OKTMO { get; set; }
        public object OKATO { get; set; }
        public string KLADR { get; set; }

        public virtual Address Address { get; set; }
        public virtual ICollection<Organization> Organizations { get; set; }
        public virtual ICollection<Flat> Flats { get; set; }
    }
}