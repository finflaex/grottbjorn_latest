﻿#region

using System.Collections.Generic;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

#endregion

namespace BaseGBFX.Tables
{
    public class Role : fxaKeyTable, fxiCaptionTable
    {
        public Role()
        {
            Logins = new HashSet<Login>();
        }

        public string Key { get; set; }
        public bool Active { get; set; }
        public string Caption { get; set; }

        public virtual ICollection<Login> Logins { get; set; }
        
    }
}