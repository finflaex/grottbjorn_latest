﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class Session : fxaHistoryTable
    {
        public string Address { get; set; }
        public string Host { get; set; }

        public virtual Login Login { get; set; }
    }
}
