﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

namespace BaseGBFX.Tables
{
    public class TaskEvent : 
        fxaHistoryTable,
        fxiCaptionTable,
        fxiAuthorTable,
        fxiCommentTable
    {
        public TaskEvent()
        {
            Accountable = new HashSet<LoginAccessTaskEvent>();
        }

        public string Caption { get; set; }
        public Guid Author { get; set; }
        public string Comment { get; set; }
        public bool IsAllDay { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual ContactDetails Contact { get; set; }
        public virtual Work Work { get; set; }
        public virtual ICollection<LoginAccessTaskEvent> Accountable { get; set; }
        public TaskEventType Type { get; set; }
        public Departament Departament { get; set; }
    }
}
