﻿using System;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class FssRegistration : fxaKeyTable
    {
        public virtual Fss Fss { get; set; }
        public DateTime? DateRegistration { get; set; }
        public string NumberRegistration { get; set; }
        public virtual Organization Organization { get; set; }
    }
}