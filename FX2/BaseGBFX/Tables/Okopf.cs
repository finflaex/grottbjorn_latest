﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

namespace BaseGBFX.Tables
{
    public class Okopf : fxaKeyTable, fxiCaptionTable, fxiDescriptionTable
    {
        public Okopf()
        {
            Organizations = new HashSet<Organization>();
        }

        public string Caption { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Code { get; set; }
        public virtual ICollection<Organization> Organizations { get; set; }
    }
}
