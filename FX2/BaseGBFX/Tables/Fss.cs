﻿using System.Collections.Generic;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class Fss : fxaKeyTable
    {
        public Fss()
        {
            Registrations = new HashSet<FssRegistration>();
        }
        public string _OOID { get; set; }
        public string Code { get; set; }
        public string ShortName { get; set; }
        public string Name { get; set; }
        public virtual ICollection<FssRegistration> Registrations { get; set; }
    }
}