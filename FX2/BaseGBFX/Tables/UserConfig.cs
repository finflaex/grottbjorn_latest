﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class UserConfig : fxaKeyTable
    {
        public string Key { get; set; }
        public byte[] Value { get; set; }
        public virtual Login User { get; set; }
    }
}
