﻿using System.Collections.Generic;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class Pfr : fxaKeyTable
    {
        public Pfr()
        {
            Registrations = new HashSet<PfrRegistration>();
        }
        public string _OOID { get; set; }
        public string Code { get; set; }
        public string ShortName { get; set; }
        public string Name { get; set; }
        public virtual ICollection<PfrRegistration> Registrations { get; set; }
    }
}