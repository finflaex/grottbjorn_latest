﻿using System.Collections.Generic;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

namespace BaseGBFX.Tables
{
    public class Post : fxaKeyTable, fxiCaptionTable
    {
        public Post()
        {
            Works = new HashSet<Work>();
            Comments = new HashSet<Comment>();
        }

        public string _OOID { get; set; }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Caption { get; set; }
        public virtual ICollection<Work> Works { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}