﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

namespace BaseGBFX.Tables
{
    public class LoginAccessOrganization
    {
        public LoginAccessOrganization()
        {
            
        }

        public LoginAccessOrganization(Organization value)
        {
            Organization = value;
        }

        public LoginAccessOrganization(Login value)
        {
            Login = value;
        }

        [Key, Column(Order = 0), ForeignKey(nameof(Login))]
        public Guid LoginId { get; set; }
        [Key, Column(Order = 1), ForeignKey(nameof(Organization))]
        public Guid OrganizationId { get; set; }

        public virtual Login Login { get; set; }
        public virtual Organization Organization { get; set; }

        public TypeLoginAccessOrganization Type { get; set; }
    }

    public enum TypeLoginAccessOrganization
    {
        unknown = 0,
        read = 1,
        write = 2
    }
}
