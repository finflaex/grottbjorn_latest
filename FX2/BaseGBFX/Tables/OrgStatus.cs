﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

namespace BaseGBFX.Tables
{
    public class OrgStatus :
        fxaKeyTable,
        fxiHelpTable<string, string>
    {
        public OrgStatus()
        {
            Organizations = new HashSet<Organization>();
        }

        public string Key { get; set; }
        public string Value { get; set; }

        public virtual ICollection<Organization> Organizations { get; set; }
    }
}
