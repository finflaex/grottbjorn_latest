﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

namespace BaseGBFX.Tables
{
    public class ContactDetails : fxaHistoryTable
    {
        public ContactDetails()
        {
            Comments = new HashSet<Comment>();
            Tasks = new HashSet<TaskEvent>();
        }
        public string Value { get; set; }
        public string RawValue { get; set; }
        public string Type { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<TaskEvent> Tasks { get; set; }

        //[ForeignKey(nameof(Author))]
        //public virtual Login UserAuthor { get; set; }

        //public Guid Author { get; set; }
    }
}
