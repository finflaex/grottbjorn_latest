﻿using System;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class PfrRegistration : fxaKeyTable
    {
        public virtual Pfr Pfr { get; set; }
        public DateTime? DateRegistration { get; set; }
        public string NumberRegistration { get; set; }
        public virtual Organization Organization { get; set; }
    }
}