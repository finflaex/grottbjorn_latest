﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.MVC;

namespace BaseGBFX.Tables
{
    public class Departament : fxaKeyTable, fxiCaptionTable
    {
        public Departament()
        {
            Comments = new HashSet<Comment>();
            Logins = new HashSet<Login>();
            Tasks = new HashSet<TaskEvent>();
        }

        public string Caption { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Login> Logins { get; set; }
        public virtual ICollection<TaskEvent> Tasks { get; set; }
        public int OldID { get; set; }
    }
}
