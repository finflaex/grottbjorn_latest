﻿using System;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class Businessman : fxaHistoryTable
    {
        public string Ogrnip { get; set; }
        public string Inn { get; set; }
        public string Kpp { get; set; }
        public string Okpo { get; set; }
        //pfr
        //fss
        // okato
        //jkogu
        //okfs
        //okopf
        public virtual Person Person { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}