﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class Person : fxaHistoryTable
    {
        public Person()
        {
            PlaceBirthday = new HashSet<Address>();
            FounderOrganizations = new HashSet<Organization>();
            DirectorOrganizations = new HashSet<Organization>();
            Businessmans = new HashSet<Businessman>();
            Works = new HashSet<Work>();
            Contacts = new HashSet<ContactDetails>();
            Comments = new HashSet<Comment>();
        }

        public string _OOID { get; set; }

        public string Inn { get; set; }
        public string FullName { get; set; }

        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string MiddleName { get; set; }

        public TypeMale Male { get; set; }

        public string Nationality { get; set; }
        public DateTime? DateBirthday { get; set; }
        public virtual ICollection<Address> PlaceBirthday { get; set; }
        public virtual ICollection<Address> PlaceResidence { get; set; }
        public virtual ICollection<Organization> FounderOrganizations { get; set; }
        public virtual ICollection<Organization> DirectorOrganizations { get; set; }
        public virtual ICollection<Businessman> Businessmans { get; set; }
        public virtual ICollection<Work> Works { get; set; }
        public virtual ICollection<ContactDetails> Contacts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual Login Login { get; set; }
    }

    public enum TypeMale
    {
        unknown = 0,
        man = 1,
        wooman = 2,
    }
}
