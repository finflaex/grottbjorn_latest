﻿using System;
using System.Collections.Generic;
using Finflaex.Support.DB.Tables.Abstracts;

namespace BaseGBFX.Tables
{
    public class Work : fxaHistoryTable
    {
        public Work()
        {
            Tasks = new HashSet<TaskEvent>();
        }
        public virtual Post Post { get; set; }
        public virtual Person Person { get; set; }
        public virtual Organization Organization { get; set; }

        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public virtual ICollection<TaskEvent> Tasks { get; set; }
    }
}