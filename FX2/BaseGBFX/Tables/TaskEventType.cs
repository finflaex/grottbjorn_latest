﻿#region

using System.Collections.Generic;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

#endregion

namespace BaseGBFX.Tables
{
    public class TaskEventType : fxaKeyTable, fxiHelpTable<string, string>
    {
        public TaskEventType()
        {
            TaskEvents = new HashSet<TaskEvent>();
        }

        public string Key { get; set; }

        public string Value { get; set; }
        public virtual ICollection<TaskEvent> TaskEvents { get; set; }
    }
}