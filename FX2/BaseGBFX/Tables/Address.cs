﻿#region

using System;
using System.Collections.Generic;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;

#endregion

namespace BaseGBFX.Tables
{
    public class Address : fxaKeyTable
    {
        public Address()
        {
            Houses = new HashSet<House>();
            Organizations = new HashSet<Organization>();
            PersonBirthday = new HashSet<Person>();
        }

        public string Name { get; set; }
        public Guid? AoGuid { get; set; }
        public string _PAddrID { get; set; }
        public string PostalCodes { get; set; }
        public FiasType Level { get; set; }
        public string TypeName { get; set; }
        public string TypeShortName { get; set; }

        public string OKTMO { get; set; }
        public string OKATO { get; set; }
        public string KLADR { get; set; }

        public virtual ICollection<House> Houses { get; set; }
        public virtual ICollection<Organization> Organizations { get; set; }
        public virtual ICollection<Person> PersonBirthday { get; set; }
    }
}