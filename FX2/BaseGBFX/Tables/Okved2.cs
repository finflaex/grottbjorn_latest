﻿using System.Collections.Generic;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

namespace BaseGBFX.Tables
{
    public class Okved2 : fxaKeyTable, fxiDescriptionTable, fxiCaptionTable
    {
        public Okved2()
        {
            Organizations = new HashSet<Organization>();
            MainOrganizations = new HashSet<Organization>();
        }

        public string _OOID { get; set; }

        public string Code { get; set; }
        public string Description { get; set; }
        public string Caption { get; set; }

        public virtual ICollection<Organization> Organizations { get; set; }
        public virtual ICollection<Organization> MainOrganizations { get; set; }
        public virtual Okved2 Parent { get; set; }
    }
}