﻿namespace BaseGBFX.Tables
{
    public enum CommentType
    {
        unknown = 0,
        history = 1,
        description = 2,
    }
}