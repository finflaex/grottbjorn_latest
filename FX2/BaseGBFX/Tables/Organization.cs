﻿#region

using System;
using System.Collections.Generic;
using Finflaex.Support.DB.Tables.Abstracts;

#endregion

namespace BaseGBFX.Tables
{
    public class Organization : fxaHistoryTable
    {
        public Organization()
        {
            Addresses = new HashSet<Address>();
            Okved1s = new HashSet<Okved1>();
            Okved2s = new HashSet<Okved2>();
            Works = new HashSet<Work>();
            DirectorPersons = new HashSet<Person>();
            FounderPersons = new HashSet<Person>();
            Comments = new HashSet<Comment>();
            Accaunted = new HashSet<LoginAccessOrganization>();
            CustomDeclarations = new HashSet<CustomDeclaration>();
            Tasks = new HashSet<TaskEvent>();
            Statuses = new HashSet<OrgStatus>();
            Directions = new HashSet<OrgDirection>();
        }

        public virtual ICollection<Work> Works { get; set; }

        public Okopf Okopf { get; set; }
        public virtual ICollection<Okved1> Okved1s { get; set; }
        public virtual Okved1 MainOkved1 { get; set; }
        public virtual ICollection<Okved2> Okved2s { get; set; }
        public virtual Okved2 MainOkved2 { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
        public virtual House House { get; set; }
        public virtual Flat Flat { get; set; }
        public string _OOID { get; set; }

        public string Name { get; set; }
        public string ShortName { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string Ogrn { get; set; }
        public string Inn { get; set; }
        public string Kpp { get; set; }
        public string Okpo { get; set; }
        public string Bik { get; set; }
        public virtual PfrRegistration Pfr { get; set; }
        public virtual FssRegistration Fss { get; set; }
        // todo okato

        public virtual ICollection<Person> DirectorPersons { get; set; }
        public virtual ICollection<Person> FounderPersons { get; set; }
        public virtual ICollection<LoginAccessOrganization> Accaunted { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<CustomDeclaration> CustomDeclarations { get; set; }
        public string RawAddress { get; set; }
        public virtual ICollection<TaskEvent> Tasks { get; set; }
        public virtual ICollection<OrgStatus> Statuses { get; set; }
        public  virtual ICollection<OrgDirection> Directions { get; set; }
    }
}