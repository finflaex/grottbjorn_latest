﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.DB.Tables.Interfaces;

namespace BaseGBFX.Tables
{
    public class LoginAccessTaskEvent
    {
        public LoginAccessTaskEvent()
        {
            
        }
        public LoginAccessTaskEvent(TaskEvent value)
        {
            TaskEvent = value;
        }

        public LoginAccessTaskEvent(Login value)
        {
            Login = value;
        }

        [Key, Column(Order = 0), ForeignKey(nameof(Login))]
        public Guid LoginId { get; set; }
        [Key, Column(Order = 1), ForeignKey(nameof(TaskEvent))]
        public Guid TaskEventId { get; set; }

        public virtual Login Login { get; set; }
        public virtual TaskEvent TaskEvent { get; set; }

        public TypeLoginAccessTaskEvent Type { get; set; }
    }

    public enum TypeLoginAccessTaskEvent
    {
        unknown = 0,
        read = 1,
        write = 2
    }
}
