// <auto-generated />
namespace BaseGBFX.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class contact_author : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(contact_author));
        
        string IMigrationMetadata.Id
        {
            get { return "201702041352037_contact_author"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
