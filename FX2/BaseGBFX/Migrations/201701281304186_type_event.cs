namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class type_event : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TaskEventTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.TaskEvents", "Type_ID", c => c.Guid());
            CreateIndex("dbo.TaskEvents", "Type_ID");
            AddForeignKey("dbo.TaskEvents", "Type_ID", "dbo.TaskEventTypes", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaskEvents", "Type_ID", "dbo.TaskEventTypes");
            DropIndex("dbo.TaskEvents", new[] { "Type_ID" });
            DropColumn("dbo.TaskEvents", "Type_ID");
            DropTable("dbo.TaskEventTypes");
        }
    }
}
