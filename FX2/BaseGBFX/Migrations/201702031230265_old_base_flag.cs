namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class old_base_flag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logins", "Block", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Logins", "Block");
        }
    }
}
