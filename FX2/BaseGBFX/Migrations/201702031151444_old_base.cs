namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class old_base : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logins", "OldId", c => c.Int(nullable: false));
            AddColumn("dbo.Departaments", "OldID", c => c.Int(nullable: false));
            AddColumn("dbo.People", "Login_ID", c => c.Guid());
            CreateIndex("dbo.People", "Login_ID");
            AddForeignKey("dbo.People", "Login_ID", "dbo.Logins", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.People", "Login_ID", "dbo.Logins");
            DropIndex("dbo.People", new[] { "Login_ID" });
            DropColumn("dbo.People", "Login_ID");
            DropColumn("dbo.Departaments", "OldID");
            DropColumn("dbo.Logins", "OldId");
        }
    }
}
