namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class departament_task : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskEvents", "Departament_ID", c => c.Guid());
            CreateIndex("dbo.TaskEvents", "Departament_ID");
            AddForeignKey("dbo.TaskEvents", "Departament_ID", "dbo.Departaments", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaskEvents", "Departament_ID", "dbo.Departaments");
            DropIndex("dbo.TaskEvents", new[] { "Departament_ID" });
            DropColumn("dbo.TaskEvents", "Departament_ID");
        }
    }
}
