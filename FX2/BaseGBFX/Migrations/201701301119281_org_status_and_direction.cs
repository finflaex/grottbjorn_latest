namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class org_status_and_direction : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrgDirections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OrgStatus",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OrganizationOrgDirections",
                c => new
                    {
                        Organization_ID = c.Guid(nullable: false),
                        OrgDirection_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organization_ID, t.OrgDirection_ID })
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .ForeignKey("dbo.OrgDirections", t => t.OrgDirection_ID, cascadeDelete: true)
                .Index(t => t.Organization_ID)
                .Index(t => t.OrgDirection_ID);
            
            CreateTable(
                "dbo.OrganizationOrgStatus",
                c => new
                    {
                        Organization_ID = c.Guid(nullable: false),
                        OrgStatus_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organization_ID, t.OrgStatus_ID })
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .ForeignKey("dbo.OrgStatus", t => t.OrgStatus_ID, cascadeDelete: true)
                .Index(t => t.Organization_ID)
                .Index(t => t.OrgStatus_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrganizationOrgStatus", "OrgStatus_ID", "dbo.OrgStatus");
            DropForeignKey("dbo.OrganizationOrgStatus", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.OrganizationOrgDirections", "OrgDirection_ID", "dbo.OrgDirections");
            DropForeignKey("dbo.OrganizationOrgDirections", "Organization_ID", "dbo.Organizations");
            DropIndex("dbo.OrganizationOrgStatus", new[] { "OrgStatus_ID" });
            DropIndex("dbo.OrganizationOrgStatus", new[] { "Organization_ID" });
            DropIndex("dbo.OrganizationOrgDirections", new[] { "OrgDirection_ID" });
            DropIndex("dbo.OrganizationOrgDirections", new[] { "Organization_ID" });
            DropTable("dbo.OrganizationOrgStatus");
            DropTable("dbo.OrganizationOrgDirections");
            DropTable("dbo.OrgStatus");
            DropTable("dbo.OrgDirections");
        }
    }
}
