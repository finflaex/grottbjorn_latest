namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class message : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Show = c.Boolean(nullable: false),
                        Actual = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                        AuthorService = c.String(),
                        Title = c.String(),
                        Description = c.String(),
                        Content = c.String(),
                        TargetUser_ID = c.Guid(),
                        AuthorUser_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Logins", t => t.TargetUser_ID)
                .ForeignKey("dbo.Logins", t => t.AuthorUser_ID)
                .Index(t => t.TargetUser_ID)
                .Index(t => t.AuthorUser_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "AuthorUser_ID", "dbo.Logins");
            DropForeignKey("dbo.Messages", "TargetUser_ID", "dbo.Logins");
            DropIndex("dbo.Messages", new[] { "AuthorUser_ID" });
            DropIndex("dbo.Messages", new[] { "TargetUser_ID" });
            DropTable("dbo.Messages");
        }
    }
}
