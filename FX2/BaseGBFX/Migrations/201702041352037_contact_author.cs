namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class contact_author : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Comments", "Author");
            AddForeignKey("dbo.Comments", "Author", "dbo.Logins", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "Author", "dbo.Logins");
            DropIndex("dbo.Comments", new[] { "Author" });
        }
    }
}
