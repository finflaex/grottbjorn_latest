namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class departament : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LoginDepartaments", "Login_ID", "dbo.Logins");
            DropForeignKey("dbo.LoginDepartaments", "Departament_ID", "dbo.Departaments");
            DropIndex("dbo.LoginDepartaments", new[] { "Login_ID" });
            DropIndex("dbo.LoginDepartaments", new[] { "Departament_ID" });
            AddColumn("dbo.Logins", "Departament_ID", c => c.Guid());
            CreateIndex("dbo.Logins", "Departament_ID");
            AddForeignKey("dbo.Logins", "Departament_ID", "dbo.Departaments", "ID");
            DropTable("dbo.LoginDepartaments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.LoginDepartaments",
                c => new
                    {
                        Login_ID = c.Guid(nullable: false),
                        Departament_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Login_ID, t.Departament_ID });
            
            DropForeignKey("dbo.Logins", "Departament_ID", "dbo.Departaments");
            DropIndex("dbo.Logins", new[] { "Departament_ID" });
            DropColumn("dbo.Logins", "Departament_ID");
            CreateIndex("dbo.LoginDepartaments", "Departament_ID");
            CreateIndex("dbo.LoginDepartaments", "Login_ID");
            AddForeignKey("dbo.LoginDepartaments", "Departament_ID", "dbo.Departaments", "ID", cascadeDelete: true);
            AddForeignKey("dbo.LoginDepartaments", "Login_ID", "dbo.Logins", "ID", cascadeDelete: true);
        }
    }
}
