#region

using System;
using System.Data.Entity.Migrations;
using System.Linq;
using BaseGBFX.Bases;
using BaseGBFX.Tables;
using Finflaex.Support.DB.Tables.Federals;
using Finflaex.Support.Reflection;

#endregion

namespace BaseGBFX.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<tfxdb_Grottbjorn>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(tfxdb_Grottbjorn context)
        {
            var role_manager = new Role()
            {
                ID = 1.ToGuid(),
                Active = true,
                Caption = "��������",
                Key = DB.Roles.Manager
            }
                .Action(v => context.Roles.AddOrUpdate(v));

            var role_operator = new Role()
            {
                ID = 2.ToGuid(),
                Active = true,
                Caption = "��������",
                Key = DB.Roles.Operator
            }
                .Action(v => context.Roles.AddOrUpdate(v));

            var role_consultant = new Role()
                {
                    ID = 3.ToGuid(),
                    Active = true,
                    Caption = "�����������",
                    Key = DB.Roles.Consultant
                }
                .Action(v => context.Roles.AddOrUpdate(v));

            var role_admin = new Role()
            {
                ID = 4.ToGuid(),
                Active = true,
                Caption = "�������������",
                Key = DB.Roles.Admin
            }
                .Action(v => context.Roles.AddOrUpdate(v));


            context.Logins.AddOrUpdate(
                new Login()
                {
                    ID = 1.ToGuid(),
                    Caption = "�������������",
                    Name = "architector",
                    Password = "carmageddon"
                });

            context.Logins.AddOrUpdate(
                new Login()
                {
                    ID = Guid.Empty,
                    Caption = "�����������",
                    Name = "guest",
                    Password = "guest"
                });

            var okopf_codes = context.Okopf.Select(v => v.Code);

            OKOPF
                .List
                .Where(v=>!okopf_codes.Contains(v.Code))
                .Select(read => new Okopf()
                {
                    Caption = read.Caption,
                    Code = read.Code,
                    Description = read.Description,
                    StartDate = read.StartDate,
                    EndDate = read.EndDate,
                })
                .ToArray()
                .Action(context.Okopf.AddOrUpdate);

            context.EventTypes.AddOrUpdate(
                new TaskEventType
                {
                    ID = Guid.Empty,
                    Key = "",
                    Value = "������"
                },
                new TaskEventType
                {
                    ID = 1.ToGuid(),
                    Key = "meeting",
                    Value = "�������"
                },
                new TaskEventType
                {
                    ID = 2.ToGuid(),
                    Key = "call",
                    Value = "������"
                });
        }
    }
}