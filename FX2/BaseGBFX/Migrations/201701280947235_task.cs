namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class task : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TaskEvents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Caption = c.String(),
                        Author = c.Guid(nullable: false),
                        Comment = c.String(),
                        IsAllDay = c.Boolean(nullable: false),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                        Work_ID = c.Guid(),
                        Contact_ID = c.Guid(),
                        Organization_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Works", t => t.Work_ID)
                .ForeignKey("dbo.ContactDetails", t => t.Contact_ID)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID)
                .Index(t => t.Work_ID)
                .Index(t => t.Contact_ID)
                .Index(t => t.Organization_ID);
            
            CreateTable(
                "dbo.LoginAccessTaskEvents",
                c => new
                    {
                        LoginId = c.Guid(nullable: false),
                        TaskEventId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginId, t.TaskEventId })
                .ForeignKey("dbo.TaskEvents", t => t.TaskEventId, cascadeDelete: true)
                .ForeignKey("dbo.Logins", t => t.LoginId, cascadeDelete: true)
                .Index(t => t.LoginId)
                .Index(t => t.TaskEventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaskEvents", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.LoginAccessTaskEvents", "LoginId", "dbo.Logins");
            DropForeignKey("dbo.TaskEvents", "Contact_ID", "dbo.ContactDetails");
            DropForeignKey("dbo.TaskEvents", "Work_ID", "dbo.Works");
            DropForeignKey("dbo.LoginAccessTaskEvents", "TaskEventId", "dbo.TaskEvents");
            DropIndex("dbo.LoginAccessTaskEvents", new[] { "TaskEventId" });
            DropIndex("dbo.LoginAccessTaskEvents", new[] { "LoginId" });
            DropIndex("dbo.TaskEvents", new[] { "Organization_ID" });
            DropIndex("dbo.TaskEvents", new[] { "Contact_ID" });
            DropIndex("dbo.TaskEvents", new[] { "Work_ID" });
            DropTable("dbo.LoginAccessTaskEvents");
            DropTable("dbo.TaskEvents");
        }
    }
}
