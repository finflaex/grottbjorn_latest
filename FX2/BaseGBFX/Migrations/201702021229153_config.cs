namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class config : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserConfigs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Value = c.Binary(),
                        User_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Logins", t => t.User_ID, cascadeDelete: true)
                .Index(t => t.User_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserConfigs", "User_ID", "dbo.Logins");
            DropIndex("dbo.UserConfigs", new[] { "User_ID" });
            DropTable("dbo.UserConfigs");
        }
    }
}
