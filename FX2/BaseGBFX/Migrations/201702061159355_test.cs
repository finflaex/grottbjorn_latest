namespace BaseGBFX.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        AoGuid = c.Guid(),
                        _PAddrID = c.String(),
                        PostalCodes = c.String(),
                        Level = c.Int(nullable: false),
                        TypeName = c.String(),
                        TypeShortName = c.String(),
                        OKTMO = c.String(),
                        OKATO = c.String(),
                        KLADR = c.String(),
                        Person_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.People", t => t.Person_ID)
                .Index(t => t.Person_ID);
            
            CreateTable(
                "dbo.Houses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Number = c.String(),
                        Building = c.String(),
                        AoGuid = c.Guid(),
                        PostalCode = c.String(),
                        _PHouseID = c.String(),
                        OKTMO = c.String(),
                        KLADR = c.String(),
                        Address_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Addresses", t => t.Address_ID)
                .Index(t => t.Address_ID);
            
            CreateTable(
                "dbo.Flats",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Number = c.String(),
                        Organization_ID = c.Guid(),
                        House_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID)
                .ForeignKey("dbo.Houses", t => t.House_ID)
                .Index(t => t.Organization_ID)
                .Index(t => t.House_ID);
            
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        _OOID = c.String(),
                        Name = c.String(),
                        ShortName = c.String(),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        Ogrn = c.String(),
                        Inn = c.String(),
                        Kpp = c.String(),
                        Okpo = c.String(),
                        Bik = c.String(),
                        RawAddress = c.String(),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                        Fss_ID = c.Guid(),
                        House_ID = c.Guid(),
                        MainOkved1_ID = c.Guid(),
                        MainOkved2_ID = c.Guid(),
                        Okopf_ID = c.Guid(),
                        Pfr_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FssRegistrations", t => t.Fss_ID)
                .ForeignKey("dbo.Houses", t => t.House_ID)
                .ForeignKey("dbo.Okved1", t => t.MainOkved1_ID)
                .ForeignKey("dbo.Okved2", t => t.MainOkved2_ID)
                .ForeignKey("dbo.Okopfs", t => t.Okopf_ID)
                .ForeignKey("dbo.PfrRegistrations", t => t.Pfr_ID)
                .Index(t => t.Fss_ID)
                .Index(t => t.House_ID)
                .Index(t => t.MainOkved1_ID)
                .Index(t => t.MainOkved2_ID)
                .Index(t => t.Okopf_ID)
                .Index(t => t.Pfr_ID);
            
            CreateTable(
                "dbo.LoginAccessOrganizations",
                c => new
                    {
                        LoginId = c.Guid(nullable: false),
                        OrganizationId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginId, t.OrganizationId })
                .ForeignKey("dbo.Logins", t => t.LoginId, cascadeDelete: true)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId, cascadeDelete: true)
                .Index(t => t.LoginId)
                .Index(t => t.OrganizationId);
            
            CreateTable(
                "dbo.Logins",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        Password = c.Binary(),
                        Caption = c.String(),
                        OldId = c.Int(nullable: false),
                        Block = c.Boolean(nullable: false),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                        Departament_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Departaments", t => t.Departament_ID)
                .Index(t => t.Departament_ID);
            
            CreateTable(
                "dbo.UserConfigs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Value = c.Binary(),
                        User_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Logins", t => t.User_ID, cascadeDelete: true)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.Departaments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Caption = c.String(),
                        OldID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Description = c.String(),
                        Author = c.Guid(nullable: false),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                        Contact_ID = c.Guid(),
                        Person_ID = c.Guid(),
                        Post_ID = c.Guid(),
                        Departament_ID = c.Guid(),
                        Organization_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContactDetails", t => t.Contact_ID)
                .ForeignKey("dbo.People", t => t.Person_ID)
                .ForeignKey("dbo.Posts", t => t.Post_ID)
                .ForeignKey("dbo.Logins", t => t.Author, cascadeDelete: true)
                .ForeignKey("dbo.Departaments", t => t.Departament_ID)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID)
                .Index(t => t.Author)
                .Index(t => t.Contact_ID)
                .Index(t => t.Person_ID)
                .Index(t => t.Post_ID)
                .Index(t => t.Departament_ID)
                .Index(t => t.Organization_ID);
            
            CreateTable(
                "dbo.ContactDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Value = c.String(),
                        RawValue = c.String(),
                        Type = c.String(),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                        Person_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.People", t => t.Person_ID)
                .Index(t => t.Person_ID);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        _OOID = c.String(),
                        Inn = c.String(),
                        FullName = c.String(),
                        FirstName = c.String(),
                        SurName = c.String(),
                        MiddleName = c.String(),
                        Male = c.Int(nullable: false),
                        Nationality = c.String(),
                        DateBirthday = c.DateTime(),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                        Login_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Logins", t => t.Login_ID)
                .Index(t => t.Login_ID);
            
            CreateTable(
                "dbo.Businessmen",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Ogrnip = c.String(),
                        Inn = c.String(),
                        Kpp = c.String(),
                        Okpo = c.String(),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                        Person_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.People", t => t.Person_ID)
                .Index(t => t.Person_ID);
            
            CreateTable(
                "dbo.Works",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Start = c.DateTime(),
                        End = c.DateTime(),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                        Post_ID = c.Guid(),
                        Person_ID = c.Guid(),
                        Organization_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.Post_ID)
                .ForeignKey("dbo.People", t => t.Person_ID)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID)
                .Index(t => t.Post_ID)
                .Index(t => t.Person_ID)
                .Index(t => t.Organization_ID);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        _OOID = c.String(),
                        Code = c.String(),
                        Name = c.String(),
                        Caption = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TaskEvents",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Caption = c.String(),
                        Author = c.Guid(nullable: false),
                        Comment = c.String(),
                        IsAllDay = c.Boolean(nullable: false),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                        Departament_ID = c.Guid(),
                        Type_ID = c.Guid(),
                        Work_ID = c.Guid(),
                        Contact_ID = c.Guid(),
                        Organization_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Departaments", t => t.Departament_ID)
                .ForeignKey("dbo.TaskEventTypes", t => t.Type_ID)
                .ForeignKey("dbo.Works", t => t.Work_ID)
                .ForeignKey("dbo.ContactDetails", t => t.Contact_ID)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID)
                .Index(t => t.Departament_ID)
                .Index(t => t.Type_ID)
                .Index(t => t.Work_ID)
                .Index(t => t.Contact_ID)
                .Index(t => t.Organization_ID);
            
            CreateTable(
                "dbo.LoginAccessTaskEvents",
                c => new
                    {
                        LoginId = c.Guid(nullable: false),
                        TaskEventId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginId, t.TaskEventId })
                .ForeignKey("dbo.TaskEvents", t => t.TaskEventId, cascadeDelete: true)
                .ForeignKey("dbo.Logins", t => t.LoginId, cascadeDelete: true)
                .Index(t => t.LoginId)
                .Index(t => t.TaskEventId);
            
            CreateTable(
                "dbo.TaskEventTypes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Show = c.Boolean(nullable: false),
                        Actual = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                        AuthorService = c.String(),
                        Title = c.String(),
                        Description = c.String(),
                        Content = c.String(),
                        TargetUser_ID = c.Guid(),
                        AuthorUser_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Logins", t => t.TargetUser_ID)
                .ForeignKey("dbo.Logins", t => t.AuthorUser_ID)
                .Index(t => t.TargetUser_ID)
                .Index(t => t.AuthorUser_ID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Active = c.Boolean(nullable: false),
                        Caption = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Sessions",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Address = c.String(),
                        Host = c.String(),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                        Login_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Logins", t => t.Login_ID)
                .Index(t => t.Login_ID);
            
            CreateTable(
                "dbo.CustomDeclarations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Currency = c.String(),
                        Curs = c.Single(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Import = c.Boolean(nullable: false),
                        Number = c.String(),
                        Summ = c.Single(nullable: false),
                        Organization_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID)
                .Index(t => t.Organization_ID);
            
            CreateTable(
                "dbo.OrgDirections",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FssRegistrations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DateRegistration = c.DateTime(),
                        NumberRegistration = c.String(),
                        Fss_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Fsses", t => t.Fss_ID)
                .Index(t => t.Fss_ID);
            
            CreateTable(
                "dbo.Fsses",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        _OOID = c.String(),
                        Code = c.String(),
                        ShortName = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Okved1",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        _OOID = c.String(),
                        Code = c.String(),
                        Description = c.String(),
                        Caption = c.String(),
                        Parent_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Okved1", t => t.Parent_ID)
                .Index(t => t.Parent_ID);
            
            CreateTable(
                "dbo.Okved2",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        _OOID = c.String(),
                        Code = c.String(),
                        Description = c.String(),
                        Caption = c.String(),
                        Parent_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Okved2", t => t.Parent_ID)
                .Index(t => t.Parent_ID);
            
            CreateTable(
                "dbo.Okopfs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Caption = c.String(),
                        Description = c.String(),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PfrRegistrations",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        DateRegistration = c.DateTime(),
                        NumberRegistration = c.String(),
                        Pfr_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pfrs", t => t.Pfr_ID)
                .Index(t => t.Pfr_ID);
            
            CreateTable(
                "dbo.Pfrs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        _OOID = c.String(),
                        Code = c.String(),
                        ShortName = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OrgStatus",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SysLink",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Root = c.Guid(nullable: false),
                        Second = c.Guid(nullable: false),
                        Type = c.Guid(nullable: false),
                        TypeRoot = c.String(),
                        TypeSecond = c.String(),
                        Comment = c.String(),
                        verStartDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verEndDate = c.DateTimeOffset(nullable: false, precision: 7),
                        verTarget = c.Guid(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SysLog",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        When = c.DateTimeOffset(nullable: false, precision: 7),
                        Type = c.String(),
                        Target = c.Guid(nullable: false),
                        Property = c.String(),
                        OriginalValue = c.Binary(),
                        ModifyValue = c.Binary(),
                        State = c.Int(nullable: false),
                        Author = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PersonOrganizations",
                c => new
                    {
                        Person_ID = c.Guid(nullable: false),
                        Organization_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Person_ID, t.Organization_ID })
                .ForeignKey("dbo.People", t => t.Person_ID, cascadeDelete: true)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .Index(t => t.Person_ID)
                .Index(t => t.Organization_ID);
            
            CreateTable(
                "dbo.PersonOrganization1",
                c => new
                    {
                        Person_ID = c.Guid(nullable: false),
                        Organization_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Person_ID, t.Organization_ID })
                .ForeignKey("dbo.People", t => t.Person_ID, cascadeDelete: true)
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .Index(t => t.Person_ID)
                .Index(t => t.Organization_ID);
            
            CreateTable(
                "dbo.PersonAddresses",
                c => new
                    {
                        Person_ID = c.Guid(nullable: false),
                        Address_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Person_ID, t.Address_ID })
                .ForeignKey("dbo.People", t => t.Person_ID, cascadeDelete: true)
                .ForeignKey("dbo.Addresses", t => t.Address_ID, cascadeDelete: true)
                .Index(t => t.Person_ID)
                .Index(t => t.Address_ID);
            
            CreateTable(
                "dbo.LoginRoles",
                c => new
                    {
                        Login_ID = c.Guid(nullable: false),
                        Role_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Login_ID, t.Role_ID })
                .ForeignKey("dbo.Logins", t => t.Login_ID, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.Role_ID, cascadeDelete: true)
                .Index(t => t.Login_ID)
                .Index(t => t.Role_ID);
            
            CreateTable(
                "dbo.OrganizationAddresses",
                c => new
                    {
                        Organization_ID = c.Guid(nullable: false),
                        Address_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organization_ID, t.Address_ID })
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .ForeignKey("dbo.Addresses", t => t.Address_ID, cascadeDelete: true)
                .Index(t => t.Organization_ID)
                .Index(t => t.Address_ID);
            
            CreateTable(
                "dbo.OrganizationOrgDirections",
                c => new
                    {
                        Organization_ID = c.Guid(nullable: false),
                        OrgDirection_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organization_ID, t.OrgDirection_ID })
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .ForeignKey("dbo.OrgDirections", t => t.OrgDirection_ID, cascadeDelete: true)
                .Index(t => t.Organization_ID)
                .Index(t => t.OrgDirection_ID);
            
            CreateTable(
                "dbo.OrganizationOkved1",
                c => new
                    {
                        Organization_ID = c.Guid(nullable: false),
                        Okved1_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organization_ID, t.Okved1_ID })
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .ForeignKey("dbo.Okved1", t => t.Okved1_ID, cascadeDelete: true)
                .Index(t => t.Organization_ID)
                .Index(t => t.Okved1_ID);
            
            CreateTable(
                "dbo.OrganizationOkved2",
                c => new
                    {
                        Organization_ID = c.Guid(nullable: false),
                        Okved2_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organization_ID, t.Okved2_ID })
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .ForeignKey("dbo.Okved2", t => t.Okved2_ID, cascadeDelete: true)
                .Index(t => t.Organization_ID)
                .Index(t => t.Okved2_ID);
            
            CreateTable(
                "dbo.OrganizationOrgStatus",
                c => new
                    {
                        Organization_ID = c.Guid(nullable: false),
                        OrgStatus_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organization_ID, t.OrgStatus_ID })
                .ForeignKey("dbo.Organizations", t => t.Organization_ID, cascadeDelete: true)
                .ForeignKey("dbo.OrgStatus", t => t.OrgStatus_ID, cascadeDelete: true)
                .Index(t => t.Organization_ID)
                .Index(t => t.OrgStatus_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Houses", "Address_ID", "dbo.Addresses");
            DropForeignKey("dbo.Flats", "House_ID", "dbo.Houses");
            DropForeignKey("dbo.Works", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.TaskEvents", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.OrganizationOrgStatus", "OrgStatus_ID", "dbo.OrgStatus");
            DropForeignKey("dbo.OrganizationOrgStatus", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.Organizations", "Pfr_ID", "dbo.PfrRegistrations");
            DropForeignKey("dbo.PfrRegistrations", "Pfr_ID", "dbo.Pfrs");
            DropForeignKey("dbo.OrganizationOkved2", "Okved2_ID", "dbo.Okved2");
            DropForeignKey("dbo.OrganizationOkved2", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.OrganizationOkved1", "Okved1_ID", "dbo.Okved1");
            DropForeignKey("dbo.OrganizationOkved1", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.Organizations", "Okopf_ID", "dbo.Okopfs");
            DropForeignKey("dbo.Organizations", "MainOkved2_ID", "dbo.Okved2");
            DropForeignKey("dbo.Okved2", "Parent_ID", "dbo.Okved2");
            DropForeignKey("dbo.Organizations", "MainOkved1_ID", "dbo.Okved1");
            DropForeignKey("dbo.Okved1", "Parent_ID", "dbo.Okved1");
            DropForeignKey("dbo.Organizations", "House_ID", "dbo.Houses");
            DropForeignKey("dbo.Organizations", "Fss_ID", "dbo.FssRegistrations");
            DropForeignKey("dbo.FssRegistrations", "Fss_ID", "dbo.Fsses");
            DropForeignKey("dbo.Flats", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.OrganizationOrgDirections", "OrgDirection_ID", "dbo.OrgDirections");
            DropForeignKey("dbo.OrganizationOrgDirections", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.CustomDeclarations", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.Comments", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.OrganizationAddresses", "Address_ID", "dbo.Addresses");
            DropForeignKey("dbo.OrganizationAddresses", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.LoginAccessOrganizations", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.LoginAccessTaskEvents", "LoginId", "dbo.Logins");
            DropForeignKey("dbo.Sessions", "Login_ID", "dbo.Logins");
            DropForeignKey("dbo.Messages", "AuthorUser_ID", "dbo.Logins");
            DropForeignKey("dbo.LoginRoles", "Role_ID", "dbo.Roles");
            DropForeignKey("dbo.LoginRoles", "Login_ID", "dbo.Logins");
            DropForeignKey("dbo.Messages", "TargetUser_ID", "dbo.Logins");
            DropForeignKey("dbo.People", "Login_ID", "dbo.Logins");
            DropForeignKey("dbo.LoginAccessOrganizations", "LoginId", "dbo.Logins");
            DropForeignKey("dbo.Logins", "Departament_ID", "dbo.Departaments");
            DropForeignKey("dbo.Comments", "Departament_ID", "dbo.Departaments");
            DropForeignKey("dbo.Comments", "Author", "dbo.Logins");
            DropForeignKey("dbo.TaskEvents", "Contact_ID", "dbo.ContactDetails");
            DropForeignKey("dbo.Works", "Person_ID", "dbo.People");
            DropForeignKey("dbo.TaskEvents", "Work_ID", "dbo.Works");
            DropForeignKey("dbo.TaskEvents", "Type_ID", "dbo.TaskEventTypes");
            DropForeignKey("dbo.TaskEvents", "Departament_ID", "dbo.Departaments");
            DropForeignKey("dbo.LoginAccessTaskEvents", "TaskEventId", "dbo.TaskEvents");
            DropForeignKey("dbo.Works", "Post_ID", "dbo.Posts");
            DropForeignKey("dbo.Comments", "Post_ID", "dbo.Posts");
            DropForeignKey("dbo.Addresses", "Person_ID", "dbo.People");
            DropForeignKey("dbo.PersonAddresses", "Address_ID", "dbo.Addresses");
            DropForeignKey("dbo.PersonAddresses", "Person_ID", "dbo.People");
            DropForeignKey("dbo.PersonOrganization1", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.PersonOrganization1", "Person_ID", "dbo.People");
            DropForeignKey("dbo.PersonOrganizations", "Organization_ID", "dbo.Organizations");
            DropForeignKey("dbo.PersonOrganizations", "Person_ID", "dbo.People");
            DropForeignKey("dbo.ContactDetails", "Person_ID", "dbo.People");
            DropForeignKey("dbo.Comments", "Person_ID", "dbo.People");
            DropForeignKey("dbo.Businessmen", "Person_ID", "dbo.People");
            DropForeignKey("dbo.Comments", "Contact_ID", "dbo.ContactDetails");
            DropForeignKey("dbo.UserConfigs", "User_ID", "dbo.Logins");
            DropIndex("dbo.OrganizationOrgStatus", new[] { "OrgStatus_ID" });
            DropIndex("dbo.OrganizationOrgStatus", new[] { "Organization_ID" });
            DropIndex("dbo.OrganizationOkved2", new[] { "Okved2_ID" });
            DropIndex("dbo.OrganizationOkved2", new[] { "Organization_ID" });
            DropIndex("dbo.OrganizationOkved1", new[] { "Okved1_ID" });
            DropIndex("dbo.OrganizationOkved1", new[] { "Organization_ID" });
            DropIndex("dbo.OrganizationOrgDirections", new[] { "OrgDirection_ID" });
            DropIndex("dbo.OrganizationOrgDirections", new[] { "Organization_ID" });
            DropIndex("dbo.OrganizationAddresses", new[] { "Address_ID" });
            DropIndex("dbo.OrganizationAddresses", new[] { "Organization_ID" });
            DropIndex("dbo.LoginRoles", new[] { "Role_ID" });
            DropIndex("dbo.LoginRoles", new[] { "Login_ID" });
            DropIndex("dbo.PersonAddresses", new[] { "Address_ID" });
            DropIndex("dbo.PersonAddresses", new[] { "Person_ID" });
            DropIndex("dbo.PersonOrganization1", new[] { "Organization_ID" });
            DropIndex("dbo.PersonOrganization1", new[] { "Person_ID" });
            DropIndex("dbo.PersonOrganizations", new[] { "Organization_ID" });
            DropIndex("dbo.PersonOrganizations", new[] { "Person_ID" });
            DropIndex("dbo.PfrRegistrations", new[] { "Pfr_ID" });
            DropIndex("dbo.Okved2", new[] { "Parent_ID" });
            DropIndex("dbo.Okved1", new[] { "Parent_ID" });
            DropIndex("dbo.FssRegistrations", new[] { "Fss_ID" });
            DropIndex("dbo.CustomDeclarations", new[] { "Organization_ID" });
            DropIndex("dbo.Sessions", new[] { "Login_ID" });
            DropIndex("dbo.Messages", new[] { "AuthorUser_ID" });
            DropIndex("dbo.Messages", new[] { "TargetUser_ID" });
            DropIndex("dbo.LoginAccessTaskEvents", new[] { "TaskEventId" });
            DropIndex("dbo.LoginAccessTaskEvents", new[] { "LoginId" });
            DropIndex("dbo.TaskEvents", new[] { "Organization_ID" });
            DropIndex("dbo.TaskEvents", new[] { "Contact_ID" });
            DropIndex("dbo.TaskEvents", new[] { "Work_ID" });
            DropIndex("dbo.TaskEvents", new[] { "Type_ID" });
            DropIndex("dbo.TaskEvents", new[] { "Departament_ID" });
            DropIndex("dbo.Works", new[] { "Organization_ID" });
            DropIndex("dbo.Works", new[] { "Person_ID" });
            DropIndex("dbo.Works", new[] { "Post_ID" });
            DropIndex("dbo.Businessmen", new[] { "Person_ID" });
            DropIndex("dbo.People", new[] { "Login_ID" });
            DropIndex("dbo.ContactDetails", new[] { "Person_ID" });
            DropIndex("dbo.Comments", new[] { "Organization_ID" });
            DropIndex("dbo.Comments", new[] { "Departament_ID" });
            DropIndex("dbo.Comments", new[] { "Post_ID" });
            DropIndex("dbo.Comments", new[] { "Person_ID" });
            DropIndex("dbo.Comments", new[] { "Contact_ID" });
            DropIndex("dbo.Comments", new[] { "Author" });
            DropIndex("dbo.UserConfigs", new[] { "User_ID" });
            DropIndex("dbo.Logins", new[] { "Departament_ID" });
            DropIndex("dbo.LoginAccessOrganizations", new[] { "OrganizationId" });
            DropIndex("dbo.LoginAccessOrganizations", new[] { "LoginId" });
            DropIndex("dbo.Organizations", new[] { "Pfr_ID" });
            DropIndex("dbo.Organizations", new[] { "Okopf_ID" });
            DropIndex("dbo.Organizations", new[] { "MainOkved2_ID" });
            DropIndex("dbo.Organizations", new[] { "MainOkved1_ID" });
            DropIndex("dbo.Organizations", new[] { "House_ID" });
            DropIndex("dbo.Organizations", new[] { "Fss_ID" });
            DropIndex("dbo.Flats", new[] { "House_ID" });
            DropIndex("dbo.Flats", new[] { "Organization_ID" });
            DropIndex("dbo.Houses", new[] { "Address_ID" });
            DropIndex("dbo.Addresses", new[] { "Person_ID" });
            DropTable("dbo.OrganizationOrgStatus");
            DropTable("dbo.OrganizationOkved2");
            DropTable("dbo.OrganizationOkved1");
            DropTable("dbo.OrganizationOrgDirections");
            DropTable("dbo.OrganizationAddresses");
            DropTable("dbo.LoginRoles");
            DropTable("dbo.PersonAddresses");
            DropTable("dbo.PersonOrganization1");
            DropTable("dbo.PersonOrganizations");
            DropTable("dbo.SysLog");
            DropTable("dbo.SysLink");
            DropTable("dbo.OrgStatus");
            DropTable("dbo.Pfrs");
            DropTable("dbo.PfrRegistrations");
            DropTable("dbo.Okopfs");
            DropTable("dbo.Okved2");
            DropTable("dbo.Okved1");
            DropTable("dbo.Fsses");
            DropTable("dbo.FssRegistrations");
            DropTable("dbo.OrgDirections");
            DropTable("dbo.CustomDeclarations");
            DropTable("dbo.Sessions");
            DropTable("dbo.Roles");
            DropTable("dbo.Messages");
            DropTable("dbo.TaskEventTypes");
            DropTable("dbo.LoginAccessTaskEvents");
            DropTable("dbo.TaskEvents");
            DropTable("dbo.Posts");
            DropTable("dbo.Works");
            DropTable("dbo.Businessmen");
            DropTable("dbo.People");
            DropTable("dbo.ContactDetails");
            DropTable("dbo.Comments");
            DropTable("dbo.Departaments");
            DropTable("dbo.UserConfigs");
            DropTable("dbo.Logins");
            DropTable("dbo.LoginAccessOrganizations");
            DropTable("dbo.Organizations");
            DropTable("dbo.Flats");
            DropTable("dbo.Houses");
            DropTable("dbo.Addresses");
        }
    }
}
