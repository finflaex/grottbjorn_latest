﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.Reflection;

namespace BaseGBFX.Bases
{
    public partial class DB
    {
        #region CONNECT

        public static void SessionConnect(Guid? sessionGuid, Action<tfxdb_Grottbjorn> action)
        {
            using (var db = new tfxdb_Grottbjorn())
            {
                SetAuthor(sessionGuid, db);
                db.Action(action);
            }
        }

        private static void SetAuthor(Guid? sessionGuid, tfxdb_Grottbjorn db)
        {
            var session = db.Sessions.Include(v => v.Login).FirstOrDefault(v => v.ID == sessionGuid);
            db.Author = session?.Login?.ID ?? Guid.Empty;

            //db.Author = sessionGuid == null
            //    ? Guid.Empty
            //    : (db
            //        .Sessions
            //        .Actual(db.Now)
            //        .Include(v => v.Login)
            //        .FirstOrDefault(v => v.ID == sessionGuid)?
            //        .Login?
            //        .ID ?? Guid.Empty);
        }

        public static R SessionConnect<R>(Guid? sessionGuid, Func<tfxdb_Grottbjorn, R> action)
        {
            using (var db = new tfxdb_Grottbjorn())
            {
                SetAuthor(sessionGuid, db);
                return db.FuncSelect(action);
            }
        }

        public static void SessionConnectAsync(Guid? sessionGuid, Action<tfxdb_Grottbjorn> action)
            => Task.Run(() => SessionConnect(sessionGuid, action));

        public static Task<R> SessionConnectAsync<R>(Guid? sessionGuid, Func<tfxdb_Grottbjorn, R> action)
            => Task.Run(() => SessionConnect(sessionGuid, action));

        #endregion
    }
}
