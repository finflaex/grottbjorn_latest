﻿#region

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BaseGBFX.Tables;
using Finflaex.Support.DB.Bases.Abstracts;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Abstracts;
using Finflaex.Support.Reflection;

#endregion

namespace BaseGBFX.Bases
{
    partial class tfxdb_Grottbjorn
    {

    }

    public static class help_db
    {
        public static IQueryable<T> Actual<T>(this IQueryable<T> @this, DateTime? date = null)
            where T : fxaHistoryTable
        {
            var d = date ?? DateTime.Now;
            return @this
                .Where(v => v.verStartDate < d)
                .Where(v => v.verEndDate > d);
        }

        public static ICollection<T> Actual<T>(this ICollection<T> @this, DateTime? date = null)
            where T : fxaHistoryTable
        {
            var d = date ?? DateTime.Now;
            return @this
                .Where(v => v.verStartDate < d)
                .Where(v => v.verEndDate > d)
                .ToArray();
        }

        public static IQueryable<T> Actual<T>(this DbSet<T> @this, DateTime? date = null)
            where T : fxaHistoryTable => @this.AsType<IQueryable<T>>().Actual(date);
    }
}