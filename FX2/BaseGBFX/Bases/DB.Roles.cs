﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finflaex.Support.Reflection;

namespace BaseGBFX.Bases
{
    partial class DB
    {
        public class Roles
        {
            public const string Operator = "operator";
            public const string Admin = "admin";
            public const string Manager = "manager";
            public const string Consultant = "consultant";
        }
    }
}
