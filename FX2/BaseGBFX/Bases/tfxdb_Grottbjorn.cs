﻿#region

using System;
using System.Data.Entity;
using BaseGBFX.Tables;
using Finflaex.Support.DB.Bases.Abstracts;
using Finflaex.Support.DB.Tables;
using Finflaex.Support.DB.Tables.Federals;

#endregion

namespace BaseGBFX.Bases
{
    public partial class tfxdb_Grottbjorn : fxaDbContext<tfxdb_Grottbjorn, fxcLog, fxcLink>
    {
        public static DateTime CurrentDate = DateTime.Now;
        public DateTime Now = CurrentDate;
        public Login User => Logins.Actual().FirstOrDefaultAsync(v => v.ID == Author).Result;

        public tfxdb_Grottbjorn()
            : base("Data Source=mars;Integrated Security=False;User ID=sa;Password=411;Database=tfxdb")
            //: base(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=tfxdb;Integrated Security=True;")
        {
            CurrentDate = DateTime.Now;
        }

        protected override void OnModelCreating(DbModelBuilder db)
        {
            base.OnModelCreating(db);

            db.Entity<Login>().HasMany(v => v.Roles).WithMany(v => v.Logins);
            db.Entity<Login>().HasMany(v => v.Sessions).WithOptional(v => v.Login);
            db.Entity<Login>().HasOptional(v => v.Departament).WithMany(v => v.Logins);
            db.Entity<Login>().HasMany(v => v.Configs).WithRequired(v => v.User);
            db.Entity<Login>().HasOptional(v => v.Person).WithOptionalPrincipal(v => v.Login);
            db.Entity<Login>().HasMany(v => v.RecieveMessages).WithOptional(v => v.TargetUser);
            db.Entity<Login>().HasMany(v => v.SendingMessages).WithOptional(v => v.AuthorUser);

            //db.Entity<Login>().HasMany(v => v.OrgAccaunted).WithMany(v => v.Accaunted);
            db.Entity<Login>().HasMany(v => v.OrgAccaunted).WithRequired(v => v.Login);
            db.Entity<Organization>().HasMany(v => v.Accaunted).WithRequired(v => v.Organization);

            

            db.Entity<Organization>().HasOptional(v => v.Okopf).WithMany(v => v.Organizations);
            db.Entity<Organization>().HasMany(v => v.Addresses).WithMany(v => v.Organizations);
            db.Entity<Organization>().HasOptional(v => v.House).WithMany(v => v.Organizations);
            db.Entity<Organization>().HasMany(v => v.Okved1s).WithMany(v => v.Organizations);
            db.Entity<Organization>().HasOptional(v => v.MainOkved1).WithMany(v => v.MainOrganizations);
            db.Entity<Organization>().HasMany(v => v.Okved2s).WithMany(v => v.Organizations);
            db.Entity<Organization>().HasOptional(v => v.MainOkved2).WithMany(v => v.MainOrganizations);
            db.Entity<Organization>().HasOptional(v => v.Pfr).WithOptionalDependent(v => v.Organization);
            db.Entity<Organization>().HasOptional(v => v.Fss).WithOptionalDependent(v => v.Organization);
            db.Entity<Organization>().HasMany(v => v.Works).WithOptional(v => v.Organization);
            db.Entity<Organization>().HasOptional(v => v.Flat).WithOptionalPrincipal(v => v.Organization);
            db.Entity<Organization>().HasMany(v => v.Comments).WithOptional(v => v.Organization);
            db.Entity<Organization>().HasMany(v => v.CustomDeclarations).WithOptional(v => v.Organization);
            db.Entity<Organization>().HasMany(v => v.Tasks).WithOptional(v => v.Organization);
            db.Entity<Organization>().HasMany(v => v.Statuses).WithMany(v => v.Organizations);
            db.Entity<Organization>().HasMany(v => v.Directions).WithMany(v => v.Organizations);

            db.Entity<Departament>().HasMany(v => v.Comments).WithOptional(v => v.Departament);

            db.Entity<Address>().HasMany(v => v.Houses).WithOptional(v => v.Address);
            db.Entity<House>().HasMany(v => v.Flats).WithOptional(v => v.House);

            db.Entity<Pfr>().HasMany(v => v.Registrations).WithOptional(v => v.Pfr);
            db.Entity<Fss>().HasMany(v => v.Registrations).WithOptional(v => v.Fss);

            db.Entity<Person>().HasMany(v => v.Businessmans).WithOptional(v => v.Person);
            db.Entity<Person>().HasMany(v => v.DirectorOrganizations).WithMany(v => v.DirectorPersons);
            db.Entity<Person>().HasMany(v => v.FounderOrganizations).WithMany(v => v.FounderPersons);
            db.Entity<Person>().HasMany(v => v.PlaceBirthday).WithMany(v => v.PersonBirthday);
            db.Entity<Person>().HasMany(v => v.Works).WithOptional(v => v.Person);
            db.Entity<Person>().HasMany(v => v.Contacts).WithOptional(v => v.Person);
            db.Entity<Person>().HasMany(v => v.Comments).WithOptional(v => v.Person);

            db.Entity<Post>().HasMany(v => v.Works).WithOptional(v => v.Post);
            db.Entity<Post>().HasMany(v => v.Comments).WithOptional(v => v.Post);

            db.Entity<ContactDetails>().HasMany(v => v.Comments).WithOptional(v => v.Contact);
            db.Entity<ContactDetails>().HasMany(v => v.Tasks).WithOptional(v => v.Contact);

            db.Entity<Work>().HasMany(v => v.Tasks).WithOptional(v => v.Work);

            db.Entity<TaskEvent>().HasMany(v => v.Accountable).WithRequired(v => v.TaskEvent);
            db.Entity<TaskEvent>().HasOptional(v => v.Departament).WithMany(v => v.Tasks);
            db.Entity<Login>().HasMany(v => v.TaskAccaunted).WithRequired(v => v.Login);

            db.Entity<TaskEvent>().HasOptional(v => v.Type).WithMany(v => v.TaskEvents);
        }

        #region TABLE

        public DbSet<Login> Logins { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Session>  Sessions { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Okopf> Okopf { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<House> Houses { get; set; }
        public DbSet<Okved1> Okved1s { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Businessman> Businessmans { get; set; }
        public DbSet<Pfr> Pfrs { get; set; }
        public DbSet<PfrRegistration> PfrRegistrations { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Work> Works { get; set; }
        public DbSet<ContactDetails> Contacts { get; set; }
        public DbSet<Departament> Departaments { get; set; }

        public DbSet<CustomDeclaration> CustomDeclarations { get; set; }
        public DbSet<TaskEvent> TaskEvents { get; set; }
        public DbSet<TaskEventType> EventTypes { get; set; }

        public DbSet<OrgDirection> OrgDirections { get; set; }
        public DbSet<OrgStatus> OrgStatuses { get; set; }

        public DbSet<UserConfig> UserConfigs { get; set; }
        #endregion
    }
}