﻿#region

using System;
using System.Collections;
using System.IO;
using System.Text.Encodings.Web;
using CoreSupport.Reflection;
using CoreSupport.Type;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Razor;

#endregion

namespace CoreLimitless
{
    public class LimitlessFormGroup : IHtmlContent
    {
        public LimitlessFormGroup()
        {
            TypeInput = LimitlessTypeInput.text;
            HelpJustify = LimitlessJustify.left;
            IconPosition = LimitlessJustify.right;
            Bold = LimitlessBold.normal;
            Justify = LimitlessJustify.left;
            Size = LimitlessSize.@default;
        }

        public string Label { get; set; }
        public string PlaceHolder { get; set; }
        public LimitlessTypeInput TypeInput { get; set; }
        public bool ReadOnly { get; set; }
        public bool Disabled { get; set; }
        public object Value { get; set; }
        public bool AutoComplete { get; set; }
        public int MaxLength { get; set; }
        public int Rows { get; set; }
        public int Cols { get; set; }
        public bool Multiple { get; set; }
        public string Help { get; set; }
        public LimitlessJustify HelpJustify { get; set; }
        public LimitlessJustify Justify { get; set; }
        public string Icon { get; set; }
        public LimitlessJustify IconPosition { get; set; }
        public LimitlessBold Bold { get; set; }
        public LimitlessSize Size { get; set; }
        public DictSO Attributes { get; set; }
        public string Name { get; set; }
        public object ID { get; set; }

        public Func<object, HelperResult> Body { get; set; }
        public IHtmlContent Content { get; set; }

        public void WriteTo(TextWriter writer, HtmlEncoder encoder)
        {
            if (Body != null)
            {
                Body(null).WriteAction(writer);
                return;
            }

            if (TypeInput == LimitlessTypeInput.hidden)
            {
                writer.Write($"<input type='{TypeInput}'");
                ID.if_NOT_NULL(v => writer.Write($" id='{ID}'"));
                Name.IfNotEmpty(v => writer.Write($" name='{Name}'"));
                Attributes.if_NOT_NULL(v => v.ForEach(attr => writer.Write($" {attr.Key}='{attr.Value}'")));
                writer.Write($"'");
                Value.if_NOT_NULL(v => writer.Write($" value='{Value}'"));
                writer.Write($">");
                return;
            }

            writer.Write("<div class='form-group");
            Icon.IfNotEmpty(v =>
            {
                writer.Write(" has-feedback");
                switch (IconPosition)
                {
                    case LimitlessJustify.left:
                        writer.Write(" has-feedback-left");
                        break;
                }
            });
            writer.Write("'>");
            if (TypeInput != LimitlessTypeInput.checkbox)
                Label.IfNotEmpty(v => writer.Write($"<label class='control-label'>{v}</label>"));
            writer.Write("<div>");
            if (Content != null)
            {
                Content.WriteTo(writer, encoder);
            }
            else
            {
                switch (TypeInput)
                {
                    case LimitlessTypeInput.select:
                        writer.Write("<select");
                        ID.if_NOT_NULL(v => writer.Write($" id='{ID}'"));
                        Name.IfNotEmpty(v => writer.Write($" name='{Name}'"));
                        Attributes.if_NOT_NULL(v => v.ForEach(attr => writer.Write($" {attr.Key}='{attr.Value}'")));
                        writer.Write($" class='form-control");
                        switch (Size)
                        {
                            case LimitlessSize.xlg:
                            case LimitlessSize.lg:
                            case LimitlessSize.sm:
                            case LimitlessSize.xs:
                                writer.Write($" input-{Size}");
                                break;
                        }
                        switch (Bold)
                        {
                            case LimitlessBold.middle:
                                writer.Write(" text-semibold");
                                break;
                            case LimitlessBold.bold:
                                writer.Write(" text-bold");
                                break;
                        }
                        writer.Write($"'");
                        Multiple.if_TRUE(() => writer.Write(" multiple='multiple'"));
                        writer.Write(">");
                        Value.if_NOT_NULL(v =>
                        {
                            foreach (var option in (IEnumerable) v)
                                switch (option)
                                {
                                    case MainValue mainValue:
                                        writer.Write($"<option");
                                        mainValue.Key.IfNotEmpty(key => writer.Write($" value='{key}'"));
                                        mainValue.Guid.if_NOT_NULL(guid => writer.Write($" value='{guid}'"));
                                        mainValue.Flag.if_TRUE(() => writer.Write(" selected='selected'"));
                                        writer.Write($">{mainValue.Value}</option>");
                                        break;
                                    case KeyValue keyvalue:
                                        writer.Write($"<option value='{keyvalue.Key}'>{keyvalue.Value}</option>");
                                        break;
                                    default:
                                        writer.Write($"<option>{option.ToString()}</option>");
                                        break;
                                }
                        });
                        ReadOnly.if_TRUE(() => writer.Write(" readonly='readonly'"));
                        Disabled.if_TRUE(() => writer.Write(" disabled='disabled'"));
                        writer.Write("</select>");
                        break;
                    case LimitlessTypeInput.textarea:
                        writer.Write($"<{TypeInput}");
                        ID.if_NOT_NULL(v => writer.Write($" id='{ID}'"));
                        Name.IfNotEmpty(v => writer.Write($" name='{Name}'"));
                        Attributes.if_NOT_NULL(v => v.ForEach(attr => writer.Write($" {attr.Key}='{attr.Value}'")));
                        writer.Write($" class='form-control");
                        switch (Bold)
                        {
                            case LimitlessBold.middle:
                                writer.Write(" text-semibold");
                                break;
                            case LimitlessBold.bold:
                                writer.Write(" text-bold");
                                break;
                        }
                        writer.Write($"'");
                        PlaceHolder.IfNotEmpty(v => writer.Write($" placeholder='{v}'"));
                        ReadOnly.if_TRUE(() => writer.Write(" readonly='readonly'"));
                        Disabled.if_TRUE(() => writer.Write(" disabled='disabled'"));
                        (Rows > 0).if_TRUE(() => writer.Write($" rows='{Rows}'"));
                        (Cols > 0).if_TRUE(() => writer.Write($" cols='{Cols}'"));
                        writer.Write($">");
                        Value.if_NOT_NULL(v => writer.Write($"{v}"));
                        writer.Write($"</{TypeInput}>");
                        break;
                    case LimitlessTypeInput.submit:
                        writer.Write("<button type='submit'");
                        Attributes.if_NOT_NULL(v => v.ForEach(attr => writer.Write($" {attr.Key}='{attr.Value}'")));
                        writer.Write(">");
                        Value.if_NOT_NULL(v => writer.Write($"{Value}"));
                        writer.Write("<i class='icon-arrow-right14 position-right'></i>");
                        writer.Write("</button>");
                        break;
                    case LimitlessTypeInput.cancel:
                        writer.Write("<button type='cancel'");
                        Attributes.if_NOT_NULL(v => v.ForEach(attr => writer.Write($" {attr.Key}='{attr.Value}'")));
                        writer.Write(">");
                        Value.if_NOT_NULL(v => writer.Write($"{Value}"));
                        writer.Write("</button>");
                        break;
                    case LimitlessTypeInput.checkbox:
                        writer.Write("<div class='checkbox'><label>");
                        writer.Write("<input type='checkbox' class='styled'");
                        ID.if_NOT_NULL(v => writer.Write($" id='{ID}'"));
                        Name.IfNotEmpty(v => writer.Write($" name='{Name}'"));
                        Attributes.if_NOT_NULL(v => v.ForEach(attr => writer.Write($" {attr.Key}='{attr.Value}'")));
                        Value.AsType<bool>().if_TRUE(() => writer.Write(" checked='checked'"));
                        writer.Write(">");
                        Label.IfNotEmpty(writer.Write);
                        writer.Write("</label></div>");
                        break;
                    default:
                        writer.Write($"<input type='{TypeInput}'");
                        ID.if_NOT_NULL(v => writer.Write($" id='{ID}'"));
                        Name.IfNotEmpty(v => writer.Write($" name='{Name}'"));
                        Attributes.if_NOT_NULL(v => v.ForEach(attr => writer.Write($" {attr.Key}='{attr.Value}'")));
                        writer.Write($" class='form-control");
                        switch (Size)
                        {
                            case LimitlessSize.xlg:
                            case LimitlessSize.lg:
                            case LimitlessSize.sm:
                            case LimitlessSize.xs:
                                writer.Write($" input-{Size}");
                                break;
                        }
                        switch (Justify)
                        {
                            case LimitlessJustify.center:
                                writer.Write(" text-center");
                                break;
                            case LimitlessJustify.right:
                                writer.Write(" text-right");
                                break;
                        }
                        switch (Bold)
                        {
                            case LimitlessBold.middle:
                                writer.Write(" text-semibold");
                                break;
                            case LimitlessBold.bold:
                                writer.Write(" text-bold");
                                break;
                        }
                        writer.Write($"'");
                        PlaceHolder.IfNotEmpty(v => writer.Write($" placeholder='{v}'"));
                        ReadOnly.if_TRUE(() => writer.Write(" readonly='readonly'"));
                        Disabled.if_TRUE(() => writer.Write(" disabled='disabled'"));
                        Value.if_NOT_NULL(v => writer.Write($" value='{Value}'"));
                        AutoComplete.if_TRUE(() => writer.Write(" autocomplete='off'"));
                        (MaxLength > 0).if_TRUE(() => writer.Write($" maxlength='{MaxLength}'"));
                        writer.Write($">");
                        break;
                }
                Icon.IfNotEmpty(
                    v =>
                    {
                        writer.Write($"<div class='form-control-feedback' style='top: 2em'><i class='{Icon}'></i></div>");
                    });
            }
            Help.notEmpty()
                .if_TRUE(() =>
                {
                    writer.Write("<span class='help-block");
                    switch (HelpJustify)
                    {
                        case LimitlessJustify.center:
                            writer.Write(" text-center");
                            break;
                        case LimitlessJustify.right:
                            writer.Write(" text-right");
                            break;
                    }
                    writer.Write($"'>{Help}</span>");
                });
            writer.Write("</div>");
            writer.Write("</div>");
        }
    }

    public enum LimitlessTypeInput
    {
        text,
        password,
        select,
        textarea,
        range,
        color,
        tel,
        search,
        url,
        email,
        number,
        week,
        time,
        month,
        date,
        datetime,
        submit,
        cancel,
        hidden,
        checkbox
    }

    public enum LimitlessJustify
    {
        left,
        center,
        right
    }

    public enum LimitlessBold
    {
        normal,
        middle,
        bold
    }

    public enum LimitlessSize
    {
        xlg,
        lg,
        @default,
        sm,
        xs
    }
}