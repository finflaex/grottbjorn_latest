﻿#region

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using CoreSupport.Reflection;
using Microsoft.AspNetCore.Html;

#endregion

namespace CoreLimitless
{
    public class LimitlessPageHeader : IHtmlContent
    {
        public IEnumerable<LessLink> Buttons = new List<LessLink>();
        public string Caption = string.Empty;
        public string Icon = "icon-arrow-left52";
        public int MarginBottom = 20;
        public IEnumerable<LessLink> Menu = new List<LessLink>();
        public IEnumerable<LessLink> Path = new List<LessLink>();
        public string Subcaption = string.Empty;

        public void WriteTo(TextWriter writer, HtmlEncoder encoder)
        {
            writer.Write($@"
<div class='page-header page-header-default' style='margin-bottom: {MarginBottom}px;'>
    <div class='page-header-content'>
        <div class='page-title'>
            <h4>
                <i class='{Icon} position-left'></i>
                <span class='text-semibold'>{Caption}</span>
                {Subcaption}
            </h4>
            {Buttons.Any().ret(v => v ? "<a class='heading-elements-toggle'><i class='icon-more'></i></a>" : "")}
        </div>");

            Buttons.if_ANY(list =>
            {
                writer.Write("<div class='heading-elements'><div class='heading-btn-group'>");
                list.ForEach(button =>
                {
                    writer.Write("<a ");

                    button.Url
                        .if_NOT_NULL(v => writer.Write($"href='{v}' "));

                    writer.Write($"class='btn btn-link btn-float{button.Caption.retIfNotEmpty(v => " has-text")}'>");

                    button.Icon
                        .retIfNotEmpty(v => $"<i class='{v} text-primary'></i>")
                        .if_NOT_NULL(writer.Write);

                    button.Caption
                        .if_NOT_NULL(v => writer.Write($"<span>{v}</span>"));
                    writer.Write("</a>");
                });

                writer.Write("</div></div>");
            });

            writer.Write("</div>");

            if (Path.Any() || Menu.Any())
            {
                writer.Write(@"
<div class='breadcrumb-line'>
    <a class='breadcrumb-elements-toggle'>
        <i class='icon-menu-open'></i>
        </a>");

                Path.if_ANY(list =>
                {
                    writer.Write("<ul class='breadcrumb'>");
                    list.ForEach(item =>
                    {
                        writer.Write("<li><a");
                        item.Url
                            .retIfNotEmpty(v => $" href='{v}'")
                            .if_NOT_NULL(writer.Write);
                        writer.Write(">");
                        item.Icon
                            .retIfNotEmpty(v => $"<i class='{v} position-left'></i>")
                            .if_NOT_NULL(writer.Write);
                        item.Caption
                            .retIfNotEmpty(v => v)
                            .if_NOT_NULL(writer.Write);
                        writer.Write("</a></li>");
                    });
                    writer.Write("</ul>");
                });

                Menu.if_ANY(list =>
                {
                    writer.Write("<ul class='breadcrumb-elements'>");
                    list.ForEach(item =>
                    {
                        writer.Write("<li");
                        item.Childs
                            .ret_if_NULL(() => new List<LessLink>())
                            .if_ANY(v => { writer.Write(" class='dropdown'"); });
                        writer.Write("><a");
                        item.Childs
                            .ret_if_NULL(() => new List<LessLink>())
                            .if_ANY(v => { writer.Write(" class='dropdown-toggle' data-toggle='dropdown'"); });
                        item.Url
                            .retIfNotEmpty(v => $" href='{v}'")
                            .if_NOT_NULL(writer.Write);
                        writer.Write(">");
                        item.Icon
                            .retIfNotEmpty(v => $"<i class='{v} position-left'></i>")
                            .if_NOT_NULL(writer.Write);
                        item.Caption
                            .retIfNotEmpty(v => v)
                            .if_NOT_NULL(writer.Write);
                        writer.Write("</li>");
                        item.Childs
                            .ret_if_NULL(() => new List<LessLink>())
                            .if_ANY(sublist =>
                            {
                                writer.Write("<ul class='dropdown-menu dropdown-menu-right'>");
                                sublist.ForEach(subitem =>
                                {
                                    writer.Write("<li><a");
                                    subitem.Url
                                        .retIfNotEmpty(v => $" href='{v}'")
                                        .if_NOT_NULL(writer.Write);
                                    writer.Write(">");
                                    subitem.Icon
                                        .retIfNotEmpty(v => $"<i class='{v}'></i>")
                                        .if_NOT_NULL(writer.Write);
                                    subitem.Caption
                                        .retIfNotEmpty(v => v)
                                        .if_NOT_NULL(writer.Write);
                                    writer.Write("</a></li>");
                                });
                                writer.Write("</ul>");
                            });
                        writer.Write("</li>");
                    });
                    writer.Write("</ul>");
                });

                writer.Write("</div>");
            }
            writer.Write("</div>");
        }
    }

    public class LessLink
    {
        public string Url { get; set; }
        public string Icon { get; set; }
        public string Caption { get; set; }
        public IEnumerable<LessLink> Childs { get; set; }
    }
}