﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Encodings.Web;
using CoreSupport.Reflection;
using CoreSupport.Type;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Razor;

#endregion

namespace CoreLimitless
{
    public class LimitlessPanel : IHtmlContent
    {
        public string Title { get; set; }
        public bool Collapse { get; set; }
        public string Reload { get; set; }
        public bool Close { get; set; }
        public IEnumerable<IHtmlContent> FormElements { get; set; }
        public string Submit { get; set; }
        public string AjaxPostUrl { get; set; }
        public string PostUrl { get; set; }
        public string Target { get; set; }
        public string Method { get; set; }
        public DictSO FormAttributes { get; set; }

        public Func<object, HelperResult> Body { get; set; }
        private bool _collapsed { get; set; }

        public bool Collapsed
        {
            get {  return _collapsed; }
            set
            {
                if (value) Collapse = true;
                _collapsed = value;
            }
        }

        public string Class { get; set; }
        public string FormClass { get; set; }
        public string FormLabel { get; set; }
        public string ID { get; set; }

        public void WriteTo(TextWriter writer, HtmlEncoder encoder)
        {
            writer.Write($"<div class='panel panel-flat");
            _collapsed.if_TRUE(() => writer.Write(" panel-collapsed"));
            Class.IfNotEmpty(v => writer.Write($" {v}"));
            writer.Write("'");
            writer.Write(">");
            writer.Write("<div class='panel-heading'>");
            writer.Write("<h5 class='panel-title'>");
            Title.IfNotEmpty(writer.Write);
            if (Collapse || Close || Reload.notEmpty())
                writer.Write("<a class='heading-elements-toggle'><i class='icon-more'></i></a>");
            writer.Write("</h5>");
            if (Collapse || Close || Reload.notEmpty())
            {
                writer.Write("<div class='heading-elements'>");
                writer.Write("<ul class='icons-list'>");
                Collapse.if_TRUE(() => writer.Write("<li><a data-action='collapse'></a></li>"));
                Reload.IfNotEmpty(v => writer.Write($"<li><a data-action='reload' data-url='{v}'></a></li>"));
                Close.if_TRUE(() => writer.Write("<li><a data-action='close'></a></li>"));
                writer.Write("</ul>");
                writer.Write("</div>");
            }
            writer.Write("</div>");

            writer.Write("<div class='panel-body'");
            ID.IfNotEmpty(id => writer.Write($" id='{id}'"));
            writer.Write(">");

            if (Body != null)
            {
                Body(null).WriteAction(writer);
            }
            else if (FormElements != null)
            {
                var form_id = Guid.NewGuid().ToString("N");
                writer.Write($"<form class='form-horizonal {FormClass}' id={form_id}");
                PostUrl.IfNotEmpty(v => writer.Write($" action='{v}' method='post'"));
                FormAttributes.if_NOT_NULL(v => v.ForEach(attr => writer.Write($" {attr.Key}='{attr.Value}'")));
                writer.Write($">");
                writer.Write("<fieldset class='content-group'>");
                FormLabel.IfNotEmpty(v => writer.Write($"<legend class='text-bold'>{v}</legend>"));
                FormElements.ForEach(el => el.WriteTo(writer, encoder));
                writer.Write("</fieldset>");
                Submit.IfNotEmpty(v =>
                {
                    writer.Write("<div class='text-right  m-10'>");
                    writer.Write("<button type='submit' class='btn btn-primary'>");
                    writer.Write(v);
                    writer.Write("<i class='icon-arrow-right14 position-right'></i>");
                    writer.Write("</button>");
                    writer.Write("</div>");
                });
                writer.Write("</form>");
                AjaxPostUrl.IfNotEmpty(v =>
                {
                    writer.Write($@"
<script>
    $('#{form_id}').submit(function(e) {{
        e.preventDefault();
        $.ajax({{
            type: 'POST',
            url: '{AjaxPostUrl}',
            data: JSON.stringify(getFormData($('#{form_id}'))),
            success: function(e) {{
                    ");
                    Target.IfNotEmpty(target => writer.Write($"$('{target}').html(e);"));
                    Method.IfNotEmpty(target => writer.Write($"window['{target}'](e);"));
                    writer.Write(@"},});});</script>");
                });
            }


            writer.Write("</div>");
            writer.Write("</div>");
        }
    }
}