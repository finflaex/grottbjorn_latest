﻿using CoreSupport.DataBase.Base;
using CoreSupport.MVC.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace CoreWeb2.Services
{
    public class ServiceDB<DB>
        where DB : FxDbContext<DB>
    {
        public readonly DB db;

        public ServiceDB(DB db, IHttpContextAccessor httpContextAccessor)
        {
            db.Context = httpContextAccessor.HttpContext;
            db.RouteData = httpContextAccessor.HttpContext.GetRouteData();
            this.db = db;
        }

        public AuthRecord Author => db.Context.AuthRecord();
    }
}