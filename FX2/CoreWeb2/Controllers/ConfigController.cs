﻿#region

using CoreDB.Base;
using CoreWeb;
using Microsoft.AspNetCore.Http;

#endregion

namespace CoreWeb2.Controllers
{
    public class ConfigController : DBController<DataDb>
    {
        public ConfigController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }
    }
}