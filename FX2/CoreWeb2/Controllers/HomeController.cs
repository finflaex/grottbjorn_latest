﻿#region

using System.Threading.Tasks;
using CoreDB.Base;
using CoreSupport.Reflection;
using CoreWeb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace CoreWeb2.Controllers
{
    public class HomeController : DBController<DataDb>
    {
        public HomeController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        //[Authorize(Roles = dbr.admin)]
        public IActionResult Error()
        {
            return View();
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index");
        }
        //        from user in db.Users
        //    var query =
        //{
        //public async Task<JsonResult> get_user_info()

        //[Authorize]
        //        where user.ID == Author.Guid
        //        select new
        //        {
        //            Login = user.Person.ShortName ?? user.Login,
        //            roles = from link in user.RoleLinks
        //            select link.Role.Description
        //        };

        //    var read = await query.FirstOrDefaultAsync();

        //    return Json(new
        //    {
        //        caption = read.Login,
        //        role = read.roles.Join(", ")
        //    });
        //}

        //public IActionResult Ping() => View();
    }
}