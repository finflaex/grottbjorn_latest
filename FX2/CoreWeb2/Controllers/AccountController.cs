﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using CoreDB.Base;
using CoreSupport.MVC.Authorization;
using CoreSupport.Reflection;
using CoreWeb;
using CoreWeb2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace CoreWeb2.Controllers
{
    public class AccountController : DBController<DataDb>
    {
        public AccountController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        [HttpGet]
        public IActionResult Login(string ReturnUrl)
        {
            ViewData["ReturnUrl"] = ReturnUrl;
            return View();
        }

        public async Task<IActionResult> AccessDenied()
        {
            return await Task.Run(() => { return View(); });
        }
    }
}