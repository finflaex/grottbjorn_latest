﻿#region

using System.Threading.Tasks;
using CoreDB.Base;
using CoreDB.Repository;
using CoreSupport.MVC.Authorization;
using CoreSupport.Reflection;
using CoreSupport.Type;

#endregion

namespace CoreWeb2.Websockets
{
    public class SystemLoginMiddleware : FxWsMiddleware<SystemLoginMiddleware, DataDb>
    {
        public override async Task OnRecieve(string recieve)
        {
            var data = recieve.FromJson<DictSO>();

            var read_password = data.GetString("password");
            var read_login = data.GetString("login");
            var read_url = data.GetString("url");

            var read = await DataRepository.Create<UserRepository>(db).GetAuthor(read_login, read_password);

            if (read != null)
            {
                await Log("OK");
                await SetCookie(read);
                await Redirect(read_url);
            }
            else
            {
                await Log("Ошибка доступа");
            }
        }

        private async Task SetCookie(AuthRecord read)
        {
            await Send(new
            {
                key = "set_cookie",
                value = read.ToJson()
            });
        }

        private async Task Log(string message)
        {
            await Send(new
            {
                key = "log",
                value = message
            });
        }

        private async Task Redirect(string url)
        {
            await Send(new
            {
                key = "redirect",
                value = url
            });
        }
    }
}