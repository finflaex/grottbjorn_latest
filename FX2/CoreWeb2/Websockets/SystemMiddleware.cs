﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Workflow.Runtime.Tracking;
using CoreDB.Base;
using CoreDB.Repository;
using CoreSupport.DataBase.Table;
using CoreSupport.MVC.Authorization;
using CoreSupport.Reflection;
using CoreSupport.Type;
using CoreWeb;
using CoreWeb2.Models;
using CoreDB.Tables;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

#endregion

namespace CoreWeb2.Websockets
{
    public class SystemMiddleware : FxWsMiddleware<SystemMiddleware, DataDb>
    {
        public static List<SystemMiddleware> List = new List<SystemMiddleware>();

        private RoleRepository _roleRep;
        public RoleRepository RoleRep => _roleRep ?? (_roleRep = new RoleRepository(db));

        private UserRepository _userRep;
        public UserRepository UserRep => _userRep ?? (_userRep = new UserRepository(db));

        public override async Task OnRecieve(string recieve)
        {
            var data = recieve.FromJson<DictSO>();
            var key = data.GetString("key");

            var pattern = @"coreweb2\.areas\.(\S+)\.controllers\.(\S+)controller";
            var area = data.GetString("area").ToLower();
            var controller = data.GetString("controller").ToLower();
            var method = data.GetString("method").ToLower();

            Assembly
                .GetEntryAssembly()
                .GetTypes()
                .Select(v => new
                {
                    name = v.FullName.ToLower(),
                    type = v
                })
                .Where(v =>
                    v.name.DoRegexIsMatch(pattern))
                .Select(v =>
                {
                    var match = v.name.DoRegexMatches(pattern);
                    return new
                    {
                        area = match[0].Groups[1].Value,
                        controller = match[0].Groups[2].Value,
                        v.type
                    };
                })
                .Where(v => v.area == area)
                .Where(v => v.controller == controller)
                .Select(v => new
                {
                    v.type,
                    methods = v.type
                        .GetMethods()
                        .Where(m => m.Name.ToLower() == method)
                        .ToArray()
                })
                .Select(v => v.methods.FirstOrDefault())
                .Where(v => v != null)
                .ForEach(async v =>
                {
                    var result = v.Invoke(null, new object[] { this, data });
                    if (v.IsAsync())
                    {
                        await result.AsType<Task>();
                    }
                });

            await db.Transaction(async () =>
            {
                switch (key)
                {
                    //case "start":
                    //    var leftUrl = await UserRep.GetConfigString(Author.Guid, "menu");
                    //    await LoadLeftPanel(leftUrl);

                    //    var leftSidebarUrl = leftUrl;
                    //    if (leftUrl.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Length < 3)
                    //        leftSidebarUrl += "/index";
                    //    leftSidebarUrl += "_leftSidebar";

                    //    var leftSidebar = await UserRep.GetConfigBool(Author.Guid, "sidebar_left");
                    //    if (!leftSidebar)
                    //        await ShowLeftSidebar(leftSidebarUrl);
                    //    else
                    //        await LoadLeftSidebar(leftSidebarUrl);

                    //    break;
                    //case "sidebar_size":
                    //case "sidebar_left":
                    //case "sidebar_right":
                    //    await UserRep.SetConfig(Author.Guid, Author.Guid, key, data.GetBool("flag"));
                    //    break;
                    case "admin_search_role":
                    case "admin_search_user":
                        await UserRep.SetConfig(Author.Guid, Author.Guid, key, data.GetString("string"));
                        break;
                    //case "menu":
                    //    await OnClickMenu(data);
                    //    break;
                    case "LoadRightPanel":
                        await LoadRightPanel(data["url"]);
                        break;
                    case "LoadRightPanelID":
                        await LoadRightPanel($"{data["url"]}?id={data["id"]}");
                        break;
                }
            });
        }


        public override Task OnConnect()
        {
            List.Add(this);

            return base.OnConnect();
        }

        public override Task OnDisconnect()
        {
            List.Remove(this);

            return base.OnDisconnect();
        }


        private async Task OnClickMenu(DictSO data)
        {
            var url = data.GetString("url")?.ToLower();
            switch (url)
            {
                case "/admin/roles":
                case "/admin/users":
                case "/admin/dbhistory":
                    await LoadLeftPanel(url);
                    await LoadLeftSidebar($"{url}/index_leftsidebar");
                    //await ShowLeftSidebar(url);
                    //await ShowRightSidebar(url);
                    //await LoadRightPanel(url);
                    break;
                default:
                    throw new Exception();
            }
            await UserRep.SetConfig(Author.Guid, Author.Guid, "menu", url);
        }

        private async Task LoadLeftPanel(string url)
        {
            if (url.isEmpty()) return;
            await Send(new
            {
                target = "#panel_left",
                action = "load",
                url = url
            });
        }

        private async Task LoadRightPanel(object url)
        {
            if (url.ToString().isEmpty()) return;
            await Send(new
            {
                target = "#panel_right",
                action = "load",
                url = url
            });
        }

        private async Task LoadLeftSidebar(string url)
        {
            if (url.isEmpty()) return;
            await Send(new
            {
                target = ".sidebar-secondary",
                action = "load",
                url = url
            });
        }

        private async Task LoadRightSidebar(string url)
        {
            if (url.isEmpty()) return;
            await Send(new
            {
                target = ".sidebar-opposite",
                action = "load",
                url = url
            });
        }

        private async Task ShowLeftSidebar(string url)
        {
            if (url.isEmpty()) return;
            await Send(new {action = "show_left_sidebar", url});
        }

        private async Task HideLeftSidebar()
            => await Send(new { action = "hide_left_sidebar" });

        private async Task ShowRightSidebar(string url)
        {
            if (url.isEmpty()) return;
            await Send(new {action = "show_right_sidebar", url});
        }

        private async Task HideRightSidebar()
            => await Send(new { action = "hide_right_sidebar" });
    }
}