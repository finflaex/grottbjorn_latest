﻿/// <reference path="../lib/dt-jquery2/jquery.d.ts"/>

$(document).on('click',
	'.sidebar-main a[href]',
	e => {
		e.preventDefault();

		send({
			area: "config",
			controller: "main",
			method: "ClickMenu",

			url: $(e.currentTarget).attr('href'),
		});

		return 0;
	});