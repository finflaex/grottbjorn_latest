﻿/// <reference path="../lib/dt-jquery2/jquery.d.ts"/>

function containerHeight() {

	var availableHeight = $(window).height() -
		($(".page-container").offset().top | 0) -
		($(".navbar-fixed-bottom").outerHeight() | 0);

	$(".page-container").attr("style", "min-height:" + availableHeight + "px");
}

$(document)
	.on('click',
		'.category-title [data-action=collapse]',
		e => {
			e.preventDefault();
			var $categoryCollapse = $(this).parent().parent().parent().nextAll();
			$(this).parents(".category-title").toggleClass("category-collapsed");
			$(this).toggleClass("rotate-180");

			containerHeight(); // adjust page height

			$categoryCollapse.slideToggle(150);
		});