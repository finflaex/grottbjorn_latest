var config = (function () {
    function config() {
    }
    config.save_sidebar_size = function () {
        send({
            area: "config",
            controller: "main",
            method: "SaveSidebar",
            key: "sidebar_size",
            flag: $('body').hasClass('sidebar-xs')
        });
    };
    config.save_sidebar_left = function () {
        send({
            area: "config",
            controller: "main",
            method: "SaveSidebar",
            key: "sidebar_left",
            flag: $('body').hasClass('sidebar-secondary-hidden')
        });
    };
    config.save_sidebar_right = function () {
        send({
            area: "config",
            controller: "main",
            method: "SaveSidebar",
            key: "sidebar_right",
            flag: !$('body').hasClass('sidebar-opposite-visible')
        });
    };
    return config;
}());
//# sourceMappingURL=config.js.map