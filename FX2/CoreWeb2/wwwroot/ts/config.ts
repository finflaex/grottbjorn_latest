﻿class config {

	public static save_sidebar_size() {
		send({
			area: "config",
			controller: "main",
			method: "SaveSidebar",

			key: "sidebar_size",
			flag: $('body').hasClass('sidebar-xs')
		});
	}

	public static save_sidebar_left() {
		send({
			area: "config",
			controller: "main",
			method: "SaveSidebar",

			key: "sidebar_left",
			flag: $('body').hasClass('sidebar-secondary-hidden')
		});
	}

	public static save_sidebar_right() {
		send({
			area: "config",
			controller: "main",
			method: "SaveSidebar",

			key: "sidebar_right",
			flag: !$('body').hasClass('sidebar-opposite-visible')
		});
	}
}