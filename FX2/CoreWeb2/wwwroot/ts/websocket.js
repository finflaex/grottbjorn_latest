/// <reference path="../lib/dt-jquery2/jquery.d.ts" />
var WsMessage = (function () {
    function WsMessage() {
    }
    return WsMessage;
}());
var ws = new WebSocket((location.protocol === "https:" ? "wss:" : "ws:") + "//" + window.location.host + "/system");
ws.onopen = function () {
    //console.log("start ws");
    //var data = new WsMessage();
    //data.key = "start";
    //send(data);
    send({
        area: "config",
        controller: "main",
        method: "Start",
    });
};
//ws.onclose = () => {
//	console.log("stop ws");
//};
ws.onmessage = function (e) {
    var item = JSON.parse(e.data);
    switch (item.action) {
        case "load":
            html_load(item.target, item.url);
            break;
        case "show_left_sidebar":
            $('body').removeClass('sidebar-secondary-hidden');
            config.save_sidebar_left();
            html_load('.sidebar-secondary', item.url);
            break;
        case "show_right_sidebar":
            $('body').addClass('sidebar-opposite-visible');
            config.save_sidebar_right();
            html_load('.sidebar-opposite', item.url);
            break;
        case "hide_left_sidebar":
            $('body').addClass('sidebar-secondary-hide');
            config.save_sidebar_left();
            break;
        case "hide_right_sidebar":
            $('body').removeClass('sidebar-opposite-visible');
            config.save_sidebar_right();
            break;
    }
};
function send(data) {
    ws.send(JSON.stringify(data));
}
function html_load(target, url) {
    var el = $(target);
    el.html('<i class="icon-spinner2 spinner"></i>');
    $.post(url, function (data) {
        el.html(data);
    })
        .fail(function () {
        el.html('<h1 class="error-title" style="font-size:82px;">ERROR</h1>');
    });
}
function ajaxFormNotify(formId, url) {
    var notify = $("#notification").data("kendoNotification");
    var form = $(formId);
    var data = form.serialize();
    form.find('input, textarea, button, select').attr('disabled', 'disabled');
    return $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: 'html',
        success: function (data) {
            notify.show(data, 'info');
        },
        statusCode: {
            404: function () {
                notify.show("error 404", 'error');
            },
            500: function (data) {
                notify.show(data.responseText, 'error');
            }
        },
        complete: function () {
            form.find('input, textarea, button, select').removeAttr('disabled');
        }
    });
}
//# sourceMappingURL=websocket.js.map