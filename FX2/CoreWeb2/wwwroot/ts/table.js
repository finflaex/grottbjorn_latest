/// <reference path="../lib/dt-jquery2/jquery.d.ts"/>
var _this = this;
function containerHeight() {
    var availableHeight = $(window).height() -
        ($(".page-container").offset().top | 0) -
        ($(".navbar-fixed-bottom").outerHeight() | 0);
    $(".page-container").attr("style", "min-height:" + availableHeight + "px");
}
$(document)
    .on('click', '.category-title [data-action=collapse]', function (e) {
    e.preventDefault();
    var $categoryCollapse = $(_this).parent().parent().parent().nextAll();
    $(_this).parents(".category-title").toggleClass("category-collapsed");
    $(_this).toggleClass("rotate-180");
    containerHeight(); // adjust page height
    $categoryCollapse.slideToggle(150);
});
//# sourceMappingURL=table.js.map