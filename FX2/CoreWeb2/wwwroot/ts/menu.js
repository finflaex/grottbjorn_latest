/// <reference path="../lib/dt-jquery2/jquery.d.ts"/>
$(document).on('click', '.sidebar-main a[href]', function (e) {
    e.preventDefault();
    send({
        area: "config",
        controller: "main",
        method: "ClickMenu",
        url: $(e.currentTarget).attr('href'),
    });
    return 0;
});
//# sourceMappingURL=menu.js.map