﻿function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};
    $.map(unindexed_array,
        function (n, i) {
            if (n['value'] === 'on') {
                n['value'] = true;
            }
            if (indexed_array[n['name']]) {
                indexed_array[n['name']] = indexed_array[n['name']] + ',' + n['value'];
            } else {
                indexed_array[n['name']] = n['value'];
            }
        });
    return indexed_array;
}