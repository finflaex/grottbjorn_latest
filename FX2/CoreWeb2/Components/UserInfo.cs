﻿#region

using CoreDB.Base;
using CoreWeb;
using Microsoft.AspNetCore.Http;

#endregion

namespace CoreWeb2.Components
{
    public class UserInfo : DBView<DataDb>
    {
        public UserInfo(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }
    }
}