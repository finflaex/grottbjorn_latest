﻿#region

using CoreDB.Base;
using CoreWeb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace CoreWeb2.Components
{
    public class NavBar_xs : DBView<DataDb>
    {
        public NavBar_xs(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public override IViewComponentResult Invoke(
            params object[] param)
            => View();
    }
}