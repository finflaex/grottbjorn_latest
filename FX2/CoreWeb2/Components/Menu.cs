﻿#region

using CoreDB.Base;
using CoreWeb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace CoreWeb2.Components
{
    public class Menu : DBView<DataDb>
    {
        public Menu(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public override IViewComponentResult Invoke(
            params object[] param)
            => View();
    }
}