﻿#region

using CoreDB.Base;
using CoreWeb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

#endregion

namespace CoreWeb2.Components
{
    public class NavBar : DBView<DataDb>
    {
        public NavBar(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public override IViewComponentResult Invoke(
            params object[] param)
            => View();
    }
}