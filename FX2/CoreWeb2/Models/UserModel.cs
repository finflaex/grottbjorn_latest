﻿#region

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace CoreWeb2.Models
{
    public class UserModel
    {
        [Display(Name = "Логин")]
        public string Login { get; set; }

        public Guid ID { get; set; }

        [Display(Name = "Роли")]
        public string Roles { get; set; }
    }
}