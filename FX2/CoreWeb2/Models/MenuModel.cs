﻿#region

using System.ComponentModel.DataAnnotations;

#endregion

namespace CoreWeb2.Models
{
    public class MenuModel
    {
        [Display(Name = "Название")]
        public string Caption { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Ссылка")]
        public string UrlLink { get; set; }

        [Display(Name = "Заголовок")]
        public bool Header { get; set; }

        [Display(Name = "Иконка")]
        public string Icon { get; set; }

        public string ParentID { get; set; }
        public string ID { get; set; }

        [Display(Name = "Роли")]
        public string RoleView { get; set; }

        [Display(Name = "Метка")]
        public string Label { get; set; }
    }
}