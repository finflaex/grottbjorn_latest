﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWeb2.Areas.Admin.Models
{
    public class ModelUser
    {
        public Guid ID { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Caption { get; set; }

        [Required]
        [Display(Name = "Логин")]
        public string Login { get; set; }

        [Required]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Роли")]
        public IEnumerable<string> Roles { get; set; }

        [Required]
        [Display(Name = "Часовой пояс")]
        public int TimeZone { get; set; }
    }
}
