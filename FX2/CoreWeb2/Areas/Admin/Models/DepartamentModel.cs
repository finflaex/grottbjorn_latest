﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWeb2.Areas.Admin.Models
{
    public class DepartamentModel
    {
        public string id { get; set; }
        public string caption { get; set; }
        public string description { get; set; }
        public string parentId { get; set; }
        public int oldid { get; set; }
        public int order { get; set; }
    }
}
