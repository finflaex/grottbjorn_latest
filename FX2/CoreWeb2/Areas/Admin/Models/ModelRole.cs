﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWeb2.Areas.Admin.Models
{
    public class ModelRole
    {
        public Guid ID { get; set; }

        [Required]
        [Display(Name = "Ключ")]
        public string Key { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string Value { get; set; }
    }
}
