﻿#region

using System;

#endregion

namespace CoreWeb2.Areas.Admin.Models
{
    public class PersonModel
    {
        public Guid? ID { get; set; }
        public Guid? UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string InnerPhoneNumber { get; set; }
        public string InnerEMailAddress { get; set; }
        public string RedirectPath { get; set; }
        public int TimeZone { get; set; }
    }
}