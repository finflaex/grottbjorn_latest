#region

using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using LibCoreDB;
using CoreSupport.Reflection;
using CoreSupport.Type;
using CoreWeb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

#endregion

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = dbr.admin)]
    public class MenuItemController : DBController<DataDb>
    {
        public MenuItemController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        //public async Task<IActionResult> Index(Guid? id, string message)
        //{
        //    return await Task.Run(() =>
        //    {
        //        db.DataModel = new MainValue
        //        {
        //            Guid = id ?? Guid.Empty,
        //            Value = message
        //        };
        //        return View(db);
        //    });
        //}

        //public async Task<IActionResult> Save(SaveMenuModel model)
        //{
        //    var query =
        //        from menu in db.Menus
        //        where menu.ID == model.ID
        //        select new
        //        {
        //            menu,
        //            has_roles = from role in menu.RolesLinks
        //            select role
        //        };
        //    var query_roles =
        //        from role in db.Roles
        //        select role;
        //    var roles = await query_roles.ToArrayAsync();
        //    var read = await query.FirstOrDefaultAsync();

        //    var record = (read?.menu).ret_if_NULL(() => db.Add(new Menu()).Entity);


        //    record.Url = model.Url;
        //    record.Caption = model.Caption;
        //    record.Description = model.Description;
        //    record.Header = model.Header.notEmpty();
        //    record.Icon = model.Icon;
        //    record.Label = model.Label;

        //    await db.SaveChangesAsync();

        //    model.Roles
        //        .ActionSaveLinkInDB(read?.has_roles.Select(v => v.RoleKey),
        //            delete =>
        //            {
        //                read?.has_roles
        //                    .FirstOrDefault(v => v.RoleKey == delete)
        //                    .if_NOT_NULL(db.Remove);
        //                db.SaveChangesAsync().Wait();
        //            },
        //            add =>
        //            {
        //                roles
        //                    .FirstOrDefault(v => v.ID == add)
        //                    .if_NOT_NULL(role =>
        //                    {
        //                        db.Add(new MenuRole
        //                        {
        //                            Role = role,
        //                            Menu = record
        //                        });
        //                    });
        //                db.SaveChangesAsync().Wait();
        //            });

        //    return RedirectToAction("Index", new
        //    {
        //        id = record.ID
        //    });
        //}

        public class SaveMenuModel
        {
            public Guid ID { get; set; }
            public string Caption { get; set; }
            public string Description { get; set; }
            public string Url { get; set; }
            public string Header { get; set; }
            public IEnumerable<string> Roles { get; set; }
            public string Icon { get; set; }
            public string Label { get; set; }
        }
    }
}