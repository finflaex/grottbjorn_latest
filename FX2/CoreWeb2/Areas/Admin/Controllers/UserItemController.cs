#region

using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using LibCoreDB;
using CoreSupport.Reflection;
using CoreSupport.Type;
using CoreWeb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

#endregion

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = dbr.admin)]
    public class UserItemController : DBController<DataDb>
    {
        public UserItemController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        //public async Task<IActionResult> Index(Guid? id = null, string message = null)
        //{
        //    return await Task.Run(() =>
        //    {
        //        db.DataModel = new MainValue
        //        {
        //            Guid = id ?? Guid.Empty,
        //            Value = message
        //        };
        //        return View(db);
        //    });
        //}

        //public async Task<IActionResult> Save(SaveUserModel model)
        //{
        //    var query =
        //        from user in db.Users
        //        where user.ID == model.ID
        //        select new
        //        {
        //            user,
        //            has_roles = from role in user.RoleLinks
        //            select role,
        //            has_deps = from dep in user.Departaments
        //            select dep
        //        };
        //    var query_roles =
        //        from role in db.Roles
        //        select role;
        //    var query_deps =
        //        from dep in db.Departaments
        //        select dep;

        //    var deps = await query_deps.ToArrayAsync();
        //    var roles = await query_roles.ToArrayAsync();
        //    var read = await query.FirstOrDefaultAsync();

        //    var record = (read?.user).ret_if_NULL(() => db.Add(new User()).Entity);

        //    record.Login = model.Login;
        //    record.Password = model.Password.Encrypt();

        //    await db.SaveChangesAsync();

        //    model.Roles.ActionSaveLinkInDB(
        //        read?.has_roles.Select(v => v.RoleKey),
        //        delete =>
        //        {
        //            read?.has_roles
        //                .FirstOrDefault(v => v.RoleKey == delete)
        //                .if_NOT_NULL(db.Remove);
        //        },
        //        add =>
        //        {
        //            roles
        //                .FirstOrDefault(v => v.ID == add)
        //                .if_NOT_NULL(role => db.Add(new UserRole
        //                {
        //                    Role = role,
        //                    User = record
        //                }));
        //        });

        //    await db.SaveChangesAsync();

        //    model.Departaments.ActionSaveLinkInDB(
        //        read?.has_deps.Select(v => v.DepartamentID.Value),
        //        delete =>
        //        {
        //            read?.has_deps
        //                .FirstOrDefault(v => v.DepartamentID == delete)
        //                .if_NOT_NULL(db.Remove);

        //        },
        //        add =>
        //        {
        //            deps
        //                .FirstOrDefault(v => v.ID == add)
        //                .if_NOT_NULL(dep =>
        //                {
        //                    record.Departaments.AddDepartament(dep);
        //                });
        //        });

        //    await db.SaveChangesAsync();

        //    return RedirectToAction("Index", new
        //    {
        //        id = record.ID
        //    });
        //}

        //public async Task<IActionResult> Delete(Guid? id)
        //{
        //    var query =
        //        from user in db.Users
        //        where user.ID == id
        //        select user;

        //    var read = await query.FirstOrDefaultAsync();

        //    if (read != null)
        //    {
        //        db.Remove(read);
        //        await db.CommitAsync(AuthorID);
        //    }
        //    return RedirectToAction("Index", "UserList");
        //}

        public class SaveUserModel
        {
            public Guid ID { get; set; }
            public string Login { get; set; }
            public string Password { get; set; }
            public IEnumerable<string> Roles { get; set; }
            public IEnumerable<Guid> Departaments { get; set; }
        }
    }
}