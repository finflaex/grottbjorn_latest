using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibCoreDB;
using CoreSupport.Reflection;
using CoreWeb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "admin, developer")]
    public class BaseActionsController : DBController<DataDb>
    {
        public BaseActionsController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public async Task<IActionResult> Index() => await Task.Run(() =>
        {
            return View(db);
        });

        public async Task<IActionResult> ResetDb()
        {
            try
            {
                //db.RemoveRange(db.Events);
                //db.RemoveRange(db.Organizations);
                //db.RemoveRange(db.Entities);
                //db.RemoveRange(db.LeadGroups);
                //db.RemoveRange(db.Leads);

                await db.CommitAsync();
                return Content("����������� �������");
            }
            catch (Exception err)
            {
                return Content(err.ToFinflaexString());
            }
        }
    }
}