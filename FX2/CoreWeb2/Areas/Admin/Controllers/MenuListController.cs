#region

using System.Linq;
using System.Threading.Tasks;
using LibCoreDB;
using CoreSupport.Reflection;
using CoreWeb;
using CoreWeb2.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

#endregion

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = dbr.admin)]
    public class MenuListController : DBController<DataDb>
    {
        public MenuListController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public async Task<IActionResult> Index()
        {
            return await Task.Run(() => { return View(db).AsType<IActionResult>(); });
        }

        //public async Task<IActionResult> Read(
        //    [DataSourceRequest] DataSourceRequest request)
        //{
        //    var query =
        //        from menu in db.Menus
        //        select menu;

        //    var read = await query.ToArrayAsync();

        //    return read
        //        .Select(item => new MenuModel
        //        {
        //            UrlLink = item.Url,
        //            ID = item.ID.ToString(),
        //            Caption = item.Caption,
        //            Icon = item.Icon,
        //            ParentID = item.ParentID?.ToString() ?? string.Empty,
        //            Description = item.Description,
        //            Header = item.Header,
        //            Label = item.Label
        //        })
        //        .ToDataSourceResult(request)
        //        .ToJsonResult();
        //}

        //public async Task<IActionResult> Update(
        //    [DataSourceRequest] DataSourceRequest request,
        //    MenuModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var id = model.ID.ToNullableGuid();
        //        var pid = model.ParentID.ToNullableGuid();

        //        var query =
        //            from menu in db.Menus
        //            where menu.ID == id
        //            select menu;

        //        var read = await query.FirstOrDefaultAsync();
        //        read.ParentID = pid;

        //        await db.CommitAsync(AuthorID);

        //        model.ParentID = pid == null ? string.Empty : pid.ToString();
        //    }
        //    return model.InArray().ToDataSourceResult(request).ToJsonResult();
        //}

        //public async Task<IActionResult> Destroy(
        //    [DataSourceRequest] DataSourceRequest request,
        //    MenuModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var read = await db.Menus.FindAsync(model.ID.ToGuid());
        //        db.Menus.Remove(read);
        //        await db.SaveChangesAsync();
        //    }

        //    return model.InArray().ToDataSourceResult(request).ToJsonResult();
        //}
    }
}