#region

using System.Threading.Tasks;
using CoreWeb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using CoreDB.Base;
using CoreSupport.Reflection;
using Microsoft.EntityFrameworkCore;

#endregion

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize(Roles = "system,admin")]
    public class DbHistoryController : DBController<DataDb>
    {
        public DbHistoryController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public IActionResult Index() => View();
        public IActionResult Index_LeftSidebar() => View();

        public IActionResult FormClearOldEvents() => View();

        public async Task<IActionResult> ClearOldEvents()
        {
            await db.Links
                .Where(v=>v.End < db.Now)
                .ToArrayAsync()
                .act(db.Links.RemoveRange);

            await db.DynValues
                .Where(v => v.End < db.Now)
                .ToArrayAsync()
                .act(db.DynValues.RemoveRange);

            await db.CommitAsync();

            return "�������� ������� ���������".ToHtmlResult();
        }
    }
}