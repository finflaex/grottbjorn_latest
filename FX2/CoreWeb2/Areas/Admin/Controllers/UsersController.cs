using System;
using System.Threading.Tasks;
using CoreDB;
using CoreWeb;
using CoreWeb2.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Kendo.Mvc.UI;
using CoreDB.Tables;
using System.Linq;
using CoreDB.Base;
using CoreDB.Migrations;
using CoreDB.Repository;
using Kendo.Mvc.Extensions;
using CoreSupport.Reflection;
using CoreSupport.Type;
using Microsoft.EntityFrameworkCore;

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize(Roles = "system,admin")]
    public class UsersController : DBController<DataDb>
    {
        public IActionResult Index() => View();
        public IActionResult Index_LeftSidebar() => View();

        public UsersController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public async Task<IActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            string contains = null)
        {
            var flag = contains.isEmpty();

            var query =
                from user in db.Keys.OfType<tbUser>()

                from role in user.Links.OfType<tbeUserRole>()
                where role.Start < db.Now
                where role.End > db.Now
                where role.Value.StringValue != "system"

                from caption in user.Values.OfType<tbUserCaption>()
                where caption.Start < db.Now
                where caption.End > db.Now
                where flag || caption.StringValue.Contains(contains)

                select new GuidValue
                {
                    Guid = user.ID,
                    Value = caption.StringValue
                };

            return await query
                .ToDataSourceResultAsync(request)
                .ToJsonResultAsync();
        }

        public IActionResult FormCreateUser()
        {
            return View("FormEditUser", new ModelUser
            {
                ID = Guid.NewGuid(),
                Caption = string.Empty,
                Login = string.Empty,
                Password = string.Empty,
                TimeZone = 5,
                Roles = new string[0]
            });
        }

        public async Task<IActionResult> FormEditUser(Guid? id)
        {
            id.throwIfNULL("�� ������ ����");

            var query =
                from user in db.Keys.OfType<tbUser>()
                where user.ID == id
                select new ModelUser
                {
                    ID = user.ID,
                    Caption = user.Values.OfType<tbUserCaption>()
                        .Where(v=>v.Start < db.Now && v.End > db.Now)
                        .Select(v=>v.StringValue)
                        .FirstOrDefault(),
                    Login = user.Values.OfType<tbUserLogin>()
                        .Where(v => v.Start < db.Now && v.End > db.Now)
                        .Select(v => v.StringValue)
                        .FirstOrDefault(),
                    Password = user.Values.OfType<tbUserPassword>()
                        .Where(v => v.Start < db.Now && v.End > db.Now)
                        .Select(v => v.StringValue)
                        .FirstOrDefault(),
                    TimeZone = user.Values.OfType<tbUserTimeZone>()
                        .Where(v => v.Start < db.Now && v.End > db.Now)
                        .Select(v => v.IntValue)
                        .FirstOrDefault(),
                    Roles = user.Links.OfType<tbeUserRole>()
                        .Where(v => v.Start < db.Now && v.End > db.Now)
                        .Select(v=>v.Value.StringValue)
                };

            var model = await query.FirstAsync();

            return View(model);
        }

        public async Task<IActionResult> SaveUser(
            ModelUser model)
        {
            using (var repo = new UserRepository(db))
            {
                var user = await db.Keys.OfType<tbUser>()
                    .Where(v => v.ID == model.ID)
                    .AnyAsync();

                if (!user) await repo.CreateUser(AuthorID);

                if (string.IsNullOrWhiteSpace(model.Caption))
                    return Content("�������� �����������");
                if (string.IsNullOrWhiteSpace(model.Login))
                    return Content("����� �����������");
                if (string.IsNullOrWhiteSpace(model.Password))
                    return Content("������ ����������");


                await repo.SetCaption(AuthorID, model.ID, model.Caption);
                await repo.SetLogin(AuthorID, model.ID, model.Login);
                await repo.SetPassword(AuthorID, model.ID, model.Password);
                await repo.SetTimeZone(AuthorID, model.ID, model.TimeZone);
                await repo.SetRoles(AuthorID, model.ID, model.Roles.ToArray());

                return "������� ���������.".ToHtmlResult();
            }
        }

        public async Task<IActionResult> RemoveUser(
            ModelUser model)
        {
            var query_values =
                from user in db.Keys.OfType<tbUser>()
                where user.ID == model.ID
                select user;

            var read = await query_values.FirstOrDefaultAsync();

            db.Keys.Remove(read);
            await db.CommitAsync(AuthorID);

            return "������� �������".ToHtmlResult();
        }
    }
}