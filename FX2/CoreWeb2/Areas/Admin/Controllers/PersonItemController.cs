#region

using System.Linq;
using System.Threading.Tasks;
using LibCoreDB;
using CoreSupport.Reflection;
using CoreWeb;
using CoreWeb2.Areas.Admin.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

#endregion

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PersonItemController : DBController<DataDb>
    {
        public PersonItemController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        //public async Task<IActionResult> Save(PersonModel model)
        //{
        //    var query_person =
        //        from person in db.Persons
        //        where person.ID == model.ID
        //        select person;

        //    var db_person = await query_person.FirstOrDefaultAsync();

        //    if (db_person == null)
        //    {
        //        db_person = db.Add(new Person()).Entity;
        //        await db.CommitAsync(AuthorID);
        //    }

        //    var query_user =
        //        from user in db.Users
        //        where user.ID == model.UserID
        //        select user;

        //    var db_user = await query_user.FirstOrDefaultAsync();

        //    db_person.FirstName = model.FirstName;
        //    db_person.MiddleName = model.MiddleName;
        //    db_person.SurName = model.LastName;
        //    db_person.TimeZone = model.TimeZone;
        //    db_person.User = db_user;
        //    db_user.if_NOT_NULL(user =>
        //    {
        //        user.TimeZone = model.TimeZone;
        //    });

        //    await db.CommitAsync(AuthorID);

        //    return Redirect(model.RedirectPath);
        //}
    }
}