using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibCoreDB;
using CoreWeb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize(Roles = "admin, developer")]
    public class DepartamentItemController : DBController<DataDb>
    {
        //public async Task<IActionResult> Index(Guid? id)
        //{
        //    db.DataModel.Guid = id ?? Guid.NewGuid();
        //    return View(db);
        //}

        //public async Task<IActionResult> Save(SaveDepartamentModel model)
        //{
        //    var dep = await db.Departaments.FindAsync(model.ID);

        //    if (dep == null)
        //    {
        //        dep = new Departament
        //        {
        //            ID = model.ID,
        //        };

        //        db.Add(dep);
        //    }

        //    dep.ParentID = model.ParentID;
        //    dep.Caption = model.Caption;
        //    dep.Description = model.Description;
        //    dep.OldID = model.OldID;
        //    dep.Order = model.Order;

        //    await db.CommitAsync(AuthorID);

        //    return RedirectToAction("Index", "DepartamentItem", new
        //    {
        //        area = "Admin",
        //        id = dep.ID
        //    });
        //}

        public async Task<IActionResult> Delete(Guid? id)
        {
            //await db.RemoveDepartament(id);
            //await db.CommitAsync(AuthorID);

            return RedirectToAction("Index", "DepartamentList", new {area = "Admin"});
        }

        public DepartamentItemController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }
    }

    public class SaveDepartamentModel
    {
        public Guid ID { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public Guid? ParentID { get; set; }
        public int OldID { get; set; }
        public int Order { get; set; }
    }
}