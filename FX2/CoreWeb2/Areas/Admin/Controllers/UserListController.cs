#region

using System.Linq;
using System.Threading.Tasks;
using LibCoreDB;
using CoreSupport.Reflection;
using CoreWeb;
using CoreWeb2.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

#endregion

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("admin")]
    public class UserListController : DBController<DataDb>
    {
        public UserListController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public async Task<IActionResult> Index() => await Task.Run(() => View(db));

        //[AcceptVerbs("Post")]
        //public async Task<IActionResult> Read(
        //    [DataSourceRequest] DataSourceRequest request,
        //    string login)
        //{
        //    var flag = login.isEmpty();
        //    var super_guid = 1.ToGuid();
        //    var admin_guid = 2.ToGuid();
        //    var query =
        //        from user in db.Users
        //        where AuthorID == dbr.system_id || user.ID != dbr.system_id
        //        where AuthorID == dbr.system_id || user.ID != super_guid
        //        where
        //        AuthorID == dbr.system_id || AuthorID == super_guid || AuthorID == admin_guid || user.ID != admin_guid
        //        where flag || user.Login.Contains(login)
        //        select new
        //        {
        //            user.ID,
        //            user.Login,
        //            Roles = from role in user.RoleLinks
        //            select role.Role.Description
        //        };

        //    var result = await query
        //        .AsEnumerable()
        //        .Select(v => new UserModel
        //        {
        //            ID = v.ID,
        //            Login = v.Login,
        //            Roles = v.Roles.Join(", ")
        //        })
        //        .ToDataSourceResultAsync(request);
        //    return result.ToJsonResult();
        //}

        //[AcceptVerbs("Post")]
        //public async Task<IActionResult> Destroy(
        //    [DataSourceRequest] DataSourceRequest request,
        //    User model)
        //{
        //    var query =
        //        from user in db.Users
        //        where user.ID == model.ID
        //        select user;

        //    var record = await query.FirstOrDefaultAsync();
        //    db.Remove(record);
        //    await db.CommitAsync(AuthorID);

        //    return model.InArray().ToDataSourceResult(request).ToJsonResult();
        //}
    }
}