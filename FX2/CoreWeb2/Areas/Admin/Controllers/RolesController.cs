using System;
using System.Linq;
using System.Threading.Tasks;
using CoreDB.Base;
using CoreDB.Tables;
using CoreSupport.Reflection;
using CoreSupport.Type;
using CoreWeb;
using CoreWeb2.Areas.Admin.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize(Roles = "system,admin")]
    public class RolesController : DBController<DataDb>
    {
        public RolesController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Index_LeftSidebar()
        {
            return View();
        }

        public async Task<IActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            string contains = null)
        {
            var flag = contains.isEmpty();

            var query =
                from role in db.Values.OfType<tbRole>()
                where flag || role.StringValue.Contains(contains)
                where role.StringValue != "system"
                select new GuidValue
                {
                    Guid = role.ID,
                    Value = role.StringValue
                };

            return await query
                .ToDataSourceResultAsync(request)
                .ToJsonResultAsync();
        }

        public async Task<IActionResult> FormEditRole(Guid? id)
        {
            id.throwIfNULL("�� ������ ����");

            var query =
                from role in db.Values.OfType<tbRole>()
                where role.ID == id
                select new ModelRole
                {
                    ID = role.ID,
                    Value = role.Caption,
                    Key = role.StringValue
                };

            var model = await query.FirstAsync();

            return View(model);
        }

        public async Task<IActionResult> SaveRole(
            ModelRole model)
        {
            var role = await db.Values.OfType<tbRole>().FirstOrDefaultAsync(v => v.ID == model.ID);

            if (string.IsNullOrWhiteSpace(model.Key))
                return Content("���� ����������");
            if (string.IsNullOrWhiteSpace(model.Value))
                return Content("��� �����������");

            if (role == null)
            {
                db.Values.Add(new tbRole()
                {
                    ID = model.ID,
                    StringValue = model.Key,
                    Caption = model.Value
                });
            }
            else
            {
                role.StringValue = model.Key;
                role.Caption = model.Value;
            }

            await db.CommitAsync(AuthorID);

            return "������� ���������.".ToHtmlResult();
        }

        public async Task<IActionResult> RemoveRole(
            ModelRole model)
        {
            var role = await db.Values.OfType<tbRole>().FirstOrDefaultAsync(v => v.ID == model.ID);
            db.Values.Remove(role);
            await db.CommitAsync();

            return "������� �������".ToHtmlResult();
        }

        public IActionResult FormCreateRole()
        {
            return View("FormEditRole", new ModelRole
            {
                ID = Guid.NewGuid(),
                Key = string.Empty,
                Value = string.Empty
            });
        }
    }
}