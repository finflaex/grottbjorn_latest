using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibCoreDB;
using CoreSupport.Reflection;
using CoreWeb;
using CoreWeb2.Areas.Admin.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CoreWeb2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Authorize(Roles = "admin, developer")]
    public class DepartamentListController : DBController<DataDb>
    {
        public async Task<IActionResult> Index()
        {
            return View(db);
        }

        public DepartamentListController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db,
            httpContextAccessor)
        {
        }

        //public async Task<IActionResult> Read(
        //    [DataSourceRequest] DataSourceRequest request)
        //{
        //    var query =
        //        from dep in db.Departaments
        //        select dep;

        //    var read = await query.ToArrayAsync();

        //    return read
        //        .Select(v => new DepartamentModel
        //        {
        //            caption = v.Caption,
        //            description = v.Description,
        //            id = v.ID.ToString("N"),
        //            oldid = v.OldID,
        //            order = v.Order,
        //            parentId = v.ParentID?.ToString("N") ?? string.Empty
        //        })
        //        .ToTreeDataSourceResult(request,
        //            v => v.id,
        //            v => v.parentId,
        //            v => v)
        //        .ToJsonResult();
        //}

        //public async Task<IActionResult> Update(
        //    [DataSourceRequest] DataSourceRequest request,
        //    DepartamentModel model)
        //{
        //    var id = model.id.ToGuid();
        //    var query =
        //        from dep in db.Departaments
        //        where dep.ID == id
        //        select dep;

        //    var read = await query.SingleOrDefaultAsync();

        //    read.Caption = model.caption;
        //    read.Description = model.description;
        //    read.OldID = model.oldid;
        //    read.Order = model.order;
        //    read.ParentID = model.parentId.ToNullableGuid();

        //    await db.CommitAsync(AuthorID);

        //    if (model.parentId.isEmpty())
        //    {
        //        model.parentId = string.Empty;
        //    }

        //    return model
        //        .InArray()
        //        .ToTreeDataSourceResult(request)
        //        .ToJsonResult();
        //}


        //public async Task<IActionResult> Destroy(
        //    [DataSourceRequest] DataSourceRequest request,
        //    DepartamentModel model)
        //{
        //    var id = model.id.ToGuid();

        //    await db.RemoveDepartament(id);
        //    await db.CommitAsync(AuthorID);

        //    return model
        //        .InArray()
        //        .ToTreeDataSourceResult(request)
        //        .ToJsonResult();
        //}
    }
}