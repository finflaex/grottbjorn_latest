using CoreDB.Base;
using CoreWeb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreWeb2.Areas.Orgs.Controllers
{
    [Area("orgs")]
    public class TableController : DBController<DataDb>
    {
        public TableController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public IActionResult Index(string id = null)
        {
            switch (id)
            {
                case null: return View();
                default: return View();
            }
        }
    }
}