using System;
using System.Threading.Tasks;
using CoreDB.Base;
using CoreWeb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreWeb2.Areas.Orgs.Controllers
{
    [Area("orgs")]
    [Authorize(Roles = "admin, developer, operator, consultant")]
    public class CardController : DBController<DataDb>
    {
        public CardController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public async Task<IActionResult> Index(
            Guid? id)
        {
            //db.DataModel.Guid = id ?? Guid.Empty;
            return View();
        }

        public async Task<IActionResult> Entities(Guid? id)
        {
            //db.DataModel.Guid = id ?? Guid.Empty;
            return View();
        }
    }
}