﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWeb2.Areas.Orgs.Models
{
    public class OrgModel
    {
        public Guid ID { get; set; }

        [Display(Name = "Наименование")]
        public string Caption { get; set; }

        [Display(Name = "Область")]
        public string Region { get; set; }

        [Display(Name = "Комментарий")]
        public string Description { get; set; }
    }
}
