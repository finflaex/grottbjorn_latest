#region

using System.Threading.Tasks;
using CoreDB.Base;
using CoreDB.Repository;
using CoreSupport.Reflection;
using CoreWeb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

#endregion

namespace CoreWeb2.Areas.Authorization.Controllers
{
    [Area("Authorization")]
    [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
    public class LoginController : DBController<DataDb>
    {
        public LoginController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }


        public IActionResult Form(
            string ReturnUrl)
        {
            return View(ReturnUrl.AsType<object>());
        }

        public async Task<IActionResult> Login(
            string url,
            string login,
            string password)
        {
            if (url.isEmpty())
                url = "/";

            using (var rep = DataRepository.Create<UserRepository>(db))
            {
                var read = await rep.GetAuthor(login, password);

                if (read.IsGuest)
                    return RedirectToAction("Form", "Login", new
                    {
                        area = "Authorization",
                        ReturnUrl = url
                    });

                await HttpContext.SignInAsync(read);
                return Redirect(url);
            }
        }
    }
}