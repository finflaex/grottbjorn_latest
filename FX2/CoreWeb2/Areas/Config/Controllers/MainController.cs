using System;
using System.Threading.Tasks;
using CoreDB.Base;
using CoreDB.Repository;
using CoreSupport.Reflection;
using CoreSupport.Type;
using CoreWeb;
using CoreWeb2.Websockets;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreWeb2.Areas.Config.Controllers
{
    [Area("config")]
    [Authorize]
    public class MainController : DBController<DataDb>
    {
        public MainController(DataDb db, IHttpContextAccessor httpContextAccessor) : base(db, httpContextAccessor)
        {
        }

        public static async Task Start(SystemMiddleware sys, DictSO data)
        {
            using (var repo = new UserRepository(sys.db))
            {
                var leftUrl = await repo.GetConfigString(sys.Author.Guid, "menu");
                await LoadLeftPanel(sys, leftUrl);

                var leftSidebarUrl = leftUrl;
                if (leftUrl.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Length < 3)
                    leftSidebarUrl += "/index";
                leftSidebarUrl += "_leftSidebar";

                var leftSidebar = await repo.GetConfigBool(sys.Author.Guid, "sidebar_left");
                if (!leftSidebar)
                    await ShowLeftSidebar(sys, leftSidebarUrl);
                else
                    await LoadLeftSidebar(sys, leftSidebarUrl);
            }
        }

        public static async Task SaveSidebar(SystemMiddleware sys, DictSO data)
        {
            var key = data.GetString("key");
            var flag = data.GetBool("flag");
            using (var repo = new UserRepository(sys.db))
            {
                await repo.SetConfig(sys.Author.Guid, sys.Author.Guid, key, flag);
            }
        }

        public static async Task ClickMenu(SystemMiddleware sys, DictSO data)
        {
            var url = data.GetString("url")?.ToLower();
            if (url.isEmpty()) return;
            switch (url)
            {
                case "/admin/roles":
                case "/admin/users":
                case "/admin/dbhistory":
                    await LoadLeftPanel(sys, url);
                    await LoadLeftSidebar(sys, $"{url}/index_leftsidebar");
                    //await ShowLeftSidebar(url);
                    //await ShowRightSidebar(url);
                    //await LoadRightPanel(url);
                    break;
                default:
                    throw new Exception();
            }
            using (var repo = new UserRepository(sys.db))
            {
                await repo.SetConfig(sys.Author.Guid, sys.Author.Guid, "menu", url);
            }
        }

        private static async Task LoadLeftPanel(SystemMiddleware sys, string url)
        {
            if (url.isEmpty()) return;
            await sys.Send(new
            {
                target = "#panel_left",
                action = "load",
                url = url
            });
        }

        private static async Task LoadRightPanel(SystemMiddleware sys, object url)
        {
            if (url.ToString().isEmpty()) return;
            await sys.Send(new
            {
                target = "#panel_right",
                action = "load",
                url = url
            });
        }

        private static async Task LoadLeftSidebar(SystemMiddleware sys, string url)
        {
            if (url.isEmpty()) return;
            await sys.Send(new
            {
                target = ".sidebar-secondary",
                action = "load",
                url = url
            });
        }

        private static async Task LoadRightSidebar(SystemMiddleware sys, string url)
        {
            if (url.isEmpty()) return;
            await sys.Send(new
            {
                target = ".sidebar-opposite",
                action = "load",
                url = url
            });
        }

        private static async Task ShowLeftSidebar(SystemMiddleware sys, string url)
        {
            if (url.isEmpty()) return;
            await sys.Send(new { action = "show_left_sidebar", url });
        }

        private static async Task HideLeftSidebar(SystemMiddleware sys)
            => await sys.Send(new { action = "hide_left_sidebar" });

        private static async Task ShowRightSidebar(SystemMiddleware sys, string url)
        {
            if (url.isEmpty()) return;
            await sys.Send(new { action = "show_right_sidebar", url });
        }

        private static async Task HideRightSidebar(SystemMiddleware sys)
            => await sys.Send(new { action = "hide_right_sidebar" });
    }
}