#region

using System.Threading.Tasks;
using CoreDB.Base;
using CoreSupport.Reflection;
using CoreWeb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

#endregion

namespace CoreWeb2.Areas.Import.Controllers
{
    [Area("Import")]
    [Authorize(Roles = "admin,operator,consultant")]
    public class OperatorDataFileController : DBController<DataDb>
    {
        private const string system_key = "operator_df";

        public OperatorDataFileController(DataDb db, IHttpContextAccessor httpContextAccessor)
            : base(db, httpContextAccessor)
        {
        }

        public async Task<IActionResult> Index()
        {
            return await Task.Run(() => View(db));
        }
        //    var user = await db.Users.FindAsync(AuthorID);
        //{

        //public async Task<IActionResult> LoadFile(IEnumerable<IFormFile> files, Guid? region = null)
        //    var upload = await db.TypeAccountables.FirstOrDefaultAsync(v => v.Key == TypeAccountable.Keys.upload_file);
        //    var responsible = await db.TypeAccountables.FirstOrDefaultAsync(v => v.Key == TypeAccountable.Keys.responsible);

        //    foreach (var file in files)
        //    {
        //        await Success($"�������� ���� {file.FileName}");

        //        using (var doc = new ExcelPackage(file.OpenReadStream()))
        //        {
        //            if (!doc.Workbook.Worksheets.Any())
        //            {
        //                await Danger("���� �� ��������� ��� ����� �������� ������");
        //            }

        //            foreach (var sheet in doc.Workbook.Worksheets)
        //            {
        //                await Success($"������ ���� '{sheet.Name}'");
        //                await Info($"���������� �����: {sheet.Dimension.Rows}");
        //                if (sheet.Dimension.End.Row > 1)
        //                {
        //                    var call_comment = -1;
        //                    var call_date = -1;
        //                    var contact_fio = -1;
        //                    var contact_fio_comment = -1;
        //                    var contact_mail = -1;
        //                    var contact_mail_comment = -1;
        //                    var contact_phone = -1;
        //                    var contact_phone_comment = -1;
        //                    var contact_post = -1;
        //                    var contact_post_comment = -1;
        //                    var contact_url = -1;
        //                    var contact_url_comment = -1;
        //                    var id = -1;
        //                    var meeting_acc = -1;
        //                    var meeting_comment = -1;
        //                    var meeting_date = -1;
        //                    var org_comment = -1;
        //                    var org_inn = -1;
        //                    var org_name = -1;
        //                    var org_name_short = -1;
        //                    var org_address = -1;

        //                    await Info($"������ ��������� ...");

        //                    sheet.Dimension.End.Column.For(1, x =>
        //                    {
        //                        var value = sheet.Cells[1, x].Value?.ToString() ?? string.Empty;
        //                        value = value.Trim().ToLower();
        //                        switch (value)
        //                        {
        //                            case "id":
        //                                id = x;
        //                                break;
        //                            case "org_inn":
        //                                org_inn = x;
        //                                break;
        //                            case "org_name":
        //                                org_name = x;
        //                                break;
        //                            case "org_name_short":
        //                                org_name_short = x;
        //                                break;
        //                            case "org_comment":
        //                                org_comment = x;
        //                                break;
        //                            case "contact_mail":
        //                                contact_mail = x;
        //                                break;
        //                            case "contact_phone":
        //                                contact_phone = x;
        //                                break;
        //                            case "contact_url":
        //                                contact_url = x;
        //                                break;
        //                            case "contact_phone_comment":
        //                                contact_phone_comment = x;
        //                                break;
        //                            case "contact_mail_comment":
        //                                contact_mail_comment = x;
        //                                break;
        //                            case "contact_url_comment":
        //                                contact_url_comment = x;
        //                                break;
        //                            case "contact_post":
        //                                contact_post = x;
        //                                break;
        //                            case "contact_post_comment":
        //                                contact_post_comment = x;
        //                                break;
        //                            case "contact_fio":
        //                                contact_fio = x;
        //                                break;
        //                            case "contact_fio_comment":
        //                                contact_fio_comment = x;
        //                                break;
        //                            case "meeting_date":
        //                                meeting_date = x;
        //                                break;
        //                            case "meeting_comment":
        //                                meeting_comment = x;
        //                                break;
        //                            case "meeting_acc":
        //                                meeting_acc = x;
        //                                break;
        //                            case "call_date":
        //                                call_date = x;
        //                                break;
        //                            case "call_comment":
        //                                call_comment = x;
        //                                break;
        //                            case "org_address":
        //                                org_address = x;
        //                                break;
        //                        }
        //                    });

        //                    Organization org = null;

        //                    for (int y = 2; y <= sheet.Dimension.End.Row; y++)
        //                    {
        //                        var value_id = sheet.ToString(id, y);
        //                        if (value_id.StartsWith("?")) continue;
        //                        var value_org_comment = sheet.ToString(org_comment, y);
        //                        var value_org_inn = sheet.ToString(org_inn, y);
        //                        var value_org_name = sheet.ToString(org_name, y);
        //                        var value_org_name_short = sheet.ToString(org_name_short, y);
        //                        var value_org_address = sheet.ToString(org_address, y);
        //                        var value_call_comment = sheet.ToString(call_comment, y);
        //                        var value_call_date = sheet.ToString(call_date, y);
        //                        var value_contact_fio = sheet.ToString(contact_fio, y);
        //                        var value_contact_fio_comment = sheet.ToString(contact_fio_comment, y);
        //                        var value_contact_mail = sheet.ToString(contact_mail, y);
        //                        var value_contact_mail_comment = sheet.ToString(contact_mail_comment, y);
        //                        var value_contact_phone = sheet.ToString(contact_phone, y);
        //                        var value_contact_phone_comment = sheet.ToString(contact_phone_comment, y);
        //                        var value_contact_post = sheet.ToString(contact_post, y);
        //                        var value_contact_post_comment = sheet.ToString(contact_post_comment, y);
        //                        var value_contact_url = sheet.ToString(contact_url, y);
        //                        var value_contact_url_comment = sheet.ToString(contact_url_comment, y);
        //                        var value_meeting_acc = sheet.ToString(meeting_acc, y);
        //                        var value_meeting_comment = sheet.ToString(meeting_comment, y);
        //                        var value_meeting_date = sheet.ToString(meeting_date, y);


        //                        if (org != null && value_id.isEmpty())
        //                        {
        //                            try
        //                            {
        //                                db.Add(org);
        //                                await db.CommitAsync(AuthorID);
        //                                org = null;

        //                                await Success("��������� �����������");
        //                            }
        //                            catch (Exception e)
        //                            {
        //                                await Danger(e.ToFinflaexString());
        //                            }
        //                        }

        //                        await Info($"������ �{y}");

        //                        if (org == null && value_id.isEmpty() && (
        //                                value_org_name.notEmpty() ||
        //                                value_org_name_short.notEmpty() ||
        //                                value_contact_fio.notEmpty() ||
        //                                value_contact_mail.notEmpty() ||
        //                                value_contact_phone.notEmpty() ||
        //                                value_contact_post.notEmpty() ||
        //                                value_contact_url.notEmpty()))
        //                        {
        //                            org = new Organization
        //                            {
        //                                Description = $"{value_org_address}\r\n{value_org_comment}".Trim('\r','\n'),
        //                                Caption = value_org_name_short ?? value_org_name,
        //                            };

        //                            org.Accauntables.Add(new Event_Org_Accauntable(user, upload));
        //                            org.Accauntables.Add(new Event_Org_Accauntable(user, responsible));

        //                            if (value_org_inn.notEmpty())
        //                            {
        //                                var entity = new Entity
        //                                {
        //                                    INN = value_org_inn,
        //                                    NameShort = value_org_name_short,
        //                                    NameFull = value_org_name,
        //                                };

        //                                org.Entities.Add(new Event_Org_Entity(entity)
        //                                {
        //                                    //Start = DateTimeOffset.Now.ConvertTimeZone(Author.TimeZone)
        //                                });
        //                            }

        //                            await Success("������� �����������");
        //                        }

        //                        if (org != null && (value_id.StartsWith("*") || value_id.isEmpty()))
        //                        {
        //                            if (value_contact_fio.notEmpty()
        //                                || value_contact_mail.notEmpty()
        //                                || value_contact_phone.notEmpty()
        //                                || value_contact_post.notEmpty()
        //                                || value_contact_url.notEmpty())
        //                            {
        //                                var types = await db.TypeLeadContacts.ToArrayAsync();

        //                                var group = new LeadGroup();

        //                                group.Leads.AddPersonNotEmpty(value_contact_fio, (e, r) =>
        //                                {
        //                                    r.Comments.AddCommentNotEmpty(value_contact_fio_comment);
        //                                });
        //                                group.Leads.AddPostNotEmpty(value_contact_post, (e, r) =>
        //                                {
        //                                    r.Comments.AddCommentNotEmpty(value_contact_post_comment);
        //                                });
        //                                group.Leads.AddContactNotEmpty(value_contact_mail, (e, r) =>
        //                                {
        //                                    e.Type = types.FirstOrDefault(v => v.Key == TypeLeadContact.Keys.email);
        //                                    r.Comments.AddCommentNotEmpty(value_contact_mail_comment);
        //                                });
        //                                group.Leads.AddContactNotEmpty(value_contact_phone, (e, r) =>
        //                                {
        //                                    e.Type = types.FirstOrDefault(v => v.Key == TypeLeadContact.Keys.phone);
        //                                    r.Comments.AddCommentNotEmpty(value_contact_phone_comment);
        //                                });
        //                                group.Leads.AddContactNotEmpty(value_contact_url, (e, r) =>
        //                                {
        //                                    e.Type = types.FirstOrDefault(v => v.Key == TypeLeadContact.Keys.url);
        //                                    r.Comments.AddCommentNotEmpty(value_contact_url_comment);
        //                                });

        //                                org.LeadGroups.AddGroup(group);
        //                            }
        //                        }
        //                    }

        //                    if (org != null)
        //                    {
        //                        try
        //                        {
        //                            db.Add(org);
        //                            await db.CommitAsync(AuthorID);

        //                            await Success("��������� �����������");
        //                        }
        //                        catch (Exception e)
        //                        {
        //                            await Danger(e.ToFinflaexString());
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    await Danger($"{sheet.Name} �����");
        //                }
        //                await Success($"��������� ����� '{sheet.Name}' ���������");
        //            }
        //        }
        //        await Success($"��������� ����� '{file.FileName}' ���������");
        //    }

        //    return Content("");
        //}

        //public async Task Log(string message)
        //{
        //    try
        //    {
        //        await db.AddAsync(new SystemMessages
        //        {
        //            Key = system_key,
        //            Value = message,
        //            UserID = AuthorID
        //        });
        //        await db.CommitAsync(AuthorID);
        //    }
        //    catch (Exception)
        //    {

        //    }
        //}

        //public async Task Info(string message) => await message
        //    .Prepend($"{DateTime.Now:HH:mm:ss} > ".WrapTag("span"))
        //    .WrapTag("div", new {@class = "text-info"})
        //    .ret(Log);

        //public async Task Danger(string message) => await message
        //    .Prepend($"{DateTime.Now:HH:mm:ss} > ".WrapTag("span"))
        //    .WrapTag("div", new {@class = "text-danger"})
        //    .ret(Log);

        //public async Task Success(string message) => await message
        //    .Prepend($"{DateTime.Now:HH:mm:ss} > ".WrapTag("span"))
        //    .WrapTag("div", new {@class = "text-success"})
        //    .ret(Log);
    }

    public static class helpExcel
    {
        public static string ToString(this ExcelWorksheet @this, int x, int y)
        {
            if (x > 0 && x <= @this.Cells.End.Column && y > 0 && y <= @this.Cells.End.Row)
                return @this.Cells[y, x].Value?.ToString().FromHtml().Trim(' ', '\r', '\n', '\t') ?? string.Empty;
            return string.Empty;
        }
    }
}