﻿#region

using System;
using System.Linq;
using System.Threading.Tasks;
using CoreDB;
using CoreDB.Base;
using CoreSupport.MVC.Authorization;
using CoreSupport.Reflection;
using CoreSupport.Type;
using CoreWeb2.Services;
using CoreWeb2.Websockets;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;

#endregion

namespace CoreWeb2
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc()
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            // Add Kendo UI services to the services container
            services.AddKendo();

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services
                .AddEntityFramework()
                .AddEntityFrameworkSqlServer()
                .AddDbContext<DataDb>(v => v.UseSqlServer(Configuration["Data:ConnectionString"]))
                ;
            services.AddScoped<ServiceDB<DataDb>>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();

            //var defaultFilesOptions = new DefaultFilesOptions();
            //defaultFilesOptions.DefaultFileNames.Clear();
            //defaultFilesOptions.DefaultFileNames.Add("index.html");
            //app.UseDefaultFiles(defaultFilesOptions);
            app.UseStaticFiles();

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler("/Home/Error");

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = "Cookies",
                LoginPath = new PathString("/Authorization/Login/Form"),
                AccessDeniedPath = new PathString("/Home/AccessDenied"),
                //LoginPath = new PathString("/Account/Login"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                ExpireTimeSpan = new TimeSpan(8, 0, 0)
            });

            app.UseWebSockets();
            //app.UseMiddleware<ChatWebSocketMiddleware>();
            //app.UseFxMiddleware<test, DataDB>("/db");
            //app.UseFxMiddleware<SystemMessagesMiddleware, DataDB>("/sysmsg");
            app.UseFxMiddleware<SystemMiddleware, DataDb>("/system");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "areas",
                    "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");
            });

            DataDb.Initialize(app.ApplicationServices).Wait();
        }
    }

    public class test : FxMiddleware<test, DataDb>
    {
        public override async Task Worker(HttpContext context)
        {
            var key = context.Request
                .QueryString.ToString().TrimStart('?');
            switch (key)
            {
                case "author":
                    await context.Response.WriteAsync(context.AuthRecord().ToJson());
                    return;
                //case "roles":
                //    var roles = await db.Roles
                //        .Where(v => v.ID != dbr.architector)
                //        .Where(v => v.ID != dbr.guest)
                //        .Select(role => new KeyValue
                //        {
                //            Key = role.ID,
                //            Value = role.Description
                //        })
                //        .ToArrayAsync();
                //    context.Response.ContentType = "application/json";
                //    await context.Response.WriteAsync(roles.ToJson());
                //    return;
            }

            //await context.Response.WriteAsync("qwe");
            //return;
            await Next.Invoke(context);
        }
    }
}