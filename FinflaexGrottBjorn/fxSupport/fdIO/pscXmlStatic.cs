﻿using System.IO;
using System.Xml.Serialization;

namespace fxSupport.fdIO
{
    public static class pscXmlStatic
    {
        public static void Write<T>(string path, T data)
        {
            using (TextWriter writer = new StreamWriter(path))
            {
                var ser = new XmlSerializer(typeof (T));
                ser.Serialize(writer, data);
            }
        }
    }
}