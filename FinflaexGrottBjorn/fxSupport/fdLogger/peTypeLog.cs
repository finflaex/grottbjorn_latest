﻿namespace fxSupport.fdLogger
{
    public enum peTypeLog
    {
        Test = 0,
        Trace = 1,
        Debug = 2,
        Info = 3,
        Warning = 4,
        Error = 5,
        Fatal = 6
    }
}