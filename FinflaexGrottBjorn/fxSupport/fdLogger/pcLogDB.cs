﻿using System.Data.Entity;
using fxSupport.fdMsSQL.fsdBase;

namespace fxSupport.fdLogger
{
    public class pcLogDB : pcaDB<pcLogDB>
    {
        public pcLogDB(string path = null, bool server = false)
            : base("log", server, path)
        {
        }

        public pcLogDB() : this(null, false)
        {
        }

        public DbSet<pcLog> Logs { get; set; }
    }
}