﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using fxSupport.fdMsSQL.fsdTable;

namespace fxSupport.fdLogger
{
    public class pcLog
    {
        public pcLog()
        {
            Id = Guid.NewGuid();
            Date = DateTime.Now;
        }

        [Key]
        public Guid Id { get; set; }
        public peTypeLog Type { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public string Method { get; set; }
    }
}