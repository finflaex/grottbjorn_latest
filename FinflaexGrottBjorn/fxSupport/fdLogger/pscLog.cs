﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using fxSupport.fdIO;
using fxSupport.fdReflection;

namespace fxSupport.fdLogger
{
    public static class pscLog
    {
        public static bool LogBase = true;
        public static bool LogFile = true;
        public static bool LogConsole = true;
        private static peTypeLog[] print;

        static pscLog()
        {
            print = new peTypeLog[0];
            //if (LogBase)
            //{
            //    try
            //    {
            //        using (var db = new pcLogDB(PathFile))
            //        {
            //            var read = db.Logs.Any();
            //        }
            //    }
            //    catch { }
            //}
        }

        

        public static string PathFile { get; set; }

        private static string _path_file
            => Path.Combine(PathFile ?? pscDirectory.Current, "logs", $"{DateTime.Now.ToString("yyyy_MM_dd")}.log");

        public static void Debug(string debug)
        {
            Save(new pcLog
            {
                Type = peTypeLog.Debug,
                Message = debug
            });
        }

        public static void Error(Exception exception, string error)
        {
            Save(new pcLog
            {
                Type = peTypeLog.Error,
                Message = $"{error} \u21A9\r\n{exception.InnerException?.Message ?? exception.Message}"
            });
        }

        public static void Log(params object[] param)
        {
            var type = param.OfType<peTypeLog>().FirstOrDefault();
            var exception = param.OfType<Exception>().FirstOrDefault();
            var message = param.OfType<string>().ToList().Join("\r\n") ?? string.Empty;
            var method = param.OfType<MethodBase>().FirstOrDefault()?.Name;

            Save(new pcLog()
            {
                Type = type,
                Message = exception == null
                    ? message
                    : $"{message}\r\n{exception.GetType().Name} => {exception.Message}",
                Method = method,
            });
        }

        private static void Save(pcLog log)
        {
            var text = new StringBuilder();
            text.Append(log.Date.ToString("HH:mm:ss.ffff"));
            text.Append(" \u21d2 ");
            text.AppendLine(log.Type.ToString());
            text.AppendLine(log.Message.Split('\n').Select(v => $"\t\t{v}").Join());
            if (LogBase)
            {
                using (var db = new pcLogDB(PathFile))
                {
                    db.Logs.Add(log);
                    db.SaveChanges();
                }
            }
            if (LogFile && isPrint(log))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_path_file));
                using (var file = File.Open(_path_file, FileMode.OpenOrCreate, FileAccess.Write))
                using (var writer = new StreamWriter(file))
                {
                    file.Seek(0, SeekOrigin.End);
                    writer.WriteAsync(text.ToString());
                }
            }
            if (LogConsole && print.Contains(log.Type))
            {
                Console.WriteLine(text.ToString());
            }
        }

        private static bool isPrint(pcLog log)
        {
            return print.Length <= 0 || print.Contains(log.Type);
        }

        public static void Print(params peTypeLog[] param)
        {
            print = param;
        }
    }
}