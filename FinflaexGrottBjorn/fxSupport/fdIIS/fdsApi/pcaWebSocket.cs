﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Web.WebSockets;

namespace fxSupport.fdIIS.fdsApi
{
    public class pcaWebSocket<T> : ApiController
        where T : WebSocketHandler
    {
        public HttpResponseMessage Get()
        {
            HttpContext.Current.AcceptWebSocketRequest((T)Activator.CreateInstance(typeof(T)));
            return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
        }
    }
}