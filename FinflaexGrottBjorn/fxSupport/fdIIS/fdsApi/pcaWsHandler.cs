﻿using System.Web;
using Microsoft.Web.WebSockets;

namespace fxSupport.fdIIS.fdsApi
{
    public abstract class pcaWsHandler : WebSocketHandler
    {
        public static WebSocketCollection Clients = new WebSocketCollection();

        public override void OnOpen()
        {
            base.OnOpen();
            Clients.Add(this);
        }

        public override void OnMessage(string message)
        {
            base.OnMessage(message);
        }
    }
}