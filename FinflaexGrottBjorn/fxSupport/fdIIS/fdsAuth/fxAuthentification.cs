﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using fxSupport.fdMsSQL.fsdBase;
using fxSupport.fdMsSQL.fsdTable;
using fxSupport.fdReflection;
using static System.String;

namespace fxSupport.fdIIS.fdsAuth
{
    public class fxAuthentification<DB>
        where DB : pcaDB_Identity<DB>
    {
        private event Func<DB> createDB;

        private DB create_db => createDB == null
            ? (DB) typeof (DB).GetMethod("Create").Invoke(null, null)
            : createDB.Invoke();

        private const string cookieName = "__AUTH_COOKIE";

        public fxAuthentification(HttpContextBase context, Func<DB> act)
        {
            createDB = act;
            this.context = context;
            try
            {
                HttpCookie authCookie = context.Request.Cookies.Get(cookieName);
                if (!IsNullOrEmpty(authCookie?.Value))
                {
                    var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    using (var db = create_db)
                    {
                        user = new UserIndentity(db.Users.FirstOrDefault(v => v.UserName == ticket.Name));
                    }
                }
                else
                {
                    user = new UserIndentity(null);
                }
            }
            catch (Exception ex)
            {
                user = new UserIndentity(null);
            }

            context.Session?.Add("fxAuth", this);
        }

        public UserIndentity user { get; set; }

        public HttpContextBase context { get; private set; }
        public bool IsAuthenticated => user?.IsAuthenticated ?? false;

        public bool IsInRole(string role)
            => user.User != null
            && user.User.InRoles(role);

        public override string ToString()
        {
            return user.Name;
        }

        public bool InRoles(string roles) => !IsNullOrWhiteSpace(roles) && roles
            .Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
            .Select(role => user.User.InRoles(role))
            .Any(hasRole => hasRole);

        public User Login(string userName, string Password, bool isPersistent)
        {
            using (var db = create_db)
            {
                var read = db.Users.FirstOrDefault(v => v.UserName == userName);
                if (read != null && read.Password == Password)
                {
                    CreateCookie(userName, isPersistent);
                }
                user = new UserIndentity(read);
                return read;
            }
        }

        public User Login(string userName)
        {
            using (var db = create_db)
            {
                var read = db.Users.FirstOrDefault(v => v.UserName == userName);
                if (read != null)
                {
                    CreateCookie(userName);
                }
                user = new UserIndentity(read);
                return read;
            }
        }

        private void CreateCookie(string userName, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(
                1,
                userName,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                isPersistent,
                Empty,
                FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket.
            var encTicket = FormsAuthentication.Encrypt(ticket);

            // Create the cookie.
            var AuthCookie = new HttpCookie(cookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
            context.Response.Cookies.Set(AuthCookie);
        }

        public void LogOut()
        {
            var httpCookie = context.Response.Cookies[cookieName];
            if (httpCookie != null)
            {
                httpCookie.Value = Empty;
                user = null;
                //context.Response.Cookies.Set(httpCookie);
            }
        }

        public bool Intersect(string[] allowedRoles)
        {
            if (!IsAuthenticated)
            {
                var shablon = new[] {"anonym"};
                return allowedRoles.Intersect(shablon).Any();
            }
            using (var db = create_db)
            {
                var roles = db
                    .Users
                    .FirstOrDefault(v=>v.UserName == user.Name)?
                    .Roles.Select(v=>v.Name)
                    .ToArray() ?? new string[0];
                return allowedRoles.Intersect(roles).Any();
            }
        }
    }

    public class UserIndentity : IIdentity
    {
        public UserIndentity(User user)
        {
            User = user;
        }
        public User User { get; set; }

        public string AuthenticationType => typeof(User).ToString();

        public bool IsAuthenticated => User != null;

        public string Name => User != null ? User.UserName : "anonym";
    }

    public static class frAuth
    {
        public static T GetValue<T>(this HttpSessionStateBase session, string key)
        {
            return session.GetValue<T>(key, default(T));
        }

        public static T GetValue<T>(this HttpSessionStateBase session, string key, T defaultValue)
        {
            if (session[key] != null)
            {
                return (T)Convert.ChangeType(session[key], typeof(T));
            }

            return defaultValue;
        }

        public static fxAuthentification<DB> GetAuthentification<DB>(this HttpContextBase self)
            where DB : pcaDB_Identity<DB>
        {
            return self.Session.GetValue<fxAuthentification<DB>>("fxAuth");
        }


    }
}
