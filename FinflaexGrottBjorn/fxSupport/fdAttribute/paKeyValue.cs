﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using fxSupport.fdReflection;

namespace fxSupport.fdAttribute
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class paKeyValue : Attribute
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }

    public static class pscKeyValue
    {
        public static Dictionary<string, object> getAttrKV<T, P>(this T self, Expression<Func<T, P>> action)
        {
            return self
                .getAttrs(action)
                .OfType<paKeyValue>()
                .ToDictionary(v => v.Name, v => v.Value);
        }

        public static Dictionary<string, object> getAttrKV<T>(this T self, string name)
        {
            return self
                .getAttrs(name)
                .OfType<paKeyValue>()
                .ToDictionary(v => v.Name, v => v.Value);
        }

        public static Dictionary<string, object> getAttrKV(this PropertyInfo self)
        {
            return self
                .getAttrs()
                .OfType<paKeyValue>()
                .ToDictionary(v => v.Name, v => v.Value);
        }
    }
}
