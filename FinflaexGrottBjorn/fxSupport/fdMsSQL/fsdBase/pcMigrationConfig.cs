﻿using System.Data.Entity.Migrations;

namespace fxSupport.fdMsSQL.fsdBase
{
    public class pcMigrationConfig<T> : DbMigrationsConfiguration<T>
        where T : pcaDB<T>
    {
        public pcMigrationConfig()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(T context)
        {
            if (!context.Database.Exists())
            {
                context.Database.Create();
            }
            base.Seed(context);
        }
    }
}