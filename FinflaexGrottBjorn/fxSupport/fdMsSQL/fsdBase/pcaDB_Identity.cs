﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fxSupport.fdMsSQL.fsdTable;

namespace fxSupport.fdMsSQL.fsdBase
{
    public class pcaDB_Identity<T> : pcaDB<T>
        where T : pcaDB_Identity<T>
    {
        public pcaDB_Identity(string connect, IDatabaseInitializer<T> initializer = null)
            :base(connect, initializer)
        {
            
        }

        public pcaDB_Identity(string connect, bool server = false)
            : base(connect, server)
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
                .HasMany(v => v.Roles)
                .WithMany(v => v.Users);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
    }
}
