﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;
using fxSupport.fdIO;
using fxSupport.fdMsSQL.fsdOther;
using fxSupport.fdMsSQL.fsdTable;
using fxSupport.fdReflection;

namespace fxSupport.fdMsSQL.fsdBase
{
    public abstract class pcaDB<T> : DbContext
        where T : pcaDB<T>
    {
        #region MIGRATION

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        #endregion

        #region METHODS

        public static void Run(
            Action<T> act = null,
            Action<Exception> err = null)
        {
            if (act == null) return;
            using (var db = CreateDB())
            {
                try
                {
                    act(db);
                }
                catch (Exception e)
                {
                    err?.Invoke(e);
                }
            }
        }

        #endregion

        #region SYSTEM

        public pcaDB(string connectionString, IDatabaseInitializer<T> initializer = null)
            : base(connectionString)
        {
            try
            {
                var path = Path.Combine(pscDirectory.Current, "basa");
                Directory.CreateDirectory(path);
                AppDomain.CurrentDomain.SetData("DataDirectory", path);
            }
            finally
            {
                Database.SetInitializer(initializer ?? new pcMigration<T>());
            }
        }

        public pcaDB(string name = "finflaex", bool server = false, string path = null)
            : this(ConnectionString(name, server, path))
        {
        }

        public static string ConnectionString(string name, bool server, string path)
        {
            return server
                            ? $@"Data Source=(LocalDB)\MSSQLLocalDB;" +
                              $@"AttachDbFilename={HostingEnvironment.MapPath($@"~\basa\{name}.mdf")};" +
                              $@"Integrated Security=True;" +
                              $@"Database={name};"
                            : $@"Data Source=(LocalDB)\MSSQLLocalDB;" +
                              $@"AttachDbFilename={Path.Combine(path ?? pscDirectory.Current, "basa", $@"{name}.mdf")};" +
                              $@"Integrated Security=True;" +
                              $@"Database={name};";
        }

        private pcaDB(string connectionString) : this(connectionString, null)
        {
        }

        public static T CreateDB(params object[] param)
        {
            try
            {
                return param != null && param.Length == 0
                    ? Activator.CreateInstance<T>()
                    : (T) Activator.CreateInstance(typeof (T), param);
            }
            catch
            {
                return null;
            }
        }

        public static void Using(Action<T> action, params object[] param)
        {
            using (var db = CreateDB(param))
            {
                action.Invoke(db);
            }
        }

        public static R Using<R>(Func<T, R> action, params object[] param)
        {
            using (var db = CreateDB(param))
            {
                return action.Invoke(db);
            }
        }

        public static T Create()
        {
            return CreateDB();
        }

        public static V SaveHistory<V>(V value, Func<T, V, Guid?> action = null)
            where V : pcaHistory<V>
        {
            try
            {
                start:
                using (var db = CreateDB())
                using (var ta = db.Database.BeginTransaction())
                {
                    V result;
                    var table = db.GetTable<V>();
                    var oldGuid = action?.Invoke(db, value);
                    var old = oldGuid != null
                        ? table.FirstOrDefault(v
                            => v.Guid == oldGuid
                            && v.sysStaus == peSysStatus.actual)
                        : null;
                    if (old == null)
                    {
                        value.CompareHistory(Activator.CreateInstance<V>());
                        value.sysStaus = peSysStatus.actual;
                        value.sysStartDT
                            = value.sysActDT
                                = DateTime.Now;
                        result = table.Add(value);
                        //Console.Write($"new => ");
                    }
                    else
                    {
                        var compare = value.CompareHistory(old);
                        if (!compare)
                        {
                            old.CompareHistory(old);
                            old.sysActDT = DateTime.Now;
                            old.sysStaus = peSysStatus.actual;
                            db.Entry(old).State = EntityState.Modified;
                            result = old;
                            //Console.Write($"old => ");
                        }
                        else
                        {
                            old.sysEndDT
                                = value.sysStartDT
                                    = old.sysActDT
                                        = value.sysActDT
                                            = DateTime.Now;

                            old.sysStaus = peSysStatus.history;
                            value.sysStaus = peSysStatus.actual;

                            value.Guid = (Guid)oldGuid;
                            db.Entry(old).State = EntityState.Modified;
                            result = table.Add(value);
                            //Console.Write($"update => ");
                        }
                    }
                    //Console.Write($"{result.sysGuid} - {result.Guid}");
                    try
                    {
                        db.SaveChanges();
                        ta.Commit();
                        //Console.WriteLine($" = save");
                    }
                    catch (DbUpdateConcurrencyException err)
                    {
                        Console.WriteLine(err.InnerException?.Message ?? err.Message);
                        ta.Rollback();
                        //Console.WriteLine($" = error concurent");
                        Thread.Sleep(100);
                        goto start;
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine(err.InnerException?.Message ?? err.Message);
                        ta.Rollback();
                        //Console.WriteLine($" = error other");
                        result = null;
                    }
                    return result;
                }
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
                return null;
            }
        }

        #endregion
    }
}