﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fxSupport.fdMsSQL.fsdBase;
using fxSupport.fdMsSQL.fsdTable;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace fxSupport.fdMsSQL.fsdOther
{
    public class fxRoleStore<DB> : IRoleStore<Role, Guid>
        where DB : pcaDB_Identity<DB>
    {
        private DB create_db => (DB) typeof (DB).GetMethod("Create").Invoke(null, null);

        public Task CreateAsync(Role role)
        {
            return Task.Run(() =>
            {
                using (var db = create_db)
                {
                    db.Roles.Add(role);
                    db.SaveChanges();
                }
            });
        }

        public Task DeleteAsync(Role role)
        {
            return Task.Run(() =>
            {
                using (var db = create_db)
                {
                    var del = db.Roles.Find(role.Id);
                    db.Roles.Remove(del);
                    db.SaveChanges();
                }
            });
        }

        public void Dispose()
        {
        }

        public async Task<Role> FindByIdAsync(Guid roleId)
        {
            return await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    return db.Roles.Find(roleId);
                }
            });
        }

        public async Task<Role> FindByNameAsync(string roleName)
        {
            return await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    return db.Roles.FirstOrDefault(v => v.Name == roleName);
                }
            });
        }

        public async Task UpdateAsync(Role role)
        {
            await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    db.Entry(role).State = EntityState.Modified;
                    db.SaveChanges();
                }
            });
        }
    }
}
