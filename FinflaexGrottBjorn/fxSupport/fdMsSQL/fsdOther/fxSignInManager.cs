﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fxSupport.fdMsSQL.fsdBase;
using fxSupport.fdMsSQL.fsdTable;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace fxSupport.fdMsSQL.fsdOther
{
    public class fxSignInManager<DB> :
        SignInManager<User, Guid>,
        IUserLockoutStore<User, Guid>
        where DB : pcaDB_Identity<DB>
    {
        public fxSignInManager(
            fxUserManager<DB> userManager, 
            IAuthenticationManager authenticationManager) 
            : base(userManager, authenticationManager)
        {
        }

        public async Task CreateAsync(User user)
        {
            await UserManager.CreateAsync(user);
        }

        public async Task DeleteAsync(User user)
        {
            await UserManager.DeleteAsync(user);
        }

        public async Task<User> FindByIdAsync(Guid userId)
        {
            return await UserManager.FindByIdAsync(userId);
        }

        public async Task<User> FindByNameAsync(string userName)
        {
            return await UserManager.FindByNameAsync(userName);
        }

        public async Task<int> GetAccessFailedCountAsync(User user)
        {
            return await Task.Run(() => UserManager.FindByIdAsync(user.Id).Result.AccessFailedCount);
        }

        public async Task<bool> GetLockoutEnabledAsync(User user)
        {
            return await Task.Run(() => UserManager.FindByIdAsync(user.Id).Result.LockOutEnable);
        }

        public async Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
        {
            return await Task.Run(() => UserManager.FindByIdAsync(user.Id).Result.LockOutEnd);
        }

        public async Task<int> IncrementAccessFailedCountAsync(User user)
        {
            return await Task.Run(async () =>
            {
                var read = UserManager.FindByIdAsync(user.Id).Result;
                var result = ++read.AccessFailedCount;
                await UpdateAsync(read);
                return result;
            });
        }

        public async Task ResetAccessFailedCountAsync(User user)
        {
            await Task.Run(async () =>
            {
                var read = UserManager.FindByIdAsync(user.Id).Result;
                read.AccessFailedCount = 0;
                await UpdateAsync(read);
            });
        }

        public async Task SetLockoutEnabledAsync(User user, bool enabled)
        {
            await Task.Run(async () =>
            {
                var read = UserManager.FindByIdAsync(user.Id).Result;
                read.LockOutEnable = enabled;
                await UpdateAsync(read);
            });
        }

        public async Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
        {
            await Task.Run(async () =>
            {
                var read = UserManager.FindByIdAsync(user.Id).Result;
                read.LockOutEnd = lockoutEnd;
                await UpdateAsync(read);
            });
        }

        public async Task UpdateAsync(User user)
        {
            await UserManager.UpdateAsync(user);
        }

        public override async Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            return await Task.Run(() =>
            {
                var user = UserManager.FindByNameAsync(userName).Result;
                return user.Password == password
                    ? (user.LockOutEnable && shouldLockout ? SignInStatus.LockedOut : SignInStatus.Success)
                    : SignInStatus.Failure;
            });
        }
    }
}
