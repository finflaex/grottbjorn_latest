﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fxSupport.fdMsSQL.fsdBase;
using fxSupport.fdMsSQL.fsdTable;
using Microsoft.AspNet.Identity;

namespace fxSupport.fdMsSQL.fsdOther
{
    public class fxRoleManager<DB> : RoleManager<Role, Guid>
        where DB : pcaDB_Identity<DB>
    {
        public fxRoleManager(fxRoleStore<DB> store = null) 
            : base(store ?? new fxRoleStore<DB>())
        {
        }
    }
}
