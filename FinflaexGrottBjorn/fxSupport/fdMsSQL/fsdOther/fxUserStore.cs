﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fxSupport.fdMsSQL.fsdBase;
using fxSupport.fdMsSQL.fsdTable;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace fxSupport.fdMsSQL.fsdOther
{
    public class fxUserStore<DB> : 
        IUserRoleStore<User, Guid>
        where DB : pcaDB_Identity<DB>
    {
        private DB create_db => (DB)typeof(DB).GetMethod("Create").Invoke(null, null);

        public async Task CreateAsync(User user)
        {
            await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                }
            });
        }

        public async Task DeleteAsync(User user)
        {
            await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    var del = db.Users.Find(user.Id);
                    db.Users.Remove(del);
                    db.SaveChanges();
                }
            });
        }

        public void Dispose()
        {
        }

        public async Task<User> FindByNameAsync(string userName)
        {
            return await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    return db.Users.FirstOrDefault(v => v.UserName == userName);
                }
            });
        }

        public async Task UpdateAsync(User user)
        {
            await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                }
            });
        }

        public async Task AddToRoleAsync(User user, string roleName)
        {
            await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    var read = db.Users.Find(user.Id);
                    var role = db.Roles.FirstOrDefault(v => v.Name == roleName);
                    if (role == null) return;
                    read.Roles.Add(role);
                    db.SaveChanges();
                }
            });
        }

        public async Task RemoveFromRoleAsync(User user, string roleName)
        {
            await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    var read = db.Users.Find(user.Id);
                    var role = read.Roles.FirstOrDefault(v => v.Name == roleName);
                    read.Roles.Remove(role);
                    db.SaveChanges();
                }
            });
        }

        public async Task<IList<string>> GetRolesAsync(User user)
        {
            return await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    return (IList<string>)db.Users.Find(user.Id).Roles.Select(v => v.Name).ToList();
                }
            });
        }

        public async Task<bool> IsInRoleAsync(User user, string roleName)
        {
            return await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    return db.Users.Find(user.Id).Roles.Select(v => v.Name).Contains(roleName);
                }
            });
        }

        public async Task<User> FindByIdAsync(Guid userId)
        {
            return await Task.Run(() =>
            {
                using (var db = create_db)
                {
                    return db.Users.Find(userId);
                }
            });
        }
    }
}
