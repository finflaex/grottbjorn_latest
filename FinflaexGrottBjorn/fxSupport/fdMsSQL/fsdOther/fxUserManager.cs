﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fxSupport.fdMsSQL.fsdBase;
using fxSupport.fdMsSQL.fsdTable;
using fxSupport.fdReflection;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace fxSupport.fdMsSQL.fsdOther
{
    public class fxUserManager<DB> : UserManager<User, Guid>
        where DB : pcaDB_Identity<DB>
    {
        public fxUserManager()
            : base(new fxUserStore<DB>())
        {
        }

        public fxUserManager(fxUserStore<DB> store) 
            : base(store)
        {
        }

        public override async Task<User> FindAsync(string userName, string password)
        {
            return await Task.Run(() =>
            {
                var user = Store.FindByNameAsync(userName).Result;
                return user.Password == password
                    ? user
                    : null;
            });
        }

        public async Task<User> FindById(string userId)
        {
            return await FindByIdAsync(userId.ToGuid());
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            return await base.AddLoginAsync(userId.ToGuid(), login);
        }
    }
}
