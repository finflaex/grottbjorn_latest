﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fxSupport.fdMsSQL.fsdOther
{
    public enum peSysStatus: short
    {
        remove      = -10,
        block       = -1,
        none        = 0,
        history     = 1,
        actual      = 10,
    }
}
