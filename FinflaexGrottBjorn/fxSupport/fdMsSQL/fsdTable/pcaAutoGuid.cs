﻿using System;
using System.ComponentModel.DataAnnotations;

namespace fxSupport.fdMsSQL.fsdTable
{
    public abstract class pcaAutoGuid
    {
        #region SYSTEM

        protected pcaAutoGuid()
        {
            sysGuid = Guid.NewGuid();
        }

        #endregion

        #region FIELDS

        [Key]
        public Guid sysGuid { get; set; }

        #endregion
    }
}