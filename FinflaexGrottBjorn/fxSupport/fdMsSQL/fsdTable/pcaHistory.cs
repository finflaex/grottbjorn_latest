﻿using fxSupport.fdReflection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using fxSupport.fdAttribute;
using fxSupport.fdMsSQL.fsdOther;

namespace fxSupport.fdMsSQL.fsdTable
{
    public abstract class pcaHistory<T>
        where T : pcaHistory<T>
    {
        public static int HistCount
        {
            get
            {
                var arr = ThisType
                    .GetProperties()
                    .Select(prop => prop.getAttrs().OfType<paHistory>().FirstOrDefault())
                    .Where(v => v != null)
                    .Select(v=>v.Index)
                    .ToArray();
                return arr.Any() ? arr.Max(v=>v) + 1 : 0;
            }
        }

        protected pcaHistory(Guid? guid = null)
        {
            sysGuid = Guid.NewGuid();
            sysStartDT = DateTime.Now;
            sysEndDT = DateTime.MaxValue;
            sysStaus = peSysStatus.actual;
            sysModification = new byte[0];

            Guid = guid ?? Guid.NewGuid();
        }

        /// <summary>
        /// 
        /// </summary>
        [Key]
        [Column("sysGuid")]
        public Guid sysGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Timestamp]
        public byte[] sysVersion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime sysStartDT { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime sysEndDT { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime sysActDT { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public peSysStatus sysStaus { get; set; }

        public byte[] sysModification { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid Guid { get; set; }

        protected T This => (T) this;
        protected static Type ThisType => typeof (T);

        public bool CompareHistory(T value)
        {
            var temp = new BitArray(HistCount);

            ThisType
                .GetProperties()
                .Select(prop => new
                {
                    prop = prop,
                    attr = prop.getAttrs().OfType<paHistory>().FirstOrDefault(),
                })
                .Where(read => read.attr != null)
                .ForEach(read =>
                {
                    var old_value = read.prop.GetValue(value);
                    var new_value = read.prop.GetValue(This);
                    var index = read.attr.Index;
                    if (read.prop.PropertyType == typeof(double))
                    {
                        var flag = !old_value.asType<double>().Compare(new_value.asType<double>());
                        temp.Set(index, flag);
                    }
                    else if (read.prop.PropertyType == typeof(decimal))
                    {
                        var flag = !old_value.asType<decimal>().Compare(new_value.asType<decimal>());
                        temp.Set(index, flag);
                    }
                    else if (read.prop.PropertyType == typeof(float))
                    {
                        var flag = !old_value.asType<float>().Compare(new_value.asType<float>());
                        temp.Set(index, flag);
                    }
                    else
                    {
                        var flag = old_value?.ToString() != new_value?.ToString();
                        temp.Set(index, flag);
                    }
                });

            sysModification = temp.ToByteArray();
            return temp.Cast<bool>().Any(bit => bit);
        }

        public string[] ListDescMod
        {
            get
            {
                var pa = This
                    .getProperties()
                    .Select(p => new
                    {
                        prop = p,
                        attr = p.getAttrs().OfType<paHistory>().FirstOrDefault()
                    })
                    .Where(v => v.attr != null)
                    .OrderBy(v => v.attr.Index)
                    .ToArray();

                return sysModification
                    .ToBitArray()
                    .ToBoolEnumerable()
                    .Select((v, i) => new
                    {
                        index = i,
                        flag = v,
                    })
                    .Where(v => v.flag)
                    .Select(v =>
                    {
                        var prop = pa.FirstOrDefault(p => p.attr.Index == v.index);
                        return prop?.attr?.Description ?? prop?.prop?.Name ?? "unknown";
                    })
                    .ToArray();
            }
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class paHistory : Attribute
    {
        public paHistory(int index, string description)
        {
            Index = index;
            Description = description;
        }

        public int Index { get; set; }
        public string Description { get; set; }
    }
}
