﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace fxSupport.fdMsSQL.fsdTable
{
    public class Role : IRole<Guid>
    {
        public Role()
        {
            Name = "unknown";
            Users = new HashSet<User>();
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
