﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using fxSupport.fdReflection;
using Microsoft.AspNet.Identity;

namespace fxSupport.fdMsSQL.fsdTable
{
    public class User : IUser<Guid>
    {
        public User()
        {
            UserName = "unknown";
            Roles = new HashSet<Role>();
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }

        public string UserName { get; set; }

        [Column(name: "Password")]
        public string sysPassword { get; set; }

        [NotMapped]
        public string Password
        {
            get { return sysPassword.FromBase64().Decrypt(); }
            set { sysPassword = value.Encrypt().ToBase64(); }
        }

        public DateTimeOffset LockOutEnd { get; set; }
        public int AccessFailedCount { get; set; }
        public bool LockOutEnable { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
        public string Email { get; set; }

        public bool InRoles(string role) => Roles.Select(v => v.Name).Contains(role);
    }
}
