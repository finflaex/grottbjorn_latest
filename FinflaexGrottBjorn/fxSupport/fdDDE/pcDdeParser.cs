﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fxSupport.fdLogger;
using fxSupport.fdReflection;

namespace fxSupport.fdDDE
{
    public class pcDdeParser
    {
        // **********************************************************************

        public enum BlockType
        {
            Table = 0x10,

            Float = 0x01,
            String = 0x02,
            Bool = 0x03,
            Error = 0x04,
            Blank = 0x05,
            Int = 0x06,
            Skip = 0x07,

            Unknown = 0x10000,
            Bad = 0x10001
        }

        // **********************************************************************

        static readonly Encoding win1251 = Encoding.GetEncoding(1251);

        // **********************************************************************

        const int wsize = 2;
        const int fsize = 8;
        const int hsize = wsize * 2;

        // **********************************************************************

        byte[] data;

        MemoryStream ms;
        BinaryReader br;

        int blocksize;

        // **********************************************************************

        public int Rows { get; private set; }
        public int Columns { get; private set; }

        public BlockType ValueType { get; private set; }

        // **********************************************************************

        public pcDdeParser(byte[] data)
        {
            this.data = data;

            ms = new MemoryStream(data);
            br = new BinaryReader(ms, win1251);

            if (data.Length < wsize * 4 || (BlockType)br.ReadUInt16() != BlockType.Table)
                SetBadDataStatus();

            ms.Seek(wsize, SeekOrigin.Current);

            Rows = br.ReadUInt16();
            Columns = br.ReadUInt16();

            ValueType = BlockType.Unknown;
        }

        // **********************************************************************

        void SetBadDataStatus()
        {
            ValueType = BlockType.Bad;
            blocksize = 1;
        }

        // **********************************************************************

        public object ReadValue()
        {
            // ------------------------------------------------------------

            if (ValueType == BlockType.Unknown)
            {
                if (ms.Position + hsize > data.Length)
                {
                    SetBadDataStatus();
                }
                else
                {
                    ValueType = (BlockType)br.ReadUInt16();
                    blocksize = br.ReadUInt16();

                    if (ms.Position + blocksize > data.Length)
                        SetBadDataStatus();
                }
            }

            // ------------------------------------------------------------

            if (blocksize > 0)
                switch (ValueType)
                {
                    // ------------------------------------------

                    case BlockType.Float:
                        blocksize -= fsize;

                        if (blocksize >= 0)
                        {
                            var result = br.ReadDouble();
                            //pscLog.Log(ValueType.ToString(), result.ToString());
                            return result;
                        }
                        else
                        {
                            SetBadDataStatus();
                            //pscLog.Log(ValueType.ToString(), "error");
                            return 0;
                        }

                    // ------------------------------------------

                    case BlockType.String:
                        int strlen = ms.ReadByte();
                        blocksize -= strlen + 1;

                        if (blocksize >= 0)
                        {
                            var StringValue = win1251.GetString(data, (int)ms.Position, strlen);
                            br.BaseStream.Seek(strlen, SeekOrigin.Current);
                            //pscLog.Log(ValueType.ToString(), StringValue);
                            return StringValue;
                        }
                        SetBadDataStatus();
                        //pscLog.Log(ValueType.ToString(), "error");
                        return null;

                    // ------------------------------------------

                    case BlockType.Bool:
                    case BlockType.Blank:
                    case BlockType.Skip:
                        blocksize -= wsize;

                        if (blocksize >= 0)
                        {
                            var result = br.ReadUInt16() != 0;
                            //pscLog.Log(ValueType.ToString(), result.ToString());
                            return result;
                        }
                        SetBadDataStatus();
                        //pscLog.Log(ValueType.ToString(), "error");
                        return false;

                    // ------------------------------------------

                    case BlockType.Error:
                    case BlockType.Int:
                        blocksize -= wsize;

                        if (blocksize >= 0)
                        {
                            var result = br.ReadUInt16();
                            //pscLog.Log(ValueType.ToString(), result.ToString());
                            return result;
                        }

                        SetBadDataStatus();
                        //pscLog.Log(ValueType.ToString(), "error");
                        return 0;

                    // ------------------------------------------

                    default:
                        SetBadDataStatus();
                        //pscLog.Log(ValueType.ToString(), "unknown");
                        return null;

                        // ------------------------------------------
                }
            ValueType = BlockType.Unknown;
            return ReadValue();
        }

        // **********************************************************************

        public void Dispose()
        {
            br.Dispose();
            ms.Dispose();
        }

        // **********************************************************************
        public double ReadDouble()
        {
            return ReadValue().asType<double>();
        }

        public long ReadLong()
        {
            return ReadDouble().ToLong();
        }

        public string ReadString()
        {
            return ReadValue().asType<string>();
        }

        public DateTime? ReadDateTime()
        {
            return ReadString().ToDateTime();
        }

        public float ReadFloat()
        {
            return ReadDouble().ToFloat();
        }

        public int ReadInt()
        {
            return ReadDouble().ToInt();
        }

        public decimal ReadDecimal()
        {
            return ReadDouble().ToDecimal();
        }
    }
}
