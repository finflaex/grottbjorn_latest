﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using fxSupport.fdReflection;
using NDde.Server;
using Timer = System.Timers.Timer;

namespace fxSupport.fdDDE
{
    public class pcDdeServer : DdeServer
    {
        private readonly Timer timer = new Timer();
        private Queue<KeyValuePair<string, byte[]>> stack;

        public Action<string, pcDdeParser> poke { get; }

        public pcDdeServer(string service, Action<string, pcDdeParser> action) : base(service)
        {
            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 100;
            timer.SynchronizingObject = Context;
            poke = action;
            stack = new Queue<KeyValuePair<string, byte[]>>();
            new Thread(work)
            {
                IsBackground = true
            }
            .Start();
        }

        private void work()
        {
            while (true)
            {
                if (stack.Count < 1)
                {
                    Thread.Sleep(timer.Interval.ToInt());
                    continue;
                }
                try
                {
                    var read = stack.Dequeue();
                    poke?.Invoke(read.Key, new pcDdeParser(read.Value));
                }
                catch (InvalidOperationException)
                {
                    Thread.Sleep(timer.Interval.ToInt());
                }
            }
        }

        public override void Register()
        {
            try
            {
                base.Register();
                timer.Start();
            }
            catch (InvalidOperationException)
            {
            }
        }

        public override void Unregister()
        {
            try
            {
                timer.Stop();
                base.Unregister();
            }
            catch (Exception)
            {
            }
        }

        protected override bool OnStartAdvise(DdeConversation conversation, string item, int format) => format == 1;

        protected override ExecuteResult OnExecute(DdeConversation conversation, string command) => ExecuteResult.Processed;

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Advise("*", "*");
        }

        protected override RequestResult OnRequest(DdeConversation conversation, string item, int format) =>
            format == 1
            ? new RequestResult(System.Text.Encoding.ASCII.GetBytes("Time=" + DateTime.Now + "\0"))
            : RequestResult.NotProcessed;

        protected override byte[] OnAdvise(string topic, string item, int format) =>
            format == 1
            ? System.Text.Encoding.ASCII.GetBytes("Time=" + DateTime.Now + "\0")
            : null;

        protected override PokeResult OnPoke(
            DdeConversation conversation, 
            string item, 
            byte[] data, 
            int format)
        {
            stack.Enqueue(new KeyValuePair<string, byte[]>(conversation.Topic, data));
            return PokeResult.Processed;
        }
    }
}
