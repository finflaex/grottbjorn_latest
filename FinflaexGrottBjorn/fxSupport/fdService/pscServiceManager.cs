﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;

namespace fxSupport.fdService
{
    public static class pscServiceManager
    {
        //private static readonly ServiceController service = new ServiceController();

        public static bool IsInstall(string name)
        {
            return ServiceController.GetServices().FirstOrDefault(v => v.DisplayName == name) != null;
        }

        public const int SC_MANAGER_CREATE_SERVICE = 0x0002;
        public const int SERVICE_WIN32_OWN_PROCESS = 0x00000010;
        //public const int SERVICE_DEMAND_START = 0x00000003;
        public const int SERVICE_ERROR_NORMAL = 0x00000001;
        public const int STANDARD_RIGHTS_REQUIRED = 0xF0000;
        public const int SERVICE_QUERY_CONFIG = 0x0001;
        public const int SERVICE_CHANGE_CONFIG = 0x0002;
        public const int SERVICE_QUERY_STATUS = 0x0004;
        public const int SERVICE_ENUMERATE_DEPENDENTS = 0x0008;
        public const int SERVICE_START = 0x0010;
        public const int SERVICE_STOP = 0x0020;
        public const int SERVICE_PAUSE_CONTINUE = 0x0040;
        public const int SERVICE_INTERROGATE = 0x0080;
        public const int SERVICE_USER_DEFINED_CONTROL = 0x0100;

        public const int SERVICE_ALL_ACCESS = STANDARD_RIGHTS_REQUIRED |
                                              SERVICE_QUERY_CONFIG |
                                              SERVICE_CHANGE_CONFIG |
                                              SERVICE_QUERY_STATUS |
                                              SERVICE_ENUMERATE_DEPENDENTS |
                                              SERVICE_START |
                                              SERVICE_STOP |
                                              SERVICE_PAUSE_CONTINUE |
                                              SERVICE_INTERROGATE |
                                              SERVICE_USER_DEFINED_CONTROL;

        public const int SERVICE_AUTO_START = 0x00000002;

        /// <summary>
        ///     This method installs and runs the service in the service control manager.
        /// </summary>
        /// <param name="svcPath">The complete path of the service.</param>
        /// <param name="svcName">Name of the service.</param>
        /// <param name="svcDispName">Display name of the service.</param>
        /// <returns>True if the process went thro successfully. False if there was any error.</returns>
        public static bool InstallService(string svcPath, string svcName, string svcDispName)
        {
            try
            {
                var sc_handle = OpenSCManager(null, null, SC_MANAGER_CREATE_SERVICE);
                if (sc_handle.ToInt64() != 0)
                {
                    var sv_handle = CreateService(sc_handle, svcName, svcDispName, SERVICE_ALL_ACCESS,
                        SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, SERVICE_ERROR_NORMAL, svcPath, null, 0, null,
                        null, null);
                    if (sv_handle.ToInt64() == 0)
                    {
                        CloseServiceHandle(sc_handle);
                        return false;
                    }
                    //now trying to start the service
                    var i = StartService(sv_handle, 0, null);
                    // If the value i is zero, then there was an error starting the service.
                    // note: error may arise if the service is already running or some other problem.
                    if (i == 0)
                    {
                        //Console.WriteLine("Couldnt start service");
                        return false;
                    }
                    //Console.WriteLine("Success");
                    CloseServiceHandle(sc_handle);
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///     This method uninstalls the service from the service conrol manager.
        /// </summary>
        /// <param name="svcName">Name of the service to uninstall.</param>
        public static bool UnInstallService(string svcName)
        {
            var GENERIC_WRITE = 0x40000000;
            var sc_hndl = OpenSCManager(null, null, GENERIC_WRITE);
            if (sc_hndl.ToInt64() != 0)
            {
                var DELETE = 0x10000;
                var svc_hndl = OpenService(sc_hndl, svcName, DELETE);
                //Console.WriteLine(svc_hndl.ToInt32());
                if (svc_hndl.ToInt64() == 0) return false;
                var i = DeleteService(svc_hndl);
                if (i != 0)
                {
                    CloseServiceHandle(sc_hndl);
                    return true;
                }
                CloseServiceHandle(sc_hndl);
                return false;
            }
            return false;
        }

        #region DLLImport

        [DllImport("advapi32.dll")]
        public static extern IntPtr OpenSCManager(string lpMachineName, string lpSCDB, int scParameter);

        [DllImport("Advapi32.dll")]
        public static extern IntPtr CreateService(IntPtr SC_HANDLE, string lpSvcName, string lpDisplayName,
            int dwDesiredAccess, int dwServiceType, int dwStartType, int dwErrorControl, string lpPathName,
            string lpLoadOrderGroup, int lpdwTagId, string lpDependencies, string lpServiceStartName, string lpPassword);

        [DllImport("advapi32.dll")]
        public static extern void CloseServiceHandle(IntPtr SCHANDLE);

        [DllImport("advapi32.dll")]
        public static extern int StartService(IntPtr SVHANDLE, int dwNumServiceArgs, string lpServiceArgVectors);

        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern IntPtr OpenService(IntPtr SCHANDLE, string lpSvcName, int dwNumServiceArgs);

        [DllImport("advapi32.dll")]
        public static extern int DeleteService(IntPtr SVHANDLE);

        [DllImport("kernel32.dll")]
        public static extern int GetLastError();

        #endregion DLLImport
    }
}
