﻿using System.Collections;
using System.Collections.Generic;

namespace fxSupport.fdReflection
{
    public static partial class pscReflection
    {
        public static byte[] ToByteArray(this BitArray self)
        {
            var numBytes = self.Count / 8;
            if (self.Count % 8 != 0) numBytes++;

            var bytes = new byte[numBytes];
            var byteIndex = 0;
            var bitIndex = 0;

            for (var i = 0; i < self.Count; i++)
            {
                if (self[i])
                    bytes[byteIndex] |= (byte)(1 << (7 - bitIndex));

                bitIndex++;
                if (bitIndex != 8) continue;
                bitIndex = 0;
                byteIndex++;
            }

            return bytes;
        }

        public static IEnumerable<bool> ToBoolEnumerable(this BitArray self)
        {
            for (var i = 0; i < self.Length; i++)
            {
                yield return self.Get(i);
            }
        }
    }
}