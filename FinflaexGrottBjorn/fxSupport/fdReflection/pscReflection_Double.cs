﻿using System;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static DateTime ToDateTime(this double self)
        {
            return DateTime.FromOADate(self);
        }

        public static bool Compare(this double self, double value)
        {
            return Math.Abs(self - value) < 0.0000000001;
        }

        public static long ToLong(this double self)
        {
            return Convert.ToInt64(self);
        }

        public static decimal ToDecimal(this double self)
        {
            return Convert.ToDecimal(self);
        }

        public static int ToInt(this double self)
        {
            return Convert.ToInt32(self);
        }

        public static float ToFloat(this double self)
        {
            return Convert.ToSingle(self);
        }
    }
}