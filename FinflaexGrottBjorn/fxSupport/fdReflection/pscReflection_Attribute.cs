﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static Attribute[] getAttrs<T, P>(this T self, Expression<Func<T, P>> action)
        {
            return Attribute.GetCustomAttributes(((MemberExpression) action.Body).Member);
        }

        public static Attribute[] getAttrs<T>(this T self, string name)
        {
            var prop = self.getProperties().FirstOrDefault(v => v.Name == name);
            return self.getAttrs(prop);
        }

        public static Attribute[] getAttrs<T>(this T self, PropertyInfo propinfo)
        {
            return Attribute.GetCustomAttributes(propinfo, typeof(T));
        }
    }
}