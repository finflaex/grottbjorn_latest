﻿using Newtonsoft.Json;

namespace fxSupport.fdReflection
{
    public static partial class pscReflection
    {
        public static string ToJson<T>(this T self)
        {
            return JsonConvert.SerializeObject(self);
        }
    }
}