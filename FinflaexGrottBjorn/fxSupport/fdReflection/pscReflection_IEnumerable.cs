﻿using System;
using System.Collections.Generic;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static string Join<T>(this IEnumerable<T> self, string spliter = "")
        {
            return string.Join(spliter, self);
        }

        public static void ForEach<T>(this IEnumerable<T> self, Action<T> action = null)
        {
            if (action == null) return;
            foreach (var read in self)
            {
                action(read);
            }
        }

        public static void For<T>(this IEnumerable<T> self, Action<int, T> action = null)
        {
            if (action == null) return;
            var index = 0;
            foreach (var read in self)
            {
                action(index++, read);
            }
        }
    }
}