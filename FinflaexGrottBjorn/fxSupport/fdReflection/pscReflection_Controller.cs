﻿using System.IO;
using System.Web.Mvc;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static string Post<T>(this T self)
            where T : Controller
        {
            using (var reader = new StreamReader(self.Request.InputStream))
            {
                reader.BaseStream.Seek(0, SeekOrigin.Begin);
                var read = reader.ReadToEnd();
                return read;
            }
        }
    }
}