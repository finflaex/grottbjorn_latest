﻿using System;

namespace fxSupport.fdReflection
{
    public static partial class pscReflection
    {
        public static int For(this int self, Action<int> action = null)
        {
            if (action!= null)
            {
                for (var i = 0; i < self; i++)
                {
                    action(i);
                }
            }
            return self;
        }

        public static int For(this int self, Action action = null)
        {
            if (action != null)
            {
                for (var i = 0; i < self; i++)
                {
                    action();
                }
            }
            return self;
        }

        public static int Dec(this int self, int dec = 1)
        {
            return self - dec;
        }
        public static int Inc(this int self, int inc = 1)
        {
            return self + inc;
        }
    }
}