﻿using System;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static void isTrue(this bool self, Action action = null)
        {
            if (self)
            {
                action?.Invoke();
            }
        }
    }
}