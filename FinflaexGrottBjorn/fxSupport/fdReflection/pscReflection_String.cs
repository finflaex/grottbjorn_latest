﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static string HtmlDecode(this string self)
        {
            return HttpUtility.HtmlDecode(self);
        }

        public static string HtmlEncode(this string self)
        {
            return HttpUtility.HtmlEncode(self);
        }

        public static string Encrypt(this string value, string password = "finflaex")
        {
            try
            {
                byte[] Results;
                var UTF8 = new UTF8Encoding();
                var HashProvider = new MD5CryptoServiceProvider();
                var TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(password));
                var TDESAlgorithm = new TripleDESCryptoServiceProvider
                {
                    Key = TDESKey,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };
                var DataToEncrypt = UTF8.GetBytes(value);
                try
                {
                    var Encryptor = TDESAlgorithm.CreateEncryptor();
                    Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
                }
                finally
                {
                    TDESAlgorithm.Clear();
                    HashProvider.Clear();
                }
                return Convert.ToBase64String(Results);
            }
            catch
            {
                return "";
            }
        }

        public static string Decrypt(this string value, string password = "finflaex")
        {
            try
            {
                byte[] Results;
                var UTF8 = new UTF8Encoding();
                var HashProvider = new MD5CryptoServiceProvider();
                var TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(password));
                var TDESAlgorithm = new TripleDESCryptoServiceProvider();
                TDESAlgorithm.Key = TDESKey;
                TDESAlgorithm.Mode = CipherMode.ECB;
                TDESAlgorithm.Padding = PaddingMode.PKCS7;
                var DataToDecrypt = Convert.FromBase64String(value);
                try
                {
                    var Decryptor = TDESAlgorithm.CreateDecryptor();
                    Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
                }
                finally
                {
                    TDESAlgorithm.Clear();
                    HashProvider.Clear();
                }
                return UTF8.GetString(Results);
            }
            catch
            {
                return "";
            }
        }

        public static string ToBase64(this string self)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(self));
        }

        public static string FromBase64(this string self)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(self));
        }

        public static T FromJson<T>(this string self)
        {
            return (T) JsonConvert.DeserializeObject(self, typeof (T));
        }

        public static object FromJson(this string self)
        {
            return JsonConvert.DeserializeObject(self);
        }

        public static bool isNullOrEmpty(this string self)
        {
            return string.IsNullOrEmpty(self);
        }

        public static string isNullOrEmpty(this string self, Action action)
        {
            if (string.IsNullOrEmpty(self))
            {
                action?.Invoke();
            }
            return self;
        }

        public static string notNullOrEmpty(this string self, Action<string> action)
        {
            if (!string.IsNullOrEmpty(self))
            {
                action?.Invoke(self);
            }
            return self;
        }

        public static Guid ToGuid(this string self)
        {
            return Guid.Parse(self);
        }

        public static DateTime? ToDateTime(this string self)
        {
            try
            {
                return DateTime.Parse(self);
            }
            catch
            {
                return null;
            }
        }

        public static int ToInt(this string self)
        {
            return int.Parse(self);
        }

        public static long ToLong(this string self)
        {
            return long.Parse(self);
        }

        public static string TrimEnd(this string self, string shablon)
        {
            var start = self.Length - shablon.Length;
            var inner = self.Substring(start);
            return inner == shablon
                ? self.Remove(start)
                : self;
        }
    }
}