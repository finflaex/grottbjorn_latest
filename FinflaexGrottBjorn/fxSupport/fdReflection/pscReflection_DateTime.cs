﻿using System;
using System.Globalization;
using System.Linq;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static double ToDouble(this DateTime self)
        {
            return self.ToOADate();
        }

        public static string ToString(this DateTime? self, params object[] param)
        {
            if (self == null)
            {
                return string.Empty;
            }
            var format = param.OfType<string>().FirstOrDefault();
            var culture = param.OfType<CultureInfo>().FirstOrDefault();
            var date = (DateTime)self;
            return format != null
                ? date.ToString()
                : culture != null
                    ? date.ToString(culture)
                    : string.Empty;
        }
    }
}