﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using fxSupport.fdLogger;
using fxSupport.fdMsSQL.fsdOther;
using fxSupport.fdMsSQL.fsdTable;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static DbSet<V> GetTable<V>(this DbContext self)
            where V : class 
        {
            return self.getProperties()
                .FirstOrDefault(p => p.PropertyType == typeof(DbSet<V>))?
                .GetValue(self)
                .asType<DbSet<V>>();
        }
    }
}