﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using fxSupport.fdLogger;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static T notNull<T>(this T self, Action<T> action = null)
        {
            if (self != null)
            {
                action?.Invoke(self);
            }
            return self;
        }

        public static T ifNull<T>(this T self, Action<T> action = null)
        {
            if (self == null)
            {
                action?.Invoke(self);
            }
            return self;
        }

        public static bool isNull<T>(this T self, Action<T> action = null)
        {
            if (self == null)
            {
                action?.Invoke(self);
            }
            return self != null;
        }

        public static R retIfNull<T, R>(this T self, Func<T, R> not_null = null, Func<R> is_null = null)
        {
            if (self == null)
            {
                return is_null == null ? default(R) : is_null.Invoke();
            }
            return not_null == null ? default(R) : not_null.Invoke(self);
        }

        public static T asType<T>(this object self)
        {
            return (T) self;
        }

        public static bool isType<T>(this object self)
        {
            return self is T;
        }

        public static object getPropertyValue<T>(this T self, string name)
        {
            var type = typeof(T);
            try
            {
                var result = type
                    .GetProperties()
                    .FirstOrDefault(v => v.Name == name)?
                    .GetValue(self);
                pscLog.Log(
                    peTypeLog.Trace,
                    MethodBase.GetCurrentMethod(),
                    $"type\t= {type.FullName}",
                    $"property\t= {name}",
                    $"value\t= {result}");

                return result;
            }
            catch (Exception error)
            {
                pscLog.Log(
                    peTypeLog.Error,
                    MethodBase.GetCurrentMethod(),
                    $"type\t= {type.FullName}",
                    $"property\t= {name}",
                    error);

                return null;
            }
        }

        public static IEnumerable<PropertyInfo> getProperties<T>(this T self, params string[] names)
        {
            var type = self.GetType();
            var result = type.GetProperties();
            return names.Length > 0
                ? result.Where(v => names.Contains(v.Name))
                : result;
        }
    }
}