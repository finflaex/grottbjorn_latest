﻿using System;
using System.Text.RegularExpressions;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static bool Compare(this decimal self, decimal value)
        {
            return Math.Abs(self - value) < 0.0000001M;
        }
    }
}