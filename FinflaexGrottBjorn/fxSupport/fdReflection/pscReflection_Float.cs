﻿using System;

namespace fxSupport.fdReflection
{
    partial class pscReflection
    {
        public static bool Compare(this float self, float value)
        {
            return Math.Abs(self - value) < 0.0000001;
        }
    }
}