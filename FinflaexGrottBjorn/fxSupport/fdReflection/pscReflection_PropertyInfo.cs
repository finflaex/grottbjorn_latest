﻿using System;
using System.Reflection;

namespace fxSupport.fdReflection
{
    public static partial class pscReflection
    {
        public static Attribute[] getAttrs(this PropertyInfo self)
        {
            return Attribute.GetCustomAttributes(self);
        }
    }
}