﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using finflaex_IIS_grottbjorn.Attributes;
using fxGDB_GrottBjorn.Base;
using fxGDB_GrottBjorn.Tables;
using fxSupport.fdMsSQL.fsdOther;
using fxSupport.fdReflection;
using static System.Data.Entity.DbFunctions;

namespace finflaex_IIS_grottbjorn.Controllers
{
    public class QuikController : Controller
    {
        [Method("Index","anonym",1)]
        public ActionResult Index(string type)
        {
            return View("Index", new QuikModel()
            {
                type = type,
            });
        }

        [Method("Index", "user", 2)]
        public ActionResult IndexUser(string type)
        {
            return View("Index", new QuikModel()
            {
                type = type,
            });
        }

        [Method("JsonData", "anonym", 1)]
        public ActionResult JsonData(string data)
        {
            return new EmptyResult();
        }

        [Method("JsonData", "user", 2)]
        public ActionResult JsonDataUser(string type)
        {
            using (var db = GDB.Create())
            {
                var cdt = DateTime.Now < DateTime.Today.AddHours(12)
                    ? DateTime.Today.AddDays(-1) : DateTime.Today;
                try
                {
                    object data = null;
                    switch (type)
                    {
                        case nameof(Quik_StopOrder):
                            data = db
                                .QuikStopOrders
                                .Where(record
                                    => record.sysStaus == peSysStatus.actual
                                       && TruncateTime(record.sysActDT) == cdt
                                )
                                .ToArray()
                                .Select(read => new
                                {
                                    Number = read.Number,
                                    Dealer = read.Dealer,
                                    DateTime = read.DateTime.ToString("yy-MM-dd hh:mm"),
                                    DateTimeWithDrawal = read.DateTimeWithDrawal.ToString("yy-MM-dd hh:mm"),
                                    Type = read.Type,
                                    PaperShortName = read.PaperShortName,
                                    Operation = read.Operation,
                                    PaperStopPrice = read.PaperStopPrice,
                                    DirectionStopPrice = read.DirectionStopPrice,
                                    StopPrice = read.StopPrice,
                                    DirectionStopLimit = read.DirectionStopLimit,
                                    StopLimit = read.StopLimit,
                                    Price = read.Price,
                                    MarketStopLimit = read.MarketStopLimit,
                                    Count = read.Count,
                                    ActualCount = read.ActualCount,
                                    UsedCount = read.UsedCount,
                                    NumberApplication = read.NumberApplication,
                                    TermsOfADeal = read.TermsOfADeal,
                                    Time = read.Time.ToString("yy-MM-dd hh:mm"),
                                    State = read.State,
                                    Result = read.Result,
                                    RelatedApplication = read.RelatedApplication,
                                    RelatedApplicationPrice = read.RelatedApplicationPrice,
                                    Error = read.Error,
                                    ProtectiveSpread = read.ProtectiveSpread,
                                    MarketTakeProfit = read.MarketTakeProfit,
                                    ApplicationCondition = read.ApplicationCondition,
                                    Contract = read.Contract.Account ?? string.Empty,
                                })
                                .ToArray();
                            break;
                    }
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception err)
                {
                    return new EmptyResult();
                }
            }
        }
    }

    public class QuikModel
    {
        public string type { get; set; }
    }
}