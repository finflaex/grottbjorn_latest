﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using finflaex_IIS_grottbjorn.Attributes;

namespace finflaex_IIS_grottbjorn.Controllers
{
    public class HomeController : Controller
    {
        [Method("Index", "admin", 1)]
        public ActionResult IndexAdmin()
        {
            ViewBag.Message = "Page for admin";

            return View("About");
        }

        [Method("Index", "user", 2)]
        public ActionResult IndexUser()
        {
            ViewBag.Message = "Page for user";

            return View("About");
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}