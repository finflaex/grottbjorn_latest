﻿using System.Web;
using fxGDB_GrottBjorn.Base;
using fxSupport.fdIIS.fdsApi;
using fxSupport.fdIIS.fdsAuth;

namespace finflaex_IIS_grottbjorn.Handlers
{
    public class WebWSController : pcaWebSocket<WebHandler>
    {
    }

    public class WebHandler : pcaWsHandler
    {
        public override void OnOpen()
        {
            base.OnOpen();
        }

        public override void OnMessage(string message)
        {
            Send("OK");
        }
    }
}