﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Async;
using System.Web.Routing;
using fxGDB_GrottBjorn.Base;
using fxSupport.fdIIS.fdsAuth;
using fxSupport.fdReflection;
using Microsoft.Ajax.Utilities;

namespace finflaex_IIS_grottbjorn.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class MethodAttribute : FilterAttribute
    {
        public MethodAttribute(string name, string role, int order = 0)
        {
            Name = name;
            Role = role;
            Order = order;
        }

        public string Name { get; set; }
        public string Role { get; set; }
    }

    public class MethodFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var fx = filterContext.HttpContext.GetAuthentification<GDB>()
                     ?? new fxAuthentification<GDB>(filterContext.HttpContext, () => new GDB());

            filterContext
                .ActionDescriptor
                .ControllerDescriptor
                .ControllerType
                .GetMethods()
                .Select(v => new
                {
                    method = v,
                    attr = (MethodAttribute) GetCustomAttribute(v, typeof (MethodAttribute))
                })
                .Where(v => v.attr != null && v.attr.Name == filterContext.ActionDescriptor.ActionName)
                .OrderBy(v => v.attr.Order)
                .FirstOrDefault(v => fx.Intersect(v.attr.Role.Split(',')))
                .IfNotNull(v =>
                {
                    var controller = filterContext
                        .ActionDescriptor
                        .ControllerDescriptor;
                    var method = controller
                        .FindAction(filterContext.Controller.ControllerContext, v.method.Name);
                    try
                    {
                        if (filterContext.HttpContext.Request.HttpMethod.ToLower() == "get")
                        {
                            var data = filterContext.HttpContext.Request.QueryString.ToDictionary<string, object>();
                            filterContext.Result = method
                                .Execute(filterContext.Controller.ControllerContext, data)
                                as ActionResult;
                        }
                        else
                        {
                            filterContext.Result = method
                                .Execute(filterContext.Controller.ControllerContext, filterContext.ActionParameters)
                                as ActionResult;
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary()
                        {
                            {"controller", controller.ControllerName},
                            {"action", method.ActionName}
                        });
                    }
                });
        }
    }
}