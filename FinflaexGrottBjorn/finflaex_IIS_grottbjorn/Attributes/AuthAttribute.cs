﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using fxGDB_GrottBjorn.Base;
using fxSupport.fdIIS.fdsAuth;

namespace finflaex_IIS_grottbjorn.Attributes
{
    public class AuthAttribute : AuthorizeAttribute
    {
        private readonly string[] allowedRoles;

        public AuthAttribute(params string[] roles)
        {
            allowedRoles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var fx = new fxAuthentification<GDB>(httpContext, () => new GDB());
            return fx.IsAuthenticated && (allowedRoles.Length <= 0 || fx.Intersect(allowedRoles));
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(
                new
                {
                    action = "Login",
                    controller = "Account"
                }));
        }
    }

    public class AuthAllAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return true;
        }
    }

    public class AuthNoneAttribute : AuthorizeAttribute
    {
        private readonly string[] allowedRoles;

        public AuthNoneAttribute(params string[] roles)
        {
            allowedRoles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var fx = new fxAuthentification<GDB>(httpContext, () => new GDB());
            return !fx.IsAuthenticated || allowedRoles.Contains("anonym");
        }
    }
}