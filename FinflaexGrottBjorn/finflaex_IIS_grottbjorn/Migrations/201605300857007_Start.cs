namespace finflaex_IIS_grottbjorn.Models
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.Double(nullable: false),
                        sysEndDT = c.Double(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.sysGuid);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        FORTS_Account = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.Double(nullable: false),
                        sysEndDT = c.Double(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                        Client_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.Clients", t => t.Client_sysGuid)
                .Index(t => t.Client_sysGuid);
            
            CreateTable(
                "dbo.RTS_PositionInstrument",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        PartFreePosition = c.Double(nullable: false),
                        CurrentLongPosition = c.Double(nullable: false),
                        CurrentShortPosition = c.Double(nullable: false),
                        CurrentFreePosition = c.Double(nullable: false),
                        ActiveBuying = c.Double(nullable: false),
                        ActiveSales = c.Double(nullable: false),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.Double(nullable: false),
                        sysEndDT = c.Double(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                        Instrument_sysGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.RTSInstruments", t => t.Instrument_sysGuid, cascadeDelete: true)
                .Index(t => t.Instrument_sysGuid);
            
            CreateTable(
                "dbo.RTSInstruments",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Code = c.String(),
                        ShortName = c.String(),
                        MaturityDate = c.DateTime(nullable: false),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.sysGuid);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ContractRTS_PositionInstrument",
                c => new
                    {
                        Contract_sysGuid = c.Guid(nullable: false),
                        RTS_PositionInstrument_sysGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Contract_sysGuid, t.RTS_PositionInstrument_sysGuid })
                .ForeignKey("dbo.Contracts", t => t.Contract_sysGuid, cascadeDelete: true)
                .ForeignKey("dbo.RTS_PositionInstrument", t => t.RTS_PositionInstrument_sysGuid, cascadeDelete: true)
                .Index(t => t.Contract_sysGuid)
                .Index(t => t.RTS_PositionInstrument_sysGuid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Contracts", "Client_sysGuid", "dbo.Clients");
            DropForeignKey("dbo.ContractRTS_PositionInstrument", "RTS_PositionInstrument_sysGuid", "dbo.RTS_PositionInstrument");
            DropForeignKey("dbo.ContractRTS_PositionInstrument", "Contract_sysGuid", "dbo.Contracts");
            DropForeignKey("dbo.RTS_PositionInstrument", "Instrument_sysGuid", "dbo.RTSInstruments");
            DropIndex("dbo.ContractRTS_PositionInstrument", new[] { "RTS_PositionInstrument_sysGuid" });
            DropIndex("dbo.ContractRTS_PositionInstrument", new[] { "Contract_sysGuid" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.RTS_PositionInstrument", new[] { "Instrument_sysGuid" });
            DropIndex("dbo.Contracts", new[] { "Client_sysGuid" });
            DropTable("dbo.ContractRTS_PositionInstrument");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.RTSInstruments");
            DropTable("dbo.RTS_PositionInstrument");
            DropTable("dbo.Contracts");
            DropTable("dbo.Clients");
        }
    }
}
