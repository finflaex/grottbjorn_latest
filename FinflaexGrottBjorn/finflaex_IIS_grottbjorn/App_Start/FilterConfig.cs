﻿using System.Web.Mvc;
using finflaex_IIS_grottbjorn.Attributes;

namespace finflaex_IIS_grottbjorn
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new MethodFilter());
            filters.Add(new HandleErrorAttribute());
        }
    }
}