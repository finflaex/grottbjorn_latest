﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using fxGDB_GrottBjorn.Base;
using fxSupport.fdMsSQL.fsdOther;
using fxSupport.fdMsSQL.fsdTable;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace finflaex_IIS_grottbjorn
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Подключите здесь службу электронной почты для отправки сообщения электронной почты.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Подключите здесь службу SMS, чтобы отправить текстовое сообщение.
            return Task.FromResult(0);
        }
    }

    // Настройка диспетчера пользователей приложения. UserManager определяется в ASP.NET Identity и используется приложением.
    public class ApplicationUserManager : fxUserManager<GDB>
    {
        public ApplicationUserManager(fxUserStore<GDB> store = null) : base(store ?? new fxUserStore<GDB>())
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options,
            IOwinContext context)
        {
            var manager = new ApplicationUserManager();
            // Настройка логики проверки имен пользователей
            manager.UserValidator = new UserValidator<User, Guid>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Настройка логики проверки паролей
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false
            };

            // Настройка параметров блокировки по умолчанию
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Регистрация поставщиков двухфакторной проверки подлинности. Для получения кода проверки пользователя в данном приложении используется телефон и сообщения электронной почты
            // Здесь можно указать собственный поставщик и подключить его.
            //manager.RegisterTwoFactorProvider("Код, полученный по телефону", new PhoneNumberTokenProvider<User>
            //{
            //    MessageFormat = "Ваш код безопасности: {0}"
            //});
            //manager.RegisterTwoFactorProvider("Код из сообщения", new EmailTokenProvider<User>
            //{
            //    Subject = "Код безопасности",
            //    BodyFormat = "Ваш код безопасности: {0}"
            //});
            //manager.EmailService = new EmailService();
            //manager.SmsService = new SmsService();
            //var dataProtectionProvider = options.DataProtectionProvider;
            //if (dataProtectionProvider != null)
            //{
            //    manager.UserTokenProvider = new DataProtectorTokenProvider<User>(
            //        dataProtectionProvider.Create("ASP.NET Identity"));
            //}
            return manager;
        }
    }

    // Настройка диспетчера входа для приложения.
    public class ApplicationSignInManager : fxSignInManager<GDB>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        //    public override Task<ClaimsIdentity> CreateUserIdentityAsync(User user)
        //    {
        //        return user.GenerateUserIdentityAsync((ApplicationUserManager) UserManager);
        //    }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options,
            IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }

        //    public Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
        //    {
        //        return Task.Run(() => UserManager.FindByIdAsync(user.Id).Result.LockOutEnd);
        //    }

        //    public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
        //    {
        //        return Task.Run(() =>
        //        {
        //            var read = UserManager.FindByIdAsync(user.Id).Result;
        //            read.LockOutEnd = lockoutEnd;
        //            UserManager.UpdateAsync(read);
        //        });
        //    }

        //    public Task<int> IncrementAccessFailedCountAsync(User user)
        //    {
        //        return Task.Run(() =>
        //        {
        //            var read = UserManager.FindByIdAsync(user.Id).Result;
        //            var result = ++read.AccessFailedCount;
        //            UserManager.UpdateAsync(read);
        //            return result;
        //        });
        //    }

        //    public Task ResetAccessFailedCountAsync(User user)
        //    {
        //        return Task.Run(() =>
        //        {
        //            var read = UserManager.FindByIdAsync(user.Id).Result;
        //            read.AccessFailedCount = 0;
        //            return UserManager.UpdateAsync(read);
        //        });
        //    }

        //    public Task<int> GetAccessFailedCountAsync(User user)
        //    {
        //        return Task.Run(() => UserManager.FindByIdAsync(user.Id).Result.AccessFailedCount);
        //    }

        //    public Task<bool> GetLockoutEnabledAsync(User user)
        //    {
        //        return Task.Run(() => UserManager.FindByIdAsync(user.Id).Result.LockOutEnable);
        //    }

        //    public Task SetLockoutEnabledAsync(User user, bool enabled)
        //    {
        //        return Task.Run(() =>
        //        {
        //            var read = UserManager.FindByIdAsync(user.Id).Result;
        //            read.LockOutEnable = enabled;
        //            return UserManager.UpdateAsync(read);
        //        });
        //    }

        //    public Task CreateAsync(User user)
        //    {
        //        return UserManager.CreateAsync(user);
        //    }

        //    public Task UpdateAsync(User user)
        //    {
        //        return UserManager.UpdateAsync(user);
        //    }

        //    public Task DeleteAsync(User user)
        //    {
        //        return UserManager.DeleteAsync(user);
        //    }

        //    public Task<User> FindByIdAsync(string userId)
        //    {
        //        return UserManager.FindByIdAsync(userId);
        //    }

        //    public Task<User> FindByNameAsync(string userName)
        //    {
        //        return UserManager.FindByNameAsync(userName);
        //    }
    }
}