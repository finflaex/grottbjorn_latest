﻿/**
 * Created by finflaex on 07.08.2015.
 */
var ws;
var wsType;
//var wixcfg = [];

var connect_ws = false;

//setTimeout(ws_connect, 1000);
function ws_connect() {
    if (connect_ws) return;
    connect_ws = true;
    var uri = "ws://" + window.location.hostname + ":" + window.location.port + "/api/WebWs";
    ws = new WebSocket(uri);
    ws.onclose = function() {
        connect_ws = false;
        setTimeout(ws_connect, 1000);
    };
    ws.onerror = function(e) {
        connect_ws = false;
        //setTimeout(ws_connect, 1000);
        switch (e.currentTarget.readyState) {
        case 0:
            console.log("подключаемся");
            break;
        case 1:
            console.log("подключено");
            break;
        case 2:
            console.log("закрываемся");
            break;
        case 3:
            console.log("закрыто");
            break;
        default:
            break;
        }
    };
    ws.onopen = function() {
        var data = {};
        data.c = "start";
        data.session = lib.cookie_get("session");
        wsSend(data);
    };
    ws.onmessage = function(e) {
        "use strict";
        try {
            switch (wsType) {
            case "script":
                eval(e.data);
                wsType = null;
                break;
            default:
                wsRunJson(e.data);
                break;
            }
        } catch (err) {
            console.log(e.data);
            console.log(err.message);
        }
    };
}

function waitForSocketConnection(socket, callback) {
    setTimeout(
        function() {
            if (socket.readyState === 1) {
                if (callback !== undefined) {
                    callback();
                }
                return;
            } else {
                waitForSocketConnection(socket, callback);
            }
        }, 5);
};

function wsSend(data) {
    if (typeof (data) == "string") {
        data = JSON.parse(data);
    }
    data = JSONConverter(data);
    data = JSON.stringify(data, function(key, value) {
        if (key === "window"
                || key === "collection"
        ) return undefined;
        return value;
    });

    waitForSocketConnection(ws, function() {
        ws.send(data);
    });
};

function wsRunJson(v) {

    v = JSONConverter(JSON.parse(v));
    //v = JSON.parse(v);

    if (v.c) {
        switch (v.c) {
        case "jqClear":
            $(v.t).empty();
            return;
        case "jqLoad":
            $.post(v.p, JSON.stringify(v.d), function(e) {
                $(v.t).html(e);
            });
            return;
        case "setCookie":
            lib.cookie_set(v.n, v.v);
            return;
        case "setStyle":
            var style = $$("combo_style");
            if (style) style.setValue(v.v);
            $("#style").attr("href", "/Content/skin/" + v.v + ".css");
            return;

        case "wixMessage":
            webix.message(v.m);
            return;
        case "wixRun":
            var wixTarget = $$(v.t);
            wixTarget[v.m].apply(wixTarget, v.p);
            return;
        case "wixEvent":
            var wixEvent = $$(v.t);
            wixEvent.attachEvent(v.e, function() {
                var data = {};
                data.c = "event";
                data.g = v.t;
                data.jd = JSON.stringify(arguments);
                data.t = v.e;
                wsSend(data);
                return v.f;
            });
            return;
        case "jqRun":
            var jqTarget = $(v.t);
            jqTarget[v.m].apply(jqTarget, v.p);
            return;
        case "webix":
            webix.ui(v.d);
            return;
        case "webixReplace":
            webix.ui(v.d, $$(v.t));
            return;
        case "webixAnimateReplace":
            webix.ui.animate(v.d, $$(v.t), v.cfg);
            return;
        default:
            return;
        }
    }
}

function JSONConverter(val) {
    if (val === null) {
        return null;
    } else if (typeof val === "object") {
        if (val.Base64) {
            var read = Base64.decode(val.data);
            switch (val.Base64) {
            case "string":
                return read;
            case "script":
                return eval(read);
            case "function":
                return lib.function(read);
            }
        } else {
            for (var prop in val)
                if (val.hasOwnProperty(prop))
                    val[prop] = JSONConverter(val[prop]);
        }
    } else if (Array.isArray(val))
    // ReSharper disable once QualifiedExpressionMaybeNull
        for (var i = 0; i < val.length; i++)
            val[i] = JSONConverter(val[i]);
    return val;
}

function Complited() {
    wsSend({
        c: "complited"
    });
}