//[Skin Customization]
webix.skin.clouds.barHeight = 36;
webix.skin.clouds.tabbarHeight = 46;
webix.skin.clouds.rowHeight = 34;
webix.skin.clouds.listItemHeight = 32;
webix.skin.clouds.inputHeight = 30;
webix.skin.clouds.layoutMargin.wide = 4;
webix.skin.clouds.layoutMargin.space = 10;
webix.skin.clouds.layoutPadding.space = 10;
webix.skin.set("clouds");