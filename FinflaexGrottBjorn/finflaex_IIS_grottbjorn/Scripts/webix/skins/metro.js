//[Skin Customization]
webix.skin.metro.barHeight = 36;
webix.skin.metro.tabbarHeight = 46;
webix.skin.metro.rowHeight = 32;
webix.skin.metro.listItemHeight = 32;
webix.skin.metro.inputHeight = 30;
webix.skin.metro.layoutMargin.wide = 4;
webix.skin.metro.layoutMargin.space = 10;
webix.skin.metro.layoutPadding.space = 10;
webix.skin.set("metro");