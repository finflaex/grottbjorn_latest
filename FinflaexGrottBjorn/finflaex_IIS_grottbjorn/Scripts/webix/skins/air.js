//[Skin Customization]
webix.skin.air.barHeight = 34;
webix.skin.air.tabbarHeight = 36;
webix.skin.air.rowHeight = 34;
webix.skin.air.listItemHeight = 28;
webix.skin.air.inputHeight = 34;
webix.skin.air.layoutMargin.wide = 4;
webix.skin.air.layoutMargin.space = 10;
webix.skin.air.layoutPadding.space = 10;
webix.skin.set("air");