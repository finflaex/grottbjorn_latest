//[Skin Customization]
webix.skin.glamour.barHeight = 39;
webix.skin.glamour.tabbarHeight = 39;
webix.skin.glamour.rowHeight = 32;
webix.skin.glamour.listItemHeight = 32;
webix.skin.glamour.inputHeight = 34;
webix.skin.glamour.layoutMargin.wide = 15;
webix.skin.glamour.layoutMargin.space = 15;
webix.skin.glamour.layoutPadding.space = 15;
webix.skin.set("glamour");