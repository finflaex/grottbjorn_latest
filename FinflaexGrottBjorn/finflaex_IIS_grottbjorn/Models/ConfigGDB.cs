﻿using System.Data.Entity.Migrations;
using finflaex_IIS_grottbjorn.Models.DBEF;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace finflaex_IIS_grottbjorn.Models
{
    internal class ConfigGDB : DbMigrationsConfiguration<GDB>
    {
        public ConfigGDB()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(GDB context)
        {
            if (!context.Database.Exists())
            {
                context.Database.Create();
            }

            var userManager = new ApplicationUserManager(new UserStore<User>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // создаем две роли
            var role1 = new IdentityRole {Name = "admin"};
            var role2 = new IdentityRole {Name = "user"};

            // добавляем роли в бд
            roleManager.Create(role1);
            roleManager.Create(role2);

            // создаем пользователей
            var admin = new User {UserName = "finflaex@gmail.com", Email = "finflaex@gmail.com"};
            var password = "TestPass_123";
            var result = userManager.Create(admin, password);

            // если создание пользователя прошло успешно
            if (result.Succeeded)
            {
                // добавляем для пользователя роль
                userManager.AddToRole(admin.Id, role1.Name);
                userManager.AddToRole(admin.Id, role2.Name);
            }

            base.Seed(context);
        }
    }
}