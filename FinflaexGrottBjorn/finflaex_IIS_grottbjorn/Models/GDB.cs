﻿using System;
using System.Data.Entity;
using finflaex_IIS_grottbjorn.Models.DBEF;
using Microsoft.AspNet.Identity.EntityFramework;

namespace finflaex_IIS_grottbjorn.Models
{
    public partial class GDB : IdentityDbContext<User>
    {
        #region SYSTEM

        public GDB()
            : base("DefaultConnection", false)
        {
        }

        public static GDB Create()
        {
            return new GDB();
        }

        public static void Using(Action<GDB> action)
        {
            using (var db = Create())
            {
                action.Invoke(db);
            }
        }

        public static R Using<R>(Func<GDB, R> action)
        {
            using (var db = Create())
            {
                return action.Invoke(db);
            }
        }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region CLIENT

            modelBuilder
                .Entity<Client>()
                .HasMany(v => v.Contracts)
                .WithOptional(v => v.Client);

            #endregion

            #region CONTRACT

            modelBuilder
                .Entity<Contract>()
                .HasMany(v => v.RTS_PositionInstruments)
                .WithMany(v => v.Contracts);

            #endregion

            #region RTS_INSTRUMENT

            modelBuilder
                .Entity<RTSInstrument>()
                .HasMany(v => v.PositionInstruments)
                .WithRequired(v => v.Instrument);

            #endregion

            base.OnModelCreating(modelBuilder);
        }

        #region TABLES

        public DbSet<Client> Clients { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<RTSInstrument> RTSInstrument { get; set; }

        public DbSet<RTS_PositionInstrument> RTS_PositionInstruments { get; set; }

        #endregion 
    }
}