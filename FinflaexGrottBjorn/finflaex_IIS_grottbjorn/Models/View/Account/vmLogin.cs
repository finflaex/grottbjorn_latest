﻿using System.ComponentModel.DataAnnotations;

namespace finflaex_IIS_grottbjorn.Models.View.Account
{
    public class vmLogin
    {
        [Required]
        [Display(Name = "Логин")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }
    }
}