﻿using System.Collections.Generic;
using fxSupport.fdAttribute;
using fxSupport.fdMsSQL.fsdTable;

namespace finflaex_IIS_grottbjorn.Models.DBEF
{
    public class Contract : pcaHistory<Contract>
    {
        public Contract()
        {
            RTS_PositionInstruments = new HashSet<RTS_PositionInstrument>();
        }

        #region FIELDS

        [paHistory(Index = 0, Description = "Счет РТС")]
        public string FORTS_Account { get; set; }
        #endregion

        #region LINKS

        public Client Client { get; set; }
        public ICollection<RTS_PositionInstrument> RTS_PositionInstruments { get; set; }

        #endregion
    }
}