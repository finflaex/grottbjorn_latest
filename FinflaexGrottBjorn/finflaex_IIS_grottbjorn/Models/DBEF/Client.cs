﻿using System.Collections.Generic;
using fxSupport.fdMsSQL.fsdTable;

namespace finflaex_IIS_grottbjorn.Models.DBEF
{
    public class Client : pcaHistory<Client>
    {
        public Client()
        {
            Contracts = new HashSet<Contract>();
        }

        public ICollection<Contract> Contracts { get; set; }
    }
}