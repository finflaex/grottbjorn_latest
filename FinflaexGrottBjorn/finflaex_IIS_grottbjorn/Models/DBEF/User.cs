﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace finflaex_IIS_grottbjorn.Models.DBEF
{
    // Чтобы добавить данные профиля для пользователя, можно добавить дополнительные свойства в класс User. Дополнительные сведения см. по адресу: http://go.microsoft.com/fwlink/?LinkID=317594.
    public class User : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Здесь добавьте утверждения пользователя
            //userIdentity.AddClaim(new Claim(ClaimTypes.Gender, this.Gender));
            //userIdentity.AddClaim(new Claim("age", this.Age.ToString()));

            return userIdentity;
        }

        //public int Age { get; set; }
        //public string Gender { get; set; }
    }
}