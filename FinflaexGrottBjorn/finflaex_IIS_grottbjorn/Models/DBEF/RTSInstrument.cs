﻿using System;
using System.Collections.Generic;
using fxSupport.fdAttribute;
using fxSupport.fdMsSQL.fsdTable;

namespace finflaex_IIS_grottbjorn.Models.DBEF
{
    public class RTSInstrument : pcaAutoGuid
    {
        public RTSInstrument()
        {
            PositionInstruments = new HashSet<RTS_PositionInstrument>();
        }

        #region FIELDS

        [paHistory(Index = 0, Description = "Код")]
        public string Code { get; set; }

        [paHistory(Index = 1, Description = "Краткое название")]
        public string ShortName { get; set; }

        [paHistory(Index = 2, Description = "Дата погашения")]
        public DateTime MaturityDate { get; set; }

        #endregion

        #region LINKS

        public ICollection<RTS_PositionInstrument> PositionInstruments { get; set; }

        #endregion
    }
}