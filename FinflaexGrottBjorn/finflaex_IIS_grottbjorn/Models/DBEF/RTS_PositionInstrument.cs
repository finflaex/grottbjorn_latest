﻿using System.Collections.Generic;
using fxSupport.fdAttribute;
using fxSupport.fdMsSQL.fsdTable;

namespace finflaex_IIS_grottbjorn.Models.DBEF
{
    public class RTS_PositionInstrument : pcaHistory<RTS_PositionInstrument>
    {
        public RTS_PositionInstrument()
        {
            Contracts = new HashSet<Contract>();
        }

        #region FIELDS

        [paHistory(Index = 0, Description = "Входящая чистая позиция")]
        public double PartFreePosition { get; set; }

        [paHistory(Index = 1, Description = "Текущая длинная позиция")]
        public double CurrentLongPosition { get; set; }

        [paHistory(Index = 2, Description = "Текущая короткая позиция")]
        public double CurrentShortPosition { get; set; }

        [paHistory(Index = 3, Description = "Текущая чистая позиция")]
        public double CurrentFreePosition { get; set; }

        [paHistory(Index = 4, Description = "Активная покупка")]
        public double ActiveBuying { get; set; }

        [paHistory(Index = 5, Description = "Активная продажа")]
        public double ActiveSales { get; set; }
        #endregion

        #region LINKS

        public RTSInstrument Instrument { get; set; }
        public ICollection<Contract> Contracts { get; set; }

        #endregion
    }
}