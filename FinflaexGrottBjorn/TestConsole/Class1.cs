﻿
/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class Report
{

    private ReportGroupTable groupTableField;

    private ReportTable12 table12Field;

    private ReportTable13 table13Field;

    private ReportTable14 table14Field;

    private ReportTable15 table15Field;

    private string reportNameField;

    private string dateFromField;

    private string dateToField;

    private string clientCodeField;

    private string clientNameField;

    private string clientDogovorField;

    /// <remarks/>
    public ReportGroupTable GroupTable
    {
        get
        {
            return this.groupTableField;
        }
        set
        {
            this.groupTableField = value;
        }
    }

    /// <remarks/>
    public ReportTable12 Table12
    {
        get
        {
            return this.table12Field;
        }
        set
        {
            this.table12Field = value;
        }
    }

    /// <remarks/>
    public ReportTable13 Table13
    {
        get
        {
            return this.table13Field;
        }
        set
        {
            this.table13Field = value;
        }
    }

    /// <remarks/>
    public ReportTable14 Table14
    {
        get
        {
            return this.table14Field;
        }
        set
        {
            this.table14Field = value;
        }
    }

    /// <remarks/>
    public ReportTable15 Table15
    {
        get
        {
            return this.table15Field;
        }
        set
        {
            this.table15Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string ReportName
    {
        get
        {
            return this.reportNameField;
        }
        set
        {
            this.reportNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string DateFrom
    {
        get
        {
            return this.dateFromField;
        }
        set
        {
            this.dateFromField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string DateTo
    {
        get
        {
            return this.dateToField;
        }
        set
        {
            this.dateToField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string ClientCode
    {
        get
        {
            return this.clientCodeField;
        }
        set
        {
            this.clientCodeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string ClientName
    {
        get
        {
            return this.clientNameField;
        }
        set
        {
            this.clientNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string ClientDogovor
    {
        get
        {
            return this.clientDogovorField;
        }
        set
        {
            this.clientDogovorField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportGroupTable
{

    private ReportGroupTableTable5 table5Field;

    private string groupTableNameField;

    /// <remarks/>
    public ReportGroupTableTable5 Table5
    {
        get
        {
            return this.table5Field;
        }
        set
        {
            this.table5Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string GroupTableName
    {
        get
        {
            return this.groupTableNameField;
        }
        set
        {
            this.groupTableNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportGroupTableTable5
{

    private ReportGroupTableTable5Row[] rowField;

    private string tableNameField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Row")]
    public ReportGroupTableTable5Row[] Row
    {
        get
        {
            return this.rowField;
        }
        set
        {
            this.rowField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string TableName
    {
        get
        {
            return this.tableNameField;
        }
        set
        {
            this.tableNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportGroupTableTable5Row
{

    private string tradeDateField;

    private string tradeTimeField;

    private string settleDateField;

    private string tradeNoField;

    private string securityNameField;

    private string tradeTypeField;

    private string buySellField;

    private string priceField;

    private string currencyPriceField;

    private string quantityField;

    private string currencyPaymentField;

    private string amountPaymentCurrencyField;

    private string marketplaceField;

    private string exchCommField;

    private string brokerCommField;

    private string ndcField;

    private string clearingTypeField;

    private string clearingTypeNameField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string TradeDate
    {
        get
        {
            return this.tradeDateField;
        }
        set
        {
            this.tradeDateField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string TradeTime
    {
        get
        {
            return this.tradeTimeField;
        }
        set
        {
            this.tradeTimeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string SettleDate
    {
        get
        {
            return this.settleDateField;
        }
        set
        {
            this.settleDateField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string TradeNo
    {
        get
        {
            return this.tradeNoField;
        }
        set
        {
            this.tradeNoField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string SecurityName
    {
        get
        {
            return this.securityNameField;
        }
        set
        {
            this.securityNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string TradeType
    {
        get
        {
            return this.tradeTypeField;
        }
        set
        {
            this.tradeTypeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string BuySell
    {
        get
        {
            return this.buySellField;
        }
        set
        {
            this.buySellField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string Price
    {
        get
        {
            return this.priceField;
        }
        set
        {
            this.priceField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string CurrencyPrice
    {
        get
        {
            return this.currencyPriceField;
        }
        set
        {
            this.currencyPriceField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string Quantity
    {
        get
        {
            return this.quantityField;
        }
        set
        {
            this.quantityField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string CurrencyPayment
    {
        get
        {
            return this.currencyPaymentField;
        }
        set
        {
            this.currencyPaymentField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string AmountPaymentCurrency
    {
        get
        {
            return this.amountPaymentCurrencyField;
        }
        set
        {
            this.amountPaymentCurrencyField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string Marketplace
    {
        get
        {
            return this.marketplaceField;
        }
        set
        {
            this.marketplaceField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string ExchComm
    {
        get
        {
            return this.exchCommField;
        }
        set
        {
            this.exchCommField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string BrokerComm
    {
        get
        {
            return this.brokerCommField;
        }
        set
        {
            this.brokerCommField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string Ndc
    {
        get
        {
            return this.ndcField;
        }
        set
        {
            this.ndcField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string ClearingType
    {
        get
        {
            return this.clearingTypeField;
        }
        set
        {
            this.clearingTypeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string ClearingTypeName
    {
        get
        {
            return this.clearingTypeNameField;
        }
        set
        {
            this.clearingTypeNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportTable12
{

    private ReportTable12Row[] rowField;

    private string tableNameField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Row")]
    public ReportTable12Row[] Row
    {
        get
        {
            return this.rowField;
        }
        set
        {
            this.rowField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string TableName
    {
        get
        {
            return this.tableNameField;
        }
        set
        {
            this.tableNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportTable12Row
{

    private string operationDateField;

    private string operationNameField;

    private string currencyNameField;

    private string amountInField;

    private string amountOutField;

    private string detailsField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string OperationDate
    {
        get
        {
            return this.operationDateField;
        }
        set
        {
            this.operationDateField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string OperationName
    {
        get
        {
            return this.operationNameField;
        }
        set
        {
            this.operationNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string CurrencyName
    {
        get
        {
            return this.currencyNameField;
        }
        set
        {
            this.currencyNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string AmountIn
    {
        get
        {
            return this.amountInField;
        }
        set
        {
            this.amountInField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string AmountOut
    {
        get
        {
            return this.amountOutField;
        }
        set
        {
            this.amountOutField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string Details
    {
        get
        {
            return this.detailsField;
        }
        set
        {
            this.detailsField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportTable13
{

    private ReportTable13Row[] rowField;

    private string tableNameField;

    private string rateInfoField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Row")]
    public ReportTable13Row[] Row
    {
        get
        {
            return this.rowField;
        }
        set
        {
            this.rowField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string TableName
    {
        get
        {
            return this.tableNameField;
        }
        set
        {
            this.tableNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string RateInfo
    {
        get
        {
            return this.rateInfoField;
        }
        set
        {
            this.rateInfoField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportTable13Row
{

    private string marketplaceField;

    private string dateField;

    private string currencyNameField;

    private string balanceOnsetField;

    private string pullbackOnsetField;

    private string marginOnsetField;

    private string amountFreeOnsetField;

    private string amountInField;

    private string amountOutField;

    private string balanceOutsetField;

    private string pullbackOutsetField;

    private string marginOutsetField;

    private string amountFreeOutsetField;

    private byte informInSumField;

    private decimal marketRateField;

    private string balanceRURField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string Marketplace
    {
        get
        {
            return this.marketplaceField;
        }
        set
        {
            this.marketplaceField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string Date
    {
        get
        {
            return this.dateField;
        }
        set
        {
            this.dateField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string CurrencyName
    {
        get
        {
            return this.currencyNameField;
        }
        set
        {
            this.currencyNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string BalanceOnset
    {
        get
        {
            return this.balanceOnsetField;
        }
        set
        {
            this.balanceOnsetField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string PullbackOnset
    {
        get
        {
            return this.pullbackOnsetField;
        }
        set
        {
            this.pullbackOnsetField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string MarginOnset
    {
        get
        {
            return this.marginOnsetField;
        }
        set
        {
            this.marginOnsetField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string AmountFreeOnset
    {
        get
        {
            return this.amountFreeOnsetField;
        }
        set
        {
            this.amountFreeOnsetField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string AmountIn
    {
        get
        {
            return this.amountInField;
        }
        set
        {
            this.amountInField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string AmountOut
    {
        get
        {
            return this.amountOutField;
        }
        set
        {
            this.amountOutField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string BalanceOutset
    {
        get
        {
            return this.balanceOutsetField;
        }
        set
        {
            this.balanceOutsetField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string PullbackOutset
    {
        get
        {
            return this.pullbackOutsetField;
        }
        set
        {
            this.pullbackOutsetField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string MarginOutset
    {
        get
        {
            return this.marginOutsetField;
        }
        set
        {
            this.marginOutsetField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string AmountFreeOutset
    {
        get
        {
            return this.amountFreeOutsetField;
        }
        set
        {
            this.amountFreeOutsetField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public byte InformInSum
    {
        get
        {
            return this.informInSumField;
        }
        set
        {
            this.informInSumField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public decimal MarketRate
    {
        get
        {
            return this.marketRateField;
        }
        set
        {
            this.marketRateField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string BalanceRUR
    {
        get
        {
            return this.balanceRURField;
        }
        set
        {
            this.balanceRURField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportTable14
{

    private ReportTable14Row[] rowField;

    private string tableNameField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Row")]
    public ReportTable14Row[] Row
    {
        get
        {
            return this.rowField;
        }
        set
        {
            this.rowField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string TableName
    {
        get
        {
            return this.tableNameField;
        }
        set
        {
            this.tableNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportTable14Row
{

    private string nameCommField;

    private string amountCommField;

    private string detailsField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string NameComm
    {
        get
        {
            return this.nameCommField;
        }
        set
        {
            this.nameCommField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string AmountComm
    {
        get
        {
            return this.amountCommField;
        }
        set
        {
            this.amountCommField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string Details
    {
        get
        {
            return this.detailsField;
        }
        set
        {
            this.detailsField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportTable15
{

    private ReportTable15Row[] rowField;

    private string tableNameField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Row")]
    public ReportTable15Row[] Row
    {
        get
        {
            return this.rowField;
        }
        set
        {
            this.rowField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string TableName
    {
        get
        {
            return this.tableNameField;
        }
        set
        {
            this.tableNameField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class ReportTable15Row
{

    private string securityNameField;

    private string securityFormField;

    private string securityTypeField;

    private string issuerNameField;

    private string regNumField;

    private string transhField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string SecurityName
    {
        get
        {
            return this.securityNameField;
        }
        set
        {
            this.securityNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string SecurityForm
    {
        get
        {
            return this.securityFormField;
        }
        set
        {
            this.securityFormField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string SecurityType
    {
        get
        {
            return this.securityTypeField;
        }
        set
        {
            this.securityTypeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string IssuerName
    {
        get
        {
            return this.issuerNameField;
        }
        set
        {
            this.issuerNameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string RegNum
    {
        get
        {
            return this.regNumField;
        }
        set
        {
            this.regNumField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string Transh
    {
        get
        {
            return this.transhField;
        }
        set
        {
            this.transhField = value;
        }
    }
}

