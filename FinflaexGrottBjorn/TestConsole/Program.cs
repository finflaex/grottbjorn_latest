﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using fxSupport.fdAttribute;
using fxSupport.fdLogger;
using fxSupport.fdMsSQL.fsdBase;
using fxSupport.fdMsSQL.fsdTable;
using fxSupport.fdReflection;

namespace TestConsole
{
    interface IInfoClass
    {
        double Sum();
        void Info();
        void Set(double d1, double d2);

        string Qwe { get; }
        string Asd { set; }
        string Zxc { get; set; }
    }

    // Тестовый класс, содержащий некоторые конструкции
    class MyTestClass : IInfoClass
    {
        public double v1;
        public double v2;

        public MyTestClass(double v1, double v2)
        {
            Set(v1,v2);
        }

        public string Asd { set {} }

        public string Qwe { get; }

        public string Zxc { get; set; }

        public void Info()
        {
            Console.WriteLine("v1 = " + v1);
            Console.WriteLine("v2 = " + v2);
        }

        public void Set(double d1, double d2)
        {
            v1 = d1;
            v2 = d2;
        }

        public double Sum()
        {
            return v1 + v2;
        }
    }

    // В данном классе определены методы использующие рефлексию
    static class Reflect
    {
        // Информация о полях и реализуемых интерфейсах 
        public static void FieldInterfaceInfo<T>() where T : class
        {
            Type t = typeof(T);
            Console.WriteLine("\n*** Реализуемые интерфейсы ***\n");
            var im = t.GetInterfaces();
            foreach (Type tp in im)
                Console.WriteLine("--> " + tp.Name);
            Console.WriteLine("\n*** Поля и свойства ***\n");
            FieldInfo[] fieldNames = t.GetFields();
            foreach (FieldInfo fil in fieldNames)
                Console.Write("--> " + fil.FieldType.Name + " " + fil.Name + "\n");
            PropertyInfo[] props = t.GetProperties();
            foreach (PropertyInfo prop in props)
                Console.Write("--> " + prop.PropertyType.Name + " " + prop.Name
                              +
                              new[]
                              {
                                  "{",
                                  (prop.GetMethod == null ? string.Empty : "get;"),
                                  (prop.SetMethod == null ? string.Empty : "set;"),
                                  "}"
                              }
                              .Where(v=>!string.IsNullOrEmpty(v))
                              .Join(" ")
                              + "\n");

            // Получаем коллекцию методов
            MethodInfo[] MArr = t
                .GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public)
                .SkipWhile(v=> v.Name.StartsWith("get_") || v.Name.StartsWith("set_"))
                .ToArray();
            Console.WriteLine("*** Список методов класса {0} ***\n", t.FullName);

            // Вывести методы
            foreach (MethodInfo m in MArr)
            {
                Console.Write(" --> " + m.ReturnType.Name + " \t" + m.Name + "(");
                // Вывести параметры методов
                ParameterInfo[] p = m.GetParameters();
                for (int i = 0; i < p.Length; i++)
                {
                    Console.Write(p[i].ParameterType.Name + " " + p[i].Name);
                    if (i + 1 < p.Length) Console.Write(", ");
                }
                Console.Write(")\n");
            }
        }
    }

    class Program
    {
        static void Main()
        {
            Reflect.FieldInterfaceInfo<MyTestClass>();

            Console.ReadLine();
        }
    }
}