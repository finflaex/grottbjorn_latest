﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fxGDB_GrottBjorn.Base;
using fxGDB_GrottBjorn.Tables;
using fxSupport.fdDDE;
using fxSupport.fdLogger;
using fxSupport.fdMsSQL.fsdOther;
using fxSupport.fdReflection;

namespace ServerQuikDDE
{
    public partial class FormServerDde : Form
    {
        private pcDdeServer server;

        public FormServerDde()
        {
            InitializeComponent();
        }

        private void FormServerDde_Shown(object sender, EventArgs e)
        {
            server = new pcDdeServer("QuikServerDDE", (table, parser) =>
            {
                parser.Rows.For(row =>
                {
                    switch (table)
                    {
                        case nameof(RTS_LimitCash):
                            parse_RTS_LimitCash(parser);
                            break;
                        case nameof(RTS_PositionInstrument):
                            parse_RTS_PositionInstrument(parser);
                            break;
                        case nameof(VRRDM_Limit):
                            parse_CM_LimitCash(parser);
                            break;
                        case nameof(VRRDM_FreeCash):
                            parse_CM_FreeCash(parser);
                            break;
                        case nameof(Quik_Trade):
                            parse_CM_Deal(parser);
                            break;
                        case nameof(Quik_StopOrder):
                            parse_CM_StopApplication(parser);
                            break;
                        case nameof(Quik_Order):
                            parse_CM_Application(parser);
                            break;
                        case nameof(MMVB_Limit):
                            parse_SM_LimitCash(parser);
                            break;
                        case nameof(MMVB_Portfolio):
                            parse_SM_Portfolio(parser);
                            break;
                        case nameof(MMVB_Security):
                            parse_SM_Security(parser);
                            break;
                        default:
                            break;
                    }
                });
            });
        }

        private void parse_SM_Security(pcDdeParser parser)
        {
            if (ReadHeader(parser)) return;

            var PaperName = parser.ReadString();
            var Contract = parser.ReadString();
            var InputBalance = parser.ReadFloat();
            var CurrentBalance = parser.ReadFloat();
            var Summ = parser.ReadFloat();
            var Locked = parser.ReadFloat();
            var Type = parser.ReadString();

            GDB.SaveHistory(new MMVB_Security()
            {
                CurrentBalance = CurrentBalance,
                InputBalance = InputBalance,
                Locked = Locked,
                PaperName = PaperName,
                Summ = Summ,
                Type = Type,
                Account = Contract,
            },
            (db, value) =>
            {
                var contract = db.Contracts.FirstOrDefault(v => v.Account == Contract)
                    ?? db.Contracts.Add(new Contract
                    {
                        Account = Contract
                    });
                value.Contract = contract;

                return contract.SM_Portfolio?.Guid;
            });
        }

        private void parse_SM_Portfolio(pcDdeParser parser)
        {
            if (ReadHeader(parser)) return;

            var Contract = parser.ReadString();
            var Input = parser.ReadFloat();
            var Price = parser.ReadFloat();
            var LockBuy = parser.ReadFloat();
            var OnBuy = parser.ReadFloat();
            var MinMargin = parser.ReadFloat();
            var StartMargin = parser.ReadFloat();
            var SpeedMargin = parser.ReadFloat();
            var Status = parser.ReadString();
            var Type = parser.ReadString();

            GDB.SaveHistory(new MMVB_Portfolio()
            {
                Input = Input,
                LockBuy = LockBuy,
                MinMargin = MinMargin,
                OnBuy = OnBuy,
                Price = Price,
                SpeedMargin = SpeedMargin,
                StartMargin = StartMargin,
                Status = Status,
                Type = Type,
                Account = Contract,
            },
            (db, value) =>
            {
                var contract = db.Contracts.FirstOrDefault(v => v.Account == Contract)
                    ?? db.Contracts.Add(new Contract
                    {
                        Account = Contract
                    });
                value.Contract = contract;

                return contract.SM_Portfolio?.Guid;
            });
        }

        private void parse_SM_LimitCash(pcDdeParser parser)
        {
            if (ReadHeader(parser)) return;

            var Currency = parser.ReadString();
            var Group = parser.ReadString();
            var Contract = parser.ReadString();
            var Type = parser.ReadString();
            var InputBalance = parser.ReadFloat();
            var CurrentBalance = parser.ReadFloat();
            var Locked = parser.ReadFloat();
            var Balance = parser.ReadFloat();

            GDB.SaveHistory(new MMVB_Limit()
            {
                Balance = Balance,
                Currency = Currency,
                CurrentBalance = CurrentBalance,
                Group = Group,
                InputBalance = InputBalance,
                Locked = Locked,
                Type = Type,
                Account = Contract,
            },
            (db, value) =>
            {
                var contract = db.Contracts.FirstOrDefault(v => v.Account == Contract)
                    ?? db.Contracts.Add(new Contract
                    {
                        Account = Contract
                    });
                value.Contract = contract;

                return contract
                    .SM_LimitCash
                    .FirstOrDefault(v
                        => v.Type == Type
                           && v.sysStaus == peSysStatus.actual)?
                    .Guid;
            });
        }

        private void parse_CM_Application(pcDdeParser parser)
        {
            if (ReadHeader(parser)) return;

            var Number = parser.ReadLong();
            var StartTime = parser.ReadDateTime();
            var StopTime = parser.ReadDateTime();
            var Paper = parser.ReadString();
            var Operation = parser.ReadString();
            var Price = parser.ReadFloat();
            var Count = parser.ReadInt();
            var Balance = parser.ReadInt();
            var Volume = parser.ReadFloat();
            var Contract = parser.ReadString();

            GDB.SaveHistory(new Quik_Order()
            {
                Balance = Balance,
                Count = Count,
                Number = Number,
                Operation = Operation,
                Paper = Paper,
                Price = Price,
                StartTime = StartTime,
                StopTime = StopTime,
                Volume = Volume,
                Account = Contract,
            },
            (db, value) =>
            {
                var contract = db.Contracts.FirstOrDefault(v => v.Account == Contract)
                               ?? db.Contracts.Add(new Contract
                               {
                                   Account = Contract
                               });
                value.Contract = contract;

                return contract
                    .CM_Applications
                    .FirstOrDefault(v
                        => v.Number == Number
                           && v.sysStaus == peSysStatus.actual)?
                    .Guid;
            });
        }

        private void parse_CM_FreeCash(pcDdeParser parser)
        {
            if (ReadHeader(parser)) return;

            var Contract = parser.ReadString().TrimEnd("_tx");
            var InputActive = parser.ReadFloat();
            var PortfolioValue = parser.ReadFloat();
            var BlockierBuy = parser.ReadFloat();
            var ToPurchase = parser.ReadFloat();
            var MinimumMargin = parser.ReadFloat();
            var StartMargin = parser.ReadFloat();
            var AmbulanceMargin = parser.ReadFloat();
            var Status = parser.ReadString();


            GDB.SaveHistory(new VRRDM_FreeCash()
            {
                AmbulanceMargin = AmbulanceMargin,
                BlockierBuy = BlockierBuy,
                InputActive = InputActive,
                PortfolioValue = PortfolioValue,
                MinimumMargin = MinimumMargin,
                StartMargin = StartMargin,
                Status = Status,
                ToPurchase = ToPurchase,
                Account = Contract,
            },
            (db, value) =>
            {
                var contract = db.Contracts.FirstOrDefault(v => v.Account == Contract)
                               ?? db.Contracts.Add(new Contract
                               {
                                   Account = Contract
                               });
                value.Contract = contract;

                return contract.CM_FreeCash?.Guid;
            });
        }

        private void parse_CM_LimitCash(pcDdeParser parser)
        {
            if (ReadHeader(parser)) return;

            var Group = parser.ReadString();
            var Contract = parser.ReadString().TrimEnd("_tx");
            var Currency = parser.ReadString();
            var InputBalance = parser.ReadFloat();
            var CurrentBalance = parser.ReadFloat();
            var Locked = parser.ReadFloat();
            var Balance = parser.ReadFloat();


            GDB.SaveHistory(new VRRDM_Limit()
            {
                Currency = Currency,
                Group = Group,
                Balance = Balance,
                CurrentBalance = CurrentBalance,
                InputBalance = InputBalance,
                Locked = Locked,
                Account = Contract,
            },
            (db, value) =>
            {
                var contract = db.Contracts.FirstOrDefault(v => v.Account == Contract)
                               ?? db.Contracts.Add(new Contract
                               {
                                   Account = Contract
                               });
                value.Contract = contract;

                return contract
                    .CM_LimitCash
                    .FirstOrDefault(v 
                        => v.Currency == Currency
                        && v.Group == Group
                        && v.sysStaus == peSysStatus.actual)?
                    .Guid;
            });
        }

        private void parse_RTS_LimitCash(pcDdeParser parser)
        {
            if (ReadHeader(parser)) return;
            var Contract = parser.ReadString();
            var PrevOpenLimit = parser.ReadFloat();
            var OpenLimit = parser.ReadFloat();
            var CurrentFree = parser.ReadFloat();
            var PlanFree = parser.ReadFloat();
            var VariationMargin = parser.ReadFloat();
            var AccruedIncome = parser.ReadFloat();
            var ExchangeFees = parser.ReadFloat();

            GDB.SaveHistory(new RTS_LimitCash()
            {
                AccruedIncome = AccruedIncome,
                CurrentFree = CurrentFree,
                ExchangeFees = ExchangeFees,
                OpenLimit = OpenLimit,
                PlanFree = PlanFree,
                PrevOpenLimit = PrevOpenLimit,
                VariationMargin = VariationMargin,
                Account = Contract,
            },
            (db, value) =>
            {
                var contract = db.Contracts.FirstOrDefault(v => v.FORTS_Account == Contract)
                               ?? db.Contracts.Add(new Contract
                               {
                                   FORTS_Account = Contract
                               });
                value.Contract = contract;

                return contract.RTS_LimitCash?.Guid;
            });
        }

        private bool ReadHeader(pcDdeParser parser)
        {
            var num = parser.ReadValue();
            if (num is string)
            {
                parser.Columns.Dec().For(() =>
                {
                    var read = parser.ReadValue();
                });
                return true;
            }
            return false;
        }

        private void parse_CM_StopApplication(pcDdeParser parser)
        {
            if (ReadHeader(parser)) return;

            var Number = parser.ReadLong();
            var Dealer = parser.ReadString();
            var DateTime = $"{parser.ReadString()} {parser.ReadString()}".ToDateTime();
            var DateTimeWithDrawal = $"{parser.ReadString()} {parser.ReadString()}".ToDateTime();
            var Type = parser.ReadString();
            var PaperShortName = parser.ReadString();
            var Operation = parser.ReadString();
            var PaperStopPrice = parser.ReadString();
            var DirectionStopPrice = parser.ReadString();
            var StopPrice = parser.ReadFloat();
            var DirectionStopLimit = parser.ReadString();
            var StopLimit = parser.ReadFloat();
            var Price = parser.ReadFloat();
            var MarketStopLimit = parser.ReadString();
            var Count = parser.ReadInt();
            var ActualCount = parser.ReadInt();
            var UsedCount = parser.ReadInt();
            var Contract = parser.ReadString();
            var NumberApplication = parser.ReadLong();
            var TermsOfADeal = parser.ReadLong();
            var Time = parser.ReadDateTime() ?? System.DateTime.MaxValue;
            var State = parser.ReadString();
            var Result = parser.ReadString();
            var RelatedApplication = parser.ReadLong();
            var RelatedApplicationPrice = parser.ReadFloat();
            var Error = parser.ReadFloat();
            var ProtectiveSpread = parser.ReadFloat();
            var MarketTakeProfit = parser.ReadString();
            var ApplicationCondition = parser.ReadLong();

            GDB.SaveHistory(new Quik_StopOrder()
            {
                Number = Number,
                Type = Type,
                DateTime = DateTime,
                Result = Result,
                ActualCount = ActualCount,
                ApplicationCondition = ApplicationCondition,
                Count = Count,
                DateTimeWithDrawal = DateTimeWithDrawal,
                Dealer = Dealer,
                DirectionStopLimit = DirectionStopLimit,
                DirectionStopPrice = DirectionStopPrice,
                Error = Error,
                MarketStopLimit = MarketStopLimit,
                MarketTakeProfit = MarketTakeProfit,
                NumberApplication = NumberApplication,
                Operation = Operation,
                PaperShortName = PaperShortName,
                PaperStopPrice = PaperStopPrice,
                Price = Price,
                ProtectiveSpread = ProtectiveSpread,
                RelatedApplication = RelatedApplication,
                RelatedApplicationPrice = RelatedApplicationPrice,
                State = State,
                StopLimit = StopLimit,
                StopPrice = StopPrice,
                TermsOfADeal = TermsOfADeal,
                Time = Time,
                UsedCount = UsedCount,
                Account = Contract,
            },
            (db, value) =>
            {
                var contract = db.Contracts.FirstOrDefault(v => v.Account == Contract)
                        ?? db.Contracts.Add(new Contract()
                        {
                            Account = Contract
                        });

                value.Contract = contract;

                return contract
                    .CM_StopApplications
                    .FirstOrDefault(v 
                        => v.Number == Number
                        && v.sysStaus == peSysStatus.actual)?
                    .Guid;
            });
        }

        private static void TestColumn(pcDdeParser parser)
        {
            var num = parser.ReadValue();
            if (num is string)
            {
                Console.WriteLine("========== start ================");
                parser.Columns.Dec().For(index =>
                {
                    Console.WriteLine($"[paHistory({index}, \"{parser.ReadString()}\")]");
                });
                Console.WriteLine("============ end ================");
            }
            else
            {
                if (num.asType<double>().Compare(1d))
                {
                    Console.WriteLine("========== start ================");
                    parser.Columns.Dec().For(index =>
                    {
                        var read = parser.ReadValue();
                        Console.WriteLine($"public {read.GetType().Name.ToLower()} value{index} {"{ get; set; }"}");
                    });
                    Console.WriteLine("============ end ================");
                }
                else
                {
                    parser.Columns.Dec().For(() =>
                    {
                        parser.ReadValue();
                    });
                }
            }
        }

        private void parse_RTS_PositionInstrument(pcDdeParser parser)
        {
            if (ReadHeader(parser)) return;

            var Contract = parser.ReadString(); 
            var Code = parser.ReadString();
            var ShortName = parser.ReadString();
            var MaturityDate = parser.ReadDateTime();
            var PartFreePosition = parser.ReadFloat();
            var CurrentLongPosition = parser.ReadFloat();
            var CurrentShortPosition = parser.ReadFloat();
            var CurrentFreePosition = parser.ReadFloat();
            var ActiveBuying = parser.ReadFloat();
            var ActiveSales = parser.ReadFloat();

            GDB.SaveHistory(new RTS_PositionInstrument()
            {
                ActiveBuying = ActiveBuying,
                ActiveSales = ActiveSales,
                CurrentFreePosition = CurrentFreePosition,
                CurrentLongPosition = CurrentLongPosition,
                CurrentShortPosition = CurrentShortPosition,
                PartFreePosition = PartFreePosition,
                Account = Contract,
                InstrumentName = ShortName,
            },
            (db, value) =>
            {
                var instrument = db.RtsInstruments.FirstOrDefault(v => v.ShortName == ShortName)
                                    ?? db.RtsInstruments.Add(new RTSInstrument
                                    {
                                        ShortName = ShortName,
                                        Code = Code,
                                        MaturityDate = MaturityDate
                                    });
                var contract = db.Contracts.FirstOrDefault(v => v.FORTS_Account == Contract)
                                ?? db.Contracts.Add(new Contract
                                {
                                    FORTS_Account = Contract
                                });

                value.Contract = contract;
                value.Instrument = instrument;

                return instrument
                    .PositionInstruments
                    .Intersect(contract.RTS_PositionInstruments)
                    .FirstOrDefault(v=>v.sysStaus == peSysStatus.actual)?
                    .Guid;
            });
        }

        private void parse_CM_Deal(pcDdeParser parser)
        {
            if (ReadHeader(parser)) return;

            var Number = parser.ReadLong();
            var DateTime = $"{parser.ReadString()} {parser.ReadString()}".ToDateTime();
            var ApplicationNumber = parser.ReadLong();
            var ShortName = parser.ReadString();
            var Operation = parser.ReadString();
            var Price = parser.ReadFloat();
            var Count = parser.ReadInt();
            var Volume = parser.ReadFloat();
            var Percent = parser.ReadFloat();
            var Comission = parser.ReadFloat();
            var SettlementDay = parser.ReadDateTime();
            var Contract = parser.ReadString();

            GDB.SaveHistory(new Quik_Trade()
            {
                Number = Number,
                ShortName = ShortName,
                ApplicationNumber = ApplicationNumber,
                Comission = Comission,
                Count = Count,
                DateTime = DateTime,
                Operation = Operation,
                Percent = Percent,
                Price = Price,
                SettlementDay = SettlementDay,
                Volume = Volume,
                Account = Contract,
            }, 
            (db, value) =>
            {
                var contract = db.Contracts.FirstOrDefault(v => v.Account == Contract)
                               ?? db.Contracts.Add(new Contract
                               {
                                   Account = Contract
                               });

                value.Contract = contract;

                return contract
                    .CM_Deals
                    .FirstOrDefault(v 
                        => v.Number == Number
                        && v.sysStaus == peSysStatus.actual)?
                    .Guid;
            });
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            server.Register();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            server.Unregister();
        }
    }
}

