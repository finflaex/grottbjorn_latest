﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestDevExpress.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        TestDevExpress.Models.ClientsEntities db = new TestDevExpress.Models.ClientsEntities();

        [ValidateInput(false)]
        public ActionResult GridView2Partial()
        {
            var model = db.Clients;
            return PartialView("_GridView2Partial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridView2PartialAddNew(TestDevExpress.Models.Client item)
        {
            var model = db.Clients;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridView2Partial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridView2PartialUpdate(TestDevExpress.Models.Client item)
        {
            var model = db.Clients;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.ClientID == item.ClientID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridView2Partial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridView2PartialDelete(System.Int32 ClientID)
        {
            var model = db.Clients;
            if (ClientID >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.ClientID == ClientID);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridView2Partial", model.ToList());
        }

        TestDevExpress.Models.ClientsEntities db2 = new TestDevExpress.Models.ClientsEntities();

        [ValidateInput(false)]
        public ActionResult DataViewPartial()
        {
            var model = db2.Clients;
            return PartialView("_DataViewPartial", model.ToList());
        }

        TestDevExpress.Models.ClientsEntities db3 = new TestDevExpress.Models.ClientsEntities();

        [ValidateInput(false)]
        public ActionResult CardView1Partial()
        {
            var model = db3.Clients;
            return PartialView("_CardView1Partial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CardView1PartialAddNew(TestDevExpress.Models.Client item)
        {
            var model = db3.Clients;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db3.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_CardView1Partial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult CardView1PartialUpdate(TestDevExpress.Models.Client item)
        {
            var model = db3.Clients;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.ClientID == item.ClientID);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db3.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_CardView1Partial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult CardView1PartialDelete(System.Int32 ClientID)
        {
            var model = db3.Clients;
            if (ClientID >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.ClientID == ClientID);
                    if (item != null)
                        model.Remove(item);
                    db3.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_CardView1Partial", model.ToList());
        }
    }
}