﻿using System;
using System.Collections.Generic;
using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class RTS_PositionInstrument : pcaHistory<RTS_PositionInstrument>
    {
        #region SYSTEM



        #endregion

        #region FIELDS

        [paHistory(0,"Входящая чистая позиция")]
        public float PartFreePosition { get; set; }

        [paHistory(1,"Текущая длинная позиция")]
        public float CurrentLongPosition { get; set; }

        [paHistory(2,"Текущая короткая позиция")]
        public float CurrentShortPosition { get; set; }

        [paHistory(3,"Текущая чистая позиция")]
        public float CurrentFreePosition { get; set; }

        [paHistory(4,"Активная покупка")]
        public float ActiveBuying { get; set; }

        [paHistory(5,"Активная продажа")]
        public float ActiveSales { get; set; }

        [paHistory(6, "Контракт")]
        public string Account { get; set; }

        [paHistory(7, "Инструмент")]
        public string InstrumentName { get; set; }
        #endregion

        #region LINKS

        public virtual RTSInstrument Instrument { get; set; }
        public virtual Contract Contract { get; set; }

        #endregion
    }
}