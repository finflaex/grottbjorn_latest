﻿using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class VRRDM_FreeCash : pcaHistory<VRRDM_FreeCash>
    {
        #region SYSTEM



        #endregion

        #region FIELDS

        [paHistory(0, "Вход.активы")]
        public float InputActive { get; set; }

        [paHistory(1, "Стоимость портфеля")]
        public float PortfolioValue { get; set; }

        [paHistory(2, "БлокПокупка")]
        public float BlockierBuy { get; set; }

        [paHistory(3, "На покупку")]
        public float ToPurchase { get; set; }

        [paHistory(4, "Мин.маржа")]
        public float MinimumMargin { get; set; }

        [paHistory(5, "Нач.маржа")]
        public float StartMargin { get; set; }

        [paHistory(6, "Скор.маржа")]
        public float AmbulanceMargin { get; set; }

        [paHistory(7, "Статус")]
        public string Status { get; set; }

        [paHistory(8, "Контракт")]
        public string Account { get; set; }

        #endregion

        #region LINKS

        public virtual Contract Contract { get; set; }

        #endregion
    }
}