﻿using System;
using System.Data;
using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class Quik_Order : pcaHistory<Quik_Order>
    {
        #region SYSTEM



        #endregion

        #region FIELDS

        [paHistory(0, "Номер")]
        public long Number { get; set; }

        [paHistory(1, "Выставлена (время)")]
        public DateTime? StartTime { get; set; }

        [paHistory(2, "Снята (время)")]
        public DateTime? StopTime { get; set; }

        [paHistory(3, "Бумага")]
        public string Paper { get; set; }

        [paHistory(4, "Операция")]
        public string Operation { get; set; }

        [paHistory(5, "Цена")]
        public float Price { get; set; }

        [paHistory(6, "Кол-во")]
        public int Count { get; set; }

        [paHistory(7, "Остаток")]
        public int Balance { get; set; }

        [paHistory(8, "Объем")]
        public float Volume { get; set; }
        [paHistory(9, "Контракт")]
        public string Account { get; set; }

        #endregion

        #region LINKS

        public virtual Contract Contract { get; set; }

        #endregion
    }
}