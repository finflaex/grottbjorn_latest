﻿using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class VRRDM_Limit : pcaHistory<VRRDM_Limit>
    {
        #region SYSTEM



        #endregion

        #region FIELDS

        [paHistory(0, "Группа")]
        public string Group { get; set; }

        [paHistory(2, "Валюта")]
        public string Currency { get; set; }

        [paHistory(3, "Входящий остаток")]
        public float InputBalance { get; set; }

        [paHistory(4, "Текущий остаток")]
        public float CurrentBalance { get; set; }

        [paHistory(5, "Заблокировано")]
        public float Locked { get; set; }

        [paHistory(6, "Баланс")]
        public float Balance { get; set; }

        [paHistory(7, "Контракт")]
        public string Account { get; set; }

        #endregion

        #region LINKS

        public virtual Contract Contract { get; set; }

        #endregion
    }
}