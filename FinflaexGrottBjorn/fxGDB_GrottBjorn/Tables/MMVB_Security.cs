﻿using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class MMVB_Security : pcaHistory<MMVB_Security>
    {
        #region SYSTEM



        #endregion

        #region FIELDS

        [paHistory(0, "Название бумаги")]
        public string PaperName { get; set; }

        [paHistory(1, "Входящий остаток")]
        public float InputBalance { get; set; }

        [paHistory(2, "Текущий остаток")]
        public float CurrentBalance { get; set; }

        [paHistory(3, "Всего")]
        public float Summ { get; set; }

        [paHistory(4, "Заблокировано")]
        public float Locked { get; set; }

        [paHistory(5, "Вид лимита")]
        public string Type { get; set; }

        [paHistory(6, "Контракт")]
        public string Account { get; set; }

        #endregion

        #region LINKS

        public virtual Contract Contract { get; set; }

        #endregion
    }
}