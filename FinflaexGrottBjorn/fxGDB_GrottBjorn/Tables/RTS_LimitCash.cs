﻿using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class RTS_LimitCash : pcaHistory<RTS_LimitCash>
    {
        #region SYSTEM



        #endregion

        #region FIELDS

        [paHistory(0, "Предыдущий лимит открытой позиции")]
        public float PrevOpenLimit { get; set; }

        [paHistory(1, "Лимит открытой позиции")]
        public float OpenLimit { get; set; }

        [paHistory(2, "Текущая чистая позиция")]
        public float CurrentFree { get; set; }

        [paHistory(3, "Плановая чистая позиция")]
        public float PlanFree { get; set; }

        [paHistory(4, "Вариационная маржа")]
        public float VariationMargin { get; set; }

        [paHistory(5, "Накопленный доход")]
        public float AccruedIncome { get; set; }

        [paHistory(6, "Биржевые сборы")]
        public float ExchangeFees { get; set; }
        [paHistory(7, "Контракт")]
        public string Account { get; set; }

        #endregion

        #region LINKS

        public virtual Contract Contract { get; set; }

        #endregion
    }
}