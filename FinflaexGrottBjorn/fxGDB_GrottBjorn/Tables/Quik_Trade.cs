﻿using System;
using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class Quik_Trade : pcaHistory<Quik_Trade>
    {
        #region SYSTEM

        #endregion

        #region FIELDS

        [paHistory(0, "Номер")]
        public long Number { get; set; }

        [paHistory(1, "Дата и время сделки")]
        public DateTime? DateTime { get; set; }

        [paHistory(2, "Заявка")]
        public long ApplicationNumber { get; set; }

        [paHistory(3, "Бумага сокр.")]
        public string ShortName { get; set; }

        [paHistory(4, "Операция")]
        public string Operation { get; set; }

        [paHistory(5, "Цена")]
        public float Price { get; set; }

        [paHistory(6, "Количество")]
        public int Count { get; set; }

        [paHistory(7, "Объем")]
        public float Volume { get; set; }

        [paHistory(8, "Купонный процент")]
        public float Percent { get; set; }

        [paHistory(9, "Комиссия")]
        public float Comission { get; set; }

        [paHistory(10, "Дата расчетов")]
        public DateTime? SettlementDay { get; set; }
        [paHistory(11, "Контракт")]
        public string Account { get; set; }

        #endregion

        #region LINKS

        public virtual Contract Contract { get; set; }

        #endregion
    }
}