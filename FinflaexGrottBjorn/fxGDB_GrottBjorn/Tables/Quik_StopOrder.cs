﻿using System;
using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class Quik_StopOrder : pcaHistory<Quik_StopOrder>
    {
        #region SYSTEM

        #endregion

        #region FIELDS

        [paHistory(0,"Номер")]
        public long Number { get; set; }

        [paHistory(1,"Диллер")]
        public string Dealer { get; set; }

        [paHistory(2,"Дата и время")]
        public DateTime? DateTime { get; set; }

        [paHistory(3,"Дата и время снятия")]
        public DateTime? DateTimeWithDrawal { get; set; }

        [paHistory(4, "Тип стоп заявки")]
        public string Type { get; set; }

        [paHistory(5,"Бумага сокр")]
        public string PaperShortName { get; set; }

        [paHistory(6, "Операция")]
        public string Operation { get; set; }

        [paHistory(7, "Бумага стоп-цены")]
        public string PaperStopPrice { get; set; }

        [paHistory(8,"Направление стоп-цены")]
        public string DirectionStopPrice { get; set; }

        [paHistory(9, "Стоп-цена")]
        public float StopPrice { get; set; }

        [paHistory(10, "Направление стоп-лимит цены")]
        public string DirectionStopLimit { get; set; }

        [paHistory(11, "Стоп-лимит цена")]
        public float StopLimit { get; set; }

        [paHistory(12,"Цена")]
        public float Price { get; set; }

        [paHistory(13, "Стоп-лимит по рыночной")]
        public string MarketStopLimit { get; set; }

        [paHistory(14, "Количество")]
        public int Count { get; set; }

        [paHistory(15,"Акт.количество")]
        public int ActualCount { get; set; }

        [paHistory(16,"Исп.количество")]
        public int UsedCount { get; set; }

        /// <summary>
        /// ////////
        /// </summary>
        [paHistory(17,"Номер заявки")]
        public long NumberApplication { get; set; }

        /// <summary>
        /// ///////
        /// </summary>
        [paHistory(18, "Сделка условия")]
        public long TermsOfADeal { get; set; }

        [paHistory(19, "Срок")]
        public DateTime? Time { get; set; }

        [paHistory(20,"Состояние")]
        public string State { get; set; }

        [paHistory(21, "Результат")]
        public string Result { get; set; }

        /// <summary>
        /// ////////
        /// </summary>
        [paHistory(22,"Связанная заявка")]
        public long RelatedApplication { get; set; }

        /// <summary>
        /// ////////
        /// </summary>
        [paHistory(23, "Цена связанной заявки")]
        public float RelatedApplicationPrice { get; set; }

        [paHistory(24, "Отступ от min/max")]
        public float Error { get; set; }

        [paHistory(25, "Защитный спред")]
        public float ProtectiveSpread { get; set; }

        [paHistory(26, "Тэйк-профит по рыночной")]
        public string MarketTakeProfit { get; set; }

        /// <summary>
        /// ////////
        /// </summary>
        [paHistory(27, "Заявка условия")]
        public long ApplicationCondition { get; set; }
        [paHistory(28, "Контракт")]
        public string Account { get; set; }

        #endregion

        #region LINKS

        public virtual Contract Contract { get; set; }

        #endregion
    }
}