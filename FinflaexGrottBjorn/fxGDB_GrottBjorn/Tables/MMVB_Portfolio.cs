﻿using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class MMVB_Portfolio : pcaHistory<MMVB_Portfolio>
    {
        #region SYSTEM



        #endregion

        #region FIELDS

        [paHistory(1, "Вход.активы")]
        public float Input { get; set; }
        [paHistory(2, "Стоимость портфеля")]
        public float Price { get; set; }
        [paHistory(3, "БлокПокупка")]
        public float LockBuy { get; set; }
        [paHistory(4, "На покупку")]
        public float OnBuy { get; set; }
        [paHistory(5, "Мин.маржа")]
        public float MinMargin { get; set; }
        [paHistory(6, "Нач.маржа")]
        public float StartMargin { get; set; }
        [paHistory(7, "Скор.маржа")]
        public float SpeedMargin { get; set; }
        [paHistory(8, "Статус")]
        public string Status { get; set; }
        [paHistory(9, "Вид лимита")]
        public string Type { get; set; }
        [paHistory(10, "Контракт")]
        public string Account { get; set; }

        #endregion

        #region LINKS

        public virtual Contract Contract { get; set; }

        #endregion
    }
}