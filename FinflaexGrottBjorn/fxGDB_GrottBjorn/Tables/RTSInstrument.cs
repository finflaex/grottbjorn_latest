﻿using System;
using System.Collections.Generic;
using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class RTSInstrument : pcaAutoGuid
    {
        #region SYSTEM

        public RTSInstrument()
        {
            PositionInstruments = new HashSet<RTS_PositionInstrument>();
        }

        #endregion

        #region FIELDS

        [paHistory(0,"Код")]
        public string Code { get; set; }

        [paHistory(1,"Краткое название")]
        public string ShortName { get; set; }

        [paHistory(2,"Дата погашения")]
        public DateTime? MaturityDate { get; set; }

        #endregion

        #region LINKS

        public virtual ICollection<RTS_PositionInstrument> PositionInstruments { get; set; }

        #endregion
    }
}