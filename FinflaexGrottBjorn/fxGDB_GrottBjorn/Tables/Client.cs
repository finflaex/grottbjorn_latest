﻿using System.Collections.Generic;
using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class Client : pcaAutoGuid
    {
        #region SYSTEM

        public Client()
        {
            Contracts = new HashSet<Contract>();
        }

        #endregion

        #region FIELDS



        #endregion

        #region LINKS

        public virtual ICollection<Contract> Contracts { get; set; }

        #endregion
    }
}