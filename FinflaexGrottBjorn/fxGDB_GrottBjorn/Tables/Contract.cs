﻿using System.Collections.Generic;
using fxSupport.fdMsSQL.fsdTable;

namespace fxGDB_GrottBjorn.Tables
{
    public class Contract : pcaAutoGuid
    {
        #region SYSTEM


        #endregion

        public Contract()
        {
            RTS_PositionInstruments = new HashSet<RTS_PositionInstrument>();
            CM_Deals = new HashSet<Quik_Trade>();
            CM_StopApplications = new HashSet<Quik_StopOrder>();
            CM_LimitCash = new HashSet<VRRDM_Limit>();
            SM_LimitCash = new HashSet<MMVB_Limit>();
        }

        #region FIELDS

        [paHistory(0,"Счет РТС")]
        public string FORTS_Account { get; set; }

        [paHistory(1,"Счет ФР/ВР")]
        public string Account { get; set; }

        #endregion

        #region LINKS

        public virtual Client Client { get; set; }
        public virtual RTS_LimitCash RTS_LimitCash { get; set; }
        public virtual VRRDM_FreeCash CM_FreeCash { get; set; }
        public virtual MMVB_Portfolio SM_Portfolio { get; set; }


        public virtual ICollection<RTS_PositionInstrument> RTS_PositionInstruments { get; set; }
        public virtual ICollection<Quik_Trade> CM_Deals { get; set; }
        public virtual ICollection<Quik_StopOrder> CM_StopApplications { get; set; }
        public virtual ICollection<VRRDM_Limit> CM_LimitCash { get; set; }
        public virtual ICollection<Quik_Order> CM_Applications { get; set; }
        public virtual ICollection<MMVB_Limit> SM_LimitCash { get; set; }
        public virtual ICollection<MMVB_Security> SM_Security { get; set; }

        #endregion
    }
}