namespace fxGDB_GrottBjorn.Base
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.sysGuid);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        FORTS_Account = c.String(),
                        Account = c.String(),
                        CM_FreeCash_sysGuid = c.Guid(),
                        RTS_LimitCash_sysGuid = c.Guid(),
                        SM_Portfolio_sysGuid = c.Guid(),
                        Client_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.VRRDM_FreeCash", t => t.CM_FreeCash_sysGuid)
                .ForeignKey("dbo.RTS_LimitCash", t => t.RTS_LimitCash_sysGuid)
                .ForeignKey("dbo.MMVB_Portfolio", t => t.SM_Portfolio_sysGuid)
                .ForeignKey("dbo.Clients", t => t.Client_sysGuid)
                .Index(t => t.CM_FreeCash_sysGuid)
                .Index(t => t.RTS_LimitCash_sysGuid)
                .Index(t => t.SM_Portfolio_sysGuid)
                .Index(t => t.Client_sysGuid);
            
            CreateTable(
                "dbo.Quik_Order",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Number = c.Long(nullable: false),
                        StartTime = c.DateTime(),
                        StopTime = c.DateTime(),
                        Paper = c.String(),
                        Operation = c.String(),
                        Price = c.Single(nullable: false),
                        Count = c.Int(nullable: false),
                        Balance = c.Int(nullable: false),
                        Volume = c.Single(nullable: false),
                        Account = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysActDT = c.DateTime(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                        Contract_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.Contracts", t => t.Contract_sysGuid)
                .Index(t => t.Contract_sysGuid);
            
            CreateTable(
                "dbo.Quik_Trade",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Number = c.Long(nullable: false),
                        DateTime = c.DateTime(),
                        ApplicationNumber = c.Long(nullable: false),
                        ShortName = c.String(),
                        Operation = c.String(),
                        Price = c.Single(nullable: false),
                        Count = c.Int(nullable: false),
                        Volume = c.Single(nullable: false),
                        Percent = c.Single(nullable: false),
                        Comission = c.Single(nullable: false),
                        SettlementDay = c.DateTime(),
                        Account = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysActDT = c.DateTime(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                        Contract_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.Contracts", t => t.Contract_sysGuid)
                .Index(t => t.Contract_sysGuid);
            
            CreateTable(
                "dbo.VRRDM_FreeCash",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        InputActive = c.Single(nullable: false),
                        PortfolioValue = c.Single(nullable: false),
                        BlockierBuy = c.Single(nullable: false),
                        ToPurchase = c.Single(nullable: false),
                        MinimumMargin = c.Single(nullable: false),
                        StartMargin = c.Single(nullable: false),
                        AmbulanceMargin = c.Single(nullable: false),
                        Status = c.String(),
                        Account = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysActDT = c.DateTime(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.sysGuid);
            
            CreateTable(
                "dbo.VRRDM_Limit",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Group = c.String(),
                        Currency = c.String(),
                        InputBalance = c.Single(nullable: false),
                        CurrentBalance = c.Single(nullable: false),
                        Locked = c.Single(nullable: false),
                        Balance = c.Single(nullable: false),
                        Account = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysActDT = c.DateTime(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                        Contract_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.Contracts", t => t.Contract_sysGuid)
                .Index(t => t.Contract_sysGuid);
            
            CreateTable(
                "dbo.Quik_StopOrder",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Number = c.Long(nullable: false),
                        Dealer = c.String(),
                        DateTime = c.DateTime(),
                        DateTimeWithDrawal = c.DateTime(),
                        Type = c.String(),
                        PaperShortName = c.String(),
                        Operation = c.String(),
                        PaperStopPrice = c.String(),
                        DirectionStopPrice = c.String(),
                        StopPrice = c.Single(nullable: false),
                        DirectionStopLimit = c.String(),
                        StopLimit = c.Single(nullable: false),
                        Price = c.Single(nullable: false),
                        MarketStopLimit = c.String(),
                        Count = c.Int(nullable: false),
                        ActualCount = c.Int(nullable: false),
                        UsedCount = c.Int(nullable: false),
                        NumberApplication = c.Long(nullable: false),
                        TermsOfADeal = c.Long(nullable: false),
                        Time = c.DateTime(),
                        State = c.String(),
                        Result = c.String(),
                        RelatedApplication = c.Long(nullable: false),
                        RelatedApplicationPrice = c.Single(nullable: false),
                        Error = c.Single(nullable: false),
                        ProtectiveSpread = c.Single(nullable: false),
                        MarketTakeProfit = c.String(),
                        ApplicationCondition = c.Long(nullable: false),
                        Account = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysActDT = c.DateTime(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                        Contract_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.Contracts", t => t.Contract_sysGuid)
                .Index(t => t.Contract_sysGuid);
            
            CreateTable(
                "dbo.RTS_LimitCash",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        PrevOpenLimit = c.Single(nullable: false),
                        OpenLimit = c.Single(nullable: false),
                        CurrentFree = c.Single(nullable: false),
                        PlanFree = c.Single(nullable: false),
                        VariationMargin = c.Single(nullable: false),
                        AccruedIncome = c.Single(nullable: false),
                        ExchangeFees = c.Single(nullable: false),
                        Account = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysActDT = c.DateTime(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.sysGuid);
            
            CreateTable(
                "dbo.RTS_PositionInstrument",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        PartFreePosition = c.Single(nullable: false),
                        CurrentLongPosition = c.Single(nullable: false),
                        CurrentShortPosition = c.Single(nullable: false),
                        CurrentFreePosition = c.Single(nullable: false),
                        ActiveBuying = c.Single(nullable: false),
                        ActiveSales = c.Single(nullable: false),
                        Account = c.String(),
                        InstrumentName = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysActDT = c.DateTime(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                        Instrument_sysGuid = c.Guid(),
                        Contract_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.RTSInstruments", t => t.Instrument_sysGuid)
                .ForeignKey("dbo.Contracts", t => t.Contract_sysGuid)
                .Index(t => t.Instrument_sysGuid)
                .Index(t => t.Contract_sysGuid);
            
            CreateTable(
                "dbo.RTSInstruments",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Code = c.String(),
                        ShortName = c.String(),
                        MaturityDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.sysGuid);
            
            CreateTable(
                "dbo.MMVB_Limit",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Currency = c.String(),
                        Group = c.String(),
                        Type = c.String(),
                        InputBalance = c.Single(nullable: false),
                        CurrentBalance = c.Single(nullable: false),
                        Locked = c.Single(nullable: false),
                        Balance = c.Single(nullable: false),
                        Account = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysActDT = c.DateTime(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                        Contract_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.Contracts", t => t.Contract_sysGuid)
                .Index(t => t.Contract_sysGuid);
            
            CreateTable(
                "dbo.MMVB_Portfolio",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        Input = c.Single(nullable: false),
                        Price = c.Single(nullable: false),
                        LockBuy = c.Single(nullable: false),
                        OnBuy = c.Single(nullable: false),
                        MinMargin = c.Single(nullable: false),
                        StartMargin = c.Single(nullable: false),
                        SpeedMargin = c.Single(nullable: false),
                        Status = c.String(),
                        Type = c.String(),
                        Account = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysActDT = c.DateTime(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.sysGuid);
            
            CreateTable(
                "dbo.MMVB_Security",
                c => new
                    {
                        sysGuid = c.Guid(nullable: false),
                        PaperName = c.String(),
                        InputBalance = c.Single(nullable: false),
                        CurrentBalance = c.Single(nullable: false),
                        Summ = c.Single(nullable: false),
                        Locked = c.Single(nullable: false),
                        Type = c.String(),
                        Account = c.String(),
                        sysVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        sysStartDT = c.DateTime(nullable: false),
                        sysEndDT = c.DateTime(nullable: false),
                        sysActDT = c.DateTime(nullable: false),
                        sysStaus = c.Short(nullable: false),
                        sysModification = c.Binary(),
                        Guid = c.Guid(nullable: false),
                        Contract_sysGuid = c.Guid(),
                    })
                .PrimaryKey(t => t.sysGuid)
                .ForeignKey("dbo.Contracts", t => t.Contract_sysGuid)
                .Index(t => t.Contract_sysGuid);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserName = c.String(),
                        Password = c.String(),
                        LockOutEnd = c.DateTimeOffset(nullable: false, precision: 7),
                        AccessFailedCount = c.Int(nullable: false),
                        LockOutEnable = c.Boolean(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        User_Id = c.Guid(nullable: false),
                        Role_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Role_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.Role_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Role_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoles", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.UserRoles", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Contracts", "Client_sysGuid", "dbo.Clients");
            DropForeignKey("dbo.MMVB_Security", "Contract_sysGuid", "dbo.Contracts");
            DropForeignKey("dbo.Contracts", "SM_Portfolio_sysGuid", "dbo.MMVB_Portfolio");
            DropForeignKey("dbo.MMVB_Limit", "Contract_sysGuid", "dbo.Contracts");
            DropForeignKey("dbo.RTS_PositionInstrument", "Contract_sysGuid", "dbo.Contracts");
            DropForeignKey("dbo.RTS_PositionInstrument", "Instrument_sysGuid", "dbo.RTSInstruments");
            DropForeignKey("dbo.Contracts", "RTS_LimitCash_sysGuid", "dbo.RTS_LimitCash");
            DropForeignKey("dbo.Quik_StopOrder", "Contract_sysGuid", "dbo.Contracts");
            DropForeignKey("dbo.VRRDM_Limit", "Contract_sysGuid", "dbo.Contracts");
            DropForeignKey("dbo.Contracts", "CM_FreeCash_sysGuid", "dbo.VRRDM_FreeCash");
            DropForeignKey("dbo.Quik_Trade", "Contract_sysGuid", "dbo.Contracts");
            DropForeignKey("dbo.Quik_Order", "Contract_sysGuid", "dbo.Contracts");
            DropIndex("dbo.UserRoles", new[] { "Role_Id" });
            DropIndex("dbo.UserRoles", new[] { "User_Id" });
            DropIndex("dbo.MMVB_Security", new[] { "Contract_sysGuid" });
            DropIndex("dbo.MMVB_Limit", new[] { "Contract_sysGuid" });
            DropIndex("dbo.RTS_PositionInstrument", new[] { "Contract_sysGuid" });
            DropIndex("dbo.RTS_PositionInstrument", new[] { "Instrument_sysGuid" });
            DropIndex("dbo.Quik_StopOrder", new[] { "Contract_sysGuid" });
            DropIndex("dbo.VRRDM_Limit", new[] { "Contract_sysGuid" });
            DropIndex("dbo.Quik_Trade", new[] { "Contract_sysGuid" });
            DropIndex("dbo.Quik_Order", new[] { "Contract_sysGuid" });
            DropIndex("dbo.Contracts", new[] { "Client_sysGuid" });
            DropIndex("dbo.Contracts", new[] { "SM_Portfolio_sysGuid" });
            DropIndex("dbo.Contracts", new[] { "RTS_LimitCash_sysGuid" });
            DropIndex("dbo.Contracts", new[] { "CM_FreeCash_sysGuid" });
            DropTable("dbo.UserRoles");
            DropTable("dbo.Users");
            DropTable("dbo.Roles");
            DropTable("dbo.MMVB_Security");
            DropTable("dbo.MMVB_Portfolio");
            DropTable("dbo.MMVB_Limit");
            DropTable("dbo.RTSInstruments");
            DropTable("dbo.RTS_PositionInstrument");
            DropTable("dbo.RTS_LimitCash");
            DropTable("dbo.Quik_StopOrder");
            DropTable("dbo.VRRDM_Limit");
            DropTable("dbo.VRRDM_FreeCash");
            DropTable("dbo.Quik_Trade");
            DropTable("dbo.Quik_Order");
            DropTable("dbo.Contracts");
            DropTable("dbo.Clients");
        }
    }
}
