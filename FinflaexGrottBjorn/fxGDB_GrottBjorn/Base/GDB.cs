﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fxGDB_GrottBjorn.Tables;
using fxSupport.fdMsSQL.fsdBase;
using fxSupport.fdMsSQL.fsdTable;
using Microsoft.AspNet.Identity.EntityFramework;

namespace fxGDB_GrottBjorn.Base
{
    public class GDB : pcaDB_Identity<GDB>
    {
#if DEBUG
        public GDB()
            : base(Properties.Settings.Default.TestConnectionString , new MigrationGDB())
        {
            
        }
#else
        public GDB()
            : base(Properties.Settings.Default.ConnectionString , new MigrationGDB())
        {
            
        }
#endif

        public GDB(params object[] param)
            : this()
        {
            
        }

        public new static GDB Create()
        {
            return new GDB();
        }

        public static void Using(Action<GDB> action)
        {
            using (var db = Create())
            {
                action.Invoke(db);
            }
        }

        public static R Using<R>(Func<GDB, R> action)
        {
            using (var db = Create())
            {
                return action.Invoke(db);
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Client>()
                .HasMany(v => v.Contracts)
                .WithOptional(v => v.Client);

            modelBuilder.Entity<Contract>()
                .HasMany(v => v.RTS_PositionInstruments)
                .WithOptional(v => v.Contract);

            modelBuilder.Entity<Contract>()
                .HasOptional(v => v.RTS_LimitCash)
                .WithOptionalDependent(v => v.Contract);

            modelBuilder.Entity<Contract>()
                .HasMany(v => v.CM_LimitCash)
                .WithOptional(v => v.Contract);

            modelBuilder.Entity<Contract>()
                .HasMany(v => v.CM_Deals)
                .WithOptional(v => v.Contract);

            modelBuilder.Entity<Contract>()
                .HasMany(v => v.CM_StopApplications)
                .WithOptional(v => v.Contract);

            modelBuilder.Entity<Contract>()
                .HasOptional(v => v.CM_FreeCash)
                .WithOptionalDependent(v => v.Contract);

            modelBuilder.Entity<Contract>()
                .HasMany(v => v.CM_Applications)
                .WithOptional(v => v.Contract);

            modelBuilder.Entity<Contract>()
                .HasMany(v => v.SM_LimitCash)
                .WithOptional(v => v.Contract);

            modelBuilder.Entity<Contract>()
                .HasOptional(v => v.SM_Portfolio)
                .WithOptionalDependent(v => v.Contract);

            modelBuilder.Entity<Contract>()
                .HasMany(v => v.SM_Security)
                .WithOptional(v => v.Contract);

        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Contract> Contracts { get; set; }


        public DbSet<RTSInstrument> RtsInstruments { get; set; }
        public DbSet<RTS_LimitCash> RtsLimitCashes { get; set; }
        public DbSet<RTS_PositionInstrument> RtsPositionInstruments { get; set; }

        public DbSet<Quik_Trade> QuikTrades { get; set; }
        public DbSet<Quik_StopOrder> QuikStopOrders { get; set; }
        public DbSet<Quik_Order> QuikOrders { get; set; }


        public DbSet<VRRDM_Limit> VRRDMLimits { get; set; }
        public DbSet<VRRDM_FreeCash> VRRDMFreeCashes { get; set; }
        
        public DbSet<MMVB_Limit> MMVBLimits { get; set; }
        public DbSet<MMVB_Portfolio> MMVBPortfolios { get; set; }
        public DbSet<MMVB_Security> MMVBSecurities { get; set; }
    }
}
