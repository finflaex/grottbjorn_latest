﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using fxGDB_GrottBjorn.Tables;
using fxSupport.fdMsSQL.fsdOther;
using fxSupport.fdMsSQL.fsdTable;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace fxGDB_GrottBjorn.Base
{
    public class ConfigGDB : DbMigrationsConfiguration<GDB>
    {
        public ConfigGDB()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(GDB db)
        {
            if (!db.Database.Exists())
            {
                db.Database.Create();
            }

            var role_admin = db.Roles.FirstOrDefault(v => v.Name == "admin") ??
                             db.Roles.Add(new Role {Name = "admin"});
            var role_user = db.Roles.FirstOrDefault(v => v.Name == "user") ??
                             db.Roles.Add(new Role { Name = "user" });
            var user_admin = db.Users.FirstOrDefault(v => v.UserName == "finflaex") ??
                             db.Users.Add(new User
                             {
                                 UserName = "finflaex",
                                 Email = "finflaex@gmail.com",
                                 Password = "test",
                             });
            var roles = user_admin.Roles.Select(v => v.Name).ToArray();
            if (!roles.Contains("admin"))
            {
                user_admin.Roles.Add(role_admin);
            }
            if (!roles.Contains("user"))
            {
                user_admin.Roles.Add(role_user);
            }
            db.SaveChanges();

            base.Seed(db);
        }
    }
}